within ThermoSysPro_H2.Properties.H2mixGases;
partial function unsafeForJacobian
  "Icon to mark functions that may not be used if analytic Jacobians are desired"
extends Modelica.Icons.Function;
  annotation (Icon(graphics));
end unsafeForJacobian;
