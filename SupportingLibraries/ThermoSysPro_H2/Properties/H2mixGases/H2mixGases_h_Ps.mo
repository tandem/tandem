within ThermoSysPro_H2.Properties.H2mixGases;
function H2mixGases_h_Ps "Specific enthalpy"
  //extends ThermoSysPro_H2.Properties.H2mixGases.unSafeForJacobian;

  input Units.SI.AbsolutePressure PMF "Flue gases average pressure";
  input Units.SI.SpecificEntropy SMF "Flue gases average specific entropy";
  input Real Xco2 "CO2 mass fraction";
  input Real Xh2o "H2O mass fraction";
  input Real Xo2 "O2 mass fraction";
  input Real Xh2 "H2 mass fraction";

  output Units.SI.SpecificEnthalpy h "Specific enthalpy";

protected
  ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.ThermodynamicState state;
  ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.ThermodynamicState state0;
  Units.SI.SpecificEntropy S0;
  Real Xn2 "N2 mass fraction";
  constant Real Hlat=2501.5999019e3 "Phase transition energy";
  constant Real Slat=9.157461e3 "Phase transition entropy";

algorithm
  Xn2 := 1 - Xco2 - Xh2o - Xo2 - Xh2;

  /* Computation of the reference thermodynamic state */
  state0.p :=0.006112*1e5;
  state0.T :=273.16;
  state0.X :={Xn2,Xo2,Xh2o,Xco2,Xh2};
//  state0 := ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.setState_pTX(0.006112*1e5, 273.16, {Xn2,Xo2,Xh2o,Xco2,Xh2});
  S0 := ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.specificEntropy(
    state0);

  /* Computation of the thermodynamic state */
  state := ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.setState_psX(
    PMF,
    SMF + S0 - Xh2o*Slat,
    {Xn2,Xo2,Xh2o,Xco2,Xh2});

  /* Computation of the specific enthalpy minus a reference at (0.006112 bars, 0.01°C) */
  h := ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.specificEnthalpy(
    state) -
    ThermoSysPro_H2.Properties.ModelicaMediaH2mixGases.specificEnthalpy(state0)
     + Xh2o*Hlat;

  annotation (smoothOrder=2,Icon(graphics),
              Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end H2mixGases_h_Ps;
