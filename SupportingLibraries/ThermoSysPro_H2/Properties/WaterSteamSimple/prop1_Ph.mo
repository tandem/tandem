within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop1_Ph "Thermodynamics properties in region 1, independent variables p and h"
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";

 output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_ph pro;

algorithm
  pro.T := ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.T1_Ph(p, h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d1_Ph(p, h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u1_Ph(p, h);
  pro.s := ThermoSysPro_H2.Properties.WaterSteamSimple.Entropy.s1_Ph(p, h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp1_Ph(p,
    h);
  pro.ddhp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1hp_Ph(p, h);
  pro.ddph := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1ph_Ph(p, h);
  pro.duph := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du1ph_Ph(p, h);
  pro.duhp := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du1hp_Ph(p, h);
  pro.x :=0;
end prop1_Ph;
