within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop2_Ph "Thermodynamics properties in region 2, independent variables p and h "
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_ph pro;

algorithm
  pro.T := ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.T2_Ph(p, h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d2_Ph(p, h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u2_Ph(p, h);
  pro.s := ThermoSysPro_H2.Properties.WaterSteamSimple.Entropy.s2_Ph(p, h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp2_Ph(p,
    h);
  pro.ddhp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2hp_Ph(p, h);
  pro.ddph := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2ph_Ph(p, h);
  pro.duph := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du2ph_Ph(p, h);
  pro.duhp := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du2hp_Ph(p, h);
  pro.x :=1;
end prop2_Ph;
