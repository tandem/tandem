within ThermoSysPro_H2.Properties.WaterSteamSimple;
function propsat1_P
  input Units.SI.AbsolutePressure p "Pressure";
  output ThermoSysPro_H2.Properties.WaterSteamSimple.PropThermoSat pro;

protected
  Units.SI.SpecificEnthalpy hl "Bubble enthalpy";

algorithm
  hl := Enthalpy.h1sat_P(p);

  pro.P := p;
  pro.T := ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.Tsat_P(p);
  pro.rho := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d1_Ph(p, hl);
  pro.h := hl;
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp1_Ph(p,
    hl);
  pro.pt := 1; //NA
  pro.cv := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cv1_Ph(p,
    hl);
end propsat1_P;
