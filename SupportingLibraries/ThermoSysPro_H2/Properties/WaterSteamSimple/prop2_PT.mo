within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop2_PT "Thermodynamics properties in region 2, independent variables p and T "
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.Temperature T "Temperature";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_pT pro;

algorithm
  pro.h := ThermoSysPro_H2.Properties.WaterSteamSimple.Enthalpy.h2_PT(p, T);
  pro.s := ThermoSysPro_H2.Properties.WaterSteamSimple.Entropy.s2_Ph(p, pro.h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d2_Ph(p, pro.h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u2_Ph(p, pro.h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp2_Ph(p,
    pro.h);
  pro.ddTp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2Tp_PT(p, T);
  pro.ddpT := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2pT_PT(p, T);
  pro.duTp := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du2Tp_PT(p, T);
  pro.dupT := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du2pT_PT(p, T);
  pro.x :=0;
end prop2_PT;
