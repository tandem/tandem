within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop1_Ps "Thermodynamics properties in region 1, independent variables p and s "
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEntropy s "Specific entropy";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_ps pro;

algorithm
  pro.h := ThermoSysPro_H2.Properties.WaterSteamSimple.Enthalpy.h1_Ps(p, s);
  pro.T := ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.T1_Ph(p, pro.h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d1_Ph(p, pro.h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u1_Ph(p, pro.h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp1_Ph(p,
    pro.h);
  pro.ddsp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1sp_Ps(p, s);
  pro.ddps := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1ps_Ps(p, s);
  pro.x :=0;
end prop1_Ps;
