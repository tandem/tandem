within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop1_PT "Thermodynamics properties in region 1, independent variables p and T "
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.Temperature T "Temperature";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_pT pro;

algorithm
  pro.h := ThermoSysPro_H2.Properties.WaterSteamSimple.Enthalpy.h1_PT(p, T);
  pro.s := ThermoSysPro_H2.Properties.WaterSteamSimple.Entropy.s1_Ph(p, pro.h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d1_Ph(p, pro.h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u1_Ph(p, pro.h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp1_Ph(p,
    pro.h);
  pro.ddTp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1Tp_PT(p, T);
  pro.ddpT := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd1pT_PT(p, T);
  pro.duTp := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du1Tp_PT(p, T);
  pro.dupT := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.du1pT_PT(p, T);
  pro.x :=0;
end prop1_PT;
