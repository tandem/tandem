within ThermoSysPro_H2.Properties.WaterSteamSimple;
function propsat2_P_der
  input Units.SI.AbsolutePressure p "Pressure";
  input Real p_der "Derivative of Pressure";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.PropThermoSat der_pro;

protected
  Units.SI.SpecificEnthalpy hv "Dew enthalpy";

algorithm
  hv := Enthalpy.h2sat_P(p);

  der_pro.P := 1;
  der_pro.T := p_der*
    ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.dTsatp_P(p);
  der_pro.rho := p_der*
    ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2ph_Ph(p, hv);
  der_pro.h := p_der*
    ThermoSysPro_H2.Properties.WaterSteamSimple.Enthalpy.dh2satp_P(p);
  der_pro.cp := p_der*
    ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.dcp2ph_Ph(p, hv);
  der_pro.pt := 1; //NA
  der_pro.cv := p_der*
    ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.dcv2ph_Ph(p, hv);
end propsat2_P_der;
