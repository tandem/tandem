within ThermoSysPro_H2.Properties.WaterSteamSimple;
function prop2_Ps "Thermodynamics properties in region 2, independent variables p and h "
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEntropy s "Specific entropy";

  output ThermoSysPro_H2.Properties.WaterSteamSimple.ThermoProperties_ps pro;

algorithm
  pro.h := ThermoSysPro_H2.Properties.WaterSteamSimple.Enthalpy.h2_Ps(p, s);
  pro.T := ThermoSysPro_H2.Properties.WaterSteamSimple.Temperature.T2_Ph(p, pro.h);
  pro.d := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.d2_Ph(p, pro.h);
  pro.u := ThermoSysPro_H2.Properties.WaterSteamSimple.Energy.u2_Ph(p, pro.h);
  pro.cp := ThermoSysPro_H2.Properties.WaterSteamSimple.HeatCapacity.cp2_Ph(p,
    pro.h);
  pro.ddsp := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2sp_Ps(p, s);
  pro.ddps := ThermoSysPro_H2.Properties.WaterSteamSimple.Density.dd2ps_Ps(p, s);
  pro.x :=1;
end prop2_Ps;
