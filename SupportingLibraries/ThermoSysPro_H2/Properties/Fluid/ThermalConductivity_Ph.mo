within ThermoSysPro_H2.Properties.Fluid;
function ThermalConductivity_Ph
  "Thermal Conductivity computation for all fluids (inputs: P, h, fluid)"

  input Units.SI.AbsolutePressure P "Pressure (Pa)";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";
  input Integer fluid
    "<html>Fluid number: <br>1 - Water/Steam <br>2 - C3H3F5 <br>3 - H2mixGases <br>4 - MoltenSalt <br>5 - Oil <br>6 - DryAirIdealGas <br>7 - WaterSteamSimple </html>";
  input Integer mode "IF97 region - 0:automatic computation";
  input Real Xco2 "CO2 mass fraction";
  input Real Xh2o "H2O mass fraction";
  input Real Xo2 "O2 mass fraction";
  input Real Xh2 "H2 mass fraction";

  output Units.SI.ThermalConductivity k "Thermal Conductivity (W/m/K)";
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ph pro
    annotation (Placement(transformation(extent={{-80,40},{-40,80}}, rotation=0)));

algorithm
  // Water/Steam  /// FONCTIONNE EN DIPHASIQUE ???
  if fluid==1 then
    pro := ThermoSysPro_H2.Properties.WaterSteam.IF97.Water_Ph(
      P,
      h,
      mode);
    k := ThermoSysPro_H2.Properties.WaterSteam.IF97.ThermalConductivity_rhoT(
      rho=pro.d,
      T=pro.T,
      P=P,
      region=mode);

  // C3H3F5
  elseif fluid==2 then
  //   k := ThermoSysPro_H2.Properties.C3H3F5.ThermalConductivity_Ph__NonFonctionnel(
  //     P=P, h=h);
    assert(false, "?????????? fluid = 2: the function ThermalConductivity_Ph is not available; incorrect option ?????????");

  // FlueGas
  elseif fluid==3 then
    assert(Xco2+Xh2o+Xo2+Xh2>0, "Wrong mass fraction definition");  /// Commentaire ajouté (si fluid==2, vérifie que les fractions massiques sont bien fournies en Input)
    k := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_k(
      PMF=P,
      TMF=ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_T(
        PMF=P,
        HMF=h,
        Xco2=Xco2,
        Xh2o=Xh2o,
        Xo2=Xo2,
        Xh2=Xh2),
      Xco2=Xco2,
      Xh2o=Xh2o,
      Xo2=Xo2,
      Xh2=Xh2);

  // MoltenSalt
  elseif fluid==4 then
    k := ThermoSysPro_H2.Properties.MoltenSalt.ThermalConductivity_T(T=
      ThermoSysPro_H2.Properties.MoltenSalt.Temperature_h(h=h));

  // Oil
  elseif fluid==5 then
    k := ThermoSysPro_H2.Properties.Oil_TherminolVP1.ThermalConductivity_T(temp=
       ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_h(h=h));

  // Dry Air Ideal Gas
  elseif fluid==6 then
    k := ThermoSysPro_H2.Properties.DryAirIdealGas.ThermalConductivity_Trho(T=
      ThermoSysPro_H2.Properties.DryAirIdealGas.Temperature_h(h=h), rho=
      ThermoSysPro_H2.Properties.DryAirIdealGas.Density_PT(P=P, T=
      ThermoSysPro_H2.Properties.DryAirIdealGas.Temperature_h(h=h)));

  // Water/Steam Simple
  elseif  fluid==7 then
    pro := ThermoSysPro_H2.Properties.WaterSteamSimple.SimpleWater.Water_Ph(
      P,
      h,
      mode);
    k :=
      ThermoSysPro_H2.Properties.WaterSteamSimple.SimpleWater.ThermalConductivity_rhoT(
      rho=pro.d, T=pro.T);

  else
    assert(false, "incorrect fluid number");  ///
  end if;

end ThermalConductivity_Ph;
