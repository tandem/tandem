within ThermoSysPro_H2.Properties.Fluid;
function derDynamicViscosity_derP_derh
  "Dynamic Viscosity computation for all fluids (inputs: P, h, fluid)"

  input Units.SI.AbsolutePressure P "Pressure (Pa)";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";
  input Integer fluid
    "<html>Fluid number: <br>1 - Water/Steam <br>2 - C3H3F5 <br>3 - H2mixGases <br>4 - MoltenSalt <br>5 - Oil <br>6 - DryAirIdealGas <br>7 - WaterSteamSimple </html>";
  input Integer mode "IF97 region - 0:automatic computation";
  input Real Xco2 "CO2 mass fraction";
  input Real Xh2o "H2O mass fraction";
  input Real Xo2 "O2 mass fraction";
  input Real Xh2 "H2 mass fraction";
  input Real der_rho "Density time derivative";
  input Real der_T "Temperature time derivative";
  input Real der_Xco2=0 "CO2 mass fraction";
  input Real der_Xh2o=0 "H2O mass fraction";
  input Real der_Xo2=0 "O2 mass fraction";
  input Real der_Xh2=0 "H2 mass fraction";

  output Real der_mu "Dynamic Viscosity time derivative";

protected
  constant Real Viscosity_c0 = 0.07551475951333098;
  constant Real Viscosity_c1 = - 0.00027760397992950003;
  constant Real Viscosity_c2 = 0.00000034888693;
  constant Real Viscosity_c3 = - 0.00000000014739999;
  Units.SI.Duration dt=1;
  Units.SI.AbsolutePressure delta_P=0.001*P;
  Units.SI.SpecificEnthalpy delta_h=0.001*h;

protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ph pro
    annotation (Placement(transformation(extent={{-80,40},{-40,80}}, rotation=0)));

algorithm
  // Water/Steam  /// FONCTIONNE EN DIPHASIQUE ???
  if fluid==1 then

    //der_mu := 0.0;

    der_mu := (ThermoSysPro_H2.Properties.Fluid.DynamicViscosity_Ph(
      P=P + delta_P,
      h=h + delta_h,
      fluid=fluid,
      mode=mode,
      Xco2=Xco2,
      Xh2o=Xh2o,
      Xo2=Xo2,
      Xh2=Xh2) - ThermoSysPro_H2.Properties.Fluid.DynamicViscosity_Ph(
      P=P - delta_P,
      h=h - delta_h,
      fluid=fluid,
      mode=mode,
      Xco2=Xco2,
      Xh2o=Xh2o,
      Xo2=Xo2,
      Xh2=Xh2))/dt;

  // C3H3F5
  // elseif fluid==2 then
  //   mu := ThermoSysPro_H2.Properties.C3H3F5.DynamicViscosity_Ph__NonFonctionnel(P=
  //     P, h=h);

  // FlueGas
  elseif fluid==3 then
  // if fluid==3 then
     assert(Xco2+Xh2o+Xo2+Xh2>0, "Wrong mass fraction definition");  /// Commentaire ajouté (si fluid==2, vérifie que les fractions massiques sont bien fournies en Input)
  //   mu := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_mu(
  //     PMF=P,
  //     TMF=ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_T(
  //       PMF=P,
  //       HMF=h,
  //       Xco2=Xco2,
  //       Xh2o=Xh2o,
  //       Xo2=Xo2,
  //       Xh2=Xh2),
  //     Xco2=Xco2,
  //     Xh2o=Xh2o,
  //     Xo2=Xo2,
  //     Xh2=Xh2);

  // MoltenSalt
  // elseif fluid==4 then
  //   mu := ThermoSysPro_H2.Properties.MoltenSalt.DynamicViscosity_T(T=
  //     ThermoSysPro_H2.Properties.MoltenSalt.Temperature_h(h=h));

  // Oil
  //elseif fluid==5 then
  elseif fluid==4 then
    //der_mu := Viscosity_c1*ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_derh(h=h,der_h=der_h) + 2*Viscosity_c2*ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_h(h=h)*ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_derh(h=h,der_h=der_h) + 3*Viscosity_c3*(ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_h(h=h))^2*ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_derh(h=h,der_h=der_h);
/*    der_mu := ThermoSysPro_H2.Properties.MoltenSalt.DynamicViscosity_dert(
      temp=ThermoSysPro_H2.Properties.MoltenSalt.Temperature_h(h=h),
      der_temp=ThermoSysPro_H2.Properties.MoltenSalt.Temperature_derh(h=h,der_h=der_h));
*/
  elseif fluid==5 then
/*    der_mu := ThermoSysPro_H2.Properties.Oil_TherminolVP1.DynamicViscosity_dert(
      temp=ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_h(h=h),
      der_temp=ThermoSysPro_H2.Properties.Oil_TherminolVP1.Temperature_derh(h=h,der_h=der_h));
*/
  // Dry Air Ideal Gas
  // elseif fluid==6 then
  //   mu := ThermoSysPro_H2.Properties.DryAirIdealGas.DynamicViscosity_Trho(
  //     T=ThermoSysPro_H2.Properties.DryAirIdealGas.Temperature_h(h=h),
  //     rho=ThermoSysPro_H2.Properties.DryAirIdealGas.Density_PT(
  //       P=P, T=ThermoSysPro_H2.Properties.DryAirIdealGas.Temperature_h(h=h)));

  elseif fluid==7 then
    //der_mu := 0.0001;
    //

   //LogVariable(der_mu);
  else
    assert(false, "incorrect fluid number");  ///
  end if;

end derDynamicViscosity_derP_derh;
