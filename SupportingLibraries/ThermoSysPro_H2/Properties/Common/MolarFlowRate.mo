within ThermoSysPro_H2.Properties.Common;
type MolarFlowRate = Real (final quantity="MolarFlowRate", final unit="mol/s");
