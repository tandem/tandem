within ThermoSysPro_H2.Properties.Common;
type DerPressureByTemperature = Real (final quantity=
        "DerPressureByTemperature", final unit="Pa/K");
