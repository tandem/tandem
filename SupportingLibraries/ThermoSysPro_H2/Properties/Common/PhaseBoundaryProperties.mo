within ThermoSysPro_H2.Properties.Common;
record PhaseBoundaryProperties
  "Thermodynamic base properties on the phase boundary"
  extends Modelica.Icons.Record;
  Units.SI.Density d "Density";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificEnergy u "Inner energy";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "Heat capacity at constant volume";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
end PhaseBoundaryProperties;
