within ThermoSysPro_H2.Properties.Common;
record NewtonDerivatives_ph
  "Derivatives for fast inverse calculations of Helmholtz functions: p & h"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  Real hd "Derivative of specific enthalpy w.r.t. density";
  Real ht "Derivative of specific enthalpy w.r.t. temperature";
end NewtonDerivatives_ph;
