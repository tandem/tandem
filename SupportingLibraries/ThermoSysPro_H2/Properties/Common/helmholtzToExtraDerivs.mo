within ThermoSysPro_H2.Properties.Common;
function helmholtzToExtraDerivs
  "Compute additional thermodynamic derivatives from dimensionless Helmholtz function"

  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output ExtraDerivatives dpro "Additional property derivatives";
protected
  Units.SI.Pressure p "Pressure";
  Units.SI.SpecificVolume v "Specific volume";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureBySpecificVolume pv
    "Derivative of pressure w.r.t. specific volume";
  Units.SI.SpecificHeatCapacity cv "Isochoric specific heat capacity";
algorithm
  v := 1/f.d;
  p := f.R*f.d*f.T*f.delta*f.fdelta;
  pv := -(f.d*f.d)*f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  cv := f.R*(-f.tau*f.tau*f.ftautau);
  dpro.kappa := 1/(f.d*p)*((-pv*cv + pt*pt*f.T)/(cv));
  dpro.theta := -1/(f.d*p)*((-pv*cv + f.T*pt*pt)/(cv + pt*v));
  dpro.alpha := -f.d*pt/pv;
  dpro.beta := pt/p;
  dpro.gamma := -f.d/pv;
  dpro.mu := (v*pv + f.T*pt)/(pt*pt*f.T - pv*cv);
end helmholtzToExtraDerivs;
