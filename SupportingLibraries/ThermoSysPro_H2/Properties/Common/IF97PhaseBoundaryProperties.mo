within ThermoSysPro_H2.Properties.Common;
record IF97PhaseBoundaryProperties
  "Thermodynamic base properties on the phase boundary for IF97 steam tables"

  extends Modelica.Icons.Record;
  Boolean region3boundary "True if boundary between 2-phase and region 3";
  Units.SI.SpecificHeatCapacity R "Specific heat capacity";
  Units.SI.Temperature T "Temperature";
  Units.SI.Density d "Density";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "Heat capacity at constant volume";
  DerPressureByTemperature dpT "dp/dT derivative of saturation curve";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  Real vt(unit="m3/(kg.K)")
    "Derivative of specific volume w.r.t. temperature";
  Real vp(unit="m3/(kg.Pa)") "Derivative of specific volume w.r.t. pressure";
end IF97PhaseBoundaryProperties;
