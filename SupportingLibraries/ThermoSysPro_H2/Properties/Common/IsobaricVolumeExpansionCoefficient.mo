within ThermoSysPro_H2.Properties.Common;
type IsobaricVolumeExpansionCoefficient = Real (final quantity=
        "IsobaricVolumeExpansionCoefficient", unit="1/K");
