within ThermoSysPro_H2.Properties.Common;
type DerEntropyByPressure = Real (final quantity="DerEntropyByPressure",
      final unit="J/(K.Pa)");
