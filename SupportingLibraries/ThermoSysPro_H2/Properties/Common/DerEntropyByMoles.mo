within ThermoSysPro_H2.Properties.Common;
type DerEntropyByMoles = Real (final quantity="DerEntropyByMoles", final unit=
       "J/(mol.K)");
