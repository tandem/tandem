within ThermoSysPro_H2.Properties.Common;
function Gibbs2_dT
  "Function to calculate analytic derivatives for computing p given d and T"
  extends Modelica.Icons.Function;
  input ThermoSysPro_H2.Properties.Common.GibbsDerivs2 g
    "Dimensionless derivatives of Gibbs function";
  output ThermoSysPro_H2.Properties.Common.NewtonDerivatives_dT nderivs
    "Derivatives for Newton iteration to compute p from d and T";

algorithm
  nderivs.v := g.gp;
  nderivs.vp := nderivs.v*g.gpp/g.gp;
end Gibbs2_dT;
