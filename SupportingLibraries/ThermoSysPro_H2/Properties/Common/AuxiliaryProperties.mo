within ThermoSysPro_H2.Properties.Common;
record AuxiliaryProperties "Intermediate property data record"
  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificHeatCapacity R "Gas constant";
  Units.SI.SpecificHeatCapacity cp "Specific heat capacity";
  Units.SI.SpecificHeatCapacity cv "Specific heat capacity";
  Units.SI.Density rho "Density";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.DerPressureByTemperature pt
    "Derivative of pressure w.r.t. temperature";
  Units.SI.DerPressureByDensity pd
    "Derivative of pressure w.r.t. density";
  Real vt "Derivative of specific volume w.r.t. temperature";
  Real vp "Derivative of specific volume w.r.t. pressure";
end AuxiliaryProperties;
