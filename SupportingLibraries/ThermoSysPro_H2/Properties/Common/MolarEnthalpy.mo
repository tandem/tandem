within ThermoSysPro_H2.Properties.Common;
type MolarEnthalpy = Real (final quantity="MolarEnthalpy", final unit="J/mol");
