within ThermoSysPro_H2.Properties.Common;
type IsentropicExponent = Real (final quantity="IsentropicExponent", unit="1");
