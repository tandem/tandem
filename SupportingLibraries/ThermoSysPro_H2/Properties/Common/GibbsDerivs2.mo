within ThermoSysPro_H2.Properties.Common;
record GibbsDerivs2
  "Derivatives of Gibbs function w.r.t. pressure and temperature"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificHeatCapacity R "Specific heat capacity";
  Real pi(unit="1") "Dimensionless pressure";
  Real theta(unit="1") "Dimensionless temperature";
  Real g(unit="J/kg") "Gibbs function";
  Real gp(unit="m3/kg") "Derivative of g w.r.t. p";
  Real gpp(unit="m3/(kg.Pa)") "2nd derivative of g w.r.t. p";
  Real gT(unit="J/(kg.K)") "Derivative of g w.r.t. T";
  Real gTT(unit="J/(kg.K2)") "2nd derivative of g w.r.t. T";
  Real gTp(unit="m3/(kg.K)") "Mixed derivative of g w.r.t. T and p";
end GibbsDerivs2;
