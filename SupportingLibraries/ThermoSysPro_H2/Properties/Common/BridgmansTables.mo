within ThermoSysPro_H2.Properties.Common;
record BridgmansTables
  "Calculates all entries in Bridgmans tables if first seven variables given"
  extends Modelica.Icons.Record;
  // the first 7 need to calculated in a function!
  Units.SI.SpecificVolume v "Specific volume";
  Units.SI.Pressure p "Pressure";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  IsobaricVolumeExpansionCoefficient alpha
    "Isobaric volume expansion coefficient";
  // beta in Bejan
  IsothermalCompressibility gamma "Isothermal compressibility";
  // kappa in Bejan
  // Derivatives at constant pressure
  Real dTp=1 "Coefficient in Bridgmans table, see info for usage";
  Real dpT=-dTp "Coefficient in Bridgmans table, see info for usage";
  Real dvp=alpha*v "Coefficient in Bridgmans table, see info for usage";
  Real dpv=-dvp "Coefficient in Bridgmans table, see info for usage";
  Real dsp=cp/T "Coefficient in Bridgmans table, see info for usage";
  Real dps=-dsp "Coefficient in Bridgmans table, see info for usage";
  Real dup=cp - alpha*p*v
    "Coefficient in Bridgmans table, see info for usage";
  Real dpu=-dup "Coefficient in Bridgmans table, see info for usage";
  Real dhp=cp "Coefficient in Bridgmans table, see info for usage";
  Real dph=-dhp "Coefficient in Bridgmans table, see info for usage";
  Real dfp=-s - alpha*p*v
    "Coefficient in Bridgmans table, see info for usage";
  Real dpf=-dfp "Coefficient in Bridgmans table, see info for usage";
  Real dgp=-s "Coefficient in Bridgmans table, see info for usage";
  Real dpg=-dgp "Coefficient in Bridgmans table, see info for usage";
  // Derivatives at constant Temperature
  Real dvT=gamma*v "Coefficient in Bridgmans table, see info for usage";
  Real dTv=-dvT "Coefficient in Bridgmans table, see info for usage";
  Real dsT=alpha*v "Coefficient in Bridgmans table, see info for usage";
  Real dTs=-dsT "Coefficient in Bridgmans table, see info for usage";
  Real duT=alpha*T*v - gamma*p*v
    "Coefficient in Bridgmans table, see info for usage";
  Real dTu=-duT "Coefficient in Bridgmans table, see info for usage";
  Real dhT=-v + alpha*T*v
    "Coefficient in Bridgmans table, see info for usage";
  Real dTh=-dhT "Coefficient in Bridgmans table, see info for usage";
  Real dfT=-gamma*p*v "Coefficient in Bridgmans table, see info for usage";
  Real dTf=-dfT "Coefficient in Bridgmans table, see info for usage";
  Real dgT=-v "Coefficient in Bridgmans table, see info for usage";
  Real dTg=-dgT "Coefficient in Bridgmans table, see info for usage";
  // Derivatives at constant v
  Real dsv=alpha*alpha*v*v - gamma*v*cp/T
    "Coefficient in Bridgmans table, see info for usage";
  Real dvs=-dsv "Coefficient in Bridgmans table, see info for usage";
  Real duv=T*alpha*alpha*v*v - gamma*v*cp
    "Coefficient in Bridgmans table, see info for usage";
  Real dvu=-duv "Coefficient in Bridgmans table, see info for usage";
  Real dhv=T*alpha*alpha*v*v - alpha*v*v - gamma*v*cp
    "Coefficient in Bridgmans table, see info for usage";
  Real dvh=-dhv "Coefficient in Bridgmans table, see info for usage";
  Real dfv=gamma*v*s "Coefficient in Bridgmans table, see info for usage";
  Real dvf=-dfv "Coefficient in Bridgmans table, see info for usage";
  Real dgv=gamma*v*s - alpha*v*v
    "Coefficient in Bridgmans table, see info for usage";
  Real dvg=-dgv "Coefficient in Bridgmans table, see info for usage";
  // Derivatives at constant s
  Real dus=dsv*p "Coefficient in Bridgmans table, see info for usage";
  Real dsu=-dus "Coefficient in Bridgmans table, see info for usage";
  Real dhs=-v*cp/T "Coefficient in Bridgmans table, see info for usage";
  Real dsh=-dhs "Coefficient in Bridgmans table, see info for usage";
  Real dfs=alpha*v*s + dus
    "Coefficient in Bridgmans table, see info for usage";
  Real dsf=-dfs "Coefficient in Bridgmans table, see info for usage";
  Real dgs=alpha*v*s - v*cp/T
    "Coefficient in Bridgmans table, see info for usage";
  Real dsg=-dgs "Coefficient in Bridgmans table, see info for usage";
  // Derivatives at constant u
  Real dhu=p*alpha*v*v + gamma*v*cp*p - v*cp - p*T*alpha*alpha*v*v
    "Coefficient in Bridgmans table, see info for usage";
  Real duh=-dhu "Coefficient in Bridgmans table, see info for usage";
  Real dfu=s*T*alpha*v - gamma*v*cp*p - gamma*v*s*p + p*T*alpha*alpha*v*v
    "Coefficient in Bridgmans table, see info for usage";
  Real duf=-dfu "Coefficient in Bridgmans table, see info for usage";
  Real dgu=alpha*v*v*p + alpha*v*s*T - v*cp - gamma*v*s*p
    "Coefficient in Bridgmans table, see info for usage";
  Real dug=-dgu "Coefficient in Bridgmans table, see info for usage";
  //  Derivatives at constant h
  Real dfh=(s - v*alpha*p)*(v - v*alpha*T) - gamma*v*cp*p
    "Coefficient in Bridgmans table, see info for usage";
  Real dhf=-dfh "Coefficient in Bridgmans table, see info for usage";
  Real dgh=alpha*v*s*T - v*(s + cp)
    "Coefficient in Bridgmans table, see info for usage";
  Real dhg=-dgh "Coefficient in Bridgmans table, see info for usage";
  // Derivatives at constant g
  Real dfg=gamma*v*s*p - v*s - alpha*v*v*p
    "Coefficient in Bridgmans table, see info for usage";
  Real dgf=-dfg "Coefficient in Bridgmans table, see info for usage";
  annotation (Documentation(info="<html>
<p>
Important: the phase equilibrium conditions are not yet considered.
this means that Bridgman's tables do not yet work in the two phase region.
Some derivatives are 0 or infinity anyways.
Idea: Do not use the values in Bridgmans table directly, all
derivatives are calculated as the quotient of two entries in the
table. The last letter indicates which variable is held constant in
taking the derivative. The second letters are the two variables
involved in the derivative and the first letter is always a d to remind
of differentiation.
</p>

<pre>
Example 1: Get the derivative of specific entropy s w.r.t. Temperature at
constant specific volume (between identical to constant density)
constant volume  --> last letter v
Temperature      --> second letter T
Specific entropy --> second letter s
--> the needed value is dsv/dTv
Known variables:
Temperature T
pressure p
specific volume v
specific inner energy u
specific enthalpy h
specific entropy s
specific Helmholtz energy f
specific gibbs enthalpy g
Not included but useful:
density d
In order to convert derivatives involving density use the following
rules:
at constant density == at constant specific volume
ddx/dyx = -d*d*dvx/dyx with y,x any of T,p,u,h,s,f,g
dyx/ddx = -1/(d*d)dyx/dvx with y,x any of T,p,u,h,s,f,g
Usage example assuming water as the medium:
model BridgmansTablesForWater
extends ThermoFluid.BaseClasses.MediumModels.Water.WaterSteamMedium_ph;
Real derOfsByTAtConstantv \"derivative of sp. entropy by temperature at constant sp. volume\"
ThermoFluid.BaseClasses.MediumModels.Common.ExtraDerivatives dpro;
ThermoFluid.BaseClasses.MediumModels.Common.BridgmansTables bt;
equation
dpro = ThermoFluid.BaseClasses.MediumModels.SteamIF97.extraDerivs_pT(p[1],T[1]);
bt.p = p[1];
bt.T = T[1];
bt.v = 1/pro[1].d;
bt.s = pro[1].s;
bt.cp = pro[1].cp;
bt.alpha = dpro.alpha;
bt.gamma = dpro.gamma;
derOfsByTAtConstantv =  bt.dsv/bt.dTv;
                ...
end BridgmansTablesForWater;
                </pre>

                </html>"));
end BridgmansTables;
