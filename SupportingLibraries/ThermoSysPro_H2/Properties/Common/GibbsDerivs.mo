within ThermoSysPro_H2.Properties.Common;
record GibbsDerivs
  "Derivatives of dimensionless Gibbs-function w.r.t. dimensionless pressure and temperature"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificHeatCapacity R "Specific heat capacity";
  Real pi(unit="1") "Dimensionless pressure";
  Real tau(unit="1") "Dimensionless temperature";
  Real g(unit="1") "Dimensionless Gibbs-function";
  Real gpi(unit="1") "Derivative of g w.r.t. pi";
  Real gpipi(unit="1") "2nd derivative of g w.r.t. pi";
  Real gtau(unit="1") "Derivative of g w.r.t. tau";
  Real gtautau(unit="1") "2nd derivative of g w.r.t. tau";
  Real gtaupi(unit="1") "Mixed derivative of g w.r.t. pi and tau";
end GibbsDerivs;
