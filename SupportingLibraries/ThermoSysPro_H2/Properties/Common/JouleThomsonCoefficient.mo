within ThermoSysPro_H2.Properties.Common;
type JouleThomsonCoefficient = Real (final quantity="JouleThomsonCoefficient",
      unit="K/Pa");
