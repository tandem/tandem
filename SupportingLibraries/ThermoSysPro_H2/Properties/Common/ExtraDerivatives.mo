within ThermoSysPro_H2.Properties.Common;
record ExtraDerivatives "Additional thermodynamic derivatives"
  extends Modelica.Icons.Record;
  IsentropicExponent kappa "Isentropic expansion coefficient";
  // k in Bejan
  IsenthalpicExponent theta "Isenthalpic exponent";
  // same as kappa, except derivative at const h
  IsobaricVolumeExpansionCoefficient alpha
    "Isobaric volume expansion coefficient";
  // beta in Bejan
  IsochoricPressureCoefficient beta "Isochoric pressure coefficient";
  // kT in Bejan
  IsothermalCompressibility gamma "Isothermal compressibility";
  // kappa in Bejan
  JouleThomsonCoefficient mu "Joule-Thomson coefficient";
  // mu_J in Bejan
end ExtraDerivatives;
