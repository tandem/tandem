within ThermoSysPro_H2.Properties.Common;
record NewtonDerivatives_pT
  "Derivatives for fast inverse calculations of Helmholtz functions:p & T"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
end NewtonDerivatives_pT;
