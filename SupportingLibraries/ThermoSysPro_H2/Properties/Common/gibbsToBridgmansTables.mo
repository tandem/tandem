within ThermoSysPro_H2.Properties.Common;
function gibbsToBridgmansTables
  "Calculates base coefficients for Bridgman's tables from gibbs enthalpy"

  extends Modelica.Icons.Function;
  input GibbsDerivs g "Dimensionless derivatives of Gibbs function";
  output Units.SI.SpecificVolume v "Specific volume";
  output Units.SI.Pressure p=g.p "Pressure";
  output Units.SI.Temperature T=g.T "Temperature";
  output Units.SI.SpecificEntropy s "Specific entropy";
  output Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  output IsobaricVolumeExpansionCoefficient alpha
    "Isobaric volume expansion coefficient";
  // beta in Bejan
  output IsothermalCompressibility gamma "Isothermal compressibility";
  // kappa in Bejan
protected
  Real vt(unit="m3/(kg.K)")
    "Derivative of specific volume w.r.t. temperature";
  Real vp(unit="m4.kg-2.s2") "Derivative of specific volume w.r.t. pressure";
algorithm
  vt := g.R/g.p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(g.p*g.p)*g.pi*g.pi*g.gpipi;
  v := (g.R*g.T*g.pi*g.gpi)/g.p;
  s := g.R*(g.tau*g.gtau - g.g);
  cp := -g.R*g.tau*g.tau*g.gtautau;
  alpha := vt/v;
  gamma := -vp/v;
end gibbsToBridgmansTables;
