within ThermoSysPro_H2.Properties.Common;
record NewtonDerivatives_dT
  "Derivatives for fast inverse calculations of Gibbs function"
  extends Modelica.Icons.Record;
  Units.SI.SpecificVolume v "Specific volume";
  Real vp "Derivative of specific volume w.r.t. pressure";
end NewtonDerivatives_dT;
