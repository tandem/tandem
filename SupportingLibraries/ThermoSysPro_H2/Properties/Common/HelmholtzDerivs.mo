within ThermoSysPro_H2.Properties.Common;
record HelmholtzDerivs
  "Derivatives of dimensionless Helmholtz-function w.r.t. dimensionless pressure, density and temperature"
  extends Modelica.Icons.Record;
  Units.SI.Density d "Density";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificHeatCapacity R "Specific heat capacity";
  Real delta(unit="1") "Dimensionless density";
  Real tau(unit="1") "Dimensionless temperature";
  Real f(unit="1") "Dimensionless Helmholtz-function";
  Real fdelta(unit="1") "Derivative of f w.r.t. delta";
  Real fdeltadelta(unit="1") "2nd derivative of f w.r.t. delta";
  Real ftau(unit="1") "Derivative of f w.r.t. tau";
  Real ftautau(unit="1") "2nd derivative of f w.r.t. tau";
  Real fdeltatau(unit="1") "Mixed derivative of f w.r.t. delta and tau";
end HelmholtzDerivs;
