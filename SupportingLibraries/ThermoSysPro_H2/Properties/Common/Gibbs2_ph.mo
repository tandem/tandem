within ThermoSysPro_H2.Properties.Common;
function Gibbs2_ph
  "Function to calculate analytic derivatives for computing T given p and h"
  extends Modelica.Icons.Function;
  input ThermoSysPro_H2.Properties.Common.GibbsDerivs2 g
    "Dimensionless derivatives of Gibbs function";
  output ThermoSysPro_H2.Properties.Common.NewtonDerivatives_ph nderivs
    "Derivatives for Newton iteration to calculate d and t from p and h";

algorithm
  nderivs.h := g.g - g.T*g.gT;
  nderivs.ht := -g.T*g.gTT;

  //dummy values - DO NOT USE
  nderivs.p := 0.0;
  nderivs.pd := 0.0;
  nderivs.pt := 0.0;
  nderivs.hd := 0.0;
end Gibbs2_ph;
