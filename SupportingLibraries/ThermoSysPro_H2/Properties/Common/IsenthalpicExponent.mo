within ThermoSysPro_H2.Properties.Common;
type IsenthalpicExponent = Real (final quantity="IsenthalpicExponent", unit=
        "1");
