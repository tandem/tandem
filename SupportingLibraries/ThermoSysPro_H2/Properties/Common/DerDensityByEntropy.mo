within ThermoSysPro_H2.Properties.Common;
type DerDensityByEntropy = Real (final quantity="DerDensityByEntropy", final
      unit="kg2.K/(m3.J)");
