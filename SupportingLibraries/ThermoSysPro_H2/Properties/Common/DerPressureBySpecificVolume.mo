within ThermoSysPro_H2.Properties.Common;
type DerPressureBySpecificVolume = Real (final quantity=
        "DerPressureBySpecificVolume", final unit="Pa.kg/m3");
