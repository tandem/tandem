within ThermoSysPro_H2.Properties;
package Common "Data structures and fundamental functions for fluid properties"

  // introduce min-manx-nominal values
  constant Real MINPOS=1.0e-9
    "Minimal value for physical variables which are always > 0.0";

  constant Units.SI.Area AMIN=MINPOS "Minimal init area";
  constant Units.SI.Area AMAX=1.0e5 "Maximal init area";
  constant Units.SI.Area ANOM=1.0 "Nominal init area";
  constant Units.SI.AmountOfSubstance MOLMIN=-1.0*MINPOS
    "Minimal Mole Number";
  constant Units.SI.AmountOfSubstance MOLMAX=1.0e8
    "Maximal Mole Number";
  constant Units.SI.AmountOfSubstance MOLNOM=1.0 "Nominal Mole Number";
  constant Units.SI.Density DMIN=1e-6 "Minimal init density";
  constant Units.SI.Density DMAX=30.0e3 "Maximal init density";
  constant Units.SI.Density DNOM=1.0 "Nominal init density";
  constant Units.SI.ThermalConductivity LAMMIN=MINPOS
    "Minimal thermal conductivity";
  constant Units.SI.ThermalConductivity LAMNOM=1.0
    "Nominal thermal conductivity";
  constant Units.SI.ThermalConductivity LAMMAX=1000.0
    "Maximal thermal conductivity";
  constant Units.SI.DynamicViscosity ETAMIN=MINPOS
    "Minimal init dynamic viscosity";
  constant Units.SI.DynamicViscosity ETAMAX=1.0e8
    "Maximal init dynamic viscosity";
  constant Units.SI.DynamicViscosity ETANOM=100.0
    "Nominal init dynamic viscosity";
  constant Units.SI.Energy EMIN=-1.0e10 "Minimal init energy";
  constant Units.SI.Energy EMAX=1.0e10 "Maximal init energy";
  constant Units.SI.Energy ENOM=1.0e3 "Nominal init energy";
  constant Units.SI.Entropy SMIN=-1.0e6 "Minimal init entropy";
  constant Units.SI.Entropy SMAX=1.0e6 "Maximal init entropy";
  constant Units.SI.Entropy SNOM=1.0e3 "Nominal init entropy";
  constant Units.SI.MassFlowRate MDOTMIN=-1.0e5
    "Minimal init mass flow rate";
  constant Units.SI.MassFlowRate MDOTMAX=1.0e5
    "Maximal init mass flow rate";
  constant Units.SI.MassFlowRate MDOTNOM=1.0
    "Nominal init mass flow rate";
  constant Units.SI.MassFraction MASSXMIN=-1.0*MINPOS
    "Minimal init mass fraction";
  constant Units.SI.MassFraction MASSXMAX=1.0
    "Maximal init mass fraction";
  constant Units.SI.MassFraction MASSXNOM=0.1
    "Nominal init mass fraction";
  constant Units.SI.Mass MMIN=-1.0*MINPOS "Minimal init mass";
  constant Units.SI.Mass MMAX=1.0e8 "Maximal init mass";
  constant Units.SI.Mass MNOM=1.0 "Nominal init mass";
  constant Units.SI.MolarMass MMMIN=0.001 "Minimal initial molar mass";
  constant Units.SI.MolarMass MMMAX=250.0 "Maximal initial molar mass";
  constant Units.SI.MolarMass MMNOM=0.2 "Nominal initial molar mass";
  constant Units.SI.MoleFraction MOLEYMIN=-1.0*MINPOS
    "Minimal init mole fraction";
  constant Units.SI.MoleFraction MOLEYMAX=1.0
    "Maximal init mole fraction";
  constant Units.SI.MoleFraction MOLEYNOM=0.1
    "Nominal init mole fraction";
  constant Units.SI.MomentumFlux GMIN=-1.0e8
    "Minimal init momentum flux";
  constant Units.SI.MomentumFlux GMAX=1.0e8
    "Maximal init momentum flux";
  constant Units.SI.MomentumFlux GNOM=1.0 "Nominal init momentum flux";
  constant Units.SI.Power POWMIN=-1.0e8 "Minimal init power or heat";
  constant Units.SI.Power POWMAX=1.0e8 "Maximal init power or heat";
  constant Units.SI.Power POWNOM=1.0e3 "Nominal init power or heat";
  constant Units.SI.Pressure PMIN=1.0e4 "Minimal init pressure";
  constant Units.SI.Pressure PMAX=1.0e8 "Maximal init pressure";
  constant Units.SI.Pressure PNOM=1.0e5 "Nominal init pressure";
  constant Units.SI.Pressure COMPPMIN=-1.0*MINPOS
    "Minimal init pressure";
  constant Units.SI.Pressure COMPPMAX=1.0e8 "Maximal init pressure";
  constant Units.SI.Pressure COMPPNOM=1.0e5 "Nominal init pressure";
  constant Units.SI.RatioOfSpecificHeatCapacities KAPPAMIN=1.0
    "Minimal init isentropic exponent";
  constant Units.SI.RatioOfSpecificHeatCapacities KAPPAMAX=1.7
    "Maximal init isentropic exponent";
  constant Units.SI.RatioOfSpecificHeatCapacities KAPPANOM=1.2
    "Nominal init isentropic exponent";
  constant Units.SI.SpecificEnergy SEMIN=-1.0e8
    "Minimal init specific energy";
  constant Units.SI.SpecificEnergy SEMAX=1.0e8
    "Maximal init specific energy";
  constant Units.SI.SpecificEnergy SENOM=1.0e6
    "Nominal init specific energy";
  constant Units.SI.SpecificEnthalpy SHMIN=-1.0e8
    "Minimal init specific enthalpy";
  constant Units.SI.SpecificEnthalpy SHMAX=1.0e8
    "Maximal init specific enthalpy";
  constant Units.SI.SpecificEnthalpy SHNOM=1.0e6
    "Nominal init specific enthalpy";
  constant Units.SI.SpecificEntropy SSMIN=-1.0e6
    "Minimal init specific entropy";
  constant Units.SI.SpecificEntropy SSMAX=1.0e6
    "Maximal init specific entropy";
  constant Units.SI.SpecificEntropy SSNOM=1.0e3
    "Nominal init specific entropy";
  constant Units.SI.SpecificHeatCapacity CPMIN=MINPOS
    "Minimal init specific heat capacity";
  constant Units.SI.SpecificHeatCapacity CPMAX=1.0e6
    "Maximal init specific heat capacity";
  constant Units.SI.SpecificHeatCapacity CPNOM=1.0e3
    "Nominal init specific heat capacity";
  constant Units.SI.Temperature TMIN=1.0 "Minimal init temperature";
  constant Units.SI.Temperature TMAX=6000.0 "Maximal init temperature";
  constant Units.SI.Temperature TNOM=320.0 "Nominal init temperature";
  constant Units.SI.ThermalConductivity LMIN=MINPOS
    "Minimal init thermal conductivity";
  constant Units.SI.ThermalConductivity LMAX=500.0
    "Maximal init thermal conductivity";
  constant Units.SI.ThermalConductivity LNOM=1.0
    "Nominal init thermal conductivity";
  constant Units.SI.Velocity VELMIN=-1.0e5 "Minimal init speed";
  constant Units.SI.Velocity VELMAX=1.0e5 "Maximal init speed";
  constant Units.SI.Velocity VELNOM=1.0 "Nominal init speed";
  constant Units.SI.Volume VMIN=0.0 "Minimal init volume";
  constant Units.SI.Volume VMAX=1.0e5 "Maximal init volume";
  constant Units.SI.Volume VNOM=1.0e-3 "Nominal init volume";


































  annotation (Documentation(info="<html>
<p><b>Package description</b> </p>
<p>This package provides records and functions shared by many of the property sub-packages. High accuracy fluid property models share a lot of common structure, even if the actual models are different. Common data structures and computations shared by these property models are collected in this library.</p>
<p>This package is copied from package Modelica.Media.Common in Modelica package version 3.2.2.</p>
</html>", revisions="<html>
      <ul>
      <li>First implemented: <i>July, 2000</i>
      by Hubertus Tummescheit
      for the ThermoFluid Library with help from Jonas Eborn and Falko Jens Wagner
      </li>
      <li>Code reorganization, enhanced documentation, additional functions: <i>December, 2002</i>
      by Hubertus Tummescheit and move to Modelica
                            properties library.</li>
      <li>Inclusion into Modelica.Media: September 2003 </li>
      </ul>

      <address>Author: Hubertus Tummescheit, <br>
      Lund University<br>
      Department of Automatic Control<br>
      Box 118, 22100 Lund, Sweden<br>
      email: hubertus@control.lth.se
      </address>
</html>"), Icon(graphics={
        Rectangle(
          lineColor={200,200,200},
          fillColor={248,248,248},
          fillPattern=FillPattern.HorizontalCylinder,
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Rectangle(
          lineColor={128,128,128},
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Rectangle(
          lineColor={200,200,200},
          fillColor={248,248,248},
          fillPattern=FillPattern.HorizontalCylinder,
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Rectangle(
          lineColor={128,128,128},
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Ellipse(
          lineColor={102,102,102},
          fillColor={204,204,204},
          pattern=LinePattern.None,
          fillPattern=FillPattern.Sphere,
          extent={{-60.0,-60.0},{60.0,60.0}})}));
end Common;
