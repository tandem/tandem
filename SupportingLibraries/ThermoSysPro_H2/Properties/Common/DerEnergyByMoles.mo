within ThermoSysPro_H2.Properties.Common;
type DerEnergyByMoles = Real (final quantity="DerEnergyByMoles", final unit=
        "J/mol");
