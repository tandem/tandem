within ThermoSysPro_H2.Properties.Common;
record SaturationBoundaryProperties
  "Properties on both phase boundaries, including some derivatives"

  extends Modelica.Icons.Record;
  Units.SI.Temp_K T "Saturation temperature";
  Units.SI.Density dl "Liquid density";
  Units.SI.Density dv "Vapour density";
  Units.SI.SpecificEnthalpy hl "Liquid specific enthalpy";
  Units.SI.SpecificEnthalpy hv "Vapour specific enthalpy";
  Real dTp "Derivative of temperature w.r.t. saturation pressure";
  Real ddldp "Derivative of density along boiling curve";
  Real ddvdp "Derivative of density along dew curve";
  Real dhldp "Derivative of specific enthalpy along boiling curve";
  Real dhvdp "Derivative of specific enthalpy along dew curve";
  Units.SI.MassFraction x "Vapour mass fraction";
end SaturationBoundaryProperties;
