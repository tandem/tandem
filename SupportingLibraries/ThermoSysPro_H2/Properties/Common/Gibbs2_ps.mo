within ThermoSysPro_H2.Properties.Common;
function Gibbs2_ps
  "Function to calculate analytic derivatives for computing d and t given p and s"

  extends Modelica.Icons.Function;
  input ThermoSysPro_H2.Properties.Common.GibbsDerivs2 g
    "Dimensionless derivatives of Gibbs function";
  output ThermoSysPro_H2.Properties.Common.NewtonDerivatives_ps nderivs
    "Derivatives for Newton iteration to compute T from p and s";

algorithm
  nderivs.s := -g.gT;
  nderivs.st := -g.gTT;

  //dummy values - DO NOT USE
  nderivs.p := 0.0;
  nderivs.pd := 0.0;
  nderivs.pt := 0.0;
  nderivs.sd := 0.0;
end Gibbs2_ps;
