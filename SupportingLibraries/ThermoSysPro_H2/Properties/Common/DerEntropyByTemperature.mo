within ThermoSysPro_H2.Properties.Common;
type DerEntropyByTemperature = Real (final quantity="DerEntropyByTemperature",
      final unit="J/K2");
