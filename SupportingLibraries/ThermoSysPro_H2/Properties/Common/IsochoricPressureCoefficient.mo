within ThermoSysPro_H2.Properties.Common;
type IsochoricPressureCoefficient = Real (final quantity=
        "IsochoricPressureCoefficient", unit="1/K");
