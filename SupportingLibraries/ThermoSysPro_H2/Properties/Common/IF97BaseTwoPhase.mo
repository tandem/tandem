within ThermoSysPro_H2.Properties.Common;
record IF97BaseTwoPhase "Intermediate property data record for IF 97"
  extends Modelica.Icons.Record;
  Integer phase(start=0)
    "Phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  Integer region(min=1, max=5) "IF 97 region";
  Units.SI.Pressure p "Pressure";
  Units.SI.Temperature T "Temperature";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificHeatCapacity R "Gas constant";
  Units.SI.SpecificHeatCapacity cp "Specific heat capacity";
  Units.SI.SpecificHeatCapacity cv "Specific heat capacity";
  Units.SI.Density rho "Density";
  Units.SI.SpecificEntropy s "Specific entropy";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  Real vt "Derivative of specific volume w.r.t. temperature";
  Real vp "Derivative of specific volume w.r.t. pressure";
  Real x "Dryness fraction";
  Real dpT "dp/dT derivative of saturation curve";
end IF97BaseTwoPhase;
