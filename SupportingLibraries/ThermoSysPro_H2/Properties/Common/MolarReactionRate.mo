within ThermoSysPro_H2.Properties.Common;
type MolarReactionRate = Real (final quantity="MolarReactionRate", final unit=
       "mol/(m3.s)");
