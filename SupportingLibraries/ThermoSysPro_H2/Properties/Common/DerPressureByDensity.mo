within ThermoSysPro_H2.Properties.Common;
type DerPressureByDensity = Real (final quantity="DerPressureByDensity",
      final unit="Pa.m3/kg");
