within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record TransportProps "Record with transport properties"
  extends Modelica.Icons.Record;
  Units.SI.DynamicViscosity eta;
  Units.SI.ThermalConductivity lam;
end TransportProps;
