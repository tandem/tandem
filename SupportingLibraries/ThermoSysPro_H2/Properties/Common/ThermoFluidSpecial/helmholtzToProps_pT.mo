within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function helmholtzToProps_pT
  "Calculate property record for pressure and temperature as states from dimensionless Helmholtz function"

  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output ThermoProperties_pT pro
    "Property record for pressure and temperature as dynamic states";
protected
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureBySpecificVolume pv
    "Derivative of pressure w.r.t. specific volume";
  IsobaricVolumeExpansionCoefficient alpha
    "Isobaric volume expansion coefficient";
  // beta in Bejan
  IsothermalCompressibility gamma "Isothermal compressibility";
  // kappa in Bejan
  Units.SI.Pressure p "Pressure";
algorithm
  pro.d := f.d;
  pro.R := f.R;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);
  pro.u := f.R*f.T*f.tau*f.ftau;
  pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  pv := -(f.d*f.d)*pd;
  alpha := -f.d*pt/pv;
  gamma := -f.d/pv;
  p := f.R*f.d*f.T*f.delta*f.fdelta;
  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pro.cv := f.R*(-f.tau*f.tau*f.ftautau);
  pro.kappa := 1/(f.d*f.R*f.d*f.T*f.delta*f.fdelta)*((-pv*pro.cv + pt*pt*f.T)
    /(pro.cv));
  pro.a := abs(f.R*f.T*(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta
     - ((f.delta*f.fdelta - f.delta*f.tau*f.fdeltatau)*(f.delta*f.fdelta -
    f.delta*f.tau*f.fdeltatau))/(f.tau*f.tau*f.ftautau)))^0.5;
  pro.ddTp := -pt/pd;
  pro.ddpT := 1/pd;
  //problem with units in last two lines
  pro.dupT := gamma*p/f.d - alpha*f.T/f.d;
  pro.duTp := pro.cp - alpha*p/f.d;
end helmholtzToProps_pT;
