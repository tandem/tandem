within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record ThermoBaseVars
  extends Modelica.Icons.Record;
  parameter Integer n(min=1) "Discretization number";
  parameter Integer nspecies(min=1) "Number of species";
  Units.SI.Pressure[n] p(
    min=PMIN,
    max=PMAX,
    nominal=PNOM,
    start=fill(1.0e5, n)) "Pressure";
  Units.SI.Temperature[n] T(
    min=TMIN,
    max=TMAX,
    nominal=TNOM) "Temperature";
  Units.SI.Density[n] d(
    min=DMIN,
    max=DMAX,
    nominal=DNOM) "Density";
  Units.SI.SpecificEnthalpy[n] h(
    min=SHMIN,
    max=SHMAX,
    nominal=SHNOM) "Specific enthalpy";
  Units.SI.SpecificEntropy[n] s(
    min=SSMIN,
    max=SSMAX,
    nominal=SSNOM) "Specific entropy";
  Units.SI.RatioOfSpecificHeatCapacities[n] kappa "Ratio of cp/cv";
  Units.SI.Mass[n] M(
    min=MMIN,
    max=MMAX,
    nominal=MNOM) "Total mass";
  Units.SI.Energy[n] U(
    min=EMIN,
    max=EMAX,
    nominal=ENOM) "Inner energy";
  Units.SI.MassFlowRate[n] dM(
    min=MDOTMIN,
    max=MDOTMAX,
    nominal=MDOTNOM) "Change in total mass";
  Units.SI.Power[n] dU(
    min=POWMIN,
    max=POWMAX,
    nominal=POWNOM) "Change in inner energy";
  Units.SI.Volume[n] V(
    min=VMIN,
    max=VMAX,
    nominal=VNOM) "Volume";
  Units.SI.MassFraction[n,nspecies] mass_x(
    min=MASSXMIN,
    max=MASSXMAX,
    nominal=MASSXNOM) "Mass fraction";
  Units.SI.MoleFraction[n,nspecies] mole_y(
    min=MOLEYMIN,
    max=MOLEYMAX,
    nominal=MOLEYNOM) "Mole fraction";
  Units.SI.Mass[n,nspecies] M_x(
    min=MMIN,
    max=MMAX,
    nominal=MNOM) "Component mass";
  Units.SI.MassFlowRate[n,nspecies] dM_x(
    min=MDOTMIN,
    max=MDOTMAX,
    nominal=MDOTNOM) "Rate of change in component mass";
  MolarFlowRate[n, nspecies] dZ(
    min=-1.0e6,
    max=1.0e6,
    nominal=0.0) "Rate of change in component moles";
  MolarFlowRate[n, nspecies] rZ(
    min=-1.0e6,
    max=1.0e6,
    nominal=0.0) "Reaction(source) mole rates";
  Units.SI.MolarMass[n] MM(
    min=MMMIN,
    max=MMMAX,
    nominal=MMNOM) "Molar mass of mixture";
  Units.SI.AmountOfSubstance[n] Moles(
    min=MOLMIN,
    max=MOLMAX,
    nominal=MOLNOM) "Total moles";
  Units.SI.AmountOfSubstance[n,nspecies] Moles_z(
    min=MOLMIN,
    max=MOLMAX,
    nominal=MOLNOM) "Mole vector";
  annotation (Documentation(info="<html>
                         <h4>Model description</h4>
                              <p>
                              <b>ThermoBaseVars</b> is inherited by all medium property models
                              and by all models defining the dynamic states for the conservation
                              of mass and energy. Thus it is a good choice as a restricting class
                              for any medium model or dynamic state model.
                           </p>
                              </html>"));
end ThermoBaseVars;
