within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function gibbsToProps_dT
  "Calculate property record for density and temperature as states from dimensionless Gibbs function"

  extends Modelica.Icons.Function;
  input GibbsDerivs g "Dimensionless derivatives of Gibbs function";
  output ThermoProperties_dT pro
    "Property record for density and temperature as dynamic states";
protected
  Real vt(unit="m3.kg-1.K-1")
    "Derivative of specific volume w.r.t. temperature";
  Real vp(unit="m4.kg-2.s2")
    "Derivative of specific volume w.r.t. pressure";
  Units.SI.Density d;
algorithm
  pro.R := g.R;
  pro.p := g.p;
  pro.u := g.T*g.R*(g.tau*g.gtau - g.pi*g.gpi);
  pro.h := g.R*g.T*g.tau*g.gtau;
  pro.s := pro.R*(g.tau*g.gtau - g.g);
  pro.cp := -pro.R*g.tau*g.tau*g.gtautau;
  pro.cv := pro.R*(-g.tau*g.tau*g.gtautau + (g.gpi - g.tau*g.gtaupi)*(g.gpi
     - g.tau*g.gtaupi)/g.gpipi);
  vt := g.R/g.p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(g.p*g.p)*g.pi*g.pi*g.gpipi;
  pro.kappa := -1/((g.p/(pro.R*g.T*g.pi*g.gpi))*g.p)*pro.cp/(vp*pro.cp + vt
    *vt*g.T);
  pro.a := abs(g.R*g.T*(g.gpi*g.gpi/((g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau
    *g.gtaupi)/(g.tau*g.tau*g.gtautau) - g.gpipi)))^0.5;

  d := g.p/(pro.R*g.T*g.pi*g.gpi);
  pro.dudT := (pro.p - g.T*vt/vp)/(d*d);
end gibbsToProps_dT;
