within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record FixedIGProperties "Constant properties for ideal gases"
  extends Modelica.Icons.Record;
  parameter Integer nspecies(min=1) "Number of components";
  Units.SI.MolarMass[nspecies] MM "Molar mass of components";
  Real[nspecies] invMM "Inverse of molar mass of components";
  Units.SI.SpecificHeatCapacity[nspecies] R "Gas constant";
  Units.SI.SpecificEnthalpy[nspecies] Hf
    "Enthalpy of formation at 298.15K";
  Units.SI.SpecificEnthalpy[nspecies] H0 "H0(298.15K) - H0(0K)";
end FixedIGProperties;
