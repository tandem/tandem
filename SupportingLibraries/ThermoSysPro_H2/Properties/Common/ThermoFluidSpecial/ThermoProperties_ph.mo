within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record ThermoProperties_ph
  "Thermodynamic property data for pressure p and specific enthalpy h as dynamic states"

  extends Modelica.Icons.Record;
  Units.SI.Temperature T(
    min=1.0e-9,
    max=10000.0,
    nominal=298.15) "Temperature";
  Units.SI.Density d(
    min=1.0e-9,
    max=10000.0,
    nominal=10.0) "Density";
  Units.SI.SpecificEnergy u(
    min=-1.0e8,
    max=1.0e8,
    nominal=1.0e6) "Specific inner energy";
  Units.SI.SpecificEntropy s(
    min=-1.0e6,
    max=1.0e6,
    nominal=1.0e3) "Specific entropy";
  Units.SI.SpecificHeatCapacity cp(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant volume";
  Units.SI.SpecificHeatCapacity R(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Gas constant";
  Units.SI.RatioOfSpecificHeatCapacities kappa "Ratio of cp/cv";
  Units.SI.Velocity a(
    min=1.0,
    max=10000.0,
    nominal=300.0) "Speed of sound";
  Units.SI.DerDensityByEnthalpy ddhp
    "Derivative of density by enthalpy at constant pressure";
  Units.SI.DerDensityByPressure ddph
    "Derivative of density by pressure at constant enthalpy";
  Real duph(unit="m3/kg")
    "Derivative of inner energy by pressure at constant enthalpy";
  Real duhp(unit="1")
    "Derivative of inner energy by enthalpy at constant pressure";
  annotation (Documentation(info="<html>
<h4>Model description</h4>
<p>
A base class for medium property models which
use pressure and enthalpy as dynamic states.
This is the preferred model for fluids that can also be in the
two phase and liquid regions.
</p>
</html>"));
end ThermoProperties_ph;
