within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record ThermoProperties
  "Thermodynamic base property data for all state models"
  extends Modelica.Icons.Record;
  parameter Integer nspecies(min=1) "Number of species";
  Units.SI.Temperature T(
    min=TMIN,
    max=TMAX,
    nominal=TNOM) "Temperature";
  Units.SI.Density d(
    min=DMIN,
    max=DMAX,
    nominal=DNOM) "Density";
  Units.SI.Pressure p(
    min=PMIN,
    max=PMAX,
    nominal=PNOM) "Pressure";
  Units.SI.Volume V(
    min=VMIN,
    max=VMAX,
    nominal=VNOM) "Volume";
  Units.SI.SpecificEnthalpy h(
    min=SHMIN,
    max=SHMAX,
    nominal=SHNOM) "Specific enthalpy";
  Units.SI.SpecificEnergy u(
    min=SEMIN,
    max=SEMAX,
    nominal=SENOM) "Specific inner energy";
  Units.SI.SpecificEntropy s(
    min=SSMIN,
    max=SSMAX,
    nominal=SSNOM) "Specific entropy";
  Units.SI.SpecificGibbsFreeEnergy g(
    min=SHMIN,
    max=SHMAX,
    nominal=SHNOM) "Specific Gibbs free energy";
  Units.SI.SpecificHeatCapacity cp(
    min=CPMIN,
    max=CPMAX,
    nominal=CPNOM) "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv(
    min=CPMIN,
    max=CPMAX,
    nominal=CPNOM) "Heat capacity at constant volume";
  Units.SI.SpecificHeatCapacity R(
    min=CPMIN,
    max=CPMAX,
    nominal=CPNOM) "Gas constant";
  Units.SI.MolarMass MM(
    min=MMMIN,
    max=MMMAX,
    nominal=MMNOM) "Molar mass of mixture";
  Units.SI.MassFraction[nspecies] mass_x(
    min=MASSXMIN,
    max=MASSXMAX,
    nominal=MASSXNOM) "Mass fraction";
  Units.SI.MoleFraction[nspecies] mole_y(
    min=MOLEYMIN,
    max=MOLEYMAX,
    nominal=MOLEYNOM) "Mole fraction";
  Units.SI.RatioOfSpecificHeatCapacities kappa "Ratio of cp/cv";
  Units.SI.DerDensityByTemperature ddTp
    "Derivative of density by temperature at constant pressure";
  Units.SI.DerDensityByPressure ddpT
    "Derivative of density by pressure at constant temperature";
  Real dupT(unit="m3.kg-1")
    "Derivative of inner energy by pressure at constant T";
  Real dudT(unit="(J.m3)/(kg2)")
    "Derivative of inner energy by density at constant T";
  Units.SI.SpecificHeatCapacity duTp
    "Derivative of inner energy by temperature at constant p";
  Units.SI.SpecificEnergy ddx[nspecies]
    "Derivative vector of density by change in mass composition";
  Units.SI.SpecificEnergy[nspecies] compu(
    min=SEMIN,
    max=SEMAX,
    nominal=SENOM) "Inner energy of the components";
  Units.SI.Pressure[nspecies] compp(
    min=COMPPMIN,
    max=COMPPMAX,
    nominal=COMPPNOM) "partial pressures of the components";
  Units.SI.Velocity a(
    min=VELMIN,
    max=VELMAX,
    nominal=VELNOM) "Speed of sound";
  Units.SI.HeatCapacity dUTZ
    "Derivative of inner energy by temperature at constant moles";
  Units.SI.MolarInternalEnergy[nspecies] dUZT
    "Derivative of inner energy by moles at constant temperature";
  Units.SI.SpecificEnthalpy[nspecies] dHMxT(
    min=SEMIN,
    max=SEMAX,
    nominal=SENOM)
    "Derivative of total enthalpy w.r.t. component mass at constant T";
  Real dpT "Derivative of pressure w.r.t. temperature";
  Real dpZ[nspecies] "Derivative of pressure w.r.t. moles";
  annotation (Documentation(info="<html>
        <h4>Model description</h4>
        <p>
        A base class for medium property models which work with most of the
        versions of dynamic states that are available in the ThermoFluid
        library. Currently used by all ideal gas models.
     </p>
        </html>"));
end ThermoProperties;
