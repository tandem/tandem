within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record ThermoProperties_pT
  "Thermodynamic property data for pressure p and temperature T as dynamic states"

  extends Modelica.Icons.Record;
  Units.SI.Density d(
    min=1.0e-9,
    max=10000.0,
    nominal=10.0) "Density";
  Units.SI.SpecificEnthalpy h(
    min=-1.0e8,
    max=1.0e8,
    nominal=1.0e6) "Specific enthalpy";
  Units.SI.SpecificEnergy u(
    min=-1.0e8,
    max=1.0e8,
    nominal=1.0e6) "Specific inner energy";
  Units.SI.SpecificEntropy s(
    min=-1.0e6,
    max=1.0e6,
    nominal=1.0e3) "Specific entropy";
  Units.SI.SpecificHeatCapacity cp(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant volume";
  Units.SI.SpecificHeatCapacity R(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Gas constant";
  Units.SI.RatioOfSpecificHeatCapacities kappa "Ratio of cp/cv";
  Units.SI.Velocity a(
    min=1.0,
    max=10000.0,
    nominal=300.0) "Speed of sound";
  Units.SI.DerDensityByTemperature ddTp
    "Derivative of density by temperature at constant pressure";
  Units.SI.DerDensityByPressure ddpT
    "Derivative of density by pressure at constant temperature";
  Real dupT(unit="m3.kg-1")
    "Derivative of inner energy by pressure at constant T";
  Units.SI.SpecificHeatCapacity duTp
    "Derivative of inner energy by temperature at constant p";
  annotation (Documentation(info="<html>
<h4>Model description</h4>
<p>
A base class for medium property models which use pressure and temperature as dynamic states.
This is a reasonable model for fluids that can also be in the gas and
liquid regions, but never in the two-phase region.
</p>
</html>"));
end ThermoProperties_pT;
