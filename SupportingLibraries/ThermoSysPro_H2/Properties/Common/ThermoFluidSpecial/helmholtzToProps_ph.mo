within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function helmholtzToProps_ph
  "Calculate property record for pressure and specific enthalpy as states from dimensionless Helmholtz function"

  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output ThermoProperties_ph pro
    "Property record for pressure and specific enthalpy as dynamic states";
protected
  Units.SI.Pressure p "Pressure";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureBySpecificVolume pv
    "Derivative of pressure w.r.t. specific volume";
algorithm
  pro.d := f.d;
  pro.T := f.T;
  pro.R := f.R;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.u := f.R*f.T*f.tau*f.ftau;
  p := pro.d*pro.R*pro.T*f.delta*f.fdelta;
  pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  pv := -pd*f.d*f.d;

  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pro.cv := f.R*(-f.tau*f.tau*f.ftautau);
  pro.kappa := 1/(f.d*f.R*f.d*f.T*f.delta*f.fdelta)*((-pv*pro.cv + pt*pt*f.T)
    /(pro.cv));
  pro.a := abs(f.R*f.T*(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta
     - ((f.delta*f.fdelta - f.delta*f.tau*f.fdeltatau)*(f.delta*f.fdelta -
    f.delta*f.tau*f.fdeltatau))/(f.tau*f.tau*f.ftautau)))^0.5;
  pro.ddph := (f.d*(pro.cv*f.d + pt))/(f.d*f.d*pd*pro.cv + f.T*pt*pt);
  pro.ddhp := -f.d*f.d*pt/(f.d*f.d*pd*pro.cv + f.T*pt*pt);
  pro.duph := -1/pro.d + p/(pro.d*pro.d)*pro.ddph;
  pro.duhp := 1 + p/(pro.d*pro.d)*pro.ddhp;
end helmholtzToProps_ph;
