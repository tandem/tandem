within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function helmholtzToProps_dT
  "Calculate property record for density and temperature as states from dimensionless Helmholtz function"

  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output ThermoProperties_dT pro
    "Property record for density and temperature as dynamic states";
protected
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureBySpecificVolume pv "Derivative of pressure w.r.t. pressure";
algorithm
  pro.p := f.R*f.d*f.T*f.delta*f.fdelta;
  pro.R := f.R;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);
  pro.u := f.R*f.T*f.tau*f.ftau;
  pv := -(f.d*f.d)*f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);

  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pro.cv := f.R*(-f.tau*f.tau*f.ftautau);
  pro.kappa := 1/(f.d*pro.p)*((-pv*pro.cv + pt*pt*f.T)/(pro.cv));
  pro.a := abs(f.R*f.T*(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta
     - ((f.delta*f.fdelta - f.delta*f.tau*f.fdeltatau)*(f.delta*f.fdelta -
    f.delta*f.tau*f.fdeltatau))/(f.tau*f.tau*f.ftautau)))^0.5;
  pro.dudT := (pro.p - f.T*pt)/(f.d*f.d);
end helmholtzToProps_dT;
