within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function TwoPhaseToProps_dT
  "Compute property record for density and temperature as states from saturation properties"

  extends Modelica.Icons.Function;
  input SaturationProperties sat "Saturation properties";
  output ThermoProperties_dT pro
    "Property record for density and temperature as dynamic states";
algorithm
  pro.p := sat.p;
  pro.h := sat.h;
  pro.u := sat.u;
  pro.s := sat.s;
  pro.cv := sat.cv;
  pro.cp := Modelica.Constants.inf;
  pro.R := sat.R;
  pro.kappa := -1/(sat.d*sat.p)*sat.dpT*sat.dpT*sat.T/sat.cv;
  pro.a := Modelica.Constants.inf;
  pro.dudT := (sat.p - sat.T*sat.dpT)/(sat.d*sat.d);
end TwoPhaseToProps_dT;
