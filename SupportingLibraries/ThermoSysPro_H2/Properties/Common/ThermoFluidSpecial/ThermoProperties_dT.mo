within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
record ThermoProperties_dT
  "Thermodynamic property data for density d and temperature T as dynamic states"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p(
    min=1.0,
    max=1.0e9,
    nominal=1.0e5) "Pressure";
  Units.SI.SpecificEnthalpy h(
    min=-1.0e8,
    max=1.0e8,
    nominal=1.0e6) "Specific enthalpy";
  Units.SI.SpecificEnergy u(
    min=-1.0e8,
    max=1.0e8,
    nominal=1.0e6) "Specific inner energy";
  Units.SI.SpecificEntropy s(
    min=-1.0e6,
    max=1.0e6,
    nominal=1.0e3) "Specific entropy";
  Units.SI.SpecificHeatCapacity cp(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Heat capacity at constant volume";
  Units.SI.SpecificHeatCapacity R(
    min=1.0,
    max=1.0e6,
    nominal=1000.0) "Gas constant";
  Units.SI.RatioOfSpecificHeatCapacities kappa "Ratio of cp/cv";
  Units.SI.Velocity a(
    min=1.0,
    max=10000.0,
    nominal=300.0) "Speed of sound";
  Real dudT(unit="m5/(kg.s2)")
    "Derivative of inner energy by density at constant T";
  annotation (Documentation(info="<html>
<h4>Model description</h4>
<p>
A base class for medium property models which use density and temperature as dynamic states.
This is a reasonable model for fluids that can be in the gas, liquid
and two-phase region. The model is numerically not well suited for
liquids except if the pressure is always above approx. 80% of the
critical pressure.
</p>
</html>"));
end ThermoProperties_dT;
