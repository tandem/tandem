within ThermoSysPro_H2.Properties.Common;
package ThermoFluidSpecial "Property records used by the ThermoFluid library"

  //   record GibbsDerivs

  //     "Derivatives of dimensionless Gibbs-function w.r.t. dimensionless pressure and temperature"
  //     extends Modelica.Icons.Record;
  //     Real pi "Dimensionless pressure";
  //     Real tau "Dimensionless temperature";
  //     Real g "Dimensionless Gibbs-function";
  //     Real gpi "Derivative of g w.r.t. pi";
  //     Real gpipi "2nd derivative of g w.r.t. pi";
  //     Real gtau "Derivative of g w.r.t. tau";
  //     Real gtautau "2nd derivative of g w.r.t. tau";
  //     Real gtaupi "Mixed derivative of g w.r.t. pi and tau";
  //   end GibbsDerivs;

  //   record HelmholtzDerivs

  //     "Derivatives of dimensionless Helmholtz-function w.r.t. dimensionless pressure, density and temperature"
  //     extends Modelica.Icons.Record;
  //     Real delta "Dimensionless density";
  //     Real tau "Dimensionless temperature";
  //     Real f "Dimensionless Helmholtz-function";
  //     Real fdelta "Derivative of f w.r.t. delta";
  //     Real fdeltadelta "2nd derivative of f w.r.t. delta";
  //     Real ftau "Derivative of f w.r.t. tau";
  //     Real ftautau "2nd derivative of f w.r.t. tau";
  //     Real fdeltatau "Mixed derivative of f w.r.t. delta and tau";
  //   end HelmholtzDerivs;
















end ThermoFluidSpecial;
