within ThermoSysPro_H2.Properties.Common.ThermoFluidSpecial;
function TwoPhaseToProps_ph
  "Compute property record for pressure and specific enthalpy as states from saturation properties"

  extends Modelica.Icons.Function;
  input SaturationProperties sat "Saturation property record";
  output ThermoProperties_ph pro
    "Property record for pressure and specific enthalpy as dynamic states";
protected
  Real dht(unit="(J/kg)/K")
    "Derivative of specific enthalpy w.r.t. temperature";
  Real dhd(unit="(J/kg)/(kg/m3)")
    "Derivative of specific enthalpy w.r.t. density";
  Real detph(unit="m4.s4/(K.s8)") "Thermodynamic determinant";
algorithm
  pro.d := sat.d;
  pro.T := sat.T;
  pro.u := sat.u;
  pro.s := sat.s;
  pro.cv := sat.cv;
  pro.R := sat.R;
  pro.cp := Modelica.Constants.inf;
  pro.kappa := -1/(sat.d*sat.p)*sat.dpT*sat.dpT*sat.T/sat.cv;
  pro.a := Modelica.Constants.inf;
  dht := sat.cv + sat.dpT/sat.d;
  dhd := -sat.T*sat.dpT/(sat.d*sat.d);
  detph := -sat.dpT*dhd;
  pro.ddph := dht/detph;
  pro.ddhp := -sat.dpT/detph;
end TwoPhaseToProps_ph;
