within ThermoSysPro_H2.Properties.Common;
type IsothermalCompressibility = Real (final quantity=
        "IsothermalCompressibility", unit="1/Pa");
