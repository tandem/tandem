within ThermoSysPro_H2.Properties.Common;
type DerEnergyByPressure = Real (final quantity="DerEnergyByPressure", final
      unit="J/Pa");
