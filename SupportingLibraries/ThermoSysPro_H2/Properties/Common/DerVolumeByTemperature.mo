within ThermoSysPro_H2.Properties.Common;
type DerVolumeByTemperature = Real (final quantity="DerVolumeByTemperature",
      final unit="m3/K");
