within ThermoSysPro_H2.Properties.Common;
record TwoPhaseTransportProps
  "Defines properties on both phase boundaries, needed in the two phase region"
  extends Modelica.Icons.Record;
  Units.SI.Density d_vap "Density on the dew line";
  Units.SI.Density d_liq "Density on the bubble line";
  Units.SI.DynamicViscosity eta_vap
    "Dynamic viscosity on the dew line";
  Units.SI.DynamicViscosity eta_liq
    "Dynamic viscosity on the bubble line";
  Units.SI.ThermalConductivity lam_vap
    "Thermal conductivity on the dew line";
  Units.SI.ThermalConductivity lam_liq
    "Thermal conductivity on the bubble line";
  Units.SI.SpecificHeatCapacity cp_vap "Cp on the dew line";
  Units.SI.SpecificHeatCapacity cp_liq "Cp on the bubble line";
  Units.SI.MassFraction x "Steam quality";
end TwoPhaseTransportProps;
