within ThermoSysPro_H2.Properties.Common;
record FundamentalConstants "Constants of the medium"
  extends Modelica.Icons.Record;
  Units.SI.MolarHeatCapacity R_bar;
  Units.SI.SpecificHeatCapacity R;
  Units.SI.MolarMass MM;
  Units.SI.MolarDensity rhored;
  Units.SI.Temperature Tred;
  Units.SI.AbsolutePressure pred;
  Units.SI.SpecificEnthalpy h_off;
  Units.SI.SpecificEntropy s_off;
end FundamentalConstants;
