within ThermoSysPro_H2.Properties.Common;
function Helmholtz_ps
  "Function to calculate analytic derivatives for computing d and t given p and s"

  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output NewtonDerivatives_ps nderivs
    "Derivatives for Newton iteration to compute d and t from p and s";
protected
  Units.SI.SpecificHeatCapacity cv "Isochoric heat capacity";
algorithm
  cv := -f.R*(f.tau*f.tau*f.ftautau);
  nderivs.p := f.d*f.R*f.T*f.delta*f.fdelta;
  nderivs.s := f.R*(f.tau*f.ftau - f.f);
  nderivs.pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  nderivs.pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  nderivs.st := cv/f.T;
  nderivs.sd := -nderivs.pt/(f.d*f.d);
end Helmholtz_ps;
