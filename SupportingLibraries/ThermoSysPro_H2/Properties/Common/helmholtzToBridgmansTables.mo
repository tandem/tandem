within ThermoSysPro_H2.Properties.Common;
function helmholtzToBridgmansTables
  "Calculates base coefficients for Bridgmans tables from Helmholtz energy"
  extends Modelica.Icons.Function;
  input HelmholtzDerivs f "Dimensionless derivatives of Helmholtz function";
  output Units.SI.SpecificVolume v=1/f.d "Specific volume";
  output Units.SI.Pressure p "Pressure";
  output Units.SI.Temperature T=f.T "Temperature";
  output Units.SI.SpecificEntropy s "Specific entropy";
  output Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  output IsobaricVolumeExpansionCoefficient alpha
    "Isobaric volume expansion coefficient";
  // beta in Bejan
  output IsothermalCompressibility gamma "Isothermal compressibility";
  // kappa in Bejan
protected
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  DerPressureBySpecificVolume pv
    "Derivative of pressure w.r.t. specific volume";
  Units.SI.SpecificHeatCapacity cv "Isochoric specific heat capacity";
algorithm
  p := f.R*f.d*f.T*f.delta*f.fdelta;
  pv := -(f.d*f.d)*f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  s := f.R*(f.tau*f.ftau - f.f);
  alpha := -f.d*pt/pv;
  gamma := -f.d/pv;
  cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau*f.fdeltatau)
    ^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
end helmholtzToBridgmansTables;
