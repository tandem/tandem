within ThermoSysPro_H2.Properties.Common;
type DerVolumeByMoles = Real (final quantity="DerVolumeByMoles", final unit=
        "m3/mol");
