within ThermoSysPro_H2.Properties.Common;
function gibbsToExtraDerivs
  "Compute additional thermodynamic derivatives from dimensionless Gibbs function"

  extends Modelica.Icons.Function;
  input GibbsDerivs g "Dimensionless derivatives of Gibbs function";
  output ExtraDerivatives dpro "Additional property derivatives";
protected
  Real vt "Derivative of specific volume w.r.t. temperature";
  Real vp "Derivative of specific volume w.r.t. pressure";
  Units.SI.Density d "Density";
  Units.SI.SpecificVolume v "Specific volume";
  Units.SI.SpecificHeatCapacity cv "Isochoric heat capacity";
  Units.SI.SpecificHeatCapacity cp "Isobaric heat capacity";
algorithm
  d := g.p/(g.R*g.T*g.pi*g.gpi);
  v := 1/d;
  vt := g.R/g.p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(g.p*g.p)*g.pi*g.pi*g.gpipi;
  cp := -g.R*g.tau*g.tau*g.gtautau;
  cv := g.R*(-g.tau*g.tau*g.gtautau + (g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau
    *g.gtaupi)/g.gpipi);
  dpro.kappa := -1/(d*g.p)*cp/(vp*cp + vt*vt*g.T);
  dpro.theta := cp/(d*g.p*(-vp*cp + vt*v - g.T*vt*vt));
  dpro.alpha := d*vt;
  dpro.beta := -vt/(g.p*vp);
  dpro.gamma := -d*vp;
  dpro.mu := -(v - g.T*vt)/cp;
end gibbsToExtraDerivs;
