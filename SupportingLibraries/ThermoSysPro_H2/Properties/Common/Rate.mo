within ThermoSysPro_H2.Properties.Common;
type Rate = Real (final quantity="Rate", final unit="s-1");
