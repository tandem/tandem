within ThermoSysPro_H2.Properties.Common;
record SaturationProperties "Properties in the two phase region"
  extends Modelica.Icons.Record;
  Units.SI.Temp_K T "Temperature";
  Units.SI.Density d "Density";
  Units.SI.Pressure p "Pressure";
  Units.SI.SpecificEnergy u "Specific inner energy";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.SpecificHeatCapacity cp
    "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "Heat capacity at constant volume";
  Units.SI.SpecificHeatCapacity R "Gas constant";
  Units.SI.RatioOfSpecificHeatCapacities kappa
    "Isentropic expansion coefficient";
  PhaseBoundaryProperties liq
    "Thermodynamic base properties on the boiling curve";
  PhaseBoundaryProperties vap
    "Thermodynamic base properties on the dew curve";
  Real dpT(unit="Pa/K")
    "Derivative of saturation pressure w.r.t. temperature";
  Units.SI.MassFraction x "Vapour mass fraction";
end SaturationProperties;
