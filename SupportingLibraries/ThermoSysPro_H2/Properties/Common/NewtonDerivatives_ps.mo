within ThermoSysPro_H2.Properties.Common;
record NewtonDerivatives_ps
  "Derivatives for fast inverse calculation of Helmholtz functions: p & s"

  extends Modelica.Icons.Record;
  Units.SI.Pressure p "Pressure";
  Units.SI.SpecificEntropy s "Specific entropy";
  DerPressureByDensity pd "Derivative of pressure w.r.t. density";
  DerPressureByTemperature pt "Derivative of pressure w.r.t. temperature";
  Real sd "Derivative of specific entropy w.r.t. density";
  Real st "Derivative of specific entropy w.r.t. temperature";
end NewtonDerivatives_ps;
