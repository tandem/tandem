within ThermoSysPro_H2.Properties.Common;
type DerVolumeByPressure = Real (final quantity="DerVolumeByPressure", final
      unit="m3/Pa");
