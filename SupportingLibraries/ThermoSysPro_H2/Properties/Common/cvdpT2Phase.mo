within ThermoSysPro_H2.Properties.Common;
function cvdpT2Phase
  "Compute isochoric specific heat capacity inside the two-phase region and derivative of pressure w.r.t. temperature"

  extends Modelica.Icons.Function;
  input PhaseBoundaryProperties liq "Properties on the boiling curve";
  input PhaseBoundaryProperties vap "Properties on the condensation curve";
  input Units.SI.MassFraction x "Vapour mass fraction";
  input Units.SI.Temperature T "Temperature";
  input Units.SI.Pressure p "Properties";
  output Units.SI.SpecificHeatCapacity cv
    "Isochoric specific heat capacity";
  output Real dpT "Derivative of pressure w.r.t. temperature";
protected
  Real dxv "Derivative of vapour mass fraction w.r.t. specific volume";
  Real dvTl "Derivative of liquid specific volume w.r.t. temperature";
  Real dvTv "Derivative of vapour specific volume w.r.t. temperature";
  Real duTl "Derivative of liquid specific inner energy w.r.t. temperature";
  Real duTv "Derivative of vapour specific inner energy w.r.t. temperature";
  Real dxt "Derivative of vapour mass fraction w.r.t. temperature";
algorithm
  dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d - vap.d) else 0.0;
  dpT := (vap.s - liq.s)*dxv;
  // wrong at critical point
  dvTl := (liq.pt - dpT)/liq.pd/liq.d/liq.d;
  dvTv := (vap.pt - dpT)/vap.pd/vap.d/vap.d;
  dxt := -dxv*(dvTl + x*(dvTv - dvTl));
  duTl := liq.cv + (T*liq.pt - p)*dvTl;
  duTv := vap.cv + (T*vap.pt - p)*dvTv;
  cv := duTl + x*(duTv - duTl) + dxt*(vap.u - liq.u);
end cvdpT2Phase;
