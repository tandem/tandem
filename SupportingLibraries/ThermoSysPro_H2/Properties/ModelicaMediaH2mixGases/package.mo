within ThermoSysPro_H2.Properties;
package ModelicaMediaH2mixGases "Flue gases library inherited from Modelica.Media"
  extends Modelica.Media.IdealGases.Common.MixtureGasNasa(
    mediumName="MediaMonomeld",
    data={Modelica.Media.IdealGases.Common.SingleGasesData.N2,Modelica.Media.IdealGases.Common.SingleGasesData.O2,
          Modelica.Media.IdealGases.Common.SingleGasesData.H2O,Modelica.Media.IdealGases.Common.SingleGasesData.CO2,
          Modelica.Media.IdealGases.Common.SingleGasesData.H2},
    fluidConstants={Modelica.Media.IdealGases.Common.FluidData.N2,Modelica.Media.IdealGases.Common.FluidData.O2,
                    Modelica.Media.IdealGases.Common.FluidData.H2O,Modelica.Media.IdealGases.Common.FluidData.CO2,
                    Modelica.Media.IdealGases.Common.FluidData.H2},
    substanceNames={"Nitrogen","Oxygen","Water","Carbondioxide","Hydrogen"},
    reference_X={0.0,0.0,0.0,0.0,1.0});

  annotation (Documentation(info="<html>

</html>"));
end ModelicaMediaH2mixGases;
