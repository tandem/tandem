within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_PT_der
  input Units.SI.AbsolutePressure p "pressure";
  input Units.SI.Temperature T "Temperature";
  input Integer mode = 0 "Région IF97 - 0:calcul automatique";

  input Real p_der "Pression";
  input Real T_der "Température";

  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_pT pro_der;
protected
  Integer region;
  Boolean supercritical;
  Integer error;

  Units.SI.Density d;

  // From aux record
  Units.SI.Pressure p_aux "pressure";
  Units.SI.Temperature T_aux "temperature";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificHeatCapacity R "gas constant";
  Units.SI.SpecificHeatCapacity cp "specific heat capacity";
  Real cpt "derivative of cp w.r.t. temperature";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Real cvt "derivative of cv w.r.t. temperature";
  Units.SI.Density rho "density";
  Units.SI.SpecificEntropy s "specific entropy";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
  Real ptt "2nd derivative of pressure wrt temperature";
  Real pdd "2nd derivative of pressure wrt density";
  Real ptd "mixed derivative of pressure w.r.t. density and temperature";
  Real vt "derivative of specific volume w.r.t. temperature";
  Real vp "derivative of specific volume w.r.t. pressure";
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "mixed derivative of specific volume w.r.t. pressure and temperature";
  Real x "dryness fraction";
  Real dpT "dp/dT derivative of saturation curve";

  // needed
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  Real vp3 "vp^3";
  Real ivp3 "1/vp3";
  Units.SI.SpecificVolume v;

  Real rho2;
  Real quotient;
  Real quotient2;
  Real pd2;
  Real pd3;
  Real pt2;
  Real pt3;
algorithm

  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_pT(
    p,
    T,
    mode);
  R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  if (region == 1) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, T);
    x := 0.0;

    h := R*T*g.tau*g.gtau;
    s := R*(g.tau*g.gtau - g.g);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/rho;
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt*vt*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd;
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt*vt + 3.0*vp*vp*T*vt*vtt -
          3.0*vtp*vp*T*vt*vt + T*vt*vt*vt*vpp)*ivp3;

    // calculate the derivatives
    pro_der.x := 0;
    pro_der.duTp := (-vt - T*vtt - p*vtp)*p_der +
          (cpt - p*vtt)*T_der;
    pro_der.dupT := (-T*vtp - vp - p*vpp)*p_der +
          (-vt - T*vtt - p*vtp)*T_der;
    pro_der.ddpT := -rho2*(vpp*p_der + vtp*T_der);
    pro_der.ddTp := -rho2*(vtp*p_der + vtt*T_der);
    pro_der.cp := (-T*vtt)*p_der + cpt*T_der;
    pro_der.s := (-vt)*p_der + (cp/T)*T_der;
    pro_der.u := (v-T*vt)*p_der + (cp-p*vt)*T_der;
    pro_der.h := (v - T*vt)*p_der + cp*T_der;
    pro_der.d := -rho2*(vp*p_der + vt*T_der);

  elseif (region == 2) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, T);
    x := 1.0;

    h := R*T*g.tau*g.gtau;
    s := R*(g.tau*g.gtau - g.g);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/rho;
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt*vt*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd;
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt*vt + 3.0*vp*vp*T*vt*vtt -
          3.0*vtp*vp*T*vt*vt + T*vt*vt*vt*vpp)*ivp3;

    // calculate the derivatives
    pro_der.x := 0;
    pro_der.duTp := (-vt - T*vtt - p*vtp)*p_der +
          (cpt - p*vtt)*T_der;
    pro_der.dupT := (-T*vtp - vp - p*vpp)*p_der +
          (-vt - T*vtt - p*vtp)*T_der;
    pro_der.ddpT := -rho2*(vpp*p_der + vtp*T_der);
    pro_der.ddTp := -rho2*(vtp*p_der + vtt*T_der);
    pro_der.cp := (-T*vtt)*p_der + cpt*T_der;
    pro_der.s := (-vt)*p_der + (cp/T)*T_der;
    pro_der.u := (v-T*vt)*p_der + (cp-p*vt)*T_der;
    pro_der.h := (v - T*vt)*p_der + cp*T_der;
    pro_der.d := -rho2*(vp*p_der + vt*T_der);
  elseif (region == 3) then
    (rho,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dofpt3(
      p,
      T,
      delp=1.0e-7);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(rho, T);

    rho2 := rho*rho;
    h := R*T*(f.tau*f.ftau + f.delta*f.fdelta);
    s := R*(f.tau*f.ftau - f.f);
    pd := R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    pd2 := pd*pd;
    pd3 := pd*pd2;
    pt := R*rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    pt2 := pt*pt;
    pt3 := pt*pt*pt;
    cv := R*(-f.tau*f.tau*f.ftautau);
    x := 0.0;
    pdd := R*T*f.delta/rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    ptt := R*rho*f.delta*f.tau*f.tau/T*f.fdeltatautau;
    ptd := R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau -
          f.delta*f.tau*f.fdeltadeltatau);
    cvt := R*f.tau*f.tau/T*(2.0*f.ftautau + f.tau*f.ftautautau);
    cpt := (cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/(rho2) -
          pt*ptd)/pd;

    // calculate the derivatives
    pro_der.x := 0;
    quotient := 1/(rho2*pd);
    quotient2 := quotient/(rho*pd2);
    pro_der.duTp := quotient2*(-(rho*pd2*T*ptt + ptd*rho*pd*p - 2.0*rho*pd*pt*T*ptd +
          rho*pd2*pt - 2.0*pt*pd*p + 2.0*pd*pt2*T - pt*pdd*rho*p + pdd*rho*pt2*T)*p_der +
          (rho2*rho*pd3*cvt - rho*pd2*ptt*p + 3.0*rho*pd2*pt*T*ptt +
          2.0*ptd*rho*pd*pt*p - 3.0*ptd*rho*pd*pt2*T + rho*pd2*pt2 -
          2.0*pt2*pd*p + 2.0*T*pt3*pd - pt2*pdd*rho*p + T*pt3*pdd*rho)*T_der);
    pro_der.dupT := quotient2 *((rho*pd2 - rho*pd*T*ptd - 2.0*pd*p +
          2.0*pd*T*pt - pdd*rho*p + pdd*rho*T*pt)*p_der -
          (rho*pd2*T*ptt + ptd*rho*pd*p - 2.0*rho*pd*pt*T*ptd +
          rho*pd2*pt - 2.0*pt*pd*p + 2.0*pd*pt2*T - pt*pdd*rho*p +
          pdd*rho*pt2*T)*T_der);
    pro_der.ddpT := -(1/pd3)*(pdd*p_der + (ptd*pd - pt*pdd)*T_der);
    pro_der.ddTp := -(1/pd3)*((ptd*pd - pt*pdd)*p_der +
          (ptt*pd2 - 2.0*pt*ptd*pd + pt2*pdd)*T_der);
    pro_der.cp := quotient2*(-T*(rho*pd2*ptt - 2.0*rho*pd*pt*ptd +
          2.0*pd*pt2 + pdd*rho*pt^2)*p_der +
          (rho2*rho*pd3*cvt + 3.0*rho*pd2*pt*T*ptt + rho*pd2*pt2 -
          3.0*ptd*rho*pd*pt2*T + 2.0*T*pt3*pd + T*pt3*pdd*rho)*T_der);
    pro_der.s := quotient*(-pt*p_der + (cv*rho2*pd/T+pt2)*T_der);
    pro_der.u := quotient*(-(-rho*pd + T*pt)*p_der +
          (cv*rho2*pd - pt*p + pt2*T)*T_der);
    pro_der.h := quotient*((-rho*pd + T*pt)*p_der +
          (rho2*pd*cv + T*pt*pt)*T_der);
    pro_der.d := (1/pd)*(p_der - pt*T_der);

  elseif (region == 5) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5L3(p, T);
    x := 1.0;

    h := R*T*g.tau*g.gtau;
    s := R*(g.tau*g.gtau - g.g);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/rho;
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt*vt*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd;
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt*vt + 3.0*vp*vp*T*vt*vtt -
          3.0*vtp*vp*T*vt*vt + T*vt*vt*vt*vpp)*ivp3;

    // calculate the derivatives
    pro_der.x := 0;
    pro_der.duTp := (-vt - T*vtt - p*vtp)*p_der +
          (cpt - p*vtt)*T_der;
    pro_der.dupT := (-T*vtp - vp - p*vpp)*p_der +
          (-vt - T*vtt - p*vtp)*T_der;
    pro_der.ddpT := -rho2*(vpp*p_der + vtp*T_der);
    pro_der.ddTp := -rho2*(vtp*p_der + vtt*T_der);
    pro_der.cp := (-T*vtt)*p_der + cpt*T_der;
    pro_der.s := (-vt)*p_der + (cp/T)*T_der;
    pro_der.u := (v-T*vt)*p_der + (cp-p*vt)*T_der;
    pro_der.h := (v - T*vt)*p_der + cp*T_der;
    pro_der.d := -rho2*(vp*p_der + vt*T_der);
  else
    assert(false, "Water_pT_der: error in region computation of IF97 steam tables"
    + "(p = " + String(p) + ", T = " + String(T) + ", region = " + String(region) + ")");
  end if;

  annotation (
    Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_PT_der;
