within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function DynamicViscosity_rhoT
  input Units.SI.Density rho "Density";
  input Units.SI.Temperature T "Temperature";

  output Units.SI.DynamicViscosity mu "Dynamic viscosity";
algorithm

  mu := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Transport.visc_dT(rho, T);
  annotation (
    smoothOrder=2,
    Window(
      x=0.34,
      y=0.34,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end DynamicViscosity_rhoT;
