within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_sat_P
  input Units.SI.AbsolutePressure P "Pressure";

protected
  Units.SI.Temperature T;

public
  output ThermoSysPro_H2.Properties.WaterSteam.Common.PropThermoSat lsat
    annotation (Placement(transformation(extent={{-85,15},{-15,85}}, rotation=0)));
  output ThermoSysPro_H2.Properties.WaterSteam.Common.PropThermoSat vsat
    annotation (Placement(transformation(extent={{15,15},{85,85}}, rotation=0)));
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs gl annotation (
      Placement(transformation(extent={{-85,-85},{-15,-15}}, rotation=0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs gv annotation (
      Placement(transformation(extent={{15,-85},{85,-15}}, rotation=0)));
algorithm

  /*  if (not (P < SteamIF97.data.plimit4a)) then
    assert(false, "Eau_sat_P: Pression > 16.5292e6 Pa");
  end if;*/

  T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(P);

  gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(P, T);
  lsat := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsPropsSat(
    P,
    T,
    gl);

  gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(P, T);
  vsat := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsPropsSat(
    P,
    T,
    gv);
  //
  annotation (
    derivative = Water_sat_P_der,
    Window(
      x=0.34,
      y=0.21,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_sat_P;
