within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_Ps_der
  input Units.SI.AbsolutePressure p "Pression";
  input Units.SI.SpecificEntropy s "Entropie spécifique";
  input Integer mode = 0 "Région IF97 - 0:calcul automatique";

  input Real p_der "derivative of Pressure";
  input Real s_der "derivative of Specific enthropy";

protected
  Boolean supercritical;

  // variables taken from auxiliary record
  Integer phase "phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  Integer region(min=1, max=5) "IF 97 region";
  Units.SI.Temperature T "temperature";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificHeatCapacity R "gas constant";
  Units.SI.SpecificHeatCapacity cp "specific heat capacity";
  Real cpt "derivative of cp w.r.t. temperature";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Real cvt "derivative of cv w.r.t. temperature";
  Units.SI.Density rho "density";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
  Real ptt "2nd derivative of pressure wrt temperature";
  Real pdd "2nd derivative of pressure wrt density";
  Real ptd "mixed derivative of pressure w.r.t. density and temperature";
  Real vt "derivative of specific volume w.r.t. temperature";
  Real vp "derivative of specific volume w.r.t. pressure";
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "mixed derivative of specific volume w.r.t. pressure and temperature";
  Real x "dryness fraction";
  Real dpT "dp/dT derivative of saturation curve";
  Units.SI.SpecificEntropy auxs "specific entropy";

  // variables taken from waterBaseProp_ps
  Integer error "error flag for inverse iterations";
  Units.SI.SpecificEntropy s_liq "liquid specific entropy";
  Units.SI.Density d_liq "liquid density";
  Units.SI.SpecificEntropy s_vap "vapour specific entropy";
  Units.SI.Density d_vap "vapour density";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Units.SI.Temperature t1
    "temperature at phase boundary, using inverse from region 1";
  Units.SI.Temperature t2
    "temperature at phase boundary, using inverse from region 2";

  // variables needed
  Real detPH;
  Real dtsp;
  Real dtps;
  Real ddsp;
  Real ddps;
  Real dsd;

  Real detPH_t;
  Real detPH_d;

  Real dcp_t;
  Real dcp_d;

  Real dcps;
  Real dcpp;

  Real dxv;
  Real dxd;
  Real dvTl;
  Real dvTv;
  Real dxT;
  Real duTl;
  Real duTv;
  Real dpTT;
  Real dxdd;
  Real dxTd;
  Real dvTTl;
  Real dvTTv;
  Real dxTT;
  Real duTTl;
  Real duTTv;

  Real rho2;
  Real cp3;
  Real invcp3;
  Real cpinv;
  Real vt2;
  Real pt2;
  Real pt3;
  Real quotient;

public
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ps pro_der;
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau" annotation (
      Placement(transformation(extent={{-90,-85},{-43.3333,-38.3333}}, rotation=
           0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau"
    annotation (Placement(transformation(extent={{-23.3333,-85},{23.3333,-38.3333}},
          rotation=0)));
algorithm
  //assert(false,"Water_ph: Derivatives of cp not yet functional");
  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  phase := if ((s < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sl_p(
    p)) or (s > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sv_p(p))
     or supercritical) then 1 else 2;

  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ps(
    p,
    s,
    phase,
    mode);

  R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  auxs := s;
  if (region == 1) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps1(p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, T);

    h := R*T*g.tau*g.gtau;
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cpinv := 1/cp;
    cp3 := cp*cp*cp;
    invcp3 := 1/cp3;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    x := 0.0;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);

    // calculate derivative

    pro_der.cp := cpinv*T*(-(vtt*cp-cpt*vt)*p_der + cpt*s_der);
    pro_der.x := 0.0;
    pro_der.ddps := (-rho2*(cp3*vpp + 3.0*cp*cp*T*vt*vtp +
            3.0*T*T*vtt*cp*vt2 - T*T*vt2*vt*cpt + T*vt2*vt*cp)*invcp3)*p_der +
            (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp - cpt*T*vt2 +
            cp*vt2)*invcp3)*s_der;
    pro_der.ddsp := (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp -
            cpt*T*vt2 + cp*vt2)*invcp3)*p_der +
            (-rho2*(-cpt*T*vt + cp*vt + T*vtt*cp)*T*invcp3)*s_der;
    pro_der.h := p_der/rho + T*s_der;
    pro_der.u := cpinv*(-p*(vp*cp+T*vt2)*p_der + (cp-p*vt)*T*s_der);
    pro_der.d := cpinv*(-rho2 *(vp*cp+T*vt2)*p_der + (-rho2*vt*T)*s_der);
    pro_der.T := (T*cpinv)*(vt*p_der  + s_der);

  elseif (region == 2) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps2(p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, T);

    h := R*T*g.tau*g.gtau;
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cpinv := 1/cp;
    cp3 := cp*cp*cp;
    invcp3 := 1/cp3;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    x := 0.0;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);

    // calculate derivative

    pro_der.cp := cpinv*T*(-(vtt*cp-cpt*vt)*p_der + cpt*s_der);
    pro_der.x := 0.0;
    pro_der.ddps := (-rho2*(cp3*vpp + 3.0*cp*cp*T*vt*vtp +
            3.0*T*T*vtt*cp*vt2 - T*T*vt2*vt*cpt + T*vt2*vt*cp)*invcp3)*p_der +
            (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp - cpt*T*vt2 +
            cp*vt2)*invcp3)*s_der;
    pro_der.ddsp := (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp -
            cpt*T*vt2 + cp*vt2)*invcp3)*p_der +
            (-rho2*(-cpt*T*vt + cp*vt + T*vtt*cp)*T*invcp3)*s_der;
    pro_der.h := p_der/rho + T*s_der;
    pro_der.u := cpinv*(-p*(vp*cp+T*vt2)*p_der + (cp-p*vt)*T*s_der);
    pro_der.d := cpinv*(-rho2 *(vp*cp+T*vt2)*p_der + (-rho2*vt*T)*s_der);
    pro_der.T := (T*cpinv)*(vt*p_der  + s_der);

  elseif (region == 3) then
    (rho,T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofps3(
      p,
      s,
      delp=1.0e-7,
      dels=1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(rho, T);

    rho2 := rho*rho;
    h := R*T*(f.tau*f.ftau + f.delta*f.fdelta);
    auxs := R*(f.tau*f.ftau - f.f);
    pd := R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    pt := R*rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    pt2 := pt*pt;
    pt3 := pt2*pt;
    cv := abs(R*(-f.tau*f.tau*f.ftautau))
      "can be close to neg. infinity near critical point";
    cp := (rho2*pd*cv + T*pt*pt)/(rho*rho*pd);
    pdd := R*T*f.delta/rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    ptt := R*rho*f.delta*f.tau*f.tau/T*f.fdeltatautau;
    ptd := R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
         -f.delta*f.tau*f.fdeltadeltatau);
    cvt := R*f.tau*f.tau/T*(2.0*f.ftautau + f.tau*f.ftautautau);
    x := 0.0;

    // Not in cache
    dsd := -pt/rho2;
    detPH := cp*pd;

    dtsp :=T*pd/detPH;
    dtps :=-T*dsd/detPH;
    ddsp :=-T*pt/detPH;
    ddps :=cv/detPH;

    detPH_t := cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/(rho2);
    detPH_d := cv*pdd + (2.0*pt*(ptd - pt/rho) - ptt*pd)*T/(rho2);

    dcp_t :=(detPH_t - cp*ptd)/pd;
    dcp_d :=(detPH_d - cp*pdd)/pd;

    dcps := ddsp * dcp_d + dtsp * dcp_t;
    dcpp := ddps * dcp_d + dtps * dcp_t;

    quotient := 1/(cv*rho2*pd + pt2*T);

    pro_der.cp := dcps*s_der + dcpp*p_der;
    pro_der.x := 0.0;
    pro_der.ddps := rho2/(quotient*quotient*quotient)*(-(-cvt*T^2*pt3 + 3.0*cv^2*T*pt*rho2*ptd +
            3.0*cv*T^2*pt2*ptt + cv*T*pt3 - 2.0*cv^2*rho*pt2*T +
            cv^3*rho2*rho2*pdd)*p_der +
            (pt2*T*cvt*rho2*pd + 2*pt2*T*cv*rho2*ptd + pt3*T^2*ptt -
            pt2*cv*rho2*pd - 2.0*pt*T*ptt*cv*rho2*pd + cv^2*rho2*rho2*pt*pdd -
            2.0*cv*rho*T*pt3 - cv^2*rho2*rho2*ptd*pd)*T*s_der);
    pro_der.ddsp := quotient/(rho2*T*pt2)*(-(pt2*T*cvt*rho2*pd + 2.0*pt2*T*cv*rho2*ptd +
            pt3*T^2*ptt - pt2*cv*rho2*pd - 2.0*pt*T*ptt*cv*rho2*pd +
            cv^2*rho2*rho2*pt*pdd - 2.0*cv*rho*T*pt3 - cv^2*rho2*rho2*ptd*pd)*p_der -
            (rho^3*pd^2*T*pt*cvt + 2.0*rho2*rho*pd*T*pt*cv*ptd +
            2.0*rho*pd*T^2*pt2*ptt - rho2*rho*pd^2*pt*cv - ptt*rho2*rho*T*pd^2*cv -
            T*pt2*rho2*rho*cv*pdd - T^2*pt3*rho*ptd + 2.0*T^2*pt2*pt2)*s_der);
    pro_der.h := p_der/rho + T*s_der;
    pro_der.u := quotient*((cv*rho2*pd-pt*p + pt2*T)*T*s_der + cv*p*p_der);
    pro_der.d := rho2*quotient*(-T*pt*s_der + cv*p_der);
    pro_der.T := T*quotient*(pt*p_der + rho2*pd*s_der);

  elseif (region == 4) then
    s_liq := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sl_p(p);
    s_vap := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sv_p(p);
    x := if (s_vap <> s_liq) then (s - s_liq)/(s_vap - s_liq) else 1.0;
    if p < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PLIMIT4A then
      t1 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps1(p, s_liq);
      t2 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps2(p, s_vap);
      gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, t1);
      gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, t2);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gv);
      T := t1 + x*(t2 - t1);
    else
      T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(p);
      d_liq := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhol_T(T);
      d_vap := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhov_T(T);
      fl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_liq, T);
      fv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_vap, T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fv);
    end if;
    dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*liq.d*vap.d/(liq.d - vap.d)
       else ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(T);
    h := h + x*(vap.h - liq.h);
    rho := liq.d*vap.d/(vap.d + x*(liq.d - vap.d));
    rho2 := rho*rho;
    //cp := liq.cp + x*(vap.cp - liq.cp);
    //pt := liq.pt + x*(vap.pt - liq.pt);
    //pd := liq.pd + x*(vap.pd - liq.pd);

    dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d-vap.d) else 0.0;
    dxd := -dxv/(rho2);
    dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*dxv else
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(T);
    dvTl := (liq.pt -dpT)/(liq.pd*liq.d*liq.d);
    dvTv := (vap.pt -dpT)/(vap.pd*vap.d*vap.d);
    dxT := -dxv*(dvTl + x*(dvTv-dvTl));
    duTl := liq.cv + (T*liq.pt-p)*dvTl;
    duTv := vap.cv + (T*vap.pt-p)*dvTv;
    cv := duTl + x*(duTv-duTl) + dxT * (vap.u-liq.u);
    dpTT := dxv*(vap.cv/T-liq.cv/T + dvTv*(vap.pt-dpT)-dvTl*(liq.pt-dpT));
    dxdd := 2.0*dxv/(rho2*rho);
    dxTd := dxv*dxv*(dvTv-dvTl)/(rho2);
    dvTTl := ((liq.ptt-dpTT)/(liq.d*liq.d) + dvTl*(liq.d*dvTl*(2.0*liq.pd + liq.d*liq.pdd)
         -2.0*liq.ptd))/liq.pd;
    dvTTv := ((vap.ptt-dpTT)/(vap.d*vap.d) + dvTv*(vap.d*dvTv*(2.0*vap.pd + vap.d*vap.pdd)
         -2.0*vap.ptd))/vap.pd;
    dxTT := -dxv*(2.0*dxT*(dvTv-dvTl) + dvTTl + x*(dvTTv-dvTTl));
    duTTl := liq.cvt +(liq.pt-dpT + T*(2.0*liq.ptt -liq.d*liq.d*liq.ptd *dvTl))*dvTl + (T*
      liq.pt - p)*dvTTl;
    duTTv := vap.cvt +(vap.pt-dpT + T*(2.0*vap.ptt -vap.d*vap.d*vap.ptd *dvTv))*dvTv + (T*
      vap.pt - p)*dvTTv;
    cvt := duTTl + x *(duTTv -duTTl) + 2.0*dxT*(duTv-duTl) + dxTT *(vap.u-liq.u);

    // not in cache
    detPH := T*dpT*dpT/(rho2);
    dtps := 1.0 / dpT;
    ddsp := -T*dpT/detPH;
    ddps := cv/detPH;

    ptt :=dpTT;

    // calculate derivatives
    pro_der.x := if (s_vap <> s_liq) then s_der/(s_vap - s_liq) else 0.0;
    pro_der.ddps := (-rho2*(-cvt*T*dpT + 3.0*cv*T*ptt + cv*dpT -
            2.0*cv^2*rho) / (dpT*dpT*dpT*dpT*T*T))*p_der +
            ((T*ptt - 2.0*cv*rho)*rho2 / (dpT*dpT*dpT*T))*s_der;
    pro_der.ddsp := (-(T*ptt - 2.0*cv*rho) / (rho2*T*dpT))*p_der +
            (-2.0 /rho)*s_der;
    pro_der.cp := 0.0;
    pro_der.h := p_der/rho + T*s_der;
    pro_der.u := (ddps*p/rho2)*p_der + (ddsp*p/rho2 + T)*s_der;
    pro_der.d := ddps*p_der + ddsp*s_der;
    pro_der.T := dtps*p_der;

  elseif (region == 5) then
    (T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofps5(
      p,
      s,
      relds=1.0e-7);
    assert(error == 0, "error in inverse iteration of steam tables");
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5L3(p, T);
    h := R*T*g.tau*g.gtau;
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    cp := -R*g.tau*g.tau*g.gtautau;
    cpinv := 1/cp;
    cp3 := cp*cp*cp;
    invcp3 := 1/cp3;
    cv := R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    x := 0.0;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);

    // calculate derivative

    pro_der.cp := cpinv*T*(-(vtt*cp-cpt*vt)*p_der + cpt*s_der);
    pro_der.x := 0.0;
    pro_der.ddps := (-rho2*(cp3*vpp + 3.0*cp*cp*T*vt*vtp +
            3.0*T*T*vtt*cp*vt2 - T*T*vt2*vt*cpt + T*vt2*vt*cp)*invcp3)*p_der +
            (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp - cpt*T*vt2 +
            cp*vt2)*invcp3)*s_der;
    pro_der.ddsp := (-rho2*T*(2.0*vtt*T*vt*cp + cp*cp*vtp -
            cpt*T*vt2 + cp*vt2)*invcp3)*p_der +
            (-rho2*(-cpt*T*vt + cp*vt + T*vtt*cp)*T*invcp3)*s_der;
    pro_der.h := p_der/rho + T*s_der;
    pro_der.u := cpinv*(-p*(vp*cp+T*vt2)*p_der + (cp-p*vt)*T*s_der);
    pro_der.d := cpinv*(-rho2 *(vp*cp+T*vt2)*p_der + (-rho2*vt*T)*s_der);
    pro_der.T := (T*cpinv)*(vt*p_der  + s_der);
  else
    assert(false, "Water_Ps_der: Incorrect region number");
  end if;
  annotation (
    Window(
      x=0.22,
      y=0.2,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_Ps_der;
