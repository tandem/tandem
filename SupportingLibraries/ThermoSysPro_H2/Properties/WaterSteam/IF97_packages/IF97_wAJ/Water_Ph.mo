within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_Ph
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";
  input Integer mode = 0 "IF97 region. 0:automatic";

protected
  Integer phase;
  Integer region;
  Integer error;
  Units.SI.Temperature T;
  Units.SI.Density d;
  Boolean supercritical;

public
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ph pro
    annotation (Placement(transformation(extent={{-90,15},{-43.3333,61.6667}},
          rotation=0)));
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g annotation (
      Placement(transformation(extent={{-90,-85},{-43.3333,-38.3333}}, rotation=
           0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f annotation (
      Placement(transformation(extent={{-23.3333,-85},{23.3333,-38.3333}},
          rotation=0)));
algorithm

  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  phase := if ((h < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hl_p(
    p)) or (h > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hv_p(p))
     or supercritical) then 1 else 2;
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ph(
    p,
    h,
    phase,
    mode);
  if (region == 1) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph1(p, h);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ph(g);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 2) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph2(p, h);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ph(g);
    pro.x := if (supercritical) then -1 else 1;
  elseif (region == 3) then
    (d,T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofph3(
      p=p,
      h=h,
      delp=1.0e-7,
      delh=1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(d, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToProps_ph(f);

    if (h > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.HCRIT) then
      pro.x := if (supercritical) then -1 else 1;
    else
      pro.x := if (supercritical) then -1 else 0;
    end if;
  elseif (region == 4) then
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.water_ph_r4(p, h);
  elseif (region == 5) then
    (T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofph5(
      p=p,
      h=h,
      reldh=1.0e-7);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ph(g);
    pro.x := if (supercritical) then -1 else 1;
  else
    assert(false, "Water_Ph: Incorrect region number (" + String(region) + ")");
  end if;
  annotation (
    derivative(noDerivative=mode) = Water_Ph_der,
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Window(
      x=0.06,
      y=0.1,
      width=0.75,
      height=0.73),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_Ph;
