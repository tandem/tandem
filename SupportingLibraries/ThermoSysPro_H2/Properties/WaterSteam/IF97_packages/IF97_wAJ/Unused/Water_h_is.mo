within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Unused;
function Water_h_is
  input Units.SI.AbsolutePressure p;
  input Units.SI.SpecificEntropy s;
  input Integer phase;
  input Integer mode = 0;
  output Units.SI.SpecificEnthalpy h;

protected
  Integer region;
  Integer error;
  Units.SI.Temperature T;
  Units.SI.Density d;
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzData dTR(R=
        ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O) annotation (
      Placement(transformation(extent={{-85,15},{-15,85}}, rotation=0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsData pTR(R=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O)
    annotation (Placement(transformation(extent={{15,15},{85,85}}, rotation=0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g annotation (
      Placement(transformation(extent={{-85,-85},{-15,-15}}, rotation=0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f annotation (
      Placement(transformation(extent={{15,-85},{85,-15}}, rotation=0)));
algorithm

  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ps(
    p,
    s,
    phase,
    mode);
  if (region == 1) then
    h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.hofps1(p, s);
  elseif (region == 2) then
    h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.hofps2(p, s);
  elseif (region == 3) then
    (d,T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofps3(
      p=p,
      s=s,
      delp=1.0e-7,
      dels=1.0e-6);
    h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.hofdT3(d, T);
  elseif (region == 4) then
    h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.hofps4(p, s);
  elseif (region == 5) then
    (T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofps5(
      p=p,
      s=s,
      relds=1.0e-7);
    h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.hofpT5(p, T);
  else
    assert(false, "Eau_H_is: Numéro de région incorrect");
  end if;
  annotation (
    Window(
      x=0.27,
      y=0.15,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Ellipse(
          extent={{-100,100},{-60,-100}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p><b>Version 1.4</b></p>
</HTML>
"));
end Water_h_is;
