within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Unused;
function Water_rhoT
  input Units.SI.Density rho "Masse volumique";
  input Units.SI.Temperature T "Température";
  input Integer phase "2: diphasique, 1 sinon";
  input Integer mode = 0 "Région IF97 - 0:calcul automatique";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_dT pro;

protected
  Integer region;
  Integer error;
  Boolean supercritical;
  Units.SI.AbsolutePressure p;
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g annotation (
      Placement(transformation(extent={{-90,-85},{-43.3333,-38.3333}}, rotation=
           0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f annotation (
      Placement(transformation(extent={{-23.3333,-85},{23.3333,-38.3333}},
          rotation=0)));
algorithm
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_dT(
    rho,
    T,
    phase,
    mode);
  if (region == 1) then
    (p,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
      d=rho,
      T=T,
      reldd=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.IterationData.DELD,
      region=1);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(p, T);
    supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_dT(g);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 2) then
    (p,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
      d=rho,
      T=T,
      reldd=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.IterationData.DELD,
      region=2);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(p, T);
    supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_dT(g);
    pro.x := if (supercritical) then -1 else 1;
  elseif (region == 3) then
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(rho, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToProps_dT(f);
    pro.x := if (supercritical) then -1 else 0;
    supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  elseif (region == 4) then
    pro := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.TwoPhase.waterR4_dT(d=
       rho, T=T);
  elseif (region == 5) then
    (p,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
      d=rho,
      T=T,
      reldd=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.IterationData.DELD,
      region=5);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(p, T);
    supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_dT(g);
    pro.x := if (supercritical) then -1 else 1;
  else
    assert(false, "Eau_rhoT: Numéro de région incorrect");
  end if;
  annotation (
    Window(
      x=0.3,
      y=0.14,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Ellipse(
          extent={{-100,100},{-60,-100}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end Water_rhoT;
