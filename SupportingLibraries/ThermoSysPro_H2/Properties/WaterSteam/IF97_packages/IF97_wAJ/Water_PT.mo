within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_PT
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.Temperature T "Temperature";
  input Integer mode=0 "IF97 region. 0:automatic";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_pT pro;

protected
  Integer region;
  Boolean supercritical;
  Integer error;
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f;
  Units.SI.Density d;
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g;
algorithm

  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_pT(
    p,
    T,
    mode);
  if (region == 1) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_pT(g);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 2) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_pT(g);
    pro.x := if (supercritical) then -1 else 1;
  elseif (region == 3) then
    (d,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dofpt3(
      p=p,
      T=T,
      delp=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.IterationData.DELP);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(d, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToProps_pT(f);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 5) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_pT(g);
    pro.x := if (supercritical) then -1 else 1;
  else
    assert(false, "Water_PT: Incorrect region number");
  end if;

  annotation (
    derivative(noDerivative=mode) = Water_PT_der,
    Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_PT;
