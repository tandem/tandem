within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_Ph_der "Derivative function of Water_Ph"
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEnthalpy h "Specific enthalpy";
  input Integer mode = 0 "Région IF97 - 0:calcul automatique";
  //input CombiPlant.ThermoFluidPro.Media.Common.IF97TwoPhaseAnalytic aux "auxiliary record";

  input Real p_der "derivative of Pressure";
  input Real h_der "derivative of Specific enthalpy";

  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ph der_pro
    "Derivative";

protected
  Integer phase;
  Integer region;
  Boolean supercritical;

  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  Units.SI.Temperature T;
  Units.SI.SpecificHeatCapacity R "gas constant";
  Units.SI.Density rho "density";
  Real vt "derivative of specific volume w.r.t. temperature";
  Units.SI.SpecificHeatCapacity cp "specific heat capacity";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Real vp "derivative of specific volume w.r.t. pressure";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  Real dpT "dp/dT derivative of saturation curve";
  Real dxv "der of x wrt v";
  Real dvTl "der of v wrt T at boiling";
  Real dvTv "der of v wrt T at dew";
  Real dxT "der of x wrt T";
  Real duTl "der of u wrt T at boiling";
  Real duTv "der of u wrt T at dew";
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real cpt "derivative of cp w.r.t. temperature";
  Real cvt "derivative of cv w.r.t. temperature";
  Real dpTT "2nd der of p wrt T";
  Real dxdd "2nd der of x wrt d";
  Real dxTd "2nd der of x wrt d and T";
  Real dvTTl "2nd der of v wrt T at boiling";
  Real dvTTv "2nd der of v wrt T at dew";
  Real dxTT " 2nd der of x wrt T";
  Real duTTl "2nd der of u wrt T at boiling";
  Real duTTv "2nd der of u wrt T at dew";

  Integer error "error flag for inverse iterations";
  Units.SI.SpecificEnthalpy h_liq "liquid specific enthalpy";
  Units.SI.Density d_liq "liquid density";
  Units.SI.SpecificEnthalpy h_vap "vapour specific enthalpy";
  Units.SI.Density d_vap "vapour density";
  Real x "dryness fraction";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd vap
    "phase boundary property record";

  Units.SI.Temperature t1
    "temperature at phase boundary, using inverse from region 1";
  Units.SI.Temperature t2
    "temperature at phase boundary, using inverse from region 2";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Units.SI.SpecificVolume v;

  Real ptt "2nd derivative of pressure wrt temperature";
  Real pdd "2nd derivative of pressure wrt density";
  Real ptd "mixed derivative of pressure w.r.t. density and temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "mixed derivative of specific volume w.r.t. pressure and temperature";

  Real vp3 "vp^3";
  Real ivp3 "1/vp3";

  Real detPH "Determinant";
  Real dht;
  Real dhd;
  Real ddhp;
  Real ddph;
  Real dtph;
  Real dthp;
  Real detPH_d;
  Real detPH_t;
  Real dhtt;
  Real dhtd;
  Real ddph_t;
  Real ddph_d;
  Real ddhp_t;
  Real ddhp_d;

  Real duhp_t;
  Real duph_t;
  Real duph_d;

  Real dupp;
  Real duph;
  Real duhh;

  Real dcp_d;

  Real rho2 "square of density";
  Real rho3 "cube of density";
  Real cp3 "cube of specific heat capacity";
  Real cpcpp;
  Real quotient;
  Real vt2;
  Real pt2;
  Real pt3;

algorithm
  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  phase := if ((h < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hl_p(
    p)) or (h > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hv_p(p))
     or supercritical) then 1 else 2;
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ph(
    p,
    h,
    phase,
    mode);

  R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  if (region == 1) then
    // get variables
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph1(p, h);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, T);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    cp := -R*g.tau*g.tau*g.gtautau;
    cp3 := cp*cp*cp;
    cpcpp := cp*cp*p;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    v := 1/rho;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt2*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd "= pvv/d^4";
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt2 + 3.0*vp*vp*T*vt*vtt
      - 3.0*vtp*vp*T*vt2 + T*vt2*vt*vpp)*ivp3;

    // not in cache
    detPH := cp*pd;
    dht := cv + pt/rho;
    dhd := (pd - T*pt/rho)/rho;
    ddph := dht/ detPH;
    ddhp := -pt/detPH;
    dtph := -dhd/detPH;
    dthp := pd/detPH;
    detPH_d := cv*pdd + (2.0*pt *(ptd - pt/rho) - ptt*pd)*T/(rho2);
    detPH_t := cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/rho2;
    dhtt := cvt + ptt*v;
    dhtd := (ptd - (T * ptt + pt)*v) *v;
    ddhp_t := ddhp * (ptt / pt - detPH_t / detPH);
    ddhp_d := ddhp * (ptd / pt - detPH_d / detPH);
    ddph_t := ddph * (dhtt / dht - detPH_t / detPH);
    ddph_d := ddph * (dhtd / dht - detPH_d / detPH);
    dupp :=-(2.0*cp3*vp + cp3*p*vpp - 2.0*cp*cp*vt*v -
          2.0*cpcpp*vtp*v - cpcpp*vt*vp + 2.0*cp*cp*T*vt2 +
          3.0*cpcpp*vt*T*vtp - 4.0*T*vtt*cp*p*vt*v +
          3.0*T*T*vtt*cp*p*vt2 + cp*p*vtt/rho2 - cpt*p*vt/rho2 +
          2.0*cpt*p*vt2*v*T - cpt*p*vt2*T^2) / cp3;
    duph := -(vtp*cpcpp + cp*cp*vt - cp*p*vtt*v + 2.0*cp*p*vt*T*vtt +
          cpt*p*vt*v - cpt*p*vt2*T)/cp3;
    duhh := -p*(cp*vtt - cpt*vt) / cp3;

    // calculate derivatives
    der_pro.x := 0.0;
    der_pro.duhp := duph*p_der + duhh*h_der;
    der_pro.duph :=  dupp*p_der + duph* h_der;
    der_pro.ddph := (ddph * ddph_d + dtph * ddph_t)*p_der +
       (ddph * ddhp_d + dtph * ddhp_t)*h_der;
    der_pro.ddhp := (ddhp * ddhp_d + dthp * ddhp_t)*h_der +
       (ddph * ddhp_d + dtph * ddhp_t)*p_der;
    der_pro.cp := (-(T * vtt * cp + cpt/rho - cpt * T * vt) / cp)*p_der +
      cpt/cp*h_der;
    der_pro.s := -1/(rho*T)*p_der + 1/T*h_der;
    der_pro.u := (-(p*vp*cp + cp*v - p*vt*v + p*vt2 *T)/cp)*p_der + ((cp - p*vt)/cp)*h_der;
    der_pro.T := ((-v + T*vt)/cp)*p_der + (1/cp)*h_der;
    der_pro.d := (-rho2*(vp*cp - vt/rho + T*vt2)/cp)*p_der + (-rho2*vt/(cp))*h_der;

  elseif (region == 2) then
    // get variables
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph2(p, h);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, T);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    cp := -R*g.tau*g.tau*g.gtautau;
    cp3 := cp*cp*cp;
    cpcpp := cp*cp*p;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    v := 1/rho;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt2*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd "= pvv/d^4";
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt2 + 3.0*vp*vp*T*vt*vtt
      - 3.0*vtp*vp*T*vt2 + T*vt2*vt*vpp)*ivp3;

    // not in cache
    detPH := cp*pd;
    dht := cv + pt/rho;
    dhd := (pd - T*pt/rho)/rho;
    ddph := dht/ detPH;
    ddhp := -pt/detPH;
    dtph := -dhd/detPH;
    dthp := pd/detPH;
    detPH_d := cv*pdd + (2.0*pt *(ptd - pt/rho) - ptt*pd)*T/(rho2);
    detPH_t := cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/rho2;
    dhtt := cvt + ptt*v;
    dhtd := (ptd - (T * ptt + pt)*v) *v;
    ddhp_t := ddhp * (ptt / pt - detPH_t / detPH);
    ddhp_d := ddhp * (ptd / pt - detPH_d / detPH);
    ddph_t := ddph * (dhtt / dht - detPH_t / detPH);
    ddph_d := ddph * (dhtd / dht - detPH_d / detPH);
    dupp :=-(2.0*cp3*vp + cp3*p*vpp - 2.0*cp*cp*vt*v -
          2.0*cpcpp*vtp*v - cpcpp*vt*vp + 2.0*cp*cp*T*vt2 +
          3.0*cpcpp*vt*T*vtp - 4.0*T*vtt*cp*p*vt*v +
          3.0*T*T*vtt*cp*p*vt2 + cp*p*vtt/rho2 - cpt*p*vt/rho2 +
          2.0*cpt*p*vt2*v*T - cpt*p*vt2*T^2) / cp3;
    duph := -(vtp*cpcpp + cp*cp*vt - cp*p*vtt*v + 2.0*cp*p*vt*T*vtt +
          cpt*p*vt*v - cpt*p*vt2*T)/cp3;
    duhh := -p*(cp*vtt - cpt*vt) / cp3;

    // calculate derivatives
    der_pro.x := 0.0;
    der_pro.duhp := duph*p_der + duhh*h_der;
    der_pro.duph :=  dupp*p_der + duph* h_der;
    der_pro.ddph := (ddph * ddph_d + dtph * ddph_t)*p_der +
       (ddph * ddhp_d + dtph * ddhp_t)*h_der;
    der_pro.ddhp := (ddhp * ddhp_d + dthp * ddhp_t)*h_der +
       (ddph * ddhp_d + dtph * ddhp_t)*p_der;
    der_pro.cp := (-(T * vtt * cp + cpt/rho - cpt * T * vt) / cp)*p_der + cpt/cp*h_der;
    der_pro.s := -1/(rho*T)*p_der + 1/T*h_der;
    der_pro.u := (-(p*vp*cp + cp*v - p*vt*v + p*vt2 *T)/cp)*p_der + ((cp - p*vt)/cp)*h_der;
    der_pro.T := ((-v + T*vt)/cp)*p_der + (1/cp)*h_der;
    der_pro.d := (-rho2*(vp*cp - vt/rho + T*vt2)/cp)*p_der + (-rho2*vt/(cp))*h_der;

  elseif (region == 3) then
    // get variables
    (rho,T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofph3(
      p,
      h,
      delp=1.0e-7,
      delh=1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(rho, T);

    rho2 := rho*rho;
    rho3 := rho*rho2;
    v := 1/rho;
    pd := R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    pt := R*rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    pt2 := pt*pt;
    pt3 := pt2*pt;
    cv := abs(R*(-f.tau*f.tau*f.ftautau))
      "can be close to neg. infinity near critical point";
    cp := (rho2*pd*cv + T*pt2)/(rho2*pd);
    pdd := R*T*f.delta/rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    ptt := R*rho*f.delta*f.tau*f.tau/T*f.fdeltatautau;
    ptd := R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
         -f.delta*f.tau*f.fdeltadeltatau);
    cvt := R*f.tau*f.tau/T*(2.0*f.ftautau + f.tau*f.ftautautau);
    cpt := (cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/(rho2) - cp*ptd)/pd;

    // not in cache
    detPH := cp*pd;
    dht := cv + pt/rho;
    dhd := (pd - T*pt/rho)/rho;
    ddph := dht/ detPH;
    ddhp := -pt/detPH;
    dtph := -dhd/detPH;
    dthp := pd/detPH;
    detPH_d := cv*pdd + (2.0*pt *(ptd - pt/rho) - ptt*pd) *T/(rho2);
    detPH_t := cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/rho2;
    dhtt := cvt + ptt*v;
    dhtd := (ptd - (T * ptt + pt)*v) *v;
    ddhp_t := ddhp * (ptt / pt - detPH_t / detPH);
    ddhp_d := ddhp * (ptd / pt - detPH_d / detPH);
    ddph_t := ddph * (dhtt / dht - detPH_t / detPH);
    ddph_d := ddph * (dhtd / dht - detPH_d / detPH);

    dcp_d :=(detPH_d - cp*pdd)/pd;

    quotient := 1/(cv*rho2*pd + T*pt2)^3;
    dupp := -(-4.0*ptt*p*cv*rho2*pd*T*pt + 2.0*p*cvt*rho2*T*pt2*pd -
          2.0*ptt*p*T*pt2*rho*pd + 3.0*p*cv^2*rho3*ptd*T*pt +
          3.0*p*cv*rho*T^2*pt2*ptt - 2.0*pt*p*cv*rho3*ptd*pd +
          4.0*pt2*p*cv*rho2*ptd*T - 2.0*T^2*pt2*pt3 - 4.0*pt2*cv^2*rho3*pd*T -
          4.0*pt3*cv*rho2*T*pd - p*cvt*rho*T^2*pt3 + ptt*p*cv*rho3*pd^2 -
          2.0*p*cv^2*rho2*rho2*ptd*pd + 2.0*p*cv*rho2*pt2*pd +
          2.0*p*cv*rho*pt3*T - pt*p*cvt*rho3*pd^2 + ptd*p*rho*T*pt3 +
          5.0*pt*p*cv^2*rho3*pd + 2*pt*p*cv^2*rho2*rho2*pdd + pt2*p*cv*rho3*pdd +
          2.0*pt2*pt2*p*T - 2.0*cv^3*rho3*rho2*pd^2 - 2.0*pt*cv^2*rho2*rho2*pd^2-
          2.0*pt2*pt2*cv*rho*T^2 + 2.0*ptt*p*T^2*pt3 - pt3*p*rho*pd +
          2.0*p*cv^3*rho2*rho2*pd + p*cv^3*rho2*rho3*pdd)*quotient/rho;
    duph := (-2.0*ptt*p*cv*rho2*pd*T*pt + p*cvt*rho2*T*pt2*pd -
          2.0*ptt*p*T*pt2*rho*pd - 2.0*pt*p*cv*rho3*ptd*pd +
          2.0*pt2*p*cv*rho2*ptd*T - T^2*pt3*pt2 - 2*pt3*cv*rho2*T*pd +
          ptt*p*cv*rho3*pd^2 - p*cv^2*rho2*rho2*ptd*pd + 2.0*p*cv*rho2*pt2*pd -
          pt*p*cvt*rho3*pd^2 + ptd*p*rho*T*pt3 + 2.0*pt*p*cv^2*rho3*pd +
          pt*p*cv^2*rho2*rho2*pdd + pt2*p*cv*rho3*pdd + pt2*pt2*p*T -
          pt*cv^2*rho2*rho2*pd^2 + ptt*p*T^2*pt3 - pt3*p*rho*pd)*quotient;
    duhh := p*(-pt3*T*ptd + 2.0*ptd*cv*rho2*pd*pt - 2.0*pt2*cv*rho*pd +
          pt*cvt*rho2*pd^2 - pt2*cv*rho2*pdd + 2.0*pt2*T*ptt*pd -
          ptt*cv*rho2*pd^2 + pt3*pd)*rho2 *quotient;

    // calculate derivatives
    der_pro.x := 0.0;
    der_pro.duhp := duph*p_der + duhh*h_der;
    der_pro.duph := dupp*p_der + duph* h_der;
    der_pro.ddph := (ddph * ddph_d + dtph * ddph_t)*p_der +
       (ddph * ddhp_d + dtph * ddhp_t)*h_der;
    der_pro.ddhp := (ddhp * ddhp_d + dthp * ddhp_t)*h_der +
       (ddph * ddhp_d + dtph * ddhp_t)*p_der;
    der_pro.cp := (ddph * dcp_d + dtph * cpt)*p_der +
       (ddhp * dcp_d + dthp * cpt)*h_der;
    der_pro.s := -1/(rho*T)*p_der + 1/T*h_der;
    der_pro.u := ((cv*rho2*pd - pt*p + T*pt2)/(cv*rho2*pd + T*pt2))*h_der +
       ((cv*rho2*pd - p*cv*rho - pt*p + T*pt2)/(rho*(cv*rho2*pd + T*pt2)))*p_der;
    der_pro.T := ((-rho*pd + T*pt)/(rho2*pd*cv+ T*pt*pt))*p_der +
       ((rho2*pd)/(rho2*pd*cv + T*pt2))*h_der;
    der_pro.d := ((rho*(cv*rho + pt))/(rho2*pd*cv + T*pt2))*p_der +
      (-rho2*pt/(rho2*pd*cv + T*pt2))*h_der;

  elseif (region == 4) then
    // get variables
    h_liq := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hl_p(p);
    h_vap := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hv_p(p);
    x := if (h_vap <> h_liq) then (h - h_liq)/(h_vap - h_liq) else 1.0;

    if p < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PLIMIT4A then
      t1 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph1(p, h_liq);
      t2 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph2(p, h_vap);
      gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, t1);
      gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, t2);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gv);
      T := t1 + x*(t2 - t1);
    else
      T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(p);
      // how to avoid ?

      d_liq := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhol_T(T);
      d_vap := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhov_T(T);
      fl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_liq, T);
      fv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_vap, T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fv);
      //  dpT := BaseIF97.Basic.dptofT(T);
    end if;

    rho := liq.d*vap.d/(vap.d + x*(liq.d - vap.d));
    rho2 := rho*rho;
    rho3 := rho*rho2;
    v := 1/rho;
    dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d-vap.d) else 0.0;
    dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*dxv else
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(T);

    dvTl := (liq.pt -dpT)/(liq.pd*liq.d*liq.d);
    dvTv := (vap.pt -dpT)/(vap.pd*vap.d*vap.d);
    dxT := -dxv*(dvTl + x*(dvTv-dvTl));
    duTl := liq.cv + (T*liq.pt-p)*dvTl;
    duTv := vap.cv + (T*vap.pt-p)*dvTv;
    cv := duTl + x*(duTv-duTl) + dxT * (vap.u-liq.u);
    dpTT := dxv*(vap.cv/T-liq.cv/T + dvTv*(vap.pt-dpT)-dvTl*(liq.pt-dpT));
    dxdd := 2.0*dxv/(rho3);
    dxTd := dxv*dxv*(dvTv-dvTl)/(rho2);
    dvTTl := ((liq.ptt-dpTT)/(liq.d*liq.d) + dvTl*(liq.d*dvTl*(2.0*liq.pd + liq.d*liq.pdd)
         -2.0*liq.ptd))/liq.pd;
    dvTTv := ((vap.ptt-dpTT)/(vap.d*vap.d) + dvTv*(vap.d*dvTv*(2.0*vap.pd + vap.d*vap.pdd)
         -2.0*vap.ptd))/vap.pd;
    dxTT := -dxv*(2.0*dxT*(dvTv-dvTl) + dvTTl + x*(dvTTv-dvTTl));
    duTTl := liq.cvt +(liq.pt-dpT + T*(2.0*liq.ptt -liq.d*liq.d*liq.ptd *dvTl))*dvTl + (T*
      liq.pt - p)*dvTTl;
    duTTv := vap.cvt +(vap.pt-dpT + T*(2.0*vap.ptt -vap.d*vap.d*vap.ptd *dvTv))*dvTv + (T*
      vap.pt - p)*dvTTv;
    cvt := duTTl + x *(duTTv -duTTl) + 2.0*dxT*(duTv-duTl) + dxTT *(vap.u-liq.u);
    ptt := dpTT;

    // not in cache
    dht := cv + dpT * v;
    dhd := -T * dpT*v*v;
    detPH := -dpT * dhd;
    dtph := 1.0 / dpT;
    ddph := dht / detPH;
    ddhp := -dpT / detPH;
    detPH_d := -2.0 * v;                   /* = detPH_d / detPH */
    detPH_t := 2.0 * ptt / dpT + 1.0 / T; /* = detPH_t / detPH */
    dhtt := cvt + ptt * v;
    dhtd := -(T * ptt + dpT) *v*v;
    ddhp_t := ddhp * (ptt / dpT - detPH_t);
    ddhp_d := ddhp * (-detPH_d);
    ddph_t := ddph * (dhtt / dht - detPH_t);
    ddph_d := ddph * (dhtd / dht - detPH_d);
    duhp_t := (ddhp * dpT + p * ddhp_t) / (rho2);
    duph_t :=(ddph*dpT + p*ddph_t)/(rho2);
    duph_d :=((-2.0*ddph/rho + ddph_d)*p + 1.0)/(rho2);

    // calculate derivatives
    der_pro.x := if (h_vap <> h_liq) then h_der/(h_vap - h_liq) else 0.0;
    der_pro.duhp := (dtph * duhp_t)*p_der;
    der_pro.duph :=  (ddph * duph_d + dtph * duph_t)*p_der + (dtph * duhp_t)* h_der;
    der_pro.ddph := (ddph * ddph_d + dtph * ddph_t)*p_der + (ddhp * ddph_d)*h_der;
    der_pro.ddhp := (ddhp * ddhp_d)*h_der + (ddhp * ddph_d)*p_der;
    der_pro.cp :=0.0;
    der_pro.s := -1/(rho*T)*p_der + 1/T*h_der;
    der_pro.u := ((ddph*p/rho - 1.0)/rho)*p_der + (ddhp*p/(rho2) + 1.0)*h_der;
    der_pro.T := 1/dpT*p_der;
    der_pro.d := (rho*(rho*cv/dpT + 1.0)/(dpT*T))*p_der
       + (-rho2/(dpT*T))*h_der;

  elseif (region == 5) then
    // get variables
    (T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofph5(
      p,
      h,
      reldh=1.0e-7);
    assert(error == 0, "error in inverse iteration of steam tables");

    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5L3(p, T);
    rho := p/(R*T*g.pi*g.gpi);
    rho2 := rho*rho;
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    vt2 := vt*vt;
    cp := -R*g.tau*g.tau*g.gtautau;
    cp3 := cp*cp*cp;
    cpcpp := cp*cp*p;
    vp := R*T/(p*p)*g.pi*g.pi*g.gpipi;
    v := 1/rho;
    vtt := R*g.pi/p*g.tau/T*g.tau*g.gpitautau;
    vtp := R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    vpp := R*T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    cpt := R*g.tau*g.tau/T*(2*g.gtautau + g.tau*g.gtautautau);
    pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    vp3 := vp*vp*vp;
    ivp3 := 1/vp3;
    ptt := -(vtt*vp*vp -2.0*vt*vtp*vp +vt2*vpp)*ivp3;
    pdd := -vpp*ivp3/(rho2*rho2) - 2*v*pd "= pvv/d^4";
    ptd := (vtp*vp-vt*vpp)*ivp3/rho2 "= -ptv/d^2";
    cvt := (vp3*cpt + vp*vp*vt2 + 3.0*vp*vp*T*vt*vtt
      - 3.0*vtp*vp*T*vt2 + T*vt2*vt*vpp)*ivp3;

    // not in cache
    detPH := cp*pd;
    dht := cv + pt/rho;
    dhd := (pd - T*pt/rho)/rho;
    ddph := dht/ detPH;
    ddhp := -pt/detPH;
    dtph := -dhd/detPH;
    dthp := pd/detPH;
    detPH_d := cv*pdd + (2.0*pt *(ptd - pt/rho) - ptt*pd)*T/(rho2);
    detPH_t := cvt*pd + cv*ptd + (pt + 2.0*T*ptt)*pt/rho2;
    dhtt := cvt + ptt*v;
    dhtd := (ptd - (T * ptt + pt)*v) *v;
    ddhp_t := ddhp * (ptt / pt - detPH_t / detPH);
    ddhp_d := ddhp * (ptd / pt - detPH_d / detPH);
    ddph_t := ddph * (dhtt / dht - detPH_t / detPH);
    ddph_d := ddph * (dhtd / dht - detPH_d / detPH);
    dupp :=-(2.0*cp3*vp + cp3*p*vpp - 2.0*cp*cp*vt*v -
          2.0*cpcpp*vtp*v - cpcpp*vt*vp + 2.0*cp*cp*T*vt2 +
          3.0*cpcpp*vt*T*vtp - 4.0*T*vtt*cp*p*vt*v +
          3.0*T*T*vtt*cp*p*vt2 + cp*p*vtt/rho2 - cpt*p*vt/rho2 +
          2.0*cpt*p*vt2*v*T - cpt*p*vt2*T^2) / cp3;
    duph := -(vtp*cpcpp + cp*cp*vt - cp*p*vtt*v + 2.0*cp*p*vt*T*vtt +
          cpt*p*vt*v - cpt*p*vt2*T)/cp3;
    duhh := -p*(cp*vtt - cpt*vt) / cp3;

    // calculate derivatives
    der_pro.x := 0.0;
    der_pro.duhp := duph*p_der + duhh*h_der;
    der_pro.duph :=  dupp*p_der + duph* h_der;
    der_pro.ddph := (ddph * ddph_d + dtph * ddph_t)*p_der +
       (ddph * ddhp_d + dtph * ddhp_t)*h_der;
    der_pro.ddhp := (ddhp * ddhp_d + dthp * ddhp_t)*h_der +
       (ddph * ddhp_d + dtph * ddhp_t)*p_der;
    der_pro.cp := (-(T * vtt * cp + cpt/rho - cpt * T * vt) / cp)*p_der + cpt/cp*h_der;
    der_pro.s := -1/(rho*T)*p_der + 1/T*h_der;
    der_pro.u := (-(p*vp*cp + cp*v - p*vt*v + p*vt2 *T)/cp)*p_der + ((cp - p*vt)/cp)*h_der;
    der_pro.T := ((-v + T*vt)/cp)*p_der + (1/cp)*h_der;
    der_pro.d := (-rho2*(vp*cp - vt/rho + T*vt2)/cp)*p_der + (-rho2*vt/(cp))*h_der;

  else
    assert(false, "Water_Ph_der: Incorrect region number");
  end if;

  annotation (
    Window(
      x=0.22,
      y=0.2,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_Ph_der;
