within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_sat_P_der
  input Units.SI.AbsolutePressure P "Pression";

  input Real P_der "derivative of pressure";

protected
  Units.SI.Temperature T;
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature dpT
    "dp/dT derivative of saturation curve";
  Units.SI.Density d "density";
  Units.SI.SpecificHeatCapacity cp
    "Chaleur spécifique à pression constante";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Real vt(unit="m3/(kg.K)")
    "derivative of specific volume w.r.t. temperature";
  Real vp(unit="m3/(kg.Pa)")
    "derivative of specific volume w.r.t. pressure";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "Derivative of pressure wrt density";

  Real vp3 "Third power of vp";
  Real ivp3 "Inverse of third power of vp";

  Real cvt "Derivative of cv w.r.t. temperature";
  Real cpt "Derivative of cp w.r.t. temperature";

  Real ptt "2nd derivative of pressure wrt temperature";
  /*
  Real pdd "2nd derivative of pressure wrt density";
  Real ptd "Mixed derivative of pressure w.r.t. density and temperature";
  */
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "Mixed derivative of specific volume w.r.t. pressure and temperature";

  Real v "specific volume";
  Real pv;
  //Real ptv;

  Real tp;
  Real p2;
  Real pi2;

public
  output ThermoSysPro_H2.Properties.WaterSteam.Common.PropThermoSat dlsat
    annotation (Placement(transformation(extent={{-85,15},{-15,85}}, rotation=0)));
  output ThermoSysPro_H2.Properties.WaterSteam.Common.PropThermoSat dvsat
    annotation (Placement(transformation(extent={{15,15},{85,85}}, rotation=0)));
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gl annotation (
      Placement(transformation(extent={{-85,-85},{-15,-15}}, rotation=0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gv annotation (
      Placement(transformation(extent={{15,-85},{85,-15}}, rotation=0)));

algorithm
  /*  if (not (P < SteamIF97.data.plimit4a)) then
    assert(false, "Eau_sat_P: Pression > 16.5292e6 Pa");
  end if;*/

  T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(P);

  // get Gibbs derivatives of third order
  gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(P, T);
  gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(P, T);

  // Precalculs
  dpT := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(T);
  ptt := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.d2ptofT(T);
  tp := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dtsatofp(P);
  p2 := gl.p*gl.p;
  pi2 := gl.pi*gl.pi;

  // compute variables for liquid
  d := gl.p/(gl.R*T*gl.pi*gl.gpi);
  vp := gl.R*T/(p2)*pi2*gl.gpipi;
  vt := gl.R/gl.p*gl.pi*(gl.gpi - gl.tau*gl.gpitau);
  cp := -gl.R*gl.tau*gl.tau*gl.gtautau;
  v :=1/d;
  cv := gl.R*(-gl.tau*gl.tau*gl.gtautau +
      (gl.gpi - gl.tau*gl.gpitau)*(gl.gpi - gl.tau*gl.gpitau)/(gl.gpipi));

  // variables needed only for derivitve of cp

  pd := -gl.R*T*gl.gpi*gl.gpi/(gl.gpipi);
  pv := -pd*d*d;

  vtt := gl.R*gl.pi/gl.p*gl.tau/T*gl.tau*gl.gpitautau;

  vtp := gl.R*pi2/(p2)*(gl.gpipi - gl.tau*gl.gpipitau);
  vpp := gl.R*T*pi2*gl.pi/(p2*gl.p)*gl.gpipipi;
  vp3 := vp*vp*vp;
  ivp3 := 1/vp3;
  /*
  ptt := -(vtt*vp*vp -2.0*vt*vtp*vp + vt*vt*vpp)*ivp3;

  ptd := (vtp*vp-vt*vpp)*ivp3/(d*d) "= -ptv/d^2";
  ptv := -ptd/(v*v);
  */

  cpt := gl.R*gl.tau*gl.tau/T*(2*gl.gtautau + gl.tau*gl.gtautautau);
  cvt := (vp3*cpt + vp*vp*vt*vt + 3.0*vp*vp*T*vt*vtt -
          3.0*vtp*vp*T*vt*vt + T*vt*vt*vt*vpp)*ivp3;

  // compute derivatives for liquid
  //dlsat.u := (cv + (T*dpT - P)*vt)*tp*P_der;
  dlsat.pt := ptt*tp*P_der;
  dlsat.cv := cvt*tp*P_der;
  dlsat.cp := (cvt*tp + vp*dpT + ((v*pv + T*dpT)*vtp + (vp*pv + tp*dpT)*vt))*P_der;
  //dlsat.cp := (cvt + (vt*dpT + ptt*v) + ((v*pv + T*dpT)*vtt + (vt*pv + v*ptv + dpT + T*ptt)*vt))*tp*P_der;
  dlsat.h := (v - T*vt)*P_der + cp/dpT*P_der;
  dlsat.rho := -d*d*(vp + vt/dpT)*P_der;
  dlsat.T := tp*P_der;
  dlsat.P := P_der;

  p2 := gv.p*gv.p;
  pi2 := gv.pi*gv.pi;

  // compute variables for liquid
  d := gv.p/(gv.R*T*gv.pi*gv.gpi);
  vp := gv.R*T/(p2)*pi2*gv.gpipi;
  vt := gv.R/gv.p*gv.pi*(gv.gpi - gv.tau*gv.gpitau);
  cp := -gv.R*gv.tau*gv.tau*gv.gtautau;
  v :=1/d;
  cv := gv.R*(-gv.tau*gv.tau*gv.gtautau +
    (gv.gpi - gv.tau*gv.gpitau)*(gv.gpi - gv.tau*gv.gpitau)/(gv.gpipi));

  // variables needed only for derivitve of cp

  pd := -gv.R*T*gv.gpi*gv.gpi/(gv.gpipi);
  pv := -pd*d*d;

  vtt := gv.R*gv.pi/gv.p*gv.tau/T*gv.tau*gv.gpitautau;

  vtp := gv.R*pi2/(p2)*(gv.gpipi - gv.tau*gv.gpipitau);
  vpp := gv.R*T*pi2*gv.pi/(p2*gv.p)*gv.gpipipi;
  vp3 := vp*vp*vp;
  ivp3 := 1/vp3;
  /*
  ptt := -(vtt*vp*vp -2.0*vt*vtp*vp + vt*vt*vpp)*ivp3;

  ptd := (vtp*vp-vt*vpp)*ivp3/(d*d) "= -ptv/d^2";
  ptv := -ptd/(v*v);
  */

  cpt := gv.R*gv.tau*gv.tau/T*(2*gv.gtautau + gv.tau*gv.gtautautau);
  cvt := (vp3*cpt + vp*vp*vt*vt + 3.0*vp*vp*T*vt*vtt -
          3.0*vtp*vp*T*vt*vt + T*vt*vt*vt*vpp)*ivp3;

  // Compute the derivatives for vapour
  //dvsat.u := (cv + (T*dpT - P)*vt)*tp*P_der;
  dvsat.pt := ptt*tp*P_der;
  dvsat.cv := cvt*tp*P_der;
  dvsat.cp := (cvt*tp + vp*dpT + ((v*pv + T*dpT)*vtp + (vp*pv + tp*dpT)*vt))*P_der;
  //dlsat.cp := (cvt + (vt*dpT + ptt*v) + ((v*pv + T*dpT)*vtt + (vt*pv + v*ptv + dpT + T*ptt)*vt))*tp*P_der;
  dvsat.h := (v - T*vt)*P_der + cp/dpT*P_der;
  dvsat.rho := -d*d*(vp + vt/dpT)*P_der;
  dvsat.T := tp *P_der;
  dvsat.P := P_der;

  annotation (
    Window(
      x=0.34,
      y=0.21,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_sat_P_der;
