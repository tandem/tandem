within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function SpecificEnthalpy_PT_der
  input Units.SI.AbsolutePressure p "pressure";
  input Units.SI.Temperature T "Temperature";
  input Integer mode = 0 "Région IF97 - 0:calcul automatique";

  input Real p_der "Pression";
  input Real T_der "Température";

  output Real H "specific enthalpy";
protected
  Integer region;
  Boolean supercritical;
  Integer error;

  Units.SI.SpecificHeatCapacity R "gas constant";
  Units.SI.SpecificHeatCapacity cp "specific heat capacity";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Units.SI.Density rho "density";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";

  Real vt "derivative of specific volume w.r.t. temperature";

  // needed
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";

  Real rho2;

algorithm
  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_pT(
    p,
    T,
    mode);
  R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  if (region == 1) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(p, T);

    rho := p/(R*T*g.pi*g.gpi);
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    cp := -R*g.tau*g.tau*g.gtautau;

    H := (1/rho - T*vt)*p_der + cp*T_der;

  elseif (region == 2) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(p, T);
    rho := p/(R*T*g.pi*g.gpi);
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    cp := -R*g.tau*g.tau*g.gtautau;

    H := (1/rho - T*vt)*p_der + cp*T_der;
  elseif (region == 3) then
    (rho,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dofpt3(
      p,
      T,
      delp=1.0e-7);
    rho2 := rho*rho;
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(rho, T);

    pd := R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    pt := R*rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    cv := R*(-f.tau*f.tau*f.ftautau);

    H := 1/(rho2*pd)*((-rho*pd + T*pt)*p_der +
          (rho2*pd*cv + T*pt*p)*T_der);

  elseif (region == 5) then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(p, T);
    rho := p/(R*T*g.pi*g.gpi);
    vt := R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    cp := -R*g.tau*g.tau*g.gtautau;

    H := (1/rho - T*vt)*p_der + cp*T_der;
  else
    assert(false, "Water_pT_der: error in region computation of IF97 steam tables"
    + "(p = " + String(p) + ", T = " + String(T) + ", region = " + String(region) + ")");
  end if;

  annotation (
    Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end SpecificEnthalpy_PT_der;
