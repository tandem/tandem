within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ;
function Water_Ps
  input Units.SI.AbsolutePressure p "Pressure";
  input Units.SI.SpecificEntropy s "Specific entropy";
  input Integer mode = 0 "IF97 region. 0:automatic";

protected
  Integer phase;
  Integer region;
  Integer error;
  Units.SI.Temperature T;
  Units.SI.Density d;
  Boolean supercritical;

public
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ps pro;
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g annotation (
      Placement(transformation(extent={{-90,-85},{-43.3333,-38.3333}}, rotation=
           0)));
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f annotation (
      Placement(transformation(extent={{-23.3333,-85},{23.3333,-38.3333}},
          rotation=0)));
algorithm

  supercritical := (p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT);
  phase := if ((s < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sl_p(
    p)) or (s > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sv_p(p))
     or supercritical) then 1 else 2;

  region := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ps(
    p,
    s,
    phase,
    mode);
  if (region == 1) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps1(p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ps(g);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 2) then
    T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps2(p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ps(g);
    pro.x := if (supercritical) then -1 else 1;
  elseif (region == 3) then
    (d,T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofps3(
      p=p,
      s=s,
      delp=1.0e-7,
      dels=1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(d, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToProps_ps(f);
    pro.x := if (supercritical) then -1 else 0;
  elseif (region == 4) then
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.water_ps_r4(p, s);
  elseif (region == 5) then
    (T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofps5(
      p=p,
      s=s,
      relds=1.0e-7);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(p, T);
    pro := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToProps_ps(g);
    pro.x := if (supercritical) then -1 else 1;
  else
    assert(false, "Water_Ps: Incorrect region number");
  end if;
  annotation (
    derivative(noDerivative=mode) = Water_Ps_der,
    Window(
      x=0.22,
      y=0.2,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end Water_Ps;
