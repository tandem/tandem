within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities;
function evaluateDer
  "Evaluate Bspline and its first derivative at one parameter"
  extends Modelica.Icons.Function;
  input Data spline "Bspline to be evaluated";
  input Real u "Parameter value at which Bspline shall be evaluated";
  output Real x[spline.ndim] "Value of Bspline at u";
  output Real xd[spline.ndim] "First derivative of Bspline at u";
protected
  Integer span;
  Real N[2,spline.degree+1];
algorithm
  //Grenzwert von rechts lim (h->0) (f(x+h)-f(x))/h
  x := zeros(spline.ndim);
  xd := zeros(spline.ndim);

  span := Utilities.n_findSpan(spline.degree, u, spline.knots);
  N := Utilities.n_DersBasisFuns(span, u, spline.degree, 1, spline.knots);

  for i in 1:spline.ndim loop
    x[i] := N[1,:]*spline.controlPoints[span-spline.degree:span,i];
    xd[i] := N[2,:]*spline.controlPoints[span-spline.degree:span,i];
  end for;

end evaluateDer;
