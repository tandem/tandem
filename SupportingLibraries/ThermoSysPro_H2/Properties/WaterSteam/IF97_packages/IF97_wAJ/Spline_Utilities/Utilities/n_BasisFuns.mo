within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Utilities;
function n_BasisFuns "Compute the nonvanishing basis functions"
  extends Modelica.Icons.Function;
  input Integer i "index";
  input Real u "parameter";
  input Integer p "degree";
  input Real knots[:] "knot vector";
  output Real N[p+1] "Basis functions";
protected
  Integer j;
  Integer r;
  Real left[p+1];
  Real right[p+1];
  Real temp;
  Real saved;
algorithm
  /*
           The NURBS Book: Algorithm A2.2 (page 70)
        */
  N[1] := 1;
  for j in 1:p loop
    left[j] := u - knots[i+1-j];
    right[j] := knots[i+j] - u;
    saved := 0.0;

    for r in 1:j loop
      temp := N[r]/(right[r]+left[j-r+1]);
      N[r] := saved + right[r]*temp;
      saved := left[j-r+1]*temp;

    end for;
    N[j+1] := saved;
  end for;
end n_BasisFuns;
