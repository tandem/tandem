within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Utilities;
function n_findSpan "Determine the knot span index"
  extends Modelica.Icons.Function;
  input Integer p "degree";
  input Real u "parameter";
  input Real knots[:] "knot vector";
  output Integer i "The knot span index";
protected
  Integer n;
  Integer low;
  Integer high;
  Integer mid;
algorithm
  /*
           The NURBS Book: Algorithm A2.1 (page 68)
        */
  n:=size(knots,1)-p-1;

  if abs(u-knots[n+1])<10e-12 then
    i := n;
  elseif abs(u-knots[1])<10e-12 then
    i := 1;
  else
    low := p;
    high := n+1;
    mid := integer((low+high)/2);

    while
      (u<knots[mid] or u>=knots[mid+1]) loop
      assert(low+1<high,"Value must be within limits for Utilities.n_findSpan");

      if (u<knots[mid]) then
        high := mid;

      else
        low := mid;

      end if;
      mid := integer((low+high)/2);

    end while;
    i := mid;

  end if;
end n_findSpan;
