within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities;
function safeEvaluate
  "Evaluate Bspline at one parameter safely, i.e. points outside the domain of the spline are moved inside the domain."
  extends Modelica.Icons.Function;
  input Data spline "Bspline to be evaluated";
  input Real u "Parameter value at which Bspline shall be evaluated";
  output Real x[spline.ndim] "Value of Bspline at u";
protected
  Real eps = 1e-10 "accurate enough?";
  Real umin =  spline.knots[1] + eps*abs(spline.knots[1]);
  Real umax =  spline.knots[end] - eps*abs(spline.knots[end]);
  Real ulim;
algorithm
  ulim := min(max(u,umin),umax);
  x :=
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.evaluate(
    spline, ulim);
end safeEvaluate;
