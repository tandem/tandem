within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities;
function dumpOneSpline
  "Write Modelica.Interpolation.Bspline1D.Data to file as record (not finished)."
  import Util = Modelica.Utilities;
  input String fileName "file name";
  input String splineName "Name of the spline in the array of records.";
  input
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Data
    spline "Data.";
  input Boolean lastSpline = false;
  output Boolean status "true if succesful.";
protected
  constant Integer recordsPerLine =  4;
  Integer nKnots;
  Integer n;
  Integer nLeftovers;
algorithm
  assert(spline.ndim == 1, "Bailing out - spline.ndim != 1");
  // This is only for testing. In the end we'd like to append multiple spline records to a file.
  //Util.Files.removeFile(fileName);
  Util.Streams.print("record IF97_spline = ThermoSysPro_addon.IF97.SplineUtilities.Modelica_Interpolation.Bspline1D.Data(", fileName);
  Util.Streams.print("    ndim = " + integerString(spline.ndim) + ",", fileName);
  Util.Streams.print("    ncontrol = " + integerString(spline.ncontrol) + ",", fileName);
  Util.Streams.print("    degree = " + integerString(spline.degree) + ",", fileName);
  nKnots := size(spline.knots, 1);
  Util.Streams.print("    knots = {", fileName);
  // print the knots
  n := integer(nKnots/recordsPerLine);
  nLeftovers := nKnots - n*recordsPerLine;
  // if nLeftOvers == 0 we output the last four records separately
  // or else we end up with "...xxx, xxx, }" (trailing ',') instead of "...xxx, xxx }"
  if nLeftovers == 0 then
    for j in 1:n-1 loop
      Util.Streams.print("    " + String(spline.knots[recordsPerLine * j - 3], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j - 2], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j - 1], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j], significantDigits=15) + ",",  fileName);
    end for;
    Util.Streams.print("    " + String(spline.knots[nKnots - 3], significantDigits=15) + ", " + String(spline.knots[nKnots - 2], significantDigits=15) + ", " + String(spline.knots[nKnots - 1], significantDigits=15) + ", " + String(spline.knots[nKnots], significantDigits=15),  fileName);
  else
    for j in 1:n loop
      Util.Streams.print("    " + String(spline.knots[recordsPerLine * j - 3], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j - 2], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j - 1], significantDigits=15) + ", " + String(spline.knots[recordsPerLine * j], significantDigits=15) + ",",  fileName);
    end for;
    if nLeftovers == 3 then
      Util.Streams.print("    " + String(spline.knots[nKnots - 2], significantDigits=15) + ", " + String(spline.knots[nKnots - 1], significantDigits=15) + ", " + String(spline.knots[nKnots], significantDigits=15),  fileName);
    elseif nLeftovers == 2 then
      Util.Streams.print("    " + String(spline.knots[nKnots - 1], significantDigits=15) + ", " + String(spline.knots[nKnots], significantDigits=15), fileName);
    elseif nLeftovers == 1 then
      Util.Streams.print("    " + String(spline.knots[nKnots], significantDigits=15), fileName);
    end if;
  end if;
  Util.Streams.print("    },", fileName);
  // print the control points
  // ncontrol = #controlPoints
  Util.Streams.print("    controlPoints = [", fileName);
  n := integer(spline.ncontrol / recordsPerLine);
  nLeftovers := spline.ncontrol - n * recordsPerLine;
  // Take care of the case when nLeftovers == 0, see the knot comment above for an explanation.
  if nLeftovers == 0 then
    for j in 1:n-1 loop
      Util.Streams.print("    " + String(spline.controlPoints[recordsPerLine * j - 3, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j - 2, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j - 1, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j, 1], significantDigits=15) + ";",  fileName);
    end for;
    Util.Streams.print("    " + String(spline.controlPoints[spline.ncontrol - 3, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol - 2, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol - 1, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol, 1], significantDigits=15),  fileName);
  else
    for j in 1:n loop
      Util.Streams.print("    " + String(spline.controlPoints[recordsPerLine * j - 3, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j - 2, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j - 1, 1], significantDigits=15) + "; " + String(spline.controlPoints[recordsPerLine * j, 1], significantDigits=15) + ";",  fileName);
    end for;
    if nLeftovers == 3 then
      Util.Streams.print("    " + String(spline.controlPoints[spline.ncontrol - 2, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol - 1, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol, 1], significantDigits=15),  fileName);
    elseif nLeftovers == 2 then
      Util.Streams.print("    " + String(spline.controlPoints[spline.ncontrol - 1, 1], significantDigits=15) + "; " + String(spline.controlPoints[spline.ncontrol, 1], significantDigits=15),  fileName);
    elseif nLeftovers == 1 then
      Util.Streams.print("    " + String(spline.controlPoints[spline.ncontrol, 1], significantDigits=15),  fileName);
    end if;
  end if;
  if lastSpline then
     Util.Streams.print("    ])", fileName);
  else
    Util.Streams.print("    ]),", fileName);
  end if;
  Util.Streams.print("end IF97_spline;",fileName);
  status := true;
  annotation ();
end dumpOneSpline;
