within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities;
model PhaseBoundary "Model used to create the phase boundary"

  parameter Integer npoints=100;
  Real p[npoints] "pressure";
  Real hl[npoints] "liquid specific enthalpy";
  Real hv[npoints] "vapour specific enthalpy";

  parameter Real TMAX=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.critical.TCRIT;
  //parameter Real TMAX=32.938;
  parameter Real TMIN=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.Ttriple
                                                                                  -0.01;
protected
  Real[npoints] T "temperature";

algorithm
  T :=
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.linspace(
    TMIN,
    TMAX,
    npoints);

  p[1] := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple;
  for i in 2:npoints-1 loop
    p[i] := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.psat(T[i]);
  end for;
  p[end] := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.critical.PCRIT;

  for i in 1:npoints loop
    hl[i] := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hl_p(p[i]);
    hv[i] := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hv_p(p[i]);
  end for;

  annotation (
    Icon(graphics));
end PhaseBoundary;
