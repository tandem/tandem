within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities;
function linspace "Returns a vector with linear spacing"
  input Real min;
  input Real max;
  input Integer npoints;

  output Real[npoints] res;

protected
  Real delta;
algorithm
  res[1] := min;
  res[end] := max;

  delta := (max - min)/npoints;

  for i in 2:npoints-1 loop
    res[i] := res[i-1] + delta;
  end for;

end linspace;
