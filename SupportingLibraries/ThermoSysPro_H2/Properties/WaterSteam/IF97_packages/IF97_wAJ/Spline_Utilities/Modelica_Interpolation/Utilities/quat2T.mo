within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function quat2T
  "Compute transformation matrix from non-consistent quaternions"
  extends Modelica.Icons.Function;
  input Real q[4] "Quaternions (non-consistent)";
  output Real T[3,3] "orthogonal transformation matrix";

algorithm
 T[1,1] := q[1]^2 + q[2]^2 - q[3]^2 - q[4]^2;
 T[2,2] := q[1]^2 - q[2]^2 + q[3]^2 - q[4]^2;
 T[3,3] := q[1]^2 - q[2]^2 - q[3]^2 + q[4]^2;
 T[1,2] := 2*(q[2]*q[3] + q[1]*q[4]);
 T[1,3] := 2*(q[2]*q[4] - q[1]*q[3]);
 T[2,1] := 2*(q[2]*q[3] - q[1]*q[4]);
 T[2,3] := 2*(q[3]*q[4] + q[1]*q[2]);
 T[3,1] := 2*(q[2]*q[4] + q[1]*q[3]);
 T[3,2] := 2*(q[3]*q[4] - q[1]*q[2]);
 //make the quaternions consistent
 T := T/(q*q);

end quat2T;
