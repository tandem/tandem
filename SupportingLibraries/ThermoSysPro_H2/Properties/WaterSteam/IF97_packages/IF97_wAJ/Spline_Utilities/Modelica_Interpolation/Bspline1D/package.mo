within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation;
package Bspline1D "1-dimensional Bspline interpolation"











annotation (
  Window(
    x=0.45,
    y=0.01,
    width=0.44,
    height=0.65,
    library=1,
    autolayout=1),
  Documentation(info="<HTML>
<p>
With this package 1-dimensional interpolation with B-Splines
is performed.
</p>
<p>
The following functions are supported:
</p>
<pre>
  init          Initialize interpolation
  initDer       Initialize interpolation (points and first derivatives are given)
  evaluate      Determine data at one point by interpolation
  evaluateDer   Determine data and first derivative at one point by interpolation
  evaluateDer2  Determine data, first and second derivative at one point by interpolation
  evaluateDerN  Determine the n-th derivative at one point by interpolation
  plot          Compute all data needed to make a nice plot of the data and plot it
                (since in Dymola plotArray cannot be called in a function, currently
                just the plot data is computed and returned. In a calling script
                this data can be used to plot it with operator plotArray).
</pre>
<p><b>Release Notes:</b></p>
<ul>
<li><i>Sept. 13, 2002</i>
       by Gerhard Schillhuber:<br>
       first version implemented
</li>
</ul>
<br>
<p><b>Copyright (C) 2002, Modelica Association and DLR.</b></p>
<p><i>
This package is <b>free</b> software. It can be redistributed and/or modified
under the terms of the <b>Modelica license</b>, see the license conditions
and the accompanying <b>disclaimer</b> in the documentation of package
Modelica in file \"Modelica/package.mo\".
</i></p>
</HTML>
"));
end Bspline1D;
