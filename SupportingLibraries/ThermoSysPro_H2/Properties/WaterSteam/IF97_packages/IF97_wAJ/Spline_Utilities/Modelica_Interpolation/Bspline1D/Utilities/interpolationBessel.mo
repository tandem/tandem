within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function interpolationBessel
  "Interpolation of the points with a Bspline of degree 3 and Bessel end condition"
//  import Modelica_Interpolation;
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  output Data Bspline(
    ndim=size(points, 2),
    ncontrol=(size(points, 1) + 2),
    degree=3) "Bspline in a form which can be quickly interpolated";
protected
  Integer ndim=size(points, 2);
  Real S[size(points,1)-2,size(points,1)-2];
  Real u;
  Real u2;
  Real nik[4];
  Real nik2[4];
  Integer degree;
  Real knots[size(points,1)+2*3];
  Integer n_data;
  Real alpha;
  Real beta;
  Real a[ndim];
  Real rs[        ndim];
  Real re[                 ndim];
  Real p_vec[Bspline.ncontrol-4,Bspline.ndim];
  Integer span;
algorithm
  /*
  Farin: Curves and Surfaces for CAGD (page 157)
  */
  degree := 3;

 n_data := size(points,1);

 //build the knots

 knots[1:degree] := ones(3)*param[1];
 knots[degree+1:n_data+degree] := param;
 knots[n_data+degree+1:n_data+2*degree] := ones(3)*param[n_data];

  Bspline.controlPoints := zeros(n_data+2,Bspline.ndim);
 Bspline.controlPoints[1,:] := points[1,:];
 Bspline.controlPoints[n_data+2,:] := points[n_data,:];

 alpha := (param[3]-param[2])/(param[3]-param[1]);
 beta := 1-alpha;
 a := (points[2,:] - (alpha^2)*points[1,:] - (beta^2)*points[3,:])/(2*alpha*beta);
 Bspline.controlPoints[2,:] := (2/3)*(alpha*points[1,:] + beta*a) + points[1,:]/3;

 alpha := (param[n_data-2]-param[n_data-1])/(param[n_data-2]-param[n_data]);
 beta := (1-alpha);
 a := (points[n_data-1,:] - (alpha^2)*points[n_data,:] - (beta^2)*points[n_data-2,:])/(2*alpha*beta);
 Bspline.controlPoints[n_data+1,:] := (2/3)*(alpha*points[n_data,:] + beta*a) + points[n_data,:]/3;

 // build the equation system
 S := zeros(n_data-2,n_data-2);

 u2 := param[2];
 span := n_findSpan(degree,u2,knots);
 nik2 := n_BasisFuns(span,u2,degree,knots);
 S[1,1:2] := nik2[2:3];
 rs := points[2,:] - Bspline.controlPoints[2,:]*nik2[1];

 for i in 1:n_data-4 loop
   u := param[i+2];
   span := n_findSpan(degree,u,knots);
  nik := n_BasisFuns(span,u,degree,knots);
   S[i+1,i:i+2] := nik[1:3];

 end for;
 u := param[n_data-1];
 span := n_findSpan(degree,u,knots);
 nik := n_BasisFuns(span,u,degree,knots);
 S[n_data-2,n_data-3:n_data-2] := nik[1:2];

 re := points[n_data-1,:] - Bspline.controlPoints[n_data+1,:]*nik[3];
 p_vec[1,:] := rs;
 p_vec[2:n_data-3,:] := points[3:n_data-2,:];
 p_vec[n_data-2,:] := re;

 // solve the equation system
  Bspline.controlPoints[3:n_data, :] :=
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities.solveMatrix(
    S, p_vec);

 Bspline.knots := knots;

end interpolationBessel;
