within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function interpolationder
  "Interpolation of the points and its first derivatives with a Bspline of degree n"
//  import Modelica_Interpolation;
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real derivs[size(points,1), size(points,2)]
    "derivs[i,:] is the derivative at points[i,:]";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  input Integer degree(min=2) = 3
    "Polynomial degree of interpolation";
  output Data spline(
    ndim=size(points, 2),
    ncontrol=2*size(points, 1),
    degree=degree)
    "Bspline in a form which can be quickly interpolated";
protected
  Integer nknots=size(spline.knots, 1);
  Integer ndim=size(points, 2);
  Integer npoints;
  Integer i;
  Integer k;
  Real S[2*size(points,1),2*size(points,1)];
  Real u;
  Real b[2*size(points,1),size(points, 2)];
  Integer span;
  Real N[2,degree+1];
algorithm
  /*
  The NURBS Book: Global Interpolation (page 373)

  */
  npoints := size(points,1);

 // compute the knots
 // the goal is a banded linear system

 k :=  integer(degree/2);
 spline.knots[1:degree+1] := ones(degree+1)*param[1];

 if degree == 2*k then
   //degree is even
   k := k-1;

   for j in 1:k loop
   spline.knots[degree+1+j] := param[j+1];

  end for;

  for j in 1:npoints-(k+1) loop
    spline.knots[degree+k+2*j] := (param[j+k]+param[j+k+1])/2;
    spline.knots[degree+k+2*j+1] := param[j+k+1];

  end for;

  for j in 1:k loop
   spline.knots[2*npoints-k+j] := param[npoints-k+j-1];

  end for;

 else
   //degree is odd

  for j in 1:k loop
   spline.knots[degree+1+j] := spline.knots[degree+1] + j*(param[k+1]-param[1])/k;

  end for;

  for j in 1:npoints-(k+1) loop
    spline.knots[degree+k+2*j] := (2*param[j+k]+param[j+k+1])/3;
    spline.knots[degree+k+2*j+1] := (param[j+k]+2*param[j+k+1])/3;

  end for;

  for j in 1:k loop
   spline.knots[2*npoints-k+j] := spline.knots[2*npoints-k] + j*(param[npoints]-param[npoints-k])/k;

  end for;

 end if;

 spline.knots[2*npoints+1:2*npoints+degree+1] := ones(degree+1)*param[npoints];

 // build the equation system
 S := zeros(2*npoints,2*npoints);

 for i in 1:npoints loop
   u := param[i];
   span := n_findSpan(spline.degree,u,spline.knots);
   N := Utilities.n_DersBasisFuns(span,u,spline.degree,1,spline.knots);
   S[2*i-1:2*i,span-spline.degree:span] := N;
   b[2*i-1,:] := points[i,:];
  b[2*i,:] := derivs[i,:];

 end for;

 // solve the equation system
  spline.controlPoints :=
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities.solveMatrix(
    S, b);
end interpolationder;
