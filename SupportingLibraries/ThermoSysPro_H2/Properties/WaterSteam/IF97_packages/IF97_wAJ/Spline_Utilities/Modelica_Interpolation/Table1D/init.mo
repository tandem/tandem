within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Table1D;
function init "Initialize 1-dim. table interpolation"
  extends Modelica.Icons.Function;
  input Real table[:, :] "[x, y1(x), y2(x), ..., yn(x)] data points";
  input Integer degree(min=1) = 1
    "Polynomial degree of interpolation";
  output Bspline1D.Data tableSpline(
    ndim=size(table, 2) - 1,
    ncontrol=size(table, 1),
    degree=degree)
    "Table data in a form which can be quickly interpolated";
protected
  Integer nknots=size(tableSpline.knots, 1);
  Integer ndim=size(table, 2);
algorithm
  //the first column is the parametrization
  tableSpline := Bspline1D.init(
              table[:, 2:ndim],
              table[:, 1],
              degree,
              false);
end init;
