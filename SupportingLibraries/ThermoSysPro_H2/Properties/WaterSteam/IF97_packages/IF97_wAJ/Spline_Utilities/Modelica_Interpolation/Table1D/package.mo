within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation;
package Table1D "Table interpolation in one dimension"




  annotation (
    Window(
      x=0.45,
      y=0.01,
      width=0.44,
      height=0.65,
      library=1,
      autolayout=1),
    Documentation(info="With this package interpolation with B-Splines
of 1-dim. tables is provided."));
end Table1D;
