within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function plot
  "Plot Bspline curve (currently not fully functional, since feature in Dymola missing)"
  extends Modelica.Icons.Function;
  input Data spline "Bspline to be plotted";
  input Integer npoints=100 "Number of points";
  output Real x[npoints,spline.ndim+1]
    "Table with u and function value at Bspline curve points";
algorithm

end plot;
