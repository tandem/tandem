within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function n_DersBasisFuns
  "Compute nonzero basis functions and their derivatives"
 extends Modelica.Icons.Function;
  input Integer i "index";
  input Real u "parameter";
  input Integer p "degree";
  input Integer n "n-th derivative";
  input Real knots[:] "knot vector";
  output Real ders[n+1,p+1] "ders[k,:] is (k-1)-th derivative";
protected
  Integer j;
  Integer r;
  Real left[p+1];
  Real right[p+1];
  Real temp;
  Real saved;
  Real ndu[p+1,p+1];
  Integer s1;
  Integer s2;
  Integer j1;
  Integer j2;
  Real a[2,p+1];
  Real d;
  Integer rk;
  Integer pk;
  Integer prod;
  Integer tt;
algorithm
 /*
  The NURBS Book: Algorithm A2.3 (page 72)
  */
  ndu[1,1] := 1;
  for j in 1:p loop
   left[j] := u - knots[i+1-j];
   right[j] := knots[i+j] - u;
   saved := 0.0;

   for r in 1:j loop
    ndu[j+1,r] := right[r]+left[j-r+1];
    temp := ndu[r,j]/ndu[j+1,r];
    ndu[r,j+1] := saved + right[r]*temp;
    saved := left[j-r+1]*temp;

  end for;
  ndu[j+1,j+1] := saved;
  end for;

  for j in 1:p+1 loop
   ders[1,j] := ndu[j,p+1];
  end for;

  for r in 1:p+1 loop
   s1 := 1;
   s2 := 2;
   a[1,1] := 1.0;

   for k in 1:n loop
    d := 0.0;
    rk := r-k-1;
    pk := p-k;

    if r-1>=k then
     a[s2,1] := a[s1,1]/ndu[pk+2,rk+1];
     d := a[s2,1]*ndu[rk+1,pk+1];

    end if;

    if rk >= -1 then
     j1 :=1;

    else
     j1 :=-rk;

    end if;

    if r-1 <= pk+1 then
     j2 := k-1;

    else
     j2 := p-r+1;

    end if;

    for j in j1:j2 loop
     a[s2,j+1] :=(a[s1, j + 1] - a[s1, j])/ndu[pk + 2, rk + j + 1];
     d :=d + a[s2, j + 1]*ndu[rk + j + 1, pk + 1];

    end for;

    if r-1 <= pk then
     a[s2,k+1] := -a[s1,k]/ndu[pk+2,r];
     d := d + a[s2,k+1]*ndu[r,pk+1];

    end if;

    ders[k+1,r] := d;
    tt := s1;
    s1 := s2;
    s2 := tt;

   end for;
  end for;

  prod := p;

 for k in 1:n loop

   for j in 1:p+1 loop
    ders[k+1,j] := ders[k+1,j]*prod;

   end for;
   prod := prod*(p-k);

 end for;

end n_DersBasisFuns;
