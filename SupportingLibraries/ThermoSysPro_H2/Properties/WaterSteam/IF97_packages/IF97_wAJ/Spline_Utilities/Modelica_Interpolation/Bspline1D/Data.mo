within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
record Data "Datastructure of a Bspline"
  parameter Integer ndim(min=1) "Number of dimensions of one control point";
  parameter Integer ncontrol(min=1) "Number of control points";
  parameter Integer degree(min=1) "Polynomial degree of the Bspline";
  Real knots[ncontrol+degree+1] "Knot vector of the Bspline";
  Real controlPoints[ncontrol,ndim] "[i,:] is data of control point i";
end Data;
