within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function solveMatrix
  "Solve linear system with right hand side matrix (similar to Modelica_Interpolation.Utilities.solveMatrix)"
  extends Modelica.Icons.Function;
  input Real A[:, size(A, 1)] "Matrix A of A*X = B";
  input Real B[size(A, 1), :] "Matrix B of A*X = B";
  output Real X[size(B, 1), size(B, 2)]=B
    "Matrix X such that A*X = B";
protected
  Integer info;
algorithm
  (X,info) := dgesv(A, B);
  assert(info == 0, "Solving a linear system of equations with function
\"Modelica_Interpolation.Utilities.solveMatrix\" is not possible, since matrix A
is singular, i.e., no unique solution exists.");
end solveMatrix;
