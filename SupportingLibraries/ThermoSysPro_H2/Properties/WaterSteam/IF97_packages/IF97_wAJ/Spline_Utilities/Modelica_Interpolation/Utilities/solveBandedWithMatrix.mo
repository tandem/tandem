within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function solveBandedWithMatrix
  "Solve linear system with banded system matrix and right hand side matrix (similar to Modelica.Matrices.solve)"
  extends Modelica.Icons.Function;
  input Integer kLower "Number of lower bands";
  input Integer kUpper "Number of upper bands";
  input Real A[2*kLower + kUpper + 1, :] "Matrix A of A*X = B";
  input Real B[size(A, 2), :] "Matrix B of A*X = B";
  output Real X[size(A, 2), size(B, 2)]=B
    "Matrix X such that A*X = B";
  output Integer info;
algorithm
  (X,info) := dgbsv(size(A,2), kLower, kUpper, A, B);
  assert(info == 0, "Solving a linear system of equations with function
\"Modelica_Interpolation.Utilities.solveBandedWithMatrix\" is not possible, since matrix A
is singular, i.e., no unique solution exists.");
end solveBandedWithMatrix;
