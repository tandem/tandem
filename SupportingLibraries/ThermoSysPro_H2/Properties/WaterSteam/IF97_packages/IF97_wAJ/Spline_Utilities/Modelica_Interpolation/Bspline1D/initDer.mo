within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function initDer
  "Initialize Bspline which interpolates the points and first derivatives"
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real derivs[size(points,1), size(points,2)]
    "derivs[i,:] is the derivative at points[i,:]";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  input Integer degree(min=2) = 3 "Polynomial degree of interpolation (max: number of points -1)
    at the moment degree=3 is supported";
  output Data spline(
    ndim=size(points, 2),
    ncontrol=2*size(points, 1),
    degree=degree)
    "Bspline in a form which can be quickly interpolated";
protected
  Integer nknots=size(spline.knots, 1);
  Integer ndim=size(points, 2);
algorithm
  spline := Utilities.interpolationder(
            points,
            derivs,
            param,
            degree);
end initDer;
