within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function curveLength "Computes the length of the curve from a to b"
  import
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation;
 extends Modelica.Icons.Function;
 input Modelica_Interpolation.Bspline1D.Data                                         spline
    "Bspline data";
 input Real a "left end";
 input Real b "right end";
 input Real err = 1e-8 "relative error";
 output Real I "curve length from a to b";

protected
  Real m;
  Real h;
  Real alpha;
  Real beta;
  Real x1 = 0.942882415695480;
  Real x2 = 0.641853342345781;
  Real x3 = 0.236383199662150;
 Real x[13];
 Real y[13];
 Real fa;
 Real fb;
 Real i1;
 Real i2;
 Real is;
 Real erri1;
 Real erri2;
 Real R;
 Real tol;
 Real eps = 1e-16;
 Integer s;

package internal "Funtions to be used only in function curveLength"
    import
      ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation;

 function quadStep "Recursive function used by curveLength"
  input ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Data
        spline;
  input Real a "right interval end";
  input Real b "left interval end";
  input Real fa "function value at a";
  input Real fb "function value at b";
  input Real is "first approximation of the integral";
  output Real I "Integral value";
    protected
  Real m;
  Real h;
  Real alpha;
  Real beta;
  Real x[5];
  Real y[5];
  Real mll;
  Real ml;
  Real mr;
  Real mrr;
  Real fmll;
  Real fml;
  Real fm;
  Real fmr;
  Real fmrr;
  Real i1;
  Real i2;
 algorithm
   h := (b-a)/2; m := (a+b)/2;
   alpha := sqrt(2/3); beta := 1/sqrt(5);
   mll := m-alpha*h; ml := m-beta*h; mr := m+beta*h; mrr := m+alpha*h;
   x := {mll,ml,m,mr,mrr};
   y := eval(spline,x);
   fmll := y[1]; fml := y[2]; fm := y[3]; fmr := y[4]; fmrr := y[5];
   i2 := (h/6)*(fa+fb+5*(fml+fmr));
   i1 := (h/1470)*(77*(fa+fb)+432*(fmll+fmrr)+625*(fml+fmr)
      +672*fm);

   if
     (is+(i1-i2)==is) or (mll<=a) or (b<=mrr) then
     I := i1;

   else
     I := quadStep(spline,a,mll,fa,fmll,is)+
       quadStep(spline,mll,ml,fmll,fml,is)+
       quadStep(spline,ml,m,fml,fm,is)+
       quadStep(spline,m,mr,fm,fmr,is)+
       quadStep(spline,mr,mrr,fmr,fmrr,is)+
       quadStep(spline,mrr,b,fmrr,fb,is);

   end if;
 end quadStep;

 function eval "evaluate the integrand"
   input Modelica_Interpolation.Bspline1D.Data spline "Bspline data";
   input Real u[:]
        "parameters at which the integrand shall be evaluated";
   output Real f[size(u,1)];
    protected
   Real xd[spline.ndim];
   Integer n;
 algorithm
   //the integrand is sqrt(xd*xd) (where xd is the first derivative)
  n := size(u,1);

  for i in 1:n loop
    xd := Modelica_Interpolation.Bspline1D.evaluateDerN(spline,u[i],1);
   f[i] := sqrt(xd*xd);

  end for;
 end eval;
end internal;

algorithm
 /*
        Numerically evaluate integral using adaptive
        Lobatto rule.
        see Walter Gander: Adaptive Quadrature - Revisited, 1998
                        ftp.inf.ethz.ch in doc/tech-reports/1998/306.ps

        x[:] are the nodes
        y[:] = f(x[:]) are function values at the nodes
                here (arc length computation): the integrand f is the
                absolute value of the first derivative of the curve (Bspline)
        */
 tol := err;
  m := (a+b)/2; h:=(b - a)/2;
  alpha := sqrt(2/3); beta:=1/sqrt(5);
  x := {a,m-x1*h,m-alpha*h,m-x2*h,m-beta*h,m-x3*h,m,m+x3*h,
        m+beta*h,m+x2*h,m+alpha*h,m+x1*h,b};
  y := internal.eval(spline,x);
  fa := y[1]; fb := y[13];
  i2 := (h/6)*(y[1]+y[13]+5*(y[5]+y[9]));
  i1 := (h/1470)*(77*(y[1]+y[13])+432*(y[3]+y[11])+
     625*(y[5]+y[9])+672*y[7]);
  is := h*(0.0158271919734802*(y[1]+y[13])+0.0942738402188500
     *(y[2]+y[12])+0.155071987336585*(y[3]+y[11])+0.188821573960182
                     *(y[4]+y[10])+0.199773405226859
     *(y[5]+y[9])+0.224926465333340*(y[6]+y[8])
     +0.242611071901408*y[7]);
  s := sign(is);
  if
    (s==0) then s := 1; end if;
  erri1 := abs(i1-is);
  erri2 := abs(i2-is);
  R := 1;
  if
    (erri2<>0) then R := erri1/erri2; end if;
  if
    (R>0 and R<1) then tol := tol/R; end if;
  is := s*abs(is)*tol/eps;
  if
    (is==0) then is := b-a; end if;
  I := internal.quadStep(spline,a,b,fa,fb,is);

end curveLength;
