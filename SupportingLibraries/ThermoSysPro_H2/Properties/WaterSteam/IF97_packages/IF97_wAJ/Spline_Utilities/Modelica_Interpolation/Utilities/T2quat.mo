within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function T2quat "Compute Quaternions from a transformation matrix"
  extends Modelica.Icons.Function;
  input Real T[3,3] "transformation matrix";
  output Real q[4] "Quaternions of T (q and -q have same T)";

protected
  Real branch "only for test purposes";
  Real paux;
  Real paux4;
  Real c1;
  Real c2;
  Real c3;
  Real c4;
  Real p4limit=0.1;
  Real c4limit=4*p4limit*p4limit;

algorithm
 c1 := 1 + T[1,1] - T[2,2] - T[3,3];
 c2 := 1 + T[2,2] - T[1,1] - T[3,3];
 c3 := 1 + T[3,3] - T[1,1] - T[2,2];
 c4 := 1 + T[1,1] + T[2,2] + T[3,3];

 if (c4 > c4limit) or (c4 > c1 and c4 > c2 and c4 > c3) then
    branch := 4;
    paux := sqrt(c4)/2;
    paux4 := 4*paux;
    q := {paux,
         (T[2,3] - T[3,2])/paux4,
         (T[3,1] - T[1,3])/paux4,
         (T[1,2] - T[2,1])/paux4};

 elseif c1 > c2 and c1 > c3 and c1 > c4 then
    branch := 1;
    paux := sqrt(c1)/2;
    paux4 := 4*paux;
    q := {(T[2,3] - T[3,2])/paux4,
         paux,
         (T[1,2] + T[2,1])/paux4,
         (T[1,3] + T[3,1])/paux4};

 elseif c2 > c1 and c2 > c3 and c2 > c4 then
    branch := 2;
    paux := sqrt(c2)/2;
    paux4 := 4*paux;
    q := {(T[3,1] - T[1,3])/paux4,
         (T[1,2] + T[2,1])/paux4,
         paux,
         (T[2,3] + T[3,2])/paux4};

 else
    branch := 3;
    paux := sqrt(c3)/2;
    paux4 := 4*paux;
    q := {(T[1,2] - T[2,1])/paux4,
         (T[1,3] + T[3,1])/paux4,
         (T[2,3] + T[3,2])/paux4,
         paux};

 end if;

end T2quat;
