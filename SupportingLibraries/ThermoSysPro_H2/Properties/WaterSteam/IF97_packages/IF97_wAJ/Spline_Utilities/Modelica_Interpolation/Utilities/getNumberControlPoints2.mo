within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function getNumberControlPoints2
  "Return the number of control points"
 extends Modelica.Icons.Function;
  input Real r[:, :]
    "r[i,:] is position vector to point i on the curve";
  input Real T[size(r, 1), 3, 3]
    "T[i,:,:] is transformation matrix from base frame to path frame at point i";
  input Integer degree "degree of the Bspline";
  output Integer ncontrol "number of control points";
protected
 Integer n;
  Real data[size(r,1),7];
  Real q[4];
  Real q_old[     4];
algorithm
 n := size(r,1);

 data := zeros(size(r,1),7);
  data[:,1:3] := r;

  // T2Quat
  // Get the quaternions of the transformation matrix
  // Choose the quaternions (+q or -q) which form the smallest angle with
  // the previous quaternions
  for i in 1:size(r,1) loop
    q := T2quat(T[i, :, :]);

   if (i > 1 and (q_old*q) < 0) then
      q := -q;

  end if;
  data[i,4:7] := q;
  q_old := q;
  end for;

  ncontrol := getNumberControlPoints(data,degree);

end getNumberControlPoints2;
