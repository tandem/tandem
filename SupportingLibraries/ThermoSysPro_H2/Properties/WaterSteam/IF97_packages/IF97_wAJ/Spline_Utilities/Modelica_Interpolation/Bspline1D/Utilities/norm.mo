within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function norm "The euklidian norm of a vector"
 extends Modelica.Icons.Function;
  input Real v[:] "A vector";
  output Real n "The norm of the vector";
algorithm
  n:=sqrt(v*v);
end norm;
