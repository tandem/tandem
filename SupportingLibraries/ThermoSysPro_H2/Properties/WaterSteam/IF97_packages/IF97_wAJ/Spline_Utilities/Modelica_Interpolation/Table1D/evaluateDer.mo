within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Table1D;
function evaluateDer
   "Evaluate Table data and first derivative at one parameter"
   extends Modelica.Icons.Function;
   input
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Data
    tableSpline "Bspline table to be evaluated";
   input Real x
     "Parameter value at which the table shall be evaluated";
   output Real y[tableSpline.ndim] "Value of the table at x";
   output Real yd[tableSpline.ndim]
     "Value of the first derivative at x";
algorithm
   (y,yd) := Bspline1D.evaluateDer(tableSpline, x);
end evaluateDer;
