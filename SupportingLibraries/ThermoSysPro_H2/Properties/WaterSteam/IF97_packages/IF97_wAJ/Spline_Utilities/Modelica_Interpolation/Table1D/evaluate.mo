within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Table1D;
function evaluate "Evaluate Table data at one parameter"
  extends Modelica.Icons.Function;
  input Bspline1D.Data tableSpline "Bspline table to be evaluated";
  input Real x "Parameter value at which table shall be evaluated";
  output Real y[tableSpline.ndim] "Value of table at x";
algorithm
  y := Bspline1D.evaluate(tableSpline, x);
end evaluate;
