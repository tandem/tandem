within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation;
package Utilities "Utility functions for package Interpolation"










annotation (
  Window(
    x=0.45,
    y=0.01,
    width=0.44,
    height=0.65,
    library=1,
    autolayout=1),
  Documentation(info="<HTML>
<p>
Utility functions are provided here which are usually not called directly
by a user, but are needed in the functions of this package
</p>
<p>
The following functions are supported:
</p>
<pre>
  curveLength                          Compute the length of a curve with adaptive quadrature
  dummy                                return a dummy Bspline with zero entries
  getNumberControlPoints        Compute the number of control points for the given data points
  getNumberControlPoints2        Compute the number of control points for the given data points
                                                                                                  and transformation matrices
  quat2T                               Compute the transformation matrix of the given quaternions
  T2quat                               Compute the quaternions of the given transformation matrix
</pre>
<p><b>Release Notes:</b></p>
<ul>
<li><i>Sept. 13, 2002</i>
       by Gerhard Schillhuber:<br>
       first version implemented
</li>
<li><i>Oct. 17, 2002</i>
       by Gerhard Schillhuber:<br>
       new functions: getNumberControlPoints, getNumberControlPoints2
                               compute the number of control points for the given data points. It's needed
                               to initialize the curve.
</li>
</ul>
<br>
<p><b>Copyright (C) 2002, Modelica Association and DLR.</b></p>
<p><i>
This package is <b>free</b> software. It can be redistributed and/or modified
under the terms of the <b>Modelica license</b>, see the license conditions
and the accompanying <b>disclaimer</b> in the documentation of package
Modelica in file \"Modelica/package.mo\".
</i></p>
</HTML>
"));
end Utilities;
