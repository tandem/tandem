within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function getNumberControlPoints "Return the number of control points"
 extends Modelica.Icons.Function;
  input Real r[:, :]
    "r[i,:] is position vector to point i on the curve";
  input Integer degree "degree of the Bspline";
  output Integer ncontrol "number of control points";
protected
  Integer n;
  Integer multi;
  Integer begin;
  Real delta[size(r,2)];
  Integer j;
  Integer jstart;
algorithm
 n := size(r,1);

 //delete multiple start data points
 multi := 1;

 for j in 1:n-1 loop
  delta := r[1,:] - r[1+j,:];

  if sqrt(delta*delta) < 1e-12 then
   multi := multi + 1;

  end if;

 end for;
 begin := multi;

 //delete multiple end data points
 multi := 0;

 for j in 1:n-1 loop
  delta := r[n,:] - r[n-j,:];

  if sqrt(delta*delta) < 1e-12 then
   multi := multi + 1;

  end if;

 end for;
 n := n-multi;

 ncontrol := n;
 jstart := begin;

 for j in begin+1:n-1 loop
   delta := r[j,:] - r[j-1,:];

   if sqrt(delta*delta) < 1e-12 then

     if j-1-jstart >= 1 and j-1-jstart < degree then
       //an interpolation with a Bspline of degree = p needs
       //p+1 data points. If there are less than p+1, insert
       //virtual data points to get p+1.
     ncontrol := ncontrol + degree - (j-1 - jstart);

    end if;
    jstart := j;
    delta := r[j+1,:] - r[j,:];

    if sqrt(delta*delta) < 1e-12 then
     //triple data point; delete one data point
     ncontrol := ncontrol - 1;
     jstart := j+1;

    end if;

   end if;
  end for;

  //end
  if n-jstart < degree then
   ncontrol := ncontrol + degree - (n-jstart);
  end if;

 //multiple start data points
 ncontrol := ncontrol - begin + 1;

 //if Bessel and degree==3 then size(r, 1)+2 else size(r, 1)

end getNumberControlPoints;
