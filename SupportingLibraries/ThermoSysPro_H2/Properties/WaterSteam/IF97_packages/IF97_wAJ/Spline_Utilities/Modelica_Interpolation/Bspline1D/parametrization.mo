within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function parametrization
  "Automatic parameterization of a Bspline in the range 0..1 (if ndim > 1)"
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Integer paramType=ParametrizationType.Centripetal
    "type of parametrization";
  output Real param[size(points, 1)]
    "parametrization of the data points";

protected
Real pi=3.141592653589;
Integer i;
Integer n_points;
Real d;
Real d_1[size(points, 2)];
Real d_2[size(points, 2)];
Real d_3[size(points, 2)];
Real phi_1;
Real phi_2;
Real lambda;
Real nu;
algorithm
//The following types of parametrization are described in the book of
//Gerald Farin, Curves and Surfaces in CAGD (page 161 'Finding a Knot
//Sequence'). For a comparison of the different parametrizations see
//the diploma thesis of Gerhard Schillhuber: 'Geometrische Modellierung
//oszillationsarmer Trajektorien von Industrierobotern (TU München)'
//(only available in german)
n_points := size(points,1);
  if paramType == ParametrizationType.Equidistant then
  //---EQUIDISTANT---
  //the parameters are choosen equidistant
    param :=
      ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.linspace(
      0,
      1,
      size(points, 1));

  elseif paramType == ParametrizationType.ChordLength then
   param[1] := 0;
  //---CHORD-LENGTH---
  //the parameters are choosen proportional to the distances of the points
    for i in 2:n_points loop
      d := Utilities.norm(points[i,:] - points[i-1,:]);
      param[i] := param[i-1]+d;
    end for;
    param := param / param[n_points];

  elseif paramType == ParametrizationType.Centripetal then
   param[1] := 0;
  //---CENTRIPETAL---
  //the idea of the centriptal parametrization is to minimize the centripetal
  //force of an object moving on the curve.
    for i in 2:n_points loop
      d := Utilities.norm(points[i,:] - points[i-1,:]);
      param[i] := param[i-1]+sqrt(d);
    end for;
    param := param / param[n_points];

  elseif paramType == ParametrizationType.Foley then
   param[1] := 0;
  //---FOLEY---
  //for computing the parameter the distances and the angle formed by the points
  //are needed.

    d_2 := (points[2,:] - points[1,:]);
    d_3 := (points[3,:] - points[2,:]);
    phi_2 := min(pi - acos((d_2 * d_3) / (Utilities.norm(d_2)*Utilities.norm(d_3))),pi/2);
    d := Utilities.norm(d_2)*(1 +
        3/2 * phi_2 * Utilities.norm(d_3) / (Utilities.norm(d_2)+Utilities.norm(d_3)));
    param[2] := param[1]+d;

    for i in 3:n_points-1 loop
      d_1 := (points[i-1,:] - points[i-2,:]);
      d_2 := (points[i,:] - points[i-1,:]);
      d_3 := (points[i+1,:] - points[i,:]);
      phi_1 := min(pi - acos((d_1 * d_2) / (Utilities.norm(d_1)*Utilities.norm(d_2))),pi/2);
      phi_2 := min(pi - acos((d_2 * d_3) / (Utilities.norm(d_2)*Utilities.norm(d_3))),pi/2);
      d := Utilities.norm(d_2)*(1 +
          3/2 * phi_1 * Utilities.norm(d_1) / (Utilities.norm(d_1)+Utilities.norm(d_2)) +
          3/2 * phi_2 * Utilities.norm(d_3) / (Utilities.norm(d_2)+Utilities.norm(d_3)));

      param[i] := param[i-1]+d;
    end for;

    d_1 := (points[n_points-1,:] - points[n_points-2,:]);
    d_2 := (points[n_points,:] - points[n_points-1,:]);
    phi_1 := min(pi - acos((d_1 * d_2) / (Utilities.norm(d_1)*Utilities.norm(d_2))),pi/2);
    d := Utilities.norm(d_2)*(1 +
        3/2 * phi_1 * Utilities.norm(d_1) / (Utilities.norm(d_1)+Utilities.norm(d_2)));
    param[n_points] := param[n_points-1]+d;

    param := param / param[n_points];

  elseif paramType == ParametrizationType.Angular then
   param[1] := 0;
  //---ANGULAR---
  //this param. is like the Foley parametrization (distances and the angle
  //are needed).
   lambda := 1.5;
    for i in 2:n_points-1 loop
      d_1 := (points[i,:] - points[i-1,:]);
      d_2 := (points[i+1,:] - points[i,:]);
      phi_1 := acos((d_1 * d_2) / (Utilities.norm(d_1)*Utilities.norm(d_2)));

      d := sqrt(Utilities.norm(d_1))*(1 + lambda*phi_1/pi);
      param[i] := param[i-1]+d;
    end for;
    d_1 := (points[n_points-2,:] - points[n_points-1,:]);
    d_2 := (points[n_points-1,:] - points[n_points,:]);
    phi_1 := acos((d_2 * d_1) / (Utilities.norm(d_1)*Utilities.norm(d_2)));
    d := sqrt(Utilities.norm(d_2))*(1 + lambda*phi_1/pi);
    param[n_points] := param[n_points-1]+d;

    param := param / param[n_points];

  elseif paramType == ParametrizationType.AreaBased then
   lambda := 2/3;
  //---AREA-BASED---
  //the parameters are choosen proportional to the area of the parallelograms
  //formed by the points.
    nu := 0.3;
    param[1] := 0;
    d_2 := (points[2,:] - points[1,:]);
    d_3 := (points[3,:] - points[2,:]);
    phi_2 := max(acos((-d_2 * d_3) / (Utilities.norm(d_2)*Utilities.norm(d_3))),pi/2);
    d := nu*Utilities.norm(d_2) + (1-nu)*(sin(phi_2)*Utilities.norm(d_3)) / Utilities.norm(d_3);
    param[2] := param[1]+d;

    for i in 3:n_points-1 loop
      d_1 := (points[i-1,:] - points[i-2,:]);
      d_2 := (points[i,:] - points[i-1,:]);
      d_3 := (points[i+1,:] - points[i,:]);
      phi_1 := max(acos((-d_1 * d_2) / (Utilities.norm(d_1)*Utilities.norm(d_2))),pi/2);
      phi_2 := max(acos((-d_2 * d_3) / (Utilities.norm(d_2)*Utilities.norm(d_3))),pi/2);

      d := lambda*Utilities.norm(d_2) +
          (1-lambda)*(sin(phi_1)*Utilities.norm(d_1)+sin(phi_2)*Utilities.norm(d_3)) /
            (Utilities.norm(d_1)+Utilities.norm(d_3));
      param[i] := param[i-1]+d;
    end for;
    d_1 := (points[n_points-1,:] - points[n_points-2,:]);
    d_2 := (points[n_points,:] - points[n_points-1,:]);
    phi_1 := max(acos((-d_1 * d_2) / (Utilities.norm(d_1)*Utilities.norm(d_2))),pi/2);
    d := nu*Utilities.norm(d_2) + (1-nu)*(sin(phi_1)*Utilities.norm(d_1)) / Utilities.norm(d_1);
    param[n_points] := param[n_points-1]+d;

    param := param / param[n_points];

  end if;
end parametrization;
