within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function evaluateDerN
  "Evaluate k-th derivative of Bspline at one parameter"
  extends Modelica.Icons.Function;
  input Data spline "Bspline to be evaluated";
  input Real u "Parameter value at which Bspline shall be evaluated";
  input Integer k(min=0)
    "Differentation order (0: function value, 1: first derivative, ...)";
  output Real x_derN[spline.ndim] "k-th derivative of Bspline at u";

protected
 Integer span;
 Real N[k+1,spline.degree+1];
algorithm
  x_derN := zeros(spline.ndim);

 span := Utilities.n_findSpan(spline.degree,u,spline.knots);
  N := Utilities.n_DersBasisFuns(span,u,spline.degree,k,spline.knots);

  for i in 1:spline.ndim loop
   x_derN[i] := N[k+1,:]*spline.controlPoints[span-spline.degree:span,i];
  end for;

end evaluateDerN;
