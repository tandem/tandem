within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities;
function dummy "Dummy Bspline"
  // import Modelica_Interpolation;
 extends Modelica.Icons.Function;
  input Integer nd "Dimension";
  input Integer nc "Number of control points";
  input Integer deg "degree";
  output
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Data
    spline(
    ndim=nd,
    ncontrol=nc,
    degree=deg) "A dummy Bspline with zero entries";
protected
  Integer j;
algorithm
  j:=1;
end dummy;
