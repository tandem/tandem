within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function interpolation_bandmatrix "Interpolation of the points with a Bspline of degree n. Do NOT return a Bspline struct.
  Return the the raw information of control points and knots."
  import U =
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  input Integer degree(min=1) = 3
    "Polynomial degree of interpolation";
  output Real controlPoints[size(points, 1),size(points, 2)]
    "Control points";
  output Real knots[size(points, 1)+degree+1] "knots";
protected
  Integer nknots=size(knots, 1);
  Integer ndim=size(points, 2);
  Integer npoints;
  Integer knots_tech;
  //Real S[size(points,1),size(points,1)];

  Real u;
  Integer span;
  Real evalBasisFuns[degree+1];
  Real Band[3*degree+1,size(points,1)];
  Integer kl = degree;
  Integer ku = degree;
  Integer info=0;
  String sout;
algorithm
  /*
  The NURBS Book: Global Interpolation (page 364)
  */
  npoints := size(points,1);

 // build the knots
 //     1:  knots = parameter (for testing)
 //     2:  knots are built with averaging
 //     3:  knots equidistant (for testing)
 knots_tech := 2;

 if knots_tech == 1 then
    knots[1:degree] := ones(degree)*param[1];
    knots[degree+1:npoints+degree] := param;
    knots[npoints+degree+1:npoints+2*degree] := ones(degree)*param[npoints];

 end if;

 if knots_tech == 2 then
 // with averaging to avoid a singularity system of equation
    knots[1:degree+1] := ones(degree+1)*param[1];
    for j in 1:npoints-degree-1 loop
       knots[degree+j+1] := sum(param[j+1:j+degree])/degree;
    end for;
    knots[npoints+1:npoints+degree+1] := ones(degree+1)*param[npoints];

 end if;

 if knots_tech == 3 then
    knots[1:degree+1] := ones(degree+1)*param[1];
    for j in 1:npoints-1 loop
       knots[degree+j+1] := j/(npoints-1);
    end for;
    knots[npoints+degree+1:npoints+2*degree] := ones(degree)*param[npoints];

 end if;

 // build the equation system
// S := zeros(npoints,npoints);

 for i in 1:npoints loop
   u := param[i];
   span := U.n_findSpan(degree,u,knots);
   //S[i,span-degree:span] := n_BasisFuns(span,u,degree,knots);
   evalBasisFuns := U.n_BasisFuns(span,u,degree,knots);

   for j in 1:degree+1 loop
     Band[kl+1+ku+i-span+degree-j+1,span-degree+j-1]:=evalBasisFuns[j];
//Band[kl+1+ku+i-span+degree-j+1,span-degree+j-1]:=10*i+span-degree-1+j;

   end for;

 end for;

/*
for i in 1:npoints loop
    sout :="i = " + String(i) + ":";
    for j in 1:degree+1 loop
      sout :=sout + "   " + String(Band[j, i]);
    end for;
    Utilities.ModelicaMessage(sout + "\n");
end for;
*/

 // solve the equation system
  (controlPoints,info) :=
    ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Utilities.solveBandedWithMatrix(
    kl,
    ku,
    Band,
    points);
 // assert(info == 0, "Error when computing spline coefficients");

if info <> 0 then
  // if solving with band matrix is not succesful, use full matrix
  (controlPoints,knots) := U.interpolation_raw(points,param,degree);

end if;

end interpolation_bandmatrix;
