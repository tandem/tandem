within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities;
function interpolation
  "Interpolation of the points with a Bspline of degree n"
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  input Integer degree(min=1) = 3
    "Polynomial degree of interpolation";
  output Data spline(
    ndim=size(points, 2),
    ncontrol=size(points, 1),
    degree=degree)
    "Bspline in a form which can be quickly interpolated";
protected
  Real ctrlp[spline.ncontrol,spline.ndim];
  Real k[spline.ncontrol+degree+1];
algorithm

 //old
 //(ctrlp,k) := interpolation_raw(points,param,degree);
 (ctrlp,k) := interpolation_bandmatrix(points,param,degree);

 spline.controlPoints := ctrlp;
 spline.knots := k;

end interpolation;
