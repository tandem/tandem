within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function init
  "Initialize Bspline (end conditions are automatically selected, see docu)"
  extends Modelica.Icons.Function;
  input Real points[:, :]
    "[i,:] is point i on the curve to be interpolated";
  input Real param[size(points, 1)]
    "parameterization of the data points (not necessarily in the range 0..1)";
  input Integer degree(min=1) = 3
    "Polynomial degree of interpolation (max number of points -1)";
  input Boolean Bessel = true
    "If true and degree=3, Bessel end condition is used";
  output Data spline(
    ndim=size(points, 2),
    ncontrol=if Bessel and degree == 3 then size(points, 1) + 2 else
        size(points, 1),
    degree=degree)
    "Bspline in a form which can be quickly interpolated";
protected
  Integer nknots=size(spline.knots, 1);
  Integer ndim=size(points, 2);
algorithm
  if (degree == 3 and Bessel) then
   //Interpolation with Bessel end-conditions
    spline :=
      ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities.interpolationBessel(
      points, param);
  else
    //Interpolation without specifying the end-conditions
    spline :=
      ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D.Utilities.interpolation(
      points,
      param,
      degree);
  end if;
end init;
