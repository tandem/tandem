within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
function evaluate "Evaluate Bspline at one parameter"
  extends Modelica.Icons.Function;
  input Data spline "Bspline to be evaluated";
  input Real u "Parameter value at which Bspline shall be evaluated";
  output Real x[spline.ndim] "Value of Bspline at u";

protected
 Integer span;
 Real N[spline.degree+1];
algorithm
  x := zeros(spline.ndim);
 span := Utilities.n_findSpan(spline.degree,u,spline.knots);
  N := Utilities.n_BasisFuns(span,u,spline.degree,spline.knots);

  for i in 1:spline.ndim loop
   x[i] := N*spline.controlPoints[span-spline.degree:span,i];
  end for;
end evaluate;
