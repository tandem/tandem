within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
record ParametrizationType "will be later replaced by enumeration"
  constant Integer Equidistant=1 "not recommended";
  constant Integer ChordLength=2;
  constant Integer Centripetal=3 "recommended";
  constant Integer Foley=4;
  constant Integer Angular=5;
  constant Integer AreaBased=6;
end ParametrizationType;
