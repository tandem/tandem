within ThermoSysPro_H2.Properties.WaterSteam.IF97_packages.IF97_wAJ.Spline_Utilities.Modelica_Interpolation.Bspline1D;
package Utilities "Utility functions for package Bspline1D"









annotation (
  Window(
    x=0.45,
    y=0.01,
    width=0.44,
    height=0.65,
    library=1,
    autolayout=1),
  Documentation(info="<HTML>
<p>
Utility functions are provided here which are usually not called directly
by a user, but are needed in the functions of this package
</p>
<p>
The following functions are supported:
</p>
<pre>
  interpolation          Compute the interpolation Bspline
  interpolation_raw      Compute the interpolation Bspline, but only return the control points
                                                                                           and the knots
  interpolationBessel    Compute the interpolation Bspline with Bessel end-conditions
  interpolationDer       Compute the interpolation Bspline when the first derivative are given
  n_BasisFuns            Compute the non zero basis functions of the Bspline
  n_DersBasisFuns        Compute the non zero basis functions and their derivatives
  n_findSpan             Compute the interval in which the parameter lies
  norm                   The euklidian norm of a vector
</pre>
<p><b>Release Notes:</b></p>
<ul>
<li><i>Sept. 13, 2002</i>
       by Gerhard Schillhuber:<br>
       first version implemented
</li>
<li><i>Oct. 17, 2002</i>
       by Gerhard Schillhuber:<br>
       new function: interpolation_raw. return only the control points and the knots.
                       'interpolation' calls 'interpolation_raw'.
</li>
</ul>
<br>
<p><b>Copyright (C) 2002, Modelica Association and DLR.</b></p>
<p><i>
This package is <b>free</b> software. It can be redistributed and/or modified
under the terms of the <b>Modelica license</b>, see the license conditions
and the accompanying <b>disclaimer</b> in the documentation of package
Modelica in file \"Modelica/package.mo\".
</i></p>
</HTML>
"));
end Utilities;
