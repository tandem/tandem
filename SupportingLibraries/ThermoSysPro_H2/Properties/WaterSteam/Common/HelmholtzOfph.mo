within ThermoSysPro_H2.Properties.WaterSteam.Common;
function HelmholtzOfph

protected
  Units.SI.SpecificHeatCapacity cv;
public
  input HelmholtzDerivs f
    "Dérivées adimensionnelles de la fonction de Helmholtz"
    annotation (Placement(transformation(extent={{-85,15},{-15,85}}, rotation=
           0)));
  input HelmholtzData dTR annotation (Placement(transformation(extent={{15,15},
            {85,85}}, rotation=0)));
  output NewtonDerivatives_ph nderivs annotation (Placement(transformation(
          extent={{-85,-85},{-15,-15}}, rotation=0)));
algorithm
  cv := -dTR.R*(f.tau*f.tau*f.ftautau);
  nderivs.p := dTR.d*dTR.R*dTR.T*f.delta*f.fdelta;
  nderivs.h := dTR.R*dTR.T*(f.tau*f.ftau + f.delta*f.fdelta);
  nderivs.pd := dTR.R*dTR.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  nderivs.pt := dTR.R*dTR.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  nderivs.ht := cv + nderivs.pt/dTR.d;
  nderivs.hd := (nderivs.pd - dTR.T*nderivs.pt/dTR.d)/dTR.d;
  annotation (
    Window(
      x=0.09,
      y=0.21,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end HelmholtzOfph;
