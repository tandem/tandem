within ThermoSysPro_H2.Properties.WaterSteam.Common;
record PhaseBoundaryProperties
  "thermodynamic base properties on the phase boundary"
  extends Modelica.Icons.Record;
  Units.SI.Density d "density";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificEnergy u "inner energy";
  Units.SI.SpecificEntropy s "specific entropy";
  Units.SI.SpecificHeatCapacity cp "heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "heat capacity at constant volume";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
end PhaseBoundaryProperties;
