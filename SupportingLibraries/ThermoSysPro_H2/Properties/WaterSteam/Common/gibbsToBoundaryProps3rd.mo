within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsToBoundaryProps3rd
  "calulate phase boundary property record from dimensionless Gibbs function"
  extends Modelica.Icons.Function;
  input ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless derivatives of Gibbs function";
  output
    ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd sat
    "phase boundary properties";
protected
  Real v "specific volume";
  Real vp3 "Third power of vp";
  Real ivp3 "Inverse of third power of vp";
algorithm
  sat.d := g.p/(g.R*g.T*g.pi*g.gpi);
  sat.h := g.R*g.T*g.tau*g.gtau;
  sat.u := g.T*g.R*(g.tau*g.gtau - g.pi*g.gpi);
  sat.s := g.R*(g.tau*g.gtau - g.g);
  sat.cp := -g.R*g.tau*g.tau*g.gtautau;
  sat.cv := g.R*(-g.tau*g.tau*g.gtautau + (g.gpi - g.tau*g.gpitau)*(g.gpi - g.
     tau*g.gpitau)/(g.gpipi));
  sat.pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
  sat.pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
  v := 1/sat.d;
  sat.vt := g.R/g.p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
  sat.vp := g.R*g.T/(g.p*g.p)*g.pi*g.pi*g.gpipi;
  sat.vtt := g.R*g.pi/g.p*g.tau/g.T*g.tau*g.gpitautau;
  sat.vtp := g.R*g.pi*g.pi/(g.p*g.p)*(g.gpipi - g.tau*g.gpipitau);
  sat.vpp := g.R*g.T*g.pi*g.pi*g.pi/(g.p*g.p*g.p)*g.gpipipi;
  sat.cpt := g.R*g.tau*g.tau/g.T*(2*g.gtautau + g.tau*g.gtautautau);
  vp3 := sat.vp*sat.vp*sat.vp;
  ivp3 := 1/vp3;
  sat.ptt := -(sat.vtt*sat.vp*sat.vp -2.0*sat.vt*sat.vtp*sat.vp + sat.vt*sat.vt*sat.vpp)*ivp3;
  sat.pdd := -sat.vpp*ivp3*v*v*v*v - 2*v*sat.pd "= pvv/d^4";
  sat.ptd := (sat.vtp*sat.vp-sat.vt*sat.vpp)*ivp3/(sat.d*sat.d) "= -ptv/d^2";
  sat.cvt := (vp3*sat.cpt + sat.vp*sat.vp*sat.vt*sat.vt + 3.0*sat.vp*sat.vp*g.T*sat.vt*sat.vtt - 3.0*sat.vtp*sat.vp*g.T*sat.vt*sat.vt + g.T*sat.vt*sat.vt*sat.vt*sat.vpp)*ivp3;
end gibbsToBoundaryProps3rd;
