within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsToProps_ph
  input GibbsDerivs g "dimensionless derivatives of the Gibbs function";
  output ThermoProperties_ph pro;
protected
  Real vt;
  Real vp;
algorithm
  pro.T := min(max(g.T, InitLimits.TMIN), InitLimits.TMAX);
  pro.d := max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    /(g.R*pro.T*g.pi*g.gpi);
  pro.u := g.T*g.R*(g.tau*g.gtau - g.pi*g.gpi);
  pro.s := g.R*(g.tau*g.gtau - g.g);
  pro.cp := -g.R*g.tau*g.tau*g.gtautau;
  vt := g.R/max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    *(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    *max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple))*g.pi
    *g.pi*g.gpipi;
  pro.ddhp := -pro.d*pro.d*vt/(pro.cp);
  pro.ddph := -pro.d*pro.d*(vp*pro.cp - vt/pro.d + g.T*vt*vt)/pro.cp;
  pro.duph := -1/pro.d + g.p/(pro.d*pro.d)*pro.ddph;
  pro.duhp := 1 + g.p/(pro.d*pro.d)*pro.ddhp;
  annotation (
    Window(
      x=0.05,
      y=0.05,
      width=0.54,
      height=0.72),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-132,102},{144,42}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"), DymolaStoredErrors,
    Diagram(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics));
end gibbsToProps_ph;
