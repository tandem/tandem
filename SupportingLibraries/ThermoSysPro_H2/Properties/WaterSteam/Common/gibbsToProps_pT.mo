within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsToProps_pT
  input GibbsDerivs g "dimensionless derivatives of the Gibbs funciton";
  output ThermoProperties_pT pro;
protected
  Real vt;
  Real vp;

algorithm
  pro.d := max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    /(g.R*g.T*g.pi*g.gpi);
  pro.u := g.T*g.R*(g.tau*g.gtau - g.pi*g.gpi);
  pro.h := g.R*g.T*g.tau*g.gtau;
  pro.s := g.R*(g.tau*g.gtau - g.g);
  pro.cp := -g.R*g.tau*g.tau*g.gtautau;
  vt := g.R/max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    *(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
    *max(g.p, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple))*g.pi
    *g.pi*g.gpipi;
  pro.ddpT := -(pro.d*pro.d)*vp;
  pro.ddTp := -(pro.d*pro.d)*vt;
  pro.duTp := pro.cp - g.p*vt;
  pro.dupT := -g.T*vt - g.p*vp;
  annotation (
    Window(
      x=0.06,
      y=0.13,
      width=0.73,
      height=0.76),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end gibbsToProps_pT;
