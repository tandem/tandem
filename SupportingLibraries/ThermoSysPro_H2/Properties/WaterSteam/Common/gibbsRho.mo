within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsRho
  input Units.SI.AbsolutePressure P "Pressure";
  input Units.SI.Temperature T "Temperature";

  output Units.SI.Density rho "density";
  input GibbsDerivs g "Dérivées de la fonction de Gibbs"
    annotation (Placement(transformation(extent={{-70,-70},{70,70}}, rotation=
           0)));
algorithm

  rho := max(P, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)/
    (ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*max(T,
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.Ttriple)*g.pi*g.gpi);
  annotation (
    Window(
      x=0.2,
      y=0.26,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end gibbsRho;
