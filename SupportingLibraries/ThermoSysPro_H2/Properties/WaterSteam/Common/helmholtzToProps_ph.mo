within ThermoSysPro_H2.Properties.WaterSteam.Common;
function helmholtzToProps_ph
  input HelmholtzDerivs f
    "dimensionless derivatives of the Helmholtz function";
  output ThermoProperties_ph pro;
protected
  Real pd;
  Real pt;
protected
 Real cv "Heat capacity at constant volume";
algorithm
  pro.d := f.d;
  pro.T := f.T;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.u := f.R*f.T*f.tau*f.ftau;
  cv := f.R*(-f.tau*f.tau*f.ftautau);
  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
            *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  pro.ddph := (f.d*(cv*f.d + pt))/(f.d*f.d*pd*cv + f.T*pt*pt);
  pro.ddhp := -f.d*f.d*pt/(f.d*f.d*pd*cv + f.T*pt*pt);
  annotation (
    Icon(graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"), Diagram(graphics));
end helmholtzToProps_ph;
