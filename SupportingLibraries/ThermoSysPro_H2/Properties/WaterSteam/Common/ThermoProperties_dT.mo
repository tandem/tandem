within ThermoSysPro_H2.Properties.WaterSteam.Common;
record ThermoProperties_dT
  Units.SI.AbsolutePressure p(
    min=InitLimits.PMIN,
    max=InitLimits.PMAX,
    nominal=InitLimits.PNOM) "Pressure";
  Units.SI.SpecificEnthalpy h(
    min=InitLimits.SHMIN,
    max=InitLimits.SHMAX,
    nominal=InitLimits.SHNOM) "Specific enthalpy";
  Units.SI.SpecificEnergy u(
    min=InitLimits.SEMIN,
    max=InitLimits.SEMAX,
    nominal=InitLimits.SENOM) "Specific inner energy";
  Units.SI.SpecificEntropy s(
    min=InitLimits.SSMIN,
    max=InitLimits.SSMAX,
    nominal=InitLimits.SSNOM) "Specific entropy";
  Units.SI.SpecificHeatCapacity cp(
    min=InitLimits.CPMIN,
    max=InitLimits.CPMAX,
    nominal=InitLimits.CPNOM) "Specific heat capacity at constant pressure";
  Real dudT
    "Derivative of the inner energy wrt. density at constant temperature";
  ThermoSysPro_H2.Units.SI.MassFraction x "Vapor mas fraction";
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Rectangle(
          extent={{-100,50},{100,-100}},
          lineColor={0,0,255},
          fillColor={255,255,127},
          fillPattern=FillPattern.Solid),
        Line(points={{-100,-50},{100,-50}}, color={0,0,0}),
        Line(points={{-100,0},{100,0}}, color={0,0,0}),
        Line(points={{0,50},{0,-100}}, color={0,0,0}),
        Text(extent={{-127,115},{127,55}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end ThermoProperties_dT;
