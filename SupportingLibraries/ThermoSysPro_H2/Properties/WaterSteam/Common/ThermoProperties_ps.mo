within ThermoSysPro_H2.Properties.WaterSteam.Common;
record ThermoProperties_ps
  Units.SI.Temperature T(
    min=InitLimits.TMIN,
    max=InitLimits.TMAX,
    nominal=InitLimits.TNOM) "Temperature";
  Units.SI.Density d(
    min=InitLimits.DMIN,
    max=InitLimits.DMAX,
    nominal=InitLimits.DNOM) "Density";
  Units.SI.SpecificEnergy u(
    min=InitLimits.SEMIN,
    max=InitLimits.SEMAX,
    nominal=InitLimits.SENOM) "Specific inner energy";
  Units.SI.SpecificEnthalpy h(
    min=InitLimits.SHMIN,
    max=InitLimits.SHMAX,
    nominal=InitLimits.SHNOM) "Specific enthalpy";
  Units.SI.SpecificHeatCapacity cp(
    min=InitLimits.CPMIN,
    max=InitLimits.CPMAX,
    nominal=InitLimits.CPNOM) "Specific heat capacity at constant pressure";
  ThermoSysPro_H2.Units.xSI.DerDensityByEntropy ddsp
    "Derivative of the density wrt. specific entropy at constant pressure";
  Units.SI.DerDensityByPressure ddps
    "Derivative of the density wrt. pressure at constant specific entropy";
  ThermoSysPro_H2.Units.SI.MassFraction x "Vapor mass fraction";
  annotation (
    Window(
      x=0.31,
      y=0.2,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Rectangle(
          extent={{-100,50},{100,-100}},
          lineColor={0,0,255},
          fillColor={255,255,127},
          fillPattern=FillPattern.Solid),
        Text(extent={{-127,115},{127,55}}, textString=
                                               "%name"),
        Line(points={{-100,-50},{100,-50}}, color={0,0,0}),
        Line(points={{-100,0},{100,0}}, color={0,0,0}),
        Line(points={{0,50},{0,-100}}, color={0,0,0})}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.5</b></p>
</HTML>
"));
end ThermoProperties_ps;
