within ThermoSysPro_H2.Properties.WaterSteam.Common;
record IF97PhaseBoundaryProperties
  "thermodynamic base properties on the phase boundary for IF97 steam tables"
  extends Modelica.Icons.Record;
  Units.SI.SpecificHeatCapacity R "specific heat capacity";
  Units.SI.Temperature T "temperature";
  Units.SI.Density d "density";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificEntropy s "specific entropy";
  Units.SI.SpecificHeatCapacity cp "heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "heat capacity at constant volume";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature dpT
    "dp/dT derivative of saturation curve";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
  Real vt(unit="m3/(kg.K)")
    "derivative of specific volume w.r.t. temperature";
  Real vp(unit="m3/(kg.Pa)") "derivative of specific volume w.r.t. pressure";
end IF97PhaseBoundaryProperties;
