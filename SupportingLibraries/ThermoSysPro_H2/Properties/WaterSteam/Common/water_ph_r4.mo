within ThermoSysPro_H2.Properties.WaterSteam.Common;
function water_ph_r4
  input Units.SI.AbsolutePressure p;
  input Units.SI.SpecificEnthalpy h;

protected
  Real x;
  Real dpT;
public
  output ThermoProperties_ph pro;
protected
  PhaseBoundaryProperties liq;
  PhaseBoundaryProperties vap;
  GibbsDerivs gl;
  GibbsDerivs gv;
  HelmholtzDerivs fl;
  HelmholtzDerivs fv;
  Units.SI.Density dl;
  Units.SI.Density dv;
  Real cv "Heat capacity at constant volume";
algorithm
  pro.T := BaseIF97.Basic.tsat(p);
  dpT := BaseIF97.Basic.dptofT(pro.T);
  dl := BaseIF97.Regions.rhol_p_R4b(p);
  dv := BaseIF97.Regions.rhov_p_R4b(p);
  if p < BaseIF97.data.PLIMIT4A then
    gl := BaseIF97.Basic.g1(p, pro.T);
    gv := BaseIF97.Basic.g2(p, pro.T);
    liq := gibbsToBoundaryProps(gl);
    vap := gibbsToBoundaryProps(gv);
  else
    fl := BaseIF97.Basic.f3(dl, pro.T);
    fv := BaseIF97.Basic.f3(dv, pro.T);
    liq := helmholtzToBoundaryProps(fl);
    vap := helmholtzToBoundaryProps(fv);
  end if;
  x := if (vap.h <> liq.h) then (h - liq.h)/(vap.h - liq.h) else 1.0;
  cv := cv2Phase(
      liq=liq,
      vap=vap,
      x=x,
      p=p,
      T=pro.T);
  pro.d := liq.d*vap.d/(vap.d + x*(liq.d - vap.d));
  pro.x := x;
  pro.u := x*vap.u + (1 - x)*liq.u;
  pro.s := x*vap.s + (1 - x)*liq.s;
  pro.cp := x*vap.cp + (1 - x)*liq.cp;
  pro.ddph := pro.d*(pro.d*cv/dpT + 1.0)/(dpT*pro.T);
  pro.ddhp := -pro.d*pro.d/(dpT*pro.T);
  annotation (
    Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.2</b></p>
</HTML>
"));
end water_ph_r4;
