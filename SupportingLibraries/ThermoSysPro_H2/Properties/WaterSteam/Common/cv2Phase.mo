within ThermoSysPro_H2.Properties.WaterSteam.Common;
function cv2Phase
  input PhaseBoundaryProperties liq;
  input PhaseBoundaryProperties vap;
  input Real x "Vapor mass fraction";
  input Units.SI.Temperature T;
  input Units.SI.AbsolutePressure p;
  output Units.SI.SpecificHeatCapacity cv;

protected
  Real dpT;
  Real dxv;
  Real dvT;
  Real dvTl;
  Real dvTv;
  Real duTl;
  Real duTv;
  Real dxt;
algorithm
  dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d - vap.d) else 0.0;
  dpT := (vap.s - liq.s)*dxv;
  // wrong at critical point
  dvTl := (liq.pt - dpT)/liq.pd/liq.d/liq.d;
  dvTv := (vap.pt - dpT)/vap.pd/vap.d/vap.d;
  dxt := -dxv*(dvTl + x*(dvTv - dvTl));
  duTl := liq.cv + (T*liq.pt - p)*dvTl;
  duTv := vap.cv + (T*vap.pt - p)*dvTv;
  cv := duTl + x*(duTv - duTl) + dxt*(vap.u - liq.u);
  annotation (
    Window(
      x=0.08,
      y=0.14,
      width=0.6,
      height=0.61),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end cv2Phase;
