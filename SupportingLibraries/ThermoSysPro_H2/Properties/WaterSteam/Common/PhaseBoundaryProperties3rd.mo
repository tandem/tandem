within ThermoSysPro_H2.Properties.WaterSteam.Common;
record PhaseBoundaryProperties3rd
  "thermodynamic base properties on the phase boundary"
  extends Modelica.Icons.Record;
  Units.SI.Temperature T "Temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature dpT
    "dp/dT derivative of saturation curve";
  Units.SI.Density d "Density";
  Units.SI.SpecificEnthalpy h "Specific enthalpy";
  Units.SI.SpecificEnergy u "Inner energy";
  Units.SI.SpecificEntropy s "Specific entropy";
  Units.SI.SpecificHeatCapacity cp "Heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "Heat capacity at constant volume";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "Derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "Derivative of pressure wrt density";
  Real cvt "Derivative of cv w.r.t. temperature";
  Real cpt "Derivative of cp w.r.t. temperature";
  Real ptt "2nd derivative of pressure wrt temperature";
  Real pdd "2nd derivative of pressure wrt density";
  Real ptd "Mixed derivative of pressure w.r.t. density and temperature";
  Real vt "Derivative of specific volume w.r.t. temperature";
  Real vp "Derivative of specific volume w.r.t. pressure";
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "Mixed derivative of specific volume w.r.t. pressure and temperature";
end PhaseBoundaryProperties3rd;
