within ThermoSysPro_H2.Properties.WaterSteam.Common;
record IF97BaseTwoPhase "Intermediate property data record for IF 97"
  extends Modelica.Icons.Record;
  Integer phase "phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  Integer region(min=1, max=5) "IF 97 region";
  Units.SI.Pressure p "pressure";
  Units.SI.Temperature T "temperature";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificHeatCapacity R "gas constant";
  Units.SI.SpecificHeatCapacity cp "specific heat capacity";
  Units.SI.SpecificHeatCapacity cv "specific heat capacity";
  Units.SI.Density rho "density";
  Units.SI.SpecificEntropy s "specific entropy";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt density";
  Real vt "derivative of specific volume w.r.t. temperature";
  Real vp "derivative of specific volume w.r.t. pressure";
  Real x "dryness fraction";
  Real dpT "dp/dT derivative of saturation curve";
end IF97BaseTwoPhase;
