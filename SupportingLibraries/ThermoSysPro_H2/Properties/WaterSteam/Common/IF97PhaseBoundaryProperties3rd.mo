within ThermoSysPro_H2.Properties.WaterSteam.Common;
record IF97PhaseBoundaryProperties3rd
  "Thermodynamic base properties on the phase boundary, Analytic Jacobian verModelica.SIunitson"
  extends Modelica.Icons.Record;
  Units.SI.SpecificHeatCapacity R "specific heat capacity";
  Units.SI.Temperature T "temperature";
  Units.SI.Density d "denModelica.SIunitsty";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
  Units.SI.SpecificEntropy s "specific entropy";
  Units.SI.SpecificHeatCapacity cp "heat capacity at constant pressure";
  Units.SI.SpecificHeatCapacity cv "heat capacity at constant volume";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature dpT
    "dp/dT derivative of saturation curve";
  Real dpTT(unit = "Pa/(K.K)") "Second derivative of saturation curve";
  ThermoSysPro_H2.Units.xSI.DerPressureByTemperature pt
    "derivative of pressure wrt temperature";
  ThermoSysPro_H2.Units.xSI.DerPressureByDensity pd
    "derivative of pressure wrt denModelica.SIunitsty";
  Real vt(unit="m3/(kg.K)")
    "derivative of specific volume w.r.t. temperature";
  Real vp(unit="m3/(kg.Pa)") "derivative of specific volume w.r.t. pressure";
  Real cvt "Derivative of cv w.r.t. temperature";
  Real cpt "Derivative of cp w.r.t. temperature";
  Real ptt "2nd derivative of pressure wrt temperature";
  Real pdd "2nd derivative of pressure wrt denModelica.SIunitsty";
  Real ptd
    "Mixed derivative of pressure w.r.t. denModelica.SIunitsty and temperature";
  Real vtt "2nd derivative of specific volume w.r.t. temperature";
  Real vpp "2nd derivative of specific volume w.r.t. pressure";
  Real vtp
    "Mixed derivative of specific volume w.r.t. pressure and temperature";
end IF97PhaseBoundaryProperties3rd;
