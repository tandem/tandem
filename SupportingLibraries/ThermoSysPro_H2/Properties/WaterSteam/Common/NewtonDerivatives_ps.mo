within ThermoSysPro_H2.Properties.WaterSteam.Common;
record NewtonDerivatives_ps
  Units.SI.AbsolutePressure p;
  Units.SI.SpecificEntropy s;
  Real pd;
  Real pt;
  Real sd;
  Real st;
  annotation (
    Window(
      x=0.15,
      y=0.3,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end NewtonDerivatives_ps;
