within ThermoSysPro_H2.Properties.WaterSteam.Common;
function helmholtzToProps_dT
  input HelmholtzDerivs f;
  output ThermoProperties_dT pro;
protected
  Real pt "derivative of presure w.r.t. temperature";
  Real pv "derivative of pressure w.r.t. specific volume";

algorithm
  pro.p := f.R*f.d*f.T*f.delta*f.fdelta;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);
  pro.u := f.R*f.T*f.tau*f.ftau;
  pv := -1/(f.d*f.d)*f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.
    fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pro.dudT := (pro.p - f.T*pt)/(f.d*f.d);
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.09,
      y=0.22,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end helmholtzToProps_dT;
