within ThermoSysPro_H2.Properties.WaterSteam.Common;
function helmholtzToProps_ps
  input HelmholtzDerivs f
    "dimensionless derivatives of the Helmholtz function";
  output ThermoProperties_ps pro;
protected
  Real pd;
  Real pt;
protected
  Real cv "Heat capacity at constant volume";
algorithm

  pro.d := f.d;
  pro.T := f.T;
  pro.u := f.R*f.T*f.tau*f.ftau;
  pro.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);

  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  cv := f.R*(-f.tau*f.tau*f.ftautau);
  pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  pro.ddps := f.d*f.d*cv/(pd*f.d*f.d*cv + pt*pt*f.T);
  pro.ddsp := -f.d*f.d*pt*f.T/(f.d*f.d*pd*cv + f.T*pt*pt);
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.13,
      y=0.25,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end helmholtzToProps_ps;
