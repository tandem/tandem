within ThermoSysPro_H2.Properties.WaterSteam.Common;
function helmholtzToBoundaryProps3rd
  "calulate phase boundary property record from dimensionless Helmholtz function"

  extends Modelica.Icons.Function;
  input ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless derivatives of Helmholtz function";
  output
    ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd sat
    "phase boundary property record";
protected
  Units.SI.Pressure p "pressure";
algorithm
  p := f.R*f.d*f.T*f.delta*f.fdelta;
  sat.d := f.d;
  sat.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);
  sat.s := f.R*(f.tau*f.ftau - f.f);
  sat.u := f.R*f.T*f.tau*f.ftau;
  sat.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau*f.
     fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  sat.cv := f.R*(-f.tau*f.tau*f.ftautau);
  sat.pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  sat.pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  sat.pdd := f.R*f.T*f.delta/f.d*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
           f.delta*f.delta*f.fdeltadeltadelta);
  sat.ptt := f.R*f.d*f.delta*f.tau*f.tau/f.T*f.fdeltatautau;
  sat.ptd := f.R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
             -f.delta*f.tau*f.fdeltadeltatau);
  sat.cvt := f.R*f.tau*f.tau/f.T*(2.0*f.ftautau + f.tau*f.ftautautau);

  annotation (Icon(graphics));
end helmholtzToBoundaryProps3rd;
