within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsToProps_dT
  input GibbsDerivs g;
  output ThermoProperties_dT pro;
protected
  Real vt "derivative of specific volume w.r.t. temperature";
  Real vp "derivative of specific volume w.r.t. pressure";
algorithm
  pro.p := g.p;
  pro.u := g.T*g.R*(g.tau*g.gtau - g.pi*g.gpi);
  pro.h := g.R*g.T*g.tau*g.gtau;
  pro.s := g.R*(g.tau*g.gtau - g.g);
  pro.cp := -g.R*g.tau*g.tau*g.gtautau;
  vt := g.R/g.p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
  vp := g.R*g.T/(g.p*g.p)*g.pi*g.pi*g.gpipi;
  pro.dudT := pro.cp + g.T*vt*vt/vp;
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.08,
      y=0.23,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end gibbsToProps_dT;
