within ThermoSysPro_H2.Properties.WaterSteam.Common;
function water_ps_r4
  input Units.SI.AbsolutePressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";

protected
  Real x;
  Real dpT;
public
  output ThermoProperties_ps pro;
protected
  PhaseBoundaryProperties liq;
  PhaseBoundaryProperties vap;
  GibbsDerivs gl
    "dimensionless Gibbs function and derivatives wrt dimensionless presure and temperature";
  GibbsDerivs gv
    "dimensionless Gibbs function and derivatives wrt dimensionless presure and temperature";
  HelmholtzDerivs fl
    "dimensionless Helmholtz function and derivatives wrt dimensionless presure and temperature";
  HelmholtzDerivs fv
    "dimensionless Helmholtz function and derivatives wrt dimensionless presure and temperature";
  Real cv "Heat capacity at constant volume";
  Units.SI.Density dl;
  Units.SI.Density dv;
algorithm

  pro.T := BaseIF97.Basic.tsat(p);
  dpT := BaseIF97.Basic.dptofT(pro.T);

  dl := BaseIF97.Regions.rhol_p_R4b(p);
  dv := BaseIF97.Regions.rhov_p_R4b(p);
  if p < BaseIF97.data.PLIMIT4A then
    gl := BaseIF97.Basic.g1(p, pro.T);
    gv := BaseIF97.Basic.g2(p, pro.T);
    liq := gibbsToBoundaryProps(gl);
    vap := gibbsToBoundaryProps(gv);
  else
    fl := BaseIF97.Basic.f3(dl, pro.T);
    fv := BaseIF97.Basic.f3(dv, pro.T);
    liq := helmholtzToBoundaryProps(fl);
    vap := helmholtzToBoundaryProps(fv);
  end if;
  x := if (vap.s <> liq.s) then (s - liq.s)/(vap.s - liq.s) else 1.0;
  pro.x := x;
  pro.d := liq.d*vap.d/(vap.d + x*(liq.d - vap.d));
  pro.u := x*vap.u + (1 - x)*liq.u;
  pro.h := x*vap.h + (1 - x)*liq.h;
  pro.cp := Modelica.Constants.inf;
  cv := cv2Phase(
      liq,
      vap,
      x,
      pro.T,
      p);
  pro.ddps := cv*pro.d*pro.d/(dpT*dpT*pro.T);
  pro.ddsp := -pro.d*pro.d/dpT;
  annotation (
    Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end water_ps_r4;
