within ThermoSysPro_H2.Properties.WaterSteam.Common;
record miniProp "Test record for derivatives"
  Units.SI.Temperature T "Temperature";
  Units.SI.Density d "Density";
end miniProp;
