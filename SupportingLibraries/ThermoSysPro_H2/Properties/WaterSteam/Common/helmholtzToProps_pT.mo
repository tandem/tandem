within ThermoSysPro_H2.Properties.WaterSteam.Common;
function helmholtzToProps_pT
  input HelmholtzDerivs f;
  output ThermoProperties_pT pro;
protected
  Real pd "derivative of pressure wrt. density";
  Real pt "derivative of pressure wrt. temperature";
  Real pv "derivative of pressure wrt. specific volume";
protected
 Real cv "Heat capacity at constant volume";
algorithm

  pro.d := f.d;
  pro.s := f.R*(f.tau*f.ftau - f.f);
  pro.h := f.R*f.T*(f.tau*f.ftau + f.delta*f.fdelta);
  pro.u := f.R*f.T*f.tau*f.ftau;
  pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  pt := f.R*f.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
  cv := f.R*(-f.tau*f.tau*f.ftautau);
  pv := -1/(f.d*f.d)*pd;
  // calculating cp near the critical point may be troublesome (cp -> inf).
  pro.cp := f.R*(-f.tau*f.tau*f.ftautau + (f.delta*f.fdelta - f.delta*f.tau
    *f.fdeltatau)^2/(2*f.delta*f.fdelta + f.delta*f.delta*f.fdeltadelta));
  pro.ddTp := -pt/pd;
  pro.ddpT := 1/pd;
  pro.dupT := (f.d - f.T*pt)/(f.d*f.d*pd);
  pro.duTp := (-cv*f.d*f.d*pd + pt*f.d - f.T*pt*pt)/(f.d*f.d*
    pd);
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.32,
      y=0.14,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end helmholtzToProps_pT;
