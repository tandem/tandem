within ThermoSysPro_H2.Properties.WaterSteam.Common;
function gibbsPropsSat
  input Units.SI.AbsolutePressure P "Pressure";
  input Units.SI.Temperature T "Temperature";

  input GibbsDerivs g "Dérivées de la fonction de Gibbs"
    annotation (Placement(transformation(extent={{-85,15},{-15,85}}, rotation=
           0)));
  output PropThermoSat sat annotation (Placement(transformation(extent={{15,
            15},{85,85}}, rotation=0)));
algorithm

  sat.P := max(P, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple);
  sat.T := max(T, ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.Ttriple);
  sat.rho := sat.P/(ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*
    sat.T*g.pi*g.gpi);
  sat.h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*sat.T*g.tau
    *g.gtau;
  /*
  sat.u := sat.T*ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*(g.tau*g.gtau - g.pi*g.gpi);
  sat.s := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*(g.tau*g.gtau - g.g);
  */
  sat.cp := -ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*g.tau*g.tau
    *g.gtautau;
  /*
  sat.cv :=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O
           *(-g.tau*g.tau*g.gtautau + (g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau*g.gtaupi)/(g.gpipi));
  sat.pt := -sat.P/sat.T*(g.gpi - g.tau*g.gtaupi)/(g.gpipi*g.pi);
  sat.pd := -ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O*sat.T*g.gpi*g.gpi/(g.gpipi);
  */
  annotation (
    Window(
      x=0.25,
      y=0.27,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end gibbsPropsSat;
