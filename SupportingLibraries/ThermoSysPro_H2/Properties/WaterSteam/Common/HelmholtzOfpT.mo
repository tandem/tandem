within ThermoSysPro_H2.Properties.WaterSteam.Common;
function HelmholtzOfpT

  input HelmholtzDerivs f;
  output NewtonDerivatives_pT nderivs annotation (Placement(transformation(
          extent={{-85,-85},{-15,-15}}, rotation=0)));
algorithm

  nderivs.p := f.d*f.R*f.T*f.delta*f.fdelta;
  nderivs.pd := f.R*f.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.11,
      y=0.25,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Adapted from the ThermoFlow library (H. Tummescheit)</b></p>
</HTML>
<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end HelmholtzOfpT;
