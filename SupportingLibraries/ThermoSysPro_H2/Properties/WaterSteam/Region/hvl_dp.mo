within ThermoSysPro_H2.Properties.WaterSteam.Region;
function hvl_dp
  "derivative function for the specific enthalpy along the phase boundary"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties3rd
    bpro "property record";
  output Real dh_dp
    "derivative of specific enthalpy along the phase boundary";
algorithm
    dh_dp := (1/bpro.d - bpro.T*bpro.vt) + bpro.cp/bpro.dpT;
    // dh_dp_der := vp - (T*vpt + dTp*vt) + d/dp(cp/(dp/dT))
  annotation (
    derivative(noDerivative=bpro) = hvl_dp_der,
    Inline=false,
    LateInline=true);
end hvl_dp;
