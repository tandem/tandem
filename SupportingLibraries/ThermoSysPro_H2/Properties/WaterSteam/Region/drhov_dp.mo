within ThermoSysPro_H2.Properties.WaterSteam.Region;
function drhov_dp "derivative of density of saturated steam w.r.t. pressure"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  output Units.SI.DerDensityByPressure dd_dp
    "derivative of density of water at the boiling point";
algorithm
  dd_dp := ThermoSysPro_H2.Properties.WaterSteam.Region.drhovl_dp(p,
    ThermoSysPro_H2.Properties.WaterSteam.Region.dewcurveL3_p(p));
end drhov_dp;
