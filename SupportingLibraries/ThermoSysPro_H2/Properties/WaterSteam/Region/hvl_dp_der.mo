within ThermoSysPro_H2.Properties.WaterSteam.Region;
function hvl_dp_der
  "derivative function for the specific enthalpy along the phase boundary"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties3rd
    bpro "property record";
  input Real p_der "Pressure derivative";
  output Real dh_dp_der
    "Second derivative of specific enthalpy along the phase boundary";
protected
  Real cpp "Derivative of cp w.r.t. p";
  Real pv "partial derivative of p w.r.t. v";
  Real pv2 "pv*pv";
  Real pv3 "pv*pv*pv";
  Real ptv "2nd partial derivative of p w.r.t t and v";
  Real pvv "2nd partial derivative of p w.r.t v and v";
algorithm
  pv :=-bpro.d*bpro.d*bpro.pd;
  pv2 := pv*pv;
  pv3 := pv2*pv;
  pvv := bpro.pdd*bpro.d*bpro.d*bpro.d*bpro.d;
  ptv := (-bpro.d*bpro.d*bpro.ptd);
  cpp := (bpro.T*(bpro.ptt*pv2 - 2.0*bpro.pt*ptv*pv + bpro.pt*bpro.pt*pvv))/pv3
    "T*(ptt*pv^2 - 2*pt*ptv*pv + pt^2*pvv)/pv^3";
  dh_dp_der := 0.0;
//   dh_dp_der := 1/pv - (bpro.T*bpro.vtp + bpro.dpT*bpro.vt) +
//     cpp/bpro.dpT - bpro.cp*(bpro.dpTT*bpro.dpT)/(bpro.dpT*bpro.dpT);
//     //d/dp(cp/(dp/dT))

  annotation (Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={255,0,0},
          lineThickness=0.5)}));
end hvl_dp_der;
