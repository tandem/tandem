within ThermoSysPro_H2.Properties.WaterSteam.Region;
function drhol_dp "derivative of density of saturated water w.r.t. pressure"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  output Units.SI.DerDensityByPressure dd_dp
    "derivative of density of water at the boiling point";
algorithm
  dd_dp := ThermoSysPro_H2.Properties.WaterSteam.Region.drhovl_dp(p,
    ThermoSysPro_H2.Properties.WaterSteam.Region.boilingcurveL3_p(p));
end drhol_dp;
