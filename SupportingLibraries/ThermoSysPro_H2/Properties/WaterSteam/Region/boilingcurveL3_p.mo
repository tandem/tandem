within ThermoSysPro_H2.Properties.WaterSteam.Region;
function boilingcurveL3_p "properties on the boiling curve"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  output
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties3rd
    bpro "property record";
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz function and dervatives";
  Units.SI.Pressure plim=min(p,ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT
       - 1e-7) "pressure limited to critical pressure - epsilon";
  Units.SI.SpecificVolume v "Specific Volume";
  Real vp3 "vp^3";
  Real ivp3 "1/vp^3";
  Real pv "partial derivative of p w.r.t v";
  Real pv2 "pv^2";
  Real pv3 "pv^3";
  Real ptv "2nd partial derivative of p w.r.t t and v";
  Real pvv "2nd partial derivative of p w.r.t v and v";
algorithm
  g.R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  bpro.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(plim);
  (bpro.dpT,bpro.dpTT) :=
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.d2ptofT(bpro.T);
  // need derivative of dpT
//  g.Region3boundary := bpro.T > data.TLIMIT1;
  if not bpro.T > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.TLIMIT1 then
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, bpro.T);
    bpro.d := p/(g.R*bpro.T*g.pi*g.gpi);
    bpro.h := if p > plim then ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.HCRIT
       else g.R*bpro.T*g.tau*g.gtau;
    bpro.s := g.R*(g.tau*g.gtau - g.g);
    bpro.cp := -g.R*g.tau*g.tau*g.gtautau;
    bpro.vt := g.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    bpro.vp := g.R*bpro.T/(p*p)*g.pi*g.pi*g.gpipi;
    bpro.pt := -p/bpro.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    bpro.pd := -g.R*bpro.T*g.gpi*g.gpi/(g.gpipi);
    bpro.vtt := g.R*g.pi/p*g.tau/bpro.T*g.tau*g.gpitautau;
    bpro.vtp := g.R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    bpro.vpp := g.R*bpro.T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    bpro.cpt := g.R*g.tau*g.tau/bpro.T*(2*g.gtautau + g.tau*g.gtautautau);
    v := 1/bpro.d;
    vp3 := bpro.vp*bpro.vp*bpro.vp;
    ivp3 := 1/vp3;
    bpro.ptt := -(bpro.vtt*bpro.vp*bpro.vp -2.0*bpro.vt*bpro.vtp*bpro.vp +bpro.vt*bpro.vt*bpro.vpp)*ivp3;
    bpro.pdd := -bpro.vpp*ivp3*v*v*v*v - 2*v*bpro.pd "= pvv/d^4";
    bpro.ptd := (bpro.vtp*bpro.vp-bpro.vt*bpro.vpp)*ivp3*v*v "= -ptv/d^2";
    bpro.cvt := (vp3*bpro.cpt + bpro.vp*bpro.vp*bpro.vt*bpro.vt + 3.0*bpro.vp*bpro.vp*bpro.T*bpro.vt*bpro.vtt
      - 3.0*bpro.vtp*bpro.vp*bpro.T*bpro.vt*bpro.vt + bpro.T*bpro.vt*bpro.vt*bpro.vt*bpro.vpp)*ivp3;
  else
    bpro.d := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhol_p_R4b(
      plim);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(bpro.d, bpro.T);
    bpro.h := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.hl_p_R4b(
      plim);
    // g.R*bpro.T*(f.tau*f.ftau + f.delta*f.fdelta);
    bpro.s := f.R*(f.tau*f.ftau - f.f);
    bpro.cv := g.R*(-f.tau*f.tau*f.ftautau);
    bpro.pt := g.R*bpro.d*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    bpro.pd := g.R*bpro.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    pv := (-f.d*f.d*bpro.pd);
    bpro.vp := 1/pv;
    bpro.vt := -bpro.pt/pv;
    bpro.pdd := f.R*bpro.T*f.delta/bpro.d*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    bpro.ptt := f.R*bpro.d*f.delta*f.tau*f.tau/bpro.T*f.fdeltatautau;
    bpro.ptd := f.R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
     -f.delta*f.tau*f.fdeltadeltatau);
    bpro.cvt := f.R*f.tau*f.tau/bpro.T*(2.0*f.ftautau + f.tau*f.ftautautau);
    bpro.cpt := (bpro.cvt*bpro.pd + bpro.cv*bpro.ptd + (bpro.pt + 2.0*bpro.T*bpro.ptt)*bpro.pt/(bpro.d*bpro.d)
     - bpro.cp*bpro.ptd)/bpro.pd;
    pv2 := pv*pv;
    pv3 := pv2*pv;
    pvv := bpro.pdd*f.d*f.d*f.d*f.d;
    ptv := (-f.d*f.d*bpro.ptd);
    bpro.vpp := -pvv/pv3;
    bpro.vtt := -(bpro.ptt*pv2 -2.0*bpro.pt*ptv*pv + bpro.pt*bpro.pt*pvv)/pv3;
    bpro.vtp := (-ptv*pv + bpro.pt*pvv)/pv3;
  end if;
end boilingcurveL3_p;
