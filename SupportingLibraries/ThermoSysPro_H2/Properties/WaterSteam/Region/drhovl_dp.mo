within ThermoSysPro_H2.Properties.WaterSteam.Region;
function drhovl_dp
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties3rd
    bpro "property record";
  output Real dd_dp(unit="kg/(m3.Pa)")
    "derivative of density along the phase boundary";
algorithm
  dd_dp := -bpro.d*bpro.d*(bpro.vp + bpro.vt/bpro.dpT);
  annotation (
    derivative(noDerivative=bpro) = drhovl_dp_der,
    Inline=false,
    LateInline=true);
end drhovl_dp;
