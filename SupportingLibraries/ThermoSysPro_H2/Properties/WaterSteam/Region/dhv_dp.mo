within ThermoSysPro_H2.Properties.WaterSteam.Region;
function dhv_dp
  "derivative of vapour specific enthalpy on the boundary between regions 4 and 3 or 1 w.r.t pressure"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  output Units.SI.DerEnthalpyByPressure dh_dp
    "specific enthalpy derivative w.r.t. pressure";
algorithm
  dh_dp := ThermoSysPro_H2.Properties.WaterSteam.Region.hvl_dp(p,
    ThermoSysPro_H2.Properties.WaterSteam.Region.dewcurveL3_p(p));
  annotation(smoothOrder=2);
end dhv_dp;
