within ThermoSysPro_H2.Properties.WaterSteam.Region;
function drhovl_dp_der
  "Time derivative of density derivative along phase boundary"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties3rd
    bpro "property record";
  input Real p_der "Time derivative of pressure";
  output Real dd_dp_der "derivative of density along the phase boundary";
algorithm
  dd_dp_der := 0.0;
end drhovl_dp_der;
