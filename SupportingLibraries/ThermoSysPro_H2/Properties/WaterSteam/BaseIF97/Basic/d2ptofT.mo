within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic;
function d2ptofT
  "Second derivative of pressure wrt temperature along the saturation pressure curve"
  extends Modelica.Icons.Function;
  input Units.SI.Temperature T "temperature (K)";
  output Real dpT(unit = "Pa/K") "Temperature derivative of pressure";
  output Real dpTT(unit = "Pa/(K.K)")
    "Second temperature derivative of pressure";
protected
  Real A "Auxiliary variable";
  Real Ad "Auxiliary variable";
  Real A1 "Auxiliary variable";
  Real A2 "Auxiliary variable";
  Real B "Auxiliary variable";
  Real Bd "Auxiliary variable";
  Real B1 "Auxiliary variable";
  Real B2 "Auxiliary variable";
  Real C "Auxiliary variable";
  Real Cd "Auxiliary variable";
  Real C1 "Auxiliary variable";
  Real C2 "Auxiliary variable";
  Real D "Auxiliary variable";
  Real D1 "Auxiliary variable";
  Real Dd "Auxiliary variable";
  Real D2 "Auxiliary variable";
//   Real F "Auxiliary variable";
//   Real Fd "Auxiliary variable";
  Real th "Auxiliary variable";
  Real thd "Auxiliary variable";
  Real thdd "Auxiliary variable";
  Real v "Auxiliary variable";
  Real v2 "Auxiliary variable";
  Real v4 "Auxiliary variable";
  Real v5 "Auxiliary variable";
  Real v6 "Auxiliary variable";
  Real[16] o "vector of auxiliary variables";
  Real Tlim "temperature limited to TCRIT";
  parameter Real[10] n =  {0.11670521452767e4,-0.72421316703206e6,
                          -0.17073846940092e2, 0.12020824702470e5,
                          -0.32325550322333e7, 0.14915108613530e2,
                          -0.48232657361591e4, 0.40511340542057e6,
                          -0.23855557567849,   0.65017534844798e3};
algorithm
  Tlim := min(T, data.TCRIT);
  o[1] := Tlim - n[10];
  th := Tlim + n[9]/o[1];
  o[2] := th*th "theta^2";
  A := o[2] + n[1]*th + n[2];
  B := n[3]*o[2] + n[4]*th + n[5];
  C := n[6]*o[2] + n[7]*th + n[8];
  o[3] := o[1]*o[1];
  o[4] := o[3]*o[3];
  D := (B*B-4.0*A*C);
  o[5] := sqrt(D);
  v := 1/(o[5]- B);
  v2 := v*v;
  v4 := v2*v2;
  v5 := v4*v;
  v6 := v4*v2;
  o[6] := 2.0*C*v;
  o[7] := o[6]*o[6];
//  F := o[7]*o[7]*1e6 "this is also psat, and correct";
  thd := 1.0 - n[9]/o[3];
  thdd :=  2.0*n[9]/(o[3]*o[1]);
  Ad := 2.0*th + n[1];
  Bd := 2.0*n[3]*th + n[4];
  Cd := 2.0*n[6]*th + n[7];
  Dd := 2*B*Bd -4*(Ad*C + Cd*A);
  A1 := Ad*thd;
  B1 := Bd*thd;
  C1 := Cd*thd;
  D1 := Dd*thd;
  o[8] := C*C "C^2";
  o[9] := o[8]*C "C^3";
  o[10] := o[9]*C "C^4";
  o[11] := 1/o[5] "1/sqrt(D)";
  o[12] := (-B1 + 0.5*D1*o[11]) "-B1 + 1/2*D1/sqrt(D)";
  o[13] := o[12]*o[12];
  o[14] := C1*C1 "C1^2";
  o[15] := B1*B1 "B1^2";
  o[16] := D*o[5] "D^3/2";
  // dpsat (=Fd) is correct
  // Fd := 64.0*C*C*C*C1*v4-64.0*C*C*C*C*(-B1 + 0.5*D1/sqrt(D))*v5;
  dpT := 64.0*(C1*o[9]*v4 - o[10]*o[12]*v5)*1.0e6 "dpsat";
  A2 := Ad*thdd + thd*thd*2.0;
  B2 := Bd*thdd + thd*thd*2.0*n[3];
  C2 := Cd*thdd + thd*thd*2.0*n[6];
  D2 := 2.0*(B*B2 + o[15]) -4.0*(A2*C + 2.0*A1*C1 + A*C2);
  dpTT := ((192.0*o[8]*o[14] + 64.0*o[9]*C2)*v4 + (-512.0*C1*o[9]*o[12] - 64.0*o[10]*(-B2-0.25*D1*D1/o[16] + 0.5*D2*o[11]))*v5
     +(320.0*o[10]*o[13])*v6)*1.0e6;
end d2ptofT;
