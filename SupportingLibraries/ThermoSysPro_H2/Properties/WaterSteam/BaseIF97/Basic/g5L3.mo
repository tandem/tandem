within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic;
function g5L3
  "base function for region 5: g(p,T), including 3rd derivatives"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature (K)";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
protected
  Real tau "dimensionless temperature";
  Real pi "dimensionless pressure";
  Real[16] o "vector of auxiliary variables";
algorithm
  assert(p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple,
    "IF97 medium function g5 called with too low pressure\n" + "p = " + String(
    p) + " Pa <= " + String(ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.triple.ptriple)
     + " Pa (triple point pressure)");
  assert(p <= ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PLIMIT5,
    "IF97 medium function g5: input pressure (= " + String(p) +
    " Pa) is higher than 10 Mpa in region 5");
  assert(T <= 2273.15,
    "IF97 medium function g5: input temperature (= " + String(T) + " K) is higher than limit of 2273.15K in region 5");
  g.p := p;
  g.T := T;
  g.R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  pi := p/ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PSTAR5;
  tau := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.TSTAR5/T;
  g.pi := pi;
  g.tau := tau;
  o[1] := tau*tau;
  o[2] := -0.004594282089991*o[1];
  o[3] := 0.0021774678714571 + o[2];
  o[4] := o[3]*tau;
  o[5] := o[1]*tau;
  o[6] := o[1]*o[1];
  o[7] := o[6]*o[6];
  o[8] := o[7]*tau;
  o[9] := -7.9449656719138e-6*o[8];
  o[10] := pi*pi;
  o[11] := o[10]*pi;
  o[12] := -0.013782846269973*o[1];
  o[13] := -0.027565692539946*tau;
  o[14] := o[1]*o[6]*tau;
  o[15] := o[1]*o[6];
  o[16] := -0.0000715046910472242*o[7];
  g.g := pi*(-0.00012563183589592 + o[4] + pi*(-3.9724828359569e-6*o[8] +
    1.2919228289784e-7*o[5]*pi)) + (-0.024805148933466 + tau*(
    0.36901534980333 + tau*(-3.1161318213925 + tau*(-13.179983674201 + (
    6.8540841634434 - 0.32961626538917*tau)*tau + Modelica.Math.log(pi)))))
    /o[5];
  g.gpi := (1.0 + pi*(-0.00012563183589592 + o[4] + pi*(o[9] +
    3.8757684869352e-7*o[5]*pi)))/pi;
  g.gpipi := (-1.0 + o[10]*(o[9] + 7.7515369738704e-7*o[5]*pi))/o[10];
  g.gpipipi := (2.0 + 7.7515369738704e-7*o[11]*o[5])/o[11];
  g.gtau := pi*(0.0021774678714571 + o[12] + pi*(-0.0000357523455236121*o[7] +
    3.8757684869352e-7*o[1]*pi)) + (0.074415446800398 + tau*(-0.73803069960666
     + (3.1161318213925 + o[1]*(6.8540841634434 - 0.65923253077834*tau))*tau))/
    o[6];
  g.gtautau := (-0.297661787201592 + tau*(2.21409209881998 + (-6.232263642785
     - 0.65923253077834*o[5])*tau))/(o[6]*tau) + pi*(o[13] + pi*(-0.000286018764188897
    *o[14] + 7.7515369738704e-7*pi*tau));
  g.gtautautau := pi*(-0.027565692539946 + (-0.00200213134932228*o[15] +
    7.7515369738704e-7*pi)*pi) + (1.48830893600796 + tau*(-8.85636839527992 +
    18.696790928355*tau))/o[15];
  g.gpitau := 0.0021774678714571 + o[12] + pi*(o[16] + 1.16273054608056e-6*o[1]
    *pi);
  g.gpipitau := o[16] + 2.32546109216112e-6*o[1]*pi;
  g.gpitautau := o[13] + pi*(-0.000572037528377794*o[14] + 2.32546109216112e-6*
    pi*tau);
end g5L3;
