within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic;
function psat_der "derivative function for psat"
  extends Modelica.Icons.Function;
  input Units.SI.Temperature T "temperature (K)";
  input Real der_T(unit = "K/s") "temperature derivative";
  output Real der_psat(unit = "Pa/s") "pressure";
protected
  Real dpt;
algorithm
  dpt := dptofT(T);
  der_psat := dpt*der_T;
end psat_der;
