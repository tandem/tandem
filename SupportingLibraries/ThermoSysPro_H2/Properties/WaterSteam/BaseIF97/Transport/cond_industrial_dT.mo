within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Transport;
function cond_industrial_dT
  "Thermal conductivity lam(d,T) (industrial use version) only in one-phase region"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "temperature (K)";
  output Units.SI.ThermalConductivity lambda "thermal conductivity";
protected
  constant Units.SI.Density rhostar2=317.7 "Reference density";
  constant Units.SI.Temperature Tstar2=647.25 "Reference temperature";
  constant Units.SI.ThermalConductivity lambdastar=1
    "Reference thermal conductivity";
  constant Real[:] C={0.642857,-4.11717,-6.17937,0.00308976,0.0822994,
      10.0932};
  constant Real[:] dpar={0.0701309,0.0118520,0.00169937,-1.0200};
  constant Real[:] b={-0.397070,0.400302,1.060000};
  constant Real[:] B={-0.171587,2.392190};
  constant Real[:] a={0.0102811,0.0299621,0.0156146,-0.00422464};
  Integer region(min=1, max=5) "IF97 region, valid values:1,2,3, and 5";
  Real TREL "Relative temperature";
  Real rhoREL "Relative density";
  Real lambdaREL "Relative thermal conductivity";
  Real deltaTREL "Relative temperature increment";
  Real Q;
  Real S;
  Real lambdaREL2
    "function, part of the interpolating equation of the thermal conductivity";
  Real lambdaREL1
    "function, part of the interpolating equation of the thermal conductivity";
  Real lambdaREL0
    "function, part of the interpolating equation of the thermal conductivity";
algorithm
  assert(d > triple.dvtriple,
    "IF97 medium function cond_dTp called with too low density\n" +
    "d = " + String(d) + " <= " + String(triple.dvtriple) + " (triple point density)");
   TREL := T/Tstar2;
   rhoREL := d/rhostar2;
   deltaTREL := abs(TREL - 1) + C[4];
   Q := 2 + C[5]/deltaTREL^(3/5);
   S := if TREL >= 1 then  1/deltaTREL else C[6]/deltaTREL^(3/5);
    lambdaREL2 := (dpar[1]/TREL^10 + dpar[2])*rhoREL^(9/5)*Modelica.Math.exp(C[1]*(1 - rhoREL^(14
      /5))) + dpar[3]*S*rhoREL^Q*Modelica.Math.exp((Q/(1 + Q))*(1 -
      rhoREL^(1 + Q))) + dpar[4]*Modelica.Math.exp(C[2]*TREL^(3/2) + C[3]
      /rhoREL^5);
    lambdaREL1 := b[1] + b[2]*rhoREL + b[3]*Modelica.Math.exp(B[1]*(
      rhoREL + B[2])^2);
    lambdaREL0 := TREL^(1/2)*sum(a[i]*TREL^(i - 1) for i in 1:4);
    lambdaREL := lambdaREL0 + lambdaREL1 + lambdaREL2;
    lambda := lambdaREL*lambdastar;
  annotation(smoothOrder=5);
end cond_industrial_dT;
