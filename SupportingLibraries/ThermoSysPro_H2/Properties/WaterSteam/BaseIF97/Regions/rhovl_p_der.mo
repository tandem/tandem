within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function rhovl_p_der
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties bpro
    "property record";
  input Real p_der "derivative of pressure";
  output Real d_der "time derivative of density along the phase boundary";
algorithm
  d_der := -bpro.d*bpro.d*(bpro.vp + bpro.vt/bpro.dpT)*p_der;
end rhovl_p_der;
