within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function sl_p
  "liquid specific entropy on the boundary between regions 4 and 3 or 1"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  output Units.SI.SpecificEntropy s "specific entropy";
protected
  Units.SI.Temperature Tsat "saturation temperature";
  Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  if (p < data.PLIMIT4A) then
    Tsat := Basic.tsat(p);
    (h,s) := Isentropic.handsofpT1(p, Tsat);
  elseif (p < data.PCRIT) then
    s := sl_p_R4b(p);
  else
    s := data.SCRIT;
  end if;
end sl_p;
