within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function hvl_p
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties bpro
    "property record";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := bpro.h;
  annotation (
    derivative(noDerivative=bpro) = hvl_p_der,
    Inline=false,
    LateInline=true);
end hvl_p;
