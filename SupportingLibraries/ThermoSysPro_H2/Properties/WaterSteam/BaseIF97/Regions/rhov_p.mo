within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function rhov_p "density of saturated vapour"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "saturation pressure";
  output Units.SI.Density rho "density of steam at the condensation point";
algorithm
  rho := rhovl_p(p, dewcurve_p(p));
end rhov_p;
