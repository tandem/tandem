within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function rhol_T "density of saturated water"
  extends Modelica.Icons.Function;
  input Units.SI.Temperature T "temperature";
  output Units.SI.Density d "density of water at the boiling point";
protected
  Units.SI.Pressure p "saturation pressure";
algorithm
  p := Basic.psat(T);
  if T < data.TLIMIT1 then
    d := d1n(p, T);
  elseif T < data.TCRIT then
    d := rhol_p_R4b(p);
  else
    d := data.DCRIT;
  end if;
end rhol_T;
