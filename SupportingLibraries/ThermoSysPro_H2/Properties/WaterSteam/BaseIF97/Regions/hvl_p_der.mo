within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function hvl_p_der
  "derivative function for the specific enthalpy along the phase boundary"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties bpro
    "property record";
  input Real p_der "derivative of pressure";
  output Real h_der
    "time derivative of specific enthalpy along the phase boundary";
algorithm
  h_der := (1/bpro.d - bpro.T*bpro.vt)*p_der + bpro.cp/bpro.dpT*p_der;
end hvl_p_der;
