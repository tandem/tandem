within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions;
function rhovl_p
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input
    ThermoSysPro_H2.Properties.WaterSteam.Common.IF97PhaseBoundaryProperties bpro
    "property record";
  output Units.SI.Density rho "density";
algorithm
  rho := bpro.d;
  annotation (
    derivative(noDerivative=bpro) = rhovl_p_der,
    Inline=false,
    LateInline=true);
end rhovl_p;
