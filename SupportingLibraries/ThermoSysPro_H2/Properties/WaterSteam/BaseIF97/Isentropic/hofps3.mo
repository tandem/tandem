within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic;
function hofps3 "isentropic specific enthalpy in region 3 h(p,s)"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
protected
  Units.SI.Density d "density";
  Units.SI.Temperature T "temperature (K)";
  Units.SI.Pressure delp=IterationData.DELP "iteration accuracy";
  Units.SI.SpecificEntropy dels=IterationData.DELS "iteration accuracy";
  Integer error "error if not 0";
algorithm
  (d,T,error) := Inverses.dtofps3(p=p,s= s,delp= delp,dels= dels);
  h := hofdT3(d, T);
end hofps3;
