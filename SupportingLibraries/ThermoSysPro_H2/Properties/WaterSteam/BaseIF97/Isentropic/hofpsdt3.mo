within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic;
function hofpsdt3
  "isentropic specific enthalpy in region 3 h(p,s) with given good guess in d and T"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input Units.SI.Density dguess
    "good guess density, e.g. from adjacent volume";
  input Units.SI.Temperature Tguess
    "good guess temperature, e.g. from adjacent volume";
  input Units.SI.Pressure delp=IterationData.DELP "relative error in p";
  input Units.SI.SpecificEntropy dels=IterationData.DELS
    "relative error in s";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
protected
  Units.SI.Density d "density";
  Units.SI.Temperature T "temperature (K)";
  Integer error "error flag";
algorithm
  (d,T,error) := Inverses.dtofpsdt3(p=p,s= s,dguess= dguess,Tguess=
    Tguess,delp= delp,dels= dels);
  h := hofdT3(d, T);
end hofpsdt3;
