within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic;
function hofps1 "function for isentropic specific enthalpy in region 1"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
protected
  Units.SI.Temperature T "temperature (K)";
algorithm
  T := Basic.tps1(p, s);
  h := hofpT1(p, T);
end hofps1;
