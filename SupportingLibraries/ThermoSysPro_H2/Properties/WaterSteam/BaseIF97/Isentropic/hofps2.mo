within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic;
function hofps2 "function for isentropic specific enthalpy in region 2"

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
protected
  Units.SI.Temperature T "temperature (K)";
algorithm
  T := Basic.tps2(p, s);
  h := hofpT2(p, T);
end hofps2;
