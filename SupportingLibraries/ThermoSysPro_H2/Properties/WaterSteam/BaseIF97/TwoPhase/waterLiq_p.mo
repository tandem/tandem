within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.TwoPhase;
function waterLiq_p "properties on the liquid phase boundary of region 4"
  import ThermoSysPro = ThermoSysPro_H2;

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  output ThermoSysPro_H2.Properties.Common.PhaseBoundaryProperties liq
    "liquid thermodynamic property collection";
protected
  Units.SI.Temperature Tsat "saturation temperature";
  Real dpT "derivative of saturation pressure wrt temperature";
  Units.SI.Density dl "liquid density";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs f
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
algorithm
  Tsat := Basic.tsat(p);
  dpT := Basic.dptofT(Tsat);
  if p < data.PLIMIT4A then
    g := Basic.g1(p, Tsat);
    liq :=ThermoSysPro_H2.Properties.Common.gibbsToBoundaryProps(g);
  else
    dl := Regions.rhol_p_R4b(p);
    f := Basic.f3(dl, Tsat);
    liq :=ThermoSysPro_H2.Properties.Common.helmholtzToBoundaryProps(f);
  end if;
end waterLiq_p;
