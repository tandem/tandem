within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.TwoPhase;
function waterVap_p "properties on the vapour phase boundary of region 4"
  import ThermoSysPro = ThermoSysPro_H2;

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  output ThermoSysPro_H2.Properties.Common.PhaseBoundaryProperties vap
    "vapour thermodynamic property collection";
protected
  Units.SI.Temperature Tsat "saturation temperature";
  Real dpT "derivative of saturation pressure wrt temperature";
  Units.SI.Density dv "vapour density";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs f
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
algorithm
  Tsat := Basic.tsat(p);
  dpT := Basic.dptofT(Tsat);
  if p < data.PLIMIT4A then
    g := Basic.g2(p, Tsat);
    vap :=ThermoSysPro_H2.Properties.Common.gibbsToBoundaryProps(g);
  else
    dv := Regions.rhov_p_R4b(p);
    f := Basic.f3(dv, Tsat);
    vap :=ThermoSysPro_H2.Properties.Common.helmholtzToBoundaryProps(f);
  end if;
end waterVap_p;
