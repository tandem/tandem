within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.TwoPhase;
function waterR4_ph
  "Water/Steam properties in region 4 of IAPWS/IF97 (two-phase)"
  import ThermoSysPro = ThermoSysPro_H2;

  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_ph pro
    "thermodynamic property collection";
protected
  Units.SI.Density dl "liquid density";
  Units.SI.Density dv "vapour density";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Units.SI.SpecificHeatCapacity cv;
  Real dpT "derivative of saturation curve";
algorithm
  pro.T := Basic.tsat(p);
  dpT := Basic.dptofT(pro.T);
  dl := Regions.rhol_p_R4b(p);
  dv := Regions.rhov_p_R4b(p);
  if p < data.PLIMIT4A then
    gl := Basic.g1(p, pro.T);
    gv := Basic.g2(p, pro.T);
    liq := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(gl);
    vap := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(gv);
  else
    fl := Basic.f3(dl, pro.T);
    fv := Basic.f3(dv, pro.T);
    liq := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(fl);
    vap := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(fv);
  end if;
  pro.x := if (vap.h <> liq.h) then (h - liq.h)/(vap.h - liq.h) else 1.0;
  pro.d := liq.d*vap.d/(vap.d + pro.x*(liq.d - vap.d));
  pro.u := pro.x*vap.u + (1 - pro.x)*liq.u;
  pro.s := pro.x*vap.s + (1 - pro.x)*liq.s;
  pro.cp := Modelica.Constants.inf;
  cv := ThermoSysPro_H2.Properties.WaterSteam.Common.cv2Phase(liq, vap, pro.x, pro.T, p);
  pro.ddph := pro.d*(pro.d*cv/dpT + 1.0)/(dpT*pro.T);
  pro.ddhp := -pro.d*pro.d/(dpT*pro.T);
end waterR4_ph;
