within ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.TwoPhase;
function waterR4_dT "Water properties in region 4 as function of d and T"
  import ThermoSysPro = ThermoSysPro_H2;

  extends Modelica.Icons.Function;
  input Units.SI.Density d "Density";
  input Units.SI.Temperature T "temperature";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.ThermoProperties_dT
                                    pro "thermodynamic property collection";
protected
  Units.SI.Density dl "liquid density";
  Units.SI.Density dv "vapour density";
  ThermoSysPro_H2.Properties.Common.PhaseBoundaryProperties liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.Common.PhaseBoundaryProperties vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.GibbsDerivs gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Real x "dryness fraction";
  Real dpT "derivative of saturation curve";
algorithm
  pro.p := Basic.psat(T);
  dpT := Basic.dptofT(T);
  dl := Regions.rhol_p_R4b(pro.p);
  dv := Regions.rhov_p_R4b(pro.p);
  if pro.p < data.PLIMIT4A then
    gl := Basic.g1(pro.p, T);
    gv := Basic.g2(pro.p, T);
    liq := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(
                                       gl);
    vap := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(
                                       gv);
  else
    fl := Basic.f3(dl, T);
    fv := Basic.f3(dv, T);
    liq := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(
                                           fl);
    vap := ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(
                                           fv);
  end if;
  x := if (vap.d <> liq.d) then (1/d - 1/liq.d)/(1/vap.d - 1/liq.d) else
    1.0;
  pro.u := x*vap.u + (1 - x)*liq.u;
  pro.h := x*vap.h + (1 - x)*liq.h;
  pro.cp := Modelica.Constants.inf;
//  pro.cv := Modelica.Media.Common.cv2Phase(liq, vap, x, T, pro.p);
//   pro.kappa := 1/(d*pro.p)*dpT*dpT*T/pro.cv;
//   pro.a := Modelica.Constants.inf;
//   pro.R := data.RH2O;
  pro.dudT := (pro.p - T*dpT)/(d*d);
end waterR4_dT;
