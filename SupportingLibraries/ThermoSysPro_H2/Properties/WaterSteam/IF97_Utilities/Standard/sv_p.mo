within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function sv_p =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sv_p
  "compute the saturated vapour specific s(p)";
