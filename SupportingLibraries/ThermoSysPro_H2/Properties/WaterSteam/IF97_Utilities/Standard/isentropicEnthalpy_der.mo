within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function isentropicEnthalpy_der
  "derivative of isentropic specific enthalpy from p,s"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  input Real p_der "pressure derivative";
  input Real s_der "entropy derivative";
  output Real h_der "specific enthalpy derivative";
algorithm
  h_der := 1/aux.rho*p_der + aux.T*s_der;
end isentropicEnthalpy_der;
