within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function cv_props_ph
  "specific heat capacity at constant volume as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificHeatCapacity cv "specific heat capacity";
algorithm
  cv := aux.cv;

  annotation (
    Inline=false,
    LateInline=true);
end cv_props_ph;
