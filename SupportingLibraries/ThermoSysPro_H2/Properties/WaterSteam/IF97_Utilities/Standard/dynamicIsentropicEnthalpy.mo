within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function dynamicIsentropicEnthalpy
  "isentropic specific enthalpy from p,s and good guesses of d and T"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input Units.SI.Density dguess
    "good guess density, e.g. from adjacent volume";
  input Units.SI.Temperature Tguess
    "good guess temperature, e.g. from adjacent volume";
  input Integer phase = 0
    "2 for two-phase, 1 for one-phase, 0 if not known";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h :=
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Isentropic.water_hisentropic_dyn(
    p,
    s,
    dguess,
    Tguess,
    0);
end dynamicIsentropicEnthalpy;
