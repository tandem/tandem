within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function s_props_ph
  "specific entropy as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase properties
    "auxiliary record";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := properties.s;

  annotation (derivative(noDerivative=properties) = s_ph_der,
Inline=false,
LateInline=true);
end s_props_ph;
