within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function p_props_dT "pressure as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.Pressure p "pressure";
algorithm
  p := aux.p;

  annotation (
    derivative(noDerivative=aux) = p_dT_der,
    Inline=false,
    LateInline=true);
end p_props_dT;
