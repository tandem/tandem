within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function rhol_T =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhol_T
  "compute the saturated liquid d(T)";
