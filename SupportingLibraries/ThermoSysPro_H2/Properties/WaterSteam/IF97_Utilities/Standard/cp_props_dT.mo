within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function cp_props_dT
  "specific heat capacity at constant pressure as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificHeatCapacity cp "specific heat capacity";
algorithm
  cp := aux.cp;

  annotation (
    Inline=false,
    LateInline=true);
end cp_props_dT;
