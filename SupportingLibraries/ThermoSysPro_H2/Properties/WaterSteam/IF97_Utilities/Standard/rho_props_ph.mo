within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function rho_props_ph
  "density as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase properties
    "auxiliary record";
  output Units.SI.Density rho "density";
algorithm
  rho := properties.rho;

  annotation (
    derivative(noDerivative=properties) = rho_ph_der,
    Inline=false,
    LateInline=true);
end rho_props_ph;
