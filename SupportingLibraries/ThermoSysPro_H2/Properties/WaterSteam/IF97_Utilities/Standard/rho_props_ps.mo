within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function rho_props_ps
  "density as function of pressure and specific entropy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase properties
    "auxiliary record";
  output Units.SI.Density rho "density";
algorithm
  rho := properties.rho;

  annotation (
    Inline=false,
    LateInline=true);
end rho_props_ps;
