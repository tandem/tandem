within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function beta_props_dT
  "isobaric expansion coefficient as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.RelativePressureCoefficient beta
    "isobaric expansion coefficient";
algorithm
  beta := if aux.region == 3 or aux.region == 4 then
    aux.pt/(aux.rho*aux.pd) else
    aux.vt*aux.rho;

  annotation (
    Inline=false,
    LateInline=true);
end beta_props_dT;
