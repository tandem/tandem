within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function s_dT "temperature as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input Integer phase =  0
    "2 for two-phase, 1 for one-phase, 0 if not known";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := s_props_dT(d, T, waterBaseProp_dT(d, T, phase, region));
end s_dT;
