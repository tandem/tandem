within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function beta_props_ph
  "isobaric expansion coefficient as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.RelativePressureCoefficient beta
    "isobaric expansion coefficient";
algorithm
  beta := if aux.region == 3 or aux.region == 4 then
    aux.pt/(aux.rho*aux.pd) else
    aux.vt*aux.rho;

  annotation (
    Inline=false,
    LateInline=true);
end beta_props_ph;
