within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function T_props_ps
  "temperature as function of pressure and specific entropy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase properties
    "auxiliary record";
  output Units.SI.Temperature T "temperature";
algorithm
  T := properties.T;

  annotation (Inline=false,
              LateInline=true);
end T_props_ps;
