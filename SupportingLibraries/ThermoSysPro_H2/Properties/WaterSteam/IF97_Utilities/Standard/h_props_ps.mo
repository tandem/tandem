within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function h_props_ps
  "specific enthalpy as function or pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := aux.h;

  annotation (
    Inline=false,
    LateInline=true);
end h_props_ps;
