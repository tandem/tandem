within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function rhov_p =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhov_p
  "compute the saturated vapour d(p)";
