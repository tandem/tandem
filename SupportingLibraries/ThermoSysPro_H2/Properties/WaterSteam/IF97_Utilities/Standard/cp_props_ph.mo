within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function cp_props_ph
  "specific heat capacity at constant pressure as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificHeatCapacity cp "specific heat capacity";
algorithm
  cp := aux.cp;

  annotation (
    Inline=false,
    LateInline=true);
end cp_props_ph;
