within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function velocityOfSound_props_ph
  "speed of sound as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.Velocity v_sound "speed of sound";
algorithm
  // dp/drho at constant s
  v_sound := if aux.region == 3 then sqrt((aux.pd*aux.rho*aux.rho*aux.cv + aux.pt*aux.pt*aux.T)/(aux.rho*aux.rho*aux.cv)) else
    if aux.region == 4 then
    sqrt(1/((aux.rho*(aux.rho*aux.cv/aux.dpT + 1.0)/(aux.dpT*aux.T)) - 1/aux.rho*aux.rho*aux.rho/(aux.dpT*aux.T))) else
         sqrt(-aux.cp/(aux.rho*aux.rho*(aux.vp*aux.cp+aux.vt*aux.vt*aux.T)));

  annotation (
    Inline=false,
    LateInline=true);
end velocityOfSound_props_ph;
