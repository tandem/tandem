within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function phase_dT "phase as a function of  pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density rho "density";
  input Units.SI.Temperature T "temperature";
  output Integer phase "true if in liquid or gas or supercritical region";
algorithm
  phase := if not ((rho < rhol_T(T) and rho > rhov_T(T)) and T <
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.TCRIT) then 1 else 2;
  annotation (InlineNoEvent=false);
end phase_dT;
