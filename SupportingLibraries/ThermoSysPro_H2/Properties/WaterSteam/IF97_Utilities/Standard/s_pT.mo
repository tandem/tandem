within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function s_pT "temperature as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := s_props_pT(p, T, waterBaseProp_pT(p, T, region));
  annotation (InlineNoEvent=false);
end s_pT;
