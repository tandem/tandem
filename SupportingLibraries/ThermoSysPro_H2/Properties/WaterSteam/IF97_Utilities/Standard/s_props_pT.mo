within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function s_props_pT
  "specific entropy as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := aux.s;

  annotation (
    Inline=false,
    LateInline=true);
end s_props_pT;
