within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function phase_ps "phase as a function of  pressure and specific entropy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  output Integer phase "true if in liquid or gas or supercritical region";
algorithm
  phase := if ((s < sl_p(p) or s > sv_p(p)) or p > ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PCRIT)
     then 1 else 2;
  annotation (InlineNoEvent=false);
end phase_ps;
