within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function rhol_p =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhol_p
  "compute the saturated liquid d(p)";
