within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function velocityOfSound_pT
  "speed of sound as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.Velocity v_sound "speed of sound";
algorithm
  v_sound := velocityOfSound_props_pT(p, T, waterBaseProp_pT(p, T, region));
end velocityOfSound_pT;
