within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function sl_p =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sl_p
  "compute the saturated liquid specific s(p)";
