within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.Standard;
function s_props_dT
  "specific entropy as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := aux.s;

  annotation (
    Inline=false,
    LateInline=true);
end s_props_dT;
