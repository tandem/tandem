within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function h_props_dT
  "specific enthalpy as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := aux.h;

  annotation (
    derivative(noDerivative=aux) = h_dT_der,
    Inline=false,
    LateInline=true);
end h_props_dT;
