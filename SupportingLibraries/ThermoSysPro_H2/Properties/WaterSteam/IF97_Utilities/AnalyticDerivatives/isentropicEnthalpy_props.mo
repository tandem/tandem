within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function isentropicEnthalpy_props
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
  output Units.SI.SpecificEnthalpy h "isentropic enthalpay";
algorithm
  h := aux.h;

  annotation (derivative(noDerivative=aux) = isentropicEnthalpy_der,
Inline=false,
LateInline=true);
end isentropicEnthalpy_props;
