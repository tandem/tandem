within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function ddph_ph_der "derivative function of ddph"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  input Real p_der "derivative of pressure";
  input Real h_der "derivative of specific enthalpy";
  output Real ddph_der "Gradient of ddph";
protected
  Units.SI.SpecificVolume v=1/aux.rho;
  Real detPH "Determinant";
  Real dht;
  Real dhd;
  Real ddhp;
  Real ddph;
  Real dtph;
  Real dthp;
  Real detPH_d;
  Real detPH_t;
  Real dhtt;
  Real dhtd;
  Real ddph_t;
  Real ddph_d;
  Real ddhp_t;
  Real ddhp_d;
algorithm
  if (aux.region == 4) then

    dht := aux.cv + aux.dpT * v;
    dhd := -aux.T * aux.dpT*v*v;
    detPH := -aux.dpT * dhd;
    dtph := 1.0 / aux.dpT;
    ddph := dht / detPH;
    ddhp := -aux.dpT / detPH;
    detPH_t := 2.0 * aux.ptt / aux.dpT + 1.0 / aux.T; /* = detPH_t / detPH */
    detPH_d := -2.0 * v;                   /* = detPH_d / detPH */

    dhtt := aux.cvt + aux.ptt * v;
    dhtd := -(aux.T * aux.ptt + aux.dpT) *v*v;
    ddhp_t := ddhp * (aux.ptt / aux.dpT - detPH_t);
    ddhp_d := ddhp * (-detPH_d);
    ddph_t := ddph * (dhtt / dht - detPH_t);
    ddph_d := ddph * (dhtd / dht - detPH_d);
    ddph_der := (ddph * ddph_d + dtph * ddph_t)*p_der + (ddhp * ddph_d)*h_der;
  else
    detPH := aux.cp*aux.pd;
    dht := aux.cv + aux.pt*v;
    dhd := (aux.pd - aux.T*aux.pt*v)*v;
    ddph := dht/ detPH;
    ddhp := -aux.pt/detPH;
    dtph := -dhd/detPH;
    dthp := aux.pd/detPH;
    detPH_d := aux.cv*aux.pdd + (2.0*aux.pt *(aux.ptd - aux.pt*v)
        -aux.ptt*aux.pd) *aux.T*v*v;
    detPH_t := aux.cvt*aux.pd + aux.cv*aux.ptd +
        (aux.pt + 2.0*aux.T*aux.ptt)*aux.pt*v*v;
    dhtt := aux.cvt + aux.ptt*v;
    dhtd := (aux.ptd - (aux.T * aux.ptt + aux.pt)*v) *v;
    ddph_t := ddph * (dhtt / dht - detPH_t / detPH);
    ddph_d := ddph * (dhtd / dht - detPH_d / detPH);
    ddhp_t := ddhp * (aux.ptt / aux.pt - detPH_t / detPH);
    ddhp_d := ddhp * (aux.ptd / aux.pt - detPH_d / detPH);
    ddph_der := (ddph * ddph_d + dtph * ddph_t)*p_der +
       (ddph * ddhp_d + dtph * ddhp_t)*h_der;

  end if;
end ddph_ph_der;
