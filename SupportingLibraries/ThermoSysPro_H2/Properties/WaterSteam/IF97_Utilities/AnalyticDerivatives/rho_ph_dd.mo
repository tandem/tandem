within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function rho_ph_dd "Second order derivative function of rho_ph"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  input Real p_d "derivative of pressure";
  input Real h_d "derivative of specific enthalpy";
  input Real p_dd "second derivative of pressure";
  input Real h_dd "second derivative of specific enthalpy";
  output Real rho_dd "Second derivative of density";
protected
  Units.SI.DerDensityByPressure ddph "Derivative of d by p at constant h";
  Units.SI.DerDensityByEnthalpy ddhp "Derivative of d by h at constant p";
  Real ddph_ph "Derivative of ddph by p";
  Real ddph_hp "Derivative of ddph by h";
  Real ddhp_hp "Derivative of ddhp by h";
  Real ddhp_ph "Derivative of ddhp by p";
algorithm
  ddph := ddph_props(p,h,aux);
  ddhp := ddhp_props(p,h,aux);
  (ddph_ph,ddph_hp) := ddph_ph_dd(p,h,aux);
  (ddhp_hp,ddhp_ph) := ddhp_ph_dd(p,h,aux);
  rho_dd := ddph*p_dd + 2.0*ddhp_ph*p_d*h_d + ddph_ph*p_d*p_d + ddhp_hp*h_d*h_d + ddhp*h_dd;
end rho_ph_dd;
