within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function h_pT "specific enthalpy as function or pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "Temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := h_props_pT(p, T, waterBasePropAnalytic_pT(p, T, region));
end h_pT;
