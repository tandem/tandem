within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function waterBasePropAnalytic_ph "intermediate property record for water"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input Integer phase =  0
    "phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  input Integer region = 0
    "if 0, do region computation, otherwise assume the region is this input";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  Integer error "error flag for inverse iterations";
  Units.SI.SpecificEnthalpy h_liq "liquid specific enthalpy";
  Units.SI.Density d_liq "liquid density";
  Units.SI.SpecificEnthalpy h_vap "vapour specific enthalpy";
  Units.SI.Density d_vap "vapour density";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Units.SI.Temperature t1
    "temperature at phase boundary, using inverse from region 1";
  Units.SI.Temperature t2
    "temperature at phase boundary, using inverse from region 2";
  /// new stuff, for analytic Jacobian
  Real dxv "der of x wrt v";
  Real dxd "der of x wrt d";
  Real dvTl "der of v wrt T at boiling";
  Real dvTv "der of v wrt T at dew";
  Real dxT "der of x wrt T";
  Real duTl "der of u wrt T at boiling";
  Real duTv "der of u wrt T at dew";
  Real dpTT "2nd der of p wrt T";
  Real dxdd "2nd der of x wrt d";
  Real dxTd "2nd der of x wrt d and T";
  Real dvTTl "2nd der of v wrt T at boiling";
  Real dvTTv "2nd der of v wrt T at dew";
  Real dxTT " 2nd der of x wrt T";
  Real duTTl "2nd der of u wrt T at boiling";
  Real duTTv "2nd der of u wrt T at dew";
  Real vp3 "vp^3";
  Real ivp3 "1/vp3";
  Units.SI.SpecificVolume v;
  // cvt is in aux record
  // Real detpht;
algorithm
  aux.region := if region == 0 then (if phase == 2 then 4 else
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ph(
    p=p,
    h=h,
    phase=phase)) else region;
  aux.phase := if phase <> 0 then phase else if aux.region == 4 then 2 else 1;
  // LogVariable(p);
  // LogVariable(h);
  // LogVariable(aux.region);
  aux.p := max(p,611.657);
  aux.h := max(h,1e3);
  aux.R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  if (aux.region == 1) or (aux.region == 2) or (aux.region == 5) then
    if (aux.region == 1) then
      aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph1(aux.p,
        aux.h);
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, aux.T);
      aux.x := 0.0;
    elseif (aux.region == 2) then
      aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph2(aux.p,
        aux.h);
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, aux.T);
      aux.x := 1.0;
    else /* region must be 5 here */
      (aux.T,error) :=
        ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofph5(
        p=aux.p,
        h=aux.h,
        reldh=1.0e-7);
      assert(error == 0, "error in inverse iteration of steam tables");
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5L3(p, aux.T);
      aux.x := 1.0;
    end if;
    aux.s := aux.R*(g.tau*g.gtau - g.g);
    aux.rho := p/(aux.R*aux.T*g.pi*g.gpi);
    aux.vt := aux.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    aux.vp := aux.R*aux.T/(p*p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    aux.dpT := -aux.vt/aux.vp;
    aux.vtt := aux.R*g.pi/p*g.tau/aux.T*g.tau*g.gpitautau;
    aux.vtp := aux.R*g.pi*g.pi/(p*p)*(g.gpipi - g.tau*g.gpipitau);
    aux.vpp := aux.R*aux.T*g.pi*g.pi*g.pi/(p*p*p)*g.gpipipi;
    aux.cpt := aux.R*g.tau*g.tau/aux.T*(2*g.gtautau + g.tau*g.gtautautau);
    aux.pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    aux.pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/aux.rho;
    vp3 := aux.vp*aux.vp*aux.vp;
    ivp3 := 1/vp3;
    aux.ptt := -(aux.vtt*aux.vp*aux.vp -2.0*aux.vt*aux.vtp*aux.vp +aux.vt*aux.vt*aux.vpp)*ivp3;
    aux.pdd := -aux.vpp*ivp3*v*v*v*v - 2*v*aux.pd "= pvv/d^4";
    aux.ptd := (aux.vtp*aux.vp-aux.vt*aux.vpp)*ivp3*v*v "= -ptv/d^2";
    aux.cvt := (vp3*aux.cpt + aux.vp*aux.vp*aux.vt*aux.vt + 3.0*aux.vp*aux.vp*aux.T*aux.vt*aux.vtt
  - 3.0*aux.vtp*aux.vp*aux.T*aux.vt*aux.vt + aux.T*aux.vt*aux.vt*aux.vt*aux.vpp)*ivp3;
  elseif (aux.region == 3) then
    (aux.rho,aux.T,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofph3(
      p=aux.p,
      h=aux.h,
      delp=1.0e-7,
      delh=1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(aux.rho, aux.T);
    aux.h := aux.R*aux.T*(f.tau*f.ftau + f.delta*f.fdelta);
    aux.s := aux.R*(f.tau*f.ftau - f.f);
    aux.pd := aux.R*aux.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    aux.pt := aux.R*aux.rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    aux.cv := abs(aux.R*(-f.tau*f.tau*f.ftautau))
      "can be close to neg. infinity near critical point";
    aux.cp := (aux.rho*aux.rho*aux.pd*aux.cv + aux.T*aux.pt*aux.pt)/(aux.rho*aux.rho*aux.pd);
    aux.x := 0.0;
    aux.dpT := aux.pt; /*safety against div-by-0 in initialization*/
    aux.pdd := aux.R*aux.T*f.delta/aux.rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    aux.ptt := aux.R*aux.rho*f.delta*f.tau*f.tau/aux.T*f.fdeltatautau;
    aux.ptd := aux.R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
  -f.delta*f.tau*f.fdeltadeltatau);
    aux.cvt := aux.R*f.tau*f.tau/aux.T*(2.0*f.ftautau + f.tau*f.ftautautau);
    aux.cpt := (aux.cvt*aux.pd + aux.cv*aux.ptd + (aux.pt + 2.0*aux.T*aux.ptt)*aux.pt/(aux.rho*aux.rho)
  - aux.cp*aux.ptd)/aux.pd;
  elseif (aux.region == 4) then
    h_liq := hl_p(p);
    h_vap := hv_p(p);
    aux.x := if (h_vap <> h_liq) then (h - h_liq)/(h_vap - h_liq) else 1.0;
    if p < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PLIMIT4A then
      t1 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph1(aux.p,
        h_liq);
      t2 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tph2(aux.p,
        h_vap);
      gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(aux.p, t1);
      gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(aux.p, t2);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gv);
      aux.T := t1 + aux.x*(t2 - t1);
    else
      aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(aux.p);
      // how to avoid ?
      d_liq := rhol_T(aux.T);
      d_vap := rhov_T(aux.T);
      fl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_liq,
        aux.T);
      fv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_vap,
        aux.T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fv);
      //  aux.dpT := BaseIF97.Basic.dptofT(aux.T);
    end if;
    aux.rho := liq.d*vap.d/(vap.d + aux.x*(liq.d - vap.d));
    dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d-vap.d) else 0.0;
    dxd := -dxv/(aux.rho*aux.rho);
    aux.dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*dxv else
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(aux.T);
    dvTl := (liq.pt -aux.dpT)/(liq.pd*liq.d*liq.d);
    dvTv := (vap.pt -aux.dpT)/(vap.pd*vap.d*vap.d);
    dxT := -dxv*(dvTl + aux.x*(dvTv-dvTl));
    duTl := liq.cv + (aux.T*liq.pt-p)*dvTl;
    duTv := vap.cv + (aux.T*vap.pt-p)*dvTv;
    aux.cv := duTl + aux.x*(duTv-duTl) + dxT * (vap.u-liq.u);
    dpTT := dxv*(vap.cv/aux.T-liq.cv/aux.T + dvTv*(vap.pt-aux.dpT)-dvTl*(liq.pt-aux.dpT));
    dxdd := 2.0*dxv/(aux.rho*aux.rho*aux.rho);
    dxTd := dxv*dxv*(dvTv-dvTl)/(aux.rho*aux.rho);
    dvTTl := ((liq.ptt-dpTT)/(liq.d*liq.d) + dvTl*(liq.d*dvTl*(2.0*liq.pd + liq.d*liq.pdd)
         -2.0*liq.ptd))/liq.pd;
    dvTTv := ((vap.ptt-dpTT)/(vap.d*vap.d) + dvTv*(vap.d*dvTv*(2.0*vap.pd + vap.d*vap.pdd)
         -2.0*vap.ptd))/vap.pd;
    dxTT := -dxv*(2.0*dxT*(dvTv-dvTl) + dvTTl + aux.x*(dvTTv-dvTTl));
    duTTl := liq.cvt +(liq.pt-aux.dpT + aux.T*(2.0*liq.ptt -liq.d*liq.d*liq.ptd *dvTl))*dvTl + (aux.T*
      liq.pt - p)*dvTTl;
    duTTv := vap.cvt +(vap.pt-aux.dpT + aux.T*(2.0*vap.ptt -vap.d*vap.d*vap.ptd *dvTv))*dvTv + (aux.T*
      vap.pt - p)*dvTTv;
    aux.cvt := duTTl + aux.x *(duTTv -duTTl) + 2.0*dxT*(duTv-duTl) + dxTT *(vap.u-liq.u);
    aux.s := liq.s + aux.x*(vap.s - liq.s);
    // next ones are only for cases where region 4 is called improperly
    aux.cp := liq.cp + aux.x*(vap.cp - liq.cp); // undefined
    aux.pt := liq.pt + aux.x*(vap.pt - liq.pt); // dpT
    aux.pd := liq.pd + aux.x*(vap.pd - liq.pd); // 0.0
    aux.vt := dvTl + aux.x*(dvTv-dvTl) +dxT*(1/vap.d-1/liq.d);
    // v = vl + x*(vv-vl)
    aux.vp := aux.vt/aux.dpT;
    aux.pdd := 0.0;
    aux.ptd := 0.0;
    aux.ptt := dpTT;
    aux.vtt := dvTTl + aux.x*(dvTTv-dvTTl);
    aux.vtp := aux.vtt/aux.dpT;
    //hpp := vp - T*vtp = aux.vt/aux.dpT - T*aux.vtt/aux.dpT;
  else
    assert(false, "error in region computation of IF97 steam tables"
    + "(p = " + String(p) + ", h = " + String(h) + ")");
  end if;
  annotation (Icon(graphics));
end waterBasePropAnalytic_ph;
