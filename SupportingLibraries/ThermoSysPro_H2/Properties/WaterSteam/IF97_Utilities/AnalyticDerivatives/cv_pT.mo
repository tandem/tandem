within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function cv_pT
  "specific heat capacity at constant volume as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificHeatCapacity cv "specific heat capacity";
algorithm
  cv := cv_props_pT(p, T, waterBasePropAnalytic_pT(p, T, region));
  annotation (InlineNoEvent=false);
end cv_pT;
