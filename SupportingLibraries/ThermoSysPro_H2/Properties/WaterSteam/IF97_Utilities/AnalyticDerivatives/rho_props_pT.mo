within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function rho_props_pT "density as function or pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.Density rho "density";
algorithm
  rho := aux.rho;

  annotation (
    derivative(noDerivative=aux) = rho_pT_der,
    Inline=false,
    LateInline=true);
end rho_props_pT;
