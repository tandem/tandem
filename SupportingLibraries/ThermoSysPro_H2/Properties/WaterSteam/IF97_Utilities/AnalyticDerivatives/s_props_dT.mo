within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function s_props_dT
  "specific entropy as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.SpecificEntropy s "specific entropy";
algorithm
  s := aux.s;

  annotation (
    Inline=false,
    LateInline=true);
end s_props_dT;
