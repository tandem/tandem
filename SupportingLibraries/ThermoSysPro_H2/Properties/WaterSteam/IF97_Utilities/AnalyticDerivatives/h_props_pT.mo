within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function h_props_pT
  "specific enthalpy as function or pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := aux.h;

  annotation (
    derivative(noDerivative=aux) = h_pT_der,
    Inline=false,
    LateInline=true);
end h_props_pT;
