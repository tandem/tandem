within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function rhov_T =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.rhov_T
  "compute the saturated vapour d(T)";
