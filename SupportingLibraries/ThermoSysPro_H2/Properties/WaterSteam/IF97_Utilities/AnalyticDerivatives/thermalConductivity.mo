within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function thermalConductivity =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Transport.cond_industrial_dT
  "compute lambda(d,T) in the one-phase region";
