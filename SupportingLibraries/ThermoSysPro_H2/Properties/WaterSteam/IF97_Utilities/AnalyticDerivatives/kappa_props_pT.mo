within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function kappa_props_pT
  "isothermal compressibility factor as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.IsothermalCompressibility kappa
    "isothermal compressibility factor";
algorithm
  kappa := if aux.region == 3 then
    1/(aux.rho*aux.pd) else -aux.vp*aux.rho;

  annotation (
    Inline=false,
    LateInline=true);
end kappa_props_pT;
