within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function h_dT "specific enthalpy as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "Temperature";
  input Integer phase =  0
    "2 for two-phase, 1 for one-phase, 0 if not known";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificEnthalpy h "specific enthalpy";
algorithm
  h := h_props_dT(d, T, waterBasePropAnalytic_dT(d, T, phase, region));
end h_dT;
