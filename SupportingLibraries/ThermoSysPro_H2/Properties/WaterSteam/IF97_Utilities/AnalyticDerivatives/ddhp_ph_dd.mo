within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function ddhp_ph_dd "Second derivatives of density w.r.t h and p"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Real ddhp_hp "Second derivative of density by h at constant p";
  output Real ddhp_ph "Second mixed derivative of density by p and h";
protected
  Units.SI.SpecificVolume v=1/aux.rho;
  Real detPH "Determinant";
  Real dht;
  Real dhd;
  Real ddhp;
  Real ddph;
  Real dtph;
  Real dthp;
  Real detPH_d;
  Real detPH_t;
  Real dhtt;
  Real dhtd;
  Real ddhp_d;
  Real ddhp_t;
  Real ddph_d;
algorithm
  if (aux.region == 4) then
    dht := aux.cv + aux.dpT * v;
    dhd := -aux.T * aux.dpT*v*v;
    detPH := -aux.dpT * dhd;
    dtph := 1.0 / aux.dpT;
    ddph := dht / detPH;
    ddhp := -aux.dpT / detPH;
    detPH_d := -2.0 * v;                   /* = detPH_d / detPH */
    dhtt := aux.cvt + aux.ptt * v;
    dhtd := -(aux.T * aux.ptt + aux.dpT) *v*v;
    ddhp_d := ddhp * (-detPH_d);
    ddph_d := ddph * (dhtd / dht - detPH_d);
    ddhp_hp := (ddhp * ddhp_d);
    ddhp_ph := (ddhp * ddph_d);
  else
    detPH := aux.cp*aux.pd;
    dht := aux.cv + aux.pt*v;
    dhd := (aux.pd - aux.T*aux.pt*v)*v;
    ddph := dht/ detPH;
    ddhp := -aux.pt/detPH;
    dtph := -dhd/detPH;
    dthp := aux.pd/detPH;
    detPH_d := aux.cv*aux.pdd + (2.0*aux.pt *(aux.ptd - aux.pt*v)
        -aux.ptt*aux.pd) *aux.T*v*v;
    detPH_t := aux.cvt*aux.pd + aux.cv*aux.ptd +
        (aux.pt + 2.0*aux.T*aux.ptt)*aux.pt*v*v;
    dhtt := aux.cvt + aux.ptt*v;
    dhtd := (aux.ptd - (aux.T * aux.ptt + aux.pt)*v) *v;
    ddhp_t := ddhp * (aux.ptt / aux.pt - detPH_t / detPH);
    ddhp_d := ddhp * (aux.ptd / aux.pt - detPH_d / detPH);
    ddhp_hp :=  (ddhp * ddhp_d + dthp * ddhp_t);
    ddhp_hp :=  (ddph * ddhp_d + dtph * ddhp_t);
  end if;
end ddhp_ph_dd;
