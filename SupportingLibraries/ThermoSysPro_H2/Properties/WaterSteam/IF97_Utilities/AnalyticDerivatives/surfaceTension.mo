within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function surfaceTension =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Transport.surfaceTension
  "compute sigma(T) at saturation T";
