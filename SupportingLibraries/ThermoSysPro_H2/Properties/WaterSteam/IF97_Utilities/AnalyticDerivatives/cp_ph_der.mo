within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function cp_ph_der "derivative function of cp_ph"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  input Real p_der "derivative of pressure";
  input Real h_der "derivative of specific enthalpy";
  output Real cp_der "derivative of heat capacity";
protected
  Real detPH "Determinant";
  Real dht;
  Real dhd;
  Real ddhp;
  Real ddph;
  Real dtph;
  Real dthp;
  Real detPH_d;
  Real dcp_d;
algorithm
  if (aux.region == 4) then
    cp_der := 0.0;
  elseif (aux.region == 3) then
    detPH := aux.cp*aux.pd;
    dht := aux.cv + aux.pt/aux.rho;
    dhd := (aux.pd - aux.T*aux.pt/aux.rho)/aux.rho;
    ddph := dht/ detPH;
    ddhp := -aux.pt/detPH;
    dtph := -dhd/detPH;
    dthp := aux.pd/detPH;
    detPH_d := aux.cv*aux.pdd + (2.0*aux.pt *(aux.ptd - aux.pt/aux.rho)
        -aux.ptt*aux.pd) *aux.T/(aux.rho*aux.rho);
    dcp_d :=(detPH_d - aux.cp*aux.pdd)/aux.pd;
    cp_der := (ddph * dcp_d + dtph * aux.cpt)*p_der +
       (ddhp * dcp_d + dthp * aux.cpt)*h_der;
  else
    //regions 1,2 or 5
    cp_der := (-(aux.T * aux.vtt * aux.cp + aux.cpt/aux.rho - aux.cpt * aux.T * aux.vt) / aux.cp)*p_der +
      aux.cpt/aux.cp*h_der;
  end if;
  annotation (Documentation(info="<html></html>"));
end cp_ph_der;
