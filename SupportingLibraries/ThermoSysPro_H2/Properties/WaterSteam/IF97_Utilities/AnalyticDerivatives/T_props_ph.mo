within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function T_props_ph
  "temperature as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic properties
    "auxiliary record";
  output Units.SI.Temperature T "temperature";
algorithm
  T := properties.T;

  annotation (derivative(noDerivative=properties) = T_ph_der,
    Inline=false,
    LateInline=true);
end T_props_ph;
