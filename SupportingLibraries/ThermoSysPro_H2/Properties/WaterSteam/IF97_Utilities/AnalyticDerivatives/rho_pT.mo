within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function rho_pT "density as function or pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.Density rho "density";
algorithm
  rho := rho_props_pT(p, T, waterBasePropAnalytic_pT(p, T, region));
end rho_pT;
