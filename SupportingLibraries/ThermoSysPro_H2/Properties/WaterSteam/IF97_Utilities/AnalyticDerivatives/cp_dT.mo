within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function cp_dT
  "specific heat capacity at constant pressure as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "temperature";
  input Integer phase =  0
    "2 for two-phase, 1 for one-phase, 0 if not known";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Units.SI.SpecificHeatCapacity cp "specific heat capacity";
algorithm
  cp := cp_props_dT(d, T, waterBasePropAnalytic_dT(d, T, phase, region));
end cp_dT;
