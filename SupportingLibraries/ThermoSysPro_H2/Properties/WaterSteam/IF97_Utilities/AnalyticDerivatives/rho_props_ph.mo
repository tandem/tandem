within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function rho_props_ph
  "density as function of pressure and specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.Density rho "density";
algorithm
  rho := aux.rho;

  annotation (
    derivative(noDerivative=aux) = rho_ph_d,
    Inline=false,
    LateInline=true);
end rho_props_ph;
