within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function cp_props_pT
  "specific heat capacity at constant pressure as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.SpecificHeatCapacity cp "specific heat capacity";
algorithm
  cp := if aux.region == 3 then
    (aux.rho*aux.rho*aux.pd*aux.cv + aux.T*aux.pt*aux.pt)/(aux.rho*aux.rho*aux.pd) else
    aux.cp;

  annotation (
    Inline=false,
    LateInline=true);
end cp_props_pT;
