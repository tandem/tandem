within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function ddhp_props "density derivative by specific enthalpy"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEnthalpy h "specific enthalpy";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.DerDensityByEnthalpy ddhp
    "density derivative by specific enthalpy";
algorithm
  ddhp := if aux.region == 3 then
    -aux.rho*aux.rho*aux.pt/(aux.rho*aux.rho*aux.pd*aux.cv + aux.T*aux.pt*aux.pt) else
    if aux.region == 4 then -aux.rho*aux.rho/(aux.dpT*aux.T) else
         -aux.rho*aux.rho*aux.vt/(aux.cp);

  annotation (
    Inline=false,
    LateInline=true,
  derivative(noDerivative=aux) = ddhp_ph_der);
end ddhp_props;
