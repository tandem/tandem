within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function cv_props_dT
  "specific heat capacity at constant volume as function of density and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Density d "density";
  input Units.SI.Temperature T "temperature";
  input ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
  output Units.SI.SpecificHeatCapacity cv "specific heat capacity";
algorithm
  cv := aux.cv;

  annotation (
    Inline=false,
    LateInline=true);
end cv_props_dT;
