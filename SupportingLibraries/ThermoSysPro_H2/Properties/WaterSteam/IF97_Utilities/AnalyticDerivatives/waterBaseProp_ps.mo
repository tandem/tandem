within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function waterBaseProp_ps "intermediate property record for water"
  import ThermoSysPro = ThermoSysPro_H2;
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.SpecificEntropy s "specific entropy";
  input Integer phase = 0
    "phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  input Integer region = 0
    "if 0, do region computation, otherwise assume the region is this input";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.IF97BaseTwoPhase aux
    "auxiliary record";
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  Integer error "error flag for inverse iterations";
  Units.SI.SpecificEntropy s_liq "liquid specific entropy";
  Units.SI.Density d_liq "liquid density";
  Units.SI.SpecificEntropy s_vap "vapour specific entropy";
  Units.SI.Density d_vap "vapour density";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.Common.HelmholtzDerivs fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Units.SI.Temperature t1
    "temperature at phase boundary, using inverse from region 1";
  Units.SI.Temperature t2
    "temperature at phase boundary, using inverse from region 2";
algorithm
  aux.region := if region == 0 then
    (if phase == 2 then 4 else ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_ps(
                                                          p=p,s=s,phase=phase)) else region;
  aux.phase := if phase <> 0 then phase else if aux.region == 4 then 2 else 1;
  aux.p := p;
  aux.s := s;
  aux.R :=ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  if (aux.region == 1) then
    aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps1(
                                 p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(
                           p, aux.T);
    aux.h := aux.R*aux.T*g.tau*g.gtau;
    aux.rho := p/(aux.R*aux.T*g.pi*g.gpi);
    aux.vt := aux.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    aux.vp := aux.R*aux.T/(p*p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau*g.gtaupi)/g.gpipi));
    aux.x := 0.0;
  elseif (aux.region == 2) then
    aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps2(
                                 p, s);
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(
                           p, aux.T);
    aux.h := aux.R*aux.T*g.tau*g.gtau;
    aux.rho := p/(aux.R*aux.T*g.pi*g.gpi);
    aux.vt := aux.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    aux.vp := aux.R*aux.T/(p*p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau*g.gtaupi)/g.gpipi));
    aux.x := 1.0;
  elseif (aux.region == 3) then
    (aux.rho,aux.T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dtofps3(
                                                       p=p,s=s,delp=1.0e-7,dels=
      1.0e-6);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(
                           aux.rho, aux.T);
    aux.h := aux.R*aux.T*(f.tau*f.ftau + f.delta*f.fdelta);
    aux.s := aux.R*(f.tau*f.ftau - f.f);
    aux.pd := aux.R*aux.T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    aux.pt := aux.R*aux.rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    aux.cv := aux.R*(-f.tau*f.tau*f.ftautau);
    aux.cp := (aux.rho*aux.rho*aux.pd*aux.cv + aux.T*aux.pt*aux.pt)/(aux.rho*aux.rho*aux.pd);
    aux.x := 0.0;
  elseif (aux.region == 4) then
    s_liq := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sl_p(
                                   p);
    s_vap := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.sv_p(
                                   p);
    aux.x := if (s_vap <> s_liq) then (s - s_liq)/(s_vap - s_liq) else 1.0;
    if p <ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.PLIMIT4A then
      t1 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps1(
                                p, s_liq);
      t2 := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tps2(
                                p, s_vap);
      gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1(
                              p, t1);
      gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2(
                              p, t2);
      liq := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(gl);
      vap := ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps(gv);
      aux.T := t1 + aux.x*(t2 - t1);
    else
      aux.T := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.tsat(
                                   p);
      d_liq := rhol_T(aux.T);
      d_vap := rhov_T(aux.T);
      fl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(
                              d_liq, aux.T);
      fv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3(
                              d_vap, aux.T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(fl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps(fv);
    end if;
    aux.dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*liq.d*vap.d/(liq.d - vap.d) else
         ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(
                               aux.T);
    aux.h := liq.h + aux.x*(vap.h - liq.h);
    aux.rho := liq.d*vap.d/(vap.d + aux.x*(liq.d - vap.d));
    aux.cv := ThermoSysPro_H2.Properties.WaterSteam.Common.cv2Phase(
          liq,
          vap,
          aux.x,
          aux.T,
          p);
    aux.cp := liq.cp + aux.x*(vap.cp - liq.cp);
    aux.pt := liq.pt + aux.x*(vap.pt - liq.pt);
    aux.pd := liq.pd + aux.x*(vap.pd - liq.pd);
  elseif (aux.region == 5) then
    (aux.T,error) := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.tofps5(
                                              p=p,s=s,relds= 1.0e-7);
    assert(error == 0, "error in inverse iteration of steam tables");
    g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5(
                           p, aux.T);
    aux.h := aux.R*aux.T*g.tau*g.gtau;
    aux.rho := p/(aux.R*aux.T*g.pi*g.gpi);
    aux.vt := aux.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gtaupi);
    aux.vp := aux.R*aux.T/(p*p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gtaupi)*(g.gpi - g.tau*g.gtaupi)/g.gpipi));
  else
    assert(false, "error in region computation of IF97 steam tables"
    + "(p = " + String(p) + ", s = " + String(s) + ")");
  end if;
end waterBaseProp_ps;
