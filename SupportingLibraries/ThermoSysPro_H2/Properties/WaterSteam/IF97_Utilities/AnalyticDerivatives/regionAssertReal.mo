within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function regionAssertReal "assert function for inlining"
  extends Modelica.Icons.Function;
  input Boolean check "condition to check";
  output Real dummy "dummy output";
algorithm
  assert(check, "this function can not be called with two-phase inputs!");
end regionAssertReal;
