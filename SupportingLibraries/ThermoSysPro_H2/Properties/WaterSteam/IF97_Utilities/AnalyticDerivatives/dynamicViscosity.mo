within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function dynamicViscosity =
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Transport.visc_dT
  "compute eta(d,T) in the one-phase region";
