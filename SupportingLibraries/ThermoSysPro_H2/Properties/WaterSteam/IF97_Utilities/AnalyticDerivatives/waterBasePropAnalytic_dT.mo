within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function waterBasePropAnalytic_dT
  "intermediate property record for water (d and T prefered states)"
  extends Modelica.Icons.Function;
  input Units.SI.Density rho "density";
  input Units.SI.Temperature T "temperature";
  input Integer phase =  0
    "phase: 2 for two-phase, 1 for one phase, 0 if unknown";
  input Integer region = 0
    "if 0, do region computation, otherwise assume the region is this input";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
protected
  Units.SI.SpecificEnthalpy h_liq "liquid specific enthalpy";
  Units.SI.Density d_liq "liquid density";
  Units.SI.SpecificEnthalpy h_vap "vapour specific enthalpy";
  Units.SI.Density d_vap "vapour density";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd liq
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.PhaseBoundaryProperties3rd vap
    "phase boundary property record";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gl
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd gv
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fl
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd fv
    "dimensionless Helmholtz function and dervatives wrt delta and tau";
  Integer error "error flag for inverse iterations";
  /// new stuff, for analytic Jacobian
  Real dxv "der of x wrt v";
  Real dxd "der of x wrt d";
  Real dvTl "der of v wrt T at boiling";
  Real dvTv "der of v wrt T at dew";
  Real dxT "der of x wrt T";
  Real duTl "der of u wrt T at boiling";
  Real duTv "der of u wrt T at dew";
  Real dpTT "2nd der of p wrt T";
  Real dxdd "2nd der of x wrt d";
  Real dxTd "2nd der of x wrt d and T";
  Real dvTTl "2nd der of v wrt T at boiling";
  Real dvTTv "2nd der of v wrt T at dew";
  Real dxTT " 2nd der of x wrt T";
  Real duTTl "2nd der of u wrt T at boiling";
  Real duTTv "2nd der of u wrt T at dew";
  // cvt is in aux record
  Real vp3 "vp^3";
  Real ivp3 "1/vp3";
  Units.SI.SpecificVolume v;
algorithm
  aux.region := if region == 0 then (if phase == 2 then 4 else
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_dT(
    d=rho,
    T=T,
    phase=phase)) else region;
  aux.phase := if aux.region == 4 then 2 else 1;
  aux.R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  aux.rho := rho;
  aux.T := T;
  if (aux.region == 1) or (aux.region == 2) or (aux.region == 5) then
    if (aux.region == 1) then
      (aux.p,error) :=
        ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
        d=rho,
        T=T,
        reldd=1.0e-9,
        region=1);
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(aux.p, T);
      aux.x := 0.0;
    elseif (aux.region == 2) then
      (aux.p,error) :=
        ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
        d=rho,
        T=T,
        reldd=1.0e-8,
        region=2);
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(aux.p, T);
      aux.x := 1.0;
    else
      (aux.p,error) :=
        ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.pofdt125(
        d=rho,
        T=T,
        reldd=1.0e-8,
        region=5);
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(aux.p, T);
      aux.x := 1.0;
    end if;
    aux.h := aux.R*aux.T*g.tau*g.gtau;
    aux.s := aux.R*(g.tau*g.gtau - g.g);
    aux.rho := aux.p/(aux.R*T*g.pi*g.gpi);
    aux.vt := aux.R/aux.p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    aux.vp := aux.R*T/(aux.p*aux.p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    aux.vtt := aux.R*g.pi/aux.p*g.tau/aux.T*g.tau*g.gpitautau;
    aux.vtp := aux.R*g.pi*g.pi/(aux.p*aux.p)*(g.gpipi - g.tau*g.gpipitau);
    aux.vpp := aux.R*aux.T*g.pi*g.pi*g.pi/(aux.p*aux.p*aux.p)*g.gpipipi;
    aux.cpt := aux.R*g.tau*g.tau/aux.T*(2*g.gtautau + g.tau*g.gtautautau);
    aux.pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    aux.pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/aux.rho;
    vp3 := aux.vp*aux.vp*aux.vp;
    ivp3 := 1/vp3;
    aux.ptt := -(aux.vtt*aux.vp*aux.vp -2.0*aux.vt*aux.vtp*aux.vp +aux.vt*aux.vt*aux.vpp)*ivp3;
    aux.pdd := -aux.vpp*ivp3*v*v*v*v - 2*v*aux.pd;
    aux.ptd := (aux.vtp*aux.vp-aux.vt*aux.vpp)*ivp3*v*v "= -ptv/d^2";
    aux.cvt := (vp3*aux.cpt + aux.vp*aux.vp*aux.vt*aux.vt + 3.0*aux.vp*aux.vp*aux.T*aux.vt*aux.vtt
  - 3.0*aux.vtp*aux.vp*aux.T*aux.vt*aux.vt + aux.T*aux.vt*aux.vt*aux.vt*aux.vpp)*ivp3;
  elseif (aux.region == 3) then
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(rho, T);
    aux.p := aux.R*rho*T*f.delta*f.fdelta;
    aux.h := aux.R*T*(f.tau*f.ftau + f.delta*f.fdelta);
    aux.s := aux.R*(f.tau*f.ftau - f.f);
    aux.pd := aux.R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    aux.pt := aux.R*rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    aux.cp := (aux.rho*aux.rho*aux.pd*aux.cv + aux.T*aux.pt*aux.pt)/(aux.rho*aux.rho*aux.pd);
    aux.cv := aux.R*(-f.tau*f.tau*f.ftautau);
    aux.x := 0.0;
    aux.dpT := aux.pt; /*safety against div-by-0 in initialization*/
    aux.pdd := aux.R*aux.T*f.delta/aux.rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    aux.ptt := aux.R*aux.rho*f.delta*f.tau*f.tau/aux.T*f.fdeltatautau;
    aux.ptd := aux.R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
  -f.delta*f.tau*f.fdeltadeltatau);
    aux.cvt := aux.R*f.tau*f.tau/aux.T*(2.0*f.ftautau + f.tau*f.ftautautau);
    aux.cpt := (aux.cvt*aux.pd + aux.cv*aux.ptd + (aux.pt + 2.0*aux.T*aux.ptt)*aux.pt/(aux.rho*aux.rho)
  - aux.pt*aux.ptd)/aux.pd;
  elseif (aux.region == 4) then
    aux.p := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.psat(T);
    d_liq := rhol_T(T);
    d_vap := rhov_T(T);
    h_liq := hl_p(aux.p);
    h_vap := hv_p(aux.p);
    aux.x := if (d_vap <> d_liq) then (1/rho - 1/d_liq)/(1/d_vap - 1/d_liq) else 1.0;
    aux.h := h_liq + aux.x*(h_vap - h_liq);
    if T < ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.TLIMIT1 then
      gl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(aux.p, T);
      gv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(aux.p, T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.gibbsToBoundaryProps3rd(gv);
    else
      fl := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_liq, T);
      fv := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(d_vap, T);
      liq :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fl);
      vap :=
        ThermoSysPro_H2.Properties.WaterSteam.Common.helmholtzToBoundaryProps3rd(
        fv);
    end if;
    aux.s := liq.s + aux.x*(vap.s - liq.s);
    dxv := if (liq.d <> vap.d) then liq.d*vap.d/(liq.d-vap.d) else 0.0;
    dxd := -dxv/(aux.rho*aux.rho);
    aux.dpT := if (liq.d <> vap.d) then (vap.s - liq.s)*dxv else
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.dptofT(aux.T);
    dvTl := (liq.pt -aux.dpT)/(liq.pd*liq.d*liq.d);
    dvTv := (vap.pt -aux.dpT)/(vap.pd*vap.d*vap.d);
    dxT := -dxv*(dvTl + aux.x*(dvTv-dvTl));
    duTl := liq.cv + (aux.T*liq.pt-aux.p)*dvTl;
    duTv := vap.cv + (aux.T*vap.pt-aux.p)*dvTv;
    aux.cv := duTl + aux.x*(duTv-duTl) + dxT * (vap.u-liq.u);
    dpTT := dxv*(vap.cv/aux.T-liq.cv/aux.T + dvTv*(vap.pt-aux.dpT)-dvTl*(liq.pt-aux.dpT));
    dxdd := 2.0*dxv/(aux.rho*aux.rho*aux.rho);
    dxTd := dxv*dxv*(dvTv-dvTl)/(aux.rho*aux.rho);
    dvTTl := ((liq.ptt-dpTT)/(liq.d*liq.d) + dvTl*(liq.d*dvTl*(2.0*liq.pd + liq.d*liq.pdd)
         -2.0*liq.ptd))/liq.pd;
    dvTTv := ((vap.ptt-dpTT)/(vap.d*vap.d) + dvTv*(vap.d*dvTv*(2.0*vap.pd + vap.d*vap.pdd)
         -2.0*vap.ptd))/vap.pd;
    dxTT := -dxv*(2.0*dxT*(dvTv-dvTl) + dvTTl + aux.x*(dvTTv-dvTTl));
    duTTl := liq.cvt +(liq.pt-aux.dpT + aux.T*(2.0*liq.ptt -liq.d*liq.d*liq.ptd *dvTl))*dvTl + (aux.T*
            liq.pt - aux.p)*dvTTl;
    duTTv := vap.cvt +(vap.pt-aux.dpT + aux.T*(2.0*vap.ptt -vap.d*vap.d*vap.ptd *dvTv))*dvTv + (aux.T*
            vap.pt - aux.p)*dvTTv;
    aux.cvt := duTTl + aux.x *(duTTv -duTTl) + 2.0*dxT*(duTv-duTl) + dxTT *(vap.u-liq.u);
    aux.cp := liq.cp + aux.x*(vap.cp - liq.cp);
    aux.pt := liq.pt + aux.x*(vap.pt - liq.pt);
    aux.pd := liq.pd + aux.x*(vap.pd - liq.pd);
    aux.ptt := dpTT;
  else
    assert(false, "error in region computation of IF97 steam tables"
    + "(rho = " + String(rho) + ", T = " + String(T) + ")");
  end if;
end waterBasePropAnalytic_dT;
