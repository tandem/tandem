within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function isentropicExponent_pT
  "isentropic exponent as function of pressure and temperature"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region =  0
    "if 0, region is unknown, otherwise known and this input";
  output Real gamma "isentropic exponent";
algorithm
  gamma := isentropicExponent_props_pT(p, T, waterBasePropAnalytic_pT(p, T, region));
  annotation (
    Inline=false,
    LateInline=true);
end isentropicExponent_pT;
