within ThermoSysPro_H2.Properties.WaterSteam.IF97_Utilities.AnalyticDerivatives;
function waterBasePropAnalytic_pT
  "intermediate property record for water (p and T prefered states)"
  extends Modelica.Icons.Function;
  input Units.SI.Pressure p "pressure";
  input Units.SI.Temperature T "temperature";
  input Integer region = 0
    "if 0, do region computation, otherwise assume the region is this input";
  output ThermoSysPro_H2.Properties.WaterSteam.Common.IF97TwoPhaseAnalytic aux
    "auxiliary record";
protected
  ThermoSysPro_H2.Properties.WaterSteam.Common.GibbsDerivs3rd g
    "dimensionless Gibbs funcion and dervatives wrt pi and tau";
  ThermoSysPro_H2.Properties.WaterSteam.Common.HelmholtzDerivs3rd f
    "dimensionless Helmholtz funcion and dervatives wrt delta and tau";
  Real vp3 "vp^3";
  Real ivp3 "1/vp3";
  Units.SI.SpecificVolume v;
  Integer error "error flag for inverse iterations";
algorithm
  aux.phase := 1;
  aux.region := if region == 0 then
    ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Regions.region_pT(p=p, T=T)
     else region;
  aux.R := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.data.RH2O;
  aux.p := p;
  aux.T := T;
  if (aux.region == 1) or (aux.region == 2) or (aux.region == 5) then
    if (aux.region == 1) then
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g1L3(p, T);
      aux.x := 0.0;
    elseif (aux.region == 2) then
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g2L3(p, T);
      aux.x := 1.0;
    else
      g := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.g5L3(p, T);
      aux.x := 1.0;
    end if;
    aux.h := aux.R*aux.T*g.tau*g.gtau;
    aux.s := aux.R*(g.tau*g.gtau - g.g);
    aux.rho := p/(aux.R*T*g.pi*g.gpi);
    aux.vt := aux.R/p*(g.pi*g.gpi - g.tau*g.pi*g.gpitau);
    aux.vp := aux.R*T/(p*p)*g.pi*g.pi*g.gpipi;
    aux.cp := -aux.R*g.tau*g.tau*g.gtautau;
    aux.cv := aux.R*(-g.tau*g.tau*g.gtautau + ((g.gpi - g.tau*g.gpitau)*(g.gpi - g.tau*g.gpitau)/g.gpipi));
    aux.x := 0.0;
    aux.vtt := aux.R*g.pi/aux.p*g.tau/aux.T*g.tau*g.gpitautau;
    aux.vtp := aux.R*g.pi*g.pi/(aux.p*aux.p)*(g.gpipi - g.tau*g.gpipitau);
    aux.vpp := aux.R*aux.T*g.pi*g.pi*g.pi/(aux.p*aux.p*aux.p)*g.gpipipi;
    aux.cpt := aux.R*g.tau*g.tau/aux.T*(2*g.gtautau + g.tau*g.gtautautau);
    aux.pt := -g.p/g.T*(g.gpi - g.tau*g.gpitau)/(g.gpipi*g.pi);
    aux.pd := -g.R*g.T*g.gpi*g.gpi/(g.gpipi);
    v := 1/aux.rho;
    vp3 := aux.vp*aux.vp*aux.vp;
    ivp3 := 1/vp3;
    aux.ptt := -(aux.vtt*aux.vp*aux.vp -2.0*aux.vt*aux.vtp*aux.vp +aux.vt*aux.vt*aux.vpp)*ivp3;
    aux.pdd := -aux.vpp*ivp3*v*v*v*v - 2*v*aux.pd;
    aux.ptd := (aux.vtp*aux.vp-aux.vt*aux.vpp)*ivp3*v*v "= -ptv/d^2";
    aux.cvt := (vp3*aux.cpt + aux.vp*aux.vp*aux.vt*aux.vt + 3.0*aux.vp*aux.vp*aux.T*aux.vt*aux.vtt
  - 3.0*aux.vtp*aux.vp*aux.T*aux.vt*aux.vt + aux.T*aux.vt*aux.vt*aux.vt*aux.vpp)*ivp3;
  elseif (aux.region == 3) then
    (aux.rho,error) :=
      ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Inverses.dofpt3(
      p=p,
      T=T,
      delp=1.0e-7);
    f := ThermoSysPro_H2.Properties.WaterSteam.BaseIF97.Basic.f3L3(aux.rho, T);
    aux.h := aux.R*T*(f.tau*f.ftau + f.delta*f.fdelta);
    aux.s := aux.R*(f.tau*f.ftau - f.f);
    aux.pd := aux.R*T*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta);
    aux.pt := aux.R*aux.rho*f.delta*(f.fdelta - f.tau*f.fdeltatau);
    aux.cv := aux.R*(-f.tau*f.tau*f.ftautau);
    aux.x := 0.0;
    aux.pdd := aux.R*aux.T*f.delta/aux.rho*(2.0*f.fdelta + 4.0*f.delta*f.fdeltadelta +
         f.delta*f.delta*f.fdeltadeltadelta);
    aux.ptt := aux.R*aux.rho*f.delta*f.tau*f.tau/aux.T*f.fdeltatautau;
    aux.ptd := aux.R*f.delta*(2.0*f.fdelta + f.delta*f.fdeltadelta - 2.0*f.tau*f.fdeltatau
  -f.delta*f.tau*f.fdeltadeltatau);
    aux.cvt := aux.R*f.tau*f.tau/aux.T*(2.0*f.ftautau + f.tau*f.ftautautau);
    aux.cpt := (aux.cvt*aux.pd + aux.cv*aux.ptd + (aux.pt + 2.0*aux.T*aux.ptt)*aux.pt/(aux.rho*aux.rho)
  - aux.pt*aux.ptd)/aux.pd;
  else
    assert(false, "error in region computation of IF97 steam tables"
    + "(p = " + String(p) + ", T = " + String(T) + ")");
  end if;
end waterBasePropAnalytic_pT;
