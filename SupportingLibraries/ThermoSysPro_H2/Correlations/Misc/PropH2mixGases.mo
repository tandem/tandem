within ThermoSysPro_H2.Correlations.Misc;
function PropH2mixGases "Computation of the flue gases properties"
  input Units.SI.AbsolutePressure Pmf "Flue gases average pressure";
  input Units.SI.Temperature Tmf "Flue gases average temperature";
  input Real XefCO2 "CO2 mass fraction";
  input Real XefH2O "H2O mass fraction";
  input Real XefO2 "O2 mass fraction";
  input Real XefN2 "N2 mass fraction";
  input Real Xefh2 "H2 mass fraction";

  output Real propf[4] "Flue gases physical properties vector";

protected
  Units.SI.ThermalConductivity condf "Flue gases thermal conductivity";
  Units.SI.SpecificHeatCapacity cpf "Flue gases specific heat capacity";
  Units.SI.DynamicViscosity muf "Flue gases dynamic viscosity";
  Units.SI.Density rhof "Flue gases density";

algorithm
  condf := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_k(
    Pmf,
    Tmf,
    XefCO2,
    XefH2O,
    XefO2,
    Xefh2);
  cpf := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_cp(
    Pmf,
    Tmf,
    XefCO2,
    XefH2O,
    XefO2,
    Xefh2);
  muf := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_mu(
    Pmf,
    Tmf,
    XefCO2,
    XefH2O,
    XefO2,
    Xefh2);
  rhof := ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_rho(
    Pmf,
    Tmf,
    XefCO2,
    XefH2O,
    XefO2,
    Xefh2);

  propf[1] := condf;
  propf[2] := cpf;
  propf[3] := muf;
  propf[4] := rhof;

  annotation (
    smoothOrder=2,
    Documentation(revisions="<html>
<p><u><b>Author</b></u></p>
<ul>
<li>Baligh El Hefni </li>
</ul>
</html>",
   info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</h4>
</HTML>
"));
end PropH2mixGases;
