within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
type WaterSteamFluidType = enumeration(
    WaterSteam "1 - IF97 water/steam (compressible)",
    WaterSteamSimple "7 - Simplified water/steam (compressible)");
