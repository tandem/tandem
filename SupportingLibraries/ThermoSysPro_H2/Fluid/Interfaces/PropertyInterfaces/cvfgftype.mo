within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
function cvfgftype "Converts flue gases fluid type into fluid type"
  import ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType;
  import
    ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.H2mixGasesFluidType;

  input H2mixGasesFluidType fgftype;
  output FluidType ftype;

algorithm

  assert(fgftype == H2mixGasesFluidType.H2mixGases, "cvfgftype: wrong fluid type");

  ftype := FluidType.H2mixGases;

end cvfgftype;
