within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
type H2mixGasesFluidType = enumeration(
    H2mixGases "3 - Flue gases (compressible)");
