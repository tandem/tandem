within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
type FluidType = enumeration(
    WaterSteam "1 - IF97 water/steam (compressible)",
    C3H3F5 "2 - C3H3F5",
    H2mixGases "3 - H2 mix gases (compressible)",
    MoltenSalt "4 - Molten salt",
    Oil_TherminolVP1 "5 - Oil Therminol VP1",
    DryAirIdealGas "6 - Dry air (compressible)",
    WaterSteamSimple "7 - Simplified water/steam (compressible)");
