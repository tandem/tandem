within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
partial model H2mixGasesFluidTypeParameterInterface
  "Interface to display the  flue gases fluid type after parametrization"
  import ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType;
  import
    ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.H2mixGasesFluidType;

  parameter H2mixGasesFluidType fgftype=H2mixGasesFluidType.H2mixGases "Flue gases fluid type" annotation(Evaluate=true, Dialog(tab="Fluid", group="Fluid properties"));

protected
  parameter Integer fgfluid=Integer(fgftype) "Fluid number" annotation(Evaluate=true);

protected
  parameter FluidType ftype=cvfgftype(fgftype) annotation(Evaluate=true);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
                                            Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2020</b> </p>
<p><b>ThermoSysPro Version 4.0</b> </p>
</html>"));
end H2mixGasesFluidTypeParameterInterface;
