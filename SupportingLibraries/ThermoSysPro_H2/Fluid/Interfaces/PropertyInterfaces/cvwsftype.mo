within ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces;
function cvwsftype "Converts water/steam fluid type into fluid type"
  import ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType;
  import
    ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.WaterSteamFluidType;

  input WaterSteamFluidType wsftype;
  output FluidType ftype;

algorithm

  assert((wsftype == WaterSteamFluidType.WaterSteam) or (wsftype == WaterSteamFluidType.WaterSteamSimple), "cvwsftype: wrong fluid type");

  ftype := if wsftype == WaterSteamFluidType.WaterSteam then FluidType.WaterSteam
           else FluidType.WaterSteamSimple;

end cvwsftype;
