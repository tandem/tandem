within ThermoSysPro_H2.Units.SI;
type EnergyFluence = Real (final quantity="EnergyFluence", final unit="J/m2");
