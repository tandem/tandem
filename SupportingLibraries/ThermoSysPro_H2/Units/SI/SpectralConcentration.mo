within ThermoSysPro_H2.Units.SI;
type SpectralConcentration = Real (final quantity="SpectralConcentration",
      final unit="s/m3");
