within ThermoSysPro_H2.Units.SI;
type ElectricPotential = Real (final quantity="ElectricPotential", final unit=
       "V");
