within ThermoSysPro_H2.Units.SI;
operator record ComplexElectricFlux =
  Complex(redeclare ThermoSysPro_H2.Units.SI.ElectricFlux re,
           redeclare ThermoSysPro_H2.Units.SI.ElectricFlux im)
  "Complex electric flux";
