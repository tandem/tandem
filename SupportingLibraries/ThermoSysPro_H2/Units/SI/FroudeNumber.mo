within ThermoSysPro_H2.Units.SI;
type FroudeNumber = Real (final quantity="FroudeNumber", final unit="1");
