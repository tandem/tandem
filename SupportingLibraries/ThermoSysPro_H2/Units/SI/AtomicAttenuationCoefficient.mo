within ThermoSysPro_H2.Units.SI;
type AtomicAttenuationCoefficient = Real (final quantity=
        "AtomicAttenuationCoefficient", final unit="m2");
