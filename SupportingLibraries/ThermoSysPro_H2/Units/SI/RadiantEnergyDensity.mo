within ThermoSysPro_H2.Units.SI;
type RadiantEnergyDensity = Real (final quantity="EnergyDensity", final unit=
        "J/m3");
