within ThermoSysPro_H2.Units.SI;
type NuclearPrecessionAngularFrequency = Real (final quantity=
        "AngularFrequency", final unit="s-1");
