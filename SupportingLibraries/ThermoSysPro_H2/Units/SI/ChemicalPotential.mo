within ThermoSysPro_H2.Units.SI;
type ChemicalPotential = Real (final quantity="ChemicalPotential", final unit=
       "J/mol");
