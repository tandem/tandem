within ThermoSysPro_H2.Units.SI;
type NusseltNumber = Real (final quantity="NusseltNumber", final unit="1");
