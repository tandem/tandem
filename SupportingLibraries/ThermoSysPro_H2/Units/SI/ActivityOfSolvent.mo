within ThermoSysPro_H2.Units.SI;
type ActivityOfSolvent = Real (final quantity="ActivityOfSolvent", final unit=
       "1");
