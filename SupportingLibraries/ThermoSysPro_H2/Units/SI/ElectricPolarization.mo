within ThermoSysPro_H2.Units.SI;
type ElectricPolarization = Real (final quantity="ElectricPolarization",
      final unit="C/m2");
