within ThermoSysPro_H2.Units.SI;
type ExposureRate = Real (final quantity="ExposureRate", final unit=
        "C/(kg.s)");
