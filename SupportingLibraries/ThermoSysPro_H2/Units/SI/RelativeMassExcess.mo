within ThermoSysPro_H2.Units.SI;
type RelativeMassExcess = Real (final quantity="RelativeMassExcess", final unit=
           "1");
