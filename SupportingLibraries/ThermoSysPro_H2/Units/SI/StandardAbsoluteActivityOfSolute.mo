within ThermoSysPro_H2.Units.SI;
type StandardAbsoluteActivityOfSolute = Real (final quantity=
        "StandardAbsoluteActivityOfSolute", final unit="1");
