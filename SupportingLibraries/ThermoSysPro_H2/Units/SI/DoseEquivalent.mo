within ThermoSysPro_H2.Units.SI;
type DoseEquivalent = Real (final quantity="DoseEquivalent", final unit="Sv");
