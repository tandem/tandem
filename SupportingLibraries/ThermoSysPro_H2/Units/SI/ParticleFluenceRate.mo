within ThermoSysPro_H2.Units.SI;
type ParticleFluenceRate = Real (final quantity="ParticleFluenceRate", final unit=
           "s-1.m2");
