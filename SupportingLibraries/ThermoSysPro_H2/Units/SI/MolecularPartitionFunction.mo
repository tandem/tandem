within ThermoSysPro_H2.Units.SI;
type MolecularPartitionFunction = Real (final quantity=
        "MolecularPartitionFunction", final unit="1");
