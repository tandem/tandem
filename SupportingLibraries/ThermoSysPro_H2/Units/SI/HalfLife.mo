within ThermoSysPro_H2.Units.SI;
type HalfLife = Real (final quantity="Time", final unit="s");
