within ThermoSysPro_H2.Units.SI;
operator record ComplexPower =
  Complex(redeclare ActivePower re,
           redeclare ReactivePower im) "Complex electrical power";
