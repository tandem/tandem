within ThermoSysPro_H2.Units.SI;
type LoudnessLevel = Real (final quantity="LoudnessLevel", final unit=
        "phon");
