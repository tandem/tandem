within ThermoSysPro_H2.Units.SI;
type Mass = Real (
    quantity="Mass",
    final unit="kg",
    min=0);
