within ThermoSysPro_H2.Units.SI;
type LewisNumber = Real (final quantity="LewisNumber", final unit="1");
