within ThermoSysPro_H2.Units.SI;
type LuminousEfficacy = Real (final quantity="LuminousEfficacy", final unit=
        "lm/W");
