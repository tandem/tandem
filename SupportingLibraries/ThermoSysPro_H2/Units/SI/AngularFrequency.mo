within ThermoSysPro_H2.Units.SI;
type AngularFrequency = Real (final quantity="AngularFrequency", final unit=
        "rad/s");
