within ThermoSysPro_H2.Units.SI;
type FaradayConstant = Real (final quantity="FaradayConstant", final unit=
        "C/mol");
