within ThermoSysPro_H2.Units.SI;
type EulerNumber = Real (final quantity="EulerNumber", final unit="1");
