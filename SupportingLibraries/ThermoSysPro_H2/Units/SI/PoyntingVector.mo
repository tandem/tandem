within ThermoSysPro_H2.Units.SI;
type PoyntingVector = Real (final quantity="PoyntingVector", final unit=
        "W/m2");
