within ThermoSysPro_H2.Units.SI;
type SectionModulus = Real (final quantity="SectionModulus", final unit="m3");
