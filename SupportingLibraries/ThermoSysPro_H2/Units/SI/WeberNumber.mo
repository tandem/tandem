within ThermoSysPro_H2.Units.SI;
type WeberNumber = Real (final quantity="WeberNumber", final unit="1");
