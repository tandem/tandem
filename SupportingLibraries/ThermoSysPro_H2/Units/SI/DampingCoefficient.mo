within ThermoSysPro_H2.Units.SI;
type DampingCoefficient = Real (final quantity="DampingCoefficient", final unit=
           "s-1");
