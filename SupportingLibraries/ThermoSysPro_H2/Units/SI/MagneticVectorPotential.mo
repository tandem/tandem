within ThermoSysPro_H2.Units.SI;
type MagneticVectorPotential = Real (final quantity="MagneticVectorPotential",
        final unit="Wb/m");
