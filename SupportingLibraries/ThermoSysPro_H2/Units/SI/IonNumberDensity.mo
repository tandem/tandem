within ThermoSysPro_H2.Units.SI;
type IonNumberDensity = Real (final quantity="IonNumberDensity", final unit=
        "m-3");
