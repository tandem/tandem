within ThermoSysPro_H2.Units.SI;
type AcousticAbsorptionCoefficient = Real (final quantity=
        "AcousticAbsorptionCoefficient", final unit="1");
