within ThermoSysPro_H2.Units.SI;
type PropagationCoefficient = Real (final quantity="PropagationCoefficient",
      final unit="m-1");
