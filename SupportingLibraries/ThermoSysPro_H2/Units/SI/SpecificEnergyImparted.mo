within ThermoSysPro_H2.Units.SI;
type SpecificEnergyImparted = Real (final quantity="SpecificEnergy", final unit=
           "Gy");
