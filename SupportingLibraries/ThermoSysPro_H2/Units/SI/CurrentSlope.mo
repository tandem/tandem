within ThermoSysPro_H2.Units.SI;
type CurrentSlope = Real(final quantity="CurrentSlope", final unit="A/s");
