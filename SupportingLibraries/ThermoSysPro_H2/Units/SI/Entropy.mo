within ThermoSysPro_H2.Units.SI;
type Entropy = Real (final quantity="Entropy", final unit="J/K");
