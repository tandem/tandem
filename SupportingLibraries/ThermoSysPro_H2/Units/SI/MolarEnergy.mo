within ThermoSysPro_H2.Units.SI;
type MolarEnergy = Real (final quantity="MolarEnergy", final unit="J/mol", nominal=2e4);
