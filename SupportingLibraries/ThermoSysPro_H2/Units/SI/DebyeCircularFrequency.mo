within ThermoSysPro_H2.Units.SI;
type DebyeCircularFrequency = Real (final quantity="AngularFrequency", final unit=
           "s-1");
