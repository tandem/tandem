within ThermoSysPro_H2.Units.SI;
type ElectricPolarizabilityOfAMolecule = Real (final quantity=
        "ElectricPolarizabilityOfAMolecule", final unit="C.m2/V");
