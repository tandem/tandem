within ThermoSysPro_H2.Units.SI;
type RichardsonConstant = Real (final quantity="RichardsonConstant", final unit=
           "A/(m2.K2)");
