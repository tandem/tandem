within ThermoSysPro_H2.Units.SI;
type ElectricSusceptibility = Real (final quantity="ElectricSusceptibility",
      final unit="1");
