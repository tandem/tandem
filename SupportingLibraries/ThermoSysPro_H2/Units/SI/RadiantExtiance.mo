within ThermoSysPro_H2.Units.SI;
type RadiantExtiance = Real (final quantity="RadiantExtiance", final unit=
        "W/m2");
