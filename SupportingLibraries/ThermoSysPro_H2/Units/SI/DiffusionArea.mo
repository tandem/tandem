within ThermoSysPro_H2.Units.SI;
type DiffusionArea = Real (final quantity="Area", final unit="m2");
