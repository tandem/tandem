within ThermoSysPro_H2.Units.SI;
type ThermalDiffusionFactor = Real (final quantity="ThermalDiffusionFactor",
      final unit="1");
