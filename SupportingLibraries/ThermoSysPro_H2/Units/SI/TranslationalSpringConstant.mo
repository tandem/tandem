within ThermoSysPro_H2.Units.SI;
type TranslationalSpringConstant=Real(final quantity="TranslationalSpringConstant", final unit="N/m");
