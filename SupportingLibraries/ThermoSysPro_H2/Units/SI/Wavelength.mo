within ThermoSysPro_H2.Units.SI;
type Wavelength = Real (final quantity="Wavelength", final unit="m");
