within ThermoSysPro_H2.Units.SI;
type SpecificActivity = Real (final quantity="SpecificActivity", final unit=
        "Bq/kg");
