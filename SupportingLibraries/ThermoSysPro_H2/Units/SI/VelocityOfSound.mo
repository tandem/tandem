within ThermoSysPro_H2.Units.SI;
type VelocityOfSound = Real (final quantity="Velocity", final unit="m/s");
