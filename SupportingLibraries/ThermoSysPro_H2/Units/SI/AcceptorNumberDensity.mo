within ThermoSysPro_H2.Units.SI;
type AcceptorNumberDensity = Real (final quantity="AcceptorNumberDensity",
      final unit="m-3");
