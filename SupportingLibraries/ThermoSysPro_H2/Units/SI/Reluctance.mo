within ThermoSysPro_H2.Units.SI;
type Reluctance = Real (final quantity="Reluctance", final unit="H-1");
