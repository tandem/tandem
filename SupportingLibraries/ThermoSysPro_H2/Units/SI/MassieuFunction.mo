within ThermoSysPro_H2.Units.SI;
type MassieuFunction = Real (final quantity="MassieuFunction", final unit=
        "J/K");
