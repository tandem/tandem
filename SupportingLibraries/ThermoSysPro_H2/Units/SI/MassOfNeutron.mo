within ThermoSysPro_H2.Units.SI;
type MassOfNeutron = Real (final quantity="Mass", final unit="kg");
