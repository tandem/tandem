within ThermoSysPro_H2.Units.SI;
type MagneticReynoldsNumber = Real (final quantity="MagneticReynoldsNumber",
      final unit="1");
