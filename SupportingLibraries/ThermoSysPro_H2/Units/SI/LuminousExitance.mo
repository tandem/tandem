within ThermoSysPro_H2.Units.SI;
type LuminousExitance = Real (final quantity="LuminousExitance", final unit=
        "lm/m2");
