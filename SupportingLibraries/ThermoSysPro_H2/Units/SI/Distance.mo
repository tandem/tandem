within ThermoSysPro_H2.Units.SI;
type Distance = Length (min=0);
