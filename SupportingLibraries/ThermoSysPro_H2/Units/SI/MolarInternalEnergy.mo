within ThermoSysPro_H2.Units.SI;
type MolarInternalEnergy = MolarEnergy;
