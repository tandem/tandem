within ThermoSysPro_H2.Units.SI;
type Thickness = Length(min=0);
