within ThermoSysPro_H2.Units.SI;
type Radius = Length(min=0);
