within ThermoSysPro_H2.Units.SI;
type ThermodynamicTemperature = Real (
    final quantity="ThermodynamicTemperature",
    final unit="K",
    min = 0.0,
    start = 288.15,
    nominal = 300,
    displayUnit="degC")
  "Absolute temperature (use type TemperatureDifference for relative temperatures)"                   annotation(absoluteValue=true);
