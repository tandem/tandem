within ThermoSysPro_H2.Units.SI;
type Time = Real (final quantity="Time", final unit="s");
