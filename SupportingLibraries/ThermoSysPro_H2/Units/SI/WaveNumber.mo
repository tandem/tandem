within ThermoSysPro_H2.Units.SI;
type WaveNumber = Real (final quantity="WaveNumber", final unit="m-1");
