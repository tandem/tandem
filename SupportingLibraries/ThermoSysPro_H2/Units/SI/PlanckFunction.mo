within ThermoSysPro_H2.Units.SI;
type PlanckFunction = Real (final quantity="PlanckFunction", final unit="J/K");
