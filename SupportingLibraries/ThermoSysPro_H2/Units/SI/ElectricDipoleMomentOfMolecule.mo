within ThermoSysPro_H2.Units.SI;
type ElectricDipoleMomentOfMolecule = Real (final quantity=
        "ElectricDipoleMomentOfMolecule", final unit="C.m");
