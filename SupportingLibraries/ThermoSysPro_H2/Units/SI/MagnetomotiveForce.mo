within ThermoSysPro_H2.Units.SI;
type MagnetomotiveForce = Real (final quantity="MagnetomotiveForce", final unit=
           "A");
