within ThermoSysPro_H2.Units.SI;
type SpectralAngularCrossSection = Real (final quantity=
        "SpectralAngularCrossSection", final unit="m2/(sr.J)");
