within ThermoSysPro_H2.Units.SI;
type ChargeNumberOfIon = Real (final quantity="ChargeNumberOfIon", final unit=
       "1");
