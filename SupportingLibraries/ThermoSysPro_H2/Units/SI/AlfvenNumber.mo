within ThermoSysPro_H2.Units.SI;
type AlfvenNumber = Real (final quantity="AlfvenNumber", final unit="1");
