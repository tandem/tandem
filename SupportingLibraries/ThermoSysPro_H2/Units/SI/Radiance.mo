within ThermoSysPro_H2.Units.SI;
type Radiance = Real (final quantity="Radiance", final unit="W/(sr.m2)");
