within ThermoSysPro_H2.Units.SI;
type MolarVolume = Real (final quantity="MolarVolume", final unit="m3/mol", min=0);
