within ThermoSysPro_H2.Units.SI;
type Permeability = Real (final quantity="Permeability", final unit="H/m");
