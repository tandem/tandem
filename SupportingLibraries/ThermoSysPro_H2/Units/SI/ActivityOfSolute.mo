within ThermoSysPro_H2.Units.SI;
type ActivityOfSolute = Real (final quantity="ActivityOfSolute", final unit=
        "1");
