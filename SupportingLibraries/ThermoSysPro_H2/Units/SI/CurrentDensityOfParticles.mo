within ThermoSysPro_H2.Units.SI;
type CurrentDensityOfParticles = Real (final quantity=
        "CurrentDensityOfParticles", final unit="m-2.s-1");
