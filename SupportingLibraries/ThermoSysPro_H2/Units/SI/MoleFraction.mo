within ThermoSysPro_H2.Units.SI;
type MoleFraction = Real (final quantity="MoleFraction", final unit="1",
                          min = 0, max = 1);
