within ThermoSysPro_H2.Units.SI;
type ReactionEnergy = Real (final quantity="Energy", final unit="J");
