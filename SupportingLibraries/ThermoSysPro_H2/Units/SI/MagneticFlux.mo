within ThermoSysPro_H2.Units.SI;
type MagneticFlux = Real (final quantity="MagneticFlux", final unit="Wb");
