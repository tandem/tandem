within ThermoSysPro_H2.Units.SI;
type StandardAbsoluteActivityOfSolvent = Real (final quantity=
        "StandardAbsoluteActivityOfSolvent", final unit="1");
