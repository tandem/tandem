within ThermoSysPro_H2.Units.SI;
type ElectricCurrent = Real (final quantity="ElectricCurrent", final unit="A");
