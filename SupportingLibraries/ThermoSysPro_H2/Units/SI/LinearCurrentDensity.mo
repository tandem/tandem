within ThermoSysPro_H2.Units.SI;
type LinearCurrentDensity = Real (final quantity="LinearCurrentDensity",
      final unit="A/m");
