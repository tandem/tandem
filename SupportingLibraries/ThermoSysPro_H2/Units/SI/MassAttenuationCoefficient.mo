within ThermoSysPro_H2.Units.SI;
type MassAttenuationCoefficient = Real (final quantity=
        "MassAttenuationCoefficient", final unit="m2/kg");
