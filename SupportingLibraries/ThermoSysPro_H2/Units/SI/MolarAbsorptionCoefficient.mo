within ThermoSysPro_H2.Units.SI;
type MolarAbsorptionCoefficient = Real (final quantity=
        "MolarAbsorptionCoefficient", final unit="m2/mol");
