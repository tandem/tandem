within ThermoSysPro_H2.Units.SI;
type PowerLevelDifference = Real (final quantity="PowerLevelDifference",
      final unit="dB");
