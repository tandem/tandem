within ThermoSysPro_H2.Units.SI;
type HartmannNumber = Real (final quantity="HartmannNumber", final unit="1");
