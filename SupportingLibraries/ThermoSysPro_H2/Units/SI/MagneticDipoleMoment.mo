within ThermoSysPro_H2.Units.SI;
type MagneticDipoleMoment = Real (final quantity="MagneticDipoleMoment",
      final unit="Wb.m");
