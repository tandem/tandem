within ThermoSysPro_H2.Units.SI;
type MassOfProton = Real (final quantity="Mass", final unit="kg");
