within ThermoSysPro_H2.Units.SI;
type DiffusionCoefficient = Real (final quantity="DiffusionCoefficient",
      final unit="m2/s");
