within ThermoSysPro_H2.Units.SI;
type RayleighNumber = Real (final quantity="RayleighNumber", final unit="1");
