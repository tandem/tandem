within ThermoSysPro_H2.Units.SI;
type StantonNumberOfMassTransfer = Real (final quantity=
        "StantonNumberOfMassTransfer", final unit="1");
