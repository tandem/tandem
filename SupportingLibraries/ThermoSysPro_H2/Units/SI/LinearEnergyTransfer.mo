within ThermoSysPro_H2.Units.SI;
type LinearEnergyTransfer = Real (final quantity="LinearEnergyTransfer",
      final unit="J/m");
