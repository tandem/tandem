within ThermoSysPro_H2.Units.SI;
type RelativeAtomicMass = Real (final quantity="RelativeAtomicMass", final unit=
           "1");
