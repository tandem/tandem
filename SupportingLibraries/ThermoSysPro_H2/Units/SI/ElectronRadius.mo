within ThermoSysPro_H2.Units.SI;
type ElectronRadius = Real (final quantity="Length", final unit="m");
