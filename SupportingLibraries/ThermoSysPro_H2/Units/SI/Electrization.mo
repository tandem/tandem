within ThermoSysPro_H2.Units.SI;
type Electrization = Real (final quantity="Electrization", final unit="V/m");
