within ThermoSysPro_H2.Units.SI;
type Angle = Real (
    final quantity="Angle",
    final unit="rad",
    displayUnit="deg");
