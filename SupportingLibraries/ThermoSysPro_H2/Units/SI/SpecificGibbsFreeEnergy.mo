within ThermoSysPro_H2.Units.SI;
type SpecificGibbsFreeEnergy = SpecificEnergy;
