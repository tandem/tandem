within ThermoSysPro_H2.Units.SI;
type SoundPressureLevel = Real (final quantity="SoundPressureLevel", final unit=
           "dB");
