within ThermoSysPro_H2.Units.SI;
type FastFissionFactor = Real (final quantity="FastFissionFactor", final unit=
       "1");
