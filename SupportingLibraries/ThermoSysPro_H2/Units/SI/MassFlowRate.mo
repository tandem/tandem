within ThermoSysPro_H2.Units.SI;
type MassFlowRate = Real (quantity="MassFlowRate", final unit="kg/s");
