within ThermoSysPro_H2.Units.SI;
type ProtonNumber = Real (final quantity="ProtonNumber", final unit="1");
