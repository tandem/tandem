within ThermoSysPro_H2.Units.SI;
type SoundPowerLevel = Real (final quantity="SoundPowerLevel", final unit=
        "dB");
