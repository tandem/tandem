within ThermoSysPro_H2.Units.SI;
type SelfInductance = Inductance(min=0);
