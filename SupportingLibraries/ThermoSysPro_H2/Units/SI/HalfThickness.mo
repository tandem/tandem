within ThermoSysPro_H2.Units.SI;
type HalfThickness = Real (final quantity="Length", final unit="m");
