within ThermoSysPro_H2.Units.SI;
type AmplitudeLevelDifference = Real (final quantity=
        "AmplitudeLevelDifference", final unit="dB");
