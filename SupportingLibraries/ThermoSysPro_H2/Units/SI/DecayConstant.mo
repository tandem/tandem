within ThermoSysPro_H2.Units.SI;
type DecayConstant = Real (final quantity="DecayConstant", final unit="s-1");
