within ThermoSysPro_H2.Units.SI;
type DirectionalSpectralEmissivity = Real (final quantity=
        "DirectionalSpectralEmissivity", final unit="1");
