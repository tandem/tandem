within ThermoSysPro_H2.Units.SI;
type RelativePermittivity = Real (final quantity="RelativePermittivity",
      final unit="1");
