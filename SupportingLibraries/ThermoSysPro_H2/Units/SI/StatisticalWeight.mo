within ThermoSysPro_H2.Units.SI;
type StatisticalWeight = Real (final quantity="StatisticalWeight", final unit=
       "1");
