within ThermoSysPro_H2.Units.SI;
type DensityOfHeatFlowRate = Real (final quantity="DensityOfHeatFlowRate",
      final unit="W/m2");
