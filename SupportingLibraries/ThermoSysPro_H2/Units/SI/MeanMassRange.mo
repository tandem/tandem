within ThermoSysPro_H2.Units.SI;
type MeanMassRange = Real (final quantity="MeanMassRange", final unit="kg/m2");
