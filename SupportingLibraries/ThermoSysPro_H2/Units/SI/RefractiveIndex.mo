within ThermoSysPro_H2.Units.SI;
type RefractiveIndex = Real (final quantity="RefractiveIndex", final unit="1");
