within ThermoSysPro_H2.Units.SI;
type RatioOfSpecificHeatCapacities = Real (final quantity=
        "RatioOfSpecificHeatCapacities", final unit="1");
