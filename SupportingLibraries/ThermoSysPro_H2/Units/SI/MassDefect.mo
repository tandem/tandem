within ThermoSysPro_H2.Units.SI;
type MassDefect = Real (final quantity="Mass", final unit="kg");
