within ThermoSysPro_H2.Units.SI;
type ActivePower = Real (final quantity="Power", final unit="W");
