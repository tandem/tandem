within ThermoSysPro_H2.Units.SI;
operator record ComplexMagneticPotential =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagneticPotential re,
           redeclare ThermoSysPro_H2.Units.SI.MagneticPotential im)
  "Complex magnetic potential";
