within ThermoSysPro_H2.Units.SI;
operator record ComplexCurrentDensity =
  Complex(redeclare ThermoSysPro_H2.Units.SI.CurrentDensity re,
           redeclare ThermoSysPro_H2.Units.SI.CurrentDensity im)
  "Complex electrical current density";
