within ThermoSysPro_H2.Units.SI;
type GrashofNumberOfMassTransfer = Real (final quantity=
        "GrashofNumberOfMassTransfer", final unit="1");
