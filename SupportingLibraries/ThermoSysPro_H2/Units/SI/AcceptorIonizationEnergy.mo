within ThermoSysPro_H2.Units.SI;
type AcceptorIonizationEnergy = Real (final quantity="Energy", final unit=
        "eV");
