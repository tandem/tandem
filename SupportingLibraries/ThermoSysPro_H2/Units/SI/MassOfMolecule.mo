within ThermoSysPro_H2.Units.SI;
type MassOfMolecule = Real (final quantity="Mass", final unit="kg");
