within ThermoSysPro_H2.Units.SI;
type SpectralReflectionFactor = Real (final quantity=
        "SpectralReflectionFactor", final unit="1");
