within ThermoSysPro_H2.Units.SI;
type NeutronYieldPerFission = Real (final quantity="NeutronYieldPerFission",
      final unit="1");
