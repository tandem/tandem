within ThermoSysPro_H2.Units.SI;
type AttenuationCoefficient = Real (final quantity="AttenuationCoefficient",
      final unit="m-1");
