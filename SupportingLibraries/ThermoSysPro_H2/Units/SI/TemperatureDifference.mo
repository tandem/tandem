within ThermoSysPro_H2.Units.SI;
type TemperatureDifference = Real (
    final quantity="ThermodynamicTemperature",
    final unit="K") annotation(absoluteValue=false);
