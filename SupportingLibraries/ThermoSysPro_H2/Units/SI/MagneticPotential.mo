within ThermoSysPro_H2.Units.SI;
type MagneticPotential = Real (final quantity="MagneticPotential", final unit="A");
