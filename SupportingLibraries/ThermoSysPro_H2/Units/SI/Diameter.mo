within ThermoSysPro_H2.Units.SI;
type Diameter = Length(min=0);
