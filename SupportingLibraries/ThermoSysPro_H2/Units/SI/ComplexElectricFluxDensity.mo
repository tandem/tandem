within ThermoSysPro_H2.Units.SI;
operator record ComplexElectricFluxDensity =
  Complex(redeclare ThermoSysPro_H2.Units.SI.ElectricFluxDensity re,
           redeclare ThermoSysPro_H2.Units.SI.ElectricFluxDensity im)
  "Complex electric flux density";
