within ThermoSysPro_H2.Units.SI;
type TranslationalDampingConstant=Real(final quantity="TranslationalDampingConstant", final unit="N.s/m");
