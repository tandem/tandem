within ThermoSysPro_H2.Units.SI;
type LinearTemperatureCoefficient = Real(final quantity = "LinearTemperatureCoefficient", final unit="1/K");
