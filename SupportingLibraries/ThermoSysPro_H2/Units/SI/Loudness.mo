within ThermoSysPro_H2.Units.SI;
type Loudness = Real (final quantity="Loudness", final unit="sone");
