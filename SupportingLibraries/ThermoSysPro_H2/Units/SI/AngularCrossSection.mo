within ThermoSysPro_H2.Units.SI;
type AngularCrossSection = Real (final quantity="AngularCrossSection", final unit=
           "m2/sr");
