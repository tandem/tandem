within ThermoSysPro_H2.Units.SI;
type MacroscopicCrossSection = Real (final quantity="MacroscopicCrossSection",
        final unit="m-1");
