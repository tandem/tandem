within ThermoSysPro_H2.Units.SI;
type ApparentPower = Real (final quantity="Power", final unit="VA");
