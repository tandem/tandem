within ThermoSysPro_H2.Units.SI;
type ActivityCoefficient = Real (final quantity="ActivityCoefficient", final unit=
           "1");
