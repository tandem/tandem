within ThermoSysPro_H2.Units.SI;
type GyromagneticCoefficient = Real (final quantity="GyromagneticCoefficient",
        final unit="A.m2/(J.s)");
