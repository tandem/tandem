within ThermoSysPro_H2.Units.SI;
type DerEnthalpyByPressure = Real (final unit="J.m.s2/kg2");
