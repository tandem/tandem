within ThermoSysPro_H2.Units.SI;
type AngularMomentumFlux = Real (final quantity="AngularMomentumFlux", final unit=
           "N.m");
