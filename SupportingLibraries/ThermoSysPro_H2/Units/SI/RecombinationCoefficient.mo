within ThermoSysPro_H2.Units.SI;
type RecombinationCoefficient = Real (final quantity=
        "RecombinationCoefficient", final unit="m3/s");
