within ThermoSysPro_H2.Units.SI;
type NucleonNumber = Real (final quantity="NucleonNumber", final unit="1");
