within ThermoSysPro_H2.Units.SI;
operator record ComplexMagnetomotiveForce =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagnetomotiveForce re,
           redeclare ThermoSysPro_H2.Units.SI.MagnetomotiveForce im)
  "Complex magneto motive force";
