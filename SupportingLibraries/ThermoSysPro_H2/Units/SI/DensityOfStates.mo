within ThermoSysPro_H2.Units.SI;
type DensityOfStates = Real (final quantity="DensityOfStates", final unit=
        "J-1/m-3");
