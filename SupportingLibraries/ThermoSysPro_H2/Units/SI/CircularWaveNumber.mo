within ThermoSysPro_H2.Units.SI;
type CircularWaveNumber = Real (final quantity="CircularWaveNumber", final unit=
           "rad/m");
