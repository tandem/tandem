within ThermoSysPro_H2.Units.SI;
type MagneticFluxDensity = Real (final quantity="MagneticFluxDensity", final unit=
           "T");
