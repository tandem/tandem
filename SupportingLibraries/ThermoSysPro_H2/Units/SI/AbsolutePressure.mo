within ThermoSysPro_H2.Units.SI;
type AbsolutePressure = Pressure (min=0.0, nominal = 1e5);
