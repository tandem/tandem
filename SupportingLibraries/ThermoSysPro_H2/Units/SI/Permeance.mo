within ThermoSysPro_H2.Units.SI;
type Permeance = Real (final quantity="Permeance", final unit="H");
