within ThermoSysPro_H2.Units.SI;
type DerEnergyByPressure = Real (final unit="J.m.s2/kg");
