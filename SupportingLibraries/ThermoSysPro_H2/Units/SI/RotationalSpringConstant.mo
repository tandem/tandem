within ThermoSysPro_H2.Units.SI;
type RotationalSpringConstant=Real(final quantity="RotationalSpringConstant", final unit="N.m/rad");
