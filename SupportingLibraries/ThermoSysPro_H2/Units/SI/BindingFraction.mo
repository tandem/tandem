within ThermoSysPro_H2.Units.SI;
type BindingFraction = Real (final quantity="BindingFraction", final unit="1");
