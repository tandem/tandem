within ThermoSysPro_H2.Units.SI;
type ThermalDiffusionRatio = Real (final quantity="ThermalDiffusionRatio",
      final unit="1");
