within ThermoSysPro_H2.Units.SI;
type SpectralTransmissionFactor = Real (final quantity=
        "SpectralTransmissionFactor", final unit="1");
