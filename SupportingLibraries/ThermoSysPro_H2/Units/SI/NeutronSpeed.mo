within ThermoSysPro_H2.Units.SI;
type NeutronSpeed = Real (final quantity="Velocity", final unit="m/s");
