within ThermoSysPro_H2.Units.SI;
type VoltageSlope = Real(final quantity="VoltageSlope", final unit="V/s");
