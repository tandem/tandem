within ThermoSysPro_H2.Units.SI;
type MolarAttenuationCoefficient = Real (final quantity=
        "MolarAttenuationCoefficient", final unit="m2/mol");
