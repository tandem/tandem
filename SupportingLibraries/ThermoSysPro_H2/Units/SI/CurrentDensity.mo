within ThermoSysPro_H2.Units.SI;
type CurrentDensity = Real (final quantity="CurrentDensity", final unit=
        "A/m2");
