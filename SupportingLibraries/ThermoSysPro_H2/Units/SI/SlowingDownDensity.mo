within ThermoSysPro_H2.Units.SI;
type SlowingDownDensity = Real (final quantity="SlowingDownDensity", final unit=
           "s-1.m-3");
