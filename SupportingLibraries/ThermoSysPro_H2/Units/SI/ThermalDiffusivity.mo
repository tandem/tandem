within ThermoSysPro_H2.Units.SI;
type ThermalDiffusivity = Real (final quantity="ThermalDiffusivity", final unit=
           "m2/s");
