within ThermoSysPro_H2.Units.SI;
type ElectricFieldStrength = Real (final quantity="ElectricFieldStrength",
      final unit="V/m");
