within ThermoSysPro_H2.Units.SI;
type MassConcentration = Real (final quantity="MassConcentration", final unit=
       "kg/m3");
