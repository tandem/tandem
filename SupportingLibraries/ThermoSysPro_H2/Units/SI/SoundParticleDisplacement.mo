within ThermoSysPro_H2.Units.SI;
type SoundParticleDisplacement = Real (final quantity="Length", final unit=
        "m");
