within ThermoSysPro_H2.Units.SI;
type DerEnergyByDensity = Real (final unit="J.m3/kg");
