within ThermoSysPro_H2.Units.SI;
type SoundPower = Real (final quantity="Power", final unit="W");
