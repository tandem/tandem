within ThermoSysPro_H2.Units.SI;
type Activity = Real (final quantity="Activity", final unit="Bq");
