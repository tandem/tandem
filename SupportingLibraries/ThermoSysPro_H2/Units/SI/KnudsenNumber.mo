within ThermoSysPro_H2.Units.SI;
type KnudsenNumber = Real (final quantity="KnudsenNumber", final unit="1");
