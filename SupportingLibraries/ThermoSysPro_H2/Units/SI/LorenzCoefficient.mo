within ThermoSysPro_H2.Units.SI;
type LorenzCoefficient = Real (final quantity="LorenzCoefficient", final unit=
       "V2/K2");
