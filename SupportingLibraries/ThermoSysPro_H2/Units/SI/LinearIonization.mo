within ThermoSysPro_H2.Units.SI;
type LinearIonization = Real (final quantity="LinearIonization", final unit=
        "m-1");
