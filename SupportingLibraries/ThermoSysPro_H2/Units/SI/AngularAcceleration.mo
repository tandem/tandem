within ThermoSysPro_H2.Units.SI;
type AngularAcceleration = Real (final quantity="AngularAcceleration", final unit=
           "rad/s2");
