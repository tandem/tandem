within ThermoSysPro_H2.Units.SI;
type StrouhalNumber = Real (final quantity="StrouhalNumber", final unit="1");
