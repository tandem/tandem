within ThermoSysPro_H2.Units.SI;
type ExchangeIntegral = Real (final quantity="Energy", final unit="eV");
