within ThermoSysPro_H2.Units.SI;
type Stress = Real (final unit="Pa");
