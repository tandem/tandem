within ThermoSysPro_H2.Units.SI;
type OsmoticPressure = Real (
    final quantity="Pressure",
    final unit="Pa",
    displayUnit="bar",
    min=0);
