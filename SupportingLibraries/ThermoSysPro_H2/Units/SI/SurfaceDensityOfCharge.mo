within ThermoSysPro_H2.Units.SI;
type SurfaceDensityOfCharge = Real (
    final quantity="SurfaceDensityOfCharge",
    final unit="C/m2",
    min=0);
