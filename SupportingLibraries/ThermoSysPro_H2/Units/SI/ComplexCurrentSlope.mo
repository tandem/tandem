within ThermoSysPro_H2.Units.SI;
operator record ComplexCurrentSlope =
  Complex(redeclare ThermoSysPro_H2.Units.SI.CurrentSlope re,
           redeclare ThermoSysPro_H2.Units.SI.CurrentSlope im)
  "Complex current slope";
