within ThermoSysPro_H2.Units.SI;
type KinematicViscosity = Real (
    final quantity="KinematicViscosity",
    final unit="m2/s",
    min=0);
