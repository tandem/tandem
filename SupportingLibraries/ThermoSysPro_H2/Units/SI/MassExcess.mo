within ThermoSysPro_H2.Units.SI;
type MassExcess = Real (final quantity="Mass", final unit="kg");
