within ThermoSysPro_H2.Units.SI;
type MagneticFieldStrength = Real (final quantity="MagneticFieldStrength",
      final unit="A/m");
