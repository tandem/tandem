within ThermoSysPro_H2.Units.SI;
type Reactivity = Real (final quantity="Reactivity", final unit="1");
