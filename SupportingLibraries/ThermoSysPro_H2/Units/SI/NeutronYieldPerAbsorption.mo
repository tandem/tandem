within ThermoSysPro_H2.Units.SI;
type NeutronYieldPerAbsorption = Real (final quantity=
        "NeutronYieldPerAbsorption", final unit="1");
