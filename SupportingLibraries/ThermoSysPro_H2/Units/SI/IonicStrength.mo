within ThermoSysPro_H2.Units.SI;
type IonicStrength = Real (final quantity="IonicStrength", final unit=
        "mol/kg");
