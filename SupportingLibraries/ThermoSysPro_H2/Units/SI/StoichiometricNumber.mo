within ThermoSysPro_H2.Units.SI;
type StoichiometricNumber = Real (final quantity="StoichiometricNumber",
      final unit="1");
