within ThermoSysPro_H2.Units.SI;
type Kerma = Real (final quantity="Kerma", final unit="Gy");
