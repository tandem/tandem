within ThermoSysPro_H2.Units.SI;
operator record ComplexCurrent =
  Complex(redeclare ThermoSysPro_H2.Units.SI.Current re,
           redeclare ThermoSysPro_H2.Units.SI.Current im)
  "Complex electrical current";
