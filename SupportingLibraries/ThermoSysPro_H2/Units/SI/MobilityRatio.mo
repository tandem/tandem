within ThermoSysPro_H2.Units.SI;
type MobilityRatio = Real (final quantity="MobilityRatio", final unit="1");
