within ThermoSysPro_H2.Units.SI;
type VolumeFlowRate = Real (final quantity="VolumeFlowRate", final unit=
        "m3/s");
