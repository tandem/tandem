within ThermoSysPro_H2.Units.SI;
type ThermalUtilizationFactor = Real (final quantity=
        "ThermalUtilizationFactor", final unit="1");
