within ThermoSysPro_H2.Units.SI;
type TimeAging = Real (final quantity="1/Modelica.SIunits.Time",final unit="1/s");
