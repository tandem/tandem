within ThermoSysPro_H2.Units.SI;
type ThomsonCoefficient = Real (final quantity="ThomsonCoefficient", final unit=
           "V/K");
