within ThermoSysPro_H2.Units.SI;
type ThermalInsulance = Real (final quantity="ThermalInsulance", final unit=
        "m2.K/W");
