within ThermoSysPro_H2.Units.SI;
type SecondMomentOfArea = Real (final quantity="SecondMomentOfArea", final unit=
           "m4");
