within ThermoSysPro_H2.Units.SI;
type Acceleration = Real (final quantity="Acceleration", final unit="m/s2");
