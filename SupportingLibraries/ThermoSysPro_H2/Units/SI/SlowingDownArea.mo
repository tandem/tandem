within ThermoSysPro_H2.Units.SI;
type SlowingDownArea = Real (final quantity="Area", final unit="m2");
