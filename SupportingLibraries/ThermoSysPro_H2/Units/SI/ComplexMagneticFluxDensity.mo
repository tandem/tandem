within ThermoSysPro_H2.Units.SI;
operator record ComplexMagneticFluxDensity =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagneticFluxDensity re,
           redeclare ThermoSysPro_H2.Units.SI.MagneticFluxDensity im)
  "Complex magnetic flux density";
