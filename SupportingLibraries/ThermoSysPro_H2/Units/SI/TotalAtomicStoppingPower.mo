within ThermoSysPro_H2.Units.SI;
type TotalAtomicStoppingPower = Real (final quantity=
        "TotalAtomicStoppingPower", final unit="J.m2");
