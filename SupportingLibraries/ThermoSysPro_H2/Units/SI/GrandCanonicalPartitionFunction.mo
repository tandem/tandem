within ThermoSysPro_H2.Units.SI;
type GrandCanonicalPartitionFunction = Real (final quantity=
        "GrandCanonicalPartitionFunction", final unit="1");
