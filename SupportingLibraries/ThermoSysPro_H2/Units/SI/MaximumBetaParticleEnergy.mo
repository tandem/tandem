within ThermoSysPro_H2.Units.SI;
type MaximumBetaParticleEnergy = Real (final quantity="Energy", final unit=
        "J");
