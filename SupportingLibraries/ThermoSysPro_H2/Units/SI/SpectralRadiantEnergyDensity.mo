within ThermoSysPro_H2.Units.SI;
type SpectralRadiantEnergyDensity = Real (final quantity=
        "SpectralRadiantEnergyDensity", final unit="J/m4");
