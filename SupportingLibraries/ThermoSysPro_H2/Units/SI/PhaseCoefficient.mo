within ThermoSysPro_H2.Units.SI;
type PhaseCoefficient = Real (final quantity="PhaseCoefficient", final unit=
        "m-1");
