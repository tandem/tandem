within ThermoSysPro_H2.Units.SI;
type Volume = Real (final quantity="Volume", final unit="m3");
