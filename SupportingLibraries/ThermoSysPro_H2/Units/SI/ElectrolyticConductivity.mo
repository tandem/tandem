within ThermoSysPro_H2.Units.SI;
type ElectrolyticConductivity = Real (final quantity=
        "ElectrolyticConductivity", final unit="S/m");
