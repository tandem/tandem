within ThermoSysPro_H2.Units.SI;
type HeatFlux = Real (final quantity="HeatFlux", final unit="W/m2");
