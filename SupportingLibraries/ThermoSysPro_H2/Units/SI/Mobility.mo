within ThermoSysPro_H2.Units.SI;
type Mobility = Real (final quantity="Mobility", final unit="m2/(V.s)");
