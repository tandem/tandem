within ThermoSysPro_H2.Units.SI;
type AbsorbedDose = Real (final quantity="AbsorbedDose", final unit="Gy");
