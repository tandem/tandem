within ThermoSysPro_H2.Units.SI;
type AngularImpulseFlowRate = Real (final quantity="AngularImpulseFlowRate", final unit= "N.m");
