within ThermoSysPro_H2.Units.SI;
operator record ComplexPotentialDifference =
  Complex(redeclare ThermoSysPro_H2.Units.SI.PotentialDifference re,
           redeclare ThermoSysPro_H2.Units.SI.PotentialDifference im)
  "Complex electric potential difference";
