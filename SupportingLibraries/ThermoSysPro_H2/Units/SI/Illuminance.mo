within ThermoSysPro_H2.Units.SI;
type Illuminance = Real (final quantity="Illuminance", final unit="lx");
