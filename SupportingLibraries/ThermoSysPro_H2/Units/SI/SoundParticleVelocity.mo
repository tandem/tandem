within ThermoSysPro_H2.Units.SI;
type SoundParticleVelocity = Real (final quantity="Velocity", final unit=
        "m/s");
