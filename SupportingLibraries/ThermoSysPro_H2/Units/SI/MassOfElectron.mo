within ThermoSysPro_H2.Units.SI;
type MassOfElectron = Real (final quantity="Mass", final unit="kg");
