within ThermoSysPro_H2.Units.SI;
type PecletNumberOfMassTransfer = Real (final quantity=
        "PecletNumberOfMassTransfer", final unit="1");
