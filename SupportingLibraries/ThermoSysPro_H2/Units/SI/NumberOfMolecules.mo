within ThermoSysPro_H2.Units.SI;
type NumberOfMolecules = Real (final quantity="NumberOfMolecules", final unit=
       "1");
