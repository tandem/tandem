within ThermoSysPro_H2.Units.SI;
type LarmorAngularFrequency = Real (final quantity="AngularFrequency", final unit=
           "s-1");
