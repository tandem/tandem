within ThermoSysPro_H2.Units.SI;
type RelativePermeability = Real (final quantity="RelativePermeability",
      final unit="1");
