within ThermoSysPro_H2.Units.SI;
type MagneticMomentOfParticle = Real (final quantity=
        "MagneticMomentOfParticle", final unit="A.m2");
