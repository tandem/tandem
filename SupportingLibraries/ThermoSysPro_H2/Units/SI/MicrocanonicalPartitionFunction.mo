within ThermoSysPro_H2.Units.SI;
type MicrocanonicalPartitionFunction = Real (final quantity=
        "MicrocanonicalPartitionFunction", final unit="1");
