within ThermoSysPro_H2.Units.SI;
type SoundParticleAcceleration = Real (final quantity="Acceleration", final unit=
           "m/s2");
