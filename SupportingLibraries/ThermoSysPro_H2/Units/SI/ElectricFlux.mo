within ThermoSysPro_H2.Units.SI;
type ElectricFlux = Real (final quantity="ElectricFlux", final unit="C");
