within ThermoSysPro_H2.Units.SI;
type GFactorOfNucleus = Real (final quantity="GFactorOfNucleus", final unit=
        "1");
