within ThermoSysPro_H2.Units.SI;
type VolumeFraction = Real (final quantity="VolumeFraction", final unit="1");
