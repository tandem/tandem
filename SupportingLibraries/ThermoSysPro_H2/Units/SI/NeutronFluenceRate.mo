within ThermoSysPro_H2.Units.SI;
type NeutronFluenceRate = Real (final quantity="NeutronFluenceRate", final unit=
           "s-1.m-2");
