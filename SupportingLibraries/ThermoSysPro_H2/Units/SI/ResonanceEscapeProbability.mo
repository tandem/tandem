within ThermoSysPro_H2.Units.SI;
type ResonanceEscapeProbability = Real (final quantity=
        "ResonanceEscapeProbability", final unit="1");
