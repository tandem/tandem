within ThermoSysPro_H2.Units.SI;
type HoleNumberDensity = Real (final quantity="HoleNumberDensity", final unit=
       "m-3");
