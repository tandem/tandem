within ThermoSysPro_H2.Units.SI;
type TotalCrossSection = Real (final quantity="Area", final unit="m2");
