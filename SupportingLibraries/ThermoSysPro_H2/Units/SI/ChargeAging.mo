within ThermoSysPro_H2.Units.SI;
type ChargeAging = Real (final quantity="1/Modelica.SIunits.ElectricCharge",final unit="1/(A.s)");
