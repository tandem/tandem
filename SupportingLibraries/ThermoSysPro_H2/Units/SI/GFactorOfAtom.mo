within ThermoSysPro_H2.Units.SI;
type GFactorOfAtom = Real (final quantity="GFactorOfAtom", final unit="1");
