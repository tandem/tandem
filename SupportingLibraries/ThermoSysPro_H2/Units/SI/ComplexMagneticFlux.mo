within ThermoSysPro_H2.Units.SI;
operator record ComplexMagneticFlux =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagneticFlux re,
           redeclare ThermoSysPro_H2.Units.SI.MagneticFlux im)
  "Complex magnetic flux";
