within ThermoSysPro_H2.Units.SI;
type ElectricalForceConstant = Real (
     final quantity="ElectricalForceConstant",
     final unit = "N/A");
