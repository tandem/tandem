within ThermoSysPro_H2.Units.SI;
type ThermoelectromotiveForce = Real (final quantity=
        "ThermoelectromotiveForce", final unit="V");
