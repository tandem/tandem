within ThermoSysPro_H2.Units.SI;
type Density = Real (
    final quantity="Density",
    final unit="kg/m3",
    displayUnit="g/cm3",
    min=0.0);
