within ThermoSysPro_H2.Units.SI;
type EntropyFlowRate = Real (final quantity="EntropyFlowRate", final unit="J/(K.s)");
