within ThermoSysPro_H2.Units.SI;
type PressureCoefficient = Real (final quantity="PressureCoefficient", final unit=
           "Pa/K");
