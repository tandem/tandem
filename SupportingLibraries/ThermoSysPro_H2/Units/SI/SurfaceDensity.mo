within ThermoSysPro_H2.Units.SI;
type SurfaceDensity = Real (
    final quantity="SurfaceDensity",
    final unit="kg/m2",
    min=0);
