within ThermoSysPro_H2.Units.SI;
type SpectralLuminousEfficiency = Real (final quantity=
        "SpectralLuminousEfficiency", final unit="1");
