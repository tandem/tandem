within ThermoSysPro_H2.Units.SI;
type SolidAngle = Real (final quantity="SolidAngle", final unit="sr");
