within ThermoSysPro_H2.Units.SI;
type RelativeDensity = Real (
    final quantity="RelativeDensity",
    final unit="1",
    min=0.0);
