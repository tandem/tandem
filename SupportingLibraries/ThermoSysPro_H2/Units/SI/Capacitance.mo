within ThermoSysPro_H2.Units.SI;
type Capacitance = Real (
    final quantity="Capacitance",
    final unit="F",
    min=0);
