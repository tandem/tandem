within ThermoSysPro_H2.Units.SI;
type Breadth = Length(min=0);
