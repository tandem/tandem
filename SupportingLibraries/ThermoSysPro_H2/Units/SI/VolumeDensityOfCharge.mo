within ThermoSysPro_H2.Units.SI;
type VolumeDensityOfCharge = Real (
    final quantity="VolumeDensityOfCharge",
    final unit="C/m3",
    min=0);
