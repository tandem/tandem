within ThermoSysPro_H2.Units.SI;
type NeutronNumber = Real (final quantity="NeutronNumber", final unit="1");
