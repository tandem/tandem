within ThermoSysPro_H2.Units.SI;
type AtomicMassConstant = Real (final quantity="Mass", final unit="kg");
