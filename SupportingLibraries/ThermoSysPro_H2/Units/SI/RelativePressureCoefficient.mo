within ThermoSysPro_H2.Units.SI;
type RelativePressureCoefficient = Real (final quantity=
        "RelativePressureCoefficient", final unit="1/K");
