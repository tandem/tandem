within ThermoSysPro_H2.Units.SI;
type QualityFactor = Real (final quantity="QualityFactor", final unit="1");
