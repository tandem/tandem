within ThermoSysPro_H2.Units.SI;
type NumberDensityOfMolecules = Real (final quantity=
        "NumberDensityOfMolecules", final unit="m-3");
