within ThermoSysPro_H2.Units.SI;
type SchmidtNumber = Real (final quantity="SchmidtNumber", final unit="1");
