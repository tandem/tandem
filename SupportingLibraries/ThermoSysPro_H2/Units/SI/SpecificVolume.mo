within ThermoSysPro_H2.Units.SI;
type SpecificVolume = Real (
    final quantity="SpecificVolume",
    final unit="m3/kg",
    min=0.0);
