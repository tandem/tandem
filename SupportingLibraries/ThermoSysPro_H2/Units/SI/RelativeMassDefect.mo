within ThermoSysPro_H2.Units.SI;
type RelativeMassDefect = Real (final quantity="RelativeMassDefect", final unit=
           "1");
