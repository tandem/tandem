within ThermoSysPro_H2.Units.SI;
type Magnetization = Real (final quantity="Magnetization", final unit="A/m");
