within ThermoSysPro_H2.Units.SI;
type Strain = Real (final quantity="Strain", final unit="1");
