within ThermoSysPro_H2.Units.SI;
type AngularMomentum = Real (final quantity="AngularMomentum", final unit=
        "kg.m2/s");
