within ThermoSysPro_H2.Units.SI;
type TemperatureSlope = Real (final quantity="TemperatureSlope",
    final unit="K/s");
