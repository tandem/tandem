within ThermoSysPro_H2.Units.SI;
type SlowingDownLength = Real (final quantity="SLength", final unit="m");
