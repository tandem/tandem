within ThermoSysPro_H2.Units.SI;
type DissipationCoefficient = Real (final quantity="DissipationCoefficient",
      final unit="1");
