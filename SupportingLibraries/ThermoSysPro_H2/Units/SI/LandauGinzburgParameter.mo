within ThermoSysPro_H2.Units.SI;
type LandauGinzburgParameter = Real (final quantity="LandauGinzburgParameter",
        final unit="1");
