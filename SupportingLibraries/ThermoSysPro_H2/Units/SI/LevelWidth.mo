within ThermoSysPro_H2.Units.SI;
type LevelWidth = Real (final quantity="LevelWidth", final unit="J");
