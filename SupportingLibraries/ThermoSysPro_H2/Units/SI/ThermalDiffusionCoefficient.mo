within ThermoSysPro_H2.Units.SI;
type ThermalDiffusionCoefficient = Real (final quantity=
        "ThermalDiffusionCoefficient", final unit="m2/s");
