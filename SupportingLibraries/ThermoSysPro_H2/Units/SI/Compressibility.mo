within ThermoSysPro_H2.Units.SI;
type Compressibility = Real (final quantity="Compressibility", final unit=
        "1/Pa");
