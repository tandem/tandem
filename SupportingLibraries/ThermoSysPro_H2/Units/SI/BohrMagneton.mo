within ThermoSysPro_H2.Units.SI;
type BohrMagneton = MagneticMomentOfParticle;
