within ThermoSysPro_H2.Units.SI;
type MachNumber = Real (final quantity="MachNumber", final unit="1");
