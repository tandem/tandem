within ThermoSysPro_H2.Units.SI;
type ThermalConductivity = Real (final quantity="ThermalConductivity", final unit=
           "W/(m.K)");
