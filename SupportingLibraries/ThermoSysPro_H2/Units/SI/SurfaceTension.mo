within ThermoSysPro_H2.Units.SI;
type SurfaceTension = Real (final quantity="SurfaceTension", final unit="N/m");
