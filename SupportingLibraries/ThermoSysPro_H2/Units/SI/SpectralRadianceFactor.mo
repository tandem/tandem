within ThermoSysPro_H2.Units.SI;
type SpectralRadianceFactor = Real (final quantity="SpectralRadianceFactor",
      final unit="1");
