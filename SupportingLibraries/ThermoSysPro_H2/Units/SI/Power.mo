within ThermoSysPro_H2.Units.SI;
type Power = Real (final quantity="Power", final unit="W");
