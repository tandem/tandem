within ThermoSysPro_H2.Units.SI;
type LuminousIntensity = Real (final quantity="LuminousIntensity", final unit=
       "cd");
