within ThermoSysPro_H2.Units.SI;
type Irradiance = Real (final quantity="Irradiance", final unit="W/m2");
