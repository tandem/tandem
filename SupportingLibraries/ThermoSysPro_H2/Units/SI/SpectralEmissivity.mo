within ThermoSysPro_H2.Units.SI;
type SpectralEmissivity = Real (final quantity="SpectralEmissivity", final unit=
           "1");
