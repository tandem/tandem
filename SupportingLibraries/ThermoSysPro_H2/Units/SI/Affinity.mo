within ThermoSysPro_H2.Units.SI;
type Affinity = Real (final quantity="Affinity", final unit="J/mol");
