within ThermoSysPro_H2.Units.SI;
type Torque = Real (final quantity="Torque", final unit="N.m");
