within ThermoSysPro_H2.Units.SI;
type LoundnessLevel = Real (final quantity="LoundnessLevel", final unit=
        "phon") "Obsolete type, use LoudnessLevel instead!";
