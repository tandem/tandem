within ThermoSysPro_H2.Units.SI;
type NuclearQuadrupoleMoment = Real (final quantity="NuclearQuadrupoleMoment",
        final unit="m2");
