within ThermoSysPro_H2.Units.SI;
type TotalMacroscopicCrossSection = Real (final quantity=
        "TotalMacroscopicCrossSection", final unit="m-1");
