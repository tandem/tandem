within ThermoSysPro_H2.Units.SI;
type VolumeStrain = Real (final quantity="VolumeStrain", final unit="1");
