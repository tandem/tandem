within ThermoSysPro_H2.Units.SI;
type Heat = Real (final quantity="Energy", final unit="J");
