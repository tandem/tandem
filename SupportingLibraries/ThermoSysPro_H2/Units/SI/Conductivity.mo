within ThermoSysPro_H2.Units.SI;
type Conductivity = Real (final quantity="Conductivity", final unit="S/m");
