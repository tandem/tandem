within ThermoSysPro_H2.Units.SI;
type HeatCapacity = Real (final quantity="HeatCapacity", final unit="J/K");
