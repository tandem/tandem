within ThermoSysPro_H2.Units.SI;
type QuantityOfLight = Real (final quantity="QuantityOfLight", final unit=
        "lm.s");
