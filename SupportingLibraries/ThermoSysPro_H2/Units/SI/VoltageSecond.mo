within ThermoSysPro_H2.Units.SI;
type VoltageSecond = Real (final quantity="VoltageSecond", final unit="V.s")
  "Voltage second";
