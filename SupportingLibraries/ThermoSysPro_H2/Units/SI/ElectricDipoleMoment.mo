within ThermoSysPro_H2.Units.SI;
type ElectricDipoleMoment = Real (final quantity="ElectricDipoleMoment",
      final unit="C.m");
