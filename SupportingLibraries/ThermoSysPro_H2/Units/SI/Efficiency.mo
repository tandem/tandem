within ThermoSysPro_H2.Units.SI;
type Efficiency = Real (
    final quantity="Efficiency",
    final unit="1",
    min=0);
