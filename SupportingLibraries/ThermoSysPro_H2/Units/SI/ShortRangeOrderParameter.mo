within ThermoSysPro_H2.Units.SI;
type ShortRangeOrderParameter = Real (final quantity="RangeOrderParameter",
      final unit="1");
