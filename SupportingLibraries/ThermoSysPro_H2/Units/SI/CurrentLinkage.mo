within ThermoSysPro_H2.Units.SI;
type CurrentLinkage = Real (final quantity="CurrentLinkage", final unit="A");
