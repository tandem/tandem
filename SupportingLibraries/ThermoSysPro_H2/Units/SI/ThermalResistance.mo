within ThermoSysPro_H2.Units.SI;
type ThermalResistance = Real (final quantity="ThermalResistance", final unit=
       "K/W");
