within ThermoSysPro_H2.Units.SI;
operator record ComplexMagneticFieldStrength =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagneticFieldStrength re,
           redeclare ThermoSysPro_H2.Units.SI.MagneticFieldStrength im)
  "Complex magnetic field strength";
