within ThermoSysPro_H2.Units.SI;
type SpectralLuminousEfficacy = Real (final quantity=
        "SpectralLuminousEfficacy", final unit="lm/W");
