within ThermoSysPro_H2.Units.SI;
type Exposure = Real (final quantity="Exposure", final unit="C/kg");
