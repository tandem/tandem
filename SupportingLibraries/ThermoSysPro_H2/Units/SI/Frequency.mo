within ThermoSysPro_H2.Units.SI;
type Frequency = Real (final quantity="Frequency", final unit="Hz");
