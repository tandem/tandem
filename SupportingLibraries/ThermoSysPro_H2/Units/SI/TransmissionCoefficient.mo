within ThermoSysPro_H2.Units.SI;
type TransmissionCoefficient = Real (final quantity="TransmissionCoefficient",
        final unit="1");
