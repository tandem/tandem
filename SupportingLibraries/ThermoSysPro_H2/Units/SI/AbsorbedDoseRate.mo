within ThermoSysPro_H2.Units.SI;
type AbsorbedDoseRate = Real (final quantity="AbsorbedDoseRate", final unit=
        "Gy/s");
