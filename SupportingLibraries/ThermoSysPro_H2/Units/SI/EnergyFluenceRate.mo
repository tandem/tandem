within ThermoSysPro_H2.Units.SI;
type EnergyFluenceRate = Real (final quantity="EnergyFluenceRate", final unit=
       "W/m2");
