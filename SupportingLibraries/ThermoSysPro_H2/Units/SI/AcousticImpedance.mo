within ThermoSysPro_H2.Units.SI;
type AcousticImpedance = Real (final quantity="AcousticImpedance", final unit=
       "Pa.s/m3");
