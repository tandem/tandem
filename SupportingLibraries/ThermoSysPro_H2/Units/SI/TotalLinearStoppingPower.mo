within ThermoSysPro_H2.Units.SI;
type TotalLinearStoppingPower = Real (final quantity=
        "TotalLinearStoppingPower", final unit="J/m");
