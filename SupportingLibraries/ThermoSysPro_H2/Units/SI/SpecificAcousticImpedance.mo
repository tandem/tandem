within ThermoSysPro_H2.Units.SI;
type SpecificAcousticImpedance = Real (final quantity=
        "SpecificAcousticImpedance", final unit="Pa.s/m");
