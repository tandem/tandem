within ThermoSysPro_H2.Units.SI;
type SpecificHeatCapacity = Real (final quantity="SpecificHeatCapacity",
      final unit="J/(kg.K)");
