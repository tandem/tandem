within ThermoSysPro_H2.Units.SI;
type MolarConductivity = Real (final quantity="MolarConductivity", final unit=
       "S.m2/mol");
