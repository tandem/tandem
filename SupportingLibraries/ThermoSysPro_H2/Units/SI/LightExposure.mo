within ThermoSysPro_H2.Units.SI;
type LightExposure = Real (final quantity="LightExposure", final unit="lx.s");
