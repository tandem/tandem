within ThermoSysPro_H2.Units.SI;
operator record ComplexElectricPotential =
  Complex(redeclare ThermoSysPro_H2.Units.SI.ElectricPotential re,
           redeclare ThermoSysPro_H2.Units.SI.ElectricPotential im)
  "Complex electric potential";
