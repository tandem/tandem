within ThermoSysPro_H2.Units.SI;
type SpectralCrossSection = Real (final quantity="SpectralCrossSection",
      final unit="m2/J");
