within ThermoSysPro_H2.Units.SI;
type LuminousFlux = Real (final quantity="LuminousFlux", final unit="lm");
