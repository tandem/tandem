within ThermoSysPro_H2.Units.SI;
type CanonicalPartitionFunction = Real (final quantity=
        "CanonicalPartitionFunction", final unit="1");
