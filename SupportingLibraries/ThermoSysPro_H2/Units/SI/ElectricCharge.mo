within ThermoSysPro_H2.Units.SI;
type ElectricCharge = Real (final quantity="ElectricCharge", final unit="C");
