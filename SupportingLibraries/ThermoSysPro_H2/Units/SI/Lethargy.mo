within ThermoSysPro_H2.Units.SI;
type Lethargy = Real (final quantity="Lethargy", final unit="1");
