within ThermoSysPro_H2.Units.SI;
type Concentration = Real (final quantity="Concentration", final unit=
        "mol/m3");
