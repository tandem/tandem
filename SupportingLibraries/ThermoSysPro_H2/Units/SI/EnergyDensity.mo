within ThermoSysPro_H2.Units.SI;
type EnergyDensity = Real (final quantity="EnergyDensity", final unit="J/m3");
