within ThermoSysPro_H2.Units.SI;
type PeltierCoefficient = Real (final quantity="PeltierCoefficient", final unit=
           "V");
