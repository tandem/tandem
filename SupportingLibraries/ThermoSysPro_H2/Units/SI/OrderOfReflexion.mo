within ThermoSysPro_H2.Units.SI;
type OrderOfReflexion = Real (final quantity="OrderOfReflexion", final unit=
        "1");
