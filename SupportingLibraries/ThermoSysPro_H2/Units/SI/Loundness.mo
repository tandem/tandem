within ThermoSysPro_H2.Units.SI;
type Loundness = Real (final quantity="Loundness", final unit="sone")
  "Obsolete type, use Loudness instead!";
