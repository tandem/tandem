within ThermoSysPro_H2.Units.SI;
type InversePotential = Real (final quantity="InversePotential", final unit=
        "1/V");
