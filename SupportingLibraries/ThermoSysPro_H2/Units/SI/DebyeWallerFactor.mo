within ThermoSysPro_H2.Units.SI;
type DebyeWallerFactor = Real (final quantity="DebyeWallerFactor", final unit=
       "1");
