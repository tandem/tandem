within ThermoSysPro_H2.Units.SI;
type MomentOfInertia = Real (final quantity="MomentOfInertia", final unit=
        "kg.m2");
