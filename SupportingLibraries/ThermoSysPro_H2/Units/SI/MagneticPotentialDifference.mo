within ThermoSysPro_H2.Units.SI;
type MagneticPotentialDifference = Real (final quantity=
        "MagneticPotential", final unit="A");
