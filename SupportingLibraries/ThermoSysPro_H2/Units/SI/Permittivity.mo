within ThermoSysPro_H2.Units.SI;
type Permittivity = Real (
    final quantity="Permittivity",
    final unit="F/m",
    min=0);
