within ThermoSysPro_H2.Units.SI;
type NuclearRadius = Real (final quantity="Length", final unit="m");
