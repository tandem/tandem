within ThermoSysPro_H2.Units.SI;
type DynamicViscosity = Real (
    final quantity="DynamicViscosity",
    final unit="Pa.s",
    min=0);
