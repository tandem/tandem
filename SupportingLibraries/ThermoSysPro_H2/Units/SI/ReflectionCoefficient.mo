within ThermoSysPro_H2.Units.SI;
type ReflectionCoefficient = Real (final quantity="ReflectionCoefficient",
      final unit="1");
