within ThermoSysPro_H2.Units.SI;
type SeebeckCoefficient = Real (final quantity="SeebeckCoefficient", final unit=
           "V/K");
