within ThermoSysPro_H2.Units.SI;
type LinearAbsorptionCoefficient = Real (final quantity=
        "LinearAbsorptionCoefficient", final unit="m-1");
