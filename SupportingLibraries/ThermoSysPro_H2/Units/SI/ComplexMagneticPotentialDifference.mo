within ThermoSysPro_H2.Units.SI;
operator record ComplexMagneticPotentialDifference =
  Complex(redeclare ThermoSysPro_H2.Units.SI.MagneticPotentialDifference re,
           redeclare ThermoSysPro_H2.Units.SI.MagneticPotentialDifference im)
  "Complex magnetic potential difference";
