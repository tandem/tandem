within ThermoSysPro_H2.Units.SI;
type HartreeEnergy = Real (final quantity="Energy", final unit="J");
