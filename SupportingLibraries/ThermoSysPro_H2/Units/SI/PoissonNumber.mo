within ThermoSysPro_H2.Units.SI;
type PoissonNumber = Real (final quantity="PoissonNumber", final unit="1");
