within ThermoSysPro_H2.Units.SI;
type CIESpectralTristimulusValues = Real (final quantity=
        "CIESpectralTristimulusValues", final unit="1");
