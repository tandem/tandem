within ThermoSysPro_H2.Units.SI;
type AbsoluteActivity = Real (final quantity="AbsoluteActivity", final unit=
        "1");
