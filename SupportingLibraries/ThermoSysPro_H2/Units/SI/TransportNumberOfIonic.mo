within ThermoSysPro_H2.Units.SI;
type TransportNumberOfIonic = Real (final quantity="TransportNumberOfIonic",
      final unit="1");
