within ThermoSysPro_H2.Units.SI;
type EnthalpyFlowRate = Real (final quantity="EnthalpyFlowRate", final unit=
        "W");
