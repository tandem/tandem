within ThermoSysPro_H2.Units.SI;
type NusseltNumberOfMassTransfer = Real (final quantity=
        "NusseltNumberOfMassTransfer", final unit="1");
