within ThermoSysPro_H2.Units.SI;
type AngularVelocity = Real (
    final quantity="AngularVelocity",
    final unit="rad/s");
