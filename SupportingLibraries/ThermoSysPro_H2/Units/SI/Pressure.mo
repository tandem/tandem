within ThermoSysPro_H2.Units.SI;
type Pressure = Real (
    final quantity="Pressure",
    final unit="Pa",
    displayUnit="bar");
