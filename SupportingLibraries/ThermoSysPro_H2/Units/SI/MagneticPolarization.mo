within ThermoSysPro_H2.Units.SI;
type MagneticPolarization = Real (final quantity="MagneticPolarization",
      final unit="T");
