within ThermoSysPro_H2.Units.SI;
type LinearDensity = Real (
    final quantity="LinearDensity",
    final unit="kg/m",
    min=0);
