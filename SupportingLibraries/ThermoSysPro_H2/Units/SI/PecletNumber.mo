within ThermoSysPro_H2.Units.SI;
type PecletNumber = Real (final quantity="PecletNumber", final unit="1");
