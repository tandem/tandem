within ThermoSysPro_H2.Units.SI;
type ReactorTimeConstant = Real (final quantity="Time", final unit="s");
