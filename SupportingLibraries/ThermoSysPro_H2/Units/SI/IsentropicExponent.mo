within ThermoSysPro_H2.Units.SI;
type IsentropicExponent = Real (final quantity="IsentropicExponent", final unit=
           "1");
