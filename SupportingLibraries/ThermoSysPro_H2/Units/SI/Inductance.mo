within ThermoSysPro_H2.Units.SI;
type Inductance = Real (
    final quantity="Inductance",
    final unit="H");
