within ThermoSysPro_H2.Units.SI;
type PhaseDifference = Real (
    final quantity="Angle",
    final unit="rad",
    displayUnit="deg");
