within ThermoSysPro_H2.Units.SI;
type Momentum = Real (final quantity="Momentum", final unit="kg.m/s");
