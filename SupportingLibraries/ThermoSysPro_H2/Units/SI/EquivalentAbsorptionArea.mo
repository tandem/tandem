within ThermoSysPro_H2.Units.SI;
type EquivalentAbsorptionArea = Real (final quantity="Area", final unit="m2");
