within ThermoSysPro_H2.Units.SI;
type SoundEnergyDensity = Real (final quantity="EnergyDensity", final unit=
        "J/m3");
