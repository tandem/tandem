within ThermoSysPro_H2.Units.SI;
type Height = Length(min=0);
