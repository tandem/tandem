within ThermoSysPro_H2.Units.SI;
type MolarMass = Real (final quantity="MolarMass", final unit="kg/mol",min=0);
