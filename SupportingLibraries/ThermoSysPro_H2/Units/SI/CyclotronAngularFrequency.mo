within ThermoSysPro_H2.Units.SI;
type CyclotronAngularFrequency = Real (final quantity="AngularFrequency",
      final unit="s-1");
