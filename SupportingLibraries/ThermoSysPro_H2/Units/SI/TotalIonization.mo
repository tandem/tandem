within ThermoSysPro_H2.Units.SI;
type TotalIonization = Real (final quantity="TotalIonization", final unit="1");
