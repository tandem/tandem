within ThermoSysPro_H2.Units.SI;
type LinearAttenuationCoefficient = Real (final quantity=
        "AttenuationCoefficient", final unit="m-1");
