within ThermoSysPro_H2.Units.SI;
type Resistivity = Real (final quantity="Resistivity", final unit="Ohm.m");
