within ThermoSysPro_H2.Units.SI;
type CubicExpansionCoefficient = Real (final quantity=
        "CubicExpansionCoefficient", final unit="1/K");
