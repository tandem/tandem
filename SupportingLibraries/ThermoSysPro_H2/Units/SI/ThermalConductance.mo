within ThermoSysPro_H2.Units.SI;
type ThermalConductance = Real (final quantity="ThermalConductance", final unit=
           "W/K");
