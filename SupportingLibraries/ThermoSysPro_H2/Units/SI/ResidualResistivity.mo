within ThermoSysPro_H2.Units.SI;
type ResidualResistivity = Real (final quantity="ResidualResistivity", final unit=
           "Ohm.m");
