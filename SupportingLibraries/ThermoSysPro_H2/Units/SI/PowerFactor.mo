within ThermoSysPro_H2.Units.SI;
type PowerFactor = Real (final quantity="PowerFactor", final unit="1");
