within ThermoSysPro_H2.Units.SI;
type Period = Real (final quantity="Time", final unit="s");
