within ThermoSysPro_H2.Units.SI;
type LogarithmicDecrement = Real (final quantity="LogarithmicDecrement",
      final unit="1/S");
