within ThermoSysPro_H2.Units.SI;
type MechanicalImpedance = Real (final quantity="MechanicalImpedance", final unit=
           "N.s/m");
