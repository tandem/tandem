within ThermoSysPro_H2.Units.SI;
type MeanLinearRange = Real (final quantity="Length", final unit="m");
