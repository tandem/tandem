within ThermoSysPro_H2.Units.SI;
type RadiantEnergyFluenceRate = Real (final quantity=
        "RadiantEnergyFluenceRate", final unit="W/m2");
