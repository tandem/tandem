within ThermoSysPro_H2.Units.SI;
type CapacitancePerArea =
            Real (final quantity="CapacitancePerArea", final unit="F/m2")
  "Capacitance per area";
