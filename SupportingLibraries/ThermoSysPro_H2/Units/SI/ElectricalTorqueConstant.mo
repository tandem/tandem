within ThermoSysPro_H2.Units.SI;
type ElectricalTorqueConstant = Real(final quantity="ElectricalTorqueConstant", final unit= "N.m/A");
