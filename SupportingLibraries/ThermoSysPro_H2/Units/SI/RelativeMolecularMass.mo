within ThermoSysPro_H2.Units.SI;
type RelativeMolecularMass = Real (final quantity="RelativeMolecularMass",
      final unit="1");
