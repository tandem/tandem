within ThermoSysPro_H2.Units.SI;
type ReactivePower = Real (final quantity="Power", final unit="var");
