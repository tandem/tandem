within ThermoSysPro_H2.Units.SI;
type RadiantPower = Real (final quantity="Power", final unit="W");
