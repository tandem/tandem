within ThermoSysPro_H2.Units.SI;
type TotalMassStoppingPower = Real (final quantity="TotalMassStoppingPower",
      final unit="J.m2/kg");
