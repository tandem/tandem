within ThermoSysPro_H2.Units.SI;
type EnergyImparted = Real (final quantity="Energy", final unit="J");
