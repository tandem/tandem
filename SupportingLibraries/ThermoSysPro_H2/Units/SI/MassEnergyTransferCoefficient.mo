within ThermoSysPro_H2.Units.SI;
type MassEnergyTransferCoefficient = Real (final quantity=
        "MassEnergyTransferCoefficient", final unit="m2/kg");
