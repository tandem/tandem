within ThermoSysPro_H2.Units.SI;
type ChromaticityCoordinates = Real (final quantity="CromaticityCoordinates",
        final unit="1");
