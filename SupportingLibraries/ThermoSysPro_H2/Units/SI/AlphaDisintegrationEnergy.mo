within ThermoSysPro_H2.Units.SI;
type AlphaDisintegrationEnergy = Real (final quantity="Energy", final unit=
        "J");
