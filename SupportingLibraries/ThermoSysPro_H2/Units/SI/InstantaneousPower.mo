within ThermoSysPro_H2.Units.SI;
type InstantaneousPower = Real (final quantity="Power", final unit="W");
