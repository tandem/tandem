within ThermoSysPro_H2.Units.SI;
type Resistance = Real (
    final quantity="Resistance",
    final unit="Ohm");
