within ThermoSysPro_H2.Units.SI;
type ElementaryCharge = Real (final quantity="ElementaryCharge", final unit=
        "C");
