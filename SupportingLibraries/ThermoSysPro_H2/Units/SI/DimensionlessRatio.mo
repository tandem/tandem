within ThermoSysPro_H2.Units.SI;
type DimensionlessRatio = Real(unit = "1");
