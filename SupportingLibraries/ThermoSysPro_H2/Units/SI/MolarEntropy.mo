within ThermoSysPro_H2.Units.SI;
type MolarEntropy = Real (final quantity="MolarEntropy", final unit=
        "J/(mol.K)");
