within ThermoSysPro_H2.Units.SI;
type SpecificEntropy = Real (final quantity="SpecificEntropy",
                             final unit="J/(kg.K)");
