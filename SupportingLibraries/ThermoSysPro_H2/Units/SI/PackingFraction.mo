within ThermoSysPro_H2.Units.SI;
type PackingFraction = Real (final quantity="PackingFraction", final unit="1");
