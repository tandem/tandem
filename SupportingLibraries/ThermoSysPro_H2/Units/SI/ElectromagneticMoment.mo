within ThermoSysPro_H2.Units.SI;
type ElectromagneticMoment = Real (final quantity="ElectromagneticMoment",
      final unit="A.m2");
