within ThermoSysPro_H2.Units.SI;
type ElectronNumberDensity = Real (final quantity="ElectronNumberDensity",
      final unit="m-3");
