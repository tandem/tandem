within ThermoSysPro_H2.Units.SI;
type SpectralAbsorptionFactor = Real (final quantity=
        "SpectralAbsorptionFactor", final unit="1");
