within ThermoSysPro_H2.Units.SI;
type CowlingNumber = Real (final quantity="CowlingNumber", final unit="1");
