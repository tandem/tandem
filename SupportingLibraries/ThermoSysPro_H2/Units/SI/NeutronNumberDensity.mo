within ThermoSysPro_H2.Units.SI;
type NeutronNumberDensity = Real (final quantity="NeutronNumberDensity",
      final unit="m-3");
