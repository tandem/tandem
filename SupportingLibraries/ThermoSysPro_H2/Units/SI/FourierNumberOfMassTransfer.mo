within ThermoSysPro_H2.Units.SI;
type FourierNumberOfMassTransfer = Real (final quantity=
        "FourierNumberOfMassTransfer", final unit="1");
