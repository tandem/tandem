within ThermoSysPro_H2.Units.SI;
operator record ComplexVoltage =
  Complex(redeclare ThermoSysPro_H2.Units.SI.Voltage re,
           redeclare ThermoSysPro_H2.Units.SI.Voltage im)
  "Complex electrical voltage";
