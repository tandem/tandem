within ThermoSysPro_H2.Units.SI;
type ImpulseFlowRate = Real (final quantity="ImpulseFlowRate", final unit="N");
