within ThermoSysPro_H2.Units.SI;
type SoundIntensity = Real (final quantity="SoundIntensity", final unit=
        "W/m2");
