within ThermoSysPro_H2.Units.SI;
type StantonNumber = Real (final quantity="StantonNumber", final unit="1");
