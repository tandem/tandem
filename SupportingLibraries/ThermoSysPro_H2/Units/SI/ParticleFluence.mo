within ThermoSysPro_H2.Units.SI;
type ParticleFluence = Real (final quantity="ParticleFluence", final unit=
        "m-2");
