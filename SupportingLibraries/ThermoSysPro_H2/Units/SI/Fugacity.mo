within ThermoSysPro_H2.Units.SI;
type Fugacity = Real (final quantity="Fugacity", final unit="Pa");
