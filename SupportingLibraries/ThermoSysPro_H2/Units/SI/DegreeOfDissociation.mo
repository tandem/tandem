within ThermoSysPro_H2.Units.SI;
type DegreeOfDissociation = Real (final quantity="DegreeOfDissociation",
      final unit="1");
