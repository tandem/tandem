within ThermoSysPro_H2.Units.SI;
type ElectricFluxDensity = Real (final quantity="ElectricFluxDensity", final unit=
           "C/m2");
