within ThermoSysPro_H2.Units.SI;
type KermaRate = Real (final quantity="KermaRate", final unit="Gy/s");
