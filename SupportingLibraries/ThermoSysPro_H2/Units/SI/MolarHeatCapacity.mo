within ThermoSysPro_H2.Units.SI;
type MolarHeatCapacity = Real (final quantity="MolarHeatCapacity", final unit=
       "J/(mol.K)");
