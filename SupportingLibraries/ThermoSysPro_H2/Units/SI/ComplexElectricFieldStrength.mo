within ThermoSysPro_H2.Units.SI;
operator record ComplexElectricFieldStrength =
  Complex(redeclare ThermoSysPro_H2.Units.SI.ElectricFieldStrength re,
           redeclare ThermoSysPro_H2.Units.SI.ElectricFieldStrength im)
  "Complex electric field strength";
