within ThermoSysPro_H2.Units.SI;
type ElectromagneticEnergyDensity = Real (final quantity="EnergyDensity",
      final unit="J/m3");
