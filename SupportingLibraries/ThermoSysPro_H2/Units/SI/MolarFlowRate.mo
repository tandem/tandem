within ThermoSysPro_H2.Units.SI;
type MolarFlowRate = Real (final quantity="MolarFlowRate", final unit=
        "mol/s");
