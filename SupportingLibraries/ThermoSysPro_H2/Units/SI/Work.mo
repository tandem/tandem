within ThermoSysPro_H2.Units.SI;
type Work = Real (final quantity="Work", final unit="J");
