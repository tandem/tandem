within ThermoSysPro_H2.Units.SI;
type ComptonWavelength = Real (final quantity="Length", final unit="m");
