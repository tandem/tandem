within ThermoSysPro_H2.Units.SI;
type Impulse = Real (final quantity="Impulse", final unit="N.s");
