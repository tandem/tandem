within ThermoSysPro_H2.Units.SI;
type GrueneisenParameter = Real (final quantity="GrueneisenParameter", final unit=
           "1");
