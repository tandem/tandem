within ThermoSysPro_H2.Units.SI;
type ActivityCoefficientOfSolute = Real (final quantity=
        "ActivityCoefficientOfSolute", final unit="1");
