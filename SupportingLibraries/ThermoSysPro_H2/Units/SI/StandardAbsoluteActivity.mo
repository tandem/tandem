within ThermoSysPro_H2.Units.SI;
type StandardAbsoluteActivity = Real (final quantity=
        "StandardAbsoluteActivity", final unit="1");
