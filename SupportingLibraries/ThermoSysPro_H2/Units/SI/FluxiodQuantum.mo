within ThermoSysPro_H2.Units.SI;
type FluxiodQuantum = Real (final quantity="FluxiodQuantum", final unit="Wb");
