within ThermoSysPro_H2.Units.SI;
type LossAngle = Real (
    final quantity="Angle",
    final unit="rad",
    displayUnit="deg");
