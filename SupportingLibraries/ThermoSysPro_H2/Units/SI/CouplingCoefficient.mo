within ThermoSysPro_H2.Units.SI;
type CouplingCoefficient = Real (final quantity="CouplingCoefficient", final unit=
           "1");
