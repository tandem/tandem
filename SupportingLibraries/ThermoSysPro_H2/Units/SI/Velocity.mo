within ThermoSysPro_H2.Units.SI;
type Velocity = Real (final quantity="Velocity", final unit="m/s");
