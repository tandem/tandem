within ThermoSysPro_H2.Units.SI;
type AngularImpulse = Real (final quantity="AngularImpulse", final unit=
        "N.m.s");
