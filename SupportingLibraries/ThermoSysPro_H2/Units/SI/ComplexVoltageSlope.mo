within ThermoSysPro_H2.Units.SI;
operator record ComplexVoltageSlope =
  Complex(redeclare ThermoSysPro_H2.Units.SI.VoltageSlope re,
           redeclare ThermoSysPro_H2.Units.SI.VoltageSlope im)
  "Complex voltage slope";
