within ThermoSysPro_H2.Units.SI;
type Length = Real (final quantity="Length", final unit="m");
