within ThermoSysPro_H2.Units.SI;
type BetaDisintegrationEnergy = Real (final quantity="Energy", final unit="J");
