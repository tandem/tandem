within ThermoSysPro_H2.Units.SI;
type DebyeCircularWavenumber = Real (final quantity="DebyeCircularWavenumber",
        final unit="m-1");
