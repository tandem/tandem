within ThermoSysPro_H2.Units.SI;
type SoundReductionIndex = Real (final quantity="SoundReductionIndex", final unit=
           "dB");
