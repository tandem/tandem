within ThermoSysPro_H2.Units.SI;
type LuminousEfficiency = Real (final quantity="LuminousEfficiency", final unit=
           "1");
