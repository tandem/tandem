within ThermoSysPro_H2.Units.SI;
type CoefficientOfFriction = Real (final quantity="CoefficientOfFriction",
      final unit="1");
