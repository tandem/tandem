within ThermoSysPro_H2.Units.SI;
type GrashofNumber = Real (final quantity="GrashofNumber", final unit="1");
