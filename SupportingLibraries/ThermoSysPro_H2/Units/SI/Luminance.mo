within ThermoSysPro_H2.Units.SI;
type Luminance = Real (final quantity="Luminance", final unit="cd/m2");
