within ThermoSysPro_H2.Units.SI;
type SpecificEnergy = Real (final quantity="SpecificEnergy",
                            final unit="J/kg");
