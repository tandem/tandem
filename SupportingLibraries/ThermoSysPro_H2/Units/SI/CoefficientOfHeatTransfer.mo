within ThermoSysPro_H2.Units.SI;
type CoefficientOfHeatTransfer = Real (final quantity=
        "CoefficientOfHeatTransfer", final unit="W/(m2.K)");
