within ThermoSysPro_H2.Units.SI;
type MadelungConstant = Real (final quantity="MadelungConstant", final unit=
        "1");
