within ThermoSysPro_H2.Units;
package SI "Library of type and unit definitions based on SI units according to ISO 31-1992"
  extends Modelica.Icons.Package;

  // Space and Time (chapter 1 of ISO 31-1992)

  // Periodic and related phenomens (chapter 2 of ISO 31-1992)
  // For compatibility reasons only
  // added to ISO-chapter

  // Mechanics (chapter 3 of ISO 31-1992)
  // added to ISO-chapter 3

  // Heat (chapter 4 of ISO 31-1992)
  // added to ISO-chapter 4

  // Electricity and Magnetism (chapter 5 of ISO 31-1992)

  // added to ISO-chapter 5

  // Light and Related Electromagnetic Radiations (chapter 6 of ISO 31-1992)

  // Acoustics (chapter 7 of ISO 31-1992)

  // Physical chemistry and molecular physics (chapter 8 of ISO 31-1992)

  // Atomic and Nuclear Physics (chapter 9 of ISO 31-1992)

  // Nuclear Reactions and Ionizing Radiations (chapter 10 of ISO 31-1992)

  // chapter 11 is not defined in ISO 31-1992

  // Characteristic Numbers (chapter 12 of ISO 31-1992)
  // The Biot number (Bi) is used when
  // the Nusselt number is reserved
  // for convective transport of heat.

  // Solid State Physics (chapter 13 of ISO 31-1992)

 // Other types not defined in ISO 31-1992

 // Complex types for electrical systems (not defined in ISO 31-1992)
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={
        Line(
          points={{-66,78},{-66,-40}},
          color={64,64,64}),
        Ellipse(
          extent={{12,36},{68,-38}},
          lineColor={64,64,64},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-74,78},{-66,-40}},
          lineColor={64,64,64},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-66,-4},{-66,6},{-16,56},{-16,46},{-66,-4}},
          lineColor={64,64,64},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-46,16},{-40,22},{-2,-40},{-10,-40},{-46,16}},
          lineColor={64,64,64},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22,26},{58,-28}},
          lineColor={64,64,64},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{68,2},{68,-46},{64,-60},{58,-68},{48,-72},{18,-72},{18,-64},
              {46,-64},{54,-60},{58,-54},{60,-46},{60,-26},{64,-20},{68,-6},{68,
              2}},
          lineColor={64,64,64},
          smooth=Smooth.Bezier,
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid)}), Documentation(info="<html>
<p>This package provides predefined types, such as <i>Mass</i>, <i>Angle</i>, <i>Time</i>, based on the international standard on units, e.g., </p>
<p><code>   <b>type</b> Angle = Real(<b>final</b> quantity = &quot;Angle&quot;,</code></p>
<p><code>                     <b>final</b> unit     = &quot;rad&quot;,</code></p>
<p><code>                     displayUnit    = &quot;deg&quot;);</code> </p>
<p><br>Copyright &copy; 1998-2016, Modelica Association and DLR. </p>
<p>This package is copied from package Modelica.SIunits in Modelica package version 3.2.2.</p>
</html>", revisions="<html>
<ul>
<li><i>May 25, 2011</i> by Stefan Wischhusen:<br/>Added molar units for energy and enthalpy.</li>
<li><i>Jan. 27, 2010</i> by Christian Kral:<br/>Added complex units.</li>
<li><i>Dec. 14, 2005</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>Add User&#39;;s Guide and removed &quot;min&quot; values for Resistance and Conductance.</li>
<li><i>October 21, 2002</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a> and Christian Schweiger:<br/>Added new package <b>Conversions</b>. Corrected typo <i>Wavelenght</i>.</li>
<li><i>June 6, 2000</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>Introduced the following new types<br/>type Temperature = ThermodynamicTemperature;<br/>types DerDensityByEnthalpy, DerDensityByPressure, DerDensityByTemperature, DerEnthalpyByPressure, DerEnergyByDensity, DerEnergyByPressure<br/>Attribute &quot;final&quot; removed from min and max values in order that these values can still be changed to narrow the allowed range of values.<br/>Quantity=&quot;Stress&quot; removed from type &quot;Stress&quot;, in order that a type &quot;Stress&quot; can be connected to a type &quot;Pressure&quot;.</li>
<li><i>Oct. 27, 1999</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>New types due to electrical library: Transconductance, InversePotential, Damping.</li>
<li><i>Sept. 18, 1999</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>Renamed from SIunit to SIunits. Subpackages expanded, i.e., the SIunits package, does no longer contain subpackages.</li>
<li><i>Aug 12, 1999</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>Type &quot;Pressure&quot; renamed to &quot;AbsolutePressure&quot; and introduced a new type &quot;Pressure&quot; which does not contain a minimum of zero in order to allow convenient handling of relative pressure. Redefined BulkModulus as an alias to AbsolutePressure instead of Stress, since needed in hydraulics.</li>
<li><i>June 29, 1999</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a>:<br/>Bug-fix: Double definition of &quot;Compressibility&quot; removed and appropriate &quot;extends Heat&quot; clause introduced in package SolidStatePhysics to incorporate ThermodynamicTemperature.</li>
<li><i>April 8, 1998</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a> and Astrid Jaschinski:<br/>Complete ISO 31 chapters realized.</li>
<li><i>Nov. 15, 1997</i> by <a href=\"http://www.robotic.dlr.de/Martin.Otter/\">Martin Otter</a> and Hubertus Tummescheit:<br/>Some chapters realized.</li>
</ul>
</html>"));
end SI;
