within ThermoSysPro_H2.Units.SI;
type MassFraction = Real (final quantity="MassFraction", final unit="1",
                          min=0-Modelica.Constants.eps, max=1+Modelica.Constants.eps);
