within ThermoSysPro_H2.Units.SI;
type RotationalDampingConstant=Real(final quantity="RotationalDampingConstant", final unit="N.m.s/rad");
