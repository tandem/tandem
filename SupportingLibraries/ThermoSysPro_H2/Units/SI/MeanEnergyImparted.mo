within ThermoSysPro_H2.Units.SI;
type MeanEnergyImparted = Real (final quantity="Energy", final unit="J");
