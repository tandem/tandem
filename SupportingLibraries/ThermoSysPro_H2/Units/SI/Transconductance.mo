within ThermoSysPro_H2.Units.SI;
type Transconductance = Real (final quantity="Transconductance", final unit=
        "A/V2");
