within ThermoSysPro_H2.Units.SI;
type CrossSection = Real (final quantity="Area", final unit="m2");
