within ThermoSysPro_H2.Units.SI;
type Conductance = Real (
    final quantity="Conductance",
    final unit="S");
