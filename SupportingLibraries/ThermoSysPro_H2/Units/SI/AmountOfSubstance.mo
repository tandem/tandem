within ThermoSysPro_H2.Units.SI;
type AmountOfSubstance = Real (
    final quantity="AmountOfSubstance",
    final unit="mol",
    min=0);
