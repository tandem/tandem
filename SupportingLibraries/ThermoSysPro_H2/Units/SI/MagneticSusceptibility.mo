within ThermoSysPro_H2.Units.SI;
type MagneticSusceptibility = Real (final quantity="MagneticSusceptibility",
      final unit="1");
