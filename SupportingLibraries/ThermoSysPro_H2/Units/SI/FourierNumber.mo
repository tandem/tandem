within ThermoSysPro_H2.Units.SI;
type FourierNumber = Real (final quantity="FourierNumber", final unit="1");
