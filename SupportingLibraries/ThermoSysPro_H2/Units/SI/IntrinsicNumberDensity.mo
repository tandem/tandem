within ThermoSysPro_H2.Units.SI;
type IntrinsicNumberDensity = Real (final quantity="IntrinsicNumberDensity",
      final unit="m-3");
