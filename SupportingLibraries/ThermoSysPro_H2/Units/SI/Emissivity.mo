within ThermoSysPro_H2.Units.SI;
type Emissivity = Real (final quantity="Emissivity", final unit="1");
