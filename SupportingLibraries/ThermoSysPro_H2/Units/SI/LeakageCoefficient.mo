within ThermoSysPro_H2.Units.SI;
type LeakageCoefficient = Real (final quantity="LeakageCoefficient", final unit=
           "1");
