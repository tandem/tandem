within ThermoSysPro_H2.Units.SI;
type OsmoticCoefficientOfSolvent = Real (final quantity=
        "OsmoticCoefficientOfSolvent", final unit="1");
