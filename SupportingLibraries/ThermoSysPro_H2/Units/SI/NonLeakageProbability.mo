within ThermoSysPro_H2.Units.SI;
type NonLeakageProbability = Real (final quantity="NonLeakageProbability",
      final unit="1");
