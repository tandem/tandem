within ThermoSysPro_H2.Units.SI;
type FermiCircularWavenumber = Real (final quantity="FermiCircularWavenumber",
        final unit="m-1");
