within ThermoSysPro_H2.Units.SI;
type RadiantIntensity = Real (final quantity="RadiantIntensity", final unit=
        "W/sr");
