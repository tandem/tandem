within ThermoSysPro_H2.Units.SI;
type LongRangeOrderParameter = Real (final quantity="RangeOrderParameter",
      final unit="1");
