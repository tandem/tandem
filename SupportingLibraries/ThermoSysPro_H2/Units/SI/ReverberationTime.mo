within ThermoSysPro_H2.Units.SI;
type ReverberationTime = Real (final quantity="Time", final unit="s");
