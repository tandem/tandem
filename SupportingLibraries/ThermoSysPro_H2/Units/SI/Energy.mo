within ThermoSysPro_H2.Units.SI;
type Energy = Real (final quantity="Energy", final unit="J");
