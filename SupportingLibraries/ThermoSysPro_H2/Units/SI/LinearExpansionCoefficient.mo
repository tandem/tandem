within ThermoSysPro_H2.Units.SI;
type LinearExpansionCoefficient = Real (final quantity=
        "LinearExpansionCoefficient", final unit="1/K");
