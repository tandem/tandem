within ThermoSysPro_H2.Units.SI;
operator record ComplexImpedance =
  Complex(redeclare Resistance re,
           redeclare Reactance im) "Complex electrical impedance";
