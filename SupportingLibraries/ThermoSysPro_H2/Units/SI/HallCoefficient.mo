within ThermoSysPro_H2.Units.SI;
type HallCoefficient = Real (final quantity="HallCoefficient", final unit=
        "m3/C");
