within ThermoSysPro_H2.Units.SI;
type FermiEnergy = Real (final quantity="Energy", final unit="eV");
