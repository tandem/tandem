within ThermoSysPro_H2.Units.nonSI;
type Temperature_degC =   Real (final quantity="ThermodynamicTemperature", final unit=
                                                                                    "degC")
   annotation (Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
