within ThermoSysPro_H2.Units.nonSI;
type Pressure_bar = Real (final quantity="Pressure", final unit="bar");
