within ThermoSysPro_H2.Units.nonSI;
type Time_minute = Real(final quantity="Time", final unit="min");
