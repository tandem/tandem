within ThermoSysPro_H2.Units.nonSI;
type Angle_deg =  Real (final quantity="Angle", final unit="deg") annotation (
   Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
