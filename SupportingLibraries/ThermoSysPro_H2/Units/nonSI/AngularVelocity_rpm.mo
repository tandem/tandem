within ThermoSysPro_H2.Units.nonSI;
type AngularVelocity_rpm = Real (final quantity="Angular velocity", final unit="rev/min") annotation (Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
