within ThermoSysPro_H2.Units.xSI;
type PressureLossCoefficient =Real (final quantity="Pressure loss coefficient", final unit="m-4")
                                                            annotation (
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
