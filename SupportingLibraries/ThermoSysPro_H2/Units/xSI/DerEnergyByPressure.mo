within ThermoSysPro_H2.Units.xSI;
type DerEnergyByPressure = Real (final quantity="DerEnergyByPressure", final unit=
           "J/Pa");
