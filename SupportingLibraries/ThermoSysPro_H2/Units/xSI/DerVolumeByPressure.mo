within ThermoSysPro_H2.Units.xSI;
type DerVolumeByPressure = Real (final quantity="DerVolumeByPressure", final unit=
           "m3/Pa");
