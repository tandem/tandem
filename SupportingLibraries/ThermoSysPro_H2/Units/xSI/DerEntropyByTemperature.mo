within ThermoSysPro_H2.Units.xSI;
type DerEntropyByTemperature = Real (final quantity="DerEntropyByTemperature",
       final unit="J/K2");
