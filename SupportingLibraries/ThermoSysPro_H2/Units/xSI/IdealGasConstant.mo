within ThermoSysPro_H2.Units.xSI;
type IdealGasConstant = Real (final quantity="Ideal gas constant", final unit="J/(kg.K)");
