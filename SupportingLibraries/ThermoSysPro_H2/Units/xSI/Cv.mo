within ThermoSysPro_H2.Units.xSI;
type Cv = Real (final quantity="Cv U.S.", final unit="m4/(s.N5)")
                                                                annotation (
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
