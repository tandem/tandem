within ThermoSysPro_H2.Units.xSI;
type DerPressureByDensity = Real (final quantity="DerPressureByDensity",
      final unit="Pa.m3/kg");
