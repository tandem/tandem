within ThermoSysPro_H2.Units.xSI;
type ViscousFriction = Real (final quantity="Viscous friction", final unit="N/(m/s)");
