within ThermoSysPro_H2.Units.xSI;
type DerVolumeByTemperature = Real (final quantity="DerVolumeByTemperature",
      final unit="m3/K");
