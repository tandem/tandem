within ThermoSysPro_H2.Units.xSI;
type SonicConductance = Real (final quantity="Sonic conductance", final unit="m3/(s.Pa)")
                                                            annotation (
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
