within ThermoSysPro_H2.Units.xSI;
type DerDensityByEntropy = Real (final quantity="DerDensityByEntropy", final unit=
           "kg2.K/(m3.J)");
