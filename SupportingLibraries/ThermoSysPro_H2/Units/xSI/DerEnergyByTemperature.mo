within ThermoSysPro_H2.Units.xSI;
type DerEnergyByTemperature = Real (final quantity="Derivative of the specific energy wrt. the temperature", final unit=
                                                                                                "J/(kg.K)")
  annotation (Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
