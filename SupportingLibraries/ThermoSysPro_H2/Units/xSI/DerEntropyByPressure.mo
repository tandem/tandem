within ThermoSysPro_H2.Units.xSI;
type DerEntropyByPressure = Real (final quantity="DerEntropyByPressure",
      final unit="J/(K.Pa)");
