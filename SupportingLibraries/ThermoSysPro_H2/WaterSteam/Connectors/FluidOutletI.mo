within ThermoSysPro_H2.WaterSteam.Connectors;
connector FluidOutletI "Internal water/steam outlet fluid connector"
  Units.SI.AbsolutePressure P(start=1.e5)
    "Fluid pressure in the control volume";
  Units.SI.SpecificEnthalpy h_vol(start=1.e5)
    "Fluid specific enthalpy in the control volume";
  Units.SI.MassFlowRate Q(start=500)
    "Mass flow rate of the fluid crossing the boundary of the control volume";
  Units.SI.SpecificEnthalpy h(start=1.e5)
    "Specific enthalpy of the fluid crossing the boundary of the control volume";

  output Boolean a
    "Pseudo-variable for the verification of the connection orientation";
  input Boolean b
    "Pseudo-variable for the verification of the connection orientation";
    annotation (
      Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={255,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.CrossDiag)}),
      Window(
        x=0.45,
        y=0.01,
        width=0.35,
        height=0.49),
      Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
",
 revisions="<html>
<p><u><b>Author</b></u></p>
<ul>
<li>Daniel Bouskela </li>
</ul>
</html>"));
end FluidOutletI;
