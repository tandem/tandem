within ThermoSysPro_H2.H2mixGases.Connectors;
connector H2mixGasesInlet "Flue gases inlet fluid connector"
  Units.SI.AbsolutePressure P(start=1.e5)
    "Fluid pressure in the control volume";
  Units.SI.Temperature T(start=300) "Fluid temperature in the control volume";
  Units.SI.MassFlowRate Q(start=100)
    "Mass flow of the fluid crossing the boundary of the control volume";
  Real Xco2(start=0.01)
    "CO2 mass fraction of the fluid crossing the boundary of the control volume";
  Real Xh2o(start=0.05)
    "H2O mass fraction of the fluid crossing the boundary of the control volume";
  Real Xo2(start=0.2)
    "O2 mass fraction of the fluid crossing the boundary of the control volume";
  Real Xh2(start=0)
    "H2 mass fraction of the fluid crossing the boundary of the control volume";

  input Boolean a=true
    "Pseudo-variable for the verification of the connection orientation";
  output Boolean b
    "Pseudo-variable for the verification of the connection orientation";

  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={Rectangle(
          extent={{-100,-100},{100,100}},
          lineColor={0,0,0},
          lineThickness=1,
          fillPattern=FillPattern.Sphere,
          fillColor={127,127,255})}),
    Window(
      x=0.31,
      y=0.13,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
",
 revisions="<html>
<p><u><b>Author</b></u></p>
<ul>
<li>Baligh El Hefni </li>
</ul>
</html>"));
end H2mixGasesInlet;
