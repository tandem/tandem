within ThermoSysPro_H2.InstrumentationAndControl.Blocks.Commun;
function fmod "fmod"
  input Real u1;
  input Real u2;
  output Real y;
external "C" y = fmod(u1, u2);
  annotation (Icon(graphics={
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(extent={{-100,40},{100,-100}}, lineColor={255,127,0}),
        Text(
          extent={{-84,18},{84,-30}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(
          extent={{-82,-22},{86,-70}},
          lineColor={255,127,0},
          textString=
               "externe")}),
                           Documentation(info="<html>
<p><b>Version 1.6</b></p>
</HTML>
"));
end fmod;
