within ThermoSysPro_H2.InstrumentationAndControl.Blocks.Commun;
function rand "rand"
  output Integer y;
external "C" y = rand(0);
  annotation (
    Window(
      x=0.45,
      y=0.01,
      width=0.35,
      height=0.49),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Text(
          extent={{-84,18},{84,-30}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name"),
        Ellipse(extent={{-100,40},{100,-100}}, lineColor={255,127,0}),
        Text(
          extent={{-82,-22},{86,-70}},
          lineColor={255,127,0},
          textString=
               "externe")}),
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end rand;
