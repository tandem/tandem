within ThermoSysPro_H2.InstrumentationAndControl.Common;
function CvtEntierVersDuree
  input Integer t "Duree en dixiemes secondes";
  output Duree d "Duree"        annotation (Placement(transformation(extent={
            {-70,-70},{70,70}}, rotation=0)));
algorithm

  d.nb_jours := integer(t/24/3600/10);
  d.nb_heures := integer((t - d.nb_jours*24*3600*10)/3600/10);
  d.nb_minutes := integer((t - d.nb_jours*24*3600*10 - d.nb_heures*3600*10)/
    60/10);
  d.nb_secondes := integer((t - d.nb_jours*24*3600*10 - d.nb_heures*3600*10
     - d.nb_minutes*60*10)/10);
  d.nb_dixiemes_secondes := integer(t - d.nb_jours*24*3600*10 - d.nb_heures*
    3600*10 - d.nb_minutes*60*10 - d.nb_secondes*10);
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Ellipse(
          extent={{-100,40},{100,-100}},
          lineColor={255,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,-4},{84,-52}},
          lineColor={255,127,0},
          textString=
               "fonction"),
        Text(extent={{-134,104},{142,44}}, textString=
                                               "%name")}),
    Window(
      x=0.27,
      y=0.27,
      width=0.6,
      height=0.6),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end CvtEntierVersDuree;
