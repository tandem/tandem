within ThermoSysPro_H2.InstrumentationAndControl.Common;
record DateEtHeure
  Integer annee(min=2000) "Année";
  Integer mois(min=1, max=12) "Mois dans l'année";
  Integer jour(min=1, max=31) "Jour dans le mois";
  Integer heure(min=0, max=23) "Heure du jour";
  Integer minutes(min=0, max=59) "Nombre de minutes";
  Integer secondes(min=0, max=59) "Nombre de secondes";
  Integer nb_dixemes_secondes(min=0, max=9) "Nombre de dixèmes de secondes";
  annotation (
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Rectangle(
          extent={{-100,50},{100,-100}},
          lineColor={0,0,255},
          fillColor={255,255,127},
          fillPattern=FillPattern.Solid),
        Text(extent={{-127,115},{127,55}}, textString=
                                               "%name"),
        Line(points={{-100,-50},{100,-50}}, color={0,0,0}),
        Line(points={{-100,0},{100,0}}, color={0,0,0}),
        Line(points={{0,50},{0,-100}}, color={0,0,0})}),
    Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2010</b></p>
</HTML>
<html>
<p><b>ThermoSysPro Version 2.0</b></p>
</HTML>
"));
end DateEtHeure;
