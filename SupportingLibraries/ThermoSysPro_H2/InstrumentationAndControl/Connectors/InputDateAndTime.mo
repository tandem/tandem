within ThermoSysPro_H2.InstrumentationAndControl.Connectors;
connector InputDateAndTime
  input ThermoSysPro_H2.InstrumentationAndControl.Common.DateEtHeure signal;
  annotation (
    Window(
      x=0.37,
      y=0.02,
      width=0.49,
      height=0.65),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-1,-1},{1,1}},
        grid={2,2}), graphics={Polygon(
          points={{-1,1},{1,0},{-1,-1},{-1,1}},
          lineColor={0,0,255},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end InputDateAndTime;
