within ThermoSysPro_H2.InstrumentationAndControl.Connectors;
connector OutputLogical
    output Boolean signal;
    annotation (
      Window(
        x=0.29,
        y=0.11,
        width=0.6,
        height=0.6),
      Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={Polygon(
          points={{-100,100},{-100,-100},{100,0},{-100,100}},
          lineColor={0,0,255},
          fillColor={127,255,0},
          fillPattern=FillPattern.Solid)}),
      Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end OutputLogical;
