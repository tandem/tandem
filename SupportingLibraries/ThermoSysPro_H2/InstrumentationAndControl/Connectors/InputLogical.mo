within ThermoSysPro_H2.InstrumentationAndControl.Connectors;
connector InputLogical
   input Boolean signal;
   annotation (
     Window(
       x=0.37,
       y=0.02,
       width=0.49,
       height=0.65),
     Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={Polygon(
          points={{-100,100},{-100,-100},{100,0},{-100,100}},
          lineColor={0,0,255},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid)}),
     Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end InputLogical;
