within ThermoSysPro_H2.InstrumentationAndControl.Connectors;
connector OutputDateAndTime
  output ThermoSysPro_H2.InstrumentationAndControl.Common.DateEtHeure signal;
  annotation (
    Window(
      x=0.29,
      y=0.11,
      width=0.6,
      height=0.6),
    Icon(coordinateSystem(
        preserveAspectRatio=false,
        extent={{-1,-1},{1,1}},
        grid={2,2}), graphics={Polygon(
          points={{-1,1},{1,0},{-1,-1},{-1,1}},
          lineColor={0,0,255},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p><b>Version 1.0</b></p>
</HTML>
"));
end OutputDateAndTime;
