within TANDEM.Methanation;
model MethanatorAndHeatflux_Unit_Test
  replaceable package Medium = Modelica.Media.Water.StandardWaterOnePhase constrainedby
    Modelica.Media.Interfaces.PartialMedium;
  ThermoPower.Water.SourceMassFlow SourceCO2(redeclare package Medium = Medium, allowFlowReversal = true, use_in_T = false, w0 = 0.44) annotation (
    Placement(visible = true, transformation(origin = {-44, -134}, extent = {{-50, 130}, {-30, 150}}, rotation = 0)));
  ThermoPower.Water.SinkPressure StorageCH4(redeclare package Medium = Medium, allowFlowReversal = true) annotation (
    Placement(visible = true, transformation(origin = {44, -106}, extent = {{30, 130}, {50, 150}}, rotation = 0)));
  ThermoPower.Water.SourceMassFlow SourceH2(redeclare package Medium = Medium, use_in_T = false, w0 = 0.08) annotation (
    Placement(visible = true, transformation(origin = {-44, -162}, extent = {{-50, 130}, {-30, 150}}, rotation = 0)));
  ThermoPower.Water.SinkPressure StorageH2O(redeclare package Medium = Medium, allowFlowReversal = true) annotation (
    Placement(visible = true, transformation(origin = {44, -132}, extent = {{30, 130}, {50, 150}}, rotation = 0)));
  Modelica.Thermal.HeatTransfer.Components.HeatCapacitor heatCapacitor(C = 9999999999999999, der_T(start = 0)) annotation (
    Placement(visible = true, transformation(origin = {62, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner ThermoPower.System system(allowFlowReversal = false, initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
    Placement(visible = true, transformation(origin = {140, 40}, extent = {{-60, 40}, {-40, 60}}, rotation = 0)));
Methanation.ReactorWithHeat reactorWithHeat annotation (Placement(visible=true,
        transformation(
        origin={3,9},
        extent={{-61,-61},{61,61}},
        rotation=0)));
equation
connect(SourceCO2.flange, reactorWithHeat.InputCO2) annotation (
    Line(points = {{-74, 6}, {-44, 6}, {-44, 2}}, color = {0, 0, 255}));
connect(SourceH2.flange, reactorWithHeat.InputH2) annotation (
    Line(points = {{-74, -22}, {-44, -22}, {-44, -10}}, color = {0, 0, 255}));
connect(reactorWithHeat.OutputCH4, StorageCH4.flange) annotation (
    Line(points = {{44, 32}, {59, 32}, {59, 34}, {74, 34}}, color = {0, 0, 255}));
connect(StorageH2O.flange, reactorWithHeat.OutputH2O) annotation (
    Line(points = {{74, 8}, {44, 8}, {44, 18}}, color = {0, 0, 255}));
connect(reactorWithHeat.HeatProduced, heatCapacitor.port) annotation (
    Line(points = {{28, -8}, {38, -8}, {38, -30}, {62, -30}}, color = {191, 0, 0}));
  annotation (
    Diagram);
end MethanatorAndHeatflux_Unit_Test;
