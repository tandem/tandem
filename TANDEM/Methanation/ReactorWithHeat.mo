within TANDEM.Methanation;
block ReactorWithHeat
  replaceable package Medium = Modelica.Media.Water.StandardWaterOnePhase constrainedby
    Modelica.Media.Interfaces.PartialMedium;
  ThermoPower.Water.FlangeA InputCO2(redeclare package Medium = Medium) annotation (
    Placement(visible = true, transformation(origin = {-30, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-78, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ThermoPower.Water.FlangeB OutputCH4(redeclare package Medium = Medium) annotation (
    Placement(visible = true, transformation(origin = {30, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {68, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ThermoPower.Water.FlangeA InputH2(redeclare package Medium = Medium) annotation (
    Placement(visible = true, transformation(origin = {-30, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-78, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ThermoPower.Water.FlangeB OutputH2O(redeclare package Medium = Medium) annotation (
    Placement(visible = true, transformation(origin = {30, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {68, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.SIunits.MassFlowRate InputTotal " total input fuel in Methanator";
  Modelica.SIunits.MassFlowRate OutputTotal " total output fuel in Methanator";
  Modelica.SIunits.MassFlowRate Losses " total output fuel in Methanator";
  parameter Real Lambda = 0.96 " efficiency ";
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b HeatProduced annotation (
    Placement(visible = true, transformation(origin = {-38, -10}, extent = {{90, -10}, {110, 10}}, rotation = 0), iconTransformation(origin = {-58, -28}, extent = {{90, -10}, {110, 10}}, rotation = 0)));
equation
  InputTotal = Lambda*(InputCO2.m_flow + InputH2.m_flow);
  OutputTotal = OutputH2O.m_flow + OutputCH4.m_flow;
  Losses = (InputCO2.m_flow + InputH2.m_flow) - InputTotal;
// InputTotal - OutputTotal=0;
  OutputCH4.p = InputCO2.p;
  OutputH2O.p = InputH2.p;
  OutputCH4.m_flow = (16/52)*InputTotal;
  OutputH2O.m_flow = InputTotal - OutputCH4.m_flow;
/*  
  InputCO2.h_outflow = inStream(OutputCH4.h_outflow);
  inStream(InputCO2.h_outflow) = OutputCH4.h_outflow;
  InputH2.h_outflow = inStream(OutputH2O.h_outflow);
  inStream(InputH2.h_outflow) =  OutputH2O.h_outflow  
  */
////incorrect values used for specific enthalpy, which should not be used by this simulation
  InputCO2.h_outflow = (44/52)*(inStream(OutputCH4.h_outflow) + inStream(OutputH2O.h_outflow));
  inStream(InputCO2.h_outflow) = (44/52)*(OutputCH4.h_outflow + OutputH2O.h_outflow);
  InputH2.h_outflow = (8/52)*(inStream(OutputH2O.h_outflow) + inStream(OutputH2O.h_outflow));
  inStream(InputH2.h_outflow) = (8/52)*(OutputH2O.h_outflow + OutputH2O.h_outflow);
//Enthalpy.k=-(164)*((16/52)*InputTotal)*(1000/16.04);
  HeatProduced.Q_flow = -(164)*((16/52)*InputTotal)*(1000/16.04)*(1000)*(                      1 + (HeatProduced.T - 293.15));
                                                                         /*Convert Kj\s to W */
//HeatProduced.Q_flow=450-HeatProduced.T;
  annotation (
    Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}}), graphics={  Rectangle(origin = {-55, -10}, fillColor = {0, 0, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-19, 8}, {19, -8}}), Rectangle(origin = {51, 38}, fillColor = {255, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-19, 8}, {19, -8}}), Ellipse(origin = {-2, 47}, fillColor = {148, 148, 148}, fillPattern = FillPattern.Sphere, extent = {{-34, 35}, {34, -35}}), Ellipse(origin = {-2, -42}, fillColor = {112, 112, 112}, fillPattern = FillPattern.Sphere, extent = {{-34, 34}, {34, -34}}), Rectangle(origin = {-2, 3}, fillColor = {56, 56, 56}, fillPattern = FillPattern.VerticalCylinder, extent = {{-34, 47}, {34, -47}}), Rectangle(origin = {51, 16}, fillColor = {255, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-19, 8}, {19, -8}}), Rectangle(origin = {-55, -32}, fillColor = {0, 0, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-19, 8}, {19, -8}}), Text(origin = {-2, 3}, textColor = {255, 255, 255}, extent = {{-28, 35}, {28, -35}}, textString = "Methanation
   reactor"),
            Line(origin = {-2.34458, 21.8117}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Line(origin = {-2.34458, 33.7478}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Line(origin = {-2.34458, 45.6838}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Line(origin = {-1.97162, -37.4956}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Line(origin = {-1.97161, -15.1155}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Line(origin = {-1.59862, -26.3056}, points = {{-30, 0}, {30, 0}}, color = {0, 255, 0}, thickness = 1), Text(origin = {-57, 6}, extent = {{-15, 6}, {15, -6}}, textString = "CO2_input"), Text(origin = {-56, -47}, extent = {{-12, 5}, {12, -5}}, textString = "H2_input"), Text(origin = {51, 52}, extent = {{-15, 6}, {15, -6}}, textString = "CH4_output"), Text(origin = {51, 2}, extent = {{-15, 6}, {15, -6}}, textString = "H2O_output"), Text(origin = {45, -13}, extent = {{-11, 7}, {11, -7}}, textString = "Heat_Flux")}));
end ReactorWithHeat;
