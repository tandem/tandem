# tandem-methanation-simulator
Simulator of the methanation process
----
Our global strategy is to use existing components from the Modelica ThermoPower library,
and components will be generally exchanging mass flow rate, pressure and enthalpy within the BOP.
Heat exchanges with other modules are done via heat flux (with heat users and with SMR). 
----
TO SIMULATE THE FILES IS NECESSARY TO UPLOAD THE THERMOPOWER LIBRARY BEFORE THE SIMULATION.
----