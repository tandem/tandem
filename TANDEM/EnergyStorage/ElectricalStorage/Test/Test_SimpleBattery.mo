within TANDEM.EnergyStorage.ElectricalStorage.Test;
model Test_SimpleBattery

  ElectricalStorage.SimpleBattery simpleBattery(SOCinit(displayUnit="Wh") =
      180000)
    annotation (Placement(transformation(extent={{-10,-40},{10,-20}})));
  ElectricalStorage.BatteryControl batteryControl(
    maxCapacity(displayUnit="Wh") = 360000,
    minCapacity=0,
    maxCharPower(displayUnit="kW") = 1000,
    maxDischPower(displayUnit="kW") = 1000)
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  ThermoPower.Electrical.Grid grid(Pgrid=100)
    annotation (Placement(transformation(extent={{28,-40},{48,-20}})));
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{60,60},{80,80}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=2e3,
    duration=10,
    offset=0,
    startTime=5)
    annotation (Placement(transformation(extent={{-80,64},{-60,84}})));
  Modelica.Blocks.Sources.Ramp ramp1(
    height=-4e3,
    duration=10,
    offset=0,
    startTime=20)
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
equation
  connect(batteryControl.powerSignal, simpleBattery.signalPower)
    annotation (Line(points={{-6,-0.4},{-6,-19.8}}, color={0,0,127}));
  connect(batteryControl.SOC, simpleBattery.SOC)
    annotation (Line(points={{6,-0.2},{6,-19.6}}, color={0,0,127}));
  connect(simpleBattery.powerConnection, grid.port) annotation (Line(
      points={{10,-30},{29.4,-30}},
      color={0,0,255},
      thickness=0.5));
  connect(ramp.y, add.u1) annotation (Line(points={{-59,74},{-50,74},{-50,56},{
          -42,56}}, color={0,0,127}));
  connect(add.y, batteryControl.powerSetpoint)
    annotation (Line(points={{-19,50},{0,50},{0,19.8}}, color={0,0,127}));
  connect(ramp1.y, add.u2) annotation (Line(points={{-59,30},{-50,30},{-50,44},
          {-42,44}}, color={0,0,127}));
end Test_SimpleBattery;
