within TANDEM.EnergyStorage.ElectricalStorage;
model SimpleBattery
  parameter Modelica.Units.SI.Energy SOCinit(displayUnit="kW.h") "Initial state of charge";

  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  Modelica.Blocks.Interfaces.RealInput signalPower annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={-60,106}), iconTransformation(
        extent={{-3.33345,-3.33336},{16.6671,16.6666}},
        rotation=270,
        origin={-66.6666,108.667})));
  Modelica.Blocks.Interfaces.RealOutput SOC annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,104})));
  Modelica.Blocks.Continuous.Integrator integrator(y_start=SOCinit)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
    powerConnection.P = signalPower;

  connect(signalPower, integrator.u)
    annotation (Line(points={{-60,106},{-60,0},{-12,0}}, color={0,0,127}));
  connect(integrator.y, SOC)
    annotation (Line(points={{11,0},{60,0},{60,104}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,190,190},
          fillColor={0,190,190},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={0,190,190},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-70,66},{64,-56}},
          textColor={0,190,190},
          textString="Battery")}),                               Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(revisions="<html>
</html>", info="<html>
<p>The battery model converts an input power signal to the <i>ThermoPower.Electrical.PowerConnection</i> connector, which can be used to couple the battery to other components. The state-of-charge (<i>SOC</i>) is obtained by integrating the power signal over time. </p>
<p>The operating limits of the battery, in terms of maximal and minimal charging and discharging power, as well as capacity limits, are accounted for in a dedicated control system. </p>
</html>"));
end SimpleBattery;
