# Thermal energy storage

The thermal energy storage (TES) technology included in the TANDEM library is a two-tank heat storage system. During the charging process, thermal power is transferred through a charging heat exchanger to the sensible loop. During this process, the heat transfer fluid (e.g., oil or molten salts) flows from the cold tank to the hot tank, increasing its temperature in the heat exchanger. The higher the charging power, the higher the flow rate pumped between the tanks. Similarly, the thermal energy storage system is discharged by releasing the thermal power of fluid flowing from the hot tank to the cold tank through a discharging heat exchanger.


## Package description
The `ThermalStorage` package is structured as follows:
- Demonstration cases showcasing possible applications of several versions of the TES model are provided in the `Test` package.
- The `Components` package collects the components developed for the `SensibleLoop` model, i.e., the `Tank` and the charging and discharging heat exchanger components.
- Controller components, based on an illustrative control strategy for the TES, are provided in the `Control` package.
- The heat transfer models and correlations adopted in the heat exchanger models are provided in the `HeatTransfer` package.
- The thermal properties of the sensible fluid, which are required in the various `SensibleLoop` subcomponents, are proposed in the `Media` package.

## Component description
The sensible loop model is based on two `Tank` components, storing the hot an cold fluid, two ThermoPower [Pump](https://build.openmodelica.org/Documentation/ThermoPower.Water.Pump.html) components followed by a [ValveLin](https://build.openmodelica.org/Documentation/ThermoPower.Water.ValveLin.html) model to regulate the flow from one tank to the other according to the charging and discharging requirements.



The charging and discharging powers are governed by the heat transfer through two heat exchangers. Several sensible loop models are available in the `ThermalStorage` package, differing from each other exclusively in terms of the heat exchanger model:
- In the `SensibleLoop_HX0D` model, the ideal heat exchanger model that does not account for dynamic aspects, `HX_0D` is used.
- `SensibleLoop_HX1D_WaterOil` encompasses 1D shell and tube heat exchangers. The fluid on the shell side of the heat exchanger is water, condensing on the charging side and evaporating to superheated steam conditions on the discharging side.
- Similarly, `SensibleLoop_HX1D_OilOil` is based on shell and tube heat exchangers, assuming a single-phase fluid flow on the shell side. As a result, this model is suited for thermal power exchange with an intermediate oil loop, for example.


### Tank
The `Tank` model is used to simulate the sensible fluid accumulation in the tanks. It is based on mass and energy balance equations.

The liquid surface pressure is assumed to be fixed by a cover gas pressure, whereas the bulk pressure is given by
Stevino's law. It is possible to account for heat losses to the environment, computed through the convective heat transfer between the wall and both liquid in the tank and external environment and heat conduction through the wall's material. This term can be deactivated through a dedicated flag, which allows the user to assume the tank to be perfectly insulated.



### Shell and tube heat exchanger
The shell and tube heat exchangers are based on a 1D finite volume approach. They are built with models of the ThermoPower library, namely the [Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) (and, in case of applications with phase change on the shell side, the [Flow1DV2ph](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV2ph.html) component) to simulate the fluid flow.
In addition, the [MetalWallFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.MetalWallFV.html) component is adopted to account for the thermal resistance and inertia of the metal structure, and the [CounterCurrentFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.CounterCurrentFV.html) component is used to account for the counter-current thermal power exchange.

Two models are available in the package, the `ShellAndTube_WaterOil` and the `ShellAndTube_OilOil` component. The only difference between the two models is related to the shell side flow model. In the first case, it is assumed to have two-phase water on the shell side; thus, the [Flow1DV2ph](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV2ph.html) component is adopted. On the other hand, oil or other single phase fluids are acceptable in the shell side of the `ShellAndTube_OilOil` since it is based on the [Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) class.


### Static heat exchanger
`HX_0D` is a simplified heat exchanger model that does not account for dynamic aspects. The heat transfer in the components is governed by the following equation:
$$
Q= \text{UA} \cdot \Delta T_{lm}
$$

where $\text{UA}$ is the golbal conductance and $\Delta T_{lm}$ the logarithmic mean temperature difference.



## Control strategy
The `Control` package provides examples of controllers to be coupled to the sensible loop model. In the proposed control scheme, the sensible fluid flow rate flowing from one tank to the other is regulated by adjusting the valve opening to maintain the target tank at its setpoint temperature. 

## Provided examples
Several demonstration cases are available in the `Test` package, showcasing the dynamic behaviour of the various `SensibleLoop` versions in response to the same transient scenarios, i.e., a ramp-wase decrease of 50% charging and discharging flows. 

An application in a more complex energy system, with the TES model directly coupled to the BOP model in ThermoSysPro and the NSSS model in ThermoPower, is available in the `TestCases` package.



# Electrical energy storage system
The `ElectricalStorage` povides the black box model `SimpleBattery` to simulate the charging and discharging process of a battery exchanging electrical power.

## Model description
The `SimpleBattery` model relies on components from the Modelica Standard Library and from the ThermoPower library and does not account for any dynamic features of the component. The [Integrator](https://doc.modelica.org/om/Modelica.Blocks.Continuous.Integrator.html) component is used to integrate the input signal for the exchanged power, which corresponds to the state of charge of the battery. The latter variable is an output of the model, as it will be adopted in the controller to regulate the charging and discharging processes. 

The input power signal is associated to the [PowerConnection](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.PowerConnection.html) connector of the ThermoPower library to facilitate the coupling of `SimpleBattery` with other models.


## Control system
An example of controller `BatteryControl`, meant to translate an input power signal from a supervisory control system into an acceptable exchanged power signal for the battery, is also included in this package. It is based solely on components from the Modelica Standard Library that are used to regulate the exchanged power accounting for the operational limits of the battery, i.e., maximal charging and discharging power and state of charge. For example, if the state of charge reaches the maximal capacity, the controller transmits only negative power signals, preventing further charging of the battery.



## Test case
The demonstration case `Test_SimpleBattery`, included in the `Test` subpackage, illustrates a possible utilisation of the `SimpleBattery` and `BatteryControl` components by simulating the charging and discharging process of a battery connected to the electrical grid (represented by ThermoPower's [Grid](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.Grid.html) component). 

