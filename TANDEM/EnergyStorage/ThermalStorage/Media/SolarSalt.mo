within TANDEM.EnergyStorage.ThermalStorage.Media;
package SolarSalt
extends Modelica.Media.Interfaces.PartialLinearFluid(
  mediumName="SolarSalt",
  constantJacobian=true,
  reference_p=101325,
  reference_T=273.15+300,
  reference_d=2263.641-0.636*reference_T,
  reference_h=cp_const*reference_T,
  reference_s=0,
  cp_const=1396.044+0.172*reference_T,
  beta_const=0.636/reference_d,
  kappa_const=4.08008e-10-8.08338e-13*reference_T+8.56603e-16*reference_T^2,
  MM_const=252e-3);

  // Correlations form "Molten salts database for energy applications", R. Serrano-Lopez et al., 2013
  // and IMPLEMENTATION OF SOLAR SALT AS FLUID IN ASYST4.1 AND VALIDATION FOR A NATURAL CIRCULATION LOOP, Trivedi et al., 2021

redeclare function extends dynamicViscosity
  "Dynamic viscosity of Solar salts"
algorithm
  eta := 0.07543937-2.77e-4*state.T+3.48e-7*state.T^2-1.47e-10*state.T^3;
end dynamicViscosity;

redeclare function extends thermalConductivity
  "Thermal conductivity of Solar salts"
algorithm
  lambda := 0.443+1.9e-4*(state.T-273.15);
end thermalConductivity;

end SolarSalt;
