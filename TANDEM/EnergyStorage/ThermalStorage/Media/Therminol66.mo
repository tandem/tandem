within TANDEM.EnergyStorage.ThermalStorage.Media;
package Therminol66
extends Modelica.Media.Interfaces.PartialLinearFluid(
  mediumName="Therminol66",
  constantJacobian=true,
  reference_p=101325,
  reference_T=273.15+200,
  reference_d=885,
  reference_h=394e3,
  reference_s=0,
  cp_const=2.19e3,
  beta_const=0.000819,
  kappa_const=0,
  MM_const=252e-3);

  // Correlations form "Modeling  the INL TEDS in the Modelica Ecosystem", Frick, Brag-Sitton, Rabiti

redeclare function extends dynamicViscosity
  "Dynamic viscosity of Therminol"
algorithm
  eta := (-0.000321*(state.T-273.15)^2-0.614254*(state.T-273.15)+1020.62)*
          exp(586.375/((state.T-273.15)+62.5)-2.2809)*1e-6;
end dynamicViscosity;

redeclare function extends thermalConductivity
  "Thermal conductivity of Therminol"
algorithm
  lambda := -0.00000015*(state.T-273.15)^2-0.000033*(state.T-273.15)+0.118294;
end thermalConductivity;

end Therminol66;
