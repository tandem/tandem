within TANDEM.EnergyStorage.ThermalStorage;
model SensibleLoop_HX1D_WaterOil
  replaceable package SensibleMedium =
      TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66                                 constrainedby
    Modelica.Media.Interfaces.PartialMedium "Sensible medium model"
    annotation(choicesAllMatching = true);

    replaceable package OuterMedium =
      ThermoPower.Water.StandardWater                               constrainedby
    Modelica.Media.Interfaces.PartialMedium "Outer medium model"
    annotation(choicesAllMatching = true);

  ThermoPower.Water.FlangeA flangeA(redeclare package Medium = OuterMedium)
    annotation (Placement(transformation(extent={{-110,50},{-90,70}})));
  ThermoPower.Water.FlangeB flangeB(redeclare package Medium = OuterMedium)
    annotation (Placement(transformation(extent={{-110,-70},{-90,-50}})));
  ThermoPower.Water.FlangeA flangeA1(redeclare package Medium = OuterMedium)
    annotation (Placement(transformation(extent={{90,-70},{110,-50}})));
  ThermoPower.Water.FlangeB flangeB1(redeclare package Medium = OuterMedium)
    annotation (Placement(transformation(extent={{90,50},{110,70}})));
  Control.SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,90},{70,110}})));
  Control.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,90},{-50,110}})));

  Components.Tank tank_simp(
    redeclare package Medium = SensibleMedium,
    V=92059.97332025,
    Rin=48.939589375/2,
    initOpt=ThermoPower.Choices.Init.Options.fixedState)
    annotation (Placement(transformation(extent={{-18,38},{16,72}})));
  Components.Tank tank_simp1(
    redeclare package Medium = SensibleMedium,
    V=92059.97332,
    Rin=48.93958937/2,
    Tstart=463.15,
    initOpt=ThermoPower.Choices.Init.Options.fixedState)
    annotation (Placement(transformation(extent={{16,-42},{-18,-8}})));
  ThermoPower.Water.Pump dPump(
    redeclare package Medium = SensibleMedium,
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,
            0.8371856039374658,1.4914862836188105}, head_nom={34.48725361772262,
            23.62140658748125,0}),
    redeclare function efficiencyCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.constantEfficiency (eta_nom=0.9),
    rho0(displayUnit="kg/m3") = 841.5111,
    n0=1500,
    hstart=525399.083,
    w0=704.501,
    dp0(displayUnit="bar") = 195000,
    use_in_n=false)
    annotation (Placement(transformation(extent={{12,22},{30,40}})));

  ThermoPower.Water.Pump cPump(
    redeclare package Medium = SensibleMedium,
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,
            0.7895796460587576,1.4066739876826615}, head_nom={32.5261607186619,
            22.278192273056096,0}),
    redeclare function efficiencyCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.constantEfficiency (eta_nom=0.9),
    rho0(displayUnit="kg/m3") = 892.24815,
    n0=1500,
    hstart=372099.083,
    noInitialPressure=false,
    w0=704.501,
    dp0(displayUnit="bar") = 195000,
    use_in_n=false)
    annotation (Placement(transformation(extent={{-28,-58},{-46,-40}})));

  ThermoPower.Water.SensT1 Tht(redeclare package Medium = SensibleMedium)
    annotation (Placement(transformation(extent={{-56,36},{-44,48}})));
  ThermoPower.Water.SensT1 Tct(redeclare package Medium = SensibleMedium)
    annotation (Placement(transformation(extent={{30,-54},{44,-40}})));
  ThermoPower.Water.ValveLin valveLin(redeclare package Medium = SensibleMedium,
      Kv=0.001761252446183953)                  annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={-60,-28})));
  ThermoPower.Water.ValveLin valveLin1(redeclare package Medium =
        SensibleMedium, Kv=0.001761252446183953)
                                       annotation (Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={42,38})));

  Components.ShellAndTube_WaterOil condenser(
    redeclare package ShellMedium = OuterMedium,
    redeclare package TubeMedium = SensibleMedium,
    Nt=4195,
    Np=2,
    L=21.229,
    di=0.013,
    do=0.018,
    pitch=1.606*0.018,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=108e6,
    ms_nom=53.393,
    mt_nom=704.501,
    ps_nom=4500000,
    pt_nom=100000,
    hsi_nom=2944104,
    hso_nom=921356.136,
    hti_nom=372099.083,
    hto_nom=525399.083,
    redeclare model ShellHeatTransfer =
        TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.NusseltShell (
        useAverageTemperature=false,
        do=0.018,
        Ntubes=4195),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter) annotation (Placement(
        transformation(
        extent={{17,18.5},{-17,-18.5}},
        rotation=180,
        origin={-73,7.5})));

  Components.ShellAndTube_WaterOil evaporator(
    redeclare package ShellMedium = OuterMedium,
    redeclare package TubeMedium = SensibleMedium,
    Nt=4092,
    Np=2,
    L=11.795,
    di=0.013,
    do=0.018,
    pitch=1.606*0.018,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=-108e6,
    ms_nom=43.605,
    mt_nom=704.501,
    ps_nom=714700,
    pt_nom=100000,
    hsi_nom=487491.159,
    hso_nom=2964247.556,
    hti_nom=525399.083,
    hto_nom=372099.083,
    redeclare model ShellHeatTransfer =
        TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.Chen (
          useAverageTemperature=false),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter)
    annotation (Placement(transformation(extent={{92,18},{58,-19}})));
equation

  connect(tank_simp.WaterOutfl,dPump. infl) annotation (Line(points={{5.8,39.02},
          {5.8,32.8},{13.8,32.8}}, color={0,0,255}));
  connect(tank_simp1.WaterOutfl,cPump. infl) annotation (Line(points={{-7.8,-40.98},
          {-7.8,-47.2},{-29.8,-47.2}}, color={0,0,255}));
  connect(cPump.outfl, valveLin.inlet) annotation (Line(points={{-42.4,-42.7},{-42.4,
          -42},{-60,-42},{-60,-34}}, color={0,0,255}));
  connect(dPump.outfl, valveLin1.inlet)
    annotation (Line(points={{26.4,37.3},{26.4,38},{36,38}}, color={0,0,255}));
  connect(actuatorBus.Kd, valveLin1.cmd) annotation (Line(
      points={{-60,100},{-60,82},{42,82},{42,42.8}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  connect(Tht.flange, tank_simp.WaterInfl) annotation (Line(points={{-50,39.6},{
          -36,39.6},{-36,39.02},{-7.8,39.02}},  color={0,0,255}));
  connect(tank_simp1.WaterInfl, Tct.flange) annotation (Line(points={{5.8,-40.98},
          {6,-40.98},{6,-49.8},{37,-49.8}},     color={0,0,255}));
  connect(flangeB, condenser.flangeB) annotation (Line(points={{-100,-60},{
          -86.6,-60},{-86.6,-7.3}}, color={0,0,255}));
  connect(valveLin.outlet, condenser.flangeA1) annotation (Line(points={{-60,-22},
          {-59.06,-22},{-59.06,-7.3}},    color={0,0,255}));
  connect(condenser.flangeB1, tank_simp.WaterInfl) annotation (Line(points={{
          -59.4,22.3},{-59.4,39.02},{-7.8,39.02}}, color={0,0,255}));
  connect(evaporator.flangeA, flangeA1) annotation (Line(points={{88.6,-15.3},{
          88.6,-60},{100,-60}}, color={0,0,255}));
  connect(evaporator.flangeB, flangeB1) annotation (Line(points={{88.6,14.3},{
          88.6,48},{100,48},{100,60}}, color={0,0,255}));
  connect(evaporator.flangeB1, tank_simp1.WaterInfl) annotation (Line(points={{61.4,
          -15.3},{61.4,-32},{62,-32},{62,-50},{6,-50},{6,-40},{5.8,-40},{5.8,
          -40.98}},     color={0,0,255}));
  connect(evaporator.flangeA1, valveLin1.outlet)
    annotation (Line(points={{61.06,14.3},{61.06,38},{48,38}},
                                                             color={0,0,255}));
  connect(actuatorBus.Kc, valveLin.cmd) annotation (Line(
      points={{-60,100},{-60,82},{-40,82},{-40,-28},{-55.2,-28}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(flangeA, condenser.flangeA) annotation (Line(points={{-100,60},{-86.6,
          60},{-86.6,22.3}}, color={0,0,255}));
  connect(condenser.flangeB1, Tht.flange) annotation (Line(points={{-59.4,22.3},
          {-59.4,30},{-60,30},{-60,39.6},{-50,39.6}}, color={0,0,255}));
  connect(evaporator.flangeB1, Tct.flange) annotation (Line(points={{61.4,-15.3},
          {61.4,-32},{62,-32},{62,-49.8},{37,-49.8}}, color={0,0,255}));
  connect(sensorBus.Tht, tank_simp.Ttank) annotation (Line(
      points={{60,100},{60,90},{22,90},{22,59.42},{12.6,59.42}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tct, tank_simp1.Ttank) annotation (Line(
      points={{60,100},{60,90},{-18,90},{-18,-20.58},{-14.6,-20.58}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tht_in, Tht.T) annotation (Line(
      points={{60,100},{60,90},{-45.2,90},{-45.2,45.6}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tct_in, Tct.T) annotation (Line(
      points={{60,100},{60,90},{52,90},{52,-42},{48,-42},{48,-42.8},{42.6,-42.8}},
      color={255,219,88},
      thickness=0.5));

  connect(sensorBus.Level_HT, tank_simp.SOC) annotation (Line(
      points={{60,100},{60,90},{22,90},{22,64},{18,64},{18,63.84},{12.6,63.84}},

      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Level_CT, tank_simp1.SOC) annotation (Line(
      points={{60,100},{60,90},{-18,90},{-18,-16.16},{-14.6,-16.16}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,190,190},
          fillColor={0,190,190},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={0,190,190},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-60,64},{60,-56}},
          textColor={0,190,190},
          textString="TES")}),                                   Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}})));
end SensibleLoop_HX1D_WaterOil;
