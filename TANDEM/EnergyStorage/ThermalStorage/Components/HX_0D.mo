within TANDEM.EnergyStorage.ThermalStorage.Components;
model HX_0D
  replaceable package HotMedium = ThermoPower.Water.StandardWater constrainedby
    Modelica.Media.Interfaces.PartialMedium "Medium model"
    annotation(choicesAllMatching = true);

    replaceable package ColdMedium = ThermoPower.Water.StandardWater constrainedby
    Modelica.Media.Interfaces.PartialMedium "Medium model"
    annotation(choicesAllMatching = true);

    parameter Boolean computeUA = true "Compute UA from the nominal conditions, else UAnom";
    parameter Modelica.Units.SI.ThermalConductance UAnom "Nominal thermal conductance" annotation(Dialog(group="Nominal conditions"));

    parameter Modelica.Units.SI.MassFlowRate mh_nom "Hot side flow rate" annotation(Dialog(group="Nominal conditions"));
    parameter Modelica.Units.SI.MassFlowRate mc_nom "Cold side flow rate" annotation(Dialog(group="Nominal conditions"));

    parameter Modelica.Units.SI.Pressure ph_nom "Hot side pressure" annotation(Dialog(group="Nominal conditions"));
    parameter Modelica.Units.SI.Pressure pc_nom "Cold side pressure" annotation(Dialog(group="Nominal conditions"));

    parameter Modelica.Units.SI.SpecificEnthalpy h_hi_nom "Hot side inlet enthalpy" annotation(Dialog(group="Nominal conditions"));
    parameter Modelica.Units.SI.SpecificEnthalpy h_ho_nom "Hot side outlet enthalpy" annotation(Dialog(group="Nominal conditions"));
    parameter Modelica.Units.SI.SpecificEnthalpy h_ci_nom "Cold side inlet enthalpy" annotation(Dialog(group="Nominal conditions"));
    parameter Modelica.Units.SI.SpecificEnthalpy h_co_nom "Cold side outlet enthalpy" annotation(Dialog(group="Nominal conditions"));

   parameter Modelica.Units.SI.Pressure dph_nom "Nominal hot side pressure drop" annotation(Dialog(group="Nominal conditions"));
   parameter Modelica.Units.SI.Pressure dpc_nom "Nominal cold side pressure drop" annotation(Dialog(group="Nominal conditions"));

   parameter Boolean CounterCurrent = true "Counter-current configuration, else co-current";

  Modelica.Units.SI.Power Q(start = Qnom);
  Modelica.Units.SI.MassFlowRate mh(start=mh_nom);
  Modelica.Units.SI.MassFlowRate mc(start=mc_nom);

  Modelica.Units.SI.Pressure ph(start=ph_nom);
  Modelica.Units.SI.Pressure pc(start=pc_nom);

  HotMedium.ThermodynamicState state_hi;
  HotMedium.ThermodynamicState state_ho;
  ColdMedium.ThermodynamicState state_ci;
  ColdMedium.ThermodynamicState state_co;

  HotMedium.ThermodynamicState state_h;
  ColdMedium.ThermodynamicState state_c;

  Modelica.Units.SI.SpecificEnthalpy h_hi(start=h_hi_nom);
  Modelica.Units.SI.SpecificEnthalpy h_ho(start=h_ho_nom);
  Modelica.Units.SI.SpecificEnthalpy h_ci(start=h_ci_nom);
  Modelica.Units.SI.SpecificEnthalpy h_co(start=h_co_nom);

  Modelica.Units.SI.Temperature T_hi(start=T_hi_start);
  Modelica.Units.SI.Temperature T_ho(start=T_ho_start);
  Modelica.Units.SI.Temperature T_ci(start=T_ci_start);
  Modelica.Units.SI.Temperature T_co(start=T_co_start);

  Modelica.Units.SI.TemperatureDifference dT_1(start=if CounterCurrent then T_hi_start-T_co_start else T_hi_start-T_ci_start);
  Modelica.Units.SI.TemperatureDifference dT_2(start=if CounterCurrent then T_ho_start-T_ci_start else T_ho_start-T_co_start);

  Modelica.Units.SI.TemperatureDifference LMTD(start=LMTDnom);


  final parameter Modelica.Units.SI.ThermalConductance UA(fixed=false);

  final parameter Modelica.Units.SI.Power Qnom = abs(mh_nom*(h_hi_nom-h_ho_nom));
  final parameter Modelica.Units.SI.TemperatureDifference LMTDnom = (T_hi_start-T_co_start - (T_ho_start-T_ci_start))/log((T_hi_start-T_co_start)/(T_ho_start-T_ci_start));



  ThermoPower.Water.FlangeA in1(redeclare package Medium = HotMedium)
    annotation (Placement(transformation(extent={{-88,70},{-68,90}}),
        iconTransformation(extent={{-88,70},{-68,90}})));
  ThermoPower.Water.FlangeA in2(redeclare package Medium = ColdMedium)
    annotation (Placement(transformation(extent={{70,-88},{90,-68}}),
        iconTransformation(extent={{70,-88},{90,-68}})));
  ThermoPower.Water.FlangeB out1(redeclare package Medium = HotMedium)
    annotation (Placement(transformation(extent={{-86,-90},{-66,-70}}),
        iconTransformation(extent={{-86,-90},{-66,-70}})));
  ThermoPower.Water.FlangeB out2(redeclare package Medium = ColdMedium)
    annotation (Placement(transformation(extent={{70,70},{90,90}}),
        iconTransformation(extent={{70,70},{90,90}})));

protected
  parameter Modelica.Units.SI.Temperature T_hi_start=HotMedium.temperature_ph(ph_nom, h_hi_nom);
  parameter Modelica.Units.SI.Temperature T_ho_start=HotMedium.temperature_ph(ph_nom, h_ho_nom);
  parameter Modelica.Units.SI.Temperature T_ci_start=ColdMedium.temperature_ph(pc_nom, h_ci_nom);
  parameter Modelica.Units.SI.Temperature T_co_start=ColdMedium.temperature_ph(pc_nom, h_co_nom);


initial equation
 if computeUA then
   UA = Qnom/LMTDnom;
 else
   UA = UAnom;
 end if;


equation
  // Fluid properties
  state_hi =HotMedium.setState_phX(in1.p, in1.h_outflow);
  T_hi = HotMedium.temperature(state_hi);
  state_ho =HotMedium.setState_phX(out1.p, out1.h_outflow);
  T_ho = HotMedium.temperature(state_ho);
  state_ci =ColdMedium.setState_phX(in2.p, in2.h_outflow);
  T_ci = ColdMedium.temperature(state_ci);
  state_co =ColdMedium.setState_phX(out2.p, out2.h_outflow);
  T_co = ColdMedium.temperature(state_co);

  state_h =HotMedium.setState_phX(in1.p, (h_hi + h_ho)/2);
  state_c =ColdMedium.setState_phX(in2.p, (h_ci + h_co)/2);

  mh =in1.m_flow;
  mh =-out1.m_flow;
  mc =in2.m_flow;
  mc =-out2.m_flow;

  h_hi =in1.h_outflow;
  h_hi =inStream(in1.h_outflow);
  h_ho =out1.h_outflow;

  h_ci =in2.h_outflow;
  h_ci =inStream(in2.h_outflow);
  h_co =out2.h_outflow;

  Q = UA*LMTD;

  ph =in1.p;
  ph =out1.p + dph_nom;
  pc =in2.p;
  pc =out2.p + dpc_nom;

  if T_hi > T_ci then
    LMTD = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.LMTDrobust(T_hi, T_ho, T_ci, T_co, 0.7,5);
    Q = mh*(h_hi-h_ho);
    Q = mc*(h_co-h_ci);
  else
    LMTD = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.LMTDrobust(T_ci, T_co, T_hi, T_ho, 0.7,5);
    Q = -mh*(h_hi-h_ho);
    Q = -mc*(h_co-h_ci);
  end if;

  if CounterCurrent then
    dT_1 = T_hi-T_co;
    dT_2 = T_ho-T_ci;
  else
    dT_1 = T_hi-T_ci;
    dT_2 = T_ho-T_co;
  end if;


  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Polygon(points={{-54,70},{-54,70}}, lineColor={28,108,200}),
        Polygon(points={{-56,68},{-56,68}}, lineColor={28,108,200}),
        Polygon(
          points={{-48,4},{-48,4}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-78,100},{82,70}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Ellipse(
          extent={{-78,-68},{82,-100}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-78,86},{82,-84}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.VerticalCylinder),
        Line(
          points={{-26,-80},{-26,-40},{-2,-20},{-50,20},{-26,40},{-26,80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{30,80},{30,40},{10,20},{54,-20},{30,-40},{30,-80}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{-26,80},{-72,80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{-26,-80},{-72,-80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{76,-80},{30,-80}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{76,80},{30,80}},
          color={0,0,255},
          thickness=1)}),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HX_0D;
