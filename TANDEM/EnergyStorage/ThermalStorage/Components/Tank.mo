within TANDEM.EnergyStorage.ThermalStorage.Components;
model Tank "Simplified tank component with fixed cover gas pressure"
  replaceable package Medium =
      TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66                          constrainedby
    Modelica.Media.Interfaces.PartialMedium "Liquid medium model"
    annotation(choicesAllMatching = true);
  Medium.ThermodynamicState liquidState "Thermodynamic state of the liquid";

  parameter Modelica.Units.SI.Volume V = 750 "Total volume";
  parameter Modelica.Units.SI.Length Rin = 3.989 "Inner radius";
  parameter Modelica.Units.SI.Pressure pg = 1.01325e5 "Gas pressure";
 parameter Boolean allowFlowReversal=system.allowFlowReversal
    "= true to allow flow reversal, false restricts to design direction"
    annotation(Evaluate=true);

  parameter Boolean HeatLosses = false "Heat losses considered" annotation (Dialog(tab="Wall"));
  parameter Modelica.Units.SI.Length tw = 1 "Wall thickness" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.ThermalConductivity kw= 1 "Wall thermal conductivity" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.SpecificHeatCapacity cpw = 1 "Wall specific heat capacity" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.Density rhow = 1 "Wall density" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.Temperature Text = 25+273.15 "External temperature" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer alpha_lw = 100 "Inner heat transfer coefficient" annotation (Dialog(tab="Wall", enable = HeatLosses));
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer alpha_ex = 100 "External transfer coefficient" annotation (Dialog(tab="Wall", enable = HeatLosses));

  parameter Real zl_start = 0.5
    "Sensible fluid initial level with respect to total height"
    annotation (Dialog(tab="Initialisation"));
  parameter Medium.Temperature Tstart = 260+273.15 annotation (Dialog(tab="Initialisation"));
  parameter Medium.SpecificEnthalpy hl_start = Medium.specificEnthalpy(Medium.setState_pTX(pg, Tstart))  "Sensibile fluid initial enthalpy"
    annotation (Dialog(tab="Initialisation"));
  parameter Modelica.Units.SI.Temperature Tw_start = 200+273.15 "Sensibile fluid initial enthalpy"
    annotation (Dialog(tab="Initialisation", enable = HeatLosses));

  outer ThermoPower.System system "System wide properties";
  parameter ThermoPower.Choices.Init.Options initOpt=system.initOpt
    "Initialisation option" annotation (Dialog(tab="Initialisation"));
protected
  constant Modelica.Units.SI.Acceleration g=Modelica.Constants.g_n;
  constant Real pi=Modelica.Constants.pi;
  final parameter Modelica.Units.SI.Volume Vw = H*(Rout^2 - Rin^2)*pi "Wall volume";
  final parameter Modelica.Units.SI.Area A=pi*Rin^2 "Cross Sectional Area";
  final parameter Modelica.Units.SI.Length Rout=Rin+tw "Cross Sectional Area";
  final parameter Modelica.Units.SI.Length H=V/A "Tank heigth";
  final parameter Modelica.Units.SI.Area Aw_in=2*pi*Rin*H "Tank internal area";
  final parameter Modelica.Units.SI.Area Aw_out=2*pi*Rout*H "Tank external area";
public
  Medium.MassFlowRate wl_in "Water inflow mass flow rate";
  Medium.MassFlowRate wl_out "Water outflow mass flow rate";
  Modelica.Units.SI.Height zl(start=zl_start*H, stateSelect=StateSelect.prefer)
    "Water level (relative to reference)";
  Medium.SpecificEnthalpy hl_in "Water inlet specific enthalpy";
  Medium.SpecificEnthalpy hl_out "Water outlet specific enthalpy";
  Medium.SpecificEnthalpy hl(start=hl_start, stateSelect=StateSelect.prefer)
    "Water internal specific enthalpy";
  Modelica.Units.SI.Volume Vl "Volume occupied by water";
  Modelica.Units.SI.Volume Vg "Volume occupied by gas";
  Medium.AbsolutePressure p "Average fluid pressure";
  Medium.AbsolutePressure pf "Pressure at the outlet flange";
  ThermoPower.Units.LiquidDensity rho "Density of the water";
  Medium.Temperature T(start=Tstart) "Fluid temperature";

  // Wall
  Modelica.Units.SI.Power Qlw;
  Modelica.Units.SI.Power Qex;
  Modelica.Units.SI.Temperature Tw(start=Tw_start);
  Modelica.Units.SI.Temperature Tin;
  Modelica.Units.SI.Temperature Tout;

  ThermoPower.Water.FlangeA WaterInfl(redeclare package Medium = Medium, m_flow(
        min=if allowFlowReversal then -Modelica.Constants.inf else 0))
    annotation (Placement(transformation(extent={{-50,-104},{-30,-84}},
          rotation=0), iconTransformation(extent={{-50,-104},{-30,-84}})));
  ThermoPower.Water.FlangeB WaterOutfl(redeclare package Medium = Medium,
      m_flow(max=if allowFlowReversal then +Modelica.Constants.inf else 0))
    annotation (Placement(transformation(extent={{30,-104},{50,-84}}, rotation=0),
        iconTransformation(extent={{30,-104},{50,-84}})));
  Modelica.Blocks.Interfaces.RealOutput Ttank annotation (Placement(
        transformation(extent={{70,16},{90,36}}), iconTransformation(extent={{70,
            16},{90,36}})));
  Modelica.Blocks.Interfaces.RealOutput SOC annotation (Placement(
        transformation(extent={{70,58},{90,78}}), iconTransformation(extent={{70,
            42},{90,62}})));
equation

  //Equations for water and gas volumes and exchanged thermal power
  Vl = A*zl;
  Vg = V - Vl;
  SOC = zl/H;

  // Boundary conditions
  hl_in = WaterInfl.h_outflow;
  hl_in = inStream(WaterInfl.h_outflow);
  hl_out = hl;
  hl_out = WaterOutfl.h_outflow;

  wl_in = WaterInfl.m_flow;
  wl_out = WaterOutfl.m_flow;
  WaterInfl.p = pg;
  WaterOutfl.p = pf;

  rho*A*der(zl) = wl_in + wl_out;
  rho*A*zl*der(hl) - A*zl*der(p) - p*A*der(zl) = wl_in*(hl_in - hl) + wl_out*(hl_out - hl)-Qlw;

  // Set liquid properties
  liquidState = Medium.setState_phX(p, hl);
  rho = Medium.density(liquidState);
  T = Medium.temperature(liquidState);
  Ttank = T;


  pf = pg + rho*g*zl "Stevino's law";
  p  = pg + rho*g*zl/2 "Average pressure";

  // Heat losses
  if HeatLosses then
    Qlw = alpha_lw*Aw_in*(T-Tin);
    Qlw = (kw*(2*pi*H)*(Tin - Tw))/(log((Rin + Rout)/(2*Rin))) "Heat conduction in the inner half of the wall";
    Qex = (kw*(2*pi*H)*(Tout - Tw))/(log((2*Rout)/(Rin + Rout))) "Heat conduction in the outer half of the wall";
    Qex = alpha_ex*Aw_out*(Text-Tout);
    rhow*cpw*Vw*der(Tw) = Qlw + Qex "Energy balance";
  else // Trivial equations
    Qlw = 0;
    Qex = 0;
    Tin = T;
    Tout = T;
    Tw = T;
  end if;

initial equation
  if initOpt == ThermoPower.Choices.Init.Options.noInit then
    // do nothing
  elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
    zl = zl_start*H;
    hl = hl_start;
    if HeatLosses then
      Tw = Tw_start;
    end if;
  elseif initOpt == ThermoPower.Choices.Init.Options.steadyState then
    zl = zl_start*H;
    der(hl) = 0;
    if HeatLosses then
      der(Tw) = 0;
    end if;
  elseif initOpt == ThermoPower.Choices.Init.Options.steadyStateNoP then
    zl = zl_start*H;
    der(hl) = 0;
    if HeatLosses then
      der(Tw) = 0;
    end if;

  else
    assert(false, "Unsupported initialisation option");
  end if;

  annotation (Icon(graphics={
        Polygon(points={{-68,84},{-68,84}}, lineColor={28,108,200}),
        Polygon(points={{-70,82},{-70,82}}, lineColor={28,108,200}),
        Polygon(
          points={{-42,96},{-42,96}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-60,18},{-60,18}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-80,98},{80,22}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Ellipse(
          extent={{-80,-64},{80,-96}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-80,60},{80,-80}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.VerticalCylinder),
        Ellipse(
          extent={{-74,-60},{74,-90}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-74,26},{74,-76}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.VerticalCylinder),
        Ellipse(
          extent={{-74,94},{74,30}},
          lineColor={135,135,135},
          pattern=LinePattern.None,
          fillColor={192,220,255},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-74,60},{74,26}},
          lineColor={135,135,135},
          pattern=LinePattern.None,
          fillColor={192,220,255},
          fillPattern=FillPattern.VerticalCylinder)}));
end Tank;
