within TANDEM.EnergyStorage.ThermalStorage.Components;
model ShellAndTube_OilOil
  replaceable package ShellMedium = ThermoPower.Water.StandardWater constrainedby
    Modelica.Media.Interfaces.PartialMedium "Shell side medium model"
    annotation(choicesAllMatching = true);
  replaceable package TubeMedium =
      ThermoPower.Water.StandardWater                                 constrainedby
    Modelica.Media.Interfaces.PartialMedium "Tube side medium model"
    annotation(choicesAllMatching = true);

  parameter Integer Nodes=11 "Number of axial nodes";
  parameter Integer Nt "Number of tubes";
  parameter Integer Np = 2 "Number of passes";
  parameter Modelica.Units.SI.Length L "Tube length";
  parameter Modelica.Units.SI.Length di "Tube inner diameter";
  parameter Modelica.Units.SI.Length do "Tube outer diameter";
  parameter Modelica.Units.SI.Length pitch "Pitch of the tubes";
  parameter Real Bfrac "Baffle fraction";

  parameter Real CL = 1 "Tube layout constant";
  parameter Real CTP = 0.9 "Tube count calculation constant";

  parameter Modelica.Units.SI.Density rhom = 7763 "Tube density";
  parameter Modelica.Units.SI.SpecificHeatCapacity cpm = 510.4 "Tube specific heat capacity";
  parameter Modelica.Units.SI.ThermalConductivity km = 57.45 "Tube thermal conductivity";

  parameter Modelica.Units.SI.Power Qnom "Nominal exchanged power" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.MassFlowRate ms_nom "Shell side nominal flow" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.MassFlowRate mt_nom "Tube side nominal flow" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.Pressure ps_nom "Shell nominal pressure" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.Pressure pt_nom "Tube nominal pressure" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.SpecificEnthalpy hsi_nom "Shell inlet enthalpy" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.SpecificEnthalpy hso_nom "Shell outlet enthalpy" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.SpecificEnthalpy hti_nom "Tube inlet enthalpy" annotation(Dialog(group="Nominal conditions"));
  parameter Modelica.Units.SI.SpecificEnthalpy hto_nom "Tube outer enthalpy" annotation(Dialog(group="Nominal conditions"));

  replaceable model ShellHeatTransfer =
      ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer
    constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
                                                           "Shell side heat transfer model"
    annotation (choicesAllMatching=true);

  replaceable model TubeHeatTransfer =
      ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer
    constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
                                                           "Tube side heat transfer model"
    annotation (choicesAllMatching=true);

  final parameter Real PR = pitch/do;
  final parameter Modelica.Units.SI.Area Ao = Np*pi*do*Nt*L;
  final parameter Modelica.Units.SI.Length Ds = 0.637*sqrt(CL/CTP)*sqrt(Ao*PR^2*do/L);
  final parameter Modelica.Units.SI.Length C = pitch-do;
  final parameter Modelica.Units.SI.Length B = Bfrac*Ds;
  final parameter Modelica.Units.SI.Area As = Ds*C*B/pitch;
  final parameter Modelica.Units.SI.Area As_tot = pi*(Ds/2)^2-Nt*Np*pi*(do/2)^2;

  ThermoPower.Water.FlangeB flangeB(redeclare package Medium = ShellMedium,
    m_flow(start=-ms_nom),
    p(start=ps_nom),
    h_outflow(start=hso_nom))
    annotation (Placement(transformation(extent={{-90,-90},{-70,-70}}),
        iconTransformation(extent={{-90,-90},{-70,-70}})));
  ThermoPower.Water.FlangeA flangeA(redeclare package Medium = ShellMedium,
    p(start=ps_nom),
    m_flow(start=ms_nom),
    h_outflow(start=hsi_nom))
    annotation (Placement(transformation(extent={{-90,70},{-70,90}}),
        iconTransformation(extent={{-90,70},{-70,90}})));
  ThermoPower.Water.FlangeA flangeA1(redeclare package Medium = TubeMedium,
    p(start=pt_nom),
    m_flow(start=mt_nom),
    h_outflow(start=hti_nom))
    annotation (Placement(transformation(extent={{72,-90},{92,-70}}),
        iconTransformation(extent={{72,-90},{92,-70}})));
  ThermoPower.Water.FlangeB flangeB1(redeclare package Medium = TubeMedium,
    p(start=pt_nom),
    m_flow(start=-mt_nom),
    h_outflow(start=hto_nom))
    annotation (Placement(transformation(extent={{70,70},{90,90}}),
        iconTransformation(extent={{70,70},{90,90}})));
  ThermoPower.Water.Flow1DFV    shell_1ph(
    redeclare package Medium = ShellMedium,
    N=Nodes,
    L=L,
    A=As,
    omega=Np*Nt*pi*do,
    Dhyd=4*(pitch^2 - pi*do^2/4)/pi/do,
    wnom=ms_nom,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.NoFriction,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
    pstart=ps_nom,
    hstartin=hsi_nom,
    hstartout=hso_nom,
    redeclare model HeatTransfer =
        ShellHeatTransfer,
    fixedMassFlowSimplified=true,
    Q(start=-Qnom))                       annotation (Placement(transformation(
        extent={{14,-14},{-14,14}},
        rotation=180,
        origin={0,50})));
  ThermoPower.Water.Flow1DFV tube(
    redeclare package Medium = TubeMedium,
    N=Nodes,
    Nt=Nt,
    L=L*Np,
    A=pi*di^2/4,
    omega=pi*di,
    Dhyd=di,
    wnom=mt_nom,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.NoFriction,
    pstart=pt_nom,
    hstartin=hti_nom,
    hstartout=hto_nom,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer =
        TubeHeatTransfer,
    Q(start=Qnom))
    annotation (Placement(transformation(extent={{14,-64},{-14,-36}})));
  ThermoPower.Thermal.MetalTubeFV metalTubeFV(
    Nw=Nodes - 1,
    Nt=Np*Nt,
    L=L,
    rint=di/2,
    rext=do/2,
    rhomcm=rhom*cpm,
    lambda=km,
    Tstartbar=453.15)                         annotation (Placement(
        transformation(extent={{-14,-14},{14,14}}, rotation=180,
        origin={0,-10})));
  ThermoPower.Thermal.HeatExchangerTopologyFV heatExchangerTopologyFV(Nw=Nodes -
        1, redeclare model HeatExchangerTopology =
        ThermoPower.Thermal.HeatExchangerTopologies.CounterCurrentFlow)
                                                                  annotation (
      Placement(transformation(
        extent={{-12,-12},{12,12}},
        rotation=180,
        origin={0,16})));

  ThermoPower.Water.SensT1 sensT1_1(redeclare package Medium = TubeMedium, T(
        start=TubeMedium.temperature_ph(pt_nom,hto_nom)))
    annotation (Placement(transformation(extent={{-52,74},{-32,94}})));
protected
    constant Real pi=Modelica.Constants.pi;

equation
  connect(flangeA1, tube.infl) annotation (Line(points={{82,-80},{60,-80},{60,
          -50},{14,-50}},
                     color={0,0,255}));
  connect(shell_1ph.wall, heatExchangerTopologyFV.side2)
    annotation (Line(points={{0,43},{0,19.72}}, color={255,127,0}));
  connect(heatExchangerTopologyFV.side1, metalTubeFV.ext)
    annotation (Line(points={{0,12.4},{0,-5.66}},color={255,127,0}));
  connect(flangeA,shell_1ph. infl) annotation (Line(points={{-80,80},{-60,80},{
          -60,50},{-14,50}},
                         color={0,0,255}));
  connect(metalTubeFV.int, tube.wall)
    annotation (Line(points={{0,-14.2},{0,-43}}, color={255,127,0}));
  connect(tube.outfl, flangeB1) annotation (Line(points={{-14,-50},{-42,-50},{
          -42,80},{80,80}}, color={0,0,255}));
  connect(tube.outfl, sensT1_1.flange)
    annotation (Line(points={{-14,-50},{-42,-50},{-42,80}}, color={0,0,255}));
  connect(shell_1ph.outfl, flangeB) annotation (Line(points={{14,50},{40,50},{
          40,-80},{-80,-80}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Polygon(
          points={{-50,4},{-50,4}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-80,100},{80,70}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Ellipse(
          extent={{-80,-68},{80,-100}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-80,86},{80,-84}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.VerticalCylinder),
        Line(
          points={{-28,-80},{-28,-40},{-4,-20},{-52,20},{-28,40},{-28,80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{28,80},{28,40},{8,20},{52,-20},{28,-40},{28,-80}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{-28,80},{-74,80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{-28,-80},{-74,-80}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{74,-80},{28,-80}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{74,80},{28,80}},
          color={0,0,255},
          thickness=1)}),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ShellAndTube_OilOil;
