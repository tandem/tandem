within TANDEM.EnergyStorage.ThermalStorage.Control;
model TEScontroller_Tin_ctrl

  parameter Modelica.Units.SI.Temperature Thot_set = 260+273.15 "Hot tank setpoint temperature";
  parameter Modelica.Units.SI.Temperature Tcold_set = 205+273.15 "Cold tank setpoint temperature";
  parameter Real Kp_char = 1000.6248 "Charging controller proportional gain";
  parameter Real Ki_char = 0.52714 "Charging controller integral gain";
  parameter Real Kp_disc = Kp_char "Discharging controller proportional gain";
  parameter Real Ki_disc = Ki_char "Discharging controller integral gain";

  SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  Modelica.Blocks.Sources.Constant const1(k=0)
    annotation (Placement(transformation(extent={{92,74},{78,88}})));
  Modelica.Blocks.Continuous.LimPID PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=Kp_disc,
    Ti=Kp_disc/Ki_disc,
    yMax=0,
    yMin=-0.99,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=-0.3,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
    annotation (Placement(transformation(extent={{-36,34},{-48,46}})));
  Modelica.Blocks.Continuous.LimPID PID1(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=Kp_char,
    Ti=Kp_char/Ki_char,
    yMax=0,
    yMin=-0.99,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=-0.99,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
    annotation (Placement(transformation(extent={{-34,-38},{-46,-26}})));
  Modelica.Blocks.Sources.Constant const4(k=1)
    annotation (Placement(transformation(extent={{-20,74},{-34,88}})));
  Modelica.Blocks.Math.Add add4
    annotation (Placement(transformation(extent={{-66,38},{-78,50}})));
  Modelica.Blocks.Math.Add add1
    annotation (Placement(transformation(extent={{-66,-34},{-78,-22}})));
  Modelica.Blocks.Math.Add add2(k1=1, k2=-1)
    annotation (Placement(transformation(extent={{6,-62},{-4,-52}})));
  Modelica.Blocks.Math.Division division
    annotation (Placement(transformation(extent={{-18,-42},{-30,-54}})));
  Modelica.Blocks.Sources.Constant const2(k=Thot_set)
    annotation (Placement(transformation(extent={{38,-50},{26,-38}})));
  Modelica.Blocks.Math.Add add3(k1=-1)
    annotation (Placement(transformation(extent={{6,2},{-4,12}})));
  Modelica.Blocks.Math.Division division1
    annotation (Placement(transformation(extent={{-18,22},{-30,10}})));
  Modelica.Blocks.Sources.Constant const3(k=Tcold_set)
    annotation (Placement(transformation(extent={{40,14},{28,26}})));
equation
  connect(PID1.u_s,const1. y) annotation (Line(points={{-32.8,-32},{54,-32},{54,
          81},{77.3,81}},         color={0,0,127}));
  connect(PID.u_s,const1. y) annotation (Line(points={{-34.8,40},{54,40},{54,81},
          {77.3,81}}, color={0,0,127}));
  connect(PID.y,add4. u2)
    annotation (Line(points={{-48.6,40},{-56,40},{-56,40.4},{-64.8,40.4}},
                                                   color={0,0,127}));
  connect(add1.u1,const4. y) annotation (Line(points={{-64.8,-24.4},{-56,-24.4},
          {-56,81},{-34.7,81}},
                          color={0,0,127}));
  connect(actuatorBus.Kc,add1. y) annotation (Line(
      points={{-60,-100},{-60,-84},{-94,-84},{-94,-28},{-78.6,-28}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.Kd,add4. y) annotation (Line(
      points={{-60,-100},{-60,-84},{-94,-84},{-94,44},{-78.6,44}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(const2.y,add2. u1) annotation (Line(points={{25.4,-44},{14,-44},{14,-54},
          {7,-54}},     color={0,0,127}));
  connect(division.u2,const2. y) annotation (Line(points={{-16.8,-44.4},{-14,-44},
          {25.4,-44}},              color={0,0,127}));
  connect(division.u1,add2. y) annotation (Line(points={{-16.8,-51.6},{-10,-51.6},
          {-10,-57},{-4.5,-57}},color={0,0,127}));
  connect(division.y,PID1. u_m) annotation (Line(points={{-30.6,-48},{-40,-48},{
          -40,-39.2}},color={0,0,127}));
  connect(const3.y,add3. u1) annotation (Line(points={{27.4,20},{14,20},{14,10},
          {7,10}},      color={0,0,127}));
  connect(division1.u2,const3. y)
    annotation (Line(points={{-16.8,19.6},{-14,20},{27.4,20}},
                                                             color={0,0,127}));
  connect(division1.u1,add3. y) annotation (Line(points={{-16.8,12.4},{-10,12.4},
          {-10,7},{-4.5,7}},color={0,0,127}));
  connect(PID.u_m,division1. y)
    annotation (Line(points={{-42,32.8},{-42,16},{-30.6,16}},
                                                            color={0,0,127}));
  connect(PID1.y, add1.u2) annotation (Line(points={{-46.6,-32},{-56,-32},{-56,-31.6},
          {-64.8,-31.6}}, color={0,0,127}));
  connect(add4.u1, const4.y) annotation (Line(points={{-64.8,47.6},{-56,47.6},{-56,
          81},{-34.7,81}}, color={0,0,127}));
  connect(sensorBus.Tht_in, add2.u2) annotation (Line(
      points={{60,-100},{60,-60},{7,-60}},
      color={255,219,88},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(sensorBus.Tct_in, add3.u2) annotation (Line(
      points={{60,-100},{60,4},{7,4}},
      color={255,219,88},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={102,44,145},
          fillColor={102,44,145},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={102,44,145},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-90,78},{88,-68}},
          textColor={102,44,145},
          textString="TES
control")}),                                                     Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TEScontroller_Tin_ctrl;
