within TANDEM.EnergyStorage.ThermalStorage.Control;
model TEScontroller_SST
  SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  Modelica.Blocks.Sources.Constant const(k=1) annotation (Placement(
        transformation(extent={{-10,-10},{10,10}}, rotation=180)));
  Modelica.Blocks.Sources.Constant const1(k=1) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={0,-40})));
equation
  connect(actuatorBus.Kd, const1.y) annotation (Line(
      points={{-60,-100},{-60,-40},{-11,-40}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.Kc, const.y) annotation (Line(
      points={{-60,-100},{-60,0},{-11,0}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={102,44,145},
          fillColor={102,44,145},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={102,44,145},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-90,78},{88,-68}},
          textColor={102,44,145},
          textString="TES
control")}),                                                     Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TEScontroller_SST;
