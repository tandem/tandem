within TANDEM.EnergyStorage.ThermalStorage.Control;
model TEScontroller_Tin_lvl_ctrl

  parameter Modelica.Units.SI.Temperature Thot_set = 260+273.15 "Hot tank setpoint temperature";
  parameter Modelica.Units.SI.Temperature Tcold_set = 205+273.15 "Cold tank setpoint temperature";
  parameter Real min_level = 0;
  parameter Real max_level = 1;
  parameter Real Kp_char = 1000.6248 "Charging controller proportional gain";
  parameter Real Ki_char = 0.52714 "Charging controller integral gain";
  parameter Real Kp_disc = Kp_char "Discharging controller proportional gain";
  parameter Real Ki_disc = Ki_char "Discharging controller integral gain";

  SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  Modelica.Blocks.Sources.Constant const1(k=0)
    annotation (Placement(transformation(extent={{92,74},{78,88}})));
  Modelica.Blocks.Continuous.LimPID PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=Kp_disc,
    Ti=Kp_disc/Ki_disc,
    yMax=0,
    yMin=-0.99,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=-0.3,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
    annotation (Placement(transformation(extent={{-16,34},{-26,44}})));
  Modelica.Blocks.Continuous.LimPID PID1(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=Kp_char,
    Ti=Kp_char/Ki_char,
    yMax=0,
    yMin=-0.99,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=-0.99,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
    annotation (Placement(transformation(extent={{-16,-36},{-26,-26}})));
  Modelica.Blocks.Sources.Constant const4(k=1)
    annotation (Placement(transformation(extent={{-20,74},{-34,88}})));
  Modelica.Blocks.Math.Add add4
    annotation (Placement(transformation(extent={{-66,38},{-78,50}})));
  Modelica.Blocks.Math.Add add1
    annotation (Placement(transformation(extent={{-66,-34},{-78,-22}})));
  Modelica.Blocks.Math.Add add2(k1=1, k2=-1)
    annotation (Placement(transformation(extent={{22,-62},{12,-52}})));
  Modelica.Blocks.Math.Division division
    annotation (Placement(transformation(extent={{2,-42},{-6,-50}})));
  Modelica.Blocks.Sources.Constant const2(k=Thot_set)
    annotation (Placement(transformation(extent={{38,-50},{26,-38}})));
  Modelica.Blocks.Math.Add add3(k1=-1)
    annotation (Placement(transformation(extent={{18,2},{8,12}})));
  Modelica.Blocks.Math.Division division1
    annotation (Placement(transformation(extent={{-4,14},{-12,6}})));
  Modelica.Blocks.Sources.Constant const3(k=Tcold_set)
    annotation (Placement(transformation(extent={{40,14},{28,26}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold=min_level)
                                                            annotation (
      Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={14,70})));
  Modelica.Blocks.Logical.Switch switch5
    annotation (Placement(transformation(extent={{-4,-4},{4,4}},
        rotation=180,
        origin={-44,42})));
  Modelica.Blocks.Sources.Constant const5(k=-0.99)
    annotation (Placement(transformation(extent={{-24,62},{-30,68}})));
  Modelica.Blocks.Logical.And and2 annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=180,
        origin={-5,57})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-3,3},{3,-3}},
        rotation=90,
        origin={-21,23})));
  Modelica.Blocks.Sources.Constant const6(k=0)
    annotation (Placement(transformation(extent={{3,-3},{-3,3}},
        rotation=180,
        origin={-41,9})));
  Modelica.Blocks.Logical.LessThreshold    lessThreshold(threshold=max_level)
                                                             annotation (
      Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={14,50})));
  Modelica.Blocks.Logical.Switch switch2
    annotation (Placement(transformation(extent={{-4,-4},{4,4}},
        rotation=180,
        origin={-44,-28})));
  Modelica.Blocks.Sources.Constant const7(k=-0.99)
    annotation (Placement(transformation(extent={{-24,-8},{-30,-2}})));
  Modelica.Blocks.Logical.Switch switch3
    annotation (Placement(transformation(extent={{-3,3},{3,-3}},
        rotation=90,
        origin={-21,-45})));
  Modelica.Blocks.Sources.Constant const8(k=0)
    annotation (Placement(transformation(extent={{3,-3},{-3,3}},
        rotation=180,
        origin={-41,-59})));
  Modelica.Blocks.Logical.GreaterThreshold greaterThreshold1(threshold=
        min_level)                                          annotation (
      Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={28,-8})));
  Modelica.Blocks.Logical.And and1 annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=180,
        origin={9,-19})));
  Modelica.Blocks.Logical.LessThreshold    lessThreshold1(threshold=max_level)
                                                             annotation (
      Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={28,-24})));
equation
  connect(PID1.u_s,const1. y) annotation (Line(points={{-15,-31},{68,-31},{68,
          81},{77.3,81}},         color={0,0,127}));
  connect(PID.u_s,const1. y) annotation (Line(points={{-15,39},{54,39},{54,81},{
          77.3,81}},  color={0,0,127}));
  connect(add1.u1,const4. y) annotation (Line(points={{-64.8,-24.4},{-56,-24.4},
          {-56,81},{-34.7,81}},
                          color={0,0,127}));
  connect(actuatorBus.Kc,add1. y) annotation (Line(
      points={{-60,-100},{-60,-84},{-94,-84},{-94,-28},{-78.6,-28}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.Kd,add4. y) annotation (Line(
      points={{-60,-100},{-60,-84},{-94,-84},{-94,44},{-78.6,44}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(const2.y,add2. u1) annotation (Line(points={{25.4,-44},{26,-44},{26,
          -48},{23,-48},{23,-54}},
                        color={0,0,127}));
  connect(division.u2,const2. y) annotation (Line(points={{2.8,-43.6},{2.8,-44},
          {25.4,-44}},              color={0,0,127}));
  connect(division.u1,add2. y) annotation (Line(points={{2.8,-48.4},{6,-48.4},{
          6,-57},{11.5,-57}},   color={0,0,127}));
  connect(const3.y,add3. u1) annotation (Line(points={{27.4,20},{24,20},{24,10},
          {19,10}},     color={0,0,127}));
  connect(division1.u2,const3. y)
    annotation (Line(points={{-3.2,12.4},{2,12.4},{2,20},{27.4,20}},
                                                             color={0,0,127}));
  connect(division1.u1,add3. y) annotation (Line(points={{-3.2,7.6},{2,7.6},{2,7},
          {7.5,7}},         color={0,0,127}));
  connect(add4.u1, const4.y) annotation (Line(points={{-64.8,47.6},{-56,47.6},{-56,
          81},{-34.7,81}}, color={0,0,127}));
  connect(sensorBus.Tht_in, add2.u2) annotation (Line(
      points={{60,-100},{60,-86},{30,-86},{30,-60},{23,-60}},
      color={255,219,88},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(sensorBus.Tct_in, add3.u2) annotation (Line(
      points={{60,-100},{60,4},{19,4}},
      color={255,219,88},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(switch5.y, add4.u2) annotation (Line(points={{-48.4,42},{-50,42},{-50,
          40.4},{-64.8,40.4}}, color={0,0,127}));
  connect(and2.u2, greaterThreshold.y) annotation (Line(points={{1,61},{4,61},{4,
          70},{9.6,70}}, color={255,0,255}));
  connect(and2.y, switch5.u2) annotation (Line(points={{-10.5,57},{-32,57},{-32,
          42},{-39.2,42}}, color={255,0,255}));
  connect(switch1.u1, division1.y) annotation (Line(points={{-18.6,19.4},{-18.6,
          10},{-12.4,10}}, color={0,0,127}));
  connect(switch1.y, PID.u_m)
    annotation (Line(points={{-21,26.3},{-21,33},{-21,33}}, color={0,0,127}));
  connect(switch5.u1, PID.y) annotation (Line(points={{-39.2,38.8},{-26.5,38.8},
          {-26.5,39}}, color={0,0,127}));
  connect(switch1.u2, and2.y) annotation (Line(points={{-21,19.4},{-21,12},{-32,
          12},{-32,57},{-10.5,57}}, color={255,0,255}));
  connect(switch5.u3, const5.y) annotation (Line(points={{-39.2,45.2},{-36,45.2},
          {-36,65},{-30.3,65}}, color={0,0,127}));
  connect(const6.y, switch1.u3) annotation (Line(points={{-37.7,9},{-23.4,9},{-23.4,
          19.4}}, color={0,0,127}));
  connect(and2.u1, lessThreshold.y) annotation (Line(points={{1,57},{6,57},{6,
          50},{9.6,50}}, color={255,0,255}));
  connect(sensorBus.Level_CT, lessThreshold.u) annotation (Line(
      points={{60,-100},{60,50},{18.8,50}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Level_HT, greaterThreshold.u) annotation (Line(
      points={{60,-100},{60,70},{18.8,70}},
      color={255,219,88},
      thickness=0.5));
  connect(switch2.u3,const7. y) annotation (Line(points={{-39.2,-24.8},{-36,
          -24.8},{-36,-5},{-30.3,-5}},
                                color={0,0,127}));
  connect(switch2.u1, PID1.y) annotation (Line(points={{-39.2,-31.2},{-26.5,
          -31.2},{-26.5,-31}}, color={0,0,127}));
  connect(const8.y,switch3. u3) annotation (Line(points={{-37.7,-59},{-23.4,-59},
          {-23.4,-48.6}},
                  color={0,0,127}));
  connect(PID1.u_m, switch3.y) annotation (Line(points={{-21,-37},{-21,-41.7},{
          -21,-41.7}}, color={0,0,127}));
  connect(switch3.u1, division.y) annotation (Line(points={{-18.6,-48.6},{-18.6,
          -52},{-12,-52},{-12,-46},{-6.4,-46}}, color={0,0,127}));
  connect(add1.u2, switch2.y) annotation (Line(points={{-64.8,-31.6},{-58,-31.6},
          {-58,-28},{-48.4,-28}}, color={0,0,127}));
  connect(and1.u2, greaterThreshold1.y) annotation (Line(points={{15,-15},{18,
          -15},{18,-8},{23.6,-8}}, color={255,0,255}));
  connect(and1.u1, lessThreshold1.y) annotation (Line(points={{15,-19},{20,-19},
          {20,-24},{23.6,-24}}, color={255,0,255}));
  connect(and1.y, switch2.u2) annotation (Line(points={{3.5,-19},{-32,-19},{-32,
          -28},{-39.2,-28}}, color={255,0,255}));
  connect(switch3.u2, and1.y) annotation (Line(points={{-21,-48.6},{-21,-58},{
          -10,-58},{-10,-19},{3.5,-19}}, color={255,0,255}));
  connect(sensorBus.Level_HT, lessThreshold1.u) annotation (Line(
      points={{60,-100},{60,-24},{32.8,-24}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Level_CT, greaterThreshold1.u) annotation (Line(
      points={{60,-100},{60,-8},{32.8,-8}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={102,44,145},
          fillColor={102,44,145},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={102,44,145},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-90,78},{88,-68}},
          textColor={102,44,145},
          textString="TES
control")}),                                                     Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TEScontroller_Tin_lvl_ctrl;
