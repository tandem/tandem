within TANDEM.EnergyStorage.ThermalStorage.HeatTransfer;
model Chen "Flow boiling heat transfer correlation"
  extends ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV(redeclare replaceable package
                          Medium =
        ThermoPower.Water.StandardWater                                                                                                    constrainedby
      Modelica.Media.Interfaces.PartialTwoPhaseMedium                                                                                                   "Medium model");

  Modelica.Units.SI.CoefficientOfHeatTransfer gamma[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_trans[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_LP[Nf]
    "Heat transfer coefficients at the nodes";
   Modelica.Units.SI.CoefficientOfHeatTransfer gamma_VP[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_TP[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_vol[Nw]
    "Heat transfer coefficients at the volumes";
  Medium.Temperature Tvol[Nw]  "Fluid temperature in the volumes";
  Medium.DynamicViscosity mu[Nf] "Dynamic viscosity";
  Medium.ThermalConductivity k[Nf] "Thermal conductivity";
  Medium.SpecificHeatCapacity cp[Nf] "Heat capacity at constant pressure";

  Medium.ThermodynamicState bubble= Medium.setBubbleState(sat, 1) "Bubble point state";
  Medium.ThermodynamicState dew = Medium.setDewState(sat, 1) "Dew point state";
  Medium.SaturationProperties sat = Medium.setSat_p(p) "Properties of saturated fluid";

  Medium.DynamicViscosity mu_ls = Medium.dynamicViscosity(bubble) "Dynamic viscosity at bubble point";
  Medium.DynamicViscosity mu_vs = Medium.dynamicViscosity(dew)
                                                              "Dynamic viscosity at dew point";
  Medium.ThermalConductivity k_ls = Medium.thermalConductivity(bubble) "Thermal conductivity at bubble point";
  Medium.ThermalConductivity k_vs = Medium.thermalConductivity(dew) "Thermal conductivity at dew point";
  Medium.SpecificHeatCapacity cp_ls = Medium.heatCapacity_cp(bubble) "Specific heat capacity at bubble point";
  Medium.SpecificHeatCapacity cp_vs = Medium.heatCapacity_cp(dew) "Specific heat capacity at dew point";
  Medium.SurfaceTension sigma = Medium.surfaceTension(sat) "Surface tension";
  Medium.SpecificEnthalpy hl "Saturated liquid temperature";
  Medium.SpecificEnthalpy hv "Saturated vapour temperature";
  Medium.SpecificEnthalpy h[Nf] "Fluid enthalpy";
  Real x[Nf] "Steam quality";
  Modelica.Units.SI.Density rhol "Saturated liquid density";
  Modelica.Units.SI.Density rhov "Saturated vapour density";
  Medium.Temperature Ts "Fluid temperature in the volumes";
  Modelica.Units.SI.Pressure p;
  Modelica.Units.SI.Temperature Twf[Nf];
  Modelica.Units.SI.Power Qwf[Nf];

equation
  assert(Nw == Nf - 1, "Number of volumes Nw on wall side should be equal to number of volumes fluid side Nf - 1");

  Ts = sat.Tsat;
  rhol = Medium.bubbleDensity(sat);
  rhov = Medium.dewDensity(sat);
  hl = Medium.bubbleEnthalpy(sat);
  hv = Medium.dewEnthalpy(sat);

  p = Medium.pressure(fluidState[1]);

  for j in 1:Nf loop
    mu[j] = Medium.dynamicViscosity(fluidState[j]);
    k[j] = Medium.thermalConductivity(fluidState[j]);
    cp[j] = Medium.heatCapacity_cp(fluidState[j]);
    h[j] = Medium.specificEnthalpy(fluidState[j]);

    x[j] = (h[j] - hl)/(hv - hl);

    // Single phase, subcooled or superheated liquid flow
    gamma_LP[j] = homotopy(ThermoPower.Water.f_dittus_boelter(
      w[j],
      Dhyd,
      A,
      mu[j],
      k[j],
      cp[j]),
      ThermoPower.Water.f_dittus_boelter(w[1],Dhyd,A,mu_ls,k_ls,cp_ls));

    gamma_VP[j] = homotopy(ThermoPower.Water.f_dittus_boelter(
      w[j],
      Dhyd,
      A,
      mu[j],
      k[j],
      cp[j]),
      ThermoPower.Water.f_dittus_boelter(w[1],Dhyd,A,mu_vs,k_vs,cp_vs));

    // Two phase flow
     Qwf[j] = (Twf[j] - T[j])*kc*omega*l*gamma[j]*Nt;

    gamma_TP[j] =homotopy(ThermoPower.Water.f_chen(
      w[j],
      Dhyd,
      A,
      mu_ls,
      k_ls,
      cp_ls,
      rhol,
      sigma,
      rhov,
      mu_vs,
      Twf[j] - Medium.saturationTemperature(p),
      Medium.saturationPressure(max(Twf[j],Ts)) - p,
      hv - hl,
      ThermoPower.Functions.smoothSat(x[j],0.02,0.98,0.02,0.02)),
      10*ThermoPower.Water.f_dittus_boelter(w[1],Dhyd,A,mu_vs,k_vs,cp_vs));

    gamma_trans[j] = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.smoothTan(
      gamma_TP[j],
      gamma_LP[j],
      x[j] - 0.02,
      0.02);
    gamma[j] = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.smoothTan(
      gamma_VP[j],
      gamma_trans[j],
      x[j] - 0.95,
      0.05);

  end for;

  Twf[1] = Tw[1];
  for j in 1:Nw loop
     Tw[j] =if useAverageTemperature then (Twf[j]+Twf[j+1])/2 else Twf[j+1];
     Tvol[j]      = if useAverageTemperature then (T[j] + T[j + 1])/2       else T[j + 1];
     gamma_vol[j] = if useAverageTemperature then (gamma[j] + gamma[j+1])/2 else gamma[j+1];
     Qw[j] = (Tw[j] - Tvol[j])*kc*omega*l*gamma_vol[j]*Nt;
  end for;

initial equation

  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}}),
            graphics),
    Icon(graphics={Text(extent={{-100,-52},{100,-80}}, textString="%name")}));
end Chen;
