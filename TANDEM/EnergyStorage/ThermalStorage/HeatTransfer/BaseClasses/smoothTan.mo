within TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses;
function smoothTan "Arctan-smoothed regularization function"
  input Real upperLimit;
  input Real lowerLimit;
  input Real x "Input argument";
  input Real sigma "Transition zone";
  output Real y "Output";
protected
  Real x_norm;
  Real y_norm;
  constant Real pi=Modelica.Constants.pi "3.14159...";

algorithm
  x_norm := x/sigma*pi/2;
  if x_norm <= -pi/2 + Modelica.Constants.eps then
    y_norm := 0;
  elseif x_norm >= pi/2 - Modelica.Constants.eps then
    y_norm := 1;
  else
    y_norm := (tanh(tan(x_norm)) + 1)/2;
  end if;
  y := upperLimit*y_norm + (1 - y_norm)*lowerLimit;

end smoothTan;
