within TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses;
function f_nusselt_shell
  //Constants
  input Modelica.Units.SI.Length Lc "Characteristic length";
  input Integer Nt "Number of tubes";
  input Integer Np "Number of passes";
  // Variables
  input Modelica.Units.SI.ThermalConductivity k
    "Thermal conductivity";
  input Modelica.Units.SI.Density rhol "Saturated liquid density";
  input Modelica.Units.SI.DynamicViscosity mu_ls "Saturated liquid viscosity";
  input Modelica.Units.SI.ThermalConductivity k_ls
    "Liquid thermal conductivity";
  input Modelica.Units.SI.SpecificHeatCapacity cp_ls
    "Liquid constant pressure heat capacity";
  input Modelica.Units.SI.Density rhov "Vapour density";
  input Modelica.Units.SI.DynamicViscosity mu_vs "Vapour dynamic viscosity";
  input Modelica.Units.SI.SpecificEnthalpy hfg "Latent heat of vaporization";
  input Modelica.Units.SI.Temperature T "Fluid temperature";
  input Modelica.Units.SI.Temperature Tsat "Fluid saturation temperature";
  input Modelica.Units.SI.Temperature Tw "Wall temperature";
  output Modelica.Units.SI.CoefficientOfHeatTransfer hTP
    "Two-phase total heat transfer coefficient";

algorithm

  hTP := 0.728*((rhol^2*Modelica.Constants.g_n*hfg*k_ls^3)/(mu_ls*max(Tsat - Tw,0.001)*Lc))^(1/4)*1/(sqrt(Np*Nt))^(-1/6);

end f_nusselt_shell;
