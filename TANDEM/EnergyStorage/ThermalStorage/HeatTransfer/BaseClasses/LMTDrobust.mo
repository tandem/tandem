within TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses;
function LMTDrobust
  // from "Dynamic modeling of thermal systems using a semi-empirical approach and the ThermoCycle Modelica Library"
  input Modelica.Units.SI.Temperature Thi;
  input Modelica.Units.SI.Temperature Tho;
  input Modelica.Units.SI.Temperature Tci;
  input Modelica.Units.SI.Temperature Tco;

  input Real epsilon;
  input Real xsi;

  output Modelica.Units.SI.TemperatureDifference dTlm;

protected
  Modelica.Units.SI.TemperatureDifference dT1;
  Modelica.Units.SI.TemperatureDifference dT2;

algorithm
  dT1 := Thi-Tco;
  dT2 := Tho-Tci;

  if dT1>epsilon and dT2>epsilon and abs(dT1-dT2)>Modelica.Constants.eps then
    dTlm := (dT1-dT2)/(log(dT1)-log(dT2));
  elseif dT1>epsilon and dT2>epsilon and abs(dT1-dT2)>Modelica.Constants.eps then
    dTlm := (dT1+dT2)/2;
  elseif dT1>epsilon and dT2<epsilon then
    dTlm := (dT1-epsilon)/(log(dT1/epsilon)*(1-xsi*(dT2-epsilon)));
  elseif dT1<epsilon and dT2>epsilon then
    dTlm := (dT2-epsilon)/(log(dT2/epsilon)*(1-xsi*(dT1-epsilon)));
  elseif dT1<epsilon and dT2<epsilon then
    dTlm := epsilon/((1-xsi*(dT1-epsilon))*(1-xsi*(dT2-epsilon)));
  end if;

end LMTDrobust;
