within TANDEM.EnergyStorage.ThermalStorage.HeatTransfer;
model NusseltShell
  extends ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV(redeclare replaceable package
                          Medium =
        ThermoPower.Water.StandardWater                                                                                                    constrainedby
      Modelica.Media.Interfaces.PartialTwoPhaseMedium                                                                                                   "Medium model");

  parameter Modelica.Units.SI.Length do "Tube outer diameter";
   parameter Integer Ntubes=2 "Number of passes";
  parameter Integer Np=2 "Number of passes";

  Modelica.Units.SI.CoefficientOfHeatTransfer gamma[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_trans1[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_LP[Nf]
    "Heat transfer coefficients at the nodes";
   Modelica.Units.SI.CoefficientOfHeatTransfer gamma_VP[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_TP[Nf]
    "Heat transfer coefficients at the nodes";
  Modelica.Units.SI.CoefficientOfHeatTransfer gamma_vol[Nw]
    "Heat transfer coefficients at the volumes";
  Medium.Temperature Tvol[Nw]  "Fluid temperature in the volumes";
  Medium.DynamicViscosity mu[Nf] "Dynamic viscosity";
  Medium.ThermalConductivity k[Nf] "Thermal conductivity";
  Medium.SpecificHeatCapacity cp[Nf] "Heat capacity at constant pressure";

  Medium.DynamicViscosity mu_vw[Nf] "Dynamic viscosity";
  Medium.ThermalConductivity k_vw[Nf] "Thermal conductivity";
  Medium.SpecificHeatCapacity cp_vw[Nf] "Heat capacity at constant pressure";
  Real Pr_vw[Nf];

  Medium.ThermodynamicState bubble= Medium.setBubbleState(sat, 1) "Bubble point state";
  Medium.ThermodynamicState dew = Medium.setDewState(sat, 1) "Dew point state";
  Medium.SaturationProperties sat = Medium.setSat_p(p) "Properties of saturated fluid";

  Medium.DynamicViscosity mu_ls = Medium.dynamicViscosity(bubble) "Dynamic viscosity at bubble point";
  Medium.DynamicViscosity mu_vs = Medium.dynamicViscosity(dew)
                                                              "Dynamic viscosity at dew point";
  Medium.ThermalConductivity k_ls = Medium.thermalConductivity(bubble) "Thermal conductivity at bubble point";
  Medium.ThermalConductivity k_vs = Medium.thermalConductivity(dew) "Thermal conductivity at dew point";
  Medium.SpecificHeatCapacity cp_ls = Medium.heatCapacity_cp(bubble) "Specific heat capacity at bubble point";
  Medium.SpecificHeatCapacity cp_vs = Medium.heatCapacity_cp(dew) "Specific heat capacity at dew point";
  Medium.SurfaceTension sigma = Medium.surfaceTension(sat) "Surface tension";
  Medium.SpecificEnthalpy hl "Saturated liquid temperature";
  Medium.SpecificEnthalpy hv "Saturated vapour temperature";
  Medium.SpecificEnthalpy h[Nf] "Fluid enthalpy";
  Real x[Nf] "Steam quality";
  Modelica.Units.SI.Density rhol "Saturated liquid density";
  Modelica.Units.SI.Density rhov "Saturated vapour density";
  Medium.Temperature Ts "Fluid temperature in the volumes";
  Modelica.Units.SI.Pressure p;
  Modelica.Units.SI.Temperature Twf[Nf];

equation
  assert(Nw == Nf - 1, "Number of volumes Nw on wall side should be equal to number of volumes fluid side Nf - 1");

  Ts = sat.Tsat;
  rhol = Medium.bubbleDensity(sat);
  rhov = Medium.dewDensity(sat);
  hl = Medium.bubbleEnthalpy(sat);
  hv = Medium.dewEnthalpy(sat);

  p = Medium.pressure(fluidState[1]);

  for j in 1:Nf loop
    mu[j] = Medium.dynamicViscosity(fluidState[j]);
    k[j] = Medium.thermalConductivity(fluidState[j]);
    cp[j] = Medium.heatCapacity_cp(fluidState[j]);
    h[j] = Medium.specificEnthalpy(fluidState[j]);

    mu_vw[j] = Medium.dynamicViscosity(Medium.setState_phX(p, Medium.specificEnthalpy_pT(p,Twf[j])));
    k_vw[j] = Medium.thermalConductivity(Medium.setState_phX(p, Medium.specificEnthalpy_pT(p,Twf[j])));
    cp_vw[j] = Medium.heatCapacity_cp(Medium.setState_phX(p, Medium.specificEnthalpy_pT(p,Twf[j])));

    Pr_vw[j] = cp_vw[j]*mu_vw[j]/k_vw[j];

    x[j] = (h[j] - hl)/(hv - hl);

    // Single phase, subcooled or superheated liquid flow
    gamma_LP[j] = homotopy(ThermoPower.Water.f_dittus_boelter(
      w[j],
      Dhyd,
      A,
      mu[j],
      k[j],
      cp[j]), ThermoPower.Water.f_dittus_boelter(w[1],Dhyd,A,mu_ls,k_ls,cp_ls));

    gamma_VP[j] = homotopy(ThermoPower.Water.f_dittus_boelter(
      w[j],
      Dhyd,
      A,
      mu[j],
      k[j],
      cp[j]), ThermoPower.Water.f_dittus_boelter(w[1],Dhyd,A,mu_vs,k_vs,cp_vs));

    // Nusselt theory for two phase correlation
    gamma_TP[j] = homotopy(
      TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.f_nusselt_shell(
        do,
        Ntubes,
        Np,
        k[j],
        rhol,
        mu_ls,
        k_ls,
        cp_ls,
        rhov,
        mu_vs,
        hv - hl,
        T[j],
        Ts,
        Twf[j]),
      ThermoPower.Water.f_dittus_boelter(
        w[1],
        Dhyd,
        A,
        mu_ls,
        k_ls,
        cp_ls));

    gamma_trans1[j] = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.smoothTan(
      gamma_TP[j],
      gamma_LP[j],
      x[j] - 0.05,
      0.05);

    gamma[j] = TANDEM.EnergyStorage.ThermalStorage.HeatTransfer.BaseClasses.smoothTan(
      gamma_VP[j],
      gamma_trans1[j],
      x[j] - 0.95,
      0.05);

  end for;

  Twf[1] = Tw[1];
  for j in 1:Nw loop
     Tw[j] =if useAverageTemperature then (Twf[j]+Twf[j+1])/2 else Twf[j+1];
     Tvol[j]      = if useAverageTemperature then (T[j] + T[j + 1])/2       else T[j + 1];
     gamma_vol[j] = if useAverageTemperature then (gamma[j] + gamma[j+1])/2 else gamma[j+1];
     Qw[j] = (Tw[j] - Tvol[j])*kc*omega*l*gamma_vol[j]*Nt;
  end for;

initial equation

  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}}),
            graphics),
    Icon(graphics={Text(extent={{-100,-52},{100,-80}}, textString="%name")}));
end NusseltShell;
