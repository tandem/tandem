within TANDEM.EnergyStorage.ThermalStorage.Test;
model Test_SL_HX0D_OilOil_TRANS
  SensibleLoop_HX0D          SL(
    redeclare package OuterMedium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    hX_0D(
      mh_nom=657.534,
      ph_nom=100000,
      h_hi_nom=591099.08299,
      h_ho_nom=426849.0829),
    hX_0D1(
      mc_nom=616.4383561,
      pc_nom=100000,
      h_ci_nom=328299.082994,
      h_co_nom=503499.082994))
    annotation (Placement(transformation(extent={{-24,-40},{24,6}})));

  inner ThermoPower.System system(allowFlowReversal=true,
                                  initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{70,70},{90,90}})));
  Control.TEScontroller_Ttank_ctrl TESctrl(
    Tcold_set=463.15,
    Kp_char=733.5,
    Ki_char=0.004186)
    annotation (Placement(transformation(extent={{-10,18},{10,38}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    use_T=false,
    T=572.55,
    h=591099.08299,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{-80,22},{-60,42}})));
  ThermoPower.Water.SinkPressure sinkPressure(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 200000,
    use_T=false,
    h=426849,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=180,
        origin={-85,-35})));
  Modelica.Blocks.Sources.Ramp     ramp(
    height=-0.5,
    duration=100,
    offset=1,
    startTime=100)
    annotation (Placement(transformation(extent={{-84,-20},{-72,-8}})));
  ThermoPower.Water.ValveLin valveLin(redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
                                                      Kv=657.5342/1e5)
    annotation (Placement(transformation(extent={{-46,-44},{-64,-26}})));
  Modelica.Blocks.Sources.Ramp     ramp2(
    height=-0.5*657.5342,
    duration=100,
    offset=657.5342,
    startTime=100)
    annotation (Placement(transformation(extent={{-94,46},{-82,58}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow1(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    use_T=false,
    T=572.55,
    h=328575.915,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{76,-44},{58,-26}})));
  ThermoPower.Water.SinkPressure sinkPressure1(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    R=1,
    use_T=false,
    h=503775.915,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={68,32})));
  Modelica.Blocks.Sources.Ramp     ramp1(
    height=-0.5*616.438,
    duration=100,
    offset=616.438,
    startTime=3000)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=180,
        origin={88,-14})));
  ThermoPower.Water.SensT1 sensT1_1(redeclare package Medium =
        Media.Therminol66)
    annotation (Placement(transformation(extent={{-48,-42},{-28,-22}})));
  ThermoPower.Water.SensT1 sensT1_2(redeclare package Medium =
        Media.Therminol66)
    annotation (Placement(transformation(extent={{36,26},{56,46}})));
equation
  connect(TESctrl.actuatorBus, SL.actuatorBus) annotation (Line(
      points={{-6,18},{-6,12},{-14.4,12},{-14.4,6}},
      color={80,200,120},
      thickness=0.5));
  connect(TESctrl.sensorBus, SL.sensorBus) annotation (Line(
      points={{6,18},{6,12},{14.4,12},{14.4,6}},
      color={255,219,88},
      thickness=0.5));
  connect(sinkPressure.flange,valveLin. outlet)
    annotation (Line(points={{-76,-35},{-64,-35}}, color={0,0,255}));
  connect(sourceMassFlow.flange, SL.flangeA) annotation (Line(points={{-60,32},
          {-40,32},{-40,-4.22222},{-24,-4.22222}}, color={0,0,255}));
  connect(ramp.y,valveLin. cmd) annotation (Line(points={{-71.4,-14},{-55,-14},
          {-55,-27.8}}, color={0,0,127}));
  connect(ramp2.y,sourceMassFlow. in_w0) annotation (Line(points={{-81.4,52},{
          -74,52},{-74,37.6}}, color={0,0,127}));
  connect(valveLin.inlet, SL.flangeB) annotation (Line(points={{-46,-35},{-34,
          -35},{-34,-36},{-24,-36},{-24,-34.8889}}, color={0,0,255}));
  connect(SL.flangeB1,sinkPressure1. flange) annotation (Line(points={{24,
          -4.22222},{40,-4.22222},{40,32},{58,32}},        color={0,0,255}));
  connect(SL.flangeA1,sourceMassFlow1. flange) annotation (Line(points={{24,
          -34.8889},{54,-34.8889},{54,-35},{58,-35}},                   color={
          0,0,255}));
  connect(ramp1.y,sourceMassFlow1. in_w0) annotation (Line(points={{81.4,-14},{
          70.6,-14},{70.6,-29.96}}, color={0,0,127}));
  connect(valveLin.inlet, sensT1_1.flange) annotation (Line(points={{-46,-35},{
          -42,-35},{-42,-36},{-38,-36}}, color={0,0,255}));
  connect(SL.flangeB1, sensT1_2.flange) annotation (Line(points={{24,-4.22222},
          {40,-4.22222},{40,32},{46,32}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=6000, __Dymola_Algorithm="Dassl"));
end Test_SL_HX0D_OilOil_TRANS;
