within TANDEM.EnergyStorage.ThermalStorage.Test;
model Test_SL_HX0D_WaterOil_TRANS
  SensibleLoop_HX0D          SL
    annotation (Placement(transformation(extent={{-24,-40},{24,6}})));

  ThermoPower.Water.SourceMassFlow sourceMassFlow(
    p0(displayUnit="bar") = 4500000,
    use_T=false,
    T=572.55,
    h=2944104.065,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{-80,22},{-60,42}})));
  ThermoPower.Water.SinkPressure sinkPressure(
    p0(displayUnit="bar") = 674700,
    use_T=false,
    h=921356.136,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=180,
        origin={-85,-35})));
  inner ThermoPower.System system(allowFlowReversal=true,
                                  initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{70,70},{90,90}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow1(
    p0(displayUnit="bar") = 674700,
    use_T=false,
    T=572.55,
    h=487491.159,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{80,-44},{62,-26}})));
  ThermoPower.Water.SinkPressure sinkPressure1(
    p0(displayUnit="bar") = 714700,
    R=100,
    use_T=false,
    h=2964247.556,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={72,32})));
  Modelica.Blocks.Sources.Ramp     ramp(
    height=-0.5,
    duration=100,
    offset=1,
    startTime=100)
    annotation (Placement(transformation(extent={{-78,-20},{-66,-8}})));
  ThermoPower.Water.ValveLin valveLin(Kv=53.393/(45e5 - 6.747e5))
    annotation (Placement(transformation(extent={{-50,-44},{-68,-26}})));
  Modelica.Blocks.Sources.Ramp     ramp1(
    duration=100,
    height=-0.5*43.9,
    offset=43.9,
    startTime=3000)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=180,
        origin={92,-14})));

  Modelica.Blocks.Sources.Ramp     ramp2(
    height=-0.5*53.393,
    duration=100,
    offset=53.393,
    startTime=100)
    annotation (Placement(transformation(extent={{-94,46},{-82,58}})));
  Control.TEScontroller_Ttank_ctrl TESctrl(
    Tcold_set=463.15,
    Kp_char=733.5,
    Ki_char=0.004186)
    annotation (Placement(transformation(extent={{-10,18},{10,38}})));
  ThermoPower.Water.SensT1 sensT1_1
    annotation (Placement(transformation(extent={{-48,-42},{-28,-22}})));
  ThermoPower.Water.SensT1 sensT1_2
    annotation (Placement(transformation(extent={{38,26},{58,46}})));
equation
  connect(SL.flangeB1, sinkPressure1.flange) annotation (Line(points={{24,
          -4.22222},{40,-4.22222},{40,32},{62,32}},        color={0,0,255}));
  connect(sinkPressure.flange, valveLin.outlet)
    annotation (Line(points={{-76,-35},{-68,-35}}, color={0,0,255}));
  connect(valveLin.inlet, SL.flangeB) annotation (Line(points={{-50,-35},{-30,
          -35},{-30,-34.8889},{-24,-34.8889}},
                                         color={0,0,255}));
  connect(SL.flangeA1, sourceMassFlow1.flange) annotation (Line(points={{24,
          -34.8889},{36,-34.8889},{36,-35},{62,-35}},                   color={
          0,0,255}));
  connect(sourceMassFlow.flange, SL.flangeA) annotation (Line(points={{-60,32},
          {-40,32},{-40,-4.22222},{-24,-4.22222}}, color={0,0,255}));
  connect(ramp.y, valveLin.cmd) annotation (Line(points={{-65.4,-14},{-59,-14},
          {-59,-27.8}}, color={0,0,127}));
  connect(ramp2.y, sourceMassFlow.in_w0) annotation (Line(points={{-81.4,52},{
          -74,52},{-74,37.6}}, color={0,0,127}));
  connect(ramp1.y, sourceMassFlow1.in_w0) annotation (Line(points={{85.4,-14},{
          74.6,-14},{74.6,-29.96}}, color={0,0,127}));
  connect(TESctrl.actuatorBus, SL.actuatorBus) annotation (Line(
      points={{-6,18},{-6,12},{-14.4,12},{-14.4,6}},
      color={80,200,120},
      thickness=0.5));
  connect(TESctrl.sensorBus, SL.sensorBus) annotation (Line(
      points={{6,18},{6,12},{14.4,12},{14.4,6}},
      color={255,219,88},
      thickness=0.5));
  connect(valveLin.inlet, sensT1_1.flange) annotation (Line(points={{-50,-35},{
          -44,-35},{-44,-36},{-38,-36}}, color={0,0,255}));
  connect(SL.flangeB1, sensT1_2.flange) annotation (Line(points={{24,-4.22222},
          {40,-4.22222},{40,32},{48,32}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=6000, __Dymola_Algorithm="Dassl"));
end Test_SL_HX0D_WaterOil_TRANS;
