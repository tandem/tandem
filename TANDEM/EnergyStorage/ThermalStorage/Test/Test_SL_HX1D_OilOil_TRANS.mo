within TANDEM.EnergyStorage.ThermalStorage.Test;
model Test_SL_HX1D_OilOil_TRANS
  SensibleLoop_HX1D_OilOil   SL(redeclare package OuterMedium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66)
    annotation (Placement(transformation(extent={{-24,-40},{24,6}})));

  inner ThermoPower.System system(allowFlowReversal=true,
                                  initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{70,70},{90,90}})));
  Control.TEScontroller_Ttank_ctrl TESctrl(
    Tcold_set=463.15,
    Kp_char=733.5,
    Ki_char=0.004186)
    annotation (Placement(transformation(extent={{-10,18},{10,38}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    use_T=false,
    T=572.55,
    h=591099.08299,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{-86,22},{-66,42}})));
  ThermoPower.Water.SinkPressure sinkPressure(
    redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
    p0(displayUnit="bar") = 200000,
    use_T=false,
    h=426849,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=180,
        origin={-91,-33})));
  Modelica.Blocks.Sources.Ramp     ramp(
    height=-0.5,
    duration=100,
    offset=1,
    startTime=100)
    annotation (Placement(transformation(extent={{-74,-20},{-62,-8}})));
  ThermoPower.Water.ValveLin valveLin(redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66,
                                                      Kv=657.5342/1e5)
    annotation (Placement(transformation(extent={{-50,-42},{-68,-24}})));
  Modelica.Blocks.Sources.Ramp     ramp2(
    height=-0.5*657.5342,
    duration=100,
    offset=657.5342,
    startTime=100)
    annotation (Placement(transformation(extent={{-100,48},{-88,60}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow1(
    redeclare package Medium = Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    use_T=false,
    T=572.55,
    h=328575.915,
    use_in_w0=true)
    annotation (Placement(transformation(extent={{74,-52},{56,-34}})));
  ThermoPower.Water.SinkPressure sinkPressure1(
    redeclare package Medium = Media.Therminol66,
    p0(displayUnit="bar") = 100000,
    R=1,
    use_T=false,
    h=503775.915,
    use_in_T=false)
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=0,
        origin={78,24})));
  Modelica.Blocks.Sources.Ramp     ramp1(
    height=-0.5*616.438,
    duration=100,
    offset=616.438,
    startTime=3000)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=180,
        origin={86,-20})));
  ThermoPower.Water.SensT1 sensT1_1(redeclare package Medium =
        TANDEM.EnergyStorage.ThermalStorage.Media.Therminol66)
    annotation (Placement(transformation(extent={{-46,-40},{-26,-20}})));
  ThermoPower.Water.SensT1 sensT1_2(redeclare package Medium =
        Media.Therminol66)
    annotation (Placement(transformation(extent={{36,18},{56,38}})));
equation
  connect(TESctrl.actuatorBus, SL.actuatorBus) annotation (Line(
      points={{-6,18},{-6,12},{-14.4,12},{-14.4,6}},
      color={80,200,120},
      thickness=0.5));
  connect(TESctrl.sensorBus, SL.sensorBus) annotation (Line(
      points={{6,18},{6,12},{14.4,12},{14.4,6}},
      color={255,219,88},
      thickness=0.5));
  connect(sinkPressure.flange,valveLin. outlet)
    annotation (Line(points={{-82,-33},{-68,-33}}, color={0,0,255}));
  connect(sourceMassFlow.flange, SL.flangeA) annotation (Line(points={{-66,32},
          {-34,32},{-34,-4.22222},{-24,-4.22222}}, color={0,0,255}));
  connect(ramp.y,valveLin. cmd) annotation (Line(points={{-61.4,-14},{-59,-14},
          {-59,-25.8}}, color={0,0,127}));
  connect(ramp2.y,sourceMassFlow. in_w0) annotation (Line(points={{-87.4,54},{
          -80,54},{-80,37.6}}, color={0,0,127}));
  connect(valveLin.inlet, SL.flangeB) annotation (Line(points={{-50,-33},{-50,
          -34.8889},{-24,-34.8889}}, color={0,0,255}));
  connect(SL.flangeB1,sinkPressure1. flange) annotation (Line(points={{24,
          -4.22222},{46,-4.22222},{46,24},{68,24}},        color={0,0,255}));
  connect(SL.flangeA1,sourceMassFlow1. flange) annotation (Line(points={{24,
          -34.8889},{50,-34.8889},{50,-43},{56,-43}},                   color={
          0,0,255}));
  connect(ramp1.y,sourceMassFlow1. in_w0) annotation (Line(points={{79.4,-20},{
          68.6,-20},{68.6,-37.96}}, color={0,0,127}));
  connect(valveLin.inlet, sensT1_1.flange)
    annotation (Line(points={{-50,-33},{-50,-34},{-36,-34}}, color={0,0,255}));
  connect(SL.flangeB1, sensT1_2.flange) annotation (Line(points={{24,-4.22222},
          {46,-4.22222},{46,24}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=6000, __Dymola_Algorithm="Dassl"));
end Test_SL_HX1D_OilOil_TRANS;
