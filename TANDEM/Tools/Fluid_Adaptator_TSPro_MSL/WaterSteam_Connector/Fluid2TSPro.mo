within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.WaterSteam_Connector;
model Fluid2TSPro "A Modelica.Fluid to TSPro connector"

// IDNES //

  replaceable package ModelicaMedium =
      Modelica.Media.Water.WaterIF97_ph                                  "Medium model of the MSL part"
                                   annotation (choicesAllMatching=true, Dialog(group="Fundamental Definitions"));

//  parameter TILMedia.VLEFluidTypes.BaseVLEFluid ClaRaMedium = simCenter.fluid1 "Medium in the component"
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
        ModelicaMedium)
    annotation (Placement(transformation(extent={{-108,-12},{-88,8}}),
        iconTransformation(extent={{-108,-10},{-88,10}})));
  // ClaRa.Basics.Interfaces.FluidPortOut steam_a(Medium=ClaRaMedium)                 annotation (Placement(
  //       transformation(extent={{90,-9.9},{109,9.9}}), iconTransformation(extent=
  //         {{90,-10},{109,9.95}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutlet steam_outlet annotation (Placement(
         transformation(extent={{90,-9.9},{109,9.9}}), iconTransformation(extent=
          {{90,-10},{109,9.95}})));

equation
 // port_a.m_flow+steam_a.m_flow = 0;
  //port_a.h_outflow = inStream(steam_a.h_outflow);
 // inStream(port_a.h_outflow) = steam_a.h_outflow;

 // port_a.p = steam_a.p;
 // inStream(port_a.Xi_outflow) =steam_a.xi_outflow;
 // port_a.Xi_outflow = inStream(steam_a.xi_outflow);

 // port_a.C_outflow=zeros(ModelicaMedium.nC);

port_a.p = steam_outlet.P;
port_a.h_outflow = steam_outlet.h_vol;
port_a.m_flow = steam_outlet.Q;
//steam_outlet.h =steam_outlet.h_vol;
0 = if (port_a.m_flow > 0) then inStream(port_a.h_outflow) - steam_outlet.h else steam_outlet.h - steam_outlet.h_vol;

port_a.C_outflow=zeros(ModelicaMedium.nC);
port_a.Xi_outflow=zeros(ModelicaMedium.nC);

    annotation(choices(choice=simCenter.fluid1 "First fluid defined in global simCenter",
                       choice=simCenter.fluid2 "Second fluid defined in global simCenter",
                       choice=simCenter.fluid3 "Third fluid defined in global simCenter"),
                                                          Dialog(group="Fundamental Definitions"),
              defaultComponentName =     "fluid2TSPro",
          Icon(graphics={Polygon(
          points={{-96,10},{2,10},{36,-10},{-98,-10},{-96,10}},
          lineColor={0,0,0},
          fillColor={37,219,255},
          fillPattern=FillPattern.Solid), Polygon(
          points={{100,10},{100,10},{-20,10},{-20,10},{-12,0},{0,0},{
              12,0},{20,-10},{20,-10},{100,-10},{100,-10},{100,10}},
          smooth=Smooth.Bezier,
          fillColor={255,83,129},
          fillPattern=FillPattern.Solid,
          lineColor={0,131,169})}));
end Fluid2TSPro;
