within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.WaterSteam_Connector;
package Demo
  model Test1
    Fluid2TSPro fluid2TSPro(port_a(h_outflow(start=164025.0), m_flow(start=
              1409.1646104961167)))
      annotation (Placement(transformation(extent={{-28,-2},{-8,18}})));
    Modelica.Fluid.Sources.Boundary_ph boundary(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      p=754700,
      h=164025,
      nPorts=1)
      annotation (Placement(transformation(extent={{-76,-2},{-56,18}})));
    ThermoSysPro.WaterSteam.PressureLosses.PipePressureLoss
      pipePressureLoss(K(fixed=false) = 20.0, Q(start=182, fixed=true))
      annotation (Placement(transformation(extent={{10,-2},{30,18}})));
    ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP(P0=714700)
      annotation (Placement(transformation(extent={{44,-2},{64,18}})));
  equation
    connect(boundary.ports[1], fluid2TSPro.port_a)
      annotation (Line(points={{-56,8},{-27.8,8}}, color={0,127,255}));
    connect(fluid2TSPro.steam_outlet, pipePressureLoss.C1)
      annotation (Line(points={{-8.05,7.9975},{10,8}}, color={0,0,255}));
    connect(pipePressureLoss.C2, sinkP.C)
      annotation (Line(points={{30,8},{44,8}}, color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end Test1;

  model Test2
    Modelica.Fluid.Sources.Boundary_ph boundary(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      p=714700,
      nPorts=1)
      annotation (Placement(transformation(extent={{-76,-2},{-56,18}})));
    ThermoSysPro.WaterSteam.PressureLosses.PipePressureLoss
      pipePressureLoss(K(fixed=false) = 20.0, Q(start=182, fixed=true))
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={20,8})));
    TSPro2Fluid fluid2TSPro annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-22,8})));
    ThermoSysPro.WaterSteam.BoundaryConditions.SourceP sourceP(
      P0=754700,
      h0=164025,
      option_temperature=2) annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={56,8})));
  equation
    connect(pipePressureLoss.C2, fluid2TSPro.steam_inlet)
      annotation (Line(points={{10,8},{-12,8}}, color={0,0,255}));
    connect(fluid2TSPro.port_b, boundary.ports[1])
      annotation (Line(points={{-32,8},{-56,8}}, color={0,127,255}));
    connect(sourceP.C, pipePressureLoss.C1)
      annotation (Line(points={{46,8},{30,8}}, color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end Test2;
end Demo;
