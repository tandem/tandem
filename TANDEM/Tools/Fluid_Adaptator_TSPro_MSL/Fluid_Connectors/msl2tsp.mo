within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.Fluid_Connectors;
model msl2tsp "Inflow to ThermoSysPro"

  // ThermoSysPro Fluid definitions
  extends
    ThermoSysPro.Fluid.Interfaces.PropertyInterfaces.FluidTypeParameterInterface;
  import ThermoSysPro.Fluid.Interfaces.PropertyInterfaces.FluidType;
  import ThermoSysPro.Fluid.Interfaces.PropertyInterfaces.IF97Region;
  parameter IF97Region region=IF97Region.All_regions "IF97 region (active for IF97 water/steam only)" annotation(Evaluate=true, Dialog(enable=(ftype==FluidType.WaterSteam), tab="Fluid", group="Fluid properties"));
protected
  parameter Boolean flue_gases=(ftype == FluidType.FlueGases) "Flue gases"; //To be used for X adaptors
  parameter Integer mode=Integer(region) - 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";

public
  ThermoSysPro.Fluid.Interfaces.Connectors.FluidOutlet Ctsp annotation (Placement(
        transformation(extent={{86,-14},{114,14}},   rotation=0),
        iconTransformation(extent={{86,-14},{114,14}})));

  // ModelicaStandardLibrary Fluid definitions
  replaceable package mslMedium = Modelica.Media.Water.WaterIF97_ph "Medium of the MSL model";  //if ftype==FluidType.WaterSteam else Modelica.Media.Water.WaterIF97_ph  ;

  Modelica.Fluid.Interfaces.FluidPort Cmsl(redeclare package Medium = mslMedium)
    annotation (Placement(transformation(extent={{-114,14},{-86,-14}}),
        iconTransformation(extent={{-114,14},{-86,-14}})));

equation
  Cmsl.p = Ctsp.P;
  Cmsl.m_flow = Ctsp.Q;

  // ThermoSysPro flow reversal
  Ctsp.h = if (Ctsp.Q > 0) then Ctsp.h_vol_1 else Ctsp.h_vol_2;

  inStream(Cmsl.h_outflow) = Ctsp.h_vol_1;
  Cmsl.h_outflow = Ctsp.h_vol_2;

  Ctsp.ftype = ftype;

  // Should be adapted depending on the fluid type (FlueGases, hydrongen...)
  Ctsp.Xco2 = 0;
  Ctsp.Xh2o = 0;
  Ctsp.Xo2 = 0;
  Ctsp.Xso2 = 0;
  Cmsl.C_outflow=zeros(mslMedium.nC);
  Cmsl.Xi_outflow=zeros(mslMedium.nC);

  Ctsp.diff_on_1 = false; //No thermal diffusion in MSL
  Ctsp.diff_res_1 = 1.e-3; //Arbitrary value

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Polygon(
          points={{-80,80},{-80,-80},{0,0},{-80,80}},
          lineColor={28,108,200},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid), Polygon(
          points={{0,80},{0,-80},{80,0},{0,80}},
          lineColor={28,108,200},
          fillColor=DynamicSelect({255,255,255}, if fluid1 == 1 then {0,0,255}
               else if fluid1 == 2 then {238,46,47} else if fluid1 == 3 then {
              0,140,72} else if fluid1 == 4 then {217,67,180} else if fluid1
               == 5 then {162,29,33} else if fluid1 == 6 then {244,125,35}
               else if fluid1 == 7 then {102,44,145} else {213,255,170}),
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end msl2tsp;
