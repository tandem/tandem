within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.Fluid_Connectors;
model tsp2msl "Outflow from ThermoSysPro"

  // ThermoSysPro Fluid definitions
  ThermoSysPro.Fluid.Interfaces.Connectors.FluidInlet Ctsp annotation (Placement(
        transformation(extent={{86,-14},{114,14}},   rotation=0),
        iconTransformation(extent={{86,-14},{114,14}})));

  // ModelicaStandardLibrary Fluid definitions
  replaceable package mslMedium = Modelica.Media.Water.WaterIF97_ph "Medium of the MSL model";  //if ftype==FluidType.WaterSteam else Modelica.Media.Water.WaterIF97_ph  ;

  Modelica.Fluid.Interfaces.FluidPort Cmsl(redeclare package Medium = mslMedium)
    annotation (Placement(transformation(extent={{-114,14},{-86,-14}}),
        iconTransformation(extent={{-114,14},{-86,-14}})));

equation
  Cmsl.p = Ctsp.P;
  Cmsl.m_flow = -Ctsp.Q;

  inStream(Cmsl.h_outflow) = Ctsp.h_vol_2;
  Cmsl.h_outflow = Ctsp.h_vol_1;

  Ctsp.diff_on_2 = false; //No thermal diffusion in MSL
  Ctsp.diff_res_2 = 1.e-3; //Arbitrary value

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Polygon(
          points={{0,80},{0,-80},{-80,0},{0,80}},
          lineColor={28,108,200},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid), Polygon(
          points={{80,80},{80,-80},{0,0},{80,80}},
          lineColor={28,108,200},
          fillColor=DynamicSelect({255,255,255}, if fluid1 == 1 then {0,0,255}
               else if fluid1 == 2 then {238,46,47} else if fluid1 == 3 then {
              0,140,72} else if fluid1 == 4 then {217,67,180} else if fluid1
               == 5 then {162,29,33} else if fluid1 == 6 then {244,125,35}
               else if fluid1 == 7 then {102,44,145} else {213,255,170}),
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end tsp2msl;
