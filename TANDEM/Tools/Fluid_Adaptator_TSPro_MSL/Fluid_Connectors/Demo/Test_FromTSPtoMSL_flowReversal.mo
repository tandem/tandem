within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.Fluid_Connectors.Demo;
model Test_FromTSPtoMSL_flowReversal "Test of the fluid adaptor"
  Modelica.Fluid.Sources.Boundary_ph boundary(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_pT,
    p=754700,
    h=164025,
    nPorts=1)
    annotation (Placement(transformation(extent={{-98,-10},{-78,10}})));
  ThermoSysPro.Fluid.PressureLosses.LumpedStraightPipe lumpedStraightPipe
    annotation (Placement(transformation(extent={{20,-10},{0,10}})));
  ThermoSysPro.Fluid.Volumes.VolumeA volumeA
    annotation (Placement(transformation(extent={{50,-10},{30,10}})));
  ThermoSysPro.Fluid.BoundaryConditions.SourceP
                                              sourceP
    annotation (Placement(transformation(extent={{80,-10},{60,10}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Trapezoide
                                                              trapezoide(
    amplitude=9.e5,
    rising=10,
    largeur=10,
    falling=10,
    periode=100,
    n=1,
    offset=1.e5,
    startTime=10)
    annotation (Placement(transformation(extent={{50,16},{70,36}})));
  Modelica.Fluid.Sensors.Temperature temperature(redeclare package Medium =
        Modelica.Media.Water.WaterIF97_ph)
    annotation (Placement(transformation(extent={{-74,18},{-54,38}})));
  tsp2msl tsp2msl1
    annotation (Placement(transformation(extent={{-48,-16},{-14,16}})));
equation
  connect(trapezoide.y, sourceP.IPressure) annotation (Line(points={{71,26},{
          86,26},{86,0},{75,0}}, color={0,0,255}));
  connect(sourceP.C, volumeA.Ce1)
    annotation (Line(points={{60,0},{50,0}}, color={0,0,0}));
  connect(lumpedStraightPipe.C1, volumeA.Cs1)
    annotation (Line(points={{20,0},{30,0}}, color={0,0,0}));
  connect(tsp2msl1.Ctsp, lumpedStraightPipe.C2)
    annotation (Line(points={{-14,0},{0,0}}, color={0,0,0}));
  connect(tsp2msl1.Cmsl, temperature.port)
    annotation (Line(points={{-48,0},{-64,0},{-64,18}}, color={0,0,0}));
  connect(tsp2msl1.Cmsl, boundary.ports[1])
    annotation (Line(points={{-48,0},{-78,0}}, color={0,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=100, __Dymola_Algorithm="Dassl"));
end Test_FromTSPtoMSL_flowReversal;
