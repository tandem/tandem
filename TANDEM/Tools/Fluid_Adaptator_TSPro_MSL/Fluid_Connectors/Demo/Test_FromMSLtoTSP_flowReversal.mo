within TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.Fluid_Connectors.Demo;
model Test_FromMSLtoTSP_flowReversal "Test of the fluid adaptor"
  msl2tsp msl2tsp1
    annotation (Placement(transformation(extent={{-50,-20},{-12,20}})));
  Modelica.Fluid.Sources.Boundary_ph boundary(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_pT,
    p=754700,
    h=164025,
    nPorts=1)
    annotation (Placement(transformation(extent={{-98,-10},{-78,10}})));
  ThermoSysPro.Fluid.PressureLosses.LumpedStraightPipe lumpedStraightPipe
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  ThermoSysPro.Fluid.Volumes.VolumeA volumeA
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));
  ThermoSysPro.Fluid.BoundaryConditions.SinkP sinkP
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Fluid.Sensors.Temperature temperature(redeclare package Medium =
        Modelica.Media.Water.WaterIF97_ph)
    annotation (Placement(transformation(extent={{-74,18},{-54,38}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Trapezoide
                                                              trapezoide(
    amplitude=9.e5,
    rising=10,
    largeur=10,
    falling=10,
    periode=100,
    n=1,
    offset=1.e5,
    startTime=10)
    annotation (Placement(transformation(extent={{54,14},{74,34}})));
equation
  connect(boundary.ports[1], msl2tsp1.Cmsl)
    annotation (Line(points={{-78,0},{-64,0},{-64,0},{-50,0}},
                                                  color={0,127,255}));
  connect(msl2tsp1.Ctsp, lumpedStraightPipe.C1)
    annotation (Line(points={{-12,0},{0,0}},color={0,0,0}));
  connect(lumpedStraightPipe.C2, volumeA.Ce1)
    annotation (Line(points={{20,0},{30,0}}, color={0,0,0}));
  connect(volumeA.Cs1, sinkP.C)
    annotation (Line(points={{50,0},{60,0}}, color={0,0,0}));
  connect(temperature.port, msl2tsp1.Cmsl) annotation (Line(points={{-64,18},{
          -64,0},{-50,0}},    color={0,127,255}));
  connect(trapezoide.y, sinkP.IPressure) annotation (Line(points={{75,24},{88,
          24},{88,0},{75,0}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=100, __Dymola_Algorithm="Dassl"));
end Test_FromMSLtoTSP_flowReversal;
