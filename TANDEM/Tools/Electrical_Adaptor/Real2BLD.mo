within TANDEM.Tools.Electrical_Adaptor;
model Real2BLD
  Modelica.Blocks.Interfaces.RealInput P
    annotation (Placement(transformation(extent={{-128,-20},{-88,20}})));
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n term_n annotation (
      Placement(transformation(extent={{94,-10},{114,10}}), iconTransformation(
          extent={{94,-10},{114,10}})));

equation
  term_n.v[1] * term_n.i[1] + term_n.v[2] * term_n.i[2] = P;
  term_n.v[2] * term_n.i[1] - term_n.v[1] * term_n.i[2] = 0;


  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-100,20},{100,-20}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.CrossDiag)}), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end Real2BLD;
