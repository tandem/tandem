within TANDEM.Tools.Electrical_Adaptor;
model Real2TP
  Modelica.Blocks.Interfaces.RealInput P
    annotation (Placement(transformation(extent={{-128,-20},{-88,20}})));

  ThermoPower.Electrical.PowerConnection
                                       powerConnection
    annotation (Placement(transformation(extent={{88,-10},{110,12}}),
        iconTransformation(extent={{88,-12},{110,10}})));
equation

  P = powerConnection.P;


  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-100,20},{100,-20}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.CrossDiag)}), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end Real2TP;
