within TANDEM.Tools.Thermal_Adaptor;
model TSPro2TP
  parameter Integer N;
  ThermoSysPro.Thermal.Connectors.ThermalPort thermalPort[N]
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  ThermoPower.Thermal.DHTVolumes dHTVolumes(N=N)
    annotation (Placement(transformation(extent={{90,-8},{110,12}})));


equation
  thermalPort.T = dHTVolumes.T;
  thermalPort.W = -dHTVolumes.Q;





  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-100,20},{100,-20}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.CrossDiag)}), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end TSPro2TP;
