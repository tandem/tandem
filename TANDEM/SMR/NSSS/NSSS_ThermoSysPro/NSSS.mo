within TANDEM.SMR.NSSS.NSSS_ThermoSysPro;
model NSSS
parameter Integer TubesNumber = 20064;
parameter ThermoSysPro.Units.SI.MassFlowRate NominalFlow = 3700 "Primary Mass Flow Rate";
parameter ThermoSysPro.Units.SI.Power MaximumPower = 540000000 "Power of the core at 100%";
parameter ThermoSysPro.Units.SI.Pressure NominalPressure = 15000000 "Pressure inside the core";
parameter ThermoSysPro.Units.SI.Temperature Tin = 573.15 "Inlet temperature of the coolant";
parameter ThermoSysPro.Units.SI.Temperature Tout = 597.65 "Outlet temperature of the coolant";
ThermoSysPro.Units.SI.Power Wsg;

  ThermoSysPro.WaterSteam.HeatExchangers.DynamicOnePhaseFlowPipe
    PrimaryCoolantFlow_Core(
    L=2,
    D=1.2598e-2,
    rugosrel=1e-6,
    ntubes=TubesNumber,
    z1=0.3,
    z2=2.3,
    Ns=6,
    simplified_dynamic_energy_balance=true,
    mode=1,
    inertia=false,
    advection=false,
    dynamic_mass_balance=false,
    steady_state=true,
    option_temperature=1,
    continuous_flow_reversal=false)
                     annotation (Placement(transformation(
        extent={{-19,-20},{19,20}},
        rotation=90,
        origin={0,-9})));
  ThermoSysPro.WaterSteam.Volumes.VolumeD LowerPlenum(
    V=3.488,
    P0=NominalPressure,
    dynamic_mass_balance=false,
    steady_state=true,
    fluid=1,
    mode=1,
    P(start=NominalPressure),
    rho(displayUnit="kg/m3", start=698.592)) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,-100})));
  ThermoSysPro.WaterSteam.Volumes.VolumeD volume_UpperSG(
    dynamic_mass_balance=false,
    steady_state=true,
    P(start=NominalPressure))
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={240,80})));
  ThermoSysPro.WaterSteam.Volumes.VolumeC PumpPlenum(
    V=1,
    P0=14600000,
    dynamic_mass_balance=false,
    steady_state=true,
    fluid=1,
    mode=1,
    P(start=14600000),
    rho(displayUnit="kg/m3")) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={240,-100})));

  ThermoSysPro.WaterSteam.HeatExchangers.DynamicOnePhaseFlowPipe SG_Primary(
    L=2,
    D=2.6667e-3,
    rugosrel=1e-6,
    ntubes=6*14409,
    z1=8.2,
    z2=6.2,
    hcCorr(fixed=false),
    Ns=6,
    inertia=false,
    advection=false,
    dynamic_mass_balance=false,
    simplified_dynamic_energy_balance=true,
    steady_state=true,
    mode=1)            annotation (Placement(transformation(
        extent={{-13,-14},{13,14}},
        rotation=270,
        origin={302,17})));

  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Bypass_SG(
    L=2,
    D=2.6667e-3,
    ntubes=2*14409,
    lambda(fixed=false) = 0.03,
    rugosrel=1e-6,
    z1=8.2,
    z2=6.2,
    lambda_fixed=true,
    mode=1,
    Q(start=10, fixed=true))  annotation (Placement(transformation(
        extent={{-17,-16},{17,16}},
        rotation=270,
        origin={180,1})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
    singularPressureLoss_CoreToSG(
    Q(start=NominalFlow),
    T(start=Tout),
    Pm(start=NominalPressure))
    annotation (Placement(transformation(extent={{170,130},{190,150}})));
  ThermoSysPro.WaterSteam.Volumes.VolumeC volume_Riser(
    V=4.39,
    P(start=NominalPressure),
    rho(displayUnit="kg/m3"))                annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,80})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Riser(
    L=5.9,
    D=4.1962e-2,
    ntubes=569.5211,
    rugosrel=1e-6,
    z1=2.3,
    z2=8.2,
    Q(start=NominalFlow, fixed=true))
                          annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,120})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Bypass_Core(
    L=2.3,
    D=1.2598e-2,
    ntubes=TubesNumber*(4.05/100),
    lambda(fixed=false),
    rugosrel=1e-6,
    z1=0,
    z2=2.3,
    lambda_fixed=true,
    Q(start=150,
      fixed=true))      annotation (Placement(transformation(
        extent={{-17,-16},{17,16}},
        rotation=90,
        origin={120,1})));
  ThermoSysPro.WaterSteam.Volumes.VolumeA UpperPlenum(V=18.8342,
    P0=NominalPressure,
    steady_state=true,
    fluid=1,
    mode=1,
    P(start=NominalPressure))
    annotation (Placement(transformation(extent={{60,130},{80,150}})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss singularPressureLoss(
    fluid=1,
    Q(start=3550),
    T(start=Tout),
    Pm(start=NominalPressure),
    h(start=1300000))   annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,30})));
  ThermoSysPro.WaterSteam.Machines.CentrifugalPump centrifugalPump(
    fluid=1,
    mode=1,
    mode_car=1,
    Qv_nom_p=4,
    hn_nom_p(fixed=false) = 50,
    Q(start=3700),
    Qv(start=4),
    hn(start=50),
    Pm(start=15000000))
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=180,
        origin={190,-150})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe pressurizer_pressureloss(
    L=2,
    D=0.05,
    ntubes=8,
    lambda(start=1e20),
    rugosrel=1e-6,
    z1=8.2,
    z2=10.2,
    Q(start=0)) annotation (Placement(transformation(extent={{34,150},{54,170}})));
  ThermoSysPro.NuclearCore.FuelThermalPower Fuel_Thermal_Power(
    Nrods=20064,
    Rp=4.05765e-3,
    Rclad=4.1402e-3,
    N=6,
    Length=2,
    steady_state=true,
    Tstart=1129.15)    annotation (Placement(transformation(extent={{-114,-20},{-90,4}})));
  ThermoSysPro.Thermal.HeatTransfer.HeatExchangerWallCounterFlow
    heatExchangerWallCounterFlow(
    L=2,
    D=0.0082804,
    e=6.0960e-4,
    Ns=6,
    rhow(displayUnit="kg/m3") = 6500,
    steady_state=true,
    ntubes=20064)
               annotation (Placement(transformation(
        extent={{-16,-16},{16,16}},
        rotation=90,
        origin={-54,-8})));
  ThermoSysPro.NuclearCore.NeutronKinetics Neutron_Kinetics(
    Tlife=10e-4,
    Kuo2=0.97,
    steady_state=true,
    Puo20=524e6,
    Pneut(start=524e6))
    annotation (Placement(transformation(extent={{-266,-6},{-228,32}})));
  ThermoSysPro.NuclearCore.ResidualPower Residual_Power(steady_state=true, Pneut(start=
          540e6)) annotation (Placement(transformation(extent={{-270,-72},{-234,-36}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante3(k=0)
    annotation (Placement(transformation(extent={{-400,90},{-380,110}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante2(k=0)
    annotation (Placement(transformation(extent={{-410,28},{-390,48}})));
  ThermoSysPro.NuclearCore.ReactivityFeedbacks Reactivity_Feedbacks(
    alfa_dop=-2,
    steady_state=true,
    PosR0=0,
    alfa_mod=-60,
    Tref_core=585.45,
    Tref_fuel=1129.15,
    Reac(start=0)) annotation (Placement(transformation(extent={{-268,62},{-220,110}})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe LowerCore_PressureLoss(
    L=0.3,
    D=1.2598e-2,
    ntubes=20064,
    rugosrel=1e-6,
    z1=0,
    z2=0.3,
    p_rho(displayUnit="kg/m3"),
    mode=1,
    Q(start=3550),
    T(start=573.15),
    h(start=100000)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-50})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource heatSource(T0={298.15},
      option_temperature=1) annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=90,
        origin={-69,177})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Downcomer(
    L=6.2,
    D=1.2,
    ntubes=5.083,
    rugosrel=1e-6,
    z1=6.2,
    z2=0,
    lambda_fixed=false,
    Q(start=3700))                  annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,-130})));
  ThermoSysPro.WaterSteam.Volumes.VolumeA volumeDowncomer(
    V=29.8954,
    P0=15000000,
    dynamic_mass_balance=false,
    steady_state=true,
    P(start=15000000)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={132,-150})));
  ThermoSysPro.Thermal.HeatTransfer.HeatExchangerWallCounterFlow
    heatExchangerWallCounterFlow1(
    L=2,
    D=2.6667e-3,
    e=1e-3,
    Ns=6,
    steady_state=true,
    ntubes=6*14409,
    Tp2(start={530.65,530.65,530.65,530.65,530.65,530.65}, displayUnit="degC"))
                    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={324,16})));
  ThermoSysPro.WaterSteam.Volumes.Pressurizer pressurizer_New(
    V=14.4867,
    Rp=1.825,
    Zm=0.8,
    P0=NominalPressure,
    steady_state=false,
    P(start=NominalPressure))
    annotation (Placement(transformation(extent={{-46,166},{-26,186}})));
  ThermoSysPro.WaterSteam.Sensors.SensorT          sensorT annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-8,-82})));
  ThermoSysPro.WaterSteam.Sensors.SensorT          sensorT1 annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-8,58})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add add(k1=0.5, k2=0.5) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-60,50})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal Tout_Output annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-72,130}),iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-72,130})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal Tin_output annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-86,-66}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-86,-66})));
  CONTROL.regulation_CRr regulation_CRr1
    annotation (Placement(transformation(extent={{-400,136},{-380,156}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource heatSource1(option_temperature=2)
                                                                 annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-70,166})));
  CONTROL.regulation_pressurizerMSL regulation_pressurizerMSL
    annotation (Placement(transformation(extent={{-160,182},{-136,204}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=15000000)
    annotation (Placement(transformation(extent={{-224,174},{-204,194}})));
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicTwoPhaseFlowPipe dynamicTwoPhaseFlowPipe(
    L=2,
    D=2.6667e-3,
    rugosrel=1e-6,
    ntubes=8*14409,
    z1=6.2,
    z2=8.2,
    Ns=6,
    inertia=false,
    advection=false,
    dynamic_mass_balance=false,
    simplified_dynamic_energy_balance=true,
    steady_state=true,
    option_temperature=1,
    continuous_flow_reversal=false)                                  annotation (Placement(
        transformation(
        extent={{-17,-16},{17,16}},
        rotation=90,
        origin={350,17})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP(
    P0=4500000,
    T0=573.55,
    option_temperature=1,
    mode=2) annotation (Placement(transformation(extent={{376,56},{408,86}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SourceP sourceP1(
    P0(fixed=false) = 4900000,
    T0=437.05,
    option_temperature=1,
    mode=1,
    Q(start=240, fixed=true)) annotation (Placement(transformation(
        extent={{-15,-16},{15,16}},
        rotation=180,
        origin={393,-36})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SourceQ sourceQ(h0(fixed=true)=
      1.2434e06)
    annotation (Placement(transformation(extent={{-104,194},{-84,214}})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss singularPressureLoss_press1(Pm(start=
          15000000), h(start=1.2434e06))
                        annotation (Placement(transformation(extent={{-78,194},
            {-58,214}})));
equation
     Wsg=sum(heatExchangerWallCounterFlow1.WT1.W);

  connect(LowerPlenum.Cs2, Bypass_Core.C1)
    annotation (Line(points={{69.8,-100},{120,-100},{120,-16}},
                                                              color={0,0,255}));
  connect(volume_Riser.Ce3, Bypass_Core.C2)
    annotation (Line(points={{70,80},{120,80},{120,18}}, color={0,0,255}));
  connect(Riser.C1, volume_Riser.Cs)
    annotation (Line(points={{-6.66134e-16,110},{-6.66134e-16,100},{60,100},{60,90}},
                                                              color={0,0,255}));
  connect(Riser.C2, UpperPlenum.Ce1)
    annotation (Line(points={{0,130},{0,140},{60,140}}, color={0,0,255}));
  connect(UpperPlenum.Cs1, singularPressureLoss_CoreToSG.C1) annotation (Line(points={{80,140},
          {170,140}},                            color={0,0,255}));
  connect(singularPressureLoss_CoreToSG.C2, volume_UpperSG.Ce)
    annotation (Line(points={{190,140},{240,140},{240,90}},
                                                          color={0,0,255}));
  connect(Bypass_SG.C1, volume_UpperSG.Cs2)
    annotation (Line(points={{180,18},{180,80},{230.2,80}}, color={0,0,255}));
  connect(volume_UpperSG.Cs1, SG_Primary.C1)
    annotation (Line(points={{250,80},{302,80},{302,30}}, color={0,0,255}));
  connect(Bypass_SG.C2, PumpPlenum.Ce3)
    annotation (Line(points={{180,-16},{180,-100},{230,-100}},
                                                             color={0,0,255}));
  connect(singularPressureLoss.C1, PrimaryCoolantFlow_Core.C2)
    annotation (Line(points={{-5.55112e-16,20},{1.11022e-15,10}}, color={0,0,255}));
  connect(centrifugalPump.C1, PumpPlenum.Cs)
    annotation (Line(points={{200,-150},{240,-150},{240,-110}},color={0,0,255}));
  connect(pressurizer_pressureloss.C2, UpperPlenum.Ce2)
    annotation (Line(points={{54,160},{70,160},{70,149.8}}, color={0,0,255}));
  connect(Neutron_Kinetics.EntreePres,Residual_Power. SortiePresTot) annotation (Line(
        points={{-267.9,5.4},{-346,5.4},{-346,-82},{-178,-82},{-178,-54},{-232.2,-54}},
        color={0,0,255}));
  connect(Neutron_Kinetics.SortiePneut,Residual_Power. EntreePneut) annotation (Line(
        points={{-226.1,22.5},{-158,22.5},{-158,-16},{-300,-16},{-300,-54},{-271.8,-54}},
        color={0,0,255}));
  connect(constante3.y,Reactivity_Feedbacks. EntreePosG) annotation (Line(points={{-379,
          100},{-282,100},{-282,100.4},{-270.4,100.4}}, color={0,0,255}));
  connect(constante2.y,Reactivity_Feedbacks. EntreeCbore) annotation (Line(points={{-389,38},
          {-376,38},{-376,71.6},{-270.4,71.6}},         color={0,0,255}, pattern=LinePattern.Dot));
  connect(constante2.y,Reactivity_Feedbacks. EntreeCxenon) annotation (Line(points={{-389,38},
          {-368,38},{-368,62},{-270.4,62}},         color={0,0,255}, pattern=LinePattern.Dot));
  connect(Reactivity_Feedbacks.SortieReac,Neutron_Kinetics. EntreeReac) annotation (
      Line(points={{-217.6,84.56},{-140,84.56},{-140,54},{-304,54},{-304,20.6},{-267.9,
          20.6}},
        color={0,0,255}));
  connect(Fuel_Thermal_Power.SortieT_fuel,Reactivity_Feedbacks. EntreeT_fuel)
    annotation (Line(points={{-102,5.2},{-102,136},{-290,136},{-290,90.8},{-270.4,90.8}},
                        color={0,0,255}));
  connect(Fuel_Thermal_Power.C_WTgaine,heatExchangerWallCounterFlow. WT2) annotation (
     Line(points={{-88.8,-7.88},{-57.2,-8}},                   color={0,0,0}));
  connect(heatExchangerWallCounterFlow.WT1, PrimaryCoolantFlow_Core.CTh) annotation (Line(
        points={{-50.8,-8},{-6,-8},{-6,-9}},                 color={0,0,0}));
  connect(PrimaryCoolantFlow_Core.C1, LowerCore_PressureLoss.C2)
    annotation (Line(points={{-1.11022e-15,-28},{5.55112e-16,-40}}, color={0,0,255}));
  connect(LowerPlenum.Ce, Downcomer.C2)
    annotation (Line(points={{60,-110},{60,-120}},         color={0,0,255}));
  connect(Downcomer.C1, volumeDowncomer.Cs1)
    annotation (Line(points={{60,-140},{60,-150},{122,-150}}, color={0,0,255}));
  connect(volumeDowncomer.Ce1, centrifugalPump.C2)
    annotation (Line(points={{142,-150},{180,-150}}, color={0,0,255}));
  connect(SG_Primary.CTh, heatExchangerWallCounterFlow1.WT2)
    annotation (Line(points={{306.2,17},{306.2,16},{322,16}}, color={0,0,0}));
  connect(heatSource.C[1], pressurizer_New.Ca)
    annotation (Line(points={{-62.14,177},{-62.14,176.2},{-45,176.2}}, color={0,0,0}));
  connect(sensorT1.C2, volume_Riser.Ce2)
    annotation (Line(points={{0,68.2},{0,80},{51,80}}, color={0,0,255}));
  connect(sensorT1.C1, singularPressureLoss.C2)
    annotation (Line(points={{-4.44089e-16,48},{5.55112e-16,40}}, color={0,0,255}));
  connect(LowerCore_PressureLoss.C1, sensorT.C2)
    annotation (Line(points={{-6.66134e-16,-60},{0,-60},{0,-71.8}}, color={0,0,255}));
  connect(sensorT.C1, LowerPlenum.Cs1) annotation (Line(points={{-8.88178e-16,-92},{0,-92},
          {0,-100},{50,-100}}, color={0,0,255}));
  connect(add.u2, sensorT1.Measure)
    annotation (Line(points={{-49,56},{-24,56},{-24,58},{-18,58}}, color={0,0,255}, pattern=LinePattern.Dot));
  connect(add.u1, sensorT.Measure)
    annotation (Line(points={{-49,44},{-28,44},{-28,-82},{-18,-82}}, color={0,0,255},  pattern=LinePattern.Dot));
  connect(add.y, Reactivity_Feedbacks.EntreeT_CoreAv) annotation (Line(points={{-71,50},{
          -202,50},{-202,52},{-332,52},{-332,81.2},{-270.4,81.2}},
                                                              color={0,0,255}, pattern=LinePattern.Dot));
  connect(sensorT1.Measure, Tout_Output) annotation (Line(points={{-18,58},{-24,58},{-24,
          56},{-32,56},{-32,130},{-72,130}},
                                       color={0,0,255}, pattern=LinePattern.Dot));
  connect(sensorT.Measure, Tin_output)
    annotation (Line(points={{-18,-82},{-44,-82},{-44,-66},{-86,-66}},
                                                   color={0,0,255}, pattern=LinePattern.Dot));
  connect(regulation_CRr1.VelR, Reactivity_Feedbacks.EntreeVelR) annotation (Line(points={{
          -379.091,150},{-316,150},{-316,110},{-270.4,110}},
                                                          color={0,0,255}, pattern=LinePattern.Dot));
  connect(Tout_Output, regulation_CRr1.Tout_Input) annotation (Line(points={{-72,130},
          {-302,130},{-302,140},{-400.909,140}},                 color={0,0,255}, pattern=LinePattern.Dot));
  connect(regulation_CRr1.Tin_Input, Tin_output) annotation (Line(points={{
          -400.909,152},{-440,152},{-440,-98},{-86,-98},{-86,-66}},  color={0,0,255}, pattern=LinePattern.Dot));
  connect(heatSource1.C[1], pressurizer_New.Cc) annotation (Line(points={{-64.12,166},{
          -48,166},{-48,172},{-36,172},{-36,172.8}}, color={0,0,0}));
  connect(regulation_pressurizerMSL.HeatersOutput, heatSource1.ISignal)
    annotation (Line(points={{-135.429,198.06},{-116,198.06},{-116,166},{-73,
          166}}, color={0,0,255}));
  connect(regulation_pressurizerMSL.MeasuredP, pressurizer_New.Pressure)
    annotation (Line(points={{-160.571,197.73},{-172,197.73},{-172,230},{-18,
          230},{-18,179},{-27,179}}, color={0,0,255}));
  connect(constante.y, regulation_pressurizerMSL.SetpointP) annotation (Line(
        points={{-203,184},{-174,184},{-174,189.26},{-160.571,189.26}}, color={
          0,0,255}));
  connect(Neutron_Kinetics.SortiePtot, Fuel_Thermal_Power.EntreePt) annotation (
     Line(points={{-226.1,16.04},{-178,16.04},{-178,-8},{-115.2,-8}}, color={0,
          0,255}));
  connect(PumpPlenum.Ce2, SG_Primary.C2)
    annotation (Line(points={{249,-100},{302,-100},{302,4}}, color={0,0,255}));
  connect(dynamicTwoPhaseFlowPipe.C2, sinkP.C)
    annotation (Line(points={{350,34},{350,71},{376,71}}, color={0,0,255}));
  connect(dynamicTwoPhaseFlowPipe.C1, sourceP1.C)
    annotation (Line(points={{350,0},{350,-36},{378,-36}}, color={0,0,255}));
  connect(heatExchangerWallCounterFlow1.WT1, dynamicTwoPhaseFlowPipe.CTh)
    annotation (Line(points={{326,16},{352,16},{352,17},{345.2,17}}, color={0,0,0}));
  connect(pressurizer_New.Cex, pressurizer_pressureloss.C1)
    annotation (Line(points={{-36,166},{-36,160},{34,160}}, color={0,0,255}));
  connect(singularPressureLoss_press1.C2, pressurizer_New.Cas)
    annotation (Line(points={{-58,204},{-36,204},{-36,186}}, color={0,0,255}));
  connect(sourceQ.C, singularPressureLoss_press1.C1)
    annotation (Line(points={{-84,204},{-78,204}}, color={0,0,255}));
  connect(regulation_pressurizerMSL.SprayValve, sourceQ.IMassFlow) annotation (
      Line(points={{-135.429,189.26},{-112,189.26},{-112,222},{-94,222},{-94,
          209}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-480,
            -180},{500,260}}),
                    graphics={
        Text(
          extent={{-236,224},{-164,202}},
          textColor={0,0,255},
          textString="Tout"),
        Text(
          extent={{-232,54},{-160,32}},
          textColor={0,0,255},
          textString="Tin"),
        Ellipse(
          extent={{-174,244},{186,-130}},
          lineColor={28,108,200},
          fillColor={217,241,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,118},{84,2}},
          textColor={0,0,255},
          textString="E-SMR NSSS 
primary side"),
        Text(
          extent={{182,144},{252,126}},
          textColor={0,0,255},
          textString="SG primary side
")}),                           Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-480,-180},{500,260}}),graphics={
        Text(
          extent={{32,16},{92,22}},
          textColor={0,0,255},
          textString="COOLANT FLOW 
IN THE CORE"),
        Text(
          extent={{176,32},{310,8}},
          textColor={0,0,255},
          textString="SG
PRIMARY SIDE"),
        Text(
          extent={{-262,-102},{-116,-126}},
          textColor={0,0,255},
          textString="FUEL RODS"),
        Text(
          extent={{30,124},{122,116}},
          textColor={255,0,0},
          textString="Upper Plenum"),
        Text(
          extent={{18,-76},{110,-84}},
          textColor={255,0,0},
          textString="Lower Plenum"),
        Text(
          extent={{-230,224},{-110,204}},
          textColor={0,0,255},
          textString="Pressure regulation"),
        Text(
          extent={{-412,180},{-328,160}},
          textColor={0,0,255},
          textString="CR regulation"),
        Text(
          extent={{-20,-134},{52,-126}},
          textColor={255,0,0},
          textString="Downcomer"),
        Text(
          extent={{134,-126},{236,-130}},
          textColor={255,0,0},
          textString="Centrifugal Pump"),
        Text(
          extent={{-62,124},{30,116}},
          textColor={255,0,0},
          textString="Riser"),
        Text(
          extent={{60,-22},{124,-16}},
          textColor={255,0,0},
          textString="Bypass
Core"), Text(
          extent={{176,-20},{240,-14}},
          textColor={255,0,0},
          textString="Bypass
SG"),   Text(
          extent={{-28,176},{42,168}},
          textColor={255,0,0},
          textString="Pressurizer"),
        Text(
          extent={{-268,124},{-138,114}},
          textColor={255,0,0},
          textString="Reactivity Feedbacks"),
        Text(
          extent={{-262,46},{-132,36}},
          textColor={255,0,0},
          textString="Neutron Kinetics"),
        Text(
          extent={{-276,-24},{-146,-34}},
          textColor={255,0,0},
          textString="Residual Power"),
        Text(
          extent={{-168,-24},{-38,-34}},
          textColor={255,0,0},
          textString="Thermal Power"),
        Text(
          extent={{370,32},{470,14}},
          textColor={0,0,255},
          textString="SG
SECONDARY SIDE")}));
end NSSS;
