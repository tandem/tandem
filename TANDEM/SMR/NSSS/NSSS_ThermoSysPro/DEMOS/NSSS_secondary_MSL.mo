within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.DEMOS;
model NSSS_secondary_MSL
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicTwoPhaseFlowPipe dynamicTwoPhaseFlowPipe(
    L=2,
    D=2.6667e-3,
    rugosrel=1e-6,
    ntubes=6*14409,
    z1=6.2,
    z2=8.2,
    Ns=6,
    inertia=false,
    advection=false,
    dynamic_mass_balance=false,
    simplified_dynamic_energy_balance=true,
    steady_state=true,
    continuous_flow_reversal=true)                                   annotation (Placement(
        transformation(
        extent={{-17,-16},{17,16}},
        rotation=90,
        origin={80,7})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI fluidInletI(
    P(start=4900000),
    Q(start=240),
    h(start=694754)) annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI fluidOutletI(
    P(start=4500000),
    Q(start=240),
    h(start=2.9453e6)) annotation (Placement(transformation(extent={{100,58},{120,78}})));
  NSSS_primary_base nSSS_basemodel
    annotation (Placement(transformation(extent={{-118,-42},{34,38}})));
  TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.WaterSteam_Connector.TSPro2Fluid fluid2TSPro1
    annotation (Placement(transformation(extent={{140,-70},{160,-50}})));
  TANDEM.Tools.Fluid_Adaptator_TSPro_MSL.WaterSteam_Connector.Fluid2TSPro fluid2TSPro
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={154,68})));
equation
  connect(nSSS_basemodel.thermalPort, dynamicTwoPhaseFlowPipe.CTh[6]) annotation (Line(
        points={{0.263415,10.1905},{54.25,10.1905},{54.25,7},{73.8667,7}},
                                                                       color={0,0,0}));
  connect(dynamicTwoPhaseFlowPipe.C1, fluidInletI)
    annotation (Line(points={{80,-10},{80,-60},{110,-60}}, color={0,0,255}));
  connect(dynamicTwoPhaseFlowPipe.C2, fluidOutletI)
    annotation (Line(points={{80,24},{80,68},{110,68}}, color={0,0,255}));
  connect(fluidInletI, fluid2TSPro1.steam_inlet)
    annotation (Line(points={{110,-60},{140,-60}}, color={0,0,255}));
  connect(fluidOutletI, fluid2TSPro.steam_outlet) annotation (Line(points={{110,68},{128,
          68},{128,68.0025},{144.05,68.0025}}, color={255,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-96,96},{98,-92}},
          lineColor={28,108,200},
          fillColor={217,241,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,74},{74,-58}},
          textColor={0,0,255},
          textString="E-SMR NSSS 
(SG secondary side)"),
        Text(
          extent={{106,44},{146,28}},
          textColor={255,0,0},
          textString="SG outlet"),
        Text(
          extent={{100,-14},{146,-26}},
          textColor={0,0,255},
          textString="SG inlet")}), Diagram(coordinateSystem(preserveAspectRatio=false),
        graphics={
        Text(
          extent={{74,18},{174,0}},
          textColor={0,0,255},
          textString="SG
SECONDARY SIDE")}));
end NSSS_secondary_MSL;
