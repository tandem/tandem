within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.DEMOS;
model NSSS_secondary
  NSSS_ThermoSysPro.NSSS_secondary_base nSSS_secondary
    annotation (Placement(transformation(extent={{-78,-38},{4,50}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP(
    P0=4500000,
    T0=573.55,
    option_temperature=1,
    mode=2) annotation (Placement(transformation(extent={{58,44},{90,74}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SourceQ sourceQ1(Q0=240, h0=
        691.1e3)
    annotation (Placement(transformation(
        extent={{-16,-15},{16,15}},
        rotation=180,
        origin={74,-49})));
equation
  connect(nSSS_secondary.fluidOutletI, sinkP.C)
    annotation (Line(points={{8.1,36.8},{8.1,59},{58,59}},
                                                         color={255,0,0}));
  connect(nSSS_secondary.fluidInletI, sourceQ1.C) annotation (Line(points={{8.1,
          -16},{52,-16},{52,-49},{58,-49}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-98,100},{100,-98}},
          lineColor={28,108,200},
          fillColor={217,241,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-64,72},{76,-60}},
          textColor={0,0,255},
          textString="E-SMR NSSS 
(SG secondary side)")}),                                         Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end NSSS_secondary;
