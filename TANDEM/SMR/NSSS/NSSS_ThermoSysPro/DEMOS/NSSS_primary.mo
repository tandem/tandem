within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.DEMOS;
model NSSS_primary
  NSSS_primary_base nSSS
    annotation (Placement(transformation(extent={{-134,-24},{34,62}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource heatSource2(
    T0={530.65,530.65,530.65,530.65,530.65,530.65},
    W0(displayUnit="MW") = {90000000,90000000,90000000,90000000,90000000,90000000},
    option_temperature=1) annotation (Placement(transformation(
        extent={{-18,-21},{18,21}},
        rotation=270,
        origin={139,42})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=
        15000000)
    annotation (Placement(transformation(extent={{-204,56},{-184,76}})));
  CONTROL.NSSS_CONTROL_ThermoSysPro nSSS_CONTROL_ThermoSysPro
    annotation (Placement(transformation(extent={{-132,62},{-88,96}})));
  ThermoSysPro.Thermal.Connectors.ThermalPort thermalPort[6]
    annotation (Placement(transformation(extent={{100,10},{120,30}})));
equation
  connect(nSSS.Tout_Output, nSSS_CONTROL_ThermoSysPro.Tout) annotation (Line(
        points={{-54.0976,30.0571},{-152,30.0571},{-152,85.8},{-134.2,85.8}},
        color={0,0,255}));
  connect(nSSS.Tin_output, nSSS_CONTROL_ThermoSysPro.Tin) annotation (Line(
        points={{-54.0976,-4.75238},{-168,-4.75238},{-168,92.6},{-134.2,92.6}},
        color={0,0,255}));
  connect(constante.y, nSSS_CONTROL_ThermoSysPro.SetpointPressure) annotation (
      Line(points={{-183,66},{-159,66},{-159,68.8},{-134.2,68.8}}, color={0,0,
          255}));
  connect(nSSS.MeasuredPressure, nSSS_CONTROL_ThermoSysPro.MeasuredPressure)
    annotation (Line(points={{-38.1171,52.581},{-32,52.581},{-32,110},{-144,110},
          {-144,75.6},{-134.2,75.6}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.VelocityR, nSSS.VelocityR) annotation (Line(
        points={{-85.8,91.24},{-76,91.24},{-76,47.2571},{-68.0293,47.2571}},
        color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Sprayers, nSSS.Sprayers) annotation (Line(
        points={{-85.8,70.16},{-68.0293,70.16},{-68.0293,59.9524}}, color={0,0,
          255}));
  connect(nSSS_CONTROL_ThermoSysPro.Heaters, nSSS.Heaters) annotation (Line(
        points={{-85.8,78.32},{-82,78.32},{-82,53.8095},{-68.0293,53.8095}},
        color={0,0,255}));
  connect(nSSS.thermalPort, thermalPort) annotation (Line(points={{33.5902,
          16.1333},{94,16.1333},{94,20},{110,20}}, color={0,0,0}));
  connect(heatSource2.C, thermalPort)
    annotation (Line(points={{118.42,42},{110,42},{110,20}}, color={0,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-98,98},{100,-100}},
          lineColor={28,108,200},
          fillColor={217,241,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-56,62},{70,-56}},
          textColor={0,0,255},
          textString="E-SMR NSSS 
(primary side)")}),                                              Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end NSSS_primary;
