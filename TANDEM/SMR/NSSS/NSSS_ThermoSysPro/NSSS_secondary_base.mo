within TANDEM.SMR.NSSS.NSSS_ThermoSysPro;
model NSSS_secondary_base
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicTwoPhaseFlowPipe dynamicTwoPhaseFlowPipe(
    L=2,
    D=2.6667e-3,
    rugosrel=1e-6,
    ntubes=8*14409,
    z1=6.2,
    z2=8.2,
    Ns=6,
    inertia=false,
    advection=false,
    dynamic_mass_balance=false,
    simplified_dynamic_energy_balance=true,
    steady_state=true,
    option_temperature=1,
    continuous_flow_reversal=false)                                               annotation (Placement(
        transformation(
        extent={{-17,-16},{17,16}},
        rotation=90,
        origin={80,7})));
  NSSS_primary_base nSSS_primary_base
    annotation (Placement(transformation(extent={{-146,-46},{28,40}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=
        15000000)
    annotation (Placement(transformation(extent={{-226,40},{-206,60}})));
  CONTROL.NSSS_CONTROL_ThermoSysPro nSSS_CONTROL_ThermoSysPro
    annotation (Placement(transformation(extent={{-148,56},{-104,90}})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI fluidInletI
    annotation (Placement(transformation(extent={{100,-60},{120,-40}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI fluidOutletI
    annotation (Placement(transformation(extent={{100,60},{120,80}})));
equation
  connect(nSSS_primary_base.thermalPort, dynamicTwoPhaseFlowPipe.CTh)
    annotation (Line(points={{27.5756,-5.86667},{53.25,-5.86667},{53.25,7},{
          75.2,7}}, color={0,0,0}));
  connect(constante.y, nSSS_CONTROL_ThermoSysPro.SetpointPressure) annotation (
      Line(points={{-205,50},{-183,50},{-183,62.8},{-150.2,62.8}}, color={0,0,
          255}));
  connect(nSSS_CONTROL_ThermoSysPro.VelocityR, nSSS_primary_base.VelocityR)
    annotation (Line(points={{-101.8,85.24},{-86,85.24},{-86,25.2571},{-77.6732,
          25.2571}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Heaters, nSSS_primary_base.Heaters)
    annotation (Line(points={{-101.8,72.32},{-94,72.32},{-94,31.8095},{-77.6732,
          31.8095}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Sprayers, nSSS_primary_base.Sprayers)
    annotation (Line(points={{-101.8,64.16},{-77.6732,64.16},{-77.6732,37.9524}},
        color={0,0,255}));
  connect(nSSS_primary_base.MeasuredPressure, nSSS_CONTROL_ThermoSysPro.MeasuredPressure)
    annotation (Line(points={{-46.6927,30.581},{-40,30.581},{-40,114},{-166,114},
          {-166,69.6},{-150.2,69.6}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Tin, nSSS_primary_base.Tin_output)
    annotation (Line(points={{-150.2,86.6},{-174,86.6},{-174,-26.7524},{
          -63.2439,-26.7524}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Tout, nSSS_primary_base.Tout_Output)
    annotation (Line(points={{-150.2,79.8},{-192,79.8},{-192,8.05714},{-63.2439,
          8.05714}}, color={0,0,255}));
  connect(dynamicTwoPhaseFlowPipe.C2, fluidOutletI) annotation (Line(points={{
          80,24},{78,24},{78,70},{110,70}}, color={0,0,255}));
  connect(dynamicTwoPhaseFlowPipe.C1, fluidInletI)
    annotation (Line(points={{80,-10},{80,-50},{110,-50}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(
          extent={{-96,96},{98,-92}},
          lineColor={28,108,200},
          fillColor={217,241,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,74},{74,-58}},
          textColor={0,0,255},
          textString="E-SMR NSSS 
(SG secondary side)"),
        Text(
          extent={{106,44},{146,28}},
          textColor={255,0,0},
          textString="SG outlet"),
        Text(
          extent={{100,-14},{146,-26}},
          textColor={0,0,255},
          textString="SG inlet")}), Diagram(coordinateSystem(preserveAspectRatio=false),
        graphics={
        Text(
          extent={{74,18},{174,0}},
          textColor={0,0,255},
          textString="SG
SECONDARY SIDE")}));
end NSSS_secondary_base;
