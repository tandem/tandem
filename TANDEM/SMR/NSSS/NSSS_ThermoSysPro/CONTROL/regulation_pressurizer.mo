within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.CONTROL;
model regulation_pressurizer

  ThermoSysPro.InstrumentationAndControl.Blocks.Math.BooleanToReal booleanToReal(realTrue=
        1,                                                                       realFalse=
       0)
    annotation (Placement(transformation(extent={{200,44},{220,64}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul
    annotation (Placement(transformation(extent={{264,48},{284,68}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=360000)
    annotation (Placement(transformation(extent={{196,86},{216,106}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1D table1D(Table=[-1,1; -0.1,
        1; -0.1,1; 0.1,0; 0.1,0; 1,0],
                          option_interpolation=1)
    annotation (Placement(transformation(extent={{150,-24},{170,-4}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul2
    annotation (Placement(transformation(extent={{234,-18},{254,2}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante1(k=240000)
    annotation (Placement(transformation(extent={{176,2},{196,22}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1D table1D1(Table=[-1,0; 0.17,
        0; 0.17,0; 0.52,1; 0.52,1; 1,1],
                                 option_interpolation=1)
    annotation (Placement(transformation(extent={{166,-78},{186,-58}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal SprayValve
    annotation (Placement(transformation(extent={{380,-78},{400,-58}}),
        iconTransformation(extent={{380,-78},{400,-58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add add1(k1=+1, k2=+1)
    annotation (Placement(transformation(extent={{330,12},{350,32}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal HeatersOutput
    annotation (Placement(transformation(extent={{380,82},{400,102}}),iconTransformation(
          extent={{380,82},{400,102}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal MeasuredP
    annotation (Placement(transformation(extent={{-60,76},{-40,96}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal SetpointP
    annotation (Placement(transformation(extent={{-60,-76},{-40,-56}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain(Gain=1/100000)
    annotation (Placement(transformation(extent={{-14,76},{6,96}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain1(Gain=1/100000)
    annotation (Placement(transformation(extent={{-14,-76},{6,-56}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.NonLineaire.Hysteresis
    hysteresis(uMax=-0.117, uMin=-0.17)
    annotation (Placement(transformation(extent={{156,44},{176,64}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback ERROR_T
    annotation (Placement(transformation(extent={{44,-28},{100,28}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul1
    annotation (Placement(transformation(extent={{262,-76},{282,-56}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante2(k=4.8)
    annotation (Placement(transformation(extent={{212,-50},{232,-30}})));
equation

  connect(booleanToReal.y, mul.u2)
    annotation (Line(points={{221,55.2},{256,55.2},{256,52},{263,52}},
                                                                     color={0,0,255}));
  connect(constante.y, mul.u1)
    annotation (Line(points={{217,96},{234,96},{234,64},{263,64}}, color={0,0,255}));
  connect(constante1.y, mul2.u1)
    annotation (Line(points={{197,12},{206,12},{206,-2},{233,-2}}, color={0,0,255}));
  connect(table1D.y, mul2.u2)
    annotation (Line(points={{171,-14},{233,-14}},                     color={0,0,255}));
  connect(mul.y, add1.u1)
    annotation (Line(points={{285,58},{312,58},{312,28},{329,28}}, color={0,0,255}));
  connect(mul2.y, add1.u2)
    annotation (Line(points={{255,-8},{312,-8},{312,16},{329,16}},   color={0,0,255}));
  connect(add1.y, HeatersOutput)
    annotation (Line(points={{351,22},{372,22},{372,92},{390,92}}, color={0,0,255}));
  connect(MeasuredP, gain.u)
    annotation (Line(points={{-50,86},{-15,86}}, color={0,0,255}));
  connect(SetpointP, gain1.u)
    annotation (Line(points={{-50,-66},{-15,-66}}, color={0,0,255}));
  connect(hysteresis.y, booleanToReal.u) annotation (Line(points={{177,53.8},{187.5,53.8},
          {187.5,55},{199,55}}, color={0,0,255}));
  connect(gain1.y, ERROR_T.u2) annotation (Line(points={{7,-66},{42,-66},{42,-68},{72,-68},
          {72,-30.8}}, color={0,0,255}));
  connect(gain.y, ERROR_T.u1)
    annotation (Line(points={{7,86},{20,86},{20,0},{41.2,0}}, color={0,0,255}));
  connect(ERROR_T.y, hysteresis.u)
    annotation (Line(points={{102.8,0},{124,0},{124,54},{155,54}}, color={0,0,255}));
  connect(ERROR_T.y, table1D.u) annotation (Line(points={{102.8,3.55271e-15},{124,
          3.55271e-15},{124,-14},{149,-14}}, color={0,0,255}));
  connect(ERROR_T.y, table1D1.u) annotation (Line(points={{102.8,3.55271e-15},{124,
          3.55271e-15},{124,-68},{165,-68}}, color={0,0,255}));
  connect(table1D1.y, mul1.u2) annotation (Line(points={{187,-68},{210,-68},{
          210,-66},{232,-66},{232,-72},{261,-72}}, color={0,0,255}));
  connect(constante2.y, mul1.u1) annotation (Line(points={{233,-40},{244,-40},{
          244,-60},{261,-60}}, color={0,0,255}));
  connect(mul1.y, SprayValve) annotation (Line(points={{283,-66},{374,-66},{374,
          -68},{390,-68}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-40,-200},{380,
            200}}), graphics={
        Rectangle(
          extent={{-40,-202},{378,200}},
          lineColor={0,0,255},
          pattern=LinePattern.Solid,
          lineThickness=0.25,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{104,42},{256,-16}},
          textColor={0,0,255},
          textString="PRESSURIZER
REGULATION")}),                                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-40,-200},{380,200}}),
                                                     graphics={
        Text(
          extent={{250,94},{330,78}},
          textColor={0,0,255},
          textString="BACKUP HEATER"),
        Text(
          extent={{248,-20},{336,-36}},
          textColor={0,0,255},
          textString="VARIABLE HEATER"),
        Text(
          extent={{190,-50},{246,-62}},
          textColor={0,0,255},
          textString="SPRAYS"),
        Text(
          extent={{-70,116},{-18,94}},
          textColor={0,0,255},
          textString="Measured P"),
        Text(
          extent={{-70,-38},{-28,-56}},
          textColor={0,0,255},
          textString="Setpoint P")}));
end regulation_pressurizer;
