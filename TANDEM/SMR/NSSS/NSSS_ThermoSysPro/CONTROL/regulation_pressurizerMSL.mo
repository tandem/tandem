within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.CONTROL;
model regulation_pressurizerMSL

  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul
    annotation (Placement(transformation(extent={{280,48},{300,68}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=360000)
    annotation (Placement(transformation(extent={{242,88},{262,108}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1D table1D(Table=[-1,1; -0.1,
        1; -0.1,1; 0.1,0; 0.1,0; 1,0],
                          option_interpolation=1)
    annotation (Placement(transformation(extent={{150,-24},{170,-4}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul2
    annotation (Placement(transformation(extent={{234,-18},{254,2}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante1(k=240000)
    annotation (Placement(transformation(extent={{176,2},{196,22}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1D table1D1(Table=[-1,0; 0.17,
        0; 0.17,0; 0.52,1; 0.52,1; 1,1],
                                 option_interpolation=1)
    annotation (Placement(transformation(extent={{166,-78},{186,-58}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal SprayValve
    annotation (Placement(transformation(extent={{380,-78},{400,-58}}),
        iconTransformation(extent={{380,-78},{400,-58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add add1(k1=+1, k2=+1)
    annotation (Placement(transformation(extent={{330,12},{350,32}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal HeatersOutput
    annotation (Placement(transformation(extent={{380,82},{400,102}}),iconTransformation(
          extent={{380,82},{400,102}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal MeasuredP
    annotation (Placement(transformation(extent={{-60,76},{-40,96}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal SetpointP
    annotation (Placement(transformation(extent={{-60,-78},{-40,-58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain(Gain=1/100000)
    annotation (Placement(transformation(extent={{-28,76},{-8,96}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain1(Gain=1/100000)
    annotation (Placement(transformation(extent={{-22,-78},{-2,-58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback ERROR_T
    annotation (Placement(transformation(extent={{12,-28},{68,28}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis3(
    uLow=0.117,
    uHigh=0.17,
    pre_y_start=false)
    annotation (Placement(transformation(extent={{156,52},{176,72}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal
    annotation (Placement(transformation(extent={{192,50},{212,70}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
    adaptorTSPModelica
    annotation (Placement(transformation(extent={{122,52},{142,72}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP
    annotation (Placement(transformation(extent={{232,50},{252,70}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul1
    annotation (Placement(transformation(extent={{246,-72},{266,-52}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante2(k=4.8)
    annotation (Placement(transformation(extent={{210,-56},{230,-36}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain2(Gain=-1)
    annotation (Placement(transformation(extent={{96,52},{116,72}})));
equation

  connect(constante.y, mul.u1)
    annotation (Line(points={{263,98},{274,98},{274,64},{279,64}}, color={0,0,255}));
  connect(constante1.y, mul2.u1)
    annotation (Line(points={{197,12},{206,12},{206,-2},{233,-2}}, color={0,0,255}));
  connect(table1D.y, mul2.u2)
    annotation (Line(points={{171,-14},{233,-14}},                     color={0,0,255}));
  connect(mul.y, add1.u1)
    annotation (Line(points={{301,58},{312,58},{312,28},{329,28}}, color={0,0,255}));
  connect(mul2.y, add1.u2)
    annotation (Line(points={{255,-8},{312,-8},{312,16},{329,16}},   color={0,0,255}));
  connect(add1.y, HeatersOutput)
    annotation (Line(points={{351,22},{372,22},{372,92},{390,92}}, color={0,0,255}));
  connect(MeasuredP, gain.u)
    annotation (Line(points={{-50,86},{-29,86}}, color={0,0,255}));
  connect(SetpointP, gain1.u)
    annotation (Line(points={{-50,-68},{-23,-68}}, color={0,0,255}));
  connect(gain1.y, ERROR_T.u2) annotation (Line(points={{-1,-68},{40,-68},{40,
          -30.8}},     color={0,0,255}));
  connect(gain.y, ERROR_T.u1)
    annotation (Line(points={{-7,86},{0,86},{0,0},{9.2,0}},   color={0,0,255}));
  connect(ERROR_T.y, table1D.u) annotation (Line(points={{70.8,3.55271e-15},{
          124,3.55271e-15},{124,-14},{149,-14}},
                                             color={0,0,255}));
  connect(ERROR_T.y, table1D1.u) annotation (Line(points={{70.8,3.55271e-15},{
          124,3.55271e-15},{124,-68},{165,-68}},
                                             color={0,0,255}));
  connect(hysteresis3.y, booleanToReal.u)
    annotation (Line(points={{177,62},{177,60},{190,60}},
                                                 color={255,0,255}));
  connect(adaptorTSPModelica.y, hysteresis3.u) annotation (Line(points={{144,62},
          {154,62}},                 color={0,0,127}));
  connect(booleanToReal.y, adaptorModelicaTSP.u) annotation (Line(points={{213,60},
          {230,60}},                       color={0,0,127}));
  connect(adaptorModelicaTSP.outputReal, mul.u2) annotation (Line(points={{253,60},
          {258,60},{258,52},{279,52}},     color={0,0,255}));
  connect(table1D1.y, mul1.u2)
    annotation (Line(points={{187,-68},{245,-68}}, color={0,0,255}));
  connect(constante2.y, mul1.u1) annotation (Line(points={{231,-46},{240,-46},{
          240,-56},{245,-56}}, color={0,0,255}));
  connect(mul1.y, SprayValve) annotation (Line(points={{267,-62},{374,-62},{374,
          -68},{390,-68}}, color={0,0,255}));
  connect(gain2.y, adaptorTSPModelica.inputReal)
    annotation (Line(points={{117,62},{121,62}}, color={0,0,255}));
  connect(ERROR_T.y, gain2.u) annotation (Line(points={{70.8,0},{78,0},{78,62},
          {95,62}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-40,-200},{380,
            200}}), graphics={
        Rectangle(
          extent={{-38,-202},{380,200}},
          lineColor={0,0,255},
          pattern=LinePattern.Solid,
          lineThickness=0.25,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{96,48},{246,-34}},
          textColor={0,0,255},
          textString="PRESSURIZER
REGULATION
(MSL)")}),                                                       Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-40,-200},{380,200}}),
                                                     graphics={
        Text(
          extent={{278,118},{358,102}},
          textColor={0,0,255},
          textString="BACKUP HEATER"),
        Text(
          extent={{248,-20},{336,-36}},
          textColor={0,0,255},
          textString="VARIABLE HEATER"),
        Text(
          extent={{216,-82},{272,-94}},
          textColor={0,0,255},
          textString="SPRAYS"),
        Text(
          extent={{-70,116},{-18,94}},
          textColor={0,0,255},
          textString="Measured P"),
        Text(
          extent={{-70,-38},{-28,-56}},
          textColor={0,0,255},
          textString="Setpoint P")}));
end regulation_pressurizerMSL;
