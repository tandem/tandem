within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.CONTROL;
model NSSS_CONTROL_ThermoSysPro
  regulation_CRr regulation_CRr1
    annotation (Placement(transformation(extent={{-30,24},{30,82}})));
  regulation_pressurizerMSL regulation_pressurizerMSL1
    annotation (Placement(transformation(extent={{-34,-66},{32,0}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal VelocityR
    annotation (Placement(transformation(extent={{100,62},{120,82}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Tin
    annotation (Placement(transformation(extent={{-120,70},{-100,90}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Tout
    annotation (Placement(transformation(extent={{-120,30},{-100,50}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal
    MeasuredPressure
    annotation (Placement(transformation(extent={{-120,-30},{-100,-10}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal
    SetpointPressure
    annotation (Placement(transformation(extent={{-120,-70},{-100,-50}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal Heaters
    annotation (Placement(transformation(extent={{100,-14},{120,6}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal Sprayers
    annotation (Placement(transformation(extent={{100,-62},{120,-42}})));
equation
  connect(regulation_CRr1.VelR, VelocityR) annotation (Line(points={{32.7273,
          64.6},{94,64.6},{94,72},{110,72}}, color={0,0,255}));
  connect(MeasuredPressure, regulation_pressurizerMSL1.MeasuredP) annotation (
     Line(points={{-110,-20},{-108,-20},{-108,-18.81},{-35.5714,-18.81}},
        color={0,0,255}));
  connect(SetpointPressure, regulation_pressurizerMSL1.SetpointP) annotation (
     Line(points={{-110,-60},{-72,-60},{-72,-44.22},{-35.5714,-44.22}}, color=
         {0,0,255}));
  connect(Tin, regulation_CRr1.Tin_Input) annotation (Line(points={{-110,80},
          {-40,80},{-40,70.4},{-32.7273,70.4}}, color={0,0,255}));
  connect(Tout, regulation_CRr1.Tout_Input) annotation (Line(points={{-110,40},
          {-40,40},{-40,35.6},{-32.7273,35.6}}, color={0,0,255}));
  connect(regulation_pressurizerMSL1.HeatersOutput, Heaters) annotation (Line(
        points={{33.5714,-17.82},{82,-17.82},{82,-4},{110,-4}}, color={0,0,
          255}));
  connect(regulation_pressurizerMSL1.SprayValve, Sprayers) annotation (Line(
        points={{33.5714,-44.22},{80,-44.22},{80,-52},{110,-52}}, color={0,0,
          255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,-100},{100,100}},
          lineColor={0,0,255},
          pattern=LinePattern.Solid,
          lineThickness=0.25,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-76,22},{76,-30}},
          textColor={0,0,255},
          textString="NSSS
CONTROL
")}),
    Diagram(coordinateSystem(preserveAspectRatio=false)));
end NSSS_CONTROL_ThermoSysPro;
