within TANDEM.SMR.NSSS.NSSS_ThermoSysPro.CONTROL;
model regulation_CRr
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Tin_Input
    annotation (Placement(transformation(extent={{-120,50},{-100,70}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Tout_Input
    annotation (Placement(transformation(extent={{-120,-70},{-100,-50}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add AverageT(k1=0.5, k2=0.5)
    annotation (Placement(transformation(extent={{-68,-10},{-48,10}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback ERROR_T
    annotation (Placement(transformation(extent={{-30,30},{-10,50}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=585.45)
    annotation (Placement(transformation(extent={{-84,50},{-64,70}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain(Gain=-32.24)
    annotation (Placement(transformation(extent={{48,30},{68,50}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.NonLineaire.BandeMorte bandeMorte(uMax=
        0.5, uMin=-0.5)
             annotation (Placement(transformation(extent={{12,30},{32,50}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.NonLineaire.Limiteur limiteur(maxval=72,
      minval=-72) annotation (Placement(transformation(extent={{82,30},{102,50}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal VelR
    annotation (Placement(transformation(extent={{120,30},{140,50}})));
equation
  connect(Tin_Input, AverageT.u1) annotation (Line(points={{-110,60},{-92,60},{-92,6},{
          -69,6}},       color={0,0,255}));
  connect(Tout_Input, AverageT.u2)
    annotation (Line(points={{-110,-60},{-92,-60},{-92,-6},{-69,-6}},
                                                                    color={0,0,255}));
  connect(AverageT.y, ERROR_T.u2)
    annotation (Line(points={{-47,0},{-20,0},{-20,29}},   color={0,0,255}));
  connect(constante.y, ERROR_T.u1)
    annotation (Line(points={{-63,60},{-38,60},{-38,40},{-31,40}}, color={0,0,255}));
  connect(ERROR_T.y, bandeMorte.u)
    annotation (Line(points={{-9,40},{11,40}}, color={0,0,255}));
  connect(bandeMorte.y, gain.u)
    annotation (Line(points={{33,40},{47,40}}, color={0,0,255}));
  connect(gain.y, limiteur.u)
    annotation (Line(points={{69,40},{81,40}}, color={0,0,255}));
  connect(limiteur.y, VelR)
    annotation (Line(points={{103,40},{130,40}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{120,
            100}}), graphics={
        Rectangle(
          extent={{-100,-100},{120,100}},
          lineColor={0,0,255},
          pattern=LinePattern.Solid,
          lineThickness=0.25,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-26,18},{44,-2}},
          textColor={0,0,255},
          textString="CONTROL RODS
REGULATION")}),                                                  Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{120,100}}),
                                      graphics={Text(
          extent={{-124,82},{-96,70}},
          textColor={0,0,255},
          textString="Tin"), Text(
          extent={{-128,-34},{-98,-46}},
          textColor={0,0,255},
          textString="Tout")}));
end regulation_CRr;
