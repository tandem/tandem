# Dynamic model of the Nuclear Steam Supply System
This package provides the dynamic model of light-water cooled Small Modular Reactor (SMR), based on the European SMR (E-SMR) design as proposed by the Euratom [ELSMOR](http://www.elsmor.eu) project. Moreover, the dataset encompassing the main geometrical and operational parameters developed within the scope of this project has been considered as a reference. The key parameters of the reactor are reported in the following table:

| Parameter | Value | Unit |
|-- | -- | -- |
| Power | 540 | MWth |
| Core inlet temperature | 300 | °C |
| Core outlet temperature | 324.5 | °C |
| Nominal coolant flow rate | 3700 | kg/s |
| Nominal pressure | 150 | bar |

The E-SMR is an integral SMR, with the main NSSS components packed within the reactor pressure vessel. The primary coolant is heated in the core and flows through the riser to the upper plenum. This is connected to the pressurizer, which features a hemispherical shape, and to the steam generator inlet. The reactor has been designed with six compact steam generators and two additional safety heat exchangers, although these are not included in the proposed dynamic model. Six spool-type pumps, located at the steam generator outlets, force the flow of the coolant through the downcomer to the lower plenum at the core's inlet.

# NSSS (ThermoSysPro)
## Component Description

The NSSS component is divided in 2 main sections : 
- On the left, the nuclear core, which includes all the modules that describe the power production in the fuel by fission, the reactor's kinetics (and hence the reactivity feedback effects in the core) and the heat transfer in the fuel and from the fuel to the coolant.
- On the right, the primary loop of the reactor, which describes the coolant's path from the core to the primary side of the steam generator is modeled.
This section includes the pressurizer component, which keeps the pressure constant in the core, and the pump.

The two sides of the model are thermally connected through a heat transfer wall, which ideally represents the cladding of the fuel rods. The NSSS model also includes the average coolant temperature control system and the pressure control system.

## Package description
The package is structured as follows:
- ***NSSS*** model, with the different interfaces available: the *NSSS_primary_base* model presents a thermal interface and does not include the secondary side of the steam generator; the *NSSS_secondary_base* model instead includes the secondary side of the steam generator and the fluid interface. 
 - ***DEMOS*** package, that contains some steady state examples of the models with different interfaces
 - ***CONTROL*** package that contains the system's regulation models

 
## Prerequisites
The model uses the _WaterSteam_, *InstrumentationAndControl*, *Thermal* and the _NuclearCore_ ThermoSysPro library subpackages, which must be opened in order to compile the code. It also uses some components of the Modelica Standard Library.

## Main Modeling Assumptions


### *Neutronics assumptions*
- The Xenon concentration in the core is considered constant and null, and this feature could be improved to consider also the variation of Xenon concentration in the core
- The control rods groups are only two, the grey (G) group and the regulating (R) group. The G group is considered still and completely extracted from the core; its position and velocity can be changed by the user if needed.
- The control rod worth is considered constant and indipendent from the position.
- The core is initialized in critical condition (the total reactivity is zero)
- For the calculation of the residual power, only six groups of radioisotopes are considered.
- The model does not consider the possibility of SCRAM of the reactor.
- Six groups of delayed neutrons are considered in the point reactor kinetics equations.

### *Thermal-hydraulics assumptions*
- It is assumed that the coolant passes in the core through 20064 fictitious tubes (same number as the fuel rods), with a diameter equal to the fuel rods' pitch. 
- In the primary loop of the core, the fluid's density is considered constant (the coolant is liquid)
- All the pipes and heat exchangers have a cylindrical geometry.
- The pressure loss components, the pipes, the steam generator and the pressurizer are considered adiabatic. 
- The *Volume* components solve the mass and energy balance equation to compute the pressure and enthalpy of the fluid in the cell.
- The *Flow components* (pipe and singular pressure losses), solve the momentum balance equation to compute the mass flow rate at the boundaries of the cell.
- The steam generator model presents a 1D staggered grid scheme, with N segments (N can be set by the user).
- The convection heat transfer coefficient is computed using the Dittus-Boelter correlation for the one-phase flow, and the Gungor-Chen correlation for two-phase flow.
- The friction pressure loss term is computed using the Darcy-Weisbach equation; the friction pressure loss coefficient is computed with the the Idel’cik correlation.
- The pipe roughness is considered constant in all the components.
- The pressurizer presents a two-region mathematical model; the conservation equations are solved for the liquid and vapor phases.
- The liquid that enters the pressurizer from the hot leg of the core is assumed to enter the volume with an enthalpy equal to the saturation enthalpy at the pressurizer's pressure.
- The water sprays of the pressurizer injected water taken from an indipendent external source, and not from the cold leg of the reactor. This could mean that the pressurizer might fill up completely if the transients are too long.
- Thermal diffusion and advection are not considered.
- The six compact plate steam generators of the SMR are modeled as one-tube equivalents of the tube bundle heat exchanger.
- The six pumps of the SMR are considered as one pump which handles the total mass flow rate of the coolant.


### *Control assumptions*
- The average temperature control systems regulates the temperature of the coolant in the core without considering the power demanded from the grid (which is not considered in this model). 
- The level control system of the water in the pressurizer is not modeled.
- The control system of the average temperature only acts on the R group of the control rods; the regulation of the G group can be added if necessary.

---


### CONTROL package

In the **CONTROL** subpackage the models of the control systems can be found:

1. regulation_pressurizer: model of the pressure control system
2. regulation_pressurizerMSL: model of the pressure control system with Modelica Standard Library classes 
3. regulation_CRr: model of the average temperature control system that acts of the group R of the control rods
4. NSSS_CONTROL_ThermoSysPro: stand-alone model of the entire control system of the NSSS

## Provided Examples

### DEMOS package
In the **DEMOS** subpackage some usage examples can be found: the demos provided are steady state, while some transient cases can be found in the **Benchmark** subpackage.
1. *NSSS_primary*: thermal interface at the primary side of the steam generator
2. *NSSS_secondary*: fluid interface at the secondary side of the steam generator.
3. *NSSS_secondary_MSL*: Modelica Standard library connectors at the secondary side of the steam generator

### Benchmark package

In the **Benchmark** subpackage, and in particular in the *NSSS_Benchmark_ThermoSysPro* class there are three power transient examples that can be used as benchmark with the ThermoPower NSSS model:

1. 10% step decrease of the power extracted at the secondary side of the steam generator
2. 50% ramp decrease (2%/minute) of the power extracted at the secondary side of the steam generator
3.  50% ramp decrease (5%/minute) of the power extracted at the secondary side of the steam generator

---

# NSSS (ThermoPower)
The `NSSS_ThermoPower` package offers various versions of the NSSS model, allowing users to select the most suitable according to their specific needs. In particular, the models have different degrees of detail (e.g., in terms of bypass flows) or different interfaces to facilitate the coupling with other models. 
The models are based on components from the [ThermoPower](https://github.com/casella/ThermoPower) library, developed at Politecnico di Milano. 

## Package description
The package is structured as follows:
- The `Test` package contains some test cases for the various versions of the NSSS model, showcasing its dynamic response after a perturbation of a variable exchanged at its interface (e.g., thermal power exchanged in the steam generator or secondary coolant flow rate).
- The main models have either a thermal or a fluid exchange interface. A simplified version, without core and steam generator bypass flows, is provided for both versions.
- The novel components developed for the NSSS model, namely the core, the pressurizer, and the steam generator, are collected in the `Components` package.
- The `Control` package delivers the controller components to regulate the pressure in the NSSS by acting on the pressurizer's sprayers and electrical heater, and an example of core average temperature control program. These examples of controllers can be coupled to the NSSS model through dedicated connectors.
- Records for the neutronic parameters, together with some examples of initial conditions for the temperature distribution in the fuel rods to support the initialization of the models, are available in the `Data` package.
- The `Interface` package encompasses the partial models with the connectors and parameters that will be used for the different NSSS and controller models.
- The units that were not available in the Modelica Standard Library, such as those related to the neutronic modelling of the core, are included in the `Units` package.

## Components description and assumptions
The core, pressurizer, and steam generator models included in this package were developed specifically for the TANDEM library. The other components used for the simulation of the primary coolant flow within the NSSS, e.g., the `Riser`, `UpperPlenum`, `LowerPlenum`, `Downcomer`, etc., stem from the [Water](https://build.openmodelica.org/Documentation/ThermoPower.Water.html) package of the ThermoPower library. 

### Core
The `Core` model is the first model available in this package and is based on three components gathered in the `BaseClasses` package:
- The `Flow1DFV_LiquidCoolant` component, employed to simulate the coolant flow through the core,
- The `NeutronicKinetics` component encompasses the neutronic model of the core,
- Lastly, the `Fuel` model provides the thermal analysis of the fuel rods.

#### Flow1DFV_LiquidCoolant
The `Flow1DFV_LiquidCoolant` is based on ThermoPower's [Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) component. In particular, the latter component is extended with an additional connector (the `Moderation` connector in the `Interfaces` subpackage) to transfer the coolant's average temperature to the `NeutronKinetics` block, which is required to estimate the moderator feedback contribution. As a result, the required parameters are those reported in the documentation of the ThermoPower component.
[Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) also includes a connector exchanging thermal power and temperature, [DHTVolumes](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.DHTVolumes.html), which is used to thermally couple the coolant flow through the subchannel to the fuel rod's outer surface.

#### NeutronKinetics
The `NeutronKinetics` model is required to simulate the neutronic behaviour in the core.
It is based on point kinetics equations, with reactivity contributions from external sources (e.g., control rods), as well as reactivity feedbacks due to Xenon poisoning and fuel and moderator temperature. 

The reactivity contribution due to Xenon poisoning is computed by exploiting the decay laws of iodine-135 and xenon-135. On the other hand, the temperature feedbacks are driven by the coefficients $\alpha_{f}$ and $\alpha_{m}$ and the fuel and moderator temperatures.
Several options can be selected to specify the external reactivity insertion. Specifically, the external input can be considered as:
- the reactivity insertion, in pcm.
- the control rod displacement with respect to a reference position. In the model, the reactivity insertion is proportional to this displacement.
- the control rod position, which will be converted into the reactivity insertion through a sinusoidal relation.
 
The `NeutronKinetics` component interacts with the other core components through dedicated connectors included in the `Interfaces` subpackage, such as the aforementioned `Moderation` connector and the `Fission` connector, employed to exchange the fission power and the Doppler temperature (computed in the fuel rod's thermal model according to Rowland's model) with the `Fuel` component.

#### Fuel
The thermal model of the fuel rods is based on a user-defined axial and radial discretization of the fuel pellets. In addition, energy balances on the gap and on the cladding are implemented.  As for the axial fission power distribution in the fuel pellets, two options can be selected by the user: either a uniform power distribution or a cosinusoidal distribution. 

The fuel zone is discretized radially and axially in control volumes of equal volume. The time-dependent Fourier equation, accounting for the heat source term due to fission and radial heat transfer through conduction, is adopted to model the thermal behaviour of the pellets. The fuel material properties and, similarly, the cladding and gap properties are available in dedicated models accounting for temperature dependencies within the `MaterialProperties` subpackage.

The boundary conditions of the models are imposed through dedicated connectors, including the aforementioned `Fission` connector and ThermoPower's [DHTVolumes](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.DHTVolumes.html) component, which is used to exchange thermal power and temperature at the cladding's outer surface with the coolant flow.

### Pressurizer
The pressurizer model `PressEq` is based on the assumption of thermal equilibrium between liquid and vapour phases, i.e., considering a homogeneous fluid mixture within the pressurizer volume.

### Compact Steam Generator
The `CompactSG` component relies exclusively on components of the ThermoPower library. In particular, the [Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) and the [Flow1DV2ph](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV2ph.html) represent the primary and secondary sides, respectively. The [MetalWallFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.MetalWallFV.html) component is adopted to account for the thermal resistance and inertia of the metal structure, whereas the [CounterCurrentFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.CounterCurrentFV.html) component allows considering counter-current flow directions.

The parameters required by these ThermoPower components can be computed, accounting for the steam generator configuration of interest. In the case of the E-SMR reactor, a printed circuit heat exchanger design, characterised by microchannels with a rectangular cross-section, is considered as a reference.  As a result, the hydraulic diameter, flow area, and other parameters required by the [Flow1DV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) component can be computed starting from the channel size and other geometric parameters.

## Control strategy
The `Control` package includes some examples of controllers to regulate the pressurizer's actuators and the reactivity insertion in the core. 

In the reference control strategy, the **pressure control** in the pressurizer is based on the activation of sprayers when the pressure exceeds a given threshold, whereas two electrical heaters are activated if the pressure falls below the specified threshold. From a modelling standpoint, the [Hysteresis](https://doc.modelica.org/Modelica%204.0.0/Resources/helpDymola/Modelica_Blocks_Logical.html#Modelica.Blocks.Logical.Hysteresis) component of the [Modelica Standard Library](https://doc.modelica.org/Modelica%204.0.0/Resources/helpDymola/Modelica.html) has been used to compare the measured pressure with the threshold and thus regulate the sprayer's and heater's activation.

As for the **reactivity control**, a constant average temperature control program has been selected. In particular, the error between the average core temperature and its setpoint is computed considering a dead zone of 0.5°C. The resulting error is used as an input in a [PI](https://doc.modelica.org/Modelica%204.0.0/Resources/helpDymola/Modelica_Blocks_Continuous.html#Modelica.Blocks.Continuous.LimPID) controller to determine the required reactivity insertion through control rods. 



## Test cases
This package provides a test case for each NSSS model version to analyse the dynamic response of the system following perturbations on the secondary side, either in terms of feedwater flow or exchanged thermal power. In each case, a ramp-wise power or feedwater decrease is simulated.

In the test cases, the NSSS model is connected to an illustrative controller, encompassing pressure and average core temperature control by regulating the pressurizer's actuators and the reactivity insertion, respectively.

The following test cases are proposed:
- The `Test_NSSSsimple_fluid` demo showcases the dynamic behaviour of the simplified NSSS model with a fluid exchange interface. The model is tested by imposing boundary conditions for the fluid's state on the secondary side, namely the feedwater conditions entering the steam generator (in terms of mass flow rate and enthalpy through the ThermoPower [SourceMassFlow](https://build.openmodelica.org/Documentation/ThermoPower.Water.SourceMassFlow.html) component) and the pressure (through the [SinkPressure](https://build.openmodelica.org/Documentation/ThermoPower.Water.SinkPressure.html) component) at the steam generator's outlet. 
- An analogous transient simulation is proposed in the `Test_NSSSbypass_fluid` model. In this case, the aim is to test the detailed NSSS model, which also features core and steam generator bypass flows.
- In the `Test_NSSSsimple_thermal` demo, the transient response of the simplified NSSS model to a reduction in thermal power removal at the steam generator is investigated. The boundary condition of the model, i.e., the thermal power exchanged at the steam generator's wall, is imposed through ThermoPower's  [HeatSource1DFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.HeatSource1DFV.html) component. 
- As for the fluid interface version, the detailed NSSS model with thermal interface is tested in the  `Test_NSSSbypass_thermal` model. 

The `Test_NSSSsimple_thermal` and `Test_NSSSbypass_thermal` demo cases are proposed also in the `Benchmark` package to compare the simulation outcomes with the NSSS model in ThermoSysPro.