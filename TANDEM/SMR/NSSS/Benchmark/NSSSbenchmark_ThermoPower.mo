within TANDEM.SMR.NSSS.Benchmark;
model NSSSbenchmark_ThermoPower
  import TANDEM;
  Modelica.Blocks.Sources.Ramp ramp_2perc(
    height=-270e6,
    duration=1500,
    offset=540e6,
    startTime=200) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={72,-68})));
  ThermoPower.Thermal.HeatSource1DFV heatSource1DFV(Nw=10)   annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin={8,-8})));
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
    Placement(transformation(extent={{-80,80},{-60,100}})));
  Modelica.Blocks.Sources.Step step1(
    height=-0.1*540e6,
    offset=540e6,
    startTime=500)                                                                     annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin={72,12})));
  Modelica.Blocks.Math.Gain gain(k=-1) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={30,-8})));
  Modelica.Blocks.Sources.Ramp ramp_5perc(
    height=-270e6,
    duration=600,
    offset=540e6,
    startTime=200) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={72,-26})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                       NSSSctrl
    annotation (Placement(transformation(extent={{-60,32},{-22,64}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSbypass_thermal nsss(
    core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom(displayUnit="Pa") = 22280,
    Cfnom=0.0037,
    Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp1=200000,
    Cfnom1=0.004,
    Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp2=40000,
    Cfnom2=0.0086448975,
    rho_sg(displayUnit="kg/m3"),
    eta=0.9,
    q_nom={0,0.85035195,1.51494276},
    head_nom={52.9277496,36.251883,0},
    hstart_pump=1.33804e6,
    dp0=257900,
    SGoutlet(noInitialPressure=false),
    SGbypass(noInitialPressure=false))
    annotation (Placement(transformation(extent={{-70,-28},{-12,8}})));
equation
  connect(gain.y,heatSource1DFV. power)
    annotation (Line(points={{23.4,-8},{12,-8}},   color={0,0,127}));
  connect(gain.u,step1. y) annotation (Line(points={{37.2,-8},{52,-8},{52,12},{
          61,12}},  color={0,0,127}));
  connect(nsss.dHTVolumes, heatSource1DFV.wall) annotation (Line(points={{-12,
          -8.38},{-10,-8.38},{-10,-8},{5,-8},{5,-8}}, color={255,127,0}));
  connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
      points={{-52.4,32},{-55.5,32},{-55.5,7.64}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
      points={{-29.6,32},{-30,32},{-30,16},{-26.5,16},{-26.5,7.64}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=1000, __Dymola_Algorithm="Dassl"));
end NSSSbenchmark_ThermoPower;
