within TANDEM.SMR.NSSS.Benchmark;
model regulationpressurizer_ThermoSysPro

  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul
    annotation (Placement(transformation(extent={{264,48},{284,68}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=80000)
    annotation (Placement(transformation(extent={{196,86},{216,106}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul2
    annotation (Placement(transformation(extent={{264,-10},{284,10}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante1(k=250000)
    annotation (Placement(transformation(extent={{194,22},{214,42}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal SprayValve
    annotation (Placement(transformation(extent={{380,-78},{400,-58}}),
        iconTransformation(extent={{380,-78},{400,-58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add add1(k1=+1, k2=+1)
    annotation (Placement(transformation(extent={{330,12},{350,32}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal HeatersOutput
    annotation (Placement(transformation(extent={{380,82},{400,102}}),iconTransformation(
          extent={{380,82},{400,102}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal MeasuredP
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain(Gain=1/100000)
    annotation (Placement(transformation(extent={{-22,-10},{-2,10}})));
  Modelica.Blocks.Logical.Hysteresis hysteresisPROP(
    uLow=-137.895,
    uHigh=-136.516,
    pre_y_start=false)
    annotation (Placement(transformation(extent={{122,70},{142,90}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal
    annotation (Placement(transformation(extent={{162,70},{182,90}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP
    annotation (Placement(transformation(extent={{198,58},{218,78}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
    adaptorTSPModelica
    annotation (Placement(transformation(extent={{86,70},{106,90}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal1
    annotation (Placement(transformation(extent={{198,-10},{218,10}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP1
    annotation (Placement(transformation(extent={{232,-10},{252,10}})));
  Modelica.Blocks.Logical.Hysteresis hysteresisBACKUP(
    uLow=-138.929,
    uHigh=-133.414,
    pre_y_start=false)
    annotation (Placement(transformation(extent={{154,-10},{174,10}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
    adaptorTSPModelica1
    annotation (Placement(transformation(extent={{120,-10},{140,10}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis1(
    uLow=139.6188,
    uHigh=142.7215,
    pre_y_start=false)
    annotation (Placement(transformation(extent={{160,-80},{180,-60}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal2(realTrue=1,    realFalse=0)
    annotation (Placement(transformation(extent={{206,-80},{226,-60}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
    adaptorTSPModelica2
    annotation (Placement(transformation(extent={{118,-80},{138,-60}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP2
    annotation (Placement(transformation(extent={{238,-80},{258,-60}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Mul mul1
    annotation (Placement(transformation(extent={{288,-76},{308,-56}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante2(k=1.91)
    annotation (Placement(transformation(extent={{244,-58},{264,-38}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain1(Gain=-1)
    annotation (Placement(transformation(extent={{54,70},{74,90}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain2(Gain=-1)
    annotation (Placement(transformation(extent={{80,-10},{100,10}})));
equation

  connect(constante.y, mul.u1)
    annotation (Line(points={{217,96},{234,96},{234,64},{263,64}}, color={0,0,255}));
  connect(constante1.y, mul2.u1)
    annotation (Line(points={{215,32},{258,32},{258,6},{263,6}},   color={0,0,255}));
  connect(mul.y, add1.u1)
    annotation (Line(points={{285,58},{312,58},{312,28},{329,28}}, color={0,0,255}));
  connect(mul2.y, add1.u2)
    annotation (Line(points={{285,0},{324,0},{324,16},{329,16}},     color={0,0,255}));
  connect(add1.y, HeatersOutput)
    annotation (Line(points={{351,22},{372,22},{372,92},{390,92}}, color={0,0,255}));
  connect(MeasuredP, gain.u)
    annotation (Line(points={{-50,0},{-23,0}},   color={0,0,255}));
  connect(booleanToReal.y, adaptorModelicaTSP.u) annotation (Line(points={{183,
          80},{190,80},{190,68},{196,68}}, color={0,0,127}));
  connect(adaptorModelicaTSP.outputReal, mul.u2) annotation (Line(points={{219,
          68},{222,68},{222,70},{226,70},{226,52},{263,52}}, color={0,0,255}));
  connect(hysteresisPROP.y, booleanToReal.u)
    annotation (Line(points={{143,80},{160,80}}, color={255,0,255}));
  connect(adaptorTSPModelica.y, hysteresisPROP.u)
    annotation (Line(points={{108,80},{120,80}}, color={0,0,127}));
  connect(mul2.u2, adaptorModelicaTSP1.outputReal)
    annotation (Line(points={{263,-6},{253,-6},{253,0}}, color={0,0,255}));
  connect(booleanToReal1.y, adaptorModelicaTSP1.u)
    annotation (Line(points={{219,0},{230,0}}, color={0,0,127}));
  connect(booleanToReal1.u, hysteresisBACKUP.y)
    annotation (Line(points={{196,0},{175,0}}, color={255,0,255}));
  connect(hysteresisBACKUP.u, adaptorTSPModelica1.y)
    annotation (Line(points={{152,0},{142,0}}, color={0,0,127}));
  connect(hysteresis1.y, booleanToReal2.u)
    annotation (Line(points={{181,-70},{204,-70}}, color={255,0,255}));
  connect(hysteresis1.u, adaptorTSPModelica2.y)
    annotation (Line(points={{158,-70},{140,-70}}, color={0,0,127}));
  connect(booleanToReal2.y, adaptorModelicaTSP2.u)
    annotation (Line(points={{227,-70},{236,-70}}, color={0,0,127}));
  connect(adaptorModelicaTSP2.outputReal, mul1.u2)
    annotation (Line(points={{259,-70},{259,-72},{287,-72}}, color={0,0,255}));
  connect(mul1.y, SprayValve) annotation (Line(points={{309,-66},{374,-66},{374,
          -68},{390,-68}}, color={0,0,255}));
  connect(constante2.y, mul1.u1) annotation (Line(points={{265,-48},{276,-48},{
          276,-60},{287,-60}}, color={0,0,255}));
  connect(gain1.y, adaptorTSPModelica.inputReal)
    annotation (Line(points={{75,80},{85,80}}, color={0,0,255}));
  connect(gain.y, gain1.u)
    annotation (Line(points={{-1,0},{48,0},{48,80},{53,80}}, color={0,0,255}));
  connect(gain.y, gain2.u)
    annotation (Line(points={{-1,0},{79,0}},               color={0,0,255}));
  connect(gain2.y, adaptorTSPModelica1.inputReal)
    annotation (Line(points={{101,0},{119,0}},         color={0,0,255}));
  connect(gain.y, adaptorTSPModelica2.inputReal) annotation (Line(points={{-1,0},
          {72,0},{72,-70},{117,-70}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-40,-120},{380,140}})),
                                                                 Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-40,-120},{380,140}}),
                                                     graphics={
        Text(
          extent={{292,-4},{372,-20}},
          textColor={0,0,255},
          textString="BACKUP HEATER"),
        Text(
          extent={{230,114},{318,98}},
          textColor={0,0,255},
          textString="VARIABLE HEATER"),
        Text(
          extent={{170,-40},{226,-52}},
          textColor={0,0,255},
          textString="SPRAYS"),
        Text(
          extent={{-68,40},{-16,18}},
          textColor={0,0,255},
          textString="Measured P")}));
end regulationpressurizer_ThermoSysPro;
