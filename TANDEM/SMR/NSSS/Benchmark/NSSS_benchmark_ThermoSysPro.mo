within TANDEM.SMR.NSSS.Benchmark;
model NSSS_benchmark_ThermoSysPro
  NSSS_ThermoSysPro.NSSS_primary_base nSSS_basemodel
    annotation (Placement(transformation(extent={{-154,-46},{14,40}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource heatSource2(
    T0={530.65,530.65,530.65,530.65,530.65,530.65},
    W0(displayUnit="MW") = {90000000,90000000,90000000,90000000,90000000,90000000},
    option_temperature=1) annotation (Placement(transformation(
        extent={{-18,-21},{18,21}},
        rotation=270,
        origin={87,20})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=nSSS_basemodel.Wsg)
    annotation (Placement(transformation(extent={{-2,28},{18,48}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback feedback
    annotation (Placement(transformation(extent={{54,74},{82,102}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP
    annotation (Placement(transformation(extent={{44,38},{64,58}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI pI(k=(312 - 257)/
        540e6) annotation (Placement(transformation(extent={{98,78},{118,98}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Echelon echelon(
    hauteur=54000000,
    offset=-540000000,
    startTime=500)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-24,74})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante constante(k=
        15000000)
    annotation (Placement(transformation(extent={{-232,84},{-212,104}})));
  NSSS_ThermoSysPro.CONTROL.NSSS_CONTROL_ThermoSysPro nSSS_CONTROL_ThermoSysPro
    annotation (Placement(transformation(extent={{-160,90},{-116,124}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampe(
    Starttime=500,
    Duration=1500,
    Initialvalue=-540000000,
    Finalvalue=-270000000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-24,102})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampe1(
    Starttime=500,
    Duration=600,
    Initialvalue=-540000000,
    Finalvalue=-270000000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-24,132})));
equation
  connect(nSSS_basemodel.thermalPort, heatSource2.C) annotation (Line(points={{13.5902,
          -5.86667},{56,-5.86667},{56,20},{66.42,20}},     color={0,0,0}));
  connect(adaptorModelicaTSP.outputReal,feedback. u2)
    annotation (Line(points={{65,48},{68,48},{68,72.6}},     color={0,0,255}));
  connect(feedback.y, pI.u)
    annotation (Line(points={{83.4,88},{97,88}}, color={0,0,255}));
  connect(realExpression.y, adaptorModelicaTSP.u) annotation (Line(points={{19,38},
          {30,38},{30,48},{42,48}},     color={0,0,127}));
  connect(pI.y, heatSource2.ISignal) annotation (Line(points={{119,88},{138,88},
          {138,20},{97.5,20}}, color={0,0,255}));
  connect(constante.y,nSSS_CONTROL_ThermoSysPro. SetpointPressure) annotation (
      Line(points={{-211,94},{-187,94},{-187,96.8},{-162.2,96.8}}, color={0,0,
          255}));
  connect(nSSS_CONTROL_ThermoSysPro.Heaters, nSSS_basemodel.Heaters)
    annotation (Line(points={{-113.8,106.32},{-94,106.32},{-94,31.8095},{
          -88.0293,31.8095}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.VelocityR, nSSS_basemodel.VelocityR)
    annotation (Line(points={{-113.8,119.24},{-104,119.24},{-104,25.2571},{
          -88.0293,25.2571}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Sprayers, nSSS_basemodel.Sprayers)
    annotation (Line(points={{-113.8,98.16},{-88.0293,98.16},{-88.0293,37.9524}},
        color={0,0,255}));
  connect(nSSS_basemodel.MeasuredPressure, nSSS_CONTROL_ThermoSysPro.MeasuredPressure)
    annotation (Line(points={{-58.1171,30.581},{-54,30.581},{-54,140},{-194,140},
          {-194,103.6},{-162.2,103.6}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Tout, nSSS_basemodel.Tout_Output)
    annotation (Line(points={{-162.2,113.8},{-176,113.8},{-176,8.05714},{
          -74.0976,8.05714}}, color={0,0,255}));
  connect(nSSS_CONTROL_ThermoSysPro.Tin, nSSS_basemodel.Tin_output) annotation (
     Line(points={{-162.2,120.6},{-186,120.6},{-186,-26.7524},{-74.0976,
          -26.7524}}, color={0,0,255}));
  connect(rampe1.y, feedback.u1) annotation (Line(points={{-13,132},{52.6,132},
          {52.6,88}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end NSSS_benchmark_ThermoSysPro;
