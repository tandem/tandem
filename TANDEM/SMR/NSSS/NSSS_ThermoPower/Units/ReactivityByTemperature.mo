within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type ReactivityByTemperature = Real (final quantity="ReactivityByTemperature",
      final unit="1/K",  displayUnit="pcm/K");
