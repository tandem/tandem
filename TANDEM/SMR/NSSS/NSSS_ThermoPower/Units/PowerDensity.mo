within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type PowerDensity = Real (final quantity="PowerDensity", final unit="W/m3");
