within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type Reactivity = Real (final quantity="Reactivity", final unit="1");
