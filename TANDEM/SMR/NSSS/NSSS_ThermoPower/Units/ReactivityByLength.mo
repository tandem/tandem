within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type ReactivityByLength = Real (final quantity="ReactivityByLength",
     final unit = "1/m",final displayUnit="pcm/m");
