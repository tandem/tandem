within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type Reactivity_pcm = Real (quantity="Reactivity",  unit= "1", displayUnit="pcm");
