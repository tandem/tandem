within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type GlobalHeatTransferCoefficient = Real (final quantity = "CoefficientOfHeatTransfer", final unit = "W/K", displayUnit = "MW/K");
