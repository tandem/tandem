within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type ReactivityByDensity = Real (final quantity="ReactivityByDensity",
     final unit="1/(kg/m3)", displayUnit="pcm/(kg/m3)");
