within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
package Conversion
  function Reactivity_to_pcm
    extends Modelica.Units.Icons.Conversion;
    input NSSS_ThermoPower.Units.Reactivity unity "unit";
    output NSSS_ThermoPower.Units.Reactivity_pcm pcm "pcm";
  algorithm
    pcm := unity*1e5;
    annotation (Inline=true,Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,
              -100},{100,100}}), graphics={Text(
            extent={{8,100},{-100,58}},
            lineColor={0,0,0},
            textString="1"), Text(
            extent={{100,-56},{-16,-100}},
            lineColor={0,0,0},
            textString="pcm")}));
  end Reactivity_to_pcm;

  function Reactivity_to_unit
    extends Modelica.Units.Icons.Conversion;
    output NSSS_ThermoPower.Units.Reactivity unity "unit";
    input NSSS_ThermoPower.Units.Reactivity_pcm pcm "pcm";
  algorithm
    unity := pcm*1e-5;
    annotation (Inline=true,Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,
              -100},{100,100}}), graphics={Text(
            extent={{8,100},{-100,58}},
            lineColor={0,0,0},
            textString="1"), Text(
            extent={{100,-56},{-16,-100}},
            lineColor={0,0,0},
            textString="pcm")}));
  end Reactivity_to_unit;
end Conversion;
