within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type PowerLinearDensity = Real (final quantity="PowerDensity", final unit=
        "W/m");
