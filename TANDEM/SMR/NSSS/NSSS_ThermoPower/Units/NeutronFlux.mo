within TANDEM.SMR.NSSS.NSSS_ThermoPower.Units;
type NeutronFlux = Real (final quantity="NeutronFlux", final unit="1/(m2.s)",displayUnit = "1/(cm2.s)");
