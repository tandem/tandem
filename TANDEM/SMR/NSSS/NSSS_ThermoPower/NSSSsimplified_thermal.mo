within TANDEM.SMR.NSSS.NSSS_ThermoPower;
model NSSSsimplified_thermal
  extends TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.Thermal_interface;

  TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Pressurizer.PressEq pressurizer(
    Geometry=Geometry,
    D=Dpress,
    H=Hpress,
    Hemi=Hemi,
    hsp=hsp,
    Kvlv=Kv,
    pvlv=pvlv,
    pstart=pstart_press,
    ystart=ystart_press,
    hstart=hstart_press)
    annotation (Placement(transformation(extent={{-92,30},{-70,52}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.Core core(
    Nax=Nax,
    Nrad=Nrad,
    Pnom=Pnom,
    wnom=wnom,
    pnom=pnom,
    Tin_nom=Tin_nom,
    Tout_nom=Tout_nom,
    core_dp=core_dp,
    dpnom=dpnom,
    Kfnom=Kfnom,
    rhonom=rhonom,
    Cfnom=Cfnom,
    e=e,
    Nch=Nch,
    Nf=Nf,
    Np=0,
    H=H,
    Ha=Ha,
    He=He,
    pitch=pitch,
    Rf=Rf,
    Rci=Rci,
    Rco=Rco,
    KP=KP,
    SigmaF=SigmaF,
    wF=wF,
    nu=nu,
    rho0=rho0,
    TemperatureFeedbacks=TemperatureFeedbacks,
    alfa_D=alfa_D,
    alfa_C=alfa_C,
    T0_Doppler=T0_Doppler,
    T0_Coolant=T0_Coolant,
    XenonFeedback=XenonFeedback,
    gammaI=gammaI,
    lambdaI=lambdaI,
    gammaXe=gammaXe,
    lambdaXe=lambdaXe,
    sigmaXe=sigmaXe,
    ReactivityInsertion=ReactivityInsertion,
    alfa_H=alfa_H,
    h_0=h_0,
    A_CR=A_CR,
    B_CR=B_CR,
    C_CR=C_CR,
    D_CR=D_CR,
    UniformPower=UniformPower,
    dynamicPromptNeutrons=dynamicPromptNeutrons,
    NeutronSource=NeutronSource,
    hin_start=hin_start,
    hout_start=hout_start,
    steadyState=steadyState,
    initFuel=initFuel,
    Tf_start=Tf_start,
    Tc_start=Tc_start,
    Nstart=Nstart,
    Dstart=Dstart)
    annotation (Placement(transformation(extent={{-102,-76},{-58,-32}})));
  ThermoPower.Water.Flow1DFV Downcomer(
    N=Nax_rd + 1,                                                   Nt = 1,
    L=L_d,
    H=-L_d,
    A=A_d,
    omega=omega_d,
    Dhyd=Dhyd_d,
    wnom=wnom,
    pstart=pstart_press + dpnom,
    hstartin=hin_start,
    hstartout=hin_start,                                                                                                                                                                                                        noInitialPressure = false, fixedMassFlowSimplified = true) annotation (
    Placement(transformation(extent = {{-12, -12}, {12, 12}}, rotation = 180, origin = {-2, -82})));
  ThermoPower.Water.Flow1DFV Riser(
    N=Nax_rd + 1,                                               Nt = 1,
    L=L_r,
    H=L_r,
    A=A_r,
    omega=omega_r,
    Dhyd=Dhyd_r,                                                                                                                                                                                    wnom = wnom, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstartin=hout_start,
    hstartout=hout_start,                                                                                                                                                                                                        noInitialPressure = true, fixedMassFlowSimplified = true) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 90, origin = {-80, -2})));
  ThermoPower.Water.Mixer UpperPlenum(
    V=V_up,                                                  FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstart=hout_start,                                                                                                                                                                  noInitialPressure = false,
    noInitialEnthalpy=false)                                                                                                                                                                                                         annotation (
    Placement(transformation(extent = {{-24, 8}, {-4, 28}})));
  ThermoPower.Water.Header LowerPlenum(
    V=V_lp,
    H=H_lp,                                                                          FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press + dpnom,
    hstart=hin_start,                                                                                                                                                                                                       noInitialPressure = false, noInitialEnthalpy = false) annotation (
    Placement(transformation(extent = {{-9, -9}, {9, 9}}, rotation = 180, origin = {-33, -83})));
  ThermoPower.Water.SensT1 sensT1_1 annotation (
    Placement(transformation(extent = {{-10, 10}, {10, -10}}, rotation = 90, origin = {-76, -24})));
  ThermoPower.Water.SensT1 sensT1_2 annotation (
    Placement(transformation(extent = {{8, 8}, {-8, -8}}, rotation = 180, origin = {-58, -80})));
  ThermoPower.Water.SensT1 sensT1_3 annotation (
    Placement(transformation(extent={{-7,-7},{7,7}},      rotation = 0, origin={59,21})));
  ThermoPower.Water.SensT1 sensT1_4 annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 0, origin = {66, -78})));
  ThermoPower.Water.Pump pump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom=q_nom,
          head_nom=head_nom),
    redeclare function efficiencyCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.constantEfficiency (eta_nom=
            eta),
    CheckValve=CheckValve,
    Np0=Np0,
    V=V,                                                                                                                                                                                                        allowFlowReversal = false,
    dp0=dp0,
    hstart=hstart_pump,
    n0=n0,                                                                                                                                                                                                        noInitialPressure = noInitialPressure_pump,
    rho0(displayUnit="kg/m3") = rho_pump,                                                                                                                                                                                                        use_in_n = true,
    w0=w0)                                                                                                                                                                                                         annotation (
    Placement(transformation(extent={{52,-98},{26,-72}})));
  Modelica.Blocks.Math.Add add annotation (
    Placement(transformation(extent = {{114, 76}, {106, 84}})));
  Modelica.Blocks.Math.Gain Tavg(k = 1/2) annotation (
    Placement(transformation(extent = {{98, 76}, {90, 84}})));
  Modelica.Blocks.Sources.RealExpression Tout(y = sensT1_1.T) annotation (
    Placement(transformation(extent = {{146, 80}, {126, 100}})));
  Modelica.Blocks.Sources.RealExpression Tin(y = sensT1_2.T) annotation (
    Placement(transformation(extent = {{146, 60}, {126, 80}})));
  ThermoPower.Water.Flow1DFV Primary(
    FFtype=Primary_dp,
    Cfnom=Cfnom1,
    Kfnom=Kfnom1,
    rhonom=rhonom1,
    A=a_sg*b_sg,
    Dhyd=4*a_sg*b_sg/(2*(a_sg + b_sg)),
    H=-L_sg,
    L=L_sg,
    N=Nax_sg + 1,
    Nt=integer(N_sg*Nch_sg/2),
    dpnom(displayUnit="bar") = dp1,
    e=e1,
    hstartin=core.hout_start,
    hstartout=core.hin_start,
    noInitialPressure=false,
    omega=2*a_sg,
    pstart=p1,
    wnom=mdot1,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter (heating=false))
                             annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=270,
        origin={100,-8})));
  ThermoPower.Water.PressDropLin pressDropLin(R=1e3) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-38,24})));
  ThermoPower.Thermal.CounterCurrentFV counterCurrentFV(Nw=Nax_sg, redeclare
      model HeatExchangerTopology =
        ThermoPower.Thermal.HeatExchangerTopologies.ShellAndTube (
        Nw=Nodes,
        Ntp=1,
        inletTubeAtTop=false,
        inletShellAtTop=true))                                    annotation (
      Placement(transformation(
        extent={{-10.5,-11.5},{10.5,11.5}},
        rotation=90,
        origin={120.5,-7.5})));
  ThermoPower.Thermal.MetalWallFV metalWallFV(
    Nw=Nax_sg,
    M=N_sg*Nch_sg*L_sg*((a_sg + t_sg/2)*(b_sg + t_sg/2) - a_sg*b_sg)*rho_sg,
    cm=cp_sg,
    WallRes=true,
    UA_ext=N_sg*k_sg/t_sg*2*a_sg*L_sg*Nch_sg,
    Tstartbar=473.15)
                 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={136,-8})));
equation
  connect(Riser.infl, core.flangeB) annotation (
    Line(points = {{-80, -12}, {-80, -32.44}, {-79.56, -32.44}}, color = {0, 0, 255}));
  connect(Riser.outfl, UpperPlenum.in2) annotation (
    Line(points = {{-80, 8}, {-80, 12}, {-22, 12}}, color = {0, 0, 255}));
  connect(Riser.infl, sensT1_1.flange) annotation (
    Line(points = {{-80, -12}, {-80, -24}}, color = {0, 0, 255}));
  connect(sensT1_1.flange, core.flangeB) annotation (
    Line(points = {{-80, -24}, {-80, -32.44}, {-79.56, -32.44}}, color = {0, 0, 255}));
  connect(core.flangeA, sensT1_2.flange) annotation (
    Line(points = {{-79.56, -76.44}, {-79.56, -83.2}, {-58, -83.2}}, color = {0, 0, 255}));
  connect(sensT1_2.flange, LowerPlenum.outlet) annotation (
    Line(points = {{-58, -83.2}, {-58, -83}, {-42, -83}}, color = {0, 0, 255}));
  connect(LowerPlenum.inlet, Downcomer.outfl) annotation (
    Line(points = {{-23.91, -83}, {-14, -83}, {-14, -82}}, color = {0, 0, 255}));
  connect(sensorBus.Tout_SG, sensT1_4.T) annotation (
    Line(points = {{80, 98}, {80, -72}, {74, -72}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tin_SG, sensT1_3.T) annotation (
    Line(points={{80,98},{80,25.2},{64.6,25.2}},        color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.pressure, pressurizer.Pressure) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, 47.38}, {-71.76, 47.38}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tout_core, sensT1_1.T) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -16}, {-70, -16}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Reactivity, core.Reactivity) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -42}, {-59.76, -42}, {-59.76, -41.68}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tin_core, sensT1_2.T) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -76}, {-51.6, -76}, {-51.6, -75.2}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Power, core.Power) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -50.04}, {-59.76, -50.04}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tc_max, core.Tc_max) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -58.84}, {-59.76, -58.84}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tf_max, core.Tf_max) annotation (
    Line(points = {{80, 98}, {80, 80}, {-52, 80}, {-52, -68}, {-59.76, -68}, {-59.76, -67.64}}, color = {255, 219, 88}, thickness = 0.5));
  connect(actuatorBus.heaterPress, pressurizer.heaters) annotation (
    Line(points = {{-80, 98}, {-80, 72}, {-112, 72}, {-112, 46.94}, {-90.68, 46.94}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.sprayPress, pressurizer.sprayers) annotation (
    Line(points = {{-80, 98}, {-80, 72}, {-112, 72}, {-112, 41.55}, {-90.57, 41.55}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.thetaPress, pressurizer.valve) annotation (
    Line(points = {{-80, 98}, {-80, 72}, {-112, 72}, {-112, 35.72}, {-90.68, 35.72}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.rhoCR, core.rhoCR) annotation (
    Line(points = {{-80, 98}, {-80, 72}, {-112, 72}, {-112, -43.88}, {-99.36, -43.88}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.S, core.S) annotation (
    Line(points = {{-80, 98}, {-80, 72}, {-112, 72}, {-112, -54.44}, {-99.8, -54.44}}, color = {80, 200, 120}, thickness = 0.5));
  connect(Downcomer.infl, pump.outfl) annotation (
    Line(points={{10,-82},{20,-82},{20,-75.9},{31.2,-75.9}},          color = {0, 0, 255}));
  connect(actuatorBus.n_pump, pump.in_n) annotation (
    Line(points={{-80,98},{-80,72},{42.38,72},{42.38,-74.6}},          color = {80, 200, 120}, thickness = 0.5));
  connect(pump.infl, sensT1_4.flange) annotation (
    Line(points={{49.4,-82.4},{56,-82.4},{56,-82},{66,-82}},          color = {0, 0, 255}));
  connect(add.y, Tavg.u) annotation (
    Line(points = {{105.6, 80}, {98.8, 80}}, color = {0, 0, 127}));
  connect(Tin.y, add.u2) annotation (
    Line(points = {{125, 70}, {118, 70}, {118, 77.6}, {114.8, 77.6}}, color = {0, 0, 127}));
  connect(Tout.y, add.u1) annotation (
    Line(points = {{125, 90}, {118, 90}, {118, 82.4}, {114.8, 82.4}}, color = {0, 0, 127}));
  connect(sensorBus.Tavg, Tavg.y) annotation (
    Line(points = {{80, 98}, {80, 80}, {89.6, 80}}, color = {255, 219, 88}, thickness = 0.5));
  connect(UpperPlenum.out, sensT1_3.flange) annotation (Line(points={{-4,18},{58,
          18},{58,18.2},{59,18.2}}, color={0,0,255}));
  connect(Primary.infl, UpperPlenum.out)
    annotation (Line(points={{100,6},{100,18},{-4,18}},  color={0,0,255}));
  connect(Primary.outfl, pump.infl) annotation (Line(points={{100,-22},{100,-82},
          {50,-82},{50,-82.4},{49.4,-82.4}},
                                color={0,0,255}));
  connect(UpperPlenum.in1, pressDropLin.outlet)
    annotation (Line(points={{-22,24},{-32,24}}, color={0,0,255}));
  connect(pressDropLin.inlet, pressurizer.bottomFlange)
    annotation (Line(points={{-44,24},{-81,24},{-81,30.22}}, color={0,0,255}));
  connect(Primary.wall,counterCurrentFV. side1) annotation (Line(points={{107,-8},
          {108,-7.5},{117.05,-7.5}},                         color={255,127,0}));
  connect(counterCurrentFV.side2,metalWallFV. int) annotation (Line(points={{124.065,
          -7.5},{124.065,-8},{133,-8}},       color={255,127,0}));
  connect(metalWallFV.ext, dHTVolumes) annotation (Line(points={{139.1,-8},{160,
          -8},{160,-1}}, color={255,127,0}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-160, -100}, {160, 100}}), graphics={  Rectangle(lineColor = {226, 26, 28}, fillColor = {226, 26, 28}, fillPattern = FillPattern.Solid, lineThickness = 1, extent = {{-160, 100}, {160, -100}}), Rectangle(lineColor = {226, 26, 28}, fillColor={215,215,
              215},                                                                                                                                                                                                        fillPattern
            = FillPattern.Solid,                                                                                                                                                                                                        lineThickness = 1, extent = {{-156, 96}, {156, -96}}),
        Text(
          extent={{-72,50},{66,-44}},
          textColor={226,26,28},
          textString="NSSS")}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-160, -100}, {160, 100}})));
end NSSSsimplified_thermal;
