within TANDEM.SMR.NSSS.NSSS_ThermoPower.Utilities;
model ph_init
  replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
    Modelica.Media.Interfaces.PartialMedium "Medium model"
    annotation(choicesAllMatching = true);
  parameter Medium.Temperature Tstart = 324.792+273.15;
  parameter Medium.AbsolutePressure pstart=150e5;

  final parameter Medium.SpecificEnthalpy hstart =  Medium.specificEnthalpy_pTX(pstart, Tstart, fill(0,0));

  ThermoPower.Water.FlangeA inlet
    annotation (Placement(transformation(extent={{-60,-58},{-40,-38}}),
        iconTransformation(extent={{-60,-58},{-40,-38}})));
  ThermoPower.Water.FlangeB outlet
    annotation (Placement(transformation(extent={{40,-58},{60,-38}}),
        iconTransformation(extent={{40,-58},{60,-38}})));

equation
  inlet.m_flow + outlet.m_flow = 0;
  inlet.h_outflow = inStream(outlet.h_outflow);
  inStream(inlet.h_outflow) = outlet.h_outflow;

  inlet.p = outlet.p;

initial equation
  outlet.h_outflow = hstart;
  inlet.h_outflow = hstart;
  outlet.p = pstart;
  //outlet.m_flow = 3700;

  annotation (Icon(graphics={
        Rectangle(
          extent={{-40,-26},{40,-66}},
          lineColor={0,0,0},
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255}),
        Line(points={{0,14},{0,-26}}, color={0,0,255},
          thickness=1),
        Ellipse(extent={{-40,94},{40,14}},  lineColor={0,0,255},
          lineThickness=1),
                   Text(
          extent={{-38,82},{40,32}},
          lineColor={0,0,0},
          textString="p h")}));
end ph_init;
