within TANDEM.SMR.NSSS.NSSS_ThermoPower;
model FMU_NSSS_ThermoPower
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
    Placement(transformation(extent={{32,72},{52,92}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(
    w0=240,
    p0=4500000,
    use_T=false,
    T=436.15,
    h=694305.44,
    use_in_w0=true,
    use_in_T=false,
    use_in_h=true)                                                                                  annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin={70,-50})));
  ThermoPower.Water.SinkPressure sinkPressure(
    p0=4500000,
    R=1,                                                  use_in_p0=true)
                                                            annotation (
    Placement(transformation(extent={{60,-2},{80,18}})));
  Control.NSSSctrl_ex2 NSSSctrl
    annotation (Placement(transformation(extent={{-22,22},{16,54}})));
  NSSSsimplified_fluid nsss(
    core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom(displayUnit="Pa") = 22280,
    Cfnom=0.0037,
    Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp1=200000,
    Cfnom1=0.004,
    Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp2=40000,
    Cfnom2=0.0086448975,
    rho_sg(displayUnit="kg/m3"),
    eta=0.9,
    q_nom={0,0.85035195,1.51494276},
    head_nom={52.9277496,36.251883,0},
    hstart_pump=1.33804e6,
    dp0=257900,
    htc2(start=15000, fixed=true),
    SG(Secondary(noInitialPressure=false)))
    annotation (Placement(transformation(extent={{-32,-44},{26,-8}})));
  Modelica.Blocks.Interfaces.RealInput mFeedwater(start=240)
    annotation (Placement(transformation(extent={{-128,-20},{-88,20}})));
  Modelica.Blocks.Interfaces.RealInput hFeedwater(start=694302.7328265718)
    annotation (Placement(transformation(extent={{-128,-80},{-88,-40}})));
  Modelica.Blocks.Interfaces.RealInput pSteam(start=45e5)
    annotation (Placement(transformation(extent={{-128,40},{-88,80}})));
  Modelica.Blocks.Interfaces.RealOutput pFeedwater
    annotation (Placement(transformation(extent={{96,-70},{116,-50}})));
  Modelica.Blocks.Interfaces.RealOutput hSteam
    annotation (Placement(transformation(extent={{96,-10},{116,10}})));
  Modelica.Blocks.Interfaces.RealOutput mSteam
    annotation (Placement(transformation(extent={{96,50},{116,70}})));
  Modelica.Blocks.Interfaces.RealOutput NSSSpower annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,106})));
  ThermoPower.PowerPlants.HRSG.Components.StateReader_water steamState
    annotation (Placement(transformation(extent={{38,0},{54,16}})));
  ThermoPower.PowerPlants.HRSG.Components.StateReader_water feedwaterState
    annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=180,
        origin={41,-37})));
equation

  pFeedwater = feedwaterState.p;

  hSteam = steamState.h;

  mSteam = steamState.w;

  NSSSpower = nsss.core.Power;





  connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
      points={{-14.4,22},{-14,22},{-14,8},{-17.5,8},{-17.5,-8.36}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
      points={{8.4,22},{8.4,8},{11.5,8},{11.5,-8.36}},
      color={255,219,88},
      thickness=0.5));
  connect(hFeedwater, sourceMassFlow.in_h) annotation (Line(points={{-108,-60},{
          64.8,-60},{64.8,-55.6}}, color={0,0,127}));
  connect(mFeedwater, sourceMassFlow.in_w0) annotation (Line(points={{-108,0},{-60,
          0},{-60,-80},{74,-80},{74,-55.6}}, color={0,0,127}));
  connect(pSteam, sinkPressure.in_p0)
    annotation (Line(points={{-108,60},{66,60},{66,16.4}}, color={0,0,127}));
  connect(nsss.flangeB, steamState.inlet) annotation (Line(points={{26,-15.2},{30,
          -15.2},{30,-16},{38,-16},{38,8},{41.2,8}}, color={0,0,255}));
  connect(steamState.outlet, sinkPressure.flange)
    annotation (Line(points={{50.8,8},{60,8}}, color={0,0,255}));
  connect(nsss.flangeA, feedwaterState.outlet) annotation (Line(points={{26,-37.16},
          {31.6,-37.16},{31.6,-37},{36.8,-37}}, color={0,0,255}));
  connect(feedwaterState.inlet, sourceMassFlow.flange) annotation (Line(points={
          {45.2,-37},{50,-37},{50,-50},{60,-50}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end FMU_NSSS_ThermoPower;
