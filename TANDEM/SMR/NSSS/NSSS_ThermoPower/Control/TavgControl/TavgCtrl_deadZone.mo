within TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.TavgControl;
model TavgCtrl_deadZone

  parameter Modelica.Units.SI.Temperature Tset = 585.5 "Setpoint core average temperature";
  parameter Modelica.Units.SI.TemperatureDifference Tdz = 0.5 "Dead zone interval";
  parameter Real Kp = 1.75e5 "PI proportional gain";
  parameter Real Ki = 4022 "PI integrator gain";



  Modelica.Blocks.Interfaces.RealInput Tavg
    annotation (Placement(transformation(extent={{-124,-20},{-84,20}})));
  Modelica.Blocks.Math.Add add1(k1=-1)
    annotation (Placement(transformation(extent={{-16,-4},{4,16}})));
  Modelica.Blocks.Sources.Constant const(k=Tset)
    annotation (Placement(transformation(extent={{-60,30},{-40,50}})));
  Modelica.Blocks.Math.Division division
    annotation (Placement(transformation(extent={{42,22},{62,2}})));
  Modelica.Blocks.Sources.Constant const1(k=0)
    annotation (Placement(transformation(extent={{30,-40},{50,-20}})));
  Modelica.Blocks.Interfaces.RealOutput CR
    annotation (Placement(transformation(extent={{136,-10},{156,10}})));
  Modelica.Blocks.Continuous.LimPID PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=Kp,
    Ti=Kp/Ki,
    yMax=1000,
    yMin=-1000)
    annotation (Placement(transformation(extent={{64,-20},{84,-40}})));
  Modelica.Blocks.Nonlinear.DeadZone deadZone(uMax=Tdz)
    annotation (Placement(transformation(extent={{18,0},{30,12}})));
equation
  connect(const.y, add1.u1) annotation (Line(points={{-39,40},{-28,40},{-28,12},
          {-18,12}},color={0,0,127}));
  connect(division.u2, add1.u1) annotation (Line(points={{40,18},{14,18},{14,40},
          {-28,40},{-28,12},{-18,12}},color={0,0,127}));
  connect(add1.u2, Tavg)
    annotation (Line(points={{-18,0},{-104,0}},color={0,0,127}));
  connect(const1.y, PID.u_s)
    annotation (Line(points={{51,-30},{62,-30}}, color={0,0,127}));
  connect(division.y, PID.u_m)
    annotation (Line(points={{63,12},{74,12},{74,-18}}, color={0,0,127}));
  connect(PID.y, CR) annotation (Line(points={{85,-30},{106,-30},{106,0},{146,0}},
        color={0,0,127}));
  connect(add1.y, deadZone.u)
    annotation (Line(points={{5,6},{16.8,6}}, color={0,0,127}));
  connect(deadZone.y, division.u1)
    annotation (Line(points={{30.6,6},{40,6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{140,100}}), graphics={ Rectangle(
          extent={{-100,100},{140,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-80,90},{120,-82}},
          textColor={0,0,0},
          fontName="sans-serif",
          textString="Core average 
temperature 
control")}),                    Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-100,-100},{140,100}})));
end TavgCtrl_deadZone;
