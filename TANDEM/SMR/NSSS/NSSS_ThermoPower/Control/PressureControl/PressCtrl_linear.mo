within TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.PressureControl;
model PressCtrl_linear
  parameter Modelica.Units.SI.Power Qback = 360e3 "Backup heater power";
  parameter Modelica.Units.SI.Power Qprop_max = 240e3 "Proportional heater power";
  parameter Modelica.Units.SI.MassFlowRate msp_max = 4.8 "Sprayers flow rate";

  parameter Modelica.Units.SI.Pressure pnom = 140.96904867735e5 "Pressure setpoiny";
  parameter Modelica.Units.SI.Pressure pBack_low = pnom-1.7e5 "Backup heater lower threshold";
  parameter Modelica.Units.SI.Pressure pBack_up= pnom-1e5 "Backup heater upper threshold";
  parameter Modelica.Units.SI.Pressure pProp_low= pnom-1e5 "Proportional heater lower threshold";
  parameter Modelica.Units.SI.Pressure pProp_up= pnom+1e5 "Proportional heater upper threshold";
  parameter Modelica.Units.SI.Pressure pSp_low= pnom+1.7e5 "Sprayers lower threshold";
  parameter Modelica.Units.SI.Pressure pSp_up= pnom+5.2e5 "Sprayers upper threshold";
  parameter Modelica.Units.SI.Pressure pVlv_low= pnom+5.2e5 "Relief valve lower threshold";
  parameter Modelica.Units.SI.Pressure pVlv_up= pnom+7e5 "Relief valve upper threshold";

  Modelica.Units.SI.Power Qprop;
  Modelica.Units.SI.MassFlowRate msp;

  Modelica.Blocks.Interfaces.RealInput p annotation (Placement(transformation(extent={{-156,
            -20},{-116,20}}), iconTransformation(extent={{-156,-20},{-116,
            20}})));

  Modelica.Blocks.Math.BooleanToReal booleanToReal1(realTrue=1, realFalse=0)
    annotation (Placement(transformation(extent={{-46,50},{-26,70}})));
  Modelica.Blocks.Math.Product product2
    annotation (Placement(transformation(extent={{34,56},{54,76}})));
  Modelica.Blocks.Sources.Constant const1(k=Qback)
    annotation (Placement(transformation(extent={{4,66},{16,78}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis1(
    uLow=-pBack_up,
    uHigh=-pBack_low)
    annotation (Placement(transformation(extent={{-86,50},{-66,70}})));
  Modelica.Blocks.Math.Gain gain1(k=-1)
                                       annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-102,60})));
  Modelica.Blocks.Interfaces.RealOutput Heater
    annotation (Placement(transformation(extent={{132,52},{152,72}}),
        iconTransformation(extent={{132,52},{152,72}})));
  Modelica.Blocks.Sources.RealExpression Qprop_source(y=Qprop)
    annotation (Placement(transformation(extent={{34,30},{54,50}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{110,50},{130,70}})));
  Modelica.Blocks.Interfaces.RealOutput Sprays
    annotation (Placement(transformation(extent={{134,-10},{154,10}}),
        iconTransformation(extent={{134,-10},{154,10}})));
  Modelica.Blocks.Sources.RealExpression spray_source(y=msp)
    annotation (Placement(transformation(extent={{36,-10},{56,10}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal2
    annotation (Placement(transformation(extent={{-46,-70},{-26,-50}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis2(uLow=pVlv_low, uHigh=pVlv_up)
    annotation (Placement(transformation(extent={{-86,-70},{-66,-50}})));
  Modelica.Blocks.Interfaces.RealOutput theta_valve
    annotation (Placement(transformation(extent={{132,-70},{152,-50}}),
        iconTransformation(extent={{132,-70},{152,-50}})));
equation
  if p<=pProp_low then
    Qprop = Qprop_max;
  elseif p>=pProp_up then
    Qprop = 0;
  else
    Qprop = (p-pProp_up)/(pProp_low-pProp_up)*Qprop_max;
  end if;

  if p>=pSp_up then
    msp = msp_max;
  elseif p<=pSp_low then
    msp = 0;
  else
    msp = (p-pSp_low)/(pSp_up-pSp_low)*msp_max;
  end if;

  connect(booleanToReal1.y,product2. u2)
    annotation (Line(points={{-25,60},{32,60}}, color={0,0,127}));
  connect(const1.y,product2. u1) annotation (Line(points={{16.6,72},{32,72}},
                          color={0,0,127}));
  connect(hysteresis1.y,booleanToReal1. u)
    annotation (Line(points={{-65,60},{-48,60}},   color={255,0,255}));
  connect(gain1.y,hysteresis1. u)
    annotation (Line(points={{-95.4,60},{-88,60}}, color={0,0,127}));
  connect(p, gain1.u) annotation (Line(points={{-136,0},{-120,0},{-120,60},
          {-109.2,60}},
                color={0,0,127}));
  connect(product2.y, add.u1) annotation (Line(points={{55,66},{108,66}},
                    color={0,0,127}));
  connect(Qprop_source.y, add.u2) annotation (Line(points={{55,40},{102,40},{102,
          54},{108,54}},
                     color={0,0,127}));
  connect(add.y, Heater)
    annotation (Line(points={{131,60},{136,60},{136,62},{142,62}},
                                                 color={0,0,127}));
  connect(spray_source.y, Sprays)
    annotation (Line(points={{57,0},{144,0}}, color={0,0,127}));
  connect(hysteresis2.y,booleanToReal2. u)
    annotation (Line(points={{-65,-60},{-48,-60}},color={255,0,255}));
  connect(booleanToReal2.y,theta_valve)
    annotation (Line(points={{-25,-60},{142,-60}},color={0,0,127}));
  connect(hysteresis2.u, gain1.u) annotation (Line(points={{-88,-60},{-120,-60},
          {-120,60},{-109.2,60}}, color={0,0,127}));
 annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,
            -100},{140,100}}), graphics={
        Text(
          extent={{-104,-76},{100,-156}},
          textColor={0,0,0},
          textString="Pressure control"), Rectangle(
          extent={{-140,100},{140,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-66,58},{82,-52}},
          textColor={0,0,0},
          fontName="sans-serif",
          textString="Pressurizer 
control")}),                    Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-140,-100},{140,100}})));
end PressCtrl_linear;
