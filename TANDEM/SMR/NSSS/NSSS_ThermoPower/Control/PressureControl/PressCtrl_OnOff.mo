within TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.PressureControl;
model PressCtrl_OnOff
  parameter Modelica.Units.SI.Power Qback = 360e3 "Backup heater power";
  parameter Modelica.Units.SI.Power Qprop_max = 240e3 "Proportional heater power";
  parameter Modelica.Units.SI.MassFlowRate msp_max = 4.8 "Sprayers flow rate";

  parameter Modelica.Units.SI.Pressure pnom = 140.96904867735e5 "Pressure setpoiny";
  parameter Modelica.Units.SI.Pressure pBack_low = pnom-1.7e5 "Backup heater lower threshold";
  parameter Modelica.Units.SI.Pressure pBack_up= pnom-1e5 "Backup heater upper threshold";
  parameter Modelica.Units.SI.Pressure pProp_low= pnom-1e5 "Proportional heater lower threshold";
  parameter Modelica.Units.SI.Pressure pProp_up= pnom+1e5 "Proportional heater upper threshold";
  parameter Modelica.Units.SI.Pressure pSp_low= pnom+1.7e5 "Sprayers lower threshold";
  parameter Modelica.Units.SI.Pressure pSp_up= pnom+5.2e5 "Sprayers upper threshold";
  parameter Modelica.Units.SI.Pressure pVlv_low= pnom+5.2e5 "Relief valve lower threshold";
  parameter Modelica.Units.SI.Pressure pVlv_up= pnom+7e5 "Relief valve upper threshold";

  Modelica.Blocks.Math.BooleanToReal booleanToReal
    annotation (Placement(transformation(extent={{-4,-10},{16,10}})));
  Modelica.Blocks.Math.Product product1
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Blocks.Sources.Constant const(k=msp_max)
    annotation (Placement(transformation(extent={{34,10},{46,22}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis(uLow=pSp_low, uHigh=pSp_up)
    annotation (Placement(transformation(extent={{-56,-10},{-36,10}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal1(realTrue=1, realFalse=0)
    annotation (Placement(transformation(extent={{-16,44},{4,64}})));
  Modelica.Blocks.Math.Product product2
    annotation (Placement(transformation(extent={{64,50},{84,70}})));
  Modelica.Blocks.Sources.Constant const1(k=Qprop_max)
    annotation (Placement(transformation(extent={{34,60},{46,72}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis1(
    uLow=-pProp_up,
    uHigh=-pProp_low,
    pre_y_start=false)
    annotation (Placement(transformation(extent={{-56,44},{-36,64}})));
  Modelica.Blocks.Math.Gain gain1(k=-1)
                                       annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-72,54})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal2
    annotation (Placement(transformation(extent={{-4,-70},{16,-50}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis2(uLow=pVlv_low, uHigh=pVlv_up)
    annotation (Placement(transformation(extent={{-54,-70},{-34,-50}})));
  Modelica.Blocks.Interfaces.RealInput p annotation (Placement(transformation(extent={{-168,
            -20},{-128,20}})));
  Modelica.Blocks.Interfaces.RealOutput Heater
    annotation (Placement(transformation(extent={{136,50},{156,70}})));
  Modelica.Blocks.Interfaces.RealOutput Sprays
    annotation (Placement(transformation(extent={{136,-10},{156,10}})));
  Modelica.Blocks.Interfaces.RealOutput theta_valve
    annotation (Placement(transformation(extent={{138,-70},{158,-50}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal3(realTrue=1, realFalse=0)
    annotation (Placement(transformation(extent={{-16,82},{4,102}})));
  Modelica.Blocks.Math.Product product3
    annotation (Placement(transformation(extent={{64,88},{84,108}})));
  Modelica.Blocks.Sources.Constant const2(k=Qback)
    annotation (Placement(transformation(extent={{34,98},{46,110}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis3(uLow=-pBack_up, uHigh=-
        pBack_low)
    annotation (Placement(transformation(extent={{-56,84},{-36,104}})));
  Modelica.Blocks.Math.Gain gain2(k=-1)
                                       annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-72,92})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{104,68},{124,88}})));
equation
  connect(booleanToReal.y, product1.u2)
    annotation (Line(points={{17,0},{38,0},{38,-6},{58,-6}},
                                              color={0,0,127}));
  connect(const.y, product1.u1)
    annotation (Line(points={{46.6,16},{54,16},{54,6},{58,6}},color={0,0,127}));
  connect(hysteresis.y, booleanToReal.u)
    annotation (Line(points={{-35,0},{-6,0}},    color={255,0,255}));
  connect(booleanToReal1.y, product2.u2)
    annotation (Line(points={{5,54},{62,54}},   color={0,0,127}));
  connect(const1.y, product2.u1) annotation (Line(points={{46.6,66},{62,66}},
                          color={0,0,127}));
  connect(hysteresis1.y, booleanToReal1.u)
    annotation (Line(points={{-35,54},{-18,54}},   color={255,0,255}));
  connect(gain1.y, hysteresis1.u)
    annotation (Line(points={{-65.4,54},{-58,54}}, color={0,0,127}));
  connect(gain1.u, hysteresis.u) annotation (Line(points={{-79.2,54},{-86,54},
          {-86,0},{-58,0}},    color={0,0,127}));
  connect(hysteresis2.y, booleanToReal2.u)
    annotation (Line(points={{-33,-60},{-6,-60}}, color={255,0,255}));
  connect(p, hysteresis.u)
    annotation (Line(points={{-148,0},{-58,0}}, color={0,0,127}));
  connect(hysteresis2.u, hysteresis.u) annotation (Line(points={{-56,-60},{
          -86,-60},{-86,0},{-58,0}}, color={0,0,127}));
  connect(product1.y, Sprays)
    annotation (Line(points={{81,0},{146,0}}, color={0,0,127}));
  connect(booleanToReal2.y, theta_valve)
    annotation (Line(points={{17,-60},{148,-60}}, color={0,0,127}));
  connect(p, gain1.u) annotation (Line(points={{-148,0},{-86,0},{-86,54},{
          -79.2,54}},
                color={0,0,127}));
  connect(booleanToReal2.y,theta_valve)
    annotation (Line(points={{17,-60},{148,-60}}, color={0,0,127}));
  connect(booleanToReal3.y,product3. u2)
    annotation (Line(points={{5,92},{62,92}},   color={0,0,127}));
  connect(const2.y,product3. u1) annotation (Line(points={{46.6,104},{62,104}},
                          color={0,0,127}));
  connect(hysteresis3.y,booleanToReal3. u)
    annotation (Line(points={{-35,94},{-26,94},{-26,92},{-18,92}},
                                                   color={255,0,255}));
  connect(gain2.y,hysteresis3. u)
    annotation (Line(points={{-65.4,92},{-62,92},{-62,94},{-58,94}},
                                                   color={0,0,127}));
  connect(gain2.u, hysteresis.u) annotation (Line(points={{-79.2,92},{-86,92},
          {-86,0},{-58,0}}, color={0,0,127}));
  connect(product2.y, add.u2) annotation (Line(points={{85,60},{92,60},{92,72},
          {102,72}}, color={0,0,127}));
  connect(product3.y, add.u1) annotation (Line(points={{85,98},{92,98},{92,84},
          {102,84}}, color={0,0,127}));
  connect(add.y, Heater) annotation (Line(points={{125,78},{128,78},{128,60},
          {146,60}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,
            -100},{140,100}}), graphics={ Rectangle(
          extent={{-140,100},{140,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-74,60},{74,-50}},
          textColor={0,0,0},
          fontName="sans-serif",
          textString="Pressurizer 
control")}),                    Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-140,-100},{140,100}})));
end PressCtrl_OnOff;
