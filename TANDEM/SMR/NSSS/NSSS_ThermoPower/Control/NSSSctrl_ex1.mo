within TANDEM.SMR.NSSS.NSSS_ThermoPower.Control;
model NSSSctrl_ex1

  extends TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.Control_interface;

  // Pressure control parameters
  parameter Modelica.Units.SI.Pressure pset = 150e5 "Setpoint pressure" annotation(Dialog(tab="Pressure control"));
  parameter Modelica.Units.SI.Power Qback = 360e3 "Backup heater power" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.Power Qprop_max = 240e3 "Max proportional heater power" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.MassFlowRate msp_max = 4.8 "Max sprayers flow rate" annotation(Dialog(group="Sprayers",tab="Pressure control"));
  parameter Modelica.Units.SI.SpecificEnthalpy hsp=1.33806e+06 annotation(Dialog(group="Sprayers",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pBack_low = pset-1.7e5 "Lower Qback setpoint" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pBack_up= pset-1e5  "Upper Qback setpoint" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pProp_low= pset-1e5  "Lower Qprop setpoint" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pProp_up= pset+1e5 "Upper Qprop setpoint" annotation(Dialog(group="Heaters",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pSp_low= pset+1.7e5  "Lower sprayers setpoint" annotation(Dialog(group="Sprayers",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pSp_up= pset+5.2e5  "Upper sprayers setpoint" annotation(Dialog(group="Sprayers",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pVlv_low= pset+5.2e5  "Lower relief valve setpoint" annotation(Dialog(group="Relief valve",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pVlv_up= pset+7e5 "Lower relief valve setpoint" annotation(Dialog(group="Relief valve",tab="Pressure control"));
  parameter ThermoPower.Units.HydraulicConductance Kv = 1e3 "Relief valve hydraulic resistance" annotation(Dialog(group="Relief valve",tab="Pressure control"));
  parameter Modelica.Units.SI.Pressure pvlv = 150e5 "Relief valve sink pressure" annotation(Dialog(group="Relief valve",tab="Pressure control"));

  // Average core temperature control parameters
  parameter Modelica.Units.SI.Temperature Tset = 585.5 "Setpoint core average temperature" annotation(Dialog(tab="Average temperature control"));
  parameter Modelica.Units.SI.TemperatureDifference Tdz = 0.5 "Dead zone interval" annotation(Dialog(tab="Average temperature control"));
  parameter Real Kp = 17769.8851 "PI proportional gain" annotation(Dialog(tab="Average temperature control"));
  parameter Real Ki = 1426.8237 "PI integrator gain" annotation(Dialog(tab="Average temperature control"));



  Modelica.Blocks.Sources.Constant const(k=0) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={6,-66})));
  Modelica.Blocks.Sources.Constant const1(k=1500)
                                              annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={6,-34})));
  TavgControl.TavgCtrl_simple Tavg(
    Tset=Tset,
    Tdz=Tdz,
    Kp=Kp,
    Ki=Ki)
    annotation (Placement(transformation(extent={{16,26},{-16,52}})));
  PressureControl.PressCtrl_OnOff linearControl(
    Qback=Qback,
    Qprop_max=Qprop_max,
    msp_max=msp_max,
    pnom=pset,
    pBack_low=pBack_low,
    pBack_up=pBack_up,
    pProp_low=pProp_low,
    pProp_up=pProp_up,
    pSp_low=pSp_low,
    pSp_up=pSp_up,
    pVlv_low=pVlv_low,
    pVlv_up=pVlv_up) annotation (Placement(transformation(
        extent={{-18,-14},{18,14}},
        rotation=180,
        origin={2,2})));
equation
  connect(actuatorBus.S, const.y) annotation (Line(
      points={{-60,-100},{-60,-66},{-5,-66}},
      color={0,197,0},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.n_pump, const1.y) annotation (Line(
      points={{-60,-100},{-60,-34},{-5,-34}},
      color={0,197,0},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(sensorBus.Tavg, Tavg.Tavg) annotation (Line(
      points={{60,-100},{62,-100},{62,39},{16.5333,39}},
      color={255,219,88},
      thickness=0.5));
  connect(actuatorBus.rhoCR, Tavg.CR) annotation (Line(
      points={{-60,-100},{-60,39},{-16.8,39}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.pressure, linearControl.p) annotation (Line(
      points={{60,-100},{62,-100},{62,2},{21.0286,2}},
      color={255,219,88},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(actuatorBus.heaterPress, linearControl.Heater) annotation (Line(
      points={{-60,-100},{-60,-6.4},{-16.7714,-6.4}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.sprayPress, linearControl.Sprays) annotation (Line(
      points={{-60,-100},{-60,2},{-16.7714,2}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.thetaPress, linearControl.theta_valve) annotation (Line(
      points={{-60,-100},{-60,10.4},{-17.0286,10.4}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={102,44,145},
          fillColor={102,44,145},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={102,44,145},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,56},{68,-44}},
          textColor={102,44,145},
          textString="NSSS
control")}),                                                     Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end NSSSctrl_ex1;
