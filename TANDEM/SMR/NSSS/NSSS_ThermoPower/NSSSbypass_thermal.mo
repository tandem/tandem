within TANDEM.SMR.NSSS.NSSS_ThermoPower;
model NSSSbypass_thermal

  extends TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.Thermal_interface;

  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{124,76},{116,84}})));
  Modelica.Blocks.Math.Gain Tavg(k=1/2)
    annotation (Placement(transformation(extent={{108,76},{100,84}})));
  Modelica.Blocks.Sources.RealExpression Tout(y=sensT1_1.T)
    annotation (Placement(transformation(extent={{156,80},{136,100}})));
  Modelica.Blocks.Sources.RealExpression Tin(y=sensT1_2.T)
    annotation (Placement(transformation(extent={{156,60},{136,80}})));
  ThermoPower.Water.Header SGoutlet(
    V=2.730102,
    H=-0.5,
    pstart=p1 - dp1,
    hstart=core.hin_start,
    noInitialPressure=true,
    noInitialEnthalpy=false)
                           annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={110,-42})));
  ThermoPower.Water.Flow1DFV Downcomer(
    N=Nax_rd + 1,
    Nt=1,
    L=L_d,
    H=-L_d,
    A=A_d,
    omega=omega_d,
    Dhyd=Dhyd_d,
    wnom=wnom,
    pstart=pstart_press + core.dpnom,
    hstartin=core.hin_start,
    hstartout=core.hin_start,
    noInitialPressure=false,
    fixedMassFlowSimplified=true)
                            annotation (Placement(transformation(
        extent={{-12,-12},{12,12}},
        rotation=180,
        origin={-2,-70})));
  ThermoPower.Water.Flow1DFV Riser(
    N=Nax_rd + 1,
    Nt=1,
    L=L_r,
    H=L_r,
    A=A_r,
    omega=omega_r,
    Dhyd=Dhyd_r,
    wnom=wnom,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstartin=core.hout_start,
    hstartout=core.hout_start,
    noInitialPressure=true,
    fixedMassFlowSimplified=true)
                            annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-92,10})));
  ThermoPower.Water.Mixer UpperPlenum(
    V=V_up,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstart=core.hout_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)
    annotation (Placement(transformation(extent={{-24,20},{-4,40}})));
  ThermoPower.Water.Header LowerPlenum(
    V=V_lp,
    H=H_lp,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press + core.dpnom,
    hstart=hin_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)                                 annotation (Placement(
        transformation(
        extent={{-9,-9},{9,9}},
        rotation=180,
        origin={-33,-71})));
  ThermoPower.Water.SensT1 sensT1_1 annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-88,-8})));
  ThermoPower.Water.SensT1 sensT1_2 annotation (Placement(transformation(
        extent={{8,8},{-8,-8}},
        rotation=180,
        origin={-58,-68})));
  ThermoPower.Water.SensT1 sensT1_3 annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={51,33})));
  ThermoPower.Water.SensT1 sensT1_4 annotation (Placement(transformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={57,-73})));
  ThermoPower.Water.FlowSplit flowSplit(allowFlowReversal=false)
    annotation (Placement(transformation(extent={{66,22},{80,36}})));
  ThermoPower.Water.PressDrop pressDrop(
    wnom=10,
    Kf=1444764.6139855,
    dpnom=217022.8,
    w(start=10))    annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=270,
        origin={91,1})));
  ThermoPower.Water.PressDropLin pressDropLin(R=1e3) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-40,36})));
  ThermoPower.Water.Flow1DFV SGbypass(
    N=11,
    L=2.5,
    H=-2.5,
    A=0.2889115,
    omega=19.163715,
    Dhyd=0.06030386,
    wnom=10,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom=16327.14449,
    Cfnom=0.02418129,
    pstart=p1 - dp1,
    hstartin=core.hout_start,
    hstartout=core.hout_start,
    noInitialPressure=true,
    fixedMassFlowSimplified=true) annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={92,-36})));
  ThermoPower.Water.FlowJoin flowJoin(allowFlowReversal=false)
                                      annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=180,
        origin={83,-75})));
  Components.Pressurizer.PressEq                                  pressurizer(
    Geometry=Geometry,
    D=Dpress,
    H=Hpress,
    Hemi=Hemi,
    hsp=hsp,
    Kvlv=Kv,
    pvlv=pvlv,
    pstart=pstart_press,
    ystart=ystart_press,
    hstart=hstart_press)
    annotation (Placement(transformation(extent={{-102,44},{-80,66}})));
  Components.Core.Core                                  core(
    Nax=Nax,
    Nrad=Nrad,
    Pnom=Pnom,
    wnom=wnom,
    pnom=pnom,
    Tin_nom=Tin_nom,
    Tout_nom=Tout_nom,
    core_dp=core_dp,
    dpnom=dpnom,
    Kfnom=Kfnom,
    rhonom=rhonom,
    Cfnom=Cfnom,
    e=e,
    Nch=Nch,
    Nf=Nf,
    Np=0,
    H=H,
    Ha=Ha,
    He=He,
    pitch=pitch,
    Rf=Rf,
    Rci=Rci,
    Rco=Rco,
    KP=KP,
    SigmaF=SigmaF,
    wF=wF,
    nu=nu,
    rho0=rho0,
    TemperatureFeedbacks=TemperatureFeedbacks,
    alfa_D=alfa_D,
    alfa_C=alfa_C,
    T0_Doppler=T0_Doppler,
    T0_Coolant=T0_Coolant,
    XenonFeedback=XenonFeedback,
    gammaI=gammaI,
    lambdaI=lambdaI,
    gammaXe=gammaXe,
    lambdaXe=lambdaXe,
    sigmaXe=sigmaXe,
    ReactivityInsertion=ReactivityInsertion,
    alfa_H=alfa_H,
    h_0=h_0,
    A_CR=A_CR,
    B_CR=B_CR,
    C_CR=C_CR,
    D_CR=D_CR,
    UniformPower=UniformPower,
    dynamicPromptNeutrons=dynamicPromptNeutrons,
    NeutronSource=NeutronSource,
    hin_start=hin_start,
    hout_start=hout_start,
    steadyState=steadyState,
    initFuel=initFuel,
    Tf_start=Tf_start,
    Tc_start=Tc_start,
    Nstart=Nstart,
    Dstart=Dstart)
    annotation (Placement(transformation(extent={{-94,-60},{-60,-28}})));
  ThermoPower.Water.Pump pump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom=q_nom,
          head_nom=head_nom),
    redeclare function efficiencyCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.constantEfficiency (eta_nom=
            eta),
    CheckValve=CheckValve,
    Np0=Np0,
    V=V,
    allowFlowReversal=false,
    dp0=dp0,
    hstart=hstart_pump,
    n0=n0,
    noInitialPressure=noInitialPressure_pump,
    rho0(displayUnit="kg/m3") = rho_pump,
    use_in_n=true,
    w0=w0)                                                                                                                                                                                                         annotation (
    Placement(transformation(extent={{50,-92},{24,-66}})));
  ThermoPower.Water.Flow1DFV Primary(
    FFtype=Primary_dp,
    Cfnom=Cfnom1,
    Kfnom=Kfnom1,
    rhonom=rhonom1,
    A=a_sg*b_sg,
    Dhyd=4*a_sg*b_sg/(2*(a_sg + b_sg)),
    H=-L_sg,
    L=L_sg,
    N=Nax_sg + 1,
    Nt=integer(N_sg*Nch_sg/2),
    dpnom(displayUnit="bar") = dp1,
    e=e1,
    hstartin=core.hout_start,
    hstartout=core.hin_start,
    noInitialPressure=false,
    omega=2*a_sg,
    pstart=p1,
    wnom=mdot1 - SGbypass.wnom,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter (heating=false))
                             annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=270,
        origin={110,0})));
  ThermoPower.Thermal.CounterCurrentFV counterCurrentFV(Nw=Nax_sg, redeclare
      model HeatExchangerTopology =
        ThermoPower.Thermal.HeatExchangerTopologies.ShellAndTube (
        Nw=Nodes,
        Ntp=1,
        inletTubeAtTop=false,
        inletShellAtTop=true))                                    annotation (
      Placement(transformation(
        extent={{-10.5,-11.5},{10.5,11.5}},
        rotation=90,
        origin={128.5,0.5})));
  ThermoPower.Thermal.MetalWallFV metalWallFV(
    Nw=Nax_sg,
    M=N_sg*Nch_sg*L_sg*((a_sg + t_sg/2)*(b_sg + t_sg/2) - a_sg*b_sg)*rho_sg,
    cm=cp_sg,
    WallRes=true,
    UA_ext=N_sg*k_sg/t_sg*2*a_sg*L_sg*Nch_sg,
    Tstartbar=473.15)
                 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={144,0})));
  ThermoPower.Water.PressDrop pressDrop1(
    wnom=BP_frac*wnom,
    FFtype=ThermoPower.Choices.PressDrop.FFtypes.Kf,
    Kf=Kf_conc,
    dpnom=dpnom_conc) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-118,-56})));
  ThermoPower.Water.Flow1DFV bypass(
    N=10,
    L=H,
    H=H,
    A=A_bp,
    omega=4*A_bp/Dh_bp,
    Dhyd=Dh_bp,
    wnom=BP_frac*wnom,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom=dpnom_bp,
    Cfnom=Cfnom_bp,
    pstart=pnom,
    hstartin=hin_start,
    hstartout=hin_start,
    initOpt=initOpt,
    noInitialPressure=false,
    fixedMassFlowSimplified=true)   annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-118,-34})));
  ThermoPower.Water.Mixer mixer(
    V=0,
    pstart=pnom,
    hstart=hout_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)
                       annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-92,-20})));
  ThermoPower.Water.FlowSplit flowSplit1(allowFlowReversal=false)
                                        annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=90,
        origin={-93,-67})));
equation

  connect(add.y,Tavg. u)
    annotation (Line(points={{115.6,80},{108.8,80}}, color={0,0,127}));
  connect(Tin.y, add.u2) annotation (Line(points={{135,70},{128,70},{128,77.6},
          {124.8,77.6}}, color={0,0,127}));
  connect(Tout.y, add.u1) annotation (Line(points={{135,90},{128,90},{128,82.4},
          {124.8,82.4}}, color={0,0,127}));
  connect(sensorBus.Tavg, Tavg.y) annotation (Line(
      points={{80,98},{80,80},{99.6,80}},
      color={255,219,88},
      thickness=0.5));
  connect(Riser.outfl,UpperPlenum. in2) annotation (Line(points={{-92,20},{-92,
          28},{-50,28},{-50,24},{-22,24}},
                               color={0,0,255}));
  connect(Riser.infl,sensT1_1. flange)
    annotation (Line(points={{-92,0},{-92,-8}},      color={0,0,255}));
  connect(sensT1_2.flange,LowerPlenum. outlet) annotation (Line(points={{-58,
          -71.2},{-58,-71},{-42,-71}},  color={0,0,255}));
  connect(LowerPlenum.inlet,Downcomer. outfl) annotation (Line(points={{-23.91,
          -71},{-14,-71},{-14,-70}},
                                color={0,0,255}));
  connect(sensorBus.Tout_SG,sensT1_4. T) annotation (Line(
      points={{80,98},{80,90},{66,90},{66,-67.6},{64.2,-67.6}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tin_SG,sensT1_3. T) annotation (Line(
      points={{80,98},{80,90},{56.6,90},{56.6,37.2}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tout_core,sensT1_1. T) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,0},{-82,0}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tin_core,sensT1_2. T) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,-66},{-51.6,-66},{-51.6,-63.2}},
      color={255,219,88},
      thickness=0.5));
  connect(flowSplit.out2,pressDrop. inlet) annotation (Line(points={{77.2,26.2},
          {91,26.2},{91,8}},    color={0,0,255}));
  connect(pressDrop.outlet,SGbypass. infl)
    annotation (Line(points={{91,-6},{92,-6},{92,-28}},      color={0,0,255}));
  connect(pressDropLin.outlet,UpperPlenum. in1)
    annotation (Line(points={{-34,36},{-22,36}}, color={0,0,255}));
  connect(flowJoin.in2,SGbypass. outfl) annotation (Line(points={{87.2,-72.2},{
          92,-72.2},{92,-44}},    color={0,0,255}));
  connect(SGoutlet.outlet, flowJoin.in1) annotation (Line(points={{110,-50},{
          110,-77.8},{87.2,-77.8}},  color={0,0,255}));
  connect(UpperPlenum.out, flowSplit.in1)
    annotation (Line(points={{-4,30},{68.8,30},{68.8,29}}, color={0,0,255}));
  connect(UpperPlenum.out, sensT1_3.flange) annotation (Line(points={{-4,30},{
          52,30},{52,30.2},{51,30.2}},
                                    color={0,0,255}));
  connect(pressurizer.bottomFlange, pressDropLin.inlet)
    annotation (Line(points={{-91,44.22},{-92,44.22},{-92,36},{-46,36}},
                                                             color={0,0,255}));
  connect(actuatorBus.heaterPress, pressurizer.heaters) annotation (Line(
      points={{-80,98},{-80,84},{-108,84},{-108,60.94},{-100.68,60.94}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.sprayPress, pressurizer.sprayers) annotation (Line(
      points={{-80,98},{-80,84},{-108,84},{-108,55.55},{-100.57,55.55}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.thetaPress, pressurizer.valve) annotation (Line(
      points={{-80,98},{-80,84},{-108,84},{-108,49.72},{-100.68,49.72}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.pressure, pressurizer.Pressure) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,60},{-82,60},{-82,61.38},{-81.76,
          61.38}},
      color={255,219,88},
      thickness=0.5));
  connect(actuatorBus.rhoCR, core.rhoCR) annotation (Line(
      points={{-80,98},{-80,84},{-108,84},{-108,-36.64},{-91.96,-36.64}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.S, core.S) annotation (Line(
      points={{-80,98},{-80,84},{-108,84},{-108,-44},{-100,-44},{-100,-44.32},{
          -92.3,-44.32}},
      color={80,200,120},
      thickness=0.5));
  connect(Downcomer.infl, pump.outfl) annotation (Line(points={{10,-70},{20,-70},
          {20,-69.9},{29.2,-69.9}}, color={0,0,255}));
  connect(pump.infl, flowJoin.out) annotation (Line(points={{47.4,-76.4},{48,
          -76.4},{48,-76},{64,-76},{64,-75},{78.8,-75}},
                                color={0,0,255}));
  connect(pump.infl, sensT1_4.flange) annotation (Line(points={{47.4,-76.4},{
          47.4,-76.6},{57,-76.6}},     color={0,0,255}));
  connect(actuatorBus.n_pump, pump.in_n) annotation (Line(
      points={{-80,98},{-80,84},{40.38,84},{40.38,-68.6}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.Reactivity, core.Reactivity) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,-35.04},{-61.36,-35.04}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Power, core.Power) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,-41.12},{-61.36,-41.12}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tc_max, core.Tc_max) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,-47.52},{-61.36,-47.52}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tf_max, core.Tf_max) annotation (Line(
      points={{80,98},{80,90},{-52,90},{-52,-54},{-54,-54},{-54,-53.92},{-61.36,
          -53.92}},
      color={255,219,88},
      thickness=0.5));
  connect(Primary.outfl, SGoutlet.inlet) annotation (Line(points={{110,-14},{
          110,-33.92}},           color={0,0,255}));
  connect(Primary.infl, flowSplit.out1) annotation (Line(points={{110,14},{112,
          14},{112,31.8},{77.2,31.8}}, color={0,0,255}));
  connect(Primary.wall,counterCurrentFV. side1) annotation (Line(points={{117,0},
          {118,0.5},{125.05,0.5}},                           color={255,127,0}));
  connect(counterCurrentFV.side2,metalWallFV. int) annotation (Line(points={{132.065,
          0.5},{132.065,0},{141,0}},          color={255,127,0}));
  connect(metalWallFV.ext, dHTVolumes)
    annotation (Line(points={{147.1,0},{148,-1},{160,-1}}, color={255,127,0}));
  connect(flowSplit1.in1, sensT1_2.flange) annotation (Line(points={{-93,-71.2},
          {-94,-71.2},{-94,-72},{-58,-72},{-58,-71.2}}, color={0,0,255}));
  connect(flowSplit1.out2, core.flangeA) annotation (Line(points={{-90.2,-62.8},
          {-84,-62.8},{-84,-60.32},{-76.66,-60.32}}, color={0,0,255}));
  connect(flowSplit1.out1, pressDrop1.inlet) annotation (Line(points={{-95.8,
          -62.8},{-118,-62.8},{-118,-62}}, color={0,0,255}));
  connect(pressDrop1.outlet, bypass.infl)
    annotation (Line(points={{-118,-50},{-118,-40}}, color={0,0,255}));
  connect(bypass.outfl, mixer.in1) annotation (Line(points={{-118,-28},{-118,
          -24.8},{-95.6,-24.8}}, color={0,0,255}));
  connect(mixer.in2, core.flangeB) annotation (Line(points={{-88.4,-24.8},{
          -76.66,-24.8},{-76.66,-28.32}}, color={0,0,255}));
  connect(mixer.out, sensT1_1.flange)
    annotation (Line(points={{-92,-14},{-92,-8}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -100},{160,100}}),
                         graphics={
        Rectangle(
          extent={{-160,100},{160,-100}},
          lineColor={226,26,28},
          lineThickness=1,
          fillColor={226,26,28},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-156,96},{156,-96}},
          lineColor={226,26,28},
          lineThickness=1,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,46},{72,-48}},
          textColor={226,26,28},
          textString="NSSS")}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-100},{160,
            100}})));
end NSSSbypass_thermal;
