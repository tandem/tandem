within TANDEM.SMR.NSSS.NSSS_ThermoPower;
model NSSSbypass_fluid

  extends TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.Fluid_interface;

  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{124,76},{116,84}})));
  Modelica.Blocks.Math.Gain Tavg(k=1/2)
    annotation (Placement(transformation(extent={{108,76},{100,84}})));
  Modelica.Blocks.Sources.RealExpression Tout(y=sensT1_1.T)
    annotation (Placement(transformation(extent={{156,80},{136,100}})));
  Modelica.Blocks.Sources.RealExpression Tin(y=sensT1_2.T)
    annotation (Placement(transformation(extent={{156,60},{136,80}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.CompactSG SG(
    Nodes=Nax_sg,
    N_SG=N_sg,
    L=L_sg,
    a=a_sg,
    b=b_sg,
    Nch=Nch_sg,
    t=t_sg,
    rho_plate=rho_sg,
    cp_plate=cp_sg,
    k_plate=k_sg,
    mdot1=mdot1,
    p1=p1,
    Primary_dp=Primary_dp,
    dp1=dp1,
    Kfnom1=Kfnom1,
    rhonom1=rhonom1,
    Cfnom1=Cfnom1,
    e1=e1,
    mdot2=mdot2,
    p2=p2,
    Kfnom2=Kfnom2,
    rhonom2=rhonom2,
    Cfnom2=Cfnom2,
    e2=e2,
    redeclare model Primary_HT =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter (heating=false),
    redeclare replaceable model Secondary_HT =
        ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (
          gamma=htc2),
    Secondary_dp=Secondary_dp,
    dp2=dp2,
    hstart_in1=core.hout_start,
    hstart_out1=core.hin_start,
    hstart_in2=694302.7328265718,
    hstart_out2=2944104.064853738,
    Primary(noInitialPressure=false))
    annotation (Placement(transformation(extent={{122,-12},{156,26}})));
  ThermoPower.Water.Header SGoutlet(
    V=2.730102,
    H=-0.5,
    pstart=p1 - dp1,
    hstart=core.hin_start,
    noInitialPressure=true,
    noInitialEnthalpy=false)
                           annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={126,-42})));
  ThermoPower.Water.Flow1DFV Downcomer(
    N=Nax_rd + 1,
    Nt=1,
    L=L_d,
    H=-L_d,
    A=A_d,
    omega=omega_d,
    Dhyd=Dhyd_d,
    wnom=wnom,
    pstart=pstart_press + core.dpnom,
    hstartin=core.hin_start,
    hstartout=core.hin_start,
    noInitialPressure=false,
    fixedMassFlowSimplified=true)
                            annotation (Placement(transformation(
        extent={{-12,-12},{12,12}},
        rotation=180,
        origin={-2,-70})));
  ThermoPower.Water.Flow1DFV Riser(
    N=Nax_rd + 1,
    Nt=1,
    L=L_r,
    H=L_r,
    A=A_r,
    omega=omega_r,
    Dhyd=Dhyd_r,
    wnom=wnom,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstartin=core.hout_start,
    hstartout=core.hout_start,
    noInitialPressure=true,
    fixedMassFlowSimplified=true)
                            annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-76,26})));
  ThermoPower.Water.Mixer UpperPlenum(
    V=V_up,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press,
    hstart=core.hout_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)
    annotation (Placement(transformation(extent={{-22,36},{-2,56}})));
  ThermoPower.Water.Header LowerPlenum(
    V=V_lp,
    H=H_lp,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
    pstart=pstart_press + core.dpnom,
    hstart=hin_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)                                 annotation (Placement(
        transformation(
        extent={{-9,-9},{9,9}},
        rotation=180,
        origin={-33,-71})));
  ThermoPower.Water.SensT1 sensT1_1 annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-72,6})));
  ThermoPower.Water.SensT1 sensT1_2 annotation (Placement(transformation(
        extent={{8,8},{-8,-8}},
        rotation=180,
        origin={-58,-68})));
  ThermoPower.Water.SensT1 sensT1_3 annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={47,49})));
  ThermoPower.Water.SensT1 sensT1_4 annotation (Placement(transformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={73,-69})));
  ThermoPower.Water.FlowSplit flowSplit(allowFlowReversal=false)
    annotation (Placement(transformation(extent={{84,40},{96,52}})));
  ThermoPower.Water.PressDrop pressDrop(
    wnom=10,
    Kf=1444764.6139855,
    dpnom=217022.8,
    w(start=10))    annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=270,
        origin={107,1})));
  ThermoPower.Water.PressDropLin pressDropLin(R=1e3) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-38,52})));
  ThermoPower.Water.Flow1DFV SGbypass(
    N=11,
    L=2.5,
    H=-2.5,
    A=0.2889115,
    omega=19.163715,
    Dhyd=0.06030386,
    wnom=10,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom=16327.14449,
    Cfnom=0.02418129,
    pstart=p1 - dp1,
    hstartin=core.hout_start,
    hstartout=core.hout_start,
    noInitialPressure=true,
    fixedMassFlowSimplified=true) annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={108,-36})));
  ThermoPower.Water.FlowJoin flowJoin(allowFlowReversal=false)
                                      annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=180,
        origin={99,-73})));
  Components.Pressurizer.PressEq                                  pressurizer(
    Geometry=Geometry,
    D=Dpress,
    H=Hpress,
    Hemi=Hemi,
    hsp=hsp,
    Kvlv=Kv,
    pvlv=pvlv,
    pstart=pstart_press,
    ystart=ystart_press,
    hstart=hstart_press)
    annotation (Placement(transformation(extent={{-90,62},{-66,86}})));
  Components.Core.Core                                  core(
    Nax=Nax,
    Nrad=Nrad,
    Pnom=Pnom,
    wnom=wnom,
    pnom=pnom,
    Tin_nom=Tin_nom,
    Tout_nom=Tout_nom,
    core_dp=core_dp,
    dpnom=dpnom,
    Kfnom=Kfnom,
    rhonom=rhonom,
    Cfnom=Cfnom,
    e=e,
    Nch=Nch,
    Nf=Nf,
    Np=0,
    H=H,
    Ha=Ha,
    He=He,
    pitch=pitch,
    Rf=Rf,
    Rci=Rci,
    Rco=Rco,
    KP=KP,
    SigmaF=SigmaF,
    wF=wF,
    nu=nu,
    rho0=rho0,
    TemperatureFeedbacks=TemperatureFeedbacks,
    alfa_D=alfa_D,
    alfa_C=alfa_C,
    T0_Doppler=T0_Doppler,
    T0_Coolant=T0_Coolant,
    XenonFeedback=XenonFeedback,
    gammaI=gammaI,
    lambdaI=lambdaI,
    gammaXe=gammaXe,
    lambdaXe=lambdaXe,
    sigmaXe=sigmaXe,
    ReactivityInsertion=ReactivityInsertion,
    alfa_H=alfa_H,
    h_0=h_0,
    A_CR=A_CR,
    B_CR=B_CR,
    C_CR=C_CR,
    D_CR=D_CR,
    UniformPower=UniformPower,
    dynamicPromptNeutrons=dynamicPromptNeutrons,
    NeutronSource=NeutronSource,
    hin_start=hin_start,
    hout_start=hout_start,
    steadyState=steadyState,
    initFuel=initFuel,
    Tf_start=Tf_start,
    Tc_start=Tc_start,
    Nstart=Nstart,
    Dstart=Dstart)
    annotation (Placement(transformation(extent={{-84,-52},{-46,-16}})));
  ThermoPower.Water.Pump pump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom=q_nom,
          head_nom=head_nom),
    redeclare function efficiencyCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.constantEfficiency (eta_nom=
            eta),
    CheckValve=CheckValve,
    Np0=Np0,
    V=V,
    allowFlowReversal=false,
    dp0=dp0,
    hstart=hstart_pump,
    n0=n0,
    noInitialPressure=noInitialPressure_pump,
    rho0(displayUnit="kg/m3") = rho_pump,
    use_in_n=true,
    w0=w0)                                                                                                                                                                                                         annotation (
    Placement(transformation(extent={{50,-92},{24,-66}})));
  ThermoPower.Water.FlowSplit flowSplit1(allowFlowReversal=false)
                                        annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=90,
        origin={-77,-67})));
  ThermoPower.Water.PressDrop pressDrop1(
    wnom=BP_frac*wnom,
    FFtype=ThermoPower.Choices.PressDrop.FFtypes.Kf,
    Kf=Kf_conc,
    dpnom=dpnom_conc) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-104,-46})));
  ThermoPower.Water.Flow1DFV bypass(
    N=10,
    L=H,
    H=H,
    A=A_bp,
    omega=4*A_bp/Dh_bp,
    Dhyd=Dh_bp,
    wnom=BP_frac*wnom,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom=dpnom_bp,
    Cfnom=Cfnom_bp,
    pstart=pnom,
    hstartin=hin_start,
    hstartout=hin_start,
    initOpt=initOpt,
    noInitialPressure=false,
    fixedMassFlowSimplified=true)   annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-104,-24})));
  ThermoPower.Water.Mixer mixer(
    V=0,
    pstart=pnom,
    hstart=hout_start,
    noInitialPressure=false,
    noInitialEnthalpy=false)
                       annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-76,-8})));
equation

  connect(add.y,Tavg. u)
    annotation (Line(points={{115.6,80},{108.8,80}}, color={0,0,127}));
  connect(Tin.y, add.u2) annotation (Line(points={{135,70},{128,70},{128,77.6},
          {124.8,77.6}}, color={0,0,127}));
  connect(Tout.y, add.u1) annotation (Line(points={{135,90},{128,90},{128,82.4},
          {124.8,82.4}}, color={0,0,127}));
  connect(sensorBus.Tavg, Tavg.y) annotation (Line(
      points={{80,98},{80,80},{99.6,80}},
      color={255,219,88},
      thickness=0.5));
  connect(SG.flangeA1, flangeA) annotation (Line(points={{152.6,-8.54545},{
          152.6,-64},{160,-64},{160,-62}}, color={0,0,255}));
  connect(SG.flangeB1, flangeB) annotation (Line(points={{152.26,19.0909},{
          152.26,30},{160,30},{160,60}}, color={0,0,255}));
  connect(SG.flangeB, SGoutlet.inlet) annotation (Line(points={{125.74,-8.54545},
          {125.74,-26},{126,-26},{126,-33.92}}, color={0,0,255}));
  connect(Riser.outfl,UpperPlenum. in2) annotation (Line(points={{-76,36},{-76,
          40},{-20,40}},       color={0,0,255}));
  connect(sensT1_2.flange,LowerPlenum. outlet) annotation (Line(points={{-58,
          -71.2},{-58,-71},{-42,-71}},  color={0,0,255}));
  connect(LowerPlenum.inlet,Downcomer. outfl) annotation (Line(points={{-23.91,
          -71},{-14,-71},{-14,-70}},
                                color={0,0,255}));
  connect(sensorBus.Tout_SG,sensT1_4. T) annotation (Line(
      points={{80,98},{80,-63.6},{80.2,-63.6}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tin_SG,sensT1_3. T) annotation (Line(
      points={{80,98},{80,80},{52.6,80},{52.6,53.2}},
      color={255,219,88},
      thickness=0.5));
  connect(flowSplit.out2,pressDrop. inlet) annotation (Line(points={{93.6,43.6},
          {107,43.6},{107,8}},  color={0,0,255}));
  connect(pressDrop.outlet,SGbypass. infl)
    annotation (Line(points={{107,-6},{108,-6},{108,-28}},   color={0,0,255}));
  connect(pressDropLin.outlet,UpperPlenum. in1)
    annotation (Line(points={{-32,52},{-20,52}}, color={0,0,255}));
  connect(flowJoin.in2,SGbypass. outfl) annotation (Line(points={{103.2,-70.2},
          {108,-70.2},{108,-44}}, color={0,0,255}));
  connect(flowSplit.out1, SG.flangeA) annotation (Line(points={{93.6,48.4},{116,
          48.4},{116,19.0909},{125.4,19.0909}},
                                        color={0,0,255}));
  connect(SGoutlet.outlet, flowJoin.in1) annotation (Line(points={{126,-50},{
          126,-75.8},{103.2,-75.8}}, color={0,0,255}));
  connect(UpperPlenum.out, flowSplit.in1)
    annotation (Line(points={{-2,46},{86.4,46}},           color={0,0,255}));
  connect(UpperPlenum.out, sensT1_3.flange) annotation (Line(points={{-2,46},{
          48,46},{48,46.2},{47,46.2}},
                                    color={0,0,255}));
  connect(pressurizer.bottomFlange, pressDropLin.inlet)
    annotation (Line(points={{-78,62.24},{-78,52},{-44,52}}, color={0,0,255}));
  connect(Downcomer.infl, pump.outfl) annotation (Line(points={{10,-70},{20,-70},
          {20,-69.9},{29.2,-69.9}}, color={0,0,255}));
  connect(pump.infl, flowJoin.out) annotation (Line(points={{47.4,-76.4},{58,
          -76.4},{58,-73},{94.8,-73}},
                                color={0,0,255}));
  connect(pump.infl, sensT1_4.flange) annotation (Line(points={{47.4,-76.4},{58,
          -76.4},{58,-72.6},{73,-72.6}},
                                       color={0,0,255}));
  connect(pressDrop1.inlet, flowSplit1.out1) annotation (Line(points={{-104,-52},
          {-104,-56},{-80,-56},{-80,-62},{-79.8,-62},{-79.8,-62.8}}, color={0,0,
          255}));
  connect(bypass.infl, pressDrop1.outlet)
    annotation (Line(points={{-104,-30},{-104,-40}}, color={0,0,255}));
  connect(mixer.in1,bypass. outfl)
    annotation (Line(points={{-79.6,-12.8},{-104,-12.8},{-104,-18}},
                                                          color={0,0,255}));
  connect(flowSplit1.in1, sensT1_2.flange)
    annotation (Line(points={{-77,-71.2},{-58,-71.2}}, color={0,0,255}));
  connect(flowSplit1.out2, core.flangeA) annotation (Line(points={{-74.2,-62.8},
          {-74.2,-56},{-64.62,-56},{-64.62,-52.36}}, color={0,0,255}));
  connect(mixer.in2, core.flangeB) annotation (Line(points={{-72.4,-12.8},{
          -64.62,-12.8},{-64.62,-16.36}}, color={0,0,255}));
  connect(mixer.out, sensT1_1.flange)
    annotation (Line(points={{-76,-2},{-76,6}}, color={0,0,255}));
  connect(Riser.infl, sensT1_1.flange)
    annotation (Line(points={{-76,16},{-76,6}}, color={0,0,255}));
  connect(sensorBus.Tout_core, sensT1_1.T) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-66,14}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tin_core, sensT1_2.T) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-42,14},{-42,-63.2},{-51.6,-63.2}},
      color={255,219,88},
      thickness=0.5));

  connect(sensorBus.Reactivity, core.Reactivity) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-42,14},{-42,-23.92},{-47.52,
          -23.92}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Power, core.Power) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-42,14},{-42,-30.76},{-47.52,
          -30.76}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tc_max, core.Tc_max) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-42,14},{-42,-37.96},{-47.52,
          -37.96}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tf_max, core.Tf_max) annotation (Line(
      points={{80,98},{80,80},{20,80},{20,14},{-42,14},{-42,-45.16},{-47.52,
          -45.16}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.pressure, pressurizer.Pressure) annotation (Line(
      points={{80,98},{80,80},{6,80},{6,80.96},{-67.92,80.96}},
      color={255,219,88},
      thickness=0.5));
  connect(actuatorBus.n_pump, pump.in_n) annotation (Line(
      points={{-80,98},{-80,88},{40.38,88},{40.38,-68.6}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.heaterPress, pressurizer.heaters) annotation (Line(
      points={{-80,98},{-80,88},{-98,88},{-98,80.48},{-88.56,80.48}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.sprayPress, pressurizer.sprayers) annotation (Line(
      points={{-80,98},{-80,88},{-98,88},{-98,74},{-88.44,74},{-88.44,74.6}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.thetaPress, pressurizer.valve) annotation (Line(
      points={{-80,98},{-80,88},{-98,88},{-98,68.24},{-88.56,68.24}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.rhoCR, core.rhoCR) annotation (Line(
      points={{-80,98},{-80,88},{-98,88},{-98,-25.72},{-81.72,-25.72}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.S, core.S) annotation (Line(
      points={{-80,98},{-80,88},{-98,88},{-98,-34.36},{-82.1,-34.36}},
      color={80,200,120},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,
            -100},{160,100}}),
                         graphics={
        Rectangle(
          extent={{-160,100},{160,-100}},
          lineColor={226,26,28},
          lineThickness=1,
          fillColor={226,26,28},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-156,96},{156,-96}},
          lineColor={226,26,28},
          lineThickness=1,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-56,50},{82,-44}},
          textColor={226,26,28},
          textString="NSSS")}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-140,-100},{160,
            100}})));
end NSSSbypass_fluid;
