within TANDEM.SMR.NSSS.NSSS_ThermoPower.Test;
model Test_NSSSsimple_fluid

  inner ThermoPower.System system(initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
    Placement(transformation(extent={{28,70},{48,90}})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(w0 = 240,
    p0=4500000,
    use_T=false,
    T=436.15,
    h=694305.44,
    use_in_w0=true)                                                                                 annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin={66,-52})));
  ThermoPower.Water.SinkPressure sinkPressure(p0=4500000)   annotation (
    Placement(transformation(extent = {{48, -4}, {68, 16}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=-0.2*240,
    duration=900,
    offset=240,
    startTime=100)                                             annotation (
    Placement(transformation(extent={{-7,-7},{7,7}},
        rotation=180,
        origin={87,-71})));
  Control.NSSSctrl_ex2 NSSSctrl
    annotation (Placement(transformation(extent={{-26,20},{12,52}})));
  NSSSsimplified_fluid nSSS_fluid(
    core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom(displayUnit="Pa") = 22280,
    Cfnom=0.0037,
    Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp1=200000,
    Cfnom1=0.004,
    Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp2=40000,
    Cfnom2=0.0086448975,
    rho_sg(displayUnit="kg/m3"),
    eta=0.9,
    q_nom={0,0.85035195,1.51494276},
    head_nom={52.9277496,36.251883,0},
    hstart_pump=1.33804e6,
    dp0=257900,
    htc2(start=15000, fixed=true))
    annotation (Placement(transformation(extent={{-36,-46},{22,-10}})));
equation
  connect(NSSSctrl.actuatorBus, nSSS_fluid.actuatorBus) annotation (Line(
      points={{-18.4,20},{-18,20},{-18,6},{-21.5,6},{-21.5,-10.36}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nSSS_fluid.sensorBus) annotation (Line(
      points={{4.4,20},{4.4,6},{7.5,6},{7.5,-10.36}},
      color={255,219,88},
      thickness=0.5));
  connect(nSSS_fluid.flangeB, sinkPressure.flange) annotation (Line(points={{22,
          -17.2},{26,-17.2},{26,-18},{34,-18},{34,0},{48,0},{48,6}}, color={0,0,
          255}));
  connect(nSSS_fluid.flangeA, sourceMassFlow.flange) annotation (Line(points={{22,
          -39.16},{46,-39.16},{46,-52},{56,-52}},                      color={0,
          0,255}));
  connect(sourceMassFlow.in_w0, ramp.y) annotation (Line(points={{70,-57.6},{70,
          -71},{79.3,-71}}, color={0,0,127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false)),
    Diagram(coordinateSystem(preserveAspectRatio = false)));
end Test_NSSSsimple_fluid;
