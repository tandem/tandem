within TANDEM.SMR.NSSS.NSSS_ThermoPower.Test;
model Test_NSSSbypass_thermal

  NSSSbypass_thermal     nsss
    annotation (Placement(transformation(extent={{-62,-34},{-14,6}})));
  ThermoPower.Thermal.HeatSource1DFV heatSource1DFV(Nw=10)   annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin={6,-10})));
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
    Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Math.Gain gain(k=-1) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={28,-10})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=-0.2*540e6,
    duration=900,
    offset=540e6,
    startTime=100)                                                                     annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin={64,-10})));
  Control.NSSSctrl_ex2 NSSSctrl
    annotation (Placement(transformation(extent={{-58,28},{-20,60}})));
equation
  connect(nsss.dHTVolumes, heatSource1DFV.wall) annotation (
    Line(points={{-14,-12.2},{-14,-10},{3,-10}},                                 color = {255, 127, 0}));
  connect(gain.y, heatSource1DFV.power)
    annotation (Line(points={{21.4,-10},{10,-10}}, color={0,0,127}));
  connect(ramp.y, gain.u)
    annotation (Line(points={{53,-10},{35.2,-10}}, color={0,0,127}));
  connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
      points={{-50.4,28},{-50,28},{-50,5.6}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
      points={{-27.6,28},{-26,28},{-26,5.6}},
      color={255,219,88},
      thickness=0.5));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false)),
    Diagram(coordinateSystem(preserveAspectRatio = false)),
    experiment(StopTime=1200, __Dymola_Algorithm="Dassl"));
end Test_NSSSbypass_thermal;
