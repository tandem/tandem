within TANDEM.SMR.NSSS.NSSS_ThermoPower.Components;
package Core
  model Core
    extends TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.Interfaces.Core_int;

    BaseClasses.Fuel fuel(
      Nax=Nax,
      Nf=Nrad,
      Nr=Nf + Np,
      H=Ha,
      He=He,
      Rf=Rf,
      Rci=Rci,
      Rco=Rco,
      redeclare model FuelProperties = FuelMaterial,
      redeclare model GapProperties = GapMaterial,
      redeclare model CladdingProperties = CladdingMaterial,
      UniformPower=UniformPower,
      steadyState=steadyState,
      Tf_start=Tf_start,
      Tc_start=Tc_start)
      annotation (Placement(transformation(extent={{-6,-48},{42,-2}})));
    BaseClasses.NeutronicKinetics neutronicKinetics(
      KP=KP,
      Dstart=Dstart,
      use_Source=NeutronSource,
      P0=Pnom,
      T0_Doppler_ext=T0_Doppler,
      T0_Coolant_ext=T0_Coolant,
      alfa_D=alfa_D,
      alfa_C=alfa_C,
      ReactivityInsertion=ReactivityInsertion,
      alfa_H=alfa_H,
      Rho0=rho0,
      A_CR=A_CR,
      B_CR=B_CR,
      C_CR=C_CR,
      D_CR=D_CR,
      gammaI=gammaI,
      lambdaI=lambdaI,
      gammaXe=gammaXe,
      lambdaXe=lambdaXe,
      sigmaXe=sigmaXe,
      h_0=h_0,
      SigmaF=SigmaF,
      Vfuel=Nf*Ha*pi*Rf^2,
      wF=wF,
      nu=nu,
      dynamicPromptNeutrons=dynamicPromptNeutrons,
      XenonFeedback=XenonFeedback)
      annotation (Placement(transformation(extent={{-2,16},{40,58}})));
    BaseClasses.Flow1DFV_LiquidCoolant moderatorFlow1D(
      redeclare package Medium = ModeratorMaterial,
      FFtype=core_dp,
      dpnom=dpnom,
      Kfnom=Kfnom,
      Cfnom=Cfnom,
      rhonom=rhonom,
      e=e,
      fixedMassFlowSimplified=true,
      redeclare model HeatTransfer =
          ThermoPower.Thermal.HeatTransferFV.DittusBoelter,
      A=pitch^2 - pi*Rco^2,
      Dhyd=2*Rco*(4/pi*(pitch/2/Rco)^2 - 1),
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid,
      H=H,
      L=H,
      N=Nax + 1,
      Nt=Nch,
      hstart=linspace(
          hin_start,
          hout_start,
          moderatorFlow1D.N),
      hstartin=hin_start,
      hstartout=hout_start,
      initOpt=initOpt,
      noInitialPressure=true,
      omega=4*moderatorFlow1D.A/moderatorFlow1D.Dhyd,
      pstart=pnom,
      wnom=wnom) annotation (Placement(transformation(
          extent={{-15,15},{15,-15}},
          rotation=90,
          origin={-37,-25})));
  equation
    connect(neutronicKinetics.fuel, fuel.fission) annotation (
      Line(points={{19,18.1},{19,-2.38333},{18,-2.38333}},
                                                      color = {255, 0, 0}));
    connect(moderatorFlow1D.wall, fuel.coolant) annotation (
      Line(points={{-29.5,-25},{-29.5,-25},{-5.8,-25}},             color = {255, 127, 0}));
    connect(moderatorFlow1D.coolant, neutronicKinetics.coolant) annotation (
      Line(points={{-29.5,-17.5},{-28,-17.5},{-28,18.1},{0.1,18.1}},          color = {0, 0, 255}));
    connect(moderatorFlow1D.infl, flangeA) annotation (
      Line(points={{-37,-40},{-37,-100},{0,-100}},      color = {0, 0, 255}));
    connect(neutronicKinetics.CRin, rhoCR) annotation (
      Line(points={{0.1,51.7},{0.1,48},{-90,48}},        color = {0, 0, 127}));
    connect(moderatorFlow1D.outfl, flangeB) annotation (
      Line(points={{-37,-10},{-37,100},{0,100}},      color = {0, 0, 255}));
    connect(S, neutronicKinetics.S_ext) annotation (
      Line(points={{-92,1.77636e-15},{-50,1.77636e-15},{-50,39.1},{0.1,39.1}},
                                                                    color = {0, 0, 127}));
    connect(fuel.Tc_max, Tc_max) annotation (
      Line(points={{42.8,-17.3333},{60,-17.3333},{60,-20},{90,-20}},     color = {0, 0, 127}));
    connect(fuel.Tf_max, Tf_max) annotation (
      Line(points={{42.4,-28.8333},{66,-28.8333},{66,-60},{90,-60}},     color = {0, 0, 127}));
    connect(neutronicKinetics.Reactivity, Reactivity) annotation (
      Line(points={{41.26,49.6},{62,49.6},{62,58},{90,58}},          color = {0, 0, 127}));
    connect(neutronicKinetics.Power, Power) annotation (
      Line(points={{40.84,24.4},{50,24.4},{50,20},{90,20}},          color = {0, 0, 127}));

      annotation (
      defaultComponentName = "core",
      Icon(coordinateSystem(preserveAspectRatio = false)),
      Diagram(coordinateSystem(preserveAspectRatio = false)));
  end Core;

  package BaseClasses
    model Fuel
      parameter Integer Nax = 10 "Number of axial nodes";
      parameter Integer Nf = 5 "Number of radial nodes in the fuel pelled";
      // Geometry
      parameter Integer Nr = 20064 "Number of fuel rods";
      parameter Modelica.Units.SI.Length H "Active length";
      parameter Modelica.Units.SI.Length He = H "Extrapolated length";
      parameter Modelica.Units.SI.Length Rf "Fuel pellet radius";
      parameter Modelica.Units.SI.Length Rci "Cladding inner radius";
      parameter Modelica.Units.SI.Length Rco "Claddingouter radius";
      replaceable model FuelProperties =
          NSSS_ThermoPower.Components.Core.MaterialProperties.StandardFuel_ESMR
        constrainedby
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                              "Fuel properties" annotation (
         choices(choice = 1 "StandardFuel", choice = 2 "StandardFuelConstant", choice = 3 "StandardFuel_ESMR"));
      replaceable model GapProperties =
          NSSS_ThermoPower.Components.Core.MaterialProperties.Helium
        constrainedby
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                  "Gap material properties" annotation (
         choices(choice = 1 "Helium"));
      replaceable model CladdingProperties =
          NSSS_ThermoPower.Components.Core.MaterialProperties.ZircaloyM5_ESMR
        constrainedby
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                                "Cladding properties" annotation (
         choices(choice = 1 "Zircaloy4", choice = 2 "Zircaloy4Constant", choice = 3 "ZircaloyM5_ESMR"));
      parameter Boolean UniformPower "Axial power profile model" annotation (
        choices(checkBox = true));
        parameter Boolean steadyState "Initialisation option" annotation (
        choices(checkBox = true));
        parameter Modelica.Units.SI.Temperature Tf_start[Nax, Nf] = if steadyState then 1000*ones(Nax, Nf) else
          NSSS_ThermoPower.Data.Core.IC_inputs.uniform_Ax10Rad5.Tf_start "Fuel temperature starting value";
      parameter Modelica.Units.SI.Temperature Tc_start[Nax] = if steadyState then 600*ones(Nax) else
          NSSS_ThermoPower.Data.Core.IC_inputs.uniform_Ax10Rad5.Tc_start "Cladding temperature starting value";


      // Fuel
      FuelProperties fuel[Nax, Nf] "Fuel properties";
      Modelica.Units.SI.Length R[Nf];
      Modelica.Units.SI.Length Rvol[Nf];
      Modelica.Units.SI.Volume Vf;
      Modelica.Units.SI.Volume Vf_tot;
      Modelica.Units.SI.Temperature Tvol[Nax, Nf](start = Tf_start);
      Modelica.Units.SI.Temperature Tf[Nax, Nf](start = Tf_start);
      NSSS_ThermoPower.Units.PowerDensity q3[Nax, Nf];
      Modelica.Units.SI.Power Q[Nax, Nf];
      Modelica.Units.SI.ThermalConductivity kf[Nax, Nf];
      Modelica.Units.SI.Density rhof[Nax, Nf];
      Modelica.Units.SI.SpecificHeatCapacity cpf[Nax, Nf];
      Modelica.Units.SI.Temperature Tmid[Nf];
      Modelica.Units.SI.Temperature Tax[Nax];
      // Gap
      GapProperties gas[Nax];
      Modelica.Units.SI.Power Qg[Nax];
      Modelica.Units.SI.Temperature Tg[Nax](start = (Tf_start[:, Nf] + Tc_start)/2);
      // Cladding
      CladdingProperties clad[Nax] "Cladding properties";
      Modelica.Units.SI.Temperature Tc[Nax](start = Tc_start);
      Modelica.Units.SI.Temperature Tci[Nax](start = Tc_start);
      Modelica.Units.SI.Temperature Tco[Nax](start = Tc_start);
      Modelica.Units.SI.Power Qcool[Nax];
      Modelica.Units.SI.Density rhoc[Nax];
      Modelica.Units.SI.ThermalConductivity kc[Nax];
      Modelica.Units.SI.SpecificHeatCapacity cpc[Nax];
      // Power distribution
      Modelica.Units.SI.Power Qvol[Nax, Nf];
      NSSS_ThermoPower.Units.PowerLinearDensity q1;
      NSSS_ThermoPower.Units.PowerLinearDensity q1_z[Nax];
      Real F[Nax, Nf];
      ThermoPower.Thermal.DHTVolumes coolant(N = Nax) annotation (
        Placement(transformation(extent={{-126,-50},{-112,50}}),     iconTransformation(extent={{-126,
                -50},{-112,50}})));
      Interfaces.Fission fission annotation (
        Placement(transformation(extent={{-10,108},{10,128}}),     iconTransformation(extent={{-10,108},
                {10,128}})));
      Modelica.Blocks.Interfaces.RealOutput Tf_max annotation (
        Placement(transformation(extent = {{96, 38}, {116, 58}}), iconTransformation(extent={{112,-30},
                {132,-10}})));
      Modelica.Blocks.Interfaces.RealOutput Tc_max annotation (
        Placement(transformation(extent = {{96, 10}, {116, 30}}), iconTransformation(extent={{114,30},
                {134,50}})));
    protected
      parameter Modelica.Units.SI.Length z[Nax] = linspace(-H/2, H/2, Nax);
      constant Real pi = Modelica.Constants.pi "3.14159...";
    equation
      Vf_tot = H*pi*Rf^2*Nr;
      Vf = Vf_tot/Nax/Nf/Nr;
      R[1] = sqrt(Vf/(H/Nax*pi));
      Rvol[1] = 0;
      for j in 2:Nf loop
        R[j] = sqrt(R[j - 1]^2 + Vf/(H/Nax*pi));
        Rvol[j] = (R[j] + R[j - 1])/2;
      end for;
      for i in 1:Nax loop
        // Axial discretization
        // Fuel
        for j in 1:Nf loop
          // Radial discretization
          if j == 1 then
            // Energy balance
            rhof[i, j]*cpf[i, j]*der(Tvol[i, j])*Vf = q3[i, j]*Vf - Q[i, j];
          else
            rhof[i, j]*cpf[i, j]*der(Tvol[i, j])*Vf = q3[i, j]*Vf + Q[i, j - 1] - Q[i, j];
          end if;
          if j == Nf then
            // Heat exchanged between control volumes
            Q[i, j] = Qg[i];
            Tvol[i, j] = (Tf[i, j - 1] + Tf[i, j])/2;
            kf[i, j] = fuel[i, j].k;
          elseif j == 1 then
            Q[i, j] = 2*H/Nax*pi*R[j]*kf[i, j]*(Tvol[i, j] - Tvol[i, j + 1])/(Rvol[j + 1] - Rvol[j]);
            // slab
            Tf[i, j] = (Tvol[i, j + 1] + Tvol[i, j])/2;
            kf[i, j] = (fuel[i, j + 1].k + fuel[i, j].k)/2;
          else
            Q[i, j] = 2*H/Nax*pi*R[j]*kf[i, j]*(Tvol[i, j] - Tvol[i, j + 1])/(Rvol[j + 1] - Rvol[j]);
            // slab
            //Q[i,j] = 2*H/Nax*pi*kf[i,j]*(Tvol[i,j]-Tvol[i,j+1])/log(Rvol[j+1]/Rvol[j]); // cylinder
            Tf[i, j] = (Tvol[i, j + 1] + Tvol[i, j])/2;
            kf[i, j] = (fuel[i, j + 1].k + fuel[i, j].k)/2;
          end if;
          fuel[i, j].T = Tf[i, j];
          cpf[i, j] = fuel[i, j].cp;
          rhof[i, j] = fuel[i, j].rho;
        end for;
        // Gap
        Qg[i] = 2*pi*H/Nax*gas[i].k*(Tf[i, Nf] - Tci[i])/log(Rci/R[Nf]);
        Tg[i] = (Tf[i, Nf] + Tci[i])/2;
        gas[i].T = Tg[i];
        // Cladding
        Qg[i] = (kc[i]*(2*pi*H/Nax)*(Tci[i] - Tc[i]))/(log((Rci + Rco)/(2*Rci)));
        Qcool[i] = (kc[i]*(2*pi*H/Nax)*(Tco[i] - Tc[i]))/(log((2*Rco)/(Rci + Rco)));
        rhoc[i]*cpc[i]*der(Tc[i])*H/Nax*pi*(Rco^2 - Rci^2) = Qg[i] + Qcool[i];
        clad[i].T = Tc[i];
        rhoc[i] = clad[i].rho;
        cpc[i] = clad[i].cp;
        kc[i] = clad[i].k;
      end for;
      fission.T_Doppler = (5/9)*sum(Tvol[:, 1])/Nax + (4/9)*sum(Tvol[:, Nf])/Nax;
      coolant.T = Tco;
      coolant.Q = Qcool*Nr;
      q1 = fission.P/H/Nr;
      q1_z = q3[:, 1]*pi*Rf^2;
      for i in 1:Nax loop
        for j in 1:Nf loop
          if UniformPower then
            F[i, j] = 1/Nax/Nf;
          else
             F[i, j] = cos(pi*z[i]/He)/sum(cos(pi*z/He))/Nf;
          end if;
          Qvol[i, j] = fission.P/Nr*F[i, j];
        end for;
      end for;
      q3 = Qvol/Vf;
      // Post processing
      Tmid = Tvol[integer(Nax/2) + 1, :];
      Tax = Tvol[:, 1];
      // BC
      Tf_max = max(Tvol);
      Tc_max = max(Tc);
    initial equation
      if steadyState then
        der(Tvol) = zeros(Nax, Nf);
        der(Tc) = zeros(Nax);
      else
        Tvol = Tf_start;
        Tc = Tc_start;
      end if;
      annotation (
        Icon(coordinateSystem(preserveAspectRatio = false, extent={{-120,-120},
                {120,120}}), graphics={
            Rectangle(
              extent={{-120,120},{120,-120}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillColor={0,0,255},
              fillPattern=FillPattern.Sphere),
            Rectangle(
              extent={{-40,100},{40,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={255,255,255}),
            Rectangle(
              extent={{-100,40},{100,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={255,255,255}),
            Rectangle(
              extent={{-80,60},{80,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={255,255,255}),
            Rectangle(
              extent={{-60,80},{60,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={255,255,255}),
            Rectangle(
              extent={{-20,20},{0,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28}),
            Rectangle(
              extent={{-20,0},{0,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28}),
            Rectangle(
              extent={{0,0},{20,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28}),
            Rectangle(
              extent={{0,20},{20,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28}),
            Rectangle(
              extent={{0,40},{20,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{20,20},{40,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{20,0},{40,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{0,-20},{20,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-20,-20},{0,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-40,0},{-20,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-40,20},{-20,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-20,40},{0,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-60,40},{-40,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-60,-20},{-40,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{40,-20},{60,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{40,40},{60,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-40,60},{-20,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{20,60},{40,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-40,-40},{-20,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{20,-40},{40,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35}),
            Rectangle(
              extent={{-40,40},{-20,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{20,40},{40,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-40,-20},{-20,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{20,-20},{40,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-20,-40},{0,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{0,-40},{20,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-20,60},{0,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{0,60},{20,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-60,20},{-40,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-60,0},{-40,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{40,0},{60,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{40,20},{60,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23}),
            Rectangle(
              extent={{-60,60},{-40,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{-40,80},{-20,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{-40,-60},{-20,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{20,-60},{40,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{60,-20},{80,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{60,40},{80,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{20,80},{40,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{-80,-20},{-60,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{-80,40},{-60,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31}),
            Rectangle(
              extent={{-100,0},{-80,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{80,20},{100,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{-20,-80},{0,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{80,0},{100,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{0,-80},{20,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{-100,20},{-80,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{-20,100},{0,80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{0,100},{20,80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255}),
            Rectangle(
              extent={{-80,20},{-60,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-80,0},{-60,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-20,-60},{0,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{0,-60},{20,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{60,0},{80,-20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{60,20},{80,0}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-20,80},{0,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{0,80},{20,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-80,60},{-60,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-60,80},{-40,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{40,-40},{60,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-60,-40},{-40,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-40,100},{-20,80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{-100,40},{-80,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{-100,-20},{-80,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{-40,-80},{-20,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{20,-80},{40,-100}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{80,-20},{100,-40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{80,40},{100,20}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{20,100},{40,80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{60,60},{80,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{40,80},{60,60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{40,60},{60,40}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184}),
            Rectangle(
              extent={{-80,-40},{-60,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{-60,-60},{-40,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{40,-60},{60,-80}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244}),
            Rectangle(
              extent={{60,-40},{80,-60}},
              lineColor={0,0,0},
              lineThickness=0.5,
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244})}),
        Diagram(coordinateSystem(preserveAspectRatio = false, extent={{-120,
                -120},{120,120}})));
    end Fuel;

    model NeutronicKinetics
      "Point kinetic neutronics with reactivity feedback coefficients"
      extends NSSS_ThermoPower.Components.Core.BaseClasses.KineticsBase;
      import Modelica.Math;
      parameter Modelica.Units.SI.Power P0 "Nominal power";
      parameter Modelica.Units.SI.Temperature T0_Doppler_ext "Nominal Fuel Temperature for Doppler effect evaluation";
      parameter Modelica.Units.SI.Temperature T0_Coolant_ext "Nominal Coolant Temperature";
      parameter NSSS_ThermoPower.Units.ReactivityByTemperature alfa_D "Doppler feedback coefficient";
      parameter NSSS_ThermoPower.Units.ReactivityByTemperature alfa_C "Coolant density feedback coefficient: function of coolant average temperature";
      parameter NSSS_ThermoPower.Units.Reactivity_pcm Rho0 "Reactivity margin";
      parameter NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion ReactivityInsertion=
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.Reactivity "Reactivity insertion model"
        annotation (choicesAllMatching=true);
      parameter NSSS_ThermoPower.Units.ReactivityByLength alfa_H "Coefficient associated with the insertion length of an ideal control rod" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
      parameter Modelica.Units.SI.Length h_0 "Reference position of the ideal control rod" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
      parameter Real A_CR "CR calibration curve coefficient (A*sin(B*h+C)+D" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real B_CR "CR calibration curve coefficient" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real C_CR "CR calibration curve coefficient" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real D_CR "CR calibration curve coefficient" annotation (
        Dialog(enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      Modelica.Units.SI.Power P(start=P0) "Generated power";
      NSSS_ThermoPower.Units.Reactivity_pcm Rho_pcm;
      NSSS_ThermoPower.Units.Reactivity_pcm Doppler_Reactivity;
      NSSS_ThermoPower.Units.Reactivity_pcm Coolant_Density_Reactivity;
      NSSS_ThermoPower.Units.Reactivity_pcm CR_Reactivity;
      // Xe feedback
      parameter Boolean XenonFeedback = true "Xenon poisoning reactivity feedback" annotation (
        choices(checkBox = true));
      parameter Modelica.Units.SI.MacroscopicCrossSection SigmaF = 0.0031089/1e-2 "Macroscopic fission cross section";
      parameter Real gammaI = 0.0639 "Iodine fission yield";
      parameter Modelica.Units.SI.DecayConstant lambdaI = 2.92615E-05 "Iodine decay constant";
      parameter Real gammaXe = 0.00237 "Xenon fission yield";
      parameter Modelica.Units.SI.DecayConstant lambdaXe = 2.10657E-05 "Xenon decay constant";
      parameter Modelica.Units.SI.CrossSection sigmaXe = 2.71e-22 "Xenon absorption cross section";
      parameter Real Nstart_I(unit = "m-3") = SigmaF*phi0*gammaI/lambdaI "Initialisation value for I concentration";
      parameter Real Nstart_Xe(unit = "m-3") = (SigmaF*phi0*gammaXe + lambdaI*Nstart_I)/(lambdaXe + sigmaXe*phi0) "Initialisation value for Xe concentration";
      NSSS_ThermoPower.Units.NeutronFlux phi;
      Real N_Xe(start = Nstart_Xe);
      Real N_I(start = Nstart_I);
      NSSS_ThermoPower.Units.Reactivity_pcm rhoXe;
      Modelica.Blocks.Interfaces.RealInput CRin "Control rods Insertion" annotation (
        Placement(transformation(extent = {{-100, 60}, {-80, 80}}, rotation = 0)));
      NSSS_ThermoPower.Components.Core.Interfaces.Moderation coolant
        "Connector for additional reactivity sources" annotation (Placement(
            transformation(extent={{-100,-100},{-80,-80}}, rotation=0)));
      NSSS_ThermoPower.Components.Core.Interfaces.Fission fuel
        "Connector to fuel rods model" annotation (Placement(transformation(
              extent={{-10,-100},{10,-80}}, rotation=0)));
      Modelica.Blocks.Interfaces.RealOutput Reactivity annotation (
        Placement(transformation(extent = {{98, 52}, {114, 68}}), iconTransformation(extent = {{98, 52}, {114, 68}})));
      Modelica.Blocks.Interfaces.RealOutput Power annotation (
        Placement(transformation(extent = {{96, -68}, {112, -52}}), iconTransformation(extent = {{96, -68}, {112, -52}})));
    protected
      parameter NSSS_ThermoPower.Units.NeutronFlux phi0 = P0/SigmaF/Vfuel/wF;
      parameter Modelica.Units.SI.Temperature T0_Doppler(fixed = false) "Nominal Fuel Temperature for Doppler effect evaluation";
      parameter Modelica.Units.SI.Temperature T0_Coolant(fixed = false) "Nominal Coolant Temperature";
      parameter Real N0_Xe(unit = "m-3", fixed = false) "Equilibrium Xe concentration";
    initial equation
      if initOpt == ThermoPower.Choices.Init.Options.steadyState then
        T0_Doppler = fuel.T_Doppler;
        T0_Coolant = coolant.T;
        der(N_Xe) = 0;
        der(N_I) = 0;
        N0_Xe = N_Xe;
      elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
        T0_Doppler = fuel.T_Doppler;
        T0_Coolant = coolant.T;
        N_Xe = Nstart_Xe;
        N_I = Nstart_I;
        N0_Xe = Nstart_Xe;
      end if;
    equation
      // Xe feedback
      phi = phi0*N/Nstart;
      if XenonFeedback then
        der(N_I) = SigmaF*phi*gammaI - lambdaI*N_I;
        der(N_Xe) = SigmaF*phi*gammaXe + lambdaI*N_I - lambdaXe*N_Xe - sigmaXe*phi*N_Xe;
        rhoXe = -sigmaXe/nu/SigmaF*(N_Xe - N0_Xe)*1e5;
      else
        der(N_I) = 0;
        der(N_Xe) = 0;
        rhoXe = 0;
      end if;
      Doppler_Reactivity = alfa_D*(fuel.T_Doppler - T0_Doppler);
      Coolant_Density_Reactivity = alfa_C*(coolant.T - T0_Coolant);
      if ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration then
        CR_Reactivity = A_CR*sin(B_CR*CRin + C_CR) + D_CR;
      elseif ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional then
        CR_Reactivity = alfa_H*(CRin - h_0);
      else
        CR_Reactivity = CRin;
      end if;
      Rho_pcm = (Doppler_Reactivity + Coolant_Density_Reactivity + CR_Reactivity + Rho0 + rhoXe) "Reactivity of the multiplicant system";
      Rho = NSSS_ThermoPower.Units.Conversion.Reactivity_to_unit(Rho_pcm);
      //Boundary conditions
      P = fuel.P;
      Reactivity = Rho_pcm;
      Power = P;
      annotation (
        extent = [-70, 80; -50, 100],
        rotation = 90,
        Icon(graphics={  Rectangle(extent = {{-60, 80}, {60, 80}}, lineColor = {0, 0, 255}, pattern = LinePattern.None, fillColor = {0, 0, 0}, fillPattern = FillPattern.Solid), Text(extent = {{10, -90}, {-30, -66}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "P"), Rectangle(extent = {{-100, 0}, {-60, -80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Rectangle(extent = {{100, 4}, {60, -100}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Rectangle(extent = {{-80, 20}, {-60, 0}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Rectangle(extent = {{-60, -80}, {60, -100}}, lineColor = {0, 0, 255}, pattern = LinePattern.None, fillColor = {0, 0, 0}, fillPattern = FillPattern.Solid), Rectangle(extent = {{-60, 100}, {60, -80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 127, 0}), Text(extent = {{-62, 54}, {60, -46}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 127, 0}, textString = "Kinetics"), Rectangle(extent = {{-80, -80}, {-60, -100}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Text(extent = {{-60, -100}, {-80, -80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "M"), Text(extent = {{-60, 0}, {-80, 20}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "S"), Rectangle(extent = {{-100, 60}, {-60, 20}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Rectangle(extent = {{-80, 80}, {-60, 60}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Text(extent = {{-60, 60}, {-80, 80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 127, 0}, textString = "h"), Rectangle(extent = {{-100, 100}, {-60, 80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Text(extent = {{30, -100}, {10, -80}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "F"), Text(extent = {{90, -70}, {70, -50}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "P"), Rectangle(extent = {{100, 100}, {60, 2}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {255, 255, 0}), Text(extent = {{94, 48}, {66, 72}}, lineColor = {0, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {0, 0, 191}, textString = "R")}),
        Icon(Rectangle(extent = [-100, 100; -60, 20], style(color = 7, gradient = 2, fillColor = 6)), Rectangle(extent = [-100, -60; -60, -100], style(color = 7, gradient = 2, fillColor = 6)), Rectangle(extent = [100, 100; 60, 10], style(color = 7, gradient = 2, fillColor = 6)), Rectangle(extent = [60, 10; 80, -10], style(color = 7, gradient = 2, fillColor = 6)), Rectangle(extent = [-60, -80; 60, -100], style(pattern = 0, fillColor = 0)), Rectangle(extent = [-60, 100; 60, 80], style(pattern = 0, fillColor = 0)), Rectangle(extent = [-60, 80; 60, -80], style(color = 7, gradient = 2, fillColor = 45)), Rectangle(extent = [-100, 0; -60, -60], style(color = 7, gradient = 2, fillColor = 6)), Text(extent = [-62, 36; 60, -64], style(color = 7, gradient = 2, fillColor = 45), string = "Kinetic"), Text(extent = [36, -14; -74, 90], style(color = 74, gradient = 2, fillColor = 74), string = "7"), Text(extent = [56, 2; -20, 30], style(color = 7, gradient = 2, fillColor = 45), string = "Groups"), Rectangle(extent = [100, -10; 60, -100], style(color = 7, gradient = 2, fillColor = 6)), Rectangle(extent = [-80, 20; -60, 0], style(color = 7, gradient = 2, fillColor = 6)), Text(extent = [-50, -102; -90, -78], string = "M", style(color = 74, gradient = 2, fillColor = 74)), Text(extent = [100, 10; 60, 34], string = "S", style(color = 74, gradient = 2, fillColor = 74)), Text(extent = [60, 76; 20, 100], style(color = 7, gradient = 2, fillColor = 45), string = "h"), Text(extent = [-20, 76; -60, 100], style(color = 7, gradient = 2, fillColor = 45), string = "N")),
        Placement(transformation(origin = {-60, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    end NeutronicKinetics;

    model Flow1DFV_LiquidCoolant "Flow1D model with Coolant Properties connector"
      extends ThermoPower.Water.Flow1DFV;
      NSSS_ThermoPower.Components.Core.Interfaces.Moderation coolant
        annotation (Placement(transformation(extent={{40,40},{60,60}}, rotation=
               0)));
    equation
      coolant.T = sum(T)/N "Average coolant temperature";
      coolant.T_out = T[N] "Outlet coolant temperature";
      coolant.T_in = T[1] "Inlet coolant temperature";
    end Flow1DFV_LiquidCoolant;

    partial model KineticsBase
      "Normalized point-kinectic neutronic dynamics"
      parameter Modelica.Units.SI.Power P0 "Nominal power";
      parameter Data.Core.Neutronics.KineticParameters KP=NSSS_ThermoPower.Data.Core.Neutronics.EightGroups_ESMR
        "Precursor group kinetic data";
      parameter Boolean dynamicPromptNeutrons = true "Dynamic balance for prompt neutron density";
      parameter Boolean steadyStateInit=true "Steady-state initialization";
      parameter ThermoPower.Choices.Init.Options initOpt = system.initOpt "Initialisation option" annotation (
        Dialog(tab = "Initialisation"));
      outer ThermoPower.System system "System wide properties";
      parameter Real Nstart = P0*KP.LAMBDA*nu/wF/Vfuel "Start value of neutron density";
      parameter Real Dstart[KP.NPG] = (KP.beta*Nstart)./(KP.LAMBDA.*KP.lambda) "Start value of normalized precursor density";
      parameter Modelica.Units.SI.ReactionEnergy wF = 202.509*1.60218e-13 "Energy yield per fission";
      parameter Modelica.Units.SI.Volume Vfuel=2.075613823 "Active reactor volume";
      parameter Real nu=2.46261 "Neutrons generated per fission";
      parameter Boolean use_Source = false "Use connector input for the external source" annotation (
        Dialog(group = "External inputs"),
        choices(checkBox = true));
      Modelica.Units.SI.Power P(start=P0) "Thermal power";
      NSSS_ThermoPower.Units.Reactivity Rho "Partial reactivity";
      Real N(start = Nstart) "Normalized neutron density";
      Real D[KP.NPG](start = Dstart) "Precursors Normalized Density";
      Modelica.Blocks.Interfaces.RealInput S_ext if use_Source "Normalized neutron flow from external source" annotation (
        Placement(transformation(extent = {{-100, 0}, {-80, 20}}, rotation = 0)));
    protected
      Modelica.Blocks.Interfaces.RealInput S;

    equation
      if not use_Source then
        S = 0;
      end if;
      assert(not (S > 0 and not dynamicPromptNeutrons), "External source requires dynamic prompt neutron balance");
      if dynamicPromptNeutrons then
        der(N) = (Rho - KP.Beta)*N/KP.LAMBDA + sum(KP.lambda.*D) + S "Prompt neutron dynamic balance";
      else
        0 = (Rho - KP.Beta)*N/KP.LAMBDA + sum(KP.lambda.*D) + S;
      end if;
      for i in 1:KP.NPG loop
        der(D[i]) = KP.beta[i]*N/KP.LAMBDA - KP.lambda[i]*D[i] "Precursor density dynamics";
      end for;
      P=wF*Vfuel/nu/KP.LAMBDA*N;

      connect(S_ext, S);

    initial equation
      // Steady-state initialization
      if initOpt == ThermoPower.Choices.Init.Options.steadyState then
        der(D) = zeros(KP.NPG);
        if dynamicPromptNeutrons then
          der(N) = 0;
        else
          N = Nstart;
        end if;
      elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
        D = Dstart;
        N = Nstart;
      end if;
      annotation (
        Icon(graphics));
    end KineticsBase;

  end BaseClasses;

  package MaterialProperties "Thermal properties of materials"
    extends Modelica.Icons.MaterialPropertiesPackage;

    partial model MaterialProperties "Thermal properties of materials"
      extends Modelica.Icons.MaterialProperty;
      Modelica.Units.SI.Temperature T "Temperature";
      Modelica.Units.NonSI.Temperature_degC T_C "Temperature in degC";
      Modelica.Units.SI.Density rho "Density";
      Modelica.Units.SI.SpecificHeatCapacity cp "Specific Heat Capacity";
      Modelica.Units.SI.ThermalConductivity k "Thermal conductivity";
    equation
      T_C = T - 273.15;
    end MaterialProperties;

    model StandardFuel "Standard UO2"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 10600;
      cp = 4.186e-2*(7100 + 0.6*T - 146*T^(-2) + 4*(T/1000)^7);
      assert((T_C > 0) and (T_C < 2500), "Fuel Temperature out of range for k calculation");
      k = 1e2/(11.8 + 0.0238*T_C) + 8.775e-15*T_C^3;
    end StandardFuel;

    model StandardFuelConstant "Standard UO2 with constant properties"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 10600;
      cp = 4.186e-2*7100;
      assert((T_C > 0) and (T_C < 2500), "Fuel Temperature out of range for k calculation");
      k = 1e2/11.8;
    end StandardFuelConstant;

    model Zircaloy4 "Zircaloy 4"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 7000;
      assert((T_C > 90) and (T_C < 673.15), "Cladding Temperature out of range for Cp calculation");
      cp = (292.14 + 0.138*T_C - 0.61167e-4*T_C^2);
      k = 7.51 + 2.09*1e-2*T_C - 1.45e-5*T_C^2 + 7.67e-9*T_C^3;
    end Zircaloy4;

    model Zircaloy4Constant "Zircaloy 4 with constant properties"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 7000;
      assert((T_C > 90) and (T_C < 673.15), "Cladding Temperature out of range for Cp calculation");
      cp = 292.14;
      k = 7.51;
    end Zircaloy4Constant;

    model StandardFuel_ESMR "Standard UO2"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 10307;
      cp = -2E-11*T_C^4 + 1E-07*T_C^3 - 0.0003*T_C^2 + 0.263*T_C + 233.07;
      //cp = 4.186e-2*(7100 + 0.6*T - 146*T^(-2) + 4*(T/1000)^7);
      //k = 8E-13*T_C^4 - 6E-09*T_C^3 + 2E-05*T_C^2 - 0.0205*T_C + 12.682;
      k = 100/(7.5408 + 17.692*(T/1000) + 3.6142*(T/1000)^2) + 6400/(T/1000)^(5/2)*exp(-16.35/(T/1000));
    end StandardFuel_ESMR;

    model ZircaloyM5_ESMR "Zircaloy M5"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 6500;
      cp = -8E-07*T_C^2 + 0.1151*T_C + 283.85;
      k = 1E-06*T_C^2 + 0.0094*T_C + 16.595;
      //k = 7.51 + 2.09*1e-2*T_C - 1.45e-5*T_C^2 + 7.67e-9*T_C^3;
    end ZircaloyM5_ESMR;

    model Helium "Helium properties"
      extends
        NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties;
    equation
      rho = 3E-12*T_C^4 - 1E-08*T_C^3 + 1E-05*T_C^2 - 0.0073*T_C + 2.549;
      cp = 5192;
      k = -5E-08*T_C^2 + 0.0003*T_C + 0.1497;
    end Helium;
  end MaterialProperties;

  package Interfaces
    connector Fission "Temperature - Power port"
      Modelica.Units.SI.Temperature T_Doppler "Average temperature of the fuel rods for Doppler effect evaluation";
      Modelica.Units.SI.Power P "Nuclear power";
      annotation (
        Icon(graphics={  Rectangle(extent = {{-100, 100}, {100, -100}}, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid)}));
    end Fission;

    connector Moderation "Moderator properties for reactivity feedback computations"
      Modelica.Units.SI.Temperature T "Average temperature of the moderator" annotation ();
      Modelica.Units.SI.Temperature T_out "Outlet temperature of the coolant" annotation ();
      Modelica.Units.SI.Temperature T_in "Inlet temperature of the coolant" annotation ();
      annotation (
        Icon(graphics={  Rectangle(extent = {{-100, 100}, {100, -100}}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid)}));
    end Moderation;

    partial model Core_int

      // General
      parameter Integer Nax = 10 "Axial discretization" annotation (
        Dialog(tab = "General"));
      parameter Integer Nrad = 5 "Fuel radial discretization" annotation (
        Dialog(tab = "General"));
      parameter Modelica.Units.SI.Power Pnom = 540e6 "Nominal power" annotation (
        Dialog(tab = "General", group = "Nominal conditions"));
      parameter Modelica.Units.SI.MassFlowRate wnom = 3700 "Nominal mass flow rate" annotation (
        Dialog(tab = "General", group = "Nominal conditions"));
      parameter Modelica.Units.SI.Pressure pnom = 150e5 "Nominal pressure" annotation (
        Dialog(tab = "General", group = "Nominal conditions"));
      parameter Modelica.Units.SI.Temperature Tin_nom = 300 + 273.15 "Nominal inlet temperature" annotation (
        Dialog(tab = "General", group = "Nominal conditions"));
      parameter Modelica.Units.SI.Temperature Tout_nom = 324.79 + 273.15 "Nominal outlet temperature" annotation (
        Dialog(tab = "General", group = "Nominal conditions"));
      replaceable package ModeratorMaterial =
          ThermoPower.Water.StandardWater                                     constrainedby
        Modelica.Media.Interfaces.PartialMedium                                                                                     "Medium model" annotation (
         Dialog(tab = "General", group = "Materials"),
         choicesAllMatching = true);
      replaceable model FuelMaterial =
          TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.StandardFuel_ESMR
        constrainedby
        TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                            "Fuel material properties" annotation (
         Dialog(tab = "General", group = "Materials"),
         choices(choice = 1 "StandardFuel", choice = 2 "StandardFuelConstant", choice = 3 "StandardFuel_ESMR"));
      replaceable model GapMaterial =
          TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.Helium
        constrainedby
        TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                "Gap material properties" annotation (
         Dialog(tab = "General", group = "Materials"),
         choices(choice = 1 "Helium"));
      replaceable model CladdingMaterial =
          TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.ZircaloyM5_ESMR
        constrainedby
        TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.MaterialProperties.MaterialProperties                                                                                              "Cladding material properties" annotation (
         Dialog(tab = "General", group = "Materials"),
         choices(choice = 1 "Zircaloy4", choice = 2 "Zircaloy4Constant", choice = 3 "ZircaloyM5_ESMR"));
      // Friction factor
      parameter ThermoPower.Choices.Flow1D.FFtypes core_dp= ThermoPower.Choices.Flow1D.FFtypes.NoFriction
        "Friction Factor Type" annotation(Dialog(tab="General",group="Coolant channel pressure drop"),Evaluate=true);
      parameter Modelica.Units.SI.PressureDifference dpnom = 0
        "Nominal pressure drop (friction term only!)" annotation(Dialog(tab="General",group="Coolant channel pressure drop",enable = not (core_dp == ThermoPower.Choices.Flow1D.FFtypes.NoFriction)));
      parameter Real Kfnom = 0
        "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
       annotation(Dialog(tab="General",group="Coolant channel pressure drop",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
      parameter Modelica.Units.SI.Density rhonom=0 "Nominal inlet density"
        annotation(Dialog(tab="General",group="Coolant channel pressure drop",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
      parameter Modelica.Units.SI.PerUnit Cfnom=0 "Nominal Fanning friction factor"
        annotation(Dialog(tab="General",group="Coolant channel pressure drop",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
      parameter Modelica.Units.SI.PerUnit e=0 "Relative roughness (ratio roughness/diameter)"
        annotation(Dialog(tab="General",group="Coolant channel pressure drop",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));


      // Geometry
      parameter Integer Nch = 21964 "Number of coolant channels" annotation (
        Dialog(tab = "Geometry"));
      parameter Integer Nf = 17936 "Number of fuel rods" annotation (
        Dialog(tab = "Geometry"));
      parameter Integer Np = 2128 "Number of poisoned rods" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length H = 2.159 "Core length" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length Ha = 2 "Active length" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length He = Ha "Extrapolated length" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length pitch = 0.012598 "Fuel rod pitch" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length Rf = 4.05765E-03 "Fuel radius" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length Rci = 4.1402E-03 "Cladding inner radius" annotation (
        Dialog(tab = "Geometry"));
      parameter Modelica.Units.SI.Length Rco = 4.7498E-03 "Cladding outer radius" annotation (
        Dialog(tab = "Geometry"));
      // Neutronics
      parameter
        TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.Neutronics.KineticParameters KP=
          NSSS_ThermoPower.Data.Core.Neutronics.EightGroups_ESMR
        "Precursor group kinetic data" annotation (Dialog(tab="Neutronics", group="Parameters"),
          choices(choice=NSSS_ThermoPower.Data.Core.EightGroups_ESMR
            "E-SMR neutronic parameters"));
      parameter Modelica.Units.SI.MacroscopicCrossSection SigmaF(displayUnit = "1/(cm)") = 63.1259758 "Macroscopic fission cross section" annotation (
        Dialog(tab = "Neutronics", group = "Parameters"));
      parameter Modelica.Units.SI.ReactionEnergy wF = 202.509e6*1.60218e-19 "Energy yield per fission" annotation (
        Dialog(tab = "Neutronics", group = "Parameters"));
      parameter Modelica.Units.SI.NeutronYieldPerFission nu = 2.46261 "Neutron yield per fission" annotation (
        Dialog(tab = "Neutronics", group = "Parameters"));
      parameter NSSS_ThermoPower.Units.Reactivity_pcm rho0=0
        "Initial reactivity margin"
        annotation (Dialog(tab="Neutronics", group="Parameters"));
      parameter Boolean TemperatureFeedbacks = true annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks"));
      parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByTemperature alfa_D=-2
        "Doppler feedback coefficient" annotation (Dialog(
          tab="Neutronics",
          group="Reactivity feedbacks",
          enable=TemperatureFeedbacks));
      parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByTemperature alfa_C=-60
        "Coolant temperature feedback coefficient" annotation (Dialog(
          tab="Neutronics",
          group="Reactivity feedbacks",
          enable=TemperatureFeedbacks));
      parameter Modelica.Units.SI.Temperature T0_Doppler = 720 + 273.15 "Reference fuel temperature (non-steady state conditions)" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = TemperatureFeedbacks and not initOpt == ThermoPower.Choices.Init.Options.steadyState));
      parameter Modelica.Units.SI.Temperature T0_Coolant = 312.4 + 273.15 "Reference coolant temperature (non-steady state conditions)" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = TemperatureFeedbacks and not initOpt == ThermoPower.Choices.Init.Options.steadyState));
      parameter Boolean XenonFeedback = true annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks"));
      parameter Real gammaI = 0.0639 "I fission yield" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = XenonFeedback));
      parameter Modelica.Units.SI.DecayConstant lambdaI = 2.92615E-05 "I decay constant" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = XenonFeedback));
      parameter Real gammaXe = 0.00237 "Xe fission yield" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = XenonFeedback));
      parameter Modelica.Units.SI.DecayConstant lambdaXe = 2.10657E-05 "Xe decay constant" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = XenonFeedback));
      parameter Modelica.Units.SI.CrossSection sigmaXe = 2.71e-22 "Xe absorption cross section" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity feedbacks", enable = XenonFeedback));
      parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion ReactivityInsertion=
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.Reactivity
        annotation (Dialog(tab="Neutronics", group="Reactivity insertion model"),
          choicesAllMatching=true);
      parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByLength alfa_H=0
        "Coefficient associated with the insertion length of an ideal control rod"
        annotation (Dialog(
          tab="Neutronics",
          group="Reactivity insertion model",
          enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
      parameter Modelica.Units.SI.Length h_0 = 0 "Reference position of the ideal control rod" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity insertion model", enable=
              ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
      parameter Real A_CR = 0 "CR calibration curve coefficient (A*sin(B*h+C)+D" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity insertion model", enable=
              ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real B_CR = 0 "CR calibration curve coefficient" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity insertion model", enable=
              ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real C_CR = 0 "CR calibration curve coefficient" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity insertion model", enable=
              ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Real D_CR = 0 "CR calibration curve coefficient" annotation (
        Dialog(tab = "Neutronics", group = "Reactivity insertion model", enable=
              ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
      parameter Boolean UniformPower = true "Uniform power distribution in the core" annotation (
        Dialog(tab = "Neutronics", group = "Options"));
      parameter Boolean dynamicPromptNeutrons = true "Dynamic balance for prompt neutron density" annotation (
        Dialog(tab = "Neutronics", group = "Options"));
      parameter Boolean NeutronSource = true "Use connector input for the external source" annotation (
        Dialog(tab = "Neutronics", group = "Options"));
      // Initialisation
      parameter ThermoPower.Choices.Init.Options initOpt = system.initOpt "Initialisation option" annotation (
        Dialog(tab = "Initialisation"));
      parameter Modelica.Units.SI.SpecificEnthalpy hin_start = Modelica.Media.Water.IF97_Utilities.h_pT(pnom, Tin_nom) "Initial inflow enthalpy" annotation (
        Dialog(tab = "Initialisation", group = "Coolant"));
      parameter Modelica.Units.SI.SpecificEnthalpy hout_start = Modelica.Media.Water.IF97_Utilities.h_pT(pnom, Tout_nom) "Initial outflow enthalpy" annotation (
        Dialog(tab = "Initialisation", group = "Coolant"));
      parameter Boolean steadyState = true annotation (
        Dialog(tab = "Initialisation", group = "Fuel"));
      replaceable parameter
        TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.IC_inputs.IC_Fuel initFuel=
          NSSS_ThermoPower.Data.Core.IC_inputs.uniform_Ax10Rad5 annotation (Dialog(
            tab="Initialisation", group="Fuel"), choices(
          choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad3,
          choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad3,
          choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad5,
          choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad5,
          choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad10,
          choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad10,
          choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax20Rad5,
          choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax20Rad5));

      parameter Modelica.Units.SI.Temperature Tf_start[Nax, Nrad] = initFuel.Tf_start annotation (
        Dialog(tab = "Initialisation", group = "Fuel"),
        choices(choice = 1000*ones(core.Nax, core.Nrad) "Steady state", choice = core.initFuel.Tf_start "Recorded value"));
      parameter Modelica.Units.SI.Temperature Tc_start[Nax] = initFuel.Tc_start annotation (
        Dialog(tab = "Initialisation", group = "Fuel"),
        choices(choice = 600*ones(core.Nax) "Steady state", choice = core.initFuel.Tc_start "Recorded value"));
      //parameter NSSS.Components.Core.Data.IC_Fuel initFuel = NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad5 annotation(Dialog(tab="Initialisation", group="Fuel", enable=not steadyState), choicesAllMatching = true);
      parameter Real Nstart = 1 "Initial normalized neutron density" annotation (
        Dialog(tab = "Initialisation", group = "Neutronics"));
      parameter Real Dstart[KP.NPG] = (KP.beta*Nstart)./(KP.LAMBDA.*KP.lambda) "Start value of normalized precursor density" annotation (
        Dialog(tab = "Initialisation", group = "Neutronics"));
      constant Real pi = Modelica.Constants.pi "3.14159...";
      outer ThermoPower.System system "System wide properties";

      ThermoPower.Water.FlangeA flangeA annotation (
        Placement(transformation(extent={{-10,-110},{10,-90}}),      iconTransformation(extent={{-8,-112},
                {12,-92}})));
      ThermoPower.Water.FlangeB flangeB annotation (
        Placement(transformation(extent={{-10,90},{10,110}}),     iconTransformation(extent={{-8,88},
                {12,108}})));
      Modelica.Blocks.Interfaces.RealInput rhoCR annotation (
        Placement(transformation(extent={{-104,34},{-76,62}}),     iconTransformation(extent={{-102,32},
                {-74,60}})));
      Modelica.Blocks.Interfaces.RealInput S annotation (
        Placement(transformation(extent={{-106,-14},{-78,14}}),     iconTransformation(extent={{-104,
                -16},{-76,12}})));
      Modelica.Blocks.Interfaces.RealOutput Reactivity annotation (
        Placement(transformation(extent={{80,48},{100,68}}),     iconTransformation(extent={{82,46},
                {102,66}})));
      Modelica.Blocks.Interfaces.RealOutput Power annotation (
        Placement(transformation(extent={{80,10},{100,30}}),     iconTransformation(extent={{82,8},{
                102,28}})));
      Modelica.Blocks.Interfaces.RealOutput Tc_max annotation (
        Placement(transformation(extent={{80,-30},{100,-10}}),    iconTransformation(extent={{82,-32},
                {102,-12}})));
      Modelica.Blocks.Interfaces.RealOutput Tf_max annotation (
        Placement(transformation(extent={{80,-70},{100,-50}}),     iconTransformation(extent={{82,-72},
                {102,-52}})));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                                                        Ellipse(extent={{-74,96},
                  {78,46}},                                                                                     lineColor = {0, 0, 0}, pattern = LinePattern.None, fillPattern=
                  FillPattern.Sphere,                                                                                                                                                                          fillColor={0,0,255}),
                                                                                                                                                                                                    Ellipse(extent={{-74,-44},
                  {78,-96}},                                                                                                                                                                                                        lineColor = {0, 0, 0}, pattern = LinePattern.None, fillPattern=
                  FillPattern.Sphere,                                                                                                                                                                                                        fillColor={0,0,255}),
                                                                                                                                                                                                    Rectangle(
              extent={{-74,70},{78,-72}},
              lineColor={0,0,0},
              pattern=LinePattern.None,
              fillPattern=FillPattern.VerticalCylinder,
              fillColor={0,0,255}),
            Rectangle(
              extent={{42,14},{46,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{48,40},{52,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{48,14},{52,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{48,-12},{52,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-6,40},{-2,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{-6,66},{-2,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{-6,14},{-2,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{-6,-12},{-2,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{-6,-38},{-2,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{6,66},{10,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{6,40},{10,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{6,14},{10,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{6,-12},{10,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{6,-38},{10,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{12,14},{16,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{12,40},{16,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{12,66},{16,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{12,-12},{16,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{12,-38},{16,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{18,14},{22,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{18,40},{22,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{18,-12},{22,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{18,-38},{22,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{18,66},{22,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{24,14},{28,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{24,40},{28,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{24,66},{28,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{24,-12},{28,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{24,-38},{28,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{36,14},{40,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{36,40},{40,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{36,-12},{40,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{36,-38},{40,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{36,66},{40,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{42,40},{46,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{42,-12},{46,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{42,66},{46,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{42,-38},{46,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{54,66},{58,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,127,0},
              lineThickness=0.5),
            Rectangle(
              extent={{48,-38},{52,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{54,14},{58,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{54,-12},{58,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{54,40},{58,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{48,66},{52,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{54,-38},{58,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,127,0},
              lineThickness=0.5),
            Rectangle(
              extent={{-12,66},{-8,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{-12,14},{-8,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={226,26,28},
              lineThickness=0.5),
            Rectangle(
              extent={{-12,40},{-8,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{-12,-12},{-8,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{-12,-38},{-8,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{-18,14},{-14,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,125,35},
              lineThickness=0.5),
            Rectangle(
              extent={{-18,40},{-14,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{-18,-12},{-14,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{-24,14},{-20,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={244,218,23},
              lineThickness=0.5),
            Rectangle(
              extent={{-18,66},{-14,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{-24,40},{-20,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{-34,14},{-30,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{-24,-12},{-20,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{-18,-38},{-14,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={159,244,31},
              lineThickness=0.5),
            Rectangle(
              extent={{-24,-38},{-20,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{-24,66},{-20,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{-34,40},{-30,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{-34,-12},{-30,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{-40,14},{-36,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,244,184},
              lineThickness=0.5),
            Rectangle(
              extent={{-34,-38},{-30,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{-34,66},{-30,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{-40,40},{-36,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{-46,14},{-42,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{-40,-12},{-36,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={33,145,244},
              lineThickness=0.5),
            Rectangle(
              extent={{-40,-38},{-36,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-46,-12},{-42,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-46,40},{-42,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-40,66},{-36,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-46,66},{-42,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-52,40},{-48,14}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-52,14},{-48,-12}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-52,-12},{-48,-38}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-46,-38},{-42,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={128,0,255},
              lineThickness=0.5),
            Rectangle(
              extent={{-52,66},{-48,40}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,127,0},
              lineThickness=0.5),
            Rectangle(
              extent={{-52,-38},{-48,-64}},
              lineColor={0,0,0},
              fillPattern=FillPattern.Solid,
              fillColor={0,127,0},
              lineThickness=0.5)}), Diagram(coordinateSystem(preserveAspectRatio=false)));
    end Core_int;
  end Interfaces;

  package Utilities
    type ReactivityInsertion = enumeration(
        Reactivity                                    "Reactivity value (pcm)",
        CRpos_proportional                                                                         "CR position - proportional",
        CRpos_calibration                                                                                                                          "CR position - calibration curve");
  end Utilities;
end Core;
