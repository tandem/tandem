within TANDEM.SMR.NSSS.NSSS_ThermoPower.Components;
package Pressurizer

  package Test
    model Test_Shippingport
      PressEq pressEq(
        Geometry="Cylinder",
        hsp=1.1533e+06,
        pvlv=14096904.867735,
        pstart=14096904.867735)
        annotation (Placement(transformation(extent={{12,-8},{44,24}})));
      Control.PressureControl.PressCtrl_OnOff pressurizerControl_OnOff(
        Qback=250e3,
        Qprop_max=80e3,
        msp_max=1.91,
        hysteresis1(uLow=-137.895e5, uHigh=-136.516e5),
        hysteresis3(uLow=-138.929e5, uHigh=-133.414e5),
        hysteresis(uLow=139.6188e5, uHigh=142.7215e5))
        annotation (Placement(transformation(extent={{42,64},{6,38}})));
      inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
        annotation (Placement(transformation(extent={{68,66},{88,86}})));
      ThermoPower.Water.SourceMassFlow sourceMassFlow(
        p0=14096904.867735,
        h=1.2434e+06,
        use_in_w0=true)
        annotation (Placement(transformation(extent={{-46,-50},{-26,-30}})));
    Modelica.Blocks.Sources.TimeTable Flow74(table=[0,0; 2.69671704,1.184503861;
            5.768629495,3.762993559; 11.16206357,6.074090891; 18.87701928,
            8.117795857; 23.84835852,8.828084528; 31.07087024,9.00417939;
            35.80771235,8.825129917; 38.12923398,8.557737551; 40.40385618,
            8.112477556; 42.60812923,7.400416118; 44.90620115,7.044089937;
            49.52579468,6.420371391; 51.80041688,5.975111397; 58.8353309,
            5.439735743; 61.08650339,4.905541933; 65.75299635,4.459691016;
            70.41948932,4.013840099; 75.109432,3.656922996; 79.75247525,3.122138265;
            86.78738927,2.586762611; 91.43043252,2.051977879; 96.07347577,
            1.517193147; 103.0849401,0.892883679; 110.1433038,0.446441839;
            126.4643043,-1.158503278; 140.5341324,-2.229254586; 145.2006253,-2.675105503;
            168.8848359,-3.570352871; 176.0369984,-3.661059453; 183.189161,-3.751766034;
            190.388223,-3.664604987; 202.3944763,-3.48969197; 209.616988,-3.313597108;
            231.2845232,-2.785312522; 257.7357999,-2.258209781; 281.7717561,-1.819449932;
            308.1761334,-1.47021482; 336.9958312,-1.032636816; 380.2136529,-0.420736717;
            399.4658676,0.019204977; 423.6425221,0.991567712; 440.6201146,
            1.876769401; 452.8139656,2.763152934; 460.1771756,3.472850683;
            467.6107348,4.449349875; 472.5351746,4.981770917; 479.7342366,
            5.068931965; 486.8629495,4.889291569; 496.2897342,4.353324992;
            510.383012,3.371507499; 522.1547681,2.657082371; 531.5346535,
            1.943248166; 543.2595102,1.050955409; 555.0312663,0.336530281;
            571.5633142,-0.468010505; 592.8791037,-1.273733137; 614.2652423,-1.812654325;
            628.4757686,-2.349802746; 659.288692,-3.424690511; 668.7623762,-3.782789458;
            685.5289213,-3.6979921; 704.8749349,-2.902315149; 717.0218864,-2.193799244;
            729.1688379,-1.48528334; 743.7076602,-0.777358359; 750,0])                                                                                                                                                                                                         annotation (
          Placement(visible = true, transformation(origin={-78,-30},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.TimeTable Flowrate105(table=[0,0; 0.685518423,0.8;
            0.856898029,0.992592593; 0.856898029,1.2; 0.856898029,1.392592593;
            0.856898029,1.6; 1.199657241,1.792592593; 1.199657241,2; 1.371036847,
            2.192592593; 1.542416452,2.4; 1.542416452,2.592592593; 2.227934876,2.8;
            2.399314482,2.992592593; 3.427592117,3.2; 3.427592117,3.392592593;
            3.770351328,3.792592593; 3.941730934,3.6; 3.941730934,4; 4.284490146,
            4.192592593; 4.284490146,4.4; 4.970008569,4.592592593; 5.312767781,4.8;
            5.312767781,4.992592593; 5.484147386,5.2; 5.484147386,5.392592593;
            5.826906598,5.6; 6.16966581,5.792592593; 7.026563839,6; 7.026563839,
            6.192592593; 7.369323051,6.4; 7.712082262,6.592592593; 8.397600686,6.8;
            8.568980291,6.992592593; 9.254498715,7.2; 9.597257926,7.392592593;
            10.11139674,7.6; 10.79691517,7.792592593; 11.6538132,8; 13.02485004,
            8.192592593; 14.05312768,8.4; 14.7386461,8.592592593; 16.28106255,8.8;
            16.28106255,8.992592593; 17.82347901,9.2; 18.16623822,9.392592593;
            19.36589546,9.6; 19.36589546,9.792592593; 21.59383033,10; 23.99314482,
            10.25185185; 27.07797772,10.41481481; 29.99143102,10.37037037;
            33.07626392,9.733333333; 35.98971722,9.007407407; 39.07455013,
            8.296296296; 41.98800343,7.62962963; 45.07283633,6.977777778;
            47.98628963,6.414814815; 51.07112254,5.777777778; 53.98457584,
            5.244444444; 57.06940874,4.592592593; 59.98286204,4.088888889;
            63.06769494,3.540740741; 65.98114824,3.140740741; 69.06598115,
            2.696296296; 71.97943445,2.237037037; 75.06426735,1.807407407;
            77.97772065,1.392592593; 81.06255356,1.125925926; 83.97600686,
            1.037037037; 87.06083976,0.874074074; 89.97429306,0.666666667;
            93.05912596,0.474074074; 95.97257926,0.296296296; 99.05741217,
            0.118518519; 101.9708655,-0.074074074; 105.0556984,-0.266666667;
            107.9691517,-0.459259259; 111.0539846,-0.637037037; 113.9674379,-0.859259259;
            117.0522708,-1.037037037; 119.9657241,-1.22962963; 123.050557,-1.466666667;
            125.9640103,-1.837037037; 129.0488432,-2.251851852; 131.9622965,-2.548148148;
            135.0471294,-2.82962963; 137.9605827,-2.933333333; 141.0454156,-3.2;
            143.9588689,-3.407407407; 147.0437018,-3.62962963; 149.9571551,-3.807407407;
            153.041988,-3.703703704; 155.9554413,-3.644444444; 159.0402742,-3.525925926;
            161.9537275,-3.392592593; 165.0385604,-3.259259259; 167.9520137,-3.185185185;
            171.0368466,-3.125925926; 173.9502999,-2.992592593; 177.0351328,-2.874074074;
            179.9485861,-2.874074074; 183.033419,-2.859259259; 185.9468723,-2.77037037;
            189.0317052,-2.725925926; 191.9451585,-2.725925926; 195.0299914,-2.696296296;
            197.9434447,-2.607407407; 201.0282776,-2.592592593; 203.9417309,-2.488888889;
            207.0265638,-2.474074074; 209.9400171,-2.340740741; 213.02485,-2.192592593;
            215.9383033,-2.074074074; 219.0231362,-1.985185185; 221.9365895,-1.896296296;
            225.0214225,-1.792592593; 227.9348757,-1.674074074; 231.0197087,-1.525925926;
            233.933162,-1.392592593; 237.0179949,-1.259259259; 239.9314482,-1.155555556;
            243.0162811,-1.081481481; 245.9297344,-0.992592593; 249.0145673,-0.874074074;
            251.9280206,-0.8; 255.0128535,-0.681481481; 257.9263068,-0.592592593;
            261.0111397,-0.459259259; 263.924593,-0.325925926; 267.0094259,-0.192592593;
            269.9228792,-0.074074074; 273.0077121,0.02962963; 275.9211654,
            0.074074074; 279.0059983,0.074074074; 281.9194516,0.074074074;
            285.0042845,0.074074074; 287.9177378,0.074074074; 291.0025707,
            0.074074074; 293.916024,0.074074074; 297.0008569,0.074074074;
            300.0856898,0.074074074; 302.9991431,0.074074074; 306.083976,
            0.074074074; 308.9974293,0.074074074; 312.0822622,0.074074074;
            314.9957155,0.074074074; 318.0805484,0.074074074; 320.9940017,
            0.074074074; 324.0788346,0.074074074; 326.9922879,0.074074074;
            330.0771208,0.074074074; 332.9905741,0.074074074; 336.075407,
            0.074074074; 338.9888603,0.074074074; 342.0736932,0.074074074;
            344.9871465,0.074074074; 348.0719794,0.074074074; 350.9854327,
            0.074074074; 354.0702656,0.074074074; 356.9837189,0.074074074;
            360.0685518,0.074074074; 362.9820051,0.074074074; 366.066838,
            0.074074074; 368.9802913,0.074074074; 372.0651243,0.074074074;
            374.9785775,0.074074074; 378.0634105,0.103703704; 380.9768638,
            0.192592593; 384.0616967,0.325925926; 386.97515,0.355555556;
            390.0599829,0.474074074; 392.9734362,0.562962963; 396.0582691,
            0.607407407; 398.9717224,0.740740741; 402.0565553,0.82962963;
            404.9700086,0.874074074; 408.0548415,0.992592593; 410.9682948,
            1.066666667; 414.0531277,1.377777778; 416.966581,1.718518519;
            420.0514139,2; 422.9648672,2.355555556; 426.0497001,2.622222222;
            428.9631534,2.977777778; 432.0479863,3.288888889; 434.9614396,3.6;
            438.0462725,3.866666667; 440.9597258,4.177777778; 444.0445587,
            4.488888889; 446.958012,4.740740741; 450.0428449,4.592592593;
            452.9562982,4.385185185; 456.0411311,4.148148148; 458.9545844,4;
            462.0394173,3.762962963; 464.9528706,3.659259259; 468.0377035,
            3.466666667; 470.9511568,3.037037037; 474.0359897,2.592592593;
            476.949443,2.177777778; 480.0342759,1.62962963; 482.9477292,1.155555556;
            486.0325621,0.281481481; 488.9460154,-0.518518519; 492.0308483,-1.081481481;
            494.9443016,-1.274074074; 498.0291345,-1.451851852; 500.9425878,-1.62962963;
            504.0274207,-1.792592593; 506.940874,-1.792592593; 510.0257069,-1.866666667;
            512.9391602,-1.911111111; 516.0239931,-1.925925926; 518.9374464,-1.925925926;
            522.0222793,-1.940740741; 524.9357326,-1.925925926; 528.0205656,-1.925925926;
            530.9340189,-2.02962963; 534.0188518,-2.02962963; 536.9323051,-2.059259259;
            540.017138,-2.148148148; 542.9305913,-2.296296296; 546.0154242,-2.459259259;
            548.9288775,-2.607407407; 552.0137104,-2.725925926; 554.9271637,-2.874074074;
            558.0119966,-2.962962963; 560.9254499,-2.962962963; 564.0102828,-2.962962963;
            566.9237361,-2.962962963; 570.008569,-3.007407407; 572.9220223,-2.992592593;
            576.0068552,-2.992592593; 578.9203085,-3.007407407; 582.0051414,-2.962962963;
            584.9185947,-2.814814815; 588.0034276,-2.696296296; 590.9168809,-2.518518519;
            594.0017138,-2.4; 596.9151671,-2.266666667; 600,-2.074074074;
            603.0848329,-1.940740741; 605.9982862,-1.851851852; 609.0831191,-1.703703704;
            611.9965724,-1.525925926; 615.0814053,-1.392592593; 617.9948586,-1.259259259;
            621.0796915,-1.125925926; 623.9931448,-1.007407407; 627.0779777,-0.859259259;
            629.991431,-0.8; 633.0762639,-0.607407407; 635.9897172,-0.474074074;
            639.0745501,-0.37037037; 641.9880034,-0.237037037; 645.0728363,-0.059259259;
            647.9862896,0.074074074; 651.0711225,0.118518519; 653.9845758,
            0.207407407; 657.0694087,0.222222222; 659.982862,0.296296296;
            663.0676949,0.340740741; 665.9811482,0.4; 669.0659811,0.474074074;
            671.9794344,0.474074074; 675.0642674,0.577777778; 677.9777207,
            0.592592593; 681.0625536,0.622222222; 683.9760069,0.696296296;
            687.0608398,0.696296296; 689.9742931,0.740740741; 693.059126,
            0.740740741; 695.9725793,0.740740741; 699.0574122,0.77037037;
            701.9708655,0.77037037; 705.0556984,0.740740741; 707.9691517,
            0.740740741; 711.0539846,0.725925926; 713.9674379,0.696296296;
            717.0522708,0.696296296; 719.9657241,0.607407407])
        annotation (Placement(transformation(extent={{-88,-2},{-68,18}})));
    equation
      connect(pressEq.Pressure, pressurizerControl_OnOff.p) annotation (Line(points={{41.44,
              17.28},{58,17.28},{58,51},{43.0286,51}},        color={0,0,127}));
      connect(pressurizerControl_OnOff.theta_valve, pressEq.valve) annotation (Line(
            points={{4.97143,58.8},{-16,58.8},{-16,0.32},{13.92,0.32}}, color={0,0,
              127}));
      connect(pressurizerControl_OnOff.Sprays, pressEq.sprayers) annotation (Line(
            points={{5.22857,51},{-10,51},{-10,8.8},{14.08,8.8}}, color={0,0,127}));
      connect(pressurizerControl_OnOff.Heater, pressEq.heaters) annotation (Line(
            points={{5.22857,43.2},{-4,43.2},{-4,16.64},{13.92,16.64}}, color={0,0,
              127}));
      connect(pressEq.bottomFlange, sourceMassFlow.flange)
        annotation (Line(points={{28,-7.68},{28,-40},{-26,-40}}, color={0,0,255}));
      connect(Flow74.y, sourceMassFlow.in_w0) annotation (Line(points={{-67,-30},{
              -40,-30},{-40,-34.4}}, color={0,0,127}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));
    end Test_Shippingport;
  end Test;

  model PressEq


     replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
      Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model";

      // Pressurizer
    parameter String Geometry= "Hemisphere" annotation(Dialog(group="Geometry"), choices(choice="Cylinder", choice="Hemisphere"));
    parameter Modelica.Units.SI.Length D=if Geometry=="Cylinder" then 1.37 else 3.65 "Pressurizer inner diameter" annotation(Dialog(group="Geometry"));
    parameter Modelica.Units.SI.Length H=if Geometry=="Cylinder" then 5.1 else 1 "Pressurizer height" annotation(Dialog(group="Geometry"));
    parameter Modelica.Units.SI.Length Hemi=1 "Height of the hemispherical part" annotation(Dialog(group="Geometry", enable=Geometry=="Hemisphere"));

      parameter Modelica.Units.SI.SpecificEnthalpy hsp=1.33806e+06 "Sprayers enthalpy" annotation(Dialog(group="Control"));
    parameter ThermoPower.Units.HydraulicConductance Kvlv = 1e3 "Valve hydraulic resistance" annotation(Dialog(group="Control"));
    parameter Modelica.Units.SI.Pressure pvlv = 150e5 "Relief valve sink pressure" annotation(Dialog(group="Control"));
    parameter Modelica.Units.SI.Pressure pstart=150e5 "Pressure start value"
      annotation (Dialog(group="Initialization"));
    parameter Modelica.Units.SI.Length ystart=if Geometry=="Cylinder" then 2.482 else 0.8 "Initial liquid level"
      annotation (Dialog(group="Initialization"));
     parameter Modelica.Units.SI.SpecificEnthalpy hstart=1.86213e6 "Initial enthalpy" annotation (Dialog(group="Initialization"));

    NSSS_ThermoPower.Components.Pressurizer.BaseClasses.PRZeq pressurizer(
      redeclare package Medium = Medium,
      Geometry=Geometry,
      D=D,
      H=H,
      Hemi=Hemi,
      hsp=hsp,
      pstart=pstart,
      ystart=ystart,
      hstart=hstart)
      annotation (Placement(transformation(extent={{22,-34},{-22,12}})));
    ThermoPower.Water.ValveLin reliefValve(Kv=Kvlv) annotation (Placement(
          transformation(
          extent={{6,6},{-6,-6}},
          rotation=270,
          origin={46,26})));
    ThermoPower.Water.SinkPressure sinkPressure(p0=pvlv, allowFlowReversal=
          false) annotation (Placement(transformation(
          extent={{6,6},{-6,-6}},
          rotation=270,
          origin={46,64})));

     ThermoPower.Water.FlangeA bottomFlange
      annotation (Placement(transformation(extent={{-10,-108},{10,-88}})));
    Modelica.Blocks.Interfaces.RealInput valve annotation (Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=270,
          origin={30,108}), iconTransformation(extent={{-104,-64},{-72,-32}},
            rotation=0)));
    Modelica.Blocks.Interfaces.RealInput sprayers annotation (Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=270,
          origin={-28,108}), iconTransformation(extent={{-102,-10},{-72,20}},
            rotation=0)));
    Modelica.Blocks.Interfaces.RealInput heaters annotation (Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=270,
          origin={-80,108}), iconTransformation(extent={{-102,40},{-74,68}},
            rotation=0)));
    Modelica.Blocks.Interfaces.RealOutput Pressure annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={80,104}), iconTransformation(extent={{74,48},{94,68}}, rotation=
             0)));
  equation
    connect(sinkPressure.flange, reliefValve.outlet)
      annotation (Line(points={{46,58},{46,32}}, color={0,0,255}));
    connect(pressurizer.relief, reliefValve.inlet) annotation (Line(points={{
            18.04,6.48},{46,6.48},{46,20}}, color={0,0,255}));
    connect(pressurizer.bottomFlange, bottomFlange) annotation (Line(points={{0,-34.92},
            {0,-66},{0,-66},{0,-98}}, color={0,0,255}));
    connect(valve, reliefValve.cmd)
      annotation (Line(points={{30,108},{30,26},{41.2,26}}, color={0,0,127}));
    connect(pressurizer.Pressure, Pressure)
      annotation (Line(points={{18.92,-11},{80,-11},{80,104}}, color={0,0,127}));
    connect(sprayers, pressurizer.m_spray) annotation (Line(points={{-28,108},{-28,
            62},{-0.22,62},{-0.22,12.23}}, color={0,0,127}));
    connect(heaters, pressurizer.Qheater) annotation (Line(points={{-80,108},{-80,
            -22.04},{-17.6,-22.04}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Ellipse(
            extent={{-80,-42},{80,-102}},
            lineColor={0,0,0},
            lineThickness=1,
            fillColor={0,0,255},
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=360,
            closure=EllipseClosure.Chord),
          Ellipse(
            extent={{-80,102},{80,42}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=360,
            closure=EllipseClosure.Chord,
            fillColor={192,220,255}),
          Rectangle(
            extent={{-80,72},{80,-72}},
            lineColor={0,0,0},
            lineThickness=1,
            fillColor={255,255,255},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-79.4,22},{79.4,-74}},
            lineThickness=1,
            fillColor={0,0,255},
            fillPattern=FillPattern.VerticalCylinder,
            pattern=LinePattern.None),
          Rectangle(
            extent={{-80,74},{80,22}},
            lineThickness=1,
            fillColor={192,220,255},
            fillPattern=FillPattern.VerticalCylinder,
            pattern=LinePattern.None),
          Line(
            points={{80,22},{-80,22}},
            color={0,0,0},
            thickness=1,
            smooth=Smooth.Bezier),
          Rectangle(
            extent={{-72,-58},{74,-60}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-54},{74,-56}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-50},{74,-52}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-46},{74,-48}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-42},{74,-44}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-38},{74,-40}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0}),
          Rectangle(
            extent={{-72,-34},{74,-36}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,128,0})}),                              Diagram(
          coordinateSystem(preserveAspectRatio=false)),
                Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end PressEq;

  package BaseClasses
    model PRZeq

    replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
        Modelica.Media.Interfaces.PartialTwoPhaseMedium                                                                        "Medium model";

       parameter String Geometry= "Hemisphere" annotation(choices(choice="Cylinder", choice="Hemisphere"));

    parameter Modelica.Units.SI.Length D = if Geometry=="Cylinder" then 1.37 else 3.65 "Pressurizer inner diameter";
    parameter Modelica.Units.SI.Length H = if Geometry=="Cylinder" then 5.61 else 1 "Height of the cylindrical part";
    parameter Modelica.Units.SI.Length Hemi=1 "Height of the hemispherical part" annotation(Dialog(enable=Geometry==NSSS.Components.Pressurizer.Utilities.GeometryTypes.Hemisphere));
    parameter Modelica.Units.SI.SpecificEnthalpy hsp = 1.1533e+06 "Sprayers enthalpy";

    parameter Modelica.Units.SI.Pressure pstart = 150e5 "Initial pressure" annotation(Dialog(tab="Initialisation"));
    parameter Modelica.Units.SI.Length ystart = if Geometry=="Cylinder" then 2.482 else 0.5 "Initial liquid level" annotation(Dialog(tab="Initialisation"));
    parameter Modelica.Units.SI.SpecificEnthalpy hstart=1.86213e6 "Initial enthalpy" annotation(Dialog(tab="Initialisation"));

    Modelica.Units.SI.Volume V;
    Modelica.Units.SI.Volume Vl(start=pi*D^2/4*ystart);
    Modelica.Units.SI.SpecificEnthalpy h(start=hstart);
    Modelica.Units.SI.Pressure p(start=pstart);
    Modelica.Units.SI.DerDensityByPressure drdp;
    Modelica.Units.SI.DerDensityByEnthalpy drdh;
    Modelica.Units.SI.SpecificEnthalpy hsf;
    Modelica.Units.SI.SpecificEnthalpy hsg;
    Modelica.Units.SI.Density rho;
    Modelica.Units.SI.MassFlowRate ms;
    Modelica.Units.SI.SpecificEnthalpy hs;
    Modelica.Units.SI.MassFlowRate msp;
    Modelica.Units.SI.MassFlowRate mvlv;
    Modelica.Units.SI.Power Qh;
    Modelica.Units.SI.PerUnit x;
    Modelica.Units.SI.Mass Ml;
    Modelica.Units.SI.Length y(start=ystart,stateSelect=StateSelect.prefer);
    Medium.ThermodynamicState state;
    Medium.SaturationProperties sat;
    ThermoPower.Water.FlangeA bottomFlange annotation (
      Placement(visible = true, transformation(origin = {0, -96}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin={0,-104},    extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealOutput Pressure annotation (
      Placement(visible = true, transformation(origin={-108,0},   extent = {{-10, -10}, {10, 10}}, rotation=180), iconTransformation(origin={-86,0},    extent = {{-10, -10}, {10, 10}}, rotation=180)));
    Modelica.Blocks.Interfaces.RealInput m_spray annotation (
      Placement(visible = true, transformation(origin={0,106},      extent = {{-20, -20}, {20, 20}}, rotation=270), iconTransformation(origin={1,101},      extent = {{-11, -11}, {11, 11}}, rotation=270)));
    Modelica.Blocks.Interfaces.RealInput Qheater annotation (
      Placement(visible = true, transformation(origin={104,0},       extent={{20,-20},
                {-20,20}},                                                                            rotation = 0), iconTransformation(origin={80,-48},               extent = {{-12, -12}, {12, 12}}, rotation=180)));

    constant Real pi=Modelica.Constants.pi "3.14159...";

      ThermoPower.Water.FlangeB relief
        annotation (Placement(transformation(extent={{-92,66},{-72,86}}),
            iconTransformation(extent={{-92,66},{-72,86}})));
    equation
      // Mass balance
      V*(drdh*der(h) + drdp*der(p)) = ms + msp + mvlv;
      // Energy balance
      V*h*(drdh*der(h) + drdp*der(p)) + V*rho*der(h) - V*der(p) = ms*hs + msp*hsp + mvlv*h + Qh;
      // Fluid Properties
      state = Medium.setState_phX(p, h);
      drdp = Medium.density_derp_h(state);
      drdh = Medium.density_derh_p(state);
      rho = Medium.density(state);
      // Closure relations
      hs = if ms >= 0 then hsf else h;
      // Boundary conditions
      Pressure = p;
      m_spray = msp;
      Qh = Qheater;
      ms = bottomFlange.m_flow;
      hs = bottomFlange.h_outflow;
      bottomFlange.p = p;
      relief.m_flow = mvlv;
      relief.h_outflow = h;
      relief.p = p;
      // Saturation conditions
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      hsf = Medium.bubbleEnthalpy(sat);
      hsg = Medium.dewEnthalpy(sat);
      x = if h <= hsf then 0 elseif h >= hsg then 1 else (h - hsf)/(hsg - hsf);
      Ml = (1 - x)*V*rho;
      Vl = Ml/Medium.bubbleDensity(sat);
      if Geometry == "Cylinder" then
        V = pi*D^2/4*H;
        y = Vl/(pi*D^2/4);
      else
        V = 1/6*pi*Hemi*(3*(D/2)^2 + Hemi^2) + pi*D^2/4*H;
        if Vl <= pi*D^2/4*H then
          y = Vl/(pi*D^2/4);
        else
          V - Vl = pi*(H + Hemi - y)^2/3*(3*(((D/2)^2 + Hemi^2)/2/Hemi) - (H + Hemi - y));
        end if;
      end if;
    initial equation
      p = pstart;
      y = ystart;

      annotation (Icon(graphics={Line(
              points={{-198,66},{-116,62},{-162,20},{-132,110},{-92,74},{-92,34}},
              color={0,0,0},
              pattern=LinePattern.None,
              thickness=1), Line(
              points={{-148,98},{-112,42}},
              color={0,0,0},
              pattern=LinePattern.None,
              thickness=1),
            Ellipse(
              extent={{-80,-40},{80,-100}},
              lineColor={0,0,0},
              lineThickness=1,
              fillColor={0,0,255},
              fillPattern=FillPattern.Sphere,
              startAngle=0,
              endAngle=360,
              closure=EllipseClosure.Chord),
            Ellipse(
              extent={{-80,104},{80,44}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.Sphere,
              startAngle=0,
              endAngle=360,
              closure=EllipseClosure.Chord,
              fillColor={192,220,255}),
            Rectangle(
              extent={{-80,74},{80,-70}},
              lineColor={0,0,0},
              lineThickness=1,
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-79.4,24},{79.4,-72}},
              lineThickness=1,
              fillColor={0,0,255},
              fillPattern=FillPattern.VerticalCylinder,
              pattern=LinePattern.None),
            Rectangle(
              extent={{-80,76},{80,24}},
              lineThickness=1,
              fillColor={192,220,255},
              fillPattern=FillPattern.VerticalCylinder,
              pattern=LinePattern.None),
            Line(
              points={{80,24},{-80,24}},
              color={0,0,0},
              thickness=1,
              smooth=Smooth.Bezier),
            Rectangle(
              extent={{-72,-56},{74,-58}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-52},{74,-54}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-48},{74,-50}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-44},{74,-46}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-40},{74,-42}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-36},{74,-38}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0}),
            Rectangle(
              extent={{-72,-32},{74,-34}},
              lineColor={0,0,0},
              lineThickness=1,
              fillPattern=FillPattern.HorizontalCylinder,
              fillColor={255,128,0})}));
    end PRZeq;
  end BaseClasses;
end Pressurizer;
