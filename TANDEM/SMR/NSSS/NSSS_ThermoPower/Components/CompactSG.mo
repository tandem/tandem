within TANDEM.SMR.NSSS.NSSS_ThermoPower.Components;
model CompactSG
  //extends HeatTransfer.Utilities.HX_icon;

  parameter Integer Nodes=10 "Number of volumes"  annotation(Dialog(group="Geometry"));
  parameter Integer N_SG=6 "Number of Steam Generators"  annotation(Dialog(group="Geometry"));
  parameter Modelica.Units.SI.Length L=2.6102 "Port-to-port length"
    annotation (Dialog(group="Geometry"));

  // Rectangular channels
  parameter Modelica.Units.SI.Length a=4e-3 "Channel width"
    annotation (Dialog(group="Geometry"));
  parameter Modelica.Units.SI.Length b=2e-3 "Channel height"
    annotation (Dialog(group="Geometry"));
   parameter Integer Nch=35552 "Number of channels" annotation(Dialog(group="Geometry"));

  parameter Modelica.Units.SI.Length t=1e-3 "Plate thickness"
    annotation (Dialog(group="Plates"));
  parameter Modelica.Units.SI.Density rho_plate=4540 "Plate density"
    annotation (Dialog(group="Plates"));
  parameter Modelica.Units.SI.SpecificHeatCapacity cp_plate=544.284
    "Plate specific heat" annotation (Dialog(group="Plates"));
  parameter Modelica.Units.SI.ThermalConductivity k_plate=17
    "Plate thermal conductivity" annotation (Dialog(group="Plates"));

  parameter Modelica.Units.SI.MassFlowRate mdot1=3700
    "Nominal mass flow rate" annotation (Dialog(group="Primary side"));
  parameter Modelica.Units.SI.Pressure p1=150e5 "Nominal pressure"
    annotation (Dialog(group="Primary side"));

    parameter ThermoPower.Choices.Flow1D.FFtypes Primary_dp= ThermoPower.Choices.Flow1D.FFtypes.NoFriction
    "Friction Factor Type"
    annotation (Dialog(group="Primary side"),Evaluate=true);

    parameter Modelica.Units.SI.PressureDifference dp1 = 0 "Nominal pressure drop on primary side" annotation (
    Dialog(group="Primary side"));

    parameter Real Kfnom1 = 0
    "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
   annotation(Dialog(group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
  parameter Modelica.Units.SI.Density rhonom1=0 "Nominal inlet density"
    annotation(Dialog(group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
  parameter Modelica.Units.SI.PerUnit Cfnom1=0 "Nominal Fanning friction factor"
    annotation(Dialog(group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
  parameter Modelica.Units.SI.PerUnit e1=0 "Relative roughness (ratio roughness/diameter)"
    annotation(Dialog(group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));

  parameter Modelica.Units.SI.MassFlowRate mdot2=294.7276
    "Nominal mass flow rate" annotation (Dialog(group="Secondary side"));
  parameter Modelica.Units.SI.Pressure p2=45e5 "Nominal pressure"
    annotation (Dialog(group="Secondary side"));

     parameter ThermoPower.Choices.Flow1D.FFtypes Secondary_dp= ThermoPower.Choices.Flow1D.FFtypes.NoFriction
    "Friction Factor Type"
    annotation (Dialog(group="Secondary side"),Evaluate=true);

    parameter Modelica.Units.SI.PressureDifference dp2 = 0 "Nominal pressure drop on primary side" annotation (
    Dialog(group="Secondary side"));

    parameter Real Kfnom2 = 0
    "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
   annotation(Dialog(group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
  parameter Modelica.Units.SI.Density rhonom2=0 "Nominal inlet density"
    annotation(Dialog(group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
  parameter Modelica.Units.SI.PerUnit Cfnom2=0 "Nominal Fanning friction factor"
    annotation(Dialog(group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
  parameter Modelica.Units.SI.PerUnit e2=0 "Relative roughness (ratio roughness/diameter)" annotation(Dialog(group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));

  replaceable model Primary_HT =
      ThermoPower.Thermal.HeatTransferFV.DittusBoelter (
       heating=false) constrainedby
    ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
    annotation (Dialog(group="Primary side"), choicesAllMatching=true);

   replaceable model Secondary_HT =
      ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (gamma=1.748e4)
    constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
    annotation (Dialog(group="Secondary side"), choicesAllMatching=true);

    parameter Modelica.Units.SI.Power Qnom=540e6 "Nominal exchanged power" annotation(Dialog(group="Initialisation"));
    parameter Modelica.Units.SI.SpecificEnthalpy hstart_in1=1.486e6 "Primary side inlet" annotation(Dialog(group="Initialisation"));
    parameter Modelica.Units.SI.SpecificEnthalpy hstart_out1=1.338e6 "Primary side outlet" annotation(Dialog(group="Initialisation"));
    parameter Modelica.Units.SI.SpecificEnthalpy hstart_in2=694305.44 "Secondary side inlet" annotation(Dialog(group="Initialisation"));
    parameter Modelica.Units.SI.SpecificEnthalpy hstart_out2=2898889.2 "Secondary side outlet" annotation(Dialog(group="Initialisation"));

  ThermoPower.Water.Flow1DFV Primary(
    N=Nodes+1,
    Nw=Nodes,
    Nt=integer(Nch/2*N_SG),
    L=L,
    H=-L,
    A=a*b,
    omega=2*a,
    Dhyd=4*a*b/(2*(a+b)),
    wnom=mdot1,
    FFtype=Primary_dp,
    dpnom(displayUnit="kPa") = dp1,
    Kfnom = Kfnom1,
    rhonom = rhonom1,
    Cfnom = Cfnom1,
    e=e1,
    pstart=p1,
    hstartin=hstart_in1,
    hstartout=hstart_out1,
    noInitialPressure=true,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer = Primary_HT,
    Q(start=-Qnom))         annotation (Placement(transformation(
        extent={{-18,-18},{18,18}},
        rotation=270,
        origin={-60,0})));

  ThermoPower.Water.FlangeA flangeA(h_outflow(start=hstart_in1), m_flow(start=
          mdot1))
    annotation (Placement(transformation(extent={{-90,70},{-70,90}}),
        iconTransformation(extent={{-90,70},{-70,90}})));
  ThermoPower.Water.FlangeB flangeB(
    h_outflow(start=hstart_out1),
    m_flow(start=-mdot1),
    p(start=p1 - dp1))
    annotation (Placement(transformation(extent={{-88,-90},{-68,-70}}),
        iconTransformation(extent={{-88,-90},{-68,-70}})));
  ThermoPower.Thermal.CounterCurrentFV counterCurrentFV(Nw=Nodes, redeclare
      model HeatExchangerTopology =
        ThermoPower.Thermal.HeatExchangerTopologies.ShellAndTube (
        Nw=Nodes,
        Ntp=1,
        inletTubeAtTop=false,
        inletShellAtTop=true))                                    annotation (
      Placement(transformation(
        extent={{-18.5,-16.5},{18.5,16.5}},
        rotation=90,
        origin={-20.5,0.5})));
  ThermoPower.Water.Flow1DFV2ph Secondary(
    N=Nodes + 1,
    Nw=Nodes,
    Nt=integer(Nch/2*N_SG),
    L=L,
    H=L,
    A=a*b,
    omega=2*a,
    Dhyd=4*a*b/(2*(a + b)),
    wnom=mdot2,
    FFtype=Secondary_dp,
    dpnom=dp2,
    Kfnom = Kfnom2,
    rhonom = rhonom2,
    Cfnom = Cfnom2,
    e=e2,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
    pstart=p2,
    hstartin=hstart_in2,
    hstartout=hstart_out2,
    noInitialPressure=true,
    redeclare model HeatTransfer = Secondary_HT,
    fixedMassFlowSimplified=true,
    Q(start=Qnom))
    annotation (Placement(transformation(
        extent={{-18,-18},{18,18}},
        rotation=90,
        origin={58,2})));

  ThermoPower.Water.FlangeA flangeA1(
    h_outflow(start=hstart_in2),
    m_flow(start=mdot2),
    p(start=p2))                     annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={100,-82}),  iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={80,-80})));
  ThermoPower.Water.FlangeB flangeB1(
    h_outflow(start=hstart_out2),
    m_flow(start=-mdot2),
    p(start=p2 - dp2))               annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={100,80}),  iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={78,80})));
  ThermoPower.Thermal.MetalWallFV metalWallFV(
    Nw=Nodes,
    M=N_SG*Nch*L*((a + t/2)*(b + t/2) - a*b)*rho_plate,
    cm=cp_plate,
    WallRes=true,
    UA_ext=N_SG*k_plate/t*2*a*L*Nch,
    Tstartbar=473.15)
                 annotation (Placement(transformation(
        extent={{-15,-15},{15,15}},
        rotation=90,
        origin={21,1})));

  ThermoPower.Water.SensT1 Tp_in(T(start=324.7+273.15)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-56,52})));
  ThermoPower.Water.SensT1 Tp_out(T(start=300+273.15)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-56,-48})));
  ThermoPower.Water.SensT1 Ts_in(T(start=163+273.15)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={54,-46})));
  ThermoPower.Water.SensT1 Ts_out(T(start=300+273.15)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={54,48})));

equation

  connect(flangeA1, flangeA1)
    annotation (Line(points={{100,-82},{100,-82}},
                                               color={0,0,255}));
  connect(Primary.wall, counterCurrentFV.side1) annotation (Line(points={{-51,0},
          {-25.45,0},{-25.45,0.5}},                          color={255,127,0}));
  connect(metalWallFV.ext, Secondary.wall)
    annotation (Line(points={{25.65,1},{24,1},{24,0},{49,0},{49,2}},
                                                       color={255,127,0}));
  connect(counterCurrentFV.side2, metalWallFV.int) annotation (Line(points={{-15.385,
          0.5},{-14,1},{16.5,1}},             color={255,127,0}));
  connect(flangeB, Primary.outfl) annotation (Line(points={{-78,-80},{-60,-80},
          {-60,-18}}, color={0,0,255}));
  connect(flangeA, Primary.infl)
    annotation (Line(points={{-80,80},{-60,80},{-60,18}},    color={0,0,255}));
  connect(Secondary.infl, flangeA1)
    annotation (Line(points={{58,-16},{58,-82},{100,-82}},   color={0,0,255}));
  connect(Secondary.outfl, flangeB1) annotation (Line(points={{58,20},{58,80},{100,
          80}},               color={0,0,255}));
  connect(flangeB, flangeB)
    annotation (Line(points={{-78,-80},{-78,-80}},     color={0,0,255}));
  connect(flangeA, Tp_in.flange)
    annotation (Line(points={{-80,80},{-60,80},{-60,52}}, color={0,0,255}));
  connect(flangeB, Tp_out.flange)
    annotation (Line(points={{-78,-80},{-60,-80},{-60,-48}}, color={0,0,255}));
  connect(Secondary.infl, Ts_in.flange)
    annotation (Line(points={{58,-16},{58,-46}}, color={0,0,255}));
  connect(Secondary.outfl, Ts_out.flange)
    annotation (Line(points={{58,20},{58,48}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,120}}), graphics={
        Line(points={{-40,120}}, color={243,243,243}),
        Line(
          points={{170,38},{170,-70},{170,42},{170,40}},
          color={0,0,0},
          pattern=LinePattern.None,
          thickness=0.5),
        Polygon(points={{-58,82},{-58,82}}, lineColor={28,108,200}),
        Polygon(points={{-60,80},{-60,80}}, lineColor={28,108,200}),
        Polygon(
          points={{-32,94},{-32,94}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Polygon(points={{-58,72},{-58,72}}, lineColor={28,108,200}),
        Polygon(points={{-60,70},{-60,70}}, lineColor={28,108,200}),
        Polygon(
          points={{-50,6},{-50,6}},
          lineColor={140,144,145},
          fillColor={233,233,233},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-80,102},{80,72}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Ellipse(
          extent={{-80,-66},{80,-98}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.Sphere),
        Rectangle(
          extent={{-80,88},{80,-82}},
          lineColor={95,95,95},
          pattern=LinePattern.None,
          fillColor={215,215,215},
          fillPattern=FillPattern.VerticalCylinder),
        Line(
          points={{-28,-78},{-28,-38},{-4,-18},{-52,22},{-28,42},{-28,82}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{28,82},{28,42},{8,22},{52,-18},{28,-38},{28,-78}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{-28,82},{-74,82}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{-28,-78},{-74,-78}},
          color={226,26,28},
          thickness=1),
        Line(
          points={{74,-78},{28,-78}},
          color={0,0,255},
          thickness=1),
        Line(
          points={{74,82},{28,82}},
          color={0,0,255},
          thickness=1)}),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}})));
end CompactSG;
