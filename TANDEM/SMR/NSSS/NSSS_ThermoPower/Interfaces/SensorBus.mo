within TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces;
expandable connector SensorBus

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Line(
            points={{-14,0},{18,0}},
            color={255,219,88},
            thickness=0.5),
          Rectangle(
            lineColor={255,219,88},
            lineThickness=0.5,
            extent={{-8,-2},{10,6}}),
          Polygon(
          fillColor={255,219,88},
          fillPattern=FillPattern.Solid,
          points={{-78,48},{82,48},{102,28},{82,-42},{62,-52},{-58,-52},{-78,
              -42},{-98,28}},
          smooth=Smooth.Bezier),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{-53,13},{-43,23}}),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{47,13},{57,23}}),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{-3,-27},{7,-17}}),
          Rectangle(
            lineColor={255,219,88},
            lineThickness=0.5,
            extent={{-18,-2},{22,2}})}),                         Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end SensorBus;
