within TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces;
expandable connector ActuatorBus

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Line(
            points={{-16,2},{16,2}},
            color={80,200,120},
            thickness=0.5),
          Rectangle(
            lineColor={80,200,120},
            lineThickness=0.5,
            extent={{-10,0},{8,8}}),
          Polygon(
          fillColor={80,200,120},
          fillPattern=FillPattern.Solid,
          points={{-80,50},{80,50},{100,30},{80,-40},{60,-50},{-60,-50},{-80,
              -40},{-100,30}},
          smooth=Smooth.Bezier),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{-55,15},{-45,25}}),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{45,15},{55,25}}),
          Ellipse(
            fillPattern=FillPattern.Solid,
            extent={{-5,-25},{5,-15}}),
          Rectangle(
            lineColor={80,200,120},
            lineThickness=0.5,
            extent={{-20,0},{20,4}})}),                          Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ActuatorBus;
