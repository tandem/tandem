within TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces;
partial model Fluid_interface


  // General
  parameter Integer Nax = 10 "Axial discretization" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Integer Nrad = 5 "Fuel radial discretization" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Modelica.Units.SI.Power Pnom = 540e6 "Nominal power" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Modelica.Units.SI.MassFlowRate wnom = 3700 "Nominal mass flow rate" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Modelica.Units.SI.Pressure pnom = 150e5 "Nominal pressure" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Modelica.Units.SI.Temperature Tin_nom = 300 + 273.15 "Nominal inlet temperature" annotation (
    Dialog(tab = "Core", group="General"));
  parameter Modelica.Units.SI.Temperature Tout_nom = 324.79 + 273.15 "Nominal outlet temperature" annotation (
    Dialog(tab = "Core", group="General"));

  // Geometry
  parameter Integer Nch = 21964 "Number of coolant channels" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Integer Nf = 20064 "Number of fuel rods" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length H = 2.159 "Core length" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length Ha = 2 "Active length" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length He = Ha "Extrapolated length" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length pitch = 0.012598 "Fuel rod pitch" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length Rf = 4.05765E-03 "Fuel radius" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length Rci = 4.1402E-03 "Cladding inner radius" annotation (
    Dialog(tab = "Core", group="Geometry"));
  parameter Modelica.Units.SI.Length Rco = 4.7498E-03 "Cladding outer radius" annotation (
    Dialog(tab = "Core", group="Geometry"));

  // Coolant flow
  parameter ThermoPower.Choices.Flow1D.FFtypes core_dp= ThermoPower.Choices.Flow1D.FFtypes.Cfnom
    "Friction Factor Type" annotation(Dialog(tab = "Core", group="Coolant flow"),Evaluate=true);
  parameter Modelica.Units.SI.PressureDifference dpnom = 22280
    "Nominal pressure drop (friction term only!)" annotation(Dialog(tab = "Core", group="Coolant flow"));
  parameter Real Kfnom = 0
    "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
   annotation(Dialog(tab = "Core", group="Coolant flow",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
  parameter Modelica.Units.SI.Density rhonom=0 "Nominal inlet density"
    annotation(Dialog(tab = "Core", group="Coolant flow",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
  parameter Modelica.Units.SI.PerUnit Cfnom=0.0037 "Nominal Fanning friction factor"
    annotation(Dialog(tab = "Core", group="Coolant flow",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
  parameter Modelica.Units.SI.PerUnit e=0 "Relative roughness (ratio roughness/diameter)"
    annotation(Dialog(tab = "Core", group="Coolant flow",enable = (core_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));

   // Bypass
   parameter Modelica.Units.SI.PerUnit BP_frac = 0.04054 "Nominal fraction of flow rate entering bypass channel"
                                                                                                                annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter ThermoPower.Units.HydraulicResistance R_bp = 40.3325875 "Hydraulic resistance" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Modelica.Units.SI.PressureDifference dpnom_bp = 0.15367e5 "Nominal pressure drop" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Modelica.Units.SI.PerUnit Cfnom_bp=0.003444 "Nominal Fanning friction factor" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Modelica.Units.SI.Area A_bp = 0.64464 "Bypass channel flow area" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Modelica.Units.SI.Length Dh_bp = 0.1613188715 "Bypass channel hydraulic diameter" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Modelica.Units.SI.PressureDifference dpnom_conc = 0.06049888e5 "Nominal concentrated losses" annotation(Dialog(tab="Core",group="Bypass channel"));
   parameter Real Kf_conc = 195.0896 "Concentrated loss coefficient" annotation(Dialog(tab="Core",group="Bypass channel"));

  // Neutronics
  parameter
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.Neutronics.KineticParameters KP=
      TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.Neutronics.SixGroups_ESMR
    "Precursor group kinetic data" annotation (Dialog(tab="Core", group="Neutronics"),
      choices(choice=NSSS.Components.Core.Components.BaseClasses.EightGroups_ESMR
        "E-SMR neutronic parameters"));
  parameter Modelica.Units.SI.MacroscopicCrossSection SigmaF(displayUnit = "1/(cm)") = 63.1259758 "Macroscopic fission cross section" annotation (
    Dialog(tab = "Core", group="Neutronics"));
  parameter Modelica.Units.SI.ReactionEnergy wF = 202.509e6*1.60218e-19 "Energy yield per fission" annotation (
    Dialog(tab = "Core", group="Neutronics"));
  parameter Modelica.Units.SI.NeutronYieldPerFission nu = 2.46261 "Neutron yield per fission" annotation (
    Dialog(tab = "Core", group="Neutronics"));
  parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.Reactivity_pcm rho0=0
    "Initial reactivity margin"
    annotation (Dialog(tab="Core", group="Neutronics"));
  parameter Boolean TemperatureFeedbacks = true annotation (
    Dialog(tab = "Core", group="Neutronics"),
    choices(checkBox = true));
  parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByTemperature alfa_D=-2
    "Doppler feedback coefficient" annotation (Dialog(
      tab="Core",
      group="Neutronics",
      enable=TemperatureFeedbacks));
  parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByTemperature alfa_C=-60
    "Coolant temperature feedback coefficient" annotation (Dialog(
      tab="Core",
      group="Neutronics",
      enable=TemperatureFeedbacks));
  parameter Modelica.Units.SI.Temperature T0_Doppler = if UniformPower then 705.877 + 273.15 else 725.94 + 273.15 "Reference fuel temperature (non-steady state conditions)" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = TemperatureFeedbacks and not initOpt == ThermoPower.Choices.Init.Options.steadyState));
  parameter Modelica.Units.SI.Temperature T0_Coolant = 312.7 + 273.15 "Reference coolant temperature (non-steady state conditions)" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = TemperatureFeedbacks and not initOpt == ThermoPower.Choices.Init.Options.steadyState));
  parameter Boolean XenonFeedback = true annotation (
    Dialog(tab = "Core", group="Neutronics"),
    choices(checkBox = true));
  parameter Real gammaI = 0.0639 "I fission yield" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = XenonFeedback));
  parameter Modelica.Units.SI.DecayConstant lambdaI = 2.92615E-05 "I decay constant" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = XenonFeedback));
  parameter Real gammaXe = 0.00237 "Xe fission yield" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = XenonFeedback));
  parameter Modelica.Units.SI.DecayConstant lambdaXe = 2.10657E-05 "Xe decay constant" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = XenonFeedback));
  parameter Modelica.Units.SI.CrossSection sigmaXe = 2.71e-22 "Xe absorption cross section" annotation (
    Dialog(tab = "Core", group="Neutronics", enable = XenonFeedback));
  parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion ReactivityInsertion=
      TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.Reactivity
    annotation (Dialog(tab="Core", group="Neutronics"), choicesAllMatching=true);
  parameter TANDEM.SMR.NSSS.NSSS_ThermoPower.Units.ReactivityByLength alfa_H=0
    "Coefficient associated with the insertion length of an ideal control rod"
    annotation (Dialog(
      tab="Core",
      group="Neutronics",
      enable=ReactivityInsertion == NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
  parameter Modelica.Units.SI.Length h_0 = 0 "Reference position of the ideal control rod" annotation (
    Dialog(tab = "Core", group="Neutronics", enable=ReactivityInsertion ==
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_proportional));
  parameter Real A_CR = 0 "CR calibration curve coefficient (A*sin(B*h+C)+D" annotation (
    Dialog(tab = "Core", group="Neutronics", enable=ReactivityInsertion ==
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
  parameter Real B_CR = 0 "CR calibration curve coefficient" annotation (
    Dialog(tab = "Core", group="Neutronics", enable=ReactivityInsertion ==
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
  parameter Real C_CR = 0 "CR calibration curve coefficient" annotation (
    Dialog(tab = "Core", group="Neutronics", enable=ReactivityInsertion ==
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
  parameter Real D_CR = 0 "CR calibration curve coefficient" annotation (
    Dialog(tab = "Core", group="Neutronics", enable=ReactivityInsertion ==
          NSSS_ThermoPower.Components.Core.Utilities.ReactivityInsertion.CRpos_calibration));
  parameter Boolean UniformPower = true "Uniform power distribution in the core" annotation (
    Dialog(tab = "Core", group="Neutronics"),
    choices(checkBox = true));
  parameter Boolean dynamicPromptNeutrons = true "Dynamic balance for prompt neutron density" annotation (
    Dialog(tab = "Core", group="Neutronics"),
    choices(checkBox = true));
  parameter Boolean NeutronSource = true "Use connector input for the external source" annotation (
    Dialog(tab = "Core", group="Neutronics"),
    choices(checkBox = true));
  // Initialisation
  outer ThermoPower.System system "System wide properties";
  parameter ThermoPower.Choices.Init.Options initOpt=system.initOpt
    "Initialisation option" annotation (Dialog(tab = "Core", group="Initialization"));
  parameter Modelica.Units.SI.SpecificEnthalpy hin_start = Modelica.Media.Water.IF97_Utilities.h_pT(pnom+dpnom, Tin_nom) "Initial inflow enthalpy" annotation (
    Dialog(tab = "Core", group="Initialization"));
  parameter Modelica.Units.SI.SpecificEnthalpy hout_start = Modelica.Media.Water.IF97_Utilities.h_pT(pnom, Tout_nom) "Initial outflow enthalpy" annotation (
    Dialog(tab = "Core", group="Initialization"));
  parameter Boolean steadyState = true "Steady state initialization for the fuel temperatures" annotation (
    Dialog(tab = "Core", group="Initialization"),
    choices(checkBox = true));
  replaceable parameter
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.IC_inputs.IC_Fuel initFuel=
      TANDEM.SMR.NSSS.NSSS_ThermoPower.Data.Core.IC_inputs.uniform_Ax10Rad5
    annotation (Dialog(tab="Core", group="Initialization"), choices(
      choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad3,
      choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad3,
      choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad5,
      choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad5,
      choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax10Rad10,
      choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax10Rad10,
      choice=NSSS.Components.Core.Data.IC_inputs.uniform_Ax20Rad5,
      choice=NSSS.Components.Core.Data.IC_inputs.profile_Ax20Rad5));

  parameter Modelica.Units.SI.Temperature Tf_start[Nax, Nrad] = initFuel.Tf_start annotation (
    Dialog(tab = "Core", group="Initialization"),
    choices(choice = 1000*ones(core_data.Nax, core_data.Nrad) "Steady state", choice = core_data.initFuel.Tf_start "Recorded value"));
  parameter Modelica.Units.SI.Temperature Tc_start[Nax] = initFuel.Tc_start annotation (
    Dialog(tab = "Core", group="Initialization"),
    choices(choice = 600*ones(core_data.Nax) "Steady state", choice = core_data.initFuel.Tc_start "Recorded value"));
  parameter Real Nstart = Pnom*KP.LAMBDA*nu/wF/(Nf*Ha*pi*Rf^2) "Initial normalized neutron density" annotation (
    Dialog(tab = "Core", group="Initialization"));
  parameter Real Dstart[KP.NPG] = (KP.beta*Nstart)./(KP.LAMBDA.*KP.lambda) "Start value of normalized precursor density" annotation (
    Dialog(tab = "Core", group="Initialization"));

  // Pressurizer
  parameter String Geometry= "Hemisphere" annotation(Dialog(tab="Pressurizer",group="Geometry"), choices(choice="Cylinder", choice="Hemisphere"));
  parameter Modelica.Units.SI.Length Dpress=if Geometry=="Cylinder" then 1.37 else 3.65 "Pressurizer inner diameter" annotation(Dialog(tab="Pressurizer",group="Geometry"));
  parameter Modelica.Units.SI.Length Hpress=if Geometry=="Cylinder" then 5.1 else 1 "Height of the cyclindrical part" annotation(Dialog(tab="Pressurizer",group="Geometry"));
  parameter Modelica.Units.SI.Length Hemi=1 "Height of the hemispherical part" annotation(Dialog(tab="Pressurizer",group="Geometry", enable=Geometry=="Hemisphere"));

    parameter Modelica.Units.SI.SpecificEnthalpy hsp=1.33806e+06 "Sprayers enthalpy" annotation(Dialog(tab="Pressurizer",group="Control"));
  parameter ThermoPower.Units.HydraulicConductance Kv = 1e3 "Relief valve hydraulic resistance" annotation(Dialog(tab="Pressurizer",group="Control"));
  parameter Modelica.Units.SI.Pressure pvlv = 150e5 "Relief valve sink pressure" annotation(Dialog(tab="Pressurizer",group="Control"));
  parameter Modelica.Units.SI.Pressure pstart_press=150e5 "Pressure start value"
    annotation (Dialog(tab="Pressurizer",group="Initialization"));
  parameter Modelica.Units.SI.Length ystart_press=if Geometry=="Cylinder" then 2.482 else 0.5 "Liquid level start value"
    annotation (Dialog(tab="Pressurizer",group="Initialization"));
   parameter Modelica.Units.SI.SpecificEnthalpy hstart_press=1.86213e6 "Enthalpy start value" annotation (Dialog(tab="Pressurizer",group="Initialization"));

  // Discretization
  parameter Integer Nax_rd = 10 "Riser and downcomer axial discretization" annotation (
    Dialog(tab="Coolant flow"));
  parameter Integer Nax_sg = 10 "SG axial discretization" annotation (
    Dialog(tab="Steam Generator", group="General"));

   parameter Integer N_sg = 6 "Number of steam generators" annotation (
    Dialog(tab="Steam Generator", group="General"));
   parameter Modelica.Units.SI.MassFlowRate mdot1 = 3700 "Nominal flow rate on primary side" annotation (
    Dialog(tab="Steam Generator", group="Primary side"));
     parameter ThermoPower.Choices.Flow1D.FFtypes Primary_dp= ThermoPower.Choices.Flow1D.FFtypes.Cfnom
    "Friction Factor Type"
    annotation (Dialog(tab="Steam Generator", group="Primary side"),Evaluate=true);
   parameter Modelica.Units.SI.Pressure p1 = 150e5 "Nominal pressure on primary side" annotation (
    Dialog(tab="Steam Generator", group="Primary side"));
   parameter Modelica.Units.SI.PressureDifference dp1 = 2e5 "Nominal pressure drop on primary side" annotation (
    Dialog(tab="Steam Generator", group="Primary side"));
  parameter Real Kfnom1 = 0
    "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
   annotation(Dialog(tab="Steam Generator", group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
  parameter Modelica.Units.SI.Density rhonom1=0 "Nominal inlet density"
    annotation(Dialog(tab="Steam Generator", group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
  parameter Modelica.Units.SI.PerUnit Cfnom1=0.004 "Nominal Fanning friction factor"
    annotation(Dialog(tab="Steam Generator", group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
  parameter Modelica.Units.SI.PerUnit e1=0 "Relative roughness (ratio roughness/diameter)"
    annotation(Dialog(tab="Steam Generator", group="Primary side",enable = (Primary_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));

 parameter Modelica.Units.SI.MassFlowRate mdot2 = 240 "Nominal flow rate on secondary side" annotation (
    Dialog(tab="Steam Generator", group="Secondary side"));


    parameter Modelica.Units.SI.CoefficientOfHeatTransfer htc2 = 15000 "Secondary side heat transfer coefficient" annotation (
    Dialog(tab="Steam Generator", group="Secondary side"));


    parameter ThermoPower.Choices.Flow1D.FFtypes Secondary_dp= ThermoPower.Choices.Flow1D.FFtypes.Cfnom
    "Friction Factor Type"
    annotation (Dialog(tab="Steam Generator", group="Secondary side"),Evaluate=true);


  parameter Modelica.Units.SI.Pressure p2 = 45e5 "Nominal pressure on secondary side" annotation (
    Dialog(tab="Steam Generator", group="Secondary side"));
  parameter Modelica.Units.SI.PressureDifference dp2 = 4e5 "Nominal pressure drop on secondary side" annotation (
    Dialog(tab="Steam Generator", group="Secondary side"));
    parameter Real Kfnom2 = 0
    "Nominal hydraulic resistance coefficient (DP = Kfnom*w^2/rho)"
   annotation(Dialog(tab="Steam Generator", group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Kfnom)));
  parameter Modelica.Units.SI.Density rhonom2=0 "Nominal inlet density"
    annotation(Dialog(tab="Steam Generator", group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.OpPoint)));
  parameter Modelica.Units.SI.PerUnit Cfnom2=0.11 "Nominal Fanning friction factor"
    annotation(Dialog(tab="Steam Generator", group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Cfnom)));
  parameter Modelica.Units.SI.PerUnit e2=0 "Relative roughness (ratio roughness/diameter)" annotation(Dialog(tab="Steam Generator", group="Secondary side",enable = (Secondary_dp == ThermoPower.Choices.Flow1D.FFtypes.Colebrook)));

  // Geometry
  // Steam generator
  parameter Modelica.Units.SI.Length L_sg = 2 "SG length" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
   parameter Modelica.Units.SI.Length a_sg = 4e-3 "Channel width" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
  parameter Modelica.Units.SI.Length b_sg = 2e-3 "Channel heigth" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
  parameter Integer Nch_sg = 28818 "Number of channels" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
   parameter Modelica.Units.SI.Length t_sg = 1e-3 "Plate thickness" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
   parameter Modelica.Units.SI.Density rho_sg = 8190 "Plate density" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
   parameter Modelica.Units.SI.SpecificHeatCapacity cp_sg = 487.18 "Plate density" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));
   parameter Modelica.Units.SI.ThermalConductivity k_sg = 15.1 "Plate thermal conductivity" annotation (
    Dialog(tab="Steam Generator", group="Geometry"));

    // Pump
  parameter Integer Np0(min=1) = 6 "Nominal number of pumps in parallel" annotation (Dialog(tab="Pump"));

  parameter Real eta= 0.9 "Nominal efficiency" annotation (Dialog(tab="Pump"));

  parameter Modelica.Units.SI.VolumeFlowRate q_nom[3]={0,0.85035195,1.51494276} "Volume flow rate for three operating points (single pump)" annotation (Dialog(tab="Pump"));

  parameter Modelica.Units.SI.Height head_nom[3]={52.9277496,36.251883,0} "Pump head for three operating points" annotation(Dialog(tab="Pump"));

  parameter Modelica.Units.SI.Density rho_pump=726.51979 "Nominal Liquid Density"
    annotation (Dialog(tab="Pump",group="Characteristics"));
  parameter Modelica.Units.NonSI.AngularVelocity_rpm n0=1500 "Nominal rotational speed"
    annotation (Dialog(tab="Pump",group="Characteristics"));
  parameter Modelica.Units.SI.Volume V=2.8746 "Pump Internal Volume" annotation (Dialog(tab="Pump"),Evaluate=true);
  parameter Boolean CheckValve=false "Reverse flow stopped"
                                                           annotation (Dialog(tab="Pump"));

  parameter Modelica.Units.SI.MassFlowRate wstart=w0 "Mass Flow Rate Start Value"
    annotation (Dialog(tab="Pump",group="Initialisation"));
  parameter Modelica.Units.SI.SpecificEnthalpy hstart_pump = 1.33804e6
    "Specific Enthalpy Start Value"
    annotation (Dialog(tab="Pump",group="Initialisation"));
  parameter Boolean noInitialPressure_pump=false
    "Remove initial equation on pressure"
    annotation (Dialog(tab="Pump",group="Initialisation"),choices(checkBox=true));
  parameter Modelica.Units.SI.MassFlowRate w0=3700 "Nominal mass flow rate"
    annotation (Dialog(tab="Pump",group="Characteristics"));
  parameter Modelica.Units.SI.Pressure dp0=2.579e5 "Nominal pressure increase"
    annotation (Dialog(tab="Pump",group="Characteristics"));

  // Riser, Upper plenum, Downcomer, Lower plenum
  parameter Modelica.Units.SI.Length L_r = 5.58 "Riser length" annotation (
    Dialog(tab = "Coolant flow", group="Riser"));
  parameter Modelica.Units.SI.Area A_r = 0.787593239 "Riser cross sectional area" annotation (
    Dialog(tab = "Coolant flow",group="Riser"));
  parameter Modelica.Units.SI.Length Dhyd_r = 0.041961512 "Riser hydraulic diameter" annotation (
    Dialog(tab = "Coolant flow",group="Riser"));
  parameter Modelica.Units.SI.Length omega_r = 7.382742736 "Riser heated perimeter" annotation (
    Dialog(tab = "Coolant flow",group="Riser"));
  parameter Modelica.Units.SI.Volume V_up = 18.83424066 "Upper plenum volume" annotation (
    Dialog(tab = "Coolant flow",group="Upper plenum"));
  parameter Modelica.Units.SI.Length L_d = 5.2 "Downcomer length" annotation (
    Dialog(tab = "Coolant flow",group="Downcomer"));
  parameter Modelica.Units.SI.Area A_d = 5.749114556 "Downcomer cross sectional area" annotation (
    Dialog(tab = "Coolant flow",group="Downcomer"));
  parameter Modelica.Units.SI.Length Dhyd_d = 1.2 "Downcomer hydraulic diameter" annotation (
    Dialog(tab = "Coolant flow", group="Downcomer"));
  parameter Modelica.Units.SI.Length omega_d = 7.696902001 "Downcomer heated perimeter" annotation (
    Dialog(tab = "Coolant flow",group="Downcomer"));
  parameter Modelica.Units.SI.Volume V_lp = 3.487822344 "Lower plenum volume" annotation (
    Dialog(tab = "Coolant flow",group="Lower plenum"));
  parameter Modelica.Units.SI.Length H_lp = 0.5 "Lower plenum elevation" annotation (
    Dialog(tab = "Coolant flow",group="Lower plenum"));

//protected
  constant Real pi = Modelica.Constants.pi "3.14159...";

  ThermoPower.Water.FlangeA flangeA
    annotation (Placement(transformation(extent={{150,-72},{170,-52}})));
  ThermoPower.Water.FlangeB flangeB
    annotation (Placement(transformation(extent={{150,50},{170,70}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.SensorBus sensorBus
    annotation (Placement(transformation(extent={{70,88},{90,108}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-90,88},{-70,108}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -100},{160,100}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-100},{160,
            100}})));
end Fluid_interface;
