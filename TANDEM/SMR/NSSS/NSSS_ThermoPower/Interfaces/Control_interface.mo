within TANDEM.SMR.NSSS.NSSS_ThermoPower.Interfaces;
partial model Control_interface
  //extends NSSS.Components.Pressurizer.Utilities.Control;
  SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Control_interface;
