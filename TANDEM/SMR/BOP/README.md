# Conventional Island - Balance of Plant
Three versions of the balance of plant (BOP) are available in the TANDEM library, two based on the [ThermoPower](#Tpower) library and one on the [ThermoSysPro](#ElHefni) library.

These BOP models are employed to simulate a Rankine steam cycle, optimized using the [CYCLOP](#cyclop) tool. This tool provided the optimal thermodynamic points of the steam cycle, assuming the steam generator’s operating parameters as boundary conditions, to maximise the electrical power output . 

The multi-development of BoP models that was carried out, is justified by the need to validate coupling connectors from one library to another: indeed the BoP plays a pivotal role in power distribution and TANDEM library effective versatility is strongly depending on the point. Another key reason for the parallel (but coordinated) developments, is the need to provide different possibilities to explore the challenging coupling between TANDEM library and system codes (CATHARE or ATHLET) : consistently, the table below reports that different choices were made regarding thermal or fluid ports. Finally, the possibility of benchmarking and measuring the effect of inertia according to the type of transient could also be seen as two aspects of prime interest.  As such, the user may be more interested in having a calculation that is fast in terms of CPU (and therefore easily integrated into a global optimisation process of an energetical system) but quasi-static to address energetic performance, or on the contrary, with the possibility of fine representation of the dynamics to carefully analyse heat and electric networks service capacities.

The following table outlines the main distinguishing features of each BOP model.

| Features	| ThermoSysPro	| ThermoPower (1) |	ThermoPower (2) |
| --- | --- | --- | --- |
| Cycle control |	ST sliding pressure, Feedwater valve, Variable speed pump , MSR control valve |	Feedwater valve |	ST sliding pressure, Feedwater valve, Variable speed pumps, MSR control valve |
|Heat interfaces |	HP, MP & LP heat extractions |	HP, MP & LP heat extractions |	HP, MP & LP heat extractions
| SG interface modelling	| Fluid port	| Thermal port |	Thermal port |
| Heat extraction interface modelling |	Fluid port	 |Thermal port|	Fluid port|
|Interface control	| External by-pass valve |	Internal by-pass valves controlled on condensate subcooling	| External by-pass valve|
|Inertia modelling	| Quasi-static |	Reheaters	|All heat exchangers and drums
|Main limitations	|Control parameters are sub-optimal (but consistent with a QS approach)|	MSR reheater and condenser inertia omitted	


## References
- <a id="Tpower"></a> F. Casella, A. Leva. _Modelling of thermo-hydraulic power generation processes using Modelica_. Mathematical and Computer Modelling of Dynamical Systems - MATH COMPUT MODEL DYNAM SYST, 12, 19–33, 2006.
- <a id="ElHefni"></a> B. El Hefni, D. Bouskela. _Modeling and Simulation of Thermal Power Plants with ThermoSysPro: A Theoretical Introduction and a Practical Guide_. Springer, 2019. 
- <a id="cyclop"></a> D. Haubensack, C. Thévenot,  P. Dumaz. _The COPERNIC/CYCLOP computer tool: Pre-conceptual design of generation 4 nuclear systems_, 2005.
