within TANDEM.SMR.BOP.BOP_TSPro;
package Added_Component "Components adapted from TSPro library"
  model OD_HeatSource "OD heat Source"
    parameter Modelica.Units.SI.Power W=1e6 "Input electrical power";
    parameter Real eta = 100 "Boiler efficiency (percent)";
    parameter Modelica.Units.SI.Pressure deltaP=0 "Pressure loss";
    parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
    Modelica.Units.SI.Temperature Te(start=300) "Inlet temperature";
    Modelica.Units.SI.Temperature Ts(start=500) "Outlet temperature";
    Modelica.Units.SI.MassFlowRate Q(start=100) "Mass flow";
    Modelica.Units.SI.SpecificEnthalpy deltaH
      "Specific enthalpy variation between the outlet and the inlet";

    ThermoSysPro.WaterSteam.Connectors.FluidInlet Ce annotation (
      Placement(visible = true, transformation(origin = {-88, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-87, 8}, extent = {{-13, -14}, {13, 14}}, rotation = 0)));
    ThermoSysPro.WaterSteam.Connectors.FluidOutlet Cs annotation (
      Placement(visible = true, transformation(origin = {86, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {87, 8}, extent = {{-13, -14}, {13, 14}}, rotation = 0)));
    ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Signal_Elec annotation (
      Placement(visible = true, transformation(origin = {0, 72}, extent = {{-10, -10}, {10, 10}}, rotation = 270), iconTransformation(origin = {0, 88}, extent = {{-12, -12}, {12, 12}}, rotation = 270)));
    ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph Prop_e annotation (
      Placement(visible = true, transformation(origin = {-72, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph Prop_s annotation (
      Placement(visible = true, transformation(origin = {68, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Units.SI.Power We "Electrical power";
  equation
    if cardinality(Signal_Elec) == 0 then
      Signal_Elec.signal = W;
    end if;
    Ce.P - Cs.P = deltaP;
    Ce.Q = Cs.Q;
    Q = Ce.Q;
    Cs.h - Ce.h = deltaH;
    /* Flow reversal */
    0 = if Q > 0 then Ce.h - Ce.h_vol else Cs.h - Cs.h_vol;
    /* Specific enthalpy variation between the outlet and the inlet */
    We * eta / 100 = Q * deltaH;
    Signal_Elec.signal = We;
    /* Fluid thermodynamic properties */
    Prop_e = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph(Ce.P, Ce.h, mode);
    Prop_s = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph(Cs.P, Cs.h, mode);
    Te = Prop_e.T;
    Ts = Prop_s.T;
    annotation (
      Icon(graphics={  Rectangle(origin = {7, 20}, lineColor = {255, 0, 0}, fillColor = {255, 255, 0},
              fillPattern =                                                                                          FillPattern.Sphere, extent = {{-87, 60}, {73, -66}})}, coordinateSystem(initialScale = 0.1)),
      Diagram(graphics={  Rectangle(origin = {5, 4}, lineColor = {255, 0, 0}, fillColor = {255, 255, 0},
              fillPattern =                                                                                            FillPattern.Sphere, extent = {{-87, 60}, {73, -66}})}, coordinateSystem(initialScale = 0.1)),
      Documentation(info="<html>
<p>OD heat source </p>
</html>"));
  end OD_HeatSource;

  model StaticCondenserThermalPort "Simple static condenser with thermal port"
    parameter Real Kc=10 "Friction pressure loss coefficient for the hot side";
    parameter ThermoSysPro.Units.SI.Position z1c=0 "Hot inlet altitude";
    parameter ThermoSysPro.Units.SI.Position z2c=0 "Hot outlet altitude";
    parameter Boolean continuous_flow_reversal=false
      "true: continuous flow reversal - false: discontinuous flow reversal";
    parameter Integer modec=0
      "IF97 region of the water for the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
    parameter Integer modecs=0
      "IF97 region of the water at the outlet of the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";

  protected
    constant ThermoSysPro.Units.SI.Acceleration g=Modelica.Constants.g_n
      "Gravity constant";
    constant Real pi=Modelica.Constants.pi "pi";
    parameter Real eps=1.e-0 "Small number for pressure loss equation";
    parameter ThermoSysPro.Units.SI.MassFlowRate Qeps=1.e-3
      "Small mass flow rate for continuous flow reversal";

  public
    ThermoSysPro.Units.SI.Power W(start=-1e6)
      "Heat emitted from the condenser";
    ThermoSysPro.Units.SI.Temperature Tec(start=500)
      "Fluid temperature at the inlet of the hot side";
    ThermoSysPro.Units.SI.Temperature Tsc(start=400)
      "Fluid temperature at the outlet of the hot side";
    ThermoSysPro.Units.SI.PressureDifference DPfc(start=1e3)
      "Friction pressure loss in the hot side";
    ThermoSysPro.Units.SI.PressureDifference DPgc(start=1e2)
      "Gravity pressure loss in the hot side";
    ThermoSysPro.Units.SI.PressureDifference DPc(start=1e3)
      "Total pressure loss in the hot side";
    ThermoSysPro.Units.SI.Density rhoc(start=998)
      "Density of the fluid in the hot side";
    ThermoSysPro.Units.SI.MassFlowRate Qc(start=100) "Hot fluid mass flow rate";

  public
    ThermoSysPro.WaterSteam.Connectors.FluidInlet Ec annotation (Placement(
          transformation(extent={{-70,-110},{-50,-90}}, rotation=0)));
    ThermoSysPro.WaterSteam.Connectors.FluidOutlet Sc annotation (Placement(
          transformation(extent={{50,-110},{70,-90}}, rotation=0)));
    ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph proce
      annotation (Placement(transformation(extent={{-100,-100},{-80,-80}},
            rotation=0)));
    ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph procs
      annotation (Placement(transformation(extent={{80,-100},{100,-80}}, rotation=
             0)));
    ThermoSysPro.Properties.WaterSteam.Common.PropThermoSat lsat
      annotation (Placement(transformation(extent={{80,80},{100,100}}, rotation=0)));
    ThermoSysPro.Properties.WaterSteam.Common.PropThermoSat vsat
                                             annotation (Placement(transformation(
            extent={{40,80},{60,100}}, rotation=0)));
    ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph promc
      annotation (Placement(transformation(extent={{0,-100},{20,-80}}, rotation=0)));
    ThermoSysPro.Thermal.Connectors.ThermalPort Cth
      annotation (Placement(transformation(extent={{-10,60},{10,80}})));
  equation

    /* Flow reversal in the hot side */
    if continuous_flow_reversal then
      0 = noEvent(if (Qc > Qeps) then Ec.h - Ec.h_vol else if (Qc < -Qeps) then
          Sc.h - Sc.h_vol else Ec.h - 0.5*((Ec.h_vol - Sc.h_vol)*Modelica.Math.sin(pi
          *Qc/2/Qeps) + Ec.h_vol + Sc.h_vol));
    else
      0 = if (Qc > 0) then Ec.h - Ec.h_vol else Sc.h - Sc.h_vol;
    end if;

    /* Flows in both sides */
    Ec.Q = Sc.Q;
    Qc = Ec.Q;

    /* The fluid specific enthalpy at the outlet of the hot side is assumed to be at the saturation point */
    Sc.h = lsat.h;

    /* Heat emitted from the condenser */
    0 = Qc*(Ec.h - Sc.h) + W;
    Cth.W = W;

    /* Pressure losses in the hot side */
    Ec.P - Sc.P = DPc;

    DPfc = Kc*ThermoSysPro.Functions.ThermoSquare(Qc, eps)/rhoc;
    DPgc = rhoc*g*(z2c - z1c);
    DPc  = DPfc + DPgc;

    /* Fluid thermodynamic properties in the hot side */
    proce = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph(Ec.P, Ec.h, modec);
    procs = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph(Sc.P, Sc.h, modecs);
    promc = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph((Ec.P + Sc.P)/2, (Ec.h + Sc.h)/2, modec);

    Tec = proce.T;
    Tsc = procs.T;
    Cth.T = lsat.T;
    rhoc = promc.d;

    (lsat,vsat) = ThermoSysPro.Properties.WaterSteam.IF97.Water_sat_P(Ec.P);

    annotation (
      Icon(coordinateSystem(
          preserveAspectRatio=false,
          extent={{-100,-100},{100,100}},
          grid={2,2}), graphics={Rectangle(
            extent={{-100,60},{100,-60}},
            lineColor={0,0,255},
            fillColor={255,255,0},
            fillPattern=FillPattern.Solid), Line(
            points={{-60,-90},{-60,38},{0,-8},{60,40},{60,-90}},
            color={0,0,255},
            thickness=0.5)}),
      Diagram(coordinateSystem(
          preserveAspectRatio=false,
          extent={{-100,-100},{100,100}},
          grid={2,2}), graphics={
          Rectangle(
            extent={{-100,60},{100,-60}},
            lineColor={0,0,255},
            fillColor={255,255,0},
            fillPattern=FillPattern.Solid),
          Line(
            points={{-60,-90},{-60,38},{0,-8},{60,40},{60,-90}},
            color={0,0,255},
            thickness=0.5),
          Text(
            extent={{-46,-93},{-26,-103}},
            lineColor={0,0,255},
            fillColor={255,213,170},
            fillPattern=FillPattern.Solid,
            textString=
                 "Hot inlet"),
          Text(
            extent={{28,-93},{48,-103}},
            lineColor={0,0,255},
            fillColor={255,213,170},
            fillPattern=FillPattern.Solid,
            textString=
                 "Hot outlet")}),
      Documentation(info="<html>
    <p><b>Simple Static Condenser with Thermal Port CEA </b> </p>
     <p><b>Pour ThermoSysPro 4.0 </b></p>
     <p><b>Créé et modifié par </h4>
     <ul>
     <li> Huynh-Duc Nguyen </li>
     </ul>
     </html>"));
  end StaticCondenserThermalPort;

  annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"));
end Added_Component;
