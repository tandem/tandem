# Balance of Plant (static), Thermosyspro version - 03 Mai 2024 - TANDEM EU Project

## Component Description

The BoP models a Rankine steam cycle with cogeneration possibilities:
- The design of the cycle follows the coordinates (P, H) of a preliminary CYCLOP calculation (CEA-IRESNE tool), optimised for electricity production;
- Cogeneration capabilities are provided by controllable steam extraction lines at high pressure - HP (45bar, 300C), intermediate pressure - IP (7.5bar, saturated) and low pressure - LP (0.8bar, again saturated). 

### Circuit Description

The Rankine cycle mainly features:
1. An expansion line with 3 turbine groups that produce mechanical work and then electrical power through a generator;
2. HP, IP and LP steam line tappings that are connected to (ideal) heat exchangers. 
3. A dryer and a super-heater;
4. A cooler that is the heat sink;
5. Two pumps (LP and HP);
6. Two reheaters;
7. A heat source that is mimicked through fluid flow boundary conditions : this component is either in the TANDEM/N3S scope, either part of the system code - CATHARE or ATHLET - modeling;
8. A liquid extraction line at the outlet of the BP pump is installed to add heat to the cycle and inject the steam produced upstream of the IP turbine.

The heat exchangers that condense the steam from the extraction lines are modelled in separate loops, plugged to the BOP model: the intention is to allow these loops to be replaced by more sophisticated models, possibly with a different secondary fluid (such as thermal oil). Hence, modularity was key to the BOP development.

## Modeling and Assumption

The modelling assumptions for each component, which are static, can only be described less well than in the Thermosyspro Book, to which careful reference is therefore recommended (El-Hefni & Bouskela, Springer 2019 Edition, ISBN 978-3-030-05104-4). Thermosyspro 4.0 is used, with modelica 4.0 (which is the standard for the TANDEM project).

In short:
- Turbine groups are modeled according to : 
    - Stoloda ellipse law that connects pressure ratio to inlet flow mach number;
    - An efficiency law which correlates steam flow wetness to entropy increase (also known as Baumann rule);
- Reheaters are NTU heat exchanger models, featuring 3 geometrical sections, only two being actually engaged in our case: 2 phase-flow and liquid thermal exchange areas;
- Pumps follow classical quadratic flow maps,with speed as a parameter;
- Condensers that tranfer heat from the steam line tappings, are ideal: they fully condensate any steam flow.

Finally, let's mention that water steam is the only fluid used in the circuit according to IAPWS-IF97 standard. 

## Main Parameters

The main parameters of the cycle design are shown in the table hereafter:

| Cycle parameters | Values |
|-- | -- | 
| Heat source power | 540 MW |
| Boiler inlet ; oulet temperature | 163C ; 300C | 
| Boiler inlet ; outlet pressure | 45.4 ; 45 bar | 
| Condenser pressure | 70 mbar |
| IP tapping line | 7.56 bar |
| LP tapping line | 0.815 bar |


These parameters are used to calibrate each parameter of the different components, as reported in the table hereafter for the main ones:

| Main Components design data | Values |
|-- | -- | 
| Stodola constant - HP | 688136 |
| Stodola constant - IP | 24244 |
| Stodola constant - LP | 659 |
| Reheater - HP: S1 ; S2 | 1587 m2 ; 32 m2 |
| Reheater - LP: S1 ; S2 | 1172 m2 ; 186 m2 |
| Pump - HP: a1 ; a2 ; a3 (normalized coefficients) | -2551 ; 0 ; 594 |
| Pump - HP: b1 ; b2 ; b3 (normalized coefficients) | -16.5 ; 7.65 ; -7.5E-3 |
| Pump - LP: a1 ; a2 ; a3 (normalized coefficients) | -477.7 ; 0 ; 72.8 |
| Pump - LP: b1 ; b2 ; b3 (normalized coefficients) | -25.2 ; 9.47 ; -7.5E-3 |
| Valve, CvMax : HP turbine inlet | 90000 m4/(sN^5) |
| Valve, CvMax : IP turbine inlet | 700000 m4/(sN^5) |
| Valve, CvMax : LP turbine inlet | 2.8E7 m4/(sN^5) |
| Valve, CvMax : Steam generator inlet | 1200 m4/(sN^5) |

## Control Strategy

Various control strategies are proposed, as this is in fact a subject for research and development:

1. Control of the steam generator and turbine:
    - The water flow at the inlet of the steam generator can be modified by the speed of the HP pump or by the opening of an inlet valve.
    - This inlet flow rate can be modified to control either the pressure ("P" approach) or the temperature ("T" approach) at the steam generator outlet:
        - In the first case, the turbine inlet pressure (admission) is defined as an auxiliary control parameter and is controlled by the opening of the inlet valve. 
        - In the second case, the steam generator pressure is defined as an auxiliary control parameter and is controlled by the opening of the turbine inlet valve.
Demo cases have been carried out for each approach (see below).

2. Steam flow from the extraction lines can be pressure controlled (e.g. to guaranty a given (saturated) temperature to the process cogeneration customer) or left free (pressure sliding mode).

It should be emphasized that the PI controller parameters must be adapted to each coupling and are therefore the responsibility of the user depending on his application and the associated couplings of modules.

## Provided Examples

In the **Demo_BOP_Plugged** subpackage some usage examples can be found:

1. **StaticBoP3_T_Plugged_BC**. 
This demonstration case connects the BoP3 model to a 0D heat source (boundary conditions). Control is performed by using the "T" approach, together with HP pump speed control.
Along a 1000s (quasi-static) transient, the BoP switches from a pure power generation mode (100s) to a cogeneration mode. Cogeneration is achieved by simultaneous extraction of steam flows at HP, IP and LP, while pressure of the tappings are controlled and maintained to their nominal values.
Cycle electrical efficiency falls from about 32.9% to 25.4% while cogeneration thermal powers reach 36.5 MWth, 54.2 MWth and 56.4 MWth, respectively at HP, IP and LP (i.e. a total of ~27% of boiler output). Conjointly, temperature at steam generator inlet rises from 163 to 172.3C.

2. **StaticBoP3_P_Plugged_BC**. 
This demonstration case connects again the BoP3 model to a 0D heat source as boundary conditions, but by using the "P" control approach.

3. **StaticBoP3_T_Plugged_N3S_TMPower**.
This demonstration case connects the BoP3 model to Polimi's N3S TANDEM mode, which can be seen as a challenge since both have been recently developed. In operational terms, this case shows importantly that library connections via the fluid connectors developed as part of the TANDEM project, are relevant. The "T" control approach is used, together with speed control of the HP pump. Over a transient of 1000s, the BoP switches from a pure electricity production mode (100s) to a cogeneration mode: cogeneration is achieved by simultaneous extraction of steam flows at HP, IP and LP.

4. **StaticBoP3_P_Plugged_N3S_TMPower**.
This demonstration case connects again the BoP3 model to Polimi's N3S TANDEM mode, but by using the "P" control approach. 

## Acknowledgements
Guido, Giorgio & Sarah!