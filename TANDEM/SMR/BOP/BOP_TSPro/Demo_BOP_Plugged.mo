within TANDEM.SMR.BOP.BOP_TSPro;
package Demo_BOP_Plugged "BOP demo, plugged to Boundary Conditions"

  model StaticBoP3_T_Plugged_BC
    "Demo of Static BoP, with Steam Geneator temperature control by inlet water flow -
   Heat Source is a 0D Boundary conditions"
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164192.02652188018)),
        Qv(start=0.18350725962648437),
        h(start=163778.77086256517),
        Pm(start=363758.3775616736, displayUnit="bar")),
      Sing7(Pm(start=81483.94514799965, displayUnit="bar"), C2(h_vol(start=317879.61051903106))),
      P11(C1(h_vol(start=371761.2439210699))),
      Sing14(Pm(start=694722.1099619151, displayUnit="bar")),
      Vol14(h(start=488060.0714811865)),
      Sing4b(Pm(start=755864.3745072351, displayUnit="bar"), C2(h_vol(start=700805.4426216882))),
      Pp4(C2(h_vol(start=2639595.2860046183), P(start=755864.3745787522, displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164192.02652188033)),
      Vv10(Pm(start=717608.3792336329, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2147941.546776068), Cs(h(start=2147941.546819757))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=2622123.070239542))),
      Pip7(C2(P(start=81483.94541890657, displayUnit="bar"), h_vol(start=2622123.070239542)), Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2622123.070239542)),
      Vol6(h(start=2977596.02891552)),
      Vol5(h(start=2765182.383708409)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=2639595.2860046183))),
      P4(C1(h_vol(start=2639595.2860046183))),
      Vol4(Cs2(Q(start=24.510005100358)), h(start=2639595.2860046183)),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468510.058956289, displayUnit="bar"), h_vol(start=2943659.8831241573))),
      Sing2(C1(h_vol(start=2943659.8831241573))),
      Bach(
        h(start=488059.1422815186),
        Ce4(h(start=371761.24392111483)),
        Cs1(Q(start=244.2983525092077)),
        Ce3(Q(start=21.204195564406678))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765182.383708424))),
      SupH(
        DPfc(start=39745.819526841864, displayUnit="bar"),
        Ec(h(start=2943659.8831262477)),
        Ef(h(start=2765182.383708409)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977596.0289155184)),
        DPf(start=81528.70844178236, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2943659.8831262477),
        Ce(h(start=2943659.883057354))),
      Vol1(h(start=691076.9808245787)),
      ReH_BP(
        HDesF(start=371761.2439210699),
        HeiF(start=171024.29179976988),
        promeF(d(start=981.4362402154908, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164192.02652188033)),
        Hep(start=393725.16087966954)),
      ReH_HP(
        HDesF(start=691075.1966525211),
        HeiF(start=493867.77023896377),
        Se(
          h_vol(start=679621.7421491045),
          h(start=691075.1966525211),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=928.4157529118911, displayUnit="g/cm3")),
        Hep(start=710778.2452102785)),
      Pp_HP(
        C2(h(start=492848.12410525547), h_vol(start=492848.12410525547)),
        Pm(start=2649408.6528954087, displayUnit="bar"),
        Qv(start=0.25309903230028524),
        h(start=490454.097793221)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4582018.156979002, displayUnit="bar")),
      Vv2(
        Q(start=218.51961076754105),
        C1(h_vol(start=2943659.8831262477), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2943659.8831262477), P(start=4400001.189086886, displayUnit="bar"))),
      Q1(C1(h_vol(start=691076.9808245787))),
      T1(C1(h_vol(start=691076.9808245787))),
      P1(C1(h_vol(start=691076.9808245787))),
      T2(C1(h_vol(start=2943659.8831241573)), C2(h_vol(start=2943659.8831241573))),
      Turb_BP1(
        Cs(h(start=2622123.0702394107), h_vol(start=2619840.125764651)),
        xm(start=0.990348445697731),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=660260.058240257, displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7673417152894, displayUnit="g/cm3")), Cv(Q(start=182.1681024769765))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.05459371059084007, displayUnit="g/cm3")),
        xm(start=0.939970853915546),
        Ce(h_vol(start=2622123.070239542)),
        Pe(start=81036.18941466877, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691076.9808245787)),
      SG_Secondary_Out(h_vol(start=2943659.8831241573), Q(start=239.72480633161524)),
      Turb_HP(Cs(h(start=2639595.286004529)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4451161.532520275, displayUnit="bar"), h(start=2943659.8353077616)),
      HeatNetwk_IP_Out(h_vol(start=2639595.2860046183)),
      HeatNetwk_LP_In(P(start=7055.9126210368, displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2622123.0648966096)),
      HeatNetwk_HP_In(P(start=4624073.085866618, displayUnit="bar"))) annotation (Placement(transformation(extent={{-40,-50},{100,20}})));

     // Extraction_ReheatBP(Ce(h(start=2619840.125764651))),
     // Extraction_TurbHP_Oulet(Cex(h(start=2492812.8893823405)), h(start=2492812.8893823395)),
     // CsBP1b(start=1692.3189652681808),
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={40.5,95.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={42,108})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={59,97})));
    TANDEM.SMR.BOP.BOP_TSPro.Added_Component.OD_HeatSource OD_HeatSource(
      Ce(h(start=690705.965184411), h_vol(start=690705.9651844109)),
      Q(start=240.02949730772582, fixed=false),
      Te(start=318.64),
      Ts(start=583.15, fixed=false),
      deltaP(fixed=true) = 40000,
      eta(fixed=true) = 100,
      mode=0,
      Cs(
        h_vol(start=2944121.464781758),
        h(start=2944106.3643035735),
        P(start=4502408.835922457))) annotation (Placement(visible=true,
          transformation(
          origin={-86,-20},
          extent={{-8,-8},{8,8}},
          rotation=90)));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampLF(
      Starttime=20,
      Duration=1000,
      Initialvalue=540E6,
      Finalvalue=1*540E6)
      annotation (Placement(visible=true, transformation(
          origin={-110,-16},
          extent={{-4,-4},{4,4}},
          rotation=0)));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={157.5,-6})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-8})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={159,-25})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-36,108})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-39.5,95.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-15,97})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-33,83})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-19,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={39,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={55,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={143,-25})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={141,-9})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={158,-40})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-40})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,48})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={178,46})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,30})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,32})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.4795372014455217E-08, displayUnit="bar"),
        DPf(start=0.05963019933392444, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118)),
        Ec(h(start=2943659.882919201))),
      Vv_Tap(C2(h_vol(start=1118785.4784898118))),
      TCond_Tap(C2(h_vol(start=1118785.4784898118)))) annotation (Placement(transformation(extent={{-36,48},{-22,62}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195832316411424E-07, displayUnit="bar"),
        DPf(start=0.05966626304582995, displayUnit="bar"),
        Ec(h(start=2639595.28600482)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vv_Tap(C2(h_vol(start=710810.4235062235))))
      annotation (Placement(transformation(extent={{26,48},{40,62}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.0063492516831889E-06, displayUnit="bar"),
        DPf(start=0.05977368960615324, displayUnit="bar"),
        Ec(h(start=2622123.070239539)),
        Sc(h_vol(start=393747.5804603163))),
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={121,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_IP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,54})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_LP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,28})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={39,-65})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{-2,26},{12,38}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=300 + 273)
               annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-72,-42})));
    BOP_2Plug.CTRL_PI CTR_PT_PpHP annotation (Placement(transformation(
          extent={{6,-5},{-6,5}},
          rotation=90,
          origin={-57,-42})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-19,-67})));
    BOP_2Plug.HX_HeatInput HX_HeatInput
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={71,-65})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={81,-89})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44.5E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={4,52})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={61,-89})));
  equation
    connect(HeatNwk_IP_In.y, Heat_IP_Network_Out.m_flow_in)
      annotation (Line(points={{42,103.6},{44.1,103.6},{44.1,100}},
          color={0,0,127}));
    connect(OD_HeatSource.Signal_Elec, rampLF.y)
      annotation (Line(
        points={{-93.04,-20},{-100,-20},{-100,-16},{-105.6,-16}},
        color={0,0,255}));
    connect(OD_HeatSource.Cs, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
      annotation (Line(points={{-86.64,-13.04},{-86.64,-10},{-50,-10},{-50,-14.2222},{-40.2593,-14.2222}},
          color={0,0,255}));
    connect(OD_HeatSource.Ce, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-86.64,-26.96},{-86.64,-28},{-50,-28},{-50,-21.7407},{-40.2593,-21.7407}},
          color={0,0,255}));
    connect(rampLP.y, HeatNwk_LP_In.m_flow_in) annotation (Line(points
          ={{171.6,-8},{172,-9.2},{162,-9.2}}, color={0,0,127}));
    connect(HeatNwk_HP_In.y, Heat_HP_Network_Out.m_flow_in)
      annotation (Line(points={{-36,103.6},{-35.9,104},{-35.9,100}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1], fluid2TSPro4.port_a)
      annotation (Line(
        points={{-39.5,91},{-39.5,89.86},{-33,89.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1], fluid2TSPro5.port_b) annotation (
        Line(points={{-15,94},{-14,94},{-14,90},{-19,90},{-19,88}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1], fluid2TSPro1.port_a)
      annotation (Line(
        points={{40.5,91},{39,91},{39,87.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1], fluid2TSPro2.port_b) annotation (
        Line(points={{59,94},{60,94},{60,88},{55,88}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro6.port_a) annotation (
        Line(points={{153,-6},{147.86,-6},{147.86,-9}}, color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1], fluid2TSPro3.port_b) annotation (
        Line(points={{156,-25},{150,-25}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u, Set_Flow_TapSteam_LP.y)
      annotation (Line(points={{162.8,-40},{171.6,-40}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y, adaptorRealModelicaTSP.u)
      annotation (Line(points={{10,103.6},{10,92.8}}, color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y, adaptorRealModelicaTSP1.u)
      annotation (Line(points={{86,103.6},{86,92.8}}, color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u, Set_P_TapSteam_IP.y)
      annotation (Line(points={{150.8,48},{170,48},{170,46},{173.6,46}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u, Set_P_TapSteam_LP.y)
      annotation (Line(points={{150.8,30},{168,30},{168,32},{171.6,32}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-33.0018,76.035},{-33.2,76.035},{-33.2,61.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-33.2,48.2},{-30,48.2},{-30,30},{-27.8148,30},{-27.8148,20}}, color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-30,48.2},{-28,48.2},{-28,32},{-23.4074,32},{-23.4074,20}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-30.2,61.8},{-32,61.8},{-32,70},{-19,70},{-19,74}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-26.2,61.6},{-26.2,68},{10,68},{10,83.6}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Set, adaptorRealModelicaTSP3.outputReal)
      annotation (Line(points={{127.6,55},{138,55},{138,48},{141.6,48}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{127.6,29},{136,29},{136,30},{141.6,30}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{108.4,58.75},{53.8519,58.75},{53.8519,20}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{107.8,47.75},{60.0741,47.75},{60.0741,19.4815}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{108.4,32.75},{73.2963,32.75},{73.2963,19.7407}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{107.8,21.75},{104,21.75},{104,24},{78.7407,24},{78.7407,19.2222}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{38.5556,-49.7407},{38.5556,-52},{38,-52},{38,-58},{39,-58},{39,-59.5}},
                                                                                      color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{9.375,25.88},{9.25926,25.88},{9.25926,20}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outb)
      annotation (Line(points={{1.675,26.24},{2,26.24},{2,20},{4.33333,20}}, color={0,0,255}));
    connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-51.9,-45.75},{-44,-45.75},{-44,-43},{-39.4815,-43}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.T_SG_Out, CTR_PT_PpHP.Sensor)
      annotation (Line(points={{-40.2593,-38.5926},{-54,-38.5926},{-54,-39.15},{-52.2,-39.15}}, color={0,0,255}));
    connect(Set_Pout_SG.y, CTR_PT_PpHP.Set) annotation (Line(points={{-67.6,-42},{-64,-42},{-64,-41.4},{-61.8,-41.4}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-19.5185,-49.7407},{-19,-49.7407},{-19,-61.5}},color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out, HX_cog_LP.fluidInletI)
      annotation (Line(points={{100,-22.5185},{100,-22.8},{114.2,-22.8}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, HX_cog_LP.fluidOutletI1)
      annotation (Line(points={{100,-26.4074},{100,-26},{114.2,-26}}, color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro6.steam_outlet)
      annotation (Line(points={{127.8,-22.8},{130,-22.8},{130,-9.00175},{134.035,-9.00175}}, color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI, fluid2TSPro3.steam_inlet) annotation (Line(points={{127.8,-25.8},{128,-25},{136,-25}}, color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP, adaptorRealModelicaTSP2.outputReal)
      annotation (Line(points={{127.6,-29.8},{132,-29.8},{132,-40},{153.6,-40}}, color={0,0,255}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{39.0018,74.035},{39.0018,66},{28.8,66},{28.8,61.8}}, color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet, HX_cog_IP.Water_Cooling_Out) annotation (Line(points={{55,74},{55,64},{31.8,64},{31.8,61.8}}, color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{35.8,61.6},{86,61.6},{86,83.6}}, color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{28.8,48.2},{28.8,24},{20.4074,24},{20.4074,20.2593}}, color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{32,48.2},{32,20.2593},{24.5556,20.2593}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{19.6296,-50},{20,-50},{20,-56},{66.8,-56},{66.8,-58.2}}, color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{70,-58.2},{72,-58.2},{72,-56},{106,-56},{106,9.37037},{100,9.37037}}, color={255,0,0}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{73.8,-71.6},{73.8,-78},{81,-78},{81,-83.5}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set) annotation (Line(points={{4,47.6},{4.3,48},{4.3,37.76}}, color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{68.4,-71.6},{68.4,-80},{61,-80},{61,-83.5}}, color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{200,120}})),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{200,120}})));
  end StaticBoP3_T_Plugged_BC;

  model StaticBoP3_P_Plugged_BC "Demo of Static BoP, with Steam Geneator pressure control by inlet water flow - 
     Heat Source is a 0D Boundary conditions"
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164192.02652393395)),
        Qv(start=0.18350725673411197),
        h(start=163778.77086358969),
        Pm(start=363758.37747187633, displayUnit="bar")),
      Sing7(Pm(start=81483.94514840622, displayUnit="bar"), C2(h_vol(start=317879.60972432443))),
      P11(C1(h_vol(start=371761.2447393281))),
      Sing14(Pm(start=693968.8516748664, displayUnit="bar")),
      Vol14(h(start=488060.21488197596)),
      Sing4b(Pm(start=755864.374872922, displayUnit="bar"), C2(h_vol(start=700805.4345049231))),
      Pp4(C2(h_vol(start=2639596.204442198), P(start=755864.3749444388, displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164192.02652393395)),
      Vv10(Pm(start=717608.3791438369, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2147941.5493544163), Cs(h(start=2147941.5493544154))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=2622123.072567575))),
      Pip7(C2(P(start=81483.94541931312, displayUnit="bar"), h_vol(start=2622123.072567575)), Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2622123.072567575)),
      Vol6(h(start=2977596.047246133)),
      Vol5(h(start=2765182.383058621)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=2639596.204442198))),
      P4(C1(h_vol(start=2639596.204442198))),
      Vol4(
        Cs2(Q(start=24.509966251870072)),
        h(start=2639596.204442198),
        Cs3(Q(start=194.00851031135295))),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468547.086316049, displayUnit="bar"), h_vol(start=2943661.0649808757))),
      Sing2(C1(h_vol(start=2943661.0649808757))),
      Bach(
        h(start=488059.2856824448),
        Ce4(h(start=371761.24473932857)),
        Cs1(Q(start=244.2983525092077))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765182.38305862))),
      SupH(
        DPfc(start=39745.57096850667, displayUnit="bar"),
        Ec(h(start=2943661.064980875)),
        Ef(h(start=2765182.383058621)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977596.047246133)),
        DPf(start=81528.70712747396, displayUnit="bar"),
        Sc(h_vol(start=1118787.9839318935))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2943661.064980875),
        Ce(h(start=2943661.0649808734))),
      Vol1(h(start=691077.0439232907)),
      ReH_BP(
        HDesF(start=371761.2447393281),
        HeiF(start=171024.2918910695),
        promeF(d(start=981.436240163131, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164192.02652393395)),
        Hep(start=393725.16088023677)),
      ReH_HP(
        HDesF(start=691075.2597402378),
        HeiF(start=493867.95406250644),
        Se(
          h_vol(start=679621.7421491045),
          h(start=691075.2597402378),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=928.4157518512499, displayUnit="g/cm3")),
        Hep(start=710778.2676195425)),
      Pp_HP(
        C2(h(start=492848.3081997887), h_vol(start=492848.3081997887)),
        Pm(start=2649427.0152045134, displayUnit="bar"),
        Qv(start=0.25309891169526044),
        h(start=490454.2615408823)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4582054.885963371, displayUnit="bar")),
      Vv2(
        Q(start=218.51947656322304),
        C1(h_vol(start=2943661.064980875), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2943661.064980875), P(start=4396325.747708942, displayUnit="bar"))),
      Q1(C1(h_vol(start=691077.0439232907))),
      T1(C1(h_vol(start=691077.0439232907))),
      P1(C1(h_vol(start=691077.0439232907))),
      T2(C1(h_vol(start=2943661.0649808757)), C2(h_vol(start=2943661.0649808757))),
      Turb_BP1(
        Cs(h(start=2622123.072567575), h_vol(start=2619840.125764651)),
        xm(start=0.9903484462100379),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=660260.0479919518, displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7673417791849, displayUnit="g/cm3")), Cv(Q(start=182.16809961917588))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.05459371049196894, displayUnit="g/cm3")),
        xm(start=0.9399708552965609),
        Ce(h_vol(start=2622123.072567575)),
        Pe(start=81036.18813447974, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691077.0439232907)),
      SG_Secondary_Out(h_vol(start=2943661.0649808757), Q(start=244.3003525092077)),
      Turb_HP(Cs(h(start=2639596.2044421975)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4450037.179400733, displayUnit="bar"), h(start=2943661.065035004)),
      HeatNetwk_IP_Out(h_vol(start=2644472.7344897604)),
      HeatNetwk_LP_In(P(start=7055.912619353334, displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2622123.0725958906))) annotation (Placement(transformation(extent={{-40,-50},{100,20}})));

     // Extraction_ReheatBP(Ce(h(start=2619840.125764651))),
     // Extraction_TurbHP_Oulet(Cex(h(start=2492812.8893823405)), h(start=2492812.8893823395)),
     // CsBP1b(start=1692.3189652681808),
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={40.5,95.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={42,108})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={59,97})));
    TANDEM.SMR.BOP.BOP_TSPro.Added_Component.OD_HeatSource
                                  OD_HeatSource(
      Ce(h(start=690705.965184411), h_vol(start=690705.9651844109)),
      Q(start=240.02949730772582, fixed=false),
      Te(start=318.64),
      Ts(start=583.15, fixed=false),
      deltaP(fixed=true) = 40000,
      eta(fixed=true) = 100,
      mode=0,
      Cs(
        h_vol(start=2944121.464781758),
        h(start=2944106.3643035735),
        P(start=4502408.835922457))) annotation (Placement(visible=true,
          transformation(
          origin={-86,-18},
          extent={{-8,-8},{8,8}},
          rotation=90)));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampLF(
      Starttime=20,
      Duration=1000,
      Initialvalue=540E6,
      Finalvalue=1*540E6)
      annotation (Placement(visible=true, transformation(
          origin={-106,-18},
          extent={{-4,-4},{4,4}},
          rotation=0)));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={157.5,-6})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-8})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={159,-25})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-36,108})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-39.5,95.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-15,97})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro
                                               fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-33,83})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-19,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={39,81})));
   TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={55,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={143,-25})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={141,-9})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={158,-40})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-40})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-6,50})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,48})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={178,46})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,30})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,32})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.47951833195559E-08, displayUnit="bar"),
        DPf(start=0.05963019887854127, displayUnit="bar"),
        Sc(h_vol(start=1118787.9839318935)),
        Ec(h(start=2943661.064980875))),
      Vv_Tap(C2(h_vol(start=1118787.9839318935))),
      TCond_Tap(C2(h_vol(start=1118787.9839318935)))) annotation (Placement(transformation(extent={{-36,48},{-22,62}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195832880541478E-07, displayUnit="bar"),
        DPf(start=0.059666263367334126, displayUnit="bar"),
        Ec(h(start=2639596.204442199)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vol_Tap(h(start=2639596.204442199)),
      Vv_Tap(C2(h_vol(start=710810.4235062235))))
      annotation (Placement(transformation(extent={{26,48},{40,62}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Hx_Hybrid_LP(
        Sc(h_vol(start=393747.5804603163)),
        DPc(start=1.0063492526775674E-06, displayUnit="bar"),
        DPf(start=0.05977368960696228, displayUnit="bar"),
        Ec(h(start=2622123.072567575)))) annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={121,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,54})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap1
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,28})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=90,
          origin={38,-72})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{0,28},{14,40}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=45e5)
               annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-78,-46})));
    BOP_2Plug.CTRL_PI CTR_PT_PpHP(PropInt_P4(k=-5e-5, Ti=5)) annotation (
        Placement(transformation(
          extent={{6,-5},{-6,5}},
          rotation=90,
          origin={-61,-44})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-21,-65})));
    BOP_2Plug.HX_HeatInput HX_HeatInput
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={81,-67})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={73,-89})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={91,-89})));
  equation
    connect(HeatNwk_IP_In.y, Heat_IP_Network_Out.m_flow_in)
      annotation (Line(points={{42,103.6},{44.1,103.6},{44.1,100}},
          color={0,0,127}));
    connect(OD_HeatSource.Signal_Elec, rampLF.y)
      annotation (Line(
        points={{-93.04,-18},{-101.6,-18}},
        color={0,0,255}));
    connect(OD_HeatSource.Cs, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
      annotation (Line(points={{-86.64,-11.04},{-86.64,-10},{-44,-10},{-44,
            -14.2222},{-40.2593,-14.2222}},
          color={0,0,255}));
    connect(OD_HeatSource.Ce, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-86.64,-24.96},{-86.64,-28},{-46,-28},{-46,
            -21.7407},{-40.2593,-21.7407}},
          color={0,0,255}));
    connect(rampLP.y, HeatNwk_LP_In.m_flow_in) annotation (Line(points
          ={{171.6,-8},{172,-9.2},{162,-9.2}}, color={0,0,127}));
    connect(HeatNwk_HP_In.y, Heat_HP_Network_Out.m_flow_in)
      annotation (Line(points={{-36,103.6},{-35.9,104},{-35.9,100}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1], fluid2TSPro4.port_a)
      annotation (Line(
        points={{-39.5,91},{-39.5,89.86},{-33,89.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1], fluid2TSPro5.port_b) annotation (
        Line(points={{-15,94},{-14,94},{-14,90},{-19,90},{-19,88}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1], fluid2TSPro1.port_a)
      annotation (Line(
        points={{40.5,91},{39,91},{39,87.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1], fluid2TSPro2.port_b) annotation (
        Line(points={{59,94},{60,94},{60,88},{55,88}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro6.port_a) annotation (
        Line(points={{153,-6},{147.86,-6},{147.86,-9}}, color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1], fluid2TSPro3.port_b) annotation (
        Line(points={{156,-25},{150,-25}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u, Set_Flow_TapSteam_LP.y)
      annotation (Line(points={{162.8,-40},{171.6,-40}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y, adaptorRealModelicaTSP.u)
      annotation (Line(points={{10,103.6},{10,92.8}}, color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y, adaptorRealModelicaTSP1.u)
      annotation (Line(points={{86,103.6},{86,92.8}}, color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u, Set_P_TapSteam_IP.y)
      annotation (Line(points={{150.8,48},{170,48},{170,46},{173.6,46}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u, Set_P_TapSteam_LP.y)
      annotation (Line(points={{150.8,30},{168,30},{168,32},{171.6,32}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-33.0018,76.035},{-33.2,76.035},{-33.2,61.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-33.2,48.2},{-30,48.2},{-30,30},{-27.8148,30},{
            -27.8148,20}},                                                                    color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-30,48.2},{-28,48.2},{-28,32},{-23.4074,32},{-23.4074,20}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-30.2,61.8},{-32,61.8},{-32,70},{-19,70},{-19,74}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-26.2,61.6},{-26.2,68},{10,68},{10,83.6}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out,
      HX_cog_LP.fluidInletI) annotation (Line(points={{100,-22.5185},{100,-22.8},
            {114.2,-22.8}},            color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, HX_cog_LP.fluidOutletI1)
      annotation (Line(points={{100,-26.4074},{100,-26},{114.2,-26}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro6.steam_outlet)
      annotation (Line(points={{127.8,-22.8},{130,-22.8},{130,-9.00175},
            {134.035,-9.00175}}, color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI, fluid2TSPro3.steam_inlet)
      annotation (Line(points={{127.8,-25.8},{128,-25},{136,-25}},
          color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP, adaptorRealModelicaTSP2.outputReal)
      annotation (Line(points={{127.6,-29.8},{132,-29.8},{132,-40},{
            153.6,-40}}, color={0,0,255}));
    connect(CTR_P_Tap.Set, adaptorRealModelicaTSP3.outputReal) annotation (Line(points={{127.6,55},{138,55},{138,48},{141.6,48}}, color={0,0,255}));
    connect(CTR_P_Tap1.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{127.6,29},{136,29},{136,30},{141.6,30}}, color={0,0,255}));
    connect(CTR_P_Tap.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{108.4,58.75},{53.8519,58.75},{53.8519,20}}, color={0,0,255}));
    connect(CTR_P_Tap.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{107.8,47.75},{60.0741,47.75},{60.0741,19.4815}}, color={0,0,255}));
    connect(CTR_P_Tap1.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{108.4,32.75},{73.2963,32.75},{73.2963,19.7407}}, color={0,0,255}));
    connect(CTR_P_Tap1.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{107.8,21.75},{104,21.75},{104,24},{78.7407,24},
            {78.7407,19.2222}},                                                                    color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{38.5556,-49.7407},{38,-52},{38,-67.6}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{11.375,27.88},{12,27.88},{12,20},{9.25926,20}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set) annotation (Line(points={{-6,45.6},{2,45.6},{2,42},{6.3,42},{6.3,39.76}}, color={0,0,255}));
    connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-55.9,-47.75},{-44,-47.75},{-44,-43},{-39.4815,
            -43}},                                                                    color={0,0,255}));
    connect(Set_Pout_SG.y, CTR_PT_PpHP.Set) annotation (Line(points={{-73.6,-46},{-70,-46},{-70,-43.4},{-65.8,-43.4}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-19.5185,-49.7407},{-21,-49.7407},{-21,-59.5}},color={0,0,255}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{39.0018,74.035},{39.0018,66},{28.8,66},{28.8,
            61.8}},                                                                  color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{28.8,48.2},{28.8,24},{20.4074,24},{20.4074,
            20.2593}},                                                                color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{32,48.2},{34,48.2},{34,20.2593},{24.5556,
            20.2593}},                                                              color={255,0,0}));
    connect(HX_cog_IP.Water_Cooling_Out, fluid2TSPro2.steam_inlet)
      annotation (Line(points={{31.8,61.8},{32,61.8},{32,64},{55,64},{55,74}}, color={255,0,0}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{35.8,61.6},{86,61.6},{86,83.6}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbHP_1stRow)
      annotation (Line(points={{3.675,28.24},{3.675,26},{13.6667,26},{13.6667,
            20}},                                                                   color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outa, CTR_PT_PpHP.Sensor)
      annotation (Line(points={{-40.2593,-34.963},{-44,-34.963},{-44,-40},{
            -56.2,-40},{-56.2,-41.15}},                                                                color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{78.4,-73.6},{78,-73.6},{78,-78},{73,-78},{73,-83.5}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{83.8,-73.6},{83.8,-80},{91,-80},{91,-83.5}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{19.6296,-50},{19.6296,-54},{76.8,-54},{76.8,
            -60.2}},                                                                 color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{80,-60.2},{80,-54},{104,-54},{104,9.37037},{100,9.37037}}, color={255,0,0}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{200,120}})),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{200,120}})));
  end StaticBoP3_P_Plugged_BC;

  model StaticBoP3_T_Plugged_N3S_TMPower "Demo of Static BoP, with Steam Geneator temperature control by inlet water flow - 
  BoP is coupled to the N3S developped by Guido-Polimi-Thermopower"
    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
      Placement(transformation(extent={{-150,86},{-130,106}})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-204,36},{-166,68}})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSbypass_fluid
                         nSSS_fluid(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      core(fuel(
          Tc(start={599.8246962010348,602.4035817089674,604.9430344251562,607.4409623146475,609.8951796044038,612.3034026102237,614.6632388629673,
                616.972164951592,619.2274875928174,621.4262824828023},
                                                      displayUnit=
                "degC"),
          Tci(start={607.357299055964,609.9263179214674,612.4560751618749,614.9444857090893,617.3893710693703,619.7884551655428,622.1393535040091,
                624.4395511010829,626.6863636986716,628.8768768445042},
                                                      displayUnit="degC"),
          Tco(start={592.79187164561,595.379969157214,597.9284740671085,600.4352878357251,602.8982178937007,605.3149734545241,607.6831546026796,
                610.0002300589125,612.2634981135637,614.470025264861},
                                                      displayUnit="degC"),
          Tvol(start=[1094.685079978524,952.2318771025554,895.1812987635391,841.9907740814742,791.6886939443132; 1097.6352591333634,
                954.8495228290908,897.668461406179,844.3581373341756,793.9444852112443; 1100.543417055899,957.4296795135934,900.1199165863549,
                846.6914339486301,796.1677435326709; 1103.4069955902705,959.970087364947,902.5335212396162,848.9886277435271,798.3565319957553;
                1106.2233230913153,962.4683864291272,904.9070373736532,851.247592405007,800.5088280064261; 1108.9896095674285,964.9221122837564,
                907.2381279809629,853.4661075998306,802.6225195892222; 1111.7029341358307,967.3286849126494,909.5243464719133,855.6418489214594,
                804.6953958110744; 1114.3602194685786,969.6853860438587,911.7631151491025,857.7723674077579,806.7251272704422; 1116.9581868294285,
                971.9893192807879,913.9516873387103,859.8550535085234,808.7092317742771; 1119.4932853203732,974.2373473748704,916.087087813568,
                861.887080400114,810.645020344306],
                                   displayUnit="degC"))),
      flowSplit(out2(m_flow(start=-10.675229181046038))))
      annotation (Placement(transformation(extent={{-214,-30},{-156,6}})));
    AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro7(steam_outlet(h(start=2944000.0)),
        port_a(h_outflow(start=2962802.891927479)))
      annotation (Placement(transformation(extent={{-136,-14},{-120,2}})));
    AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro8 annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-126,-22})));
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164201.60747706925)),
        Qv(start=0.17263386337434475),
        h(start=163783.56134015904),
        Pm(start=363423.9294671584, displayUnit="bar"),
        C1(P(start=6999.997040673345, displayUnit="bar"))),
      Sing7(Pm(start=81485.40464429374, displayUnit="bar"), C2(h_vol(start=314987.741178026))),
      P11(C1(h_vol(start=374802.82135656744))),
      Sing14(Pm(start=694730.3132819665, displayUnit="bar")),
      Vol14(h(start=492573.82435723214)),
      Sing4b(Pm(start=755882.1887889785, displayUnit="bar"), C2(h_vol(start=700282.3666931049))),
      Pp4(C2(h_vol(start=2654301.081720553), P(start=755882.1888511011, displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164201.60747706928)),
      Vv10(Pm(start=717273.9309468217, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2155434.92928129), Cs(h(start=2155434.9292812874))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=2621238.7036729585))),
      Pip7(C2(P(start=81485.40489057124, displayUnit="bar"), h_vol(start=2621238.7036729585)), Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2621238.7036729585)),
      Vol6(h(start=2977467.9227152397)),
      Vol5(h(start=2765429.0132694803)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=2654301.081720553))),
      P4(C1(h_vol(start=2654301.081720553))),
      Vol4(Cs2(Q(start=22.757622544851948)), h(start=2654301.081720553)),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468523.728905819, displayUnit="bar"), h_vol(start=2962802.891927478))),
      Sing2(C1(h_vol(start=2962802.8919274784))),
      Bach(
        h(start=492572.85729923117),
        Ce4(h(start=374802.8213566989)),
        Cs1(Q(start=225.67064119881007)),
        Ce3(Q(start=21.07281825188525))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765429.0132694854))),
      SupH(
        DPfc(start=39644.58643045058, displayUnit="bar"),
        Ec(h(start=2962802.891927478)),
        Ef(h(start=2765429.0132694803)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977467.922715237)),
        DPf(start=82029.33356331526, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2962802.891927478),
        Ce(h(start=2962802.891927478))),
      Vol1(h(start=694365.885437522)),
      ReH_BP(
        HDesF(start=374802.82135656744),
        HeiF(start=171391.9030371648),
        promeF(d(start=981.4362402154908, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164201.60747706928)),
        Hep(start=393727.1966266233)),
      ReH_HP(
        HDesF(start=694364.0047422458),
        HeiF(start=498371.8889398137),
        Se(
          h_vol(start=679621.7421491045),
          h(start=694364.0047422458),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=927.601137766441, displayUnit="g/cm3")),
        Hep(start=710782.4668576005)),
      Pp_HP(
        C2(h(start=497313.0171850046), h_vol(start=497313.0171850046)),
        Pm(start=2645443.7122672196, displayUnit="bar"),
        Qv(start=0.2384748323949854),
        h(start=494943.42077111837)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4574296.430358849, displayUnit="bar")),
      Vv2(
        Q(start=216.48672695692787),
        C1(h_vol(start=2962802.891927478), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2962802.891927478), P(start=4390466.403719328, displayUnit="bar"))),
      Q1(C1(h_vol(start=694365.8854375221))),
      T1(C1(h_vol(start=694365.885437522))),
      P1(C1(h_vol(start=694365.8854375221))),
      T2(C1(h_vol(start=2962802.8919274774)), C2(h_vol(start=2962802.8919274793))),
      Turb_BP1(
        Cs(h(start=2621238.7036729474), h_vol(start=2619840.125764651)),
        xm(start=0.9901538430496221),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=664164.0933327859, displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7239942023749, displayUnit="g/cm3")), Cv(Q(start=182.1681024769765))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.054646827734247665, displayUnit="g/cm3")),
        xm(start=0.9392823471575731),
        Ce(h_vol(start=2621238.7036729585)),
        Pe(start=81943.99622985195, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691076.9808245787)),
      SG_Secondary_Out(h_vol(start=2943659.8831241573), Q(start=239.72480633161524)),
      Turb_HP(Cs(h(start=2654301.0817205533)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4451161.532520275, displayUnit="bar"), h(start=2962802.8918941384)),
      HeatNetwk_IP_Out(h_vol(start=2639595.2860046183)),
      HeatNetwk_LP_In(P(start=7056.796286544793, displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2621238.702647789)),
      HeatNetwk_HP_In(P(start=4624073.085866618, displayUnit="bar")),
      P_TurbHP_1stRow(signal(start=4383698.917220635))) annotation (Placement(transformation(extent={{-54,-54},{86,16}})));

    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={26.5,91.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={28,104})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={45,93})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={143.5,-10})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={162,-12})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={145,-29})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-50,104})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-53.5,91.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-29,93})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-47,79})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-33,77})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={25,77})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={41,77})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={129,-29})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={127,-13})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-4,84})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={72,84})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={144,-44})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-4,104})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={72,104})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={162,-44})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={132,44})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={164,42})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={132,26})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={162,28})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.504217543078709E-08, displayUnit="bar"),
        DPf(start=0.059636796392389585, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118)),
        Ec(h(start=2962802.891927478))),
      Vv_Tap(C2(h_vol(start=1118785.4784898118))),
      TCond_Tap(C2(h_vol(start=1118785.4784898118)))) annotation (Placement(transformation(extent={{-50,44},{-36,58}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.2048650294668932E-07, displayUnit="bar"),
        DPf(start=0.0596714170932571, displayUnit="bar"),
        Ec(h(start=2654301.081720553)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vol_Tap(h(start=2654301.081720553)),
      Vv_Tap(C2(h_vol(start=710810.4235062235))))
      annotation (Placement(transformation(extent={{12,44},{26,58}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Hx_Hybrid_LP(
        Sc(h_vol(start=393747.5804603163)),
        DPc(start=1.0059502780546025E-06,displayUnit="bar"),
        DPf(start=0.059773365039258015,displayUnit="bar"),
        Ec(h(start=2621238.7036729585)))) annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={107,-31})));
    BOP_2Plug.CTRL_PI                          CTR_P_Tap_IP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={104,50})));
    BOP_2Plug.CTRL_PI                          CTR_P_Tap_LP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={104,24})));
    BOP_2Plug.CTRL_PI_Sat CTRL_PI_Sat(PropInt_P4(
        k=0.5,
        Ti=1,
        maxval=1440,
        minval=1370,
        ureset0=1400,
        permanent=false)) annotation (Placement(transformation(
          extent={{7.5,-5.5},{-7.5,5.5}},
          rotation=90,
          origin={-74.5,-46.5})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=90,
          origin={26,-76})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44.5E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-20,44})));
    BOP_2Plug.CTRL_PI                          CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{-16,22},{-2,34}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=305 +
          273) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-94,-46})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-33,-67})));
    BOP_2Plug.HX_HeatInput HX_HeatInput
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={77,-67})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={67,-91})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={87,-91})));
  equation
    connect(nSSS_fluid.flangeB, fluid2TSPro7.port_a)
      annotation (Line(points={{-156,-1.2},{-142,-1.2},{-142,-6},{-135.84,-6}},     color={0,0,255}));
    connect(nSSS_fluid.flangeA, fluid2TSPro8.port_b)
      annotation (Line(points={{-156,-23.16},{-136,-23.16},{-136,-22},{-134,-22}}, color={0,0,255}));
    connect(NSSSctrl.actuatorBus, nSSS_fluid.actuatorBus)
      annotation (Line(
        points={{-196.4,36},{-196.4,12},{-202.4,12},{-202.4,5.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus, nSSS_fluid.sensorBus)
      annotation (Line(
        points={{-173.6,36},{-173.6,12},{-171.467,12},{-171.467,5.64}},
        color={255,219,88},
        thickness=0.5));
    connect(HeatNwk_IP_In.y,Heat_IP_Network_Out. m_flow_in)
      annotation (Line(points={{28,99.6},{30.1,99.6},{30.1,96}},
          color={0,0,127}));
    connect(rampLP.y,HeatNwk_LP_In. m_flow_in) annotation (Line(points={{157.6,
            -12},{158,-13.2},{148,-13.2}},     color={0,0,127}));
    connect(HeatNwk_HP_In.y,Heat_HP_Network_Out. m_flow_in)
      annotation (Line(points={{-50,99.6},{-49.9,100},{-49.9,96}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1],fluid2TSPro4. port_a)
      annotation (Line(
        points={{-53.5,87},{-53.5,85.86},{-47,85.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1],fluid2TSPro5. port_b) annotation (
        Line(points={{-29,90},{-28,90},{-28,86},{-33,86},{-33,84}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1],fluid2TSPro1. port_a)
      annotation (Line(
        points={{26.5,87},{25,87},{25,83.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1],fluid2TSPro2. port_b) annotation (
        Line(points={{45,90},{46,90},{46,84},{41,84}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1],fluid2TSPro6. port_a) annotation (
        Line(points={{139,-10},{133.86,-10},{133.86,-13}},
                                                        color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1],fluid2TSPro3. port_b) annotation (
        Line(points={{142,-29},{136,-29}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u,Set_Flow_TapSteam_LP. y)
      annotation (Line(points={{148.8,-44},{157.6,-44}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y,adaptorRealModelicaTSP. u)
      annotation (Line(points={{-4,99.6},{-4,88.8}},  color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y,adaptorRealModelicaTSP1. u)
      annotation (Line(points={{72,99.6},{72,88.8}},  color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u,Set_P_TapSteam_IP. y)
      annotation (Line(points={{136.8,44},{156,44},{156,42},{159.6,42}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u,Set_P_TapSteam_LP. y)
      annotation (Line(points={{136.8,26},{154,26},{154,28},{157.6,28}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-47.0018,72.035},{-47.2,72.035},{-47.2,57.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-47.2,44.2},{-44,44.2},{-44,26},{-41.8148,26},{
            -41.8148,16}},                                                                    color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-44,44.2},{-42,44.2},{-42,28},{-37.4074,28},{-37.4074,16}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-44.2,57.8},{-46,57.8},{-46,66},{-33,66},{-33,70}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-40.2,57.6},{-40.2,64},{-4,64},{-4,79.6}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out,HX_cog_LP.
                fluidInletI) annotation (Line(points={{86,-26.5185},{86,-26.8},
            {100.2,-26.8}},            color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In,HX_cog_LP. fluidOutletI1)
      annotation (Line(points={{86,-30.4074},{86,-30},{100.2,-30}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1,fluid2TSPro6. steam_outlet)
      annotation (Line(points={{113.8,-26.8},{116,-26.8},{116,-13.0017},{
            120.035,-13.0017}},  color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI,fluid2TSPro3. steam_inlet)
      annotation (Line(points={{113.8,-29.8},{114,-29},{122,-29}},
          color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP,adaptorRealModelicaTSP2. outputReal)
      annotation (Line(points={{113.6,-33.8},{118,-33.8},{118,-44},{
            139.6,-44}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Set, adaptorRealModelicaTSP3.outputReal)
      annotation (Line(points={{113.6,51},{124,51},{124,44},{127.6,44}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{113.6,25},{122,25},{122,26},{127.6,26}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{94.4,54.75},{39.8519,54.75},{39.8519,16}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{93.8,43.75},{46.0741,43.75},{46.0741,15.4815}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{94.4,28.75},{59.2963,28.75},{59.2963,15.7407}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{93.8,17.75},{90,17.75},{90,20},{64.7407,20},{
            64.7407,15.2222}},                                                                  color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{24.5556,-53.7407},{24.5556,-68},{26,-68},{26,
            -71.6}},                                                                  color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{-4.625,21.88},{-4.74074,21.88},{-4.74074,16}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set)
      annotation (Line(points={{-20,39.6},{-20,40},{-16,40},{-16,33.76},{-9.7,33.76}}, color={0,0,255}));
    connect(fluid2TSPro7.steam_outlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
      annotation (Line(points={{-120.04,-6.002},{-60,-6.002},{-60,-18.2222},{
            -54.2593,-18.2222}},            color={0,0,255}));
    connect(fluid2TSPro8.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-118,-22},{-60,-22},{-60,-25.7407},{-54.2593,
            -25.7407}},          color={0,0,255}));
    connect(CTRL_PI_Sat.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-68.89,-51.1875},{-62,-51.1875},{-62,-47},{
            -53.4815,-47}},                                                                color={0,0,255}));
    connect(CTRL_PI_Sat.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.T_SG_Out)
      annotation (Line(points={{-69.22,-43.6875},{-60,-43.6875},{-60,-42.5926},
            {-54.2593,-42.5926}},                                                                    color={0,0,255}));
    connect(Set_Pout_SG.y, CTRL_PI_Sat.Set) annotation (Line(points={{-89.6,-46},{-89.6,-45.5625},{-79.89,-45.5625}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-33.5185,-53.7407},{-33,-53.7407},{-33,-61.5}},color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{14.8,44.2},{14.8,22},{6.40741,22},{6.40741,
            16.2593}},                                                                color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{18,44.2},{20,44.2},{20,16.2593},{10.5556,
            16.2593}},                                                              color={255,0,0}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{25.0018,70.035},{25.0018,64},{14.8,64},{14.8,
            57.8}},                                                                  color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet, HX_cog_IP.Water_Cooling_Out)
      annotation (Line(points={{41,70},{30,70},{30,62},{17.8,62},{17.8,57.8}}, color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{21.8,57.6},{72,57.6},{72,79.6}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outb)
      annotation (Line(points={{-12.325,22.24},{-12,22.24},{-12,16},{-9.66667,16}}, color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{74.4,-73.6},{74.4,-82},{67,-82},{67,-85.5}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{79.8,-73.6},{79.8,-80},{87,-80},{87,-85.5}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{5.62963,-54},{5.62963,-60.2},{72.8,-60.2}}, color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{76,-60.2},{98,-60.2},{98,5.37037},{86,5.37037}}, color={255,0,0}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-220,-100},{180,120}})), Diagram(coordinateSystem(
            preserveAspectRatio=false, extent={{-220,-100},{180,120}})));
  end StaticBoP3_T_Plugged_N3S_TMPower;

  model StaticBoP3_P_Plugged_N3S_TMPower "Demo of Static BoP, with Steam Geneator pressure control by inlet water flow - 
  BoP is coupled to the N3S developped by Guido-Polimi-Thermopower"
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164192.02652393395)),
        Qv(start=0.18350725673411197),
        h(start=163778.77086358969),
        Pm(start=363758.37747187633, displayUnit="bar")),
      Sing7(Pm(start=81483.94514840622, displayUnit="bar"), C2(h_vol(start=317879.60972432443))),
      P11(C1(h_vol(start=371761.2447393281))),
      Sing14(Pm(start=693968.8516748664, displayUnit="bar")),
      Vol14(h(start=488060.21488197596)),
      Sing4b(Pm(start=755864.374872922, displayUnit="bar"), C2(h_vol(start=700805.4345049231))),
      Pp4(C2(h_vol(start=2639596.204442198), P(start=755864.3749444388, displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164192.02652393395)),
      Vv10(Pm(start=717608.3791438369, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2147941.5493544163), Cs(h(start=2147941.5493544154))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=2622123.072567575))),
      Pip7(C2(P(start=81483.94541931312, displayUnit="bar"), h_vol(start=2622123.072567575)), Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2622123.072567575)),
      Vol6(h(start=2977596.047246133)),
      Vol5(h(start=2765182.383058621)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=2639596.204442198))),
      P4(C1(h_vol(start=2639596.204442198))),
      Vol4(
        Cs2(Q(start=24.509966251870072)),
        h(start=2639596.204442198),
        Cs3(Q(start=194.00851031135295))),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468547.086316049, displayUnit="bar"), h_vol(start=2943661.0649808757))),
      Sing2(C1(h_vol(start=2943661.0649808757))),
      Bach(
        h(start=488059.2856824448),
        Ce4(h(start=371761.24473932857)),
        Cs1(Q(start=244.2983525092077))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765182.38305862))),
      SupH(
        DPfc(start=39745.57096850667, displayUnit="bar"),
        Ec(h(start=2943661.064980875)),
        Ef(h(start=2765182.383058621)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977596.047246133)),
        DPf(start=81528.70712747396, displayUnit="bar"),
        Sc(h_vol(start=1118787.9839318935))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2943661.064980875),
        Ce(h(start=2943661.0649808734))),
      Vol1(h(start=691077.0439232907)),
      ReH_BP(
        HDesF(start=371761.2447393281),
        HeiF(start=171024.2918910695),
        promeF(d(start=981.436240163131, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164192.02652393395)),
        Hep(start=393725.16088023677)),
      ReH_HP(
        HDesF(start=691075.2597402378),
        HeiF(start=493867.95406250644),
        Se(
          h_vol(start=679621.7421491045),
          h(start=691075.2597402378),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=928.4157518512499, displayUnit="g/cm3")),
        Hep(start=710778.2676195425)),
      Pp_HP(
        C2(h(start=492848.3081997887), h_vol(start=492848.3081997887)),
        Pm(start=2649427.0152045134, displayUnit="bar"),
        Qv(start=0.25309891169526044),
        h(start=490454.2615408823)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4582054.885963371, displayUnit="bar")),
      Vv2(
        Q(start=218.51947656322304),
        C1(h_vol(start=2943661.064980875), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2943661.064980875), P(start=4396325.747708942, displayUnit="bar"))),
      Q1(C1(h_vol(start=691077.0439232907))),
      T1(C1(h_vol(start=691077.0439232907))),
      P1(C1(h_vol(start=691077.0439232907))),
      T2(C1(h_vol(start=2943661.0649808757)), C2(h_vol(start=2943661.0649808757))),
      Turb_BP1(
        Cs(h(start=2622123.072567575), h_vol(start=2619840.125764651)),
        xm(start=0.9903484462100379),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=660260.0479919518, displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7673417791849, displayUnit="g/cm3")), Cv(Q(start=182.16809961917588))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.05459371049196894, displayUnit="g/cm3")),
        xm(start=0.9399708552965609),
        Ce(h_vol(start=2622123.072567575)),
        Pe(start=81036.18813447974, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691077.0439232907)),
      SG_Secondary_Out(h_vol(start=2943661.0649808757), Q(start=244.3003525092077)),
      Turb_HP(Cs(h(start=2639596.2044421975)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4450037.179400733, displayUnit="bar"), h(start=2943661.065035004)),
      HeatNetwk_IP_Out(h_vol(start=2644472.7344897604)),
      HeatNetwk_LP_In(P(start=7055.912619353334, displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2622123.0725958906))) annotation (Placement(transformation(extent={{-40,-50},{100,20}})));

     // Extraction_ReheatBP(Ce(h(start=2619840.125764651))),
     // Extraction_TurbHP_Oulet(Cex(h(start=2492812.8893823405)), h(start=2492812.8893823395)),
     // CsBP1b(start=1692.3189652681808),
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={40.5,95.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={42,108})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={59,97})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={157.5,-6})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-8})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={159,-25})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-36,108})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-39.5,95.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-15,97})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro
                                               fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-33,83})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-19,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={39,81})));
   TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={55,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={143,-25})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={141,-9})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={158,-40})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-40})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=41.65E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-6,50})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,48})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={178,46})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,30})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,32})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.47951833195559E-08, displayUnit="bar"),
        DPf(start=0.05963019887854127, displayUnit="bar"),
        Sc(h_vol(start=1118787.9839318935)),
        Ec(h(start=2943661.064980875))),
      Vv_Tap(C2(h_vol(start=1118787.9839318935))),
      TCond_Tap(C2(h_vol(start=1118787.9839318935)))) annotation (Placement(transformation(extent={{-36,48},{-22,62}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195832880541478E-07, displayUnit="bar"),
        DPf(start=0.059666263367334126, displayUnit="bar"),
        Ec(h(start=2639596.204442199)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vol_Tap(h(start=2639596.204442199)),
      Vv_Tap(C2(h_vol(start=710810.4235062235))))
      annotation (Placement(transformation(extent={{26,48},{40,62}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Hx_Hybrid_LP(
        Sc(h_vol(start=393747.5804603163)),
        DPc(start=1.0063492526775674E-06, displayUnit="bar"),
        DPf(start=0.05977368960696228, displayUnit="bar"),
        Ec(h(start=2622123.072567575)))) annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={121,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,54})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap1
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,28})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=90,
          origin={40,-72})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{0,28},{14,40}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=45e5)
               annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-78,-46})));
    BOP_2Plug.CTRL_PI_Sat
                      CTR_PT_PpHP(PropInt_P4(
        k=-5e-4,
        Ti=1,
        maxval=1440,
        minval=1371,
        ureset0=1400,
        permanent=false))                                    annotation (
        Placement(transformation(
          extent={{6,-5},{-6,5}},
          rotation=90,
          origin={-61,-44})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-21,-65})));
    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
      Placement(transformation(extent={{-86,82},{-66,102}})));
    NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-140,32},{-102,64}})));
    NSSS.NSSS_ThermoPower.NSSSbypass_fluid
                         nSSS_fluid(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      core(fuel(
          Tc(start={599.8246962010348,602.4035817089674,604.9430344251562,607.4409623146475,609.8951796044038,612.3034026102237,614.6632388629673,616.972164951592,
                619.2274875928174,621.4262824828023}, displayUnit="degC"),
          Tci(start={607.357299055964,609.9263179214674,612.4560751618749,614.9444857090893,617.3893710693703,619.7884551655428,622.1393535040091,624.4395511010829,
                626.6863636986716,628.8768768445042}, displayUnit="degC"),
          Tco(start={592.79187164561,595.379969157214,597.9284740671085,600.4352878357251,602.8982178937007,605.3149734545241,607.6831546026796,610.0002300589125,
                612.2634981135637,614.470025264861}, displayUnit="degC"),
          Tvol(start=[1094.685079978524,952.2318771025554,895.1812987635391,841.9907740814742,791.6886939443132; 1097.6352591333634,954.8495228290908,
                897.668461406179,844.3581373341756,793.9444852112443; 1100.543417055899,957.4296795135934,900.1199165863549,846.6914339486301,796.1677435326709;
                1103.4069955902705,959.970087364947,902.5335212396162,848.9886277435271,798.3565319957553; 1106.2233230913153,962.4683864291272,904.9070373736532,
                851.247592405007,800.5088280064261; 1108.9896095674285,964.9221122837564,907.2381279809629,853.4661075998306,802.6225195892222; 1111.7029341358307,
                967.3286849126494,909.5243464719133,855.6418489214594,804.6953958110744; 1114.3602194685786,969.6853860438587,911.7631151491025,857.7723674077579,
                806.7251272704422; 1116.9581868294285,971.9893192807879,913.9516873387103,859.8550535085234,808.7092317742771; 1119.4932853203732,974.2373473748704,
                916.087087813568,861.887080400114,810.645020344306], displayUnit="degC"))),
      flowSplit(out2(m_flow(start=-10.675229181046038))))
      annotation (Placement(transformation(extent={{-150,-34},{-92,2}})));
    AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro7(steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=2962802.891927479)))
      annotation (Placement(transformation(extent={{-72,-18},{-56,-2}})));
    AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro8 annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-62,-26})));
    BOP_2Plug.HX_HeatInput HX_HeatInput
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={83,-61})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={73,-85})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={93,-85})));
  equation
    connect(HeatNwk_IP_In.y, Heat_IP_Network_Out.m_flow_in)
      annotation (Line(points={{42,103.6},{44.1,103.6},{44.1,100}},
          color={0,0,127}));
    connect(rampLP.y, HeatNwk_LP_In.m_flow_in) annotation (Line(points
          ={{171.6,-8},{172,-9.2},{162,-9.2}}, color={0,0,127}));
    connect(HeatNwk_HP_In.y, Heat_HP_Network_Out.m_flow_in)
      annotation (Line(points={{-36,103.6},{-35.9,104},{-35.9,100}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1], fluid2TSPro4.port_a)
      annotation (Line(
        points={{-39.5,91},{-39.5,89.86},{-33,89.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1], fluid2TSPro5.port_b) annotation (
        Line(points={{-15,94},{-14,94},{-14,90},{-19,90},{-19,88}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1], fluid2TSPro1.port_a)
      annotation (Line(
        points={{40.5,91},{39,91},{39,87.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1], fluid2TSPro2.port_b) annotation (
        Line(points={{59,94},{60,94},{60,88},{55,88}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro6.port_a) annotation (
        Line(points={{153,-6},{147.86,-6},{147.86,-9}}, color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1], fluid2TSPro3.port_b) annotation (
        Line(points={{156,-25},{150,-25}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u, Set_Flow_TapSteam_LP.y)
      annotation (Line(points={{162.8,-40},{171.6,-40}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y, adaptorRealModelicaTSP.u)
      annotation (Line(points={{10,103.6},{10,92.8}}, color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y, adaptorRealModelicaTSP1.u)
      annotation (Line(points={{86,103.6},{86,92.8}}, color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u, Set_P_TapSteam_IP.y)
      annotation (Line(points={{150.8,48},{170,48},{170,46},{173.6,46}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u, Set_P_TapSteam_LP.y)
      annotation (Line(points={{150.8,30},{168,30},{168,32},{171.6,32}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-33.0018,76.035},{-33.2,76.035},{-33.2,61.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-33.2,48.2},{-30,48.2},{-30,30},{-27.8148,30},{-27.8148,20}}, color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-30,48.2},{-28,48.2},{-28,32},{-23.4074,32},{-23.4074,20}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-30.2,61.8},{-32,61.8},{-32,70},{-19,70},{-19,74}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-26.2,61.6},{-26.2,68},{10,68},{10,83.6}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out,
      HX_cog_LP.fluidInletI) annotation (Line(points={{100,-22.5185},{100,-22.8},{114.2,-22.8}},
                                       color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, HX_cog_LP.fluidOutletI1)
      annotation (Line(points={{100,-26.4074},{100,-26},{114.2,-26}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro6.steam_outlet)
      annotation (Line(points={{127.8,-22.8},{130,-22.8},{130,-9.00175},
            {134.035,-9.00175}}, color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI, fluid2TSPro3.steam_inlet)
      annotation (Line(points={{127.8,-25.8},{128,-25},{136,-25}},
          color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP, adaptorRealModelicaTSP2.outputReal)
      annotation (Line(points={{127.6,-29.8},{132,-29.8},{132,-40},{
            153.6,-40}}, color={0,0,255}));
    connect(CTR_P_Tap.Set, adaptorRealModelicaTSP3.outputReal) annotation (Line(points={{127.6,55},{138,55},{138,48},{141.6,48}}, color={0,0,255}));
    connect(CTR_P_Tap1.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{127.6,29},{136,29},{136,30},{141.6,30}}, color={0,0,255}));
    connect(CTR_P_Tap.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{108.4,58.75},{53.8519,58.75},{53.8519,20}}, color={0,0,255}));
    connect(CTR_P_Tap.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{107.8,47.75},{60.0741,47.75},{60.0741,19.4815}}, color={0,0,255}));
    connect(CTR_P_Tap1.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{108.4,32.75},{73.2963,32.75},{73.2963,19.7407}}, color={0,0,255}));
    connect(CTR_P_Tap1.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{107.8,21.75},{104,21.75},{104,24},{78.7407,24},{78.7407,19.2222}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{38.5556,-49.7407},{40,-52},{40,-67.6}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{11.375,27.88},{12,27.88},{12,20},{9.25926,20}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set) annotation (Line(points={{-6,45.6},{2,45.6},{2,42},{6.3,42},{6.3,39.76}}, color={0,0,255}));
    connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-55.9,-47.75},{-44,-47.75},{-44,-43},{-39.4815,-43}}, color={0,0,255}));
    connect(Set_Pout_SG.y, CTR_PT_PpHP.Set) annotation (Line(points={{-73.6,-46},{-70,-46},{-70,-43.25},{-65.9,-43.25}},
                                                                                                                       color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-19.5185,-49.7407},{-21,-49.7407},{-21,-59.5}},color={0,0,255}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{39.0018,74.035},{39.0018,66},{28.8,66},{28.8,61.8}}, color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{28.8,48.2},{28.8,24},{20.4074,24},{20.4074,20.2593}}, color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{32,48.2},{34,48.2},{34,20.2593},{24.5556,20.2593}}, color={255,0,0}));
    connect(HX_cog_IP.Water_Cooling_Out, fluid2TSPro2.steam_inlet)
      annotation (Line(points={{31.8,61.8},{32,61.8},{32,64},{55,64},{55,74}}, color={255,0,0}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{35.8,61.6},{86,61.6},{86,83.6}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbHP_1stRow)
      annotation (Line(points={{3.675,28.24},{3.675,26},{13.6667,26},{13.6667,20}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outa, CTR_PT_PpHP.Sensor)
      annotation (Line(points={{-40.2593,-34.963},{-44,-34.963},{-44,-40},{-56.2,-40},{-56.2,-41.75}}, color={0,0,255}));
    connect(nSSS_fluid.flangeB,fluid2TSPro7. port_a)
      annotation (Line(points={{-92,-5.2},{-78,-5.2},{-78,-10},{-71.84,-10}},       color={0,0,255}));
    connect(nSSS_fluid.flangeA,fluid2TSPro8. port_b)
      annotation (Line(points={{-92,-27.16},{-72,-27.16},{-72,-26},{-70,-26}},     color={0,0,255}));
    connect(NSSSctrl.actuatorBus,nSSS_fluid. actuatorBus)
      annotation (Line(
        points={{-132.4,32},{-132.4,8},{-138.4,8},{-138.4,1.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus,nSSS_fluid. sensorBus)
      annotation (Line(
        points={{-109.6,32},{-109.6,8},{-107.467,8},{-107.467,1.64}},
        color={255,219,88},
        thickness=0.5));
    connect(fluid2TSPro7.steam_outlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
      annotation (Line(points={{-56.04,-10.002},{-48,-10.002},{-48,-14.2222},{-40.2593,-14.2222}},
                                            color={0,0,255}));
    connect(fluid2TSPro8.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-54,-26},{-46,-26},{-46,-21.7407},{-40.2593,-21.7407}},
                                 color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{80.4,-67.6},{80.4,-76},{73,-76},{73,-79.5}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{85.8,-67.6},{85.8,-74},{93,-74},{93,-79.5}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{19.6296,-50},{19.6296,-54},{78,-54},{78,-54.2},{78.8,-54.2}}, color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{82,-54.2},{82,-54},{106,-54},{106,9.37037},{100,9.37037}}, color={255,0,0}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,-100},{200,120}})),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-160,-100},{200,120}})),
      __Dymola_Commands(file="../../../../../tot5Mai.mos" "tot5Mai"));
  end StaticBoP3_P_Plugged_N3S_TMPower;

  model StaticBoP3_T_Plugged_N3Ssimple_TMPower_Tail
    "Demo of Static BoP, with Steam Geneator temperature control by inlet water flow and NSSS model"
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164191.70520937204)),
        Qv(start=0.18396416597366846),
        h(start=163778.6102063112),
        Pm(start=363772.87837973965,displayUnit="bar")),
      Sing7(Pm(start=81483.88090490695, displayUnit="bar"), C2(h_vol(start=
                318004.84418054804))),
      P11(C1(h_vol(start=371631.93129043165))),
      Sing14(Pm(start=694616.4546996551, displayUnit="bar")),
      Vol14(h(start=487996.72664115264)),
      Sing4b(Pm(start=755863.7314451099, displayUnit="bar"), C2(h_vol(start=
                700826.4859500551))),
      Pp4(C2(h_vol(start=2639188.304057642),  P(start=755863.731516966,  displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164191.70520937204)),
      Vv10(Pm(start=717608.3792336329, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2147534.9782363693),Cs(h(start=2147534.97823637))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=
                2621755.7202959014))),
      Pip7(C2(P(start=81483.88117689801, displayUnit="bar"), h_vol(start=
                2621755.7202959014)),                                                         Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2621755.7202959014)),
      Vol6(h(start=2977542.9234993923)),
      Vol5(h(start=2765272.378605388)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=
                2639188.304057642))),
      P4(C1(h_vol(start=2639188.304057642))),
      Vol4(Cs2(Q(start=24.570618097912405)),
                                          h(start=2639188.304057642)),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468607.801056146, displayUnit="bar"), h_vol(start=
                2943650.4153317506))),
      Sing2(C1(h_vol(start=2943650.415331751)), Pm(start=4484416.962964356,
            displayUnit="bar")),
      Bach(
        h(start=487995.7996310873),
        Ce4(h(start=371631.2995773625)),
        Cs1(Q(start=244.2983525092077)),
        Ce3(Q(start=21.204195564406678))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765284.8485874794))),
      SupH(
        DPfc(start=39891.1456653408,   displayUnit="bar"),
        Ec(h(start=2943650.4153317516)),
        Ef(h(start=2765272.378605388)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977542.923499393)),
        DPf(start=81735.80129097332, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2943650.4153317516),
        Ce(h(start=2943650.415331751))),
      Vol1(h(start=690937.8423668927)),
      ReH_BP(
        HDesF(start=371631.93129043165),
        HeiF(start=171009.89993465273),
        promeF(d(start=981.4445528787293, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164191.70520937204)),
        Hep(start=393725.0712697711)),
      ReH_HP(
        HDesF(start=690936.0623263465),
        HeiF(start=493804.4575817768),
        Se(
          h_vol(start=679621.7421491045),
          h(start=690936.0623263465),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=928.4366408318273, displayUnit="g/cm3")),
        Hep(start=710778.0928075291)),
      Pp_HP(
        C2(h(start=492787.15518248087), h_vol(start=492787.15518248087)),
        Pm(start=2649394.384329739,  displayUnit="bar"),
        Qv(start=0.2537658926107881),
        h(start=490391.9409118168)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4581980.951330721, displayUnit="bar")),
      Vv2(
        Q(start=219.1155858553195),
        C1(h_vol(start=2943650.4153317516), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2943650.4153317516), P(start=4400001.189086886, displayUnit="bar"))),
      Q1(C1(h_vol(start=690937.8423668927))),
      T1(C1(h_vol(start=690937.8423668925))),
      P1(C1(h_vol(start=690937.8423668926))),
      T2(C1(h_vol(start=2943650.4153317506)), C2(h_vol(start=2943650.4153317516))),
      Turb_BP1(
        Cs(h(start=2621755.7202959023), h_vol(start=2619840.125764651)),
        xm(start=0.9902676112626673),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=661879.0065677789,displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7572466324344, displayUnit="g/cm3")), Cv(Q(start=182.1681024769765))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.054609316755873674,displayUnit="g/cm3")),
        xm(start=0.9397529567878202),
        Ce(h_vol(start=2621755.7202959014)),
        Pe(start=81238.43223648514, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691076.9808245787)),
      SG_Secondary_Out(h_vol(start=2943650.4153317516), Q(start=239.72480633161524)),
      Turb_HP(Cs(h(start=2639188.304057642)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4451161.532520275, displayUnit="bar"), h(start=
              2943650.4153191415)),
      HeatNetwk_IP_Out(h_vol(start=2639595.2860046183)),
      HeatNetwk_LP_In(P(start=7056.178861423586,
                                               displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2621755.720373541)),
      HeatNetwk_HP_In(P(start=4624255.859260167, displayUnit="bar")),
      P_TurbHP_1stRow(signal(start=4412190.189143764)),
      Pp_LP_Out(h(start=486995.79950686404)))                         annotation (Placement(transformation(extent={{-40,-50},{100,20}})));

     // Extraction_ReheatBP(Ce(h(start=2619840.125764651))),
     // Extraction_TurbHP_Oulet(Cex(h(start=2492812.8893823405)), h(start=2492812.8893823395)),
     // CsBP1b(start=1692.3189652681808),
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={40.5,95.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={42,108})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={59,97})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={157.5,-6})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-8})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={159,-25})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-36,108})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-39.5,95.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-15,97})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-33,83})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-19,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={39,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={55,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={143,-25})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={141,-9})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={158,-40})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-40})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,48})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={178,46})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,30})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,32})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.479524994983914E-08,  displayUnit="bar"),
        DPf(start=0.05963019607638345, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118)),
        Ec(h(start=2943650.4153317516))),
      Vv_Tap(C2(h_vol(start=1118785.4784898118))),
      TCond_Tap(C2(h_vol(start=1118785.4784898118)))) annotation (Placement(transformation(extent={{-36,48},{-22,62}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195582336659725E-07, displayUnit="bar"),
        DPf(start=0.05966612058428442, displayUnit="bar"),
        Ec(h(start=2639188.304057642)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vv_Tap(C2(h_vol(start=710810.4235062235))),
      Vol_Tap(h(start=2639188.304057642)))
      annotation (Placement(transformation(extent={{26,48},{40,62}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.006183525146145E-06,  displayUnit="bar"),
        DPf(start=0.059773554781532685,displayUnit="bar"),
        Ec(h(start=2621755.7202959014)),
        Sc(h_vol(start=393747.5804603163))),
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={121,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_IP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,54})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_LP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,28})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={39,-65})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{-2,26},{12,38}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=300 + 273)
               annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-72,-42})));
    BOP_2Plug.CTRL_PI CTR_PT_PpHP annotation (Placement(transformation(
          extent={{6,-5},{-6,5}},
          rotation=90,
          origin={-57,-42})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-19,-67})));
    BOP_2Plug.HX_HeatInput HX_HeatInput(T_HeatInput(C2(h_vol(start=
                487995.7996310873))), Vv_HeatInput(C2(h_vol(start=
                487995.7996310873))))
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={71,-65})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={81,-89})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44.5E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={4,52})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={61,-89})));
    NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-174,22},{-136,54}})));
    NSSS.NSSS_ThermoPower.NSSSsimplified_fluid            nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      SG(Secondary(
          noInitialPressure=false,
          h(start={690937.8423668928,1064870.7514157868,1340564.8159344522,
                1583442.3069997572,1767402.42179104,1959645.4744647408,
                2192813.978529689,2416513.215588785,2617288.116428511,
                2817518.4492800417,2943650.415331751}),
          heatTransfer(gamma(start=14308.517324568174))), Primary(p(start=
                14767027.575329589, displayUnit="bar"), wall(T(start={
                  589.9042652165566,588.0176336049378,585.7288398453098,
                  583.4920193232052,584.0264664098835,580.3449429668044,
                  578.0726833260838,576.0467781505952,574.0854410761073,
                  565.9519664428825}, displayUnit="degC")))),
      LowerPlenum(p(start=15058335.330254601, displayUnit="bar")),
      core(fuel(
          Tc(start={599.3746604503278,601.9263151032364,604.4400160445794,
                606.9137767799631,609.3455224084037,611.7330866429734,
                614.0742035048177,616.3664897983715,618.607413552013,
                620.7942433521856}, displayUnit="degC"),
          Tci(start={607.0709641760956,609.6126427101676,612.1165362433052,
                614.5806650372223,617.0029612480846,619.3812659581304,
                621.7133209014313,623.9967510064081,626.2290329574838,
                628.4074447228353}, displayUnit="degC"),
          Tco(start={592.188996361697,594.7499652310686,597.272822871972,
                599.7555764821652,602.1961445717468,604.592353973558,
                606.9419315075601,609.2424863932183,611.4914785771831,
                613.6861678866301}, displayUnit="degC"),
          Tvol(start=[1106.578817096677,960.0016500293058,901.3910693375121,
                846.8076204597714,795.2455372673762; 1109.5085702143774,
                962.594134240766,903.8514625360174,849.146914731936,
                797.4722138923822; 1112.397829648536,965.150584505449,
                906.2775710010369,851.4535329875008,799.6677161485076;
                1115.244154221902,967.6688503583792,908.6673577900106,
                853.7255420780147,801.830207141639; 1118.0449952223844,
                970.1466866839086,911.018696364572,855.9609238831752,
                803.9577692917536; 1120.797692899955,972.5817506196134,
                913.3293676555867,858.1575725238752,806.0484016835574;
                1123.4994668379209,974.9715930399968,915.5970519842706,
                860.3132866846092,808.100012762022; 1126.147395663902,
                977.3136406117213,917.8193120372151,862.4257534319092,
                810.1104049349835; 1128.7383804693648,979.6051634442834,
                919.9935621757226,864.4925190441909,812.0772468165186;
                1131.269085978881,981.8432230723798,922.117019087871,
                866.5109421107381,813.9980286004151], displayUnit="degC")),
          neutronicKinetics(D(start={2.6297433004241203E+17,
                6.425892720202542E+17,1.7257554821707536E+17,
                1.6728770166583648E+17,13303534357977988.0,1894272960287383.5}),
            P(start=540e6, fixed=true))),
      flangeA(h_outflow(start=1064870.7514157868)),
      flangeB(h_outflow(start=2943650.415331751)),
      pressurizer(pressurizer(h(start=1862126.0574145121))),
      pump(h(start=1339553.169100718), q_single(start=0.8497619609894508)),
      htc2(start=14308.518, fixed=false))
      annotation (Placement(transformation(extent={{-184,-40},{-126,-4}})));
    AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro7(steam_outlet(h(start=
              2944000.0)), port_a(h_outflow(start=2962802.891927479)))
      annotation (Placement(transformation(extent={{-100,-22},{-84,-6}})));
    AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro8 annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-90,-30})));
    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
      Placement(transformation(extent={{-160,80},{-140,100}})));
  equation
    connect(HeatNwk_IP_In.y, Heat_IP_Network_Out.m_flow_in)
      annotation (Line(points={{42,103.6},{44.1,103.6},{44.1,100}},
          color={0,0,127}));
    connect(rampLP.y, HeatNwk_LP_In.m_flow_in) annotation (Line(points
          ={{171.6,-8},{172,-9.2},{162,-9.2}}, color={0,0,127}));
    connect(HeatNwk_HP_In.y, Heat_HP_Network_Out.m_flow_in)
      annotation (Line(points={{-36,103.6},{-35.9,104},{-35.9,100}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1], fluid2TSPro4.port_a)
      annotation (Line(
        points={{-39.5,91},{-39.5,89.86},{-33,89.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1], fluid2TSPro5.port_b) annotation (
        Line(points={{-15,94},{-14,94},{-14,90},{-19,90},{-19,88}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1], fluid2TSPro1.port_a)
      annotation (Line(
        points={{40.5,91},{39,91},{39,87.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1], fluid2TSPro2.port_b) annotation (
        Line(points={{59,94},{60,94},{60,88},{55,88}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro6.port_a) annotation (
        Line(points={{153,-6},{147.86,-6},{147.86,-9}}, color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1], fluid2TSPro3.port_b) annotation (
        Line(points={{156,-25},{150,-25}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u, Set_Flow_TapSteam_LP.y)
      annotation (Line(points={{162.8,-40},{171.6,-40}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y, adaptorRealModelicaTSP.u)
      annotation (Line(points={{10,103.6},{10,92.8}}, color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y, adaptorRealModelicaTSP1.u)
      annotation (Line(points={{86,103.6},{86,92.8}}, color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u, Set_P_TapSteam_IP.y)
      annotation (Line(points={{150.8,48},{170,48},{170,46},{173.6,46}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u, Set_P_TapSteam_LP.y)
      annotation (Line(points={{150.8,30},{168,30},{168,32},{171.6,32}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-33.0018,76.035},{-33.2,76.035},{-33.2,61.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-33.2,48.2},{-30,48.2},{-30,30},{-27.8148,30},{-27.8148,20}}, color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-30,48.2},{-28,48.2},{-28,32},{-23.4074,32},{-23.4074,20}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-30.2,61.8},{-32,61.8},{-32,70},{-19,70},{-19,74}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-26.2,61.6},{-26.2,68},{10,68},{10,83.6}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Set, adaptorRealModelicaTSP3.outputReal)
      annotation (Line(points={{127.6,55},{138,55},{138,48},{141.6,48}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{127.6,29},{136,29},{136,30},{141.6,30}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{108.4,58.75},{53.8519,58.75},{53.8519,20}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{107.8,47.75},{60.0741,47.75},{60.0741,19.4815}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{108.4,32.75},{73.2963,32.75},{73.2963,19.7407}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{107.8,21.75},{104,21.75},{104,24},{78.7407,24},{78.7407,19.2222}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{38.5556,-49.7407},{38.5556,-52},{38,-52},{38,-58},{39,-58},{39,-59.5}},
                                                                                      color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{9.375,25.88},{9.25926,25.88},{9.25926,20}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outb)
      annotation (Line(points={{1.675,26.24},{2,26.24},{2,20},{4.33333,20}}, color={0,0,255}));
    connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-51.9,-45.75},{-44,-45.75},{-44,-43},{-39.4815,-43}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.T_SG_Out, CTR_PT_PpHP.Sensor)
      annotation (Line(points={{-40.2593,-38.5926},{-54,-38.5926},{-54,-39.15},{-52.2,-39.15}}, color={0,0,255}));
    connect(Set_Pout_SG.y, CTR_PT_PpHP.Set) annotation (Line(points={{-67.6,-42},{-64,-42},{-64,-41.4},{-61.8,-41.4}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-19.5185,-49.7407},{-19,-49.7407},{-19,-61.5}},color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out, HX_cog_LP.fluidInletI)
      annotation (Line(points={{100,-22.5185},{100,-22.8},{114.2,-22.8}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, HX_cog_LP.fluidOutletI1)
      annotation (Line(points={{100,-26.4074},{100,-26},{114.2,-26}}, color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro6.steam_outlet)
      annotation (Line(points={{127.8,-22.8},{130,-22.8},{130,-9.00175},{134.035,-9.00175}}, color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI, fluid2TSPro3.steam_inlet) annotation (Line(points={{127.8,-25.8},{128,-25},{136,-25}}, color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP, adaptorRealModelicaTSP2.outputReal)
      annotation (Line(points={{127.6,-29.8},{132,-29.8},{132,-40},{153.6,-40}}, color={0,0,255}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{39.0018,74.035},{39.0018,66},{28.8,66},{28.8,61.8}}, color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet, HX_cog_IP.Water_Cooling_Out) annotation (Line(points={{55,74},{55,64},{31.8,64},{31.8,61.8}}, color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{35.8,61.6},{86,61.6},{86,83.6}}, color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{28.8,48.2},{28.8,24},{20.4074,24},{20.4074,20.2593}}, color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{32,48.2},{32,20.2593},{24.5556,20.2593}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{19.6296,-50},{20,-50},{20,-56},{66.8,-56},{66.8,-58.2}}, color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{70,-58.2},{72,-58.2},{72,-56},{106,-56},{106,9.37037},{100,9.37037}}, color={255,0,0}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{73.8,-71.6},{73.8,-78},{81,-78},{81,-83.5}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set) annotation (Line(points={{4,47.6},{4.3,48},{4.3,37.76}}, color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{68.4,-71.6},{68.4,-80},{61,-80},{61,-83.5}}, color={0,0,255}));
    connect(NSSSctrl.actuatorBus,nsss. actuatorBus) annotation (Line(
        points={{-166.4,22},{-166.4,2},{-169.5,2},{-169.5,-4.36}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus,nsss. sensorBus) annotation (Line(
        points={{-143.6,22},{-143.6,2},{-140.5,2},{-140.5,-4.36}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.flangeB, fluid2TSPro7.port_a) annotation (Line(points={{-126,
            -11.2},{-106,-11.2},{-106,-14},{-99.84,-14}}, color={0,0,255}));
    connect(nsss.flangeA, fluid2TSPro8.port_b) annotation (Line(points={{-126,
            -33.16},{-122,-33.16},{-122,-34},{-114,-34},{-114,-30},{-98,-30}},
          color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out, fluid2TSPro7.steam_outlet)
      annotation (Line(points={{-40.2593,-14.2222},{-86,-14.2222},{-86,-14.002},{-84.04,-14.002}},
                               color={0,0,255}));
    connect(fluid2TSPro8.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-82,-30},{-68,-30},{-68,-21.7407},{-40.2593,-21.7407}},
                        color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-200,
              -100},{200,120}})),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-200,-100},
              {200,120}})));
  end StaticBoP3_T_Plugged_N3Ssimple_TMPower_Tail;

  model StaticBoP3_T_Plugged_N3Sbypass_TMPower_Tail
    "Demo of Static BoP, with Steam Geneator temperature control by inlet water flow and NSSS model"
    BOP_2Plug.StaticBoP3 StaticBOP2_PlugSGTSP_PlugCogSML(
      Pp_LP(
        C2(h(start=164184.75305878202), h_vol(start=164191.70520937204)),
        Qv(start=0.1839654387530579),
        h(start=163778.6102063112),
        Pm(start=363772.9188248031, displayUnit="bar")),
      Sing7(Pm(start=81483.88090490695, displayUnit="bar"), C2(h_vol(start=
                318005.192183456))),
      P11(C1(h_vol(start=371631.57092418283))),
      Sing14(Pm(start=694616.1608640878, displayUnit="bar")),
      Vol14(h(start=487996.53720115323)),
      Sing4b(Pm(start=755863.7314451099, displayUnit="bar"), C2(h_vol(start=
                700826.5438211269))),
      Pp4(C2(h_vol(start=2639187.1729444573), P(start=755863.731516966,  displayUnit="bar"))),
      Vv12(Ouv(signal(start=0.7, fixed=false))),
      Vol10(h(start=164191.70520937204)),
      Vv10(Pm(start=717608.3792336329, displayUnit="bar")),
      Vol9(h(start=163365.51520324874)),
      Vol8(h(start=2147533.847793209), Cs(h(start=2147533.8477932094))),
      Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=
                2621754.698190866))),
      Pip7(C2(P(start=81483.88117689801, displayUnit="bar"), h_vol(start=
                2621754.698190866)),                                                          Q(start=16.0)),
      Vol7(
        Ce(h(start=2619840.1257646517)),
        Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
        Cs2(h(start=2620466.1773813386)),
        h(start=2621754.698190866)),
      Vol6(h(start=2977542.7754979664)),
      Vol5(h(start=2765272.663770078)),
      Vv4(Pm(start=740860.4782670301, displayUnit="bar"), C1(h_vol(start=
                2639187.1729444573))),
      P4(C1(h_vol(start=2639187.1729444573))),
      Vol4(Cs2(Q(start=24.57078249425477)),
                                          h(start=2639187.1729444573)),
      Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026, displayUnit="bar"))),
      P2b(C1(h_vol(start=2751055.4478353322))),
      Pip2(C1(P(start=4468607.801056146, displayUnit="bar"), h_vol(start=
                2943650.4153317506))),
      Sing2(C1(h_vol(start=2943650.415331751)), Pm(start=4484417.4645256195,
            displayUnit="bar")),
      Bach(
        h(start=487995.61019709776),
        Ce4(h(start=371630.9392145475)),
        Cs1(Q(start=244.2983525092077)),
        Ce3(Q(start=21.204195564406678))),
      Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765285.1336684935))),
      SupH(
        DPfc(start=39891.53746390497,  displayUnit="bar"),
        Ec(h(start=2943650.4153317516)),
        Ef(h(start=2765272.663770078)),
        Sf(h(start=2977121.8265122348), h_vol(start=2977542.7754979664)),
        DPf(start=81736.3796185704,  displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118))),
      Vol2(
        Cs1(h(start=2854461.5968234967)),
        h(start=2943650.4153317516),
        Ce(h(start=2943650.415331751))),
      Vol1(h(start=690937.5170823894)),
      ReH_BP(
        HDesF(start=371631.57092418283),
        HeiF(start=171009.85992957553),
        promeF(d(start=981.4445528787293, displayUnit="g/cm3")),
        Ee(h_vol(start=164205.69145221374), h(start=164191.70520937204)),
        Hep(start=393725.0712697711)),
      ReH_HP(
        HDesF(start=690935.7370535439),
        HeiF(start=493804.37467021996),
        Se(
          h_vol(start=679621.7421491045),
          h(start=690935.7370535439),
          P(start=4972964.078124865, displayUnit="bar")),
        promeF(d(start=928.4367468683788, displayUnit="g/cm3")),
        Hep(start=710778.0928075291)),
      Pp_HP(
        C2(h(start=492787.07888326974), h_vol(start=492787.07888326974)),
        Pm(start=2649442.105511399,  displayUnit="bar"),
        Qv(start=0.253767739082038),
        h(start=490391.80804221146)),
      Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4582076.374696884, displayUnit="bar")),
      Vv2(
        Q(start=219.11724468396753),
        C1(h_vol(start=2943650.4153317516), P(start=4451800.957266087, displayUnit="bar")),
        C2(h_vol(start=2943650.4153317516), P(start=4400001.189086886, displayUnit="bar"))),
      Q1(C1(h_vol(start=690937.5170823896))),
      T1(C1(h_vol(start=690937.5170823893))),
      P1(C1(h_vol(start=690937.5170823894))),
      T2(C1(h_vol(start=2943650.4153317506)), C2(h_vol(start=2943650.4153317516))),
      Turb_BP1(
        Cs(h(start=2621754.698190866),  h_vol(start=2619840.125764651)),
        xm(start=0.9902673863510139),
        Ps(start=81048.13419790535, displayUnit="bar"),
        Pe(start=661883.5164701171,displayUnit="bar")),
      CsBP1a(start=24789.121403189514),
      CsBP2(start=629.7547379428868),
      CsHP(start=651197.0971866344),
      HeatSink(proe(d(start=990.7572185066449, displayUnit="g/cm3")), Cv(Q(start=182.1681024769765))),
      SPurgeBP(start=188.23374753410693),
      SPurgeHP(start=35.86826283480325),
      ScondesBP(start=1117.4768845481353),
      ScondesHP(start=1340.6503847191232),
      Turb_BP2(
        Ps(start=7056.561129075783, displayUnit="bar"),
        pros(d(start=0.054609360193241585,displayUnit="g/cm3")),
        xm(start=0.9397523505551033),
        Ce(h_vol(start=2621754.698190866)),
        Pe(start=81238.99564868012, displayUnit="bar")),
      SG_Secondary_In(h_vol(start=691076.9808245787)),
      SG_Secondary_Out(h_vol(start=2943650.4153317516), Q(start=239.72480633161524)),
      Turb_HP(Cs(h(start=2639187.172944457)), Ps(start=756001.1492580863, displayUnit="bar")),
      HeatNetwk_HP_Out(P(start=4451161.532520275, displayUnit="bar"), h(start=
              2943650.4153191415)),
      HeatNetwk_IP_Out(h_vol(start=2639595.2860046183)),
      HeatNetwk_LP_In(P(start=7056.17960392654,displayUnit="bar")),
      HeatNetwk_LP_Out(h(start=2621754.698216772)),
      HeatNetwk_HP_In(P(start=4624351.8892946225,displayUnit="bar")),
      P_TurbHP_1stRow(signal(start=4412224.1208061995)),
      Pp_LP_Out(h(start=486995.610146984)))                           annotation (Placement(transformation(extent={{-40,-50},{100,20}})));

     // Extraction_ReheatBP(Ce(h(start=2619840.125764651))),
     // Extraction_TurbHP_Oulet(Cex(h(start=2492812.8893823405)), h(start=2492812.8893823395)),
     // CsBP1b(start=1692.3189652681808),
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={40.5,95.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={42,108})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={59,97})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={157.5,-6})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-8})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={159,-25})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-36,108})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-39.5,95.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-15,97})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro4 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-33,83})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro5
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-19,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={39,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={55,81})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={143,-25})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={141,-9})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP1
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,88})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={158,-40})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={10,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={86,108})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,-40})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP3
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,48})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
      height=0,
      duration=900,
      offset=7.56e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={178,46})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP4
                             annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=180,
          origin={146,30})));
    Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
      height=0,
      duration=900,
      offset=0.815e5,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={176,32})));
    BOP_2Plug.HX_cog HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.479524994983914E-08,  displayUnit="bar"),
        DPf(start=0.05963019607638345, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118)),
        Ec(h(start=2943650.4153317516))),
      Vv_Tap(C2(h_vol(start=1118785.4784898118))),
      TCond_Tap(C2(h_vol(start=1118785.4784898118)))) annotation (Placement(transformation(extent={{-36,48},{-22,62}})));
    BOP_2Plug.HX_cog    HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195581641898213E-07, displayUnit="bar"),
        DPf(start=0.05966612058428442, displayUnit="bar"),
        Ec(h(start=2639187.1729444573)),
        Sc(h_vol(start=710810.4235062235))),
      TCond_Tap(C2(h_vol(start=710810.4235062235))),
      Vv_Tap(C2(h_vol(start=710810.4235062235))),
      Vol_Tap(h(start=2639187.1729444573)))
      annotation (Placement(transformation(extent={{26,48},{40,62}})));
    BOP_2Plug.HX_cog_LP HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.0061830640975333E-06, displayUnit="bar"),
        DPf(start=0.059773554781532685,displayUnit="bar"),
        Ec(h(start=2621754.698190866)),
        Sc(h_vol(start=393747.5804603163))),
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={121,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_IP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,54})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_LP annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={118,28})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
      Starttime=100,
      Duration=900,
      Initialvalue=7.147E5,
      Finalvalue=7.147E5) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={39,-65})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
      annotation (Placement(transformation(extent={{-2,26},{12,38}})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=300 + 273)
               annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={-72,-42})));
    BOP_2Plug.CTRL_PI CTR_PT_PpHP annotation (Placement(transformation(
          extent={{6,-5},{-6,5}},
          rotation=90,
          origin={-57,-42})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
      Starttime=100,
      Duration=900,
      Initialvalue=1,
      Finalvalue=1) annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-19,-67})));
    BOP_2Plug.HX_HeatInput HX_HeatInput(T_HeatInput(C2(h_vol(start=
                487995.61019709776))), Vv_HeatInput(C2(h_vol(start=
                487995.61019709776))))
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={71,-65})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={81,-89})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44.5E5)
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={4,52})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={61,-89})));
    NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-174,22},{-136,54}})));
    NSSS.NSSS_ThermoPower.NSSSbypass_fluid                nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      SG(Secondary(
          noInitialPressure=false,
          h(start={690937.5170823897,1067049.8813956545,1343087.2266229836,
                1586609.6032363991,1770422.5539011476,1962633.1347483664,
                2196260.7317245477,2420249.168250299,2621117.6683182376,
                2820106.1949159433,2943650.388996709}),
          heatTransfer(gamma(start=14826.950527435387))), Primary(p(start=
                14767027.575329589, displayUnit="bar"), wall(T(start={
                  589.0401332041343,587.1365687114652,584.8458354237409,
                  582.6097508026114,583.2206833440588,579.5373335708282,
                  577.242287796009,575.223304132996,573.2704856470505,
                  565.0973083325083}, displayUnit="degC")))),
      LowerPlenum(p(start=15058335.330254601, displayUnit="bar")),
      core(fuel(
          Tc(start={599.1106157951681,601.7512096861052,604.3518135503487,
                606.9102532724794,609.4242547480337,611.8914412465658,
                614.3093248802053,616.6752874388995,618.9865445875104,
                621.24008696637}, displayUnit="degC"),
          Tci(start={606.8079530366643,609.4382212318815,612.0286775374298,
                614.5771552351927,617.081387958137,619.5390070628982,
                621.9475331308953,624.3043568771762,626.6067034848081,
                628.8515739305334}, displayUnit="degC"),
          Tco(start={591.9239867632183,594.5742212535351,597.1842993992911,
                599.7520401785657,602.275162262833,604.7512813711336,
                607.1779017082334,609.5523967311484,611.8719732180551,
                614.1336121586829}, displayUnit="degC"),
          Tvol(start=[1106.2758276373306,959.7335284809009,901.1366039283432,
                846.5656750780728,795.0152353882587; 1109.3074170882123,
                962.4161441682506,903.6825441628615,848.9863129814548,
                797.3193461188739; 1112.2963976593198,965.0608396899214,
                906.1924033243015,851.3725612959554,799.5906462818721;
                1115.2400979623599,967.6652617484018,908.6639523261746,
                853.7223045021873,801.8271256762409; 1118.1357249312618,
                970.2269499507829,911.094860707516,856.033330939396,
                804.0266828100597; 1120.980360699108,972.7433340495853,
                913.4826940182855,858.3033303237257,806.1871225422683;
                1123.7709527036961,975.2117251914905,915.8249055178698,
                860.5298858593987,808.3061485775739; 1126.5042915025958,
                977.6292962953804,918.118817558885,862.7104565448014,
                810.3813466300447; 1129.1769692782093,979.9930453566164,
                920.3615867728903,864.8423440829504,812.4101529369906;
                1131.7853114477825,982.2997349771053,922.5501467039322,
                866.9226383595472,814.3898023810821], displayUnit="degC")),
          neutronicKinetics(D(start={2.6297433004241203E+17,
                6.425892720202542E+17,1.7257554821707536E+17,
                1.6728770166583648E+17,13303534357977988.0,1894272960287383.5}),
            P(start=540e6, fixed=true))),
      flangeA(h_outflow(start=1067049.8813956545)),
      flangeB(h_outflow(start=2943650.415331751)),
      pressurizer(pressurizer(h(start=1862126.0574145121))),
      pump(h(start=1335324.00934794), q_single(start=0.8562867086127629)),
      SGoutlet(noInitialPressure=false),
      htc2(fixed=false, start=14826.950527435387),
      flowSplit(out2(m_flow(start=-10.710783489378356))))
      annotation (Placement(transformation(extent={{-184,-40},{-126,-4}})));
    AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro7(steam_outlet(h(start=
              2944000.0)), port_a(h_outflow(start=2962802.891927479)))
      annotation (Placement(transformation(extent={{-100,-22},{-84,-6}})));
    AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro8 annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-90,-30})));
    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
      Placement(transformation(extent={{-160,80},{-140,100}})));
  equation
    connect(HeatNwk_IP_In.y, Heat_IP_Network_Out.m_flow_in)
      annotation (Line(points={{42,103.6},{44.1,103.6},{44.1,100}},
          color={0,0,127}));
    connect(rampLP.y, HeatNwk_LP_In.m_flow_in) annotation (Line(points
          ={{171.6,-8},{172,-9.2},{162,-9.2}}, color={0,0,127}));
    connect(HeatNwk_HP_In.y, Heat_HP_Network_Out.m_flow_in)
      annotation (Line(points={{-36,103.6},{-35.9,104},{-35.9,100}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1], fluid2TSPro4.port_a)
      annotation (Line(
        points={{-39.5,91},{-39.5,89.86},{-33,89.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1], fluid2TSPro5.port_b) annotation (
        Line(points={{-15,94},{-14,94},{-14,90},{-19,90},{-19,88}},
          color={0,127,255}));
    connect(Heat_IP_Network_Out.ports[1], fluid2TSPro1.port_a)
      annotation (Line(
        points={{40.5,91},{39,91},{39,87.86}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1], fluid2TSPro2.port_b) annotation (
        Line(points={{59,94},{60,94},{60,88},{55,88}}, color={0,127,
            255}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro6.port_a) annotation (
        Line(points={{153,-6},{147.86,-6},{147.86,-9}}, color={0,127,
            255}));
    connect(HeatNwk_LP_Out.ports[1], fluid2TSPro3.port_b) annotation (
        Line(points={{156,-25},{150,-25}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u, Set_Flow_TapSteam_LP.y)
      annotation (Line(points={{162.8,-40},{171.6,-40}}, color={0,0,
            127}));
    connect(Set_Flow_TapSteam_HP.y, adaptorRealModelicaTSP.u)
      annotation (Line(points={{10,103.6},{10,92.8}}, color={0,0,127}));
    connect(Set_Flow_TapSteam_IP.y, adaptorRealModelicaTSP1.u)
      annotation (Line(points={{86,103.6},{86,92.8}}, color={0,0,127}));
    connect(adaptorRealModelicaTSP3.u, Set_P_TapSteam_IP.y)
      annotation (Line(points={{150.8,48},{170,48},{170,46},{173.6,46}},
          color={0,0,127}));
    connect(adaptorRealModelicaTSP4.u, Set_P_TapSteam_LP.y)
      annotation (Line(points={{150.8,30},{168,30},{168,32},{171.6,32}},
          color={0,0,127}));
    connect(fluid2TSPro4.steam_outlet, HX_cog_HP.Water_Cooling_In)
      annotation (Line(points={{-33.0018,76.035},{-33.2,76.035},{-33.2,61.8}}, color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
      annotation (Line(points={{-33.2,48.2},{-30,48.2},{-30,30},{-27.8148,30},{-27.8148,20}}, color={0,0,255}));
    connect(HX_cog_HP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
      annotation (Line(points={{-30,48.2},{-28,48.2},{-28,32},{-23.4074,32},{-23.4074,20}}, color={255,0,0}));
    connect(HX_cog_HP.Water_Cooling_Out, fluid2TSPro5.steam_inlet)
      annotation (Line(points={{-30.2,61.8},{-32,61.8},{-32,70},{-19,70},{-19,74}}, color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow, adaptorRealModelicaTSP.outputReal)
      annotation (Line(points={{-26.2,61.6},{-26.2,68},{10,68},{10,83.6}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Set, adaptorRealModelicaTSP3.outputReal)
      annotation (Line(points={{127.6,55},{138,55},{138,48},{141.6,48}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Set, adaptorRealModelicaTSP4.outputReal)
      annotation (Line(points={{127.6,29},{136,29},{136,30},{141.6,30}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In)
      annotation (Line(points={{108.4,58.75},{53.8519,58.75},{53.8519,20}}, color={0,0,255}));
    connect(CTR_P_Tap_IP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
      annotation (Line(points={{107.8,47.75},{60.0741,47.75},{60.0741,19.4815}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In)
      annotation (Line(points={{108.4,32.75},{73.2963,32.75},{73.2963,19.7407}}, color={0,0,255}));
    connect(CTR_P_Tap_LP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
      annotation (Line(points={{107.8,21.75},{104,21.75},{104,24},{78.7407,24},{78.7407,19.2222}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
      annotation (Line(points={{38.5556,-49.7407},{38.5556,-52},{38,-52},{38,-58},{39,-58},{39,-59.5}},
                                                                                      color={0,0,255}));
    connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
      annotation (Line(points={{9.375,25.88},{9.25926,25.88},{9.25926,20}}, color={0,0,255}));
    connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outb)
      annotation (Line(points={{1.675,26.24},{2,26.24},{2,20},{4.33333,20}}, color={0,0,255}));
    connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
      annotation (Line(points={{-51.9,-45.75},{-44,-45.75},{-44,-43},{-39.4815,-43}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.T_SG_Out, CTR_PT_PpHP.Sensor)
      annotation (Line(points={{-40.2593,-38.5926},{-54,-38.5926},{-54,-39.15},{-52.2,-39.15}}, color={0,0,255}));
    connect(Set_Pout_SG.y, CTR_PT_PpHP.Set) annotation (Line(points={{-67.6,-42},{-64,-42},{-64,-41.4},{-61.8,-41.4}}, color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening, Set_Ouv_Vv_SG_In.y)
      annotation (Line(points={{-19.5185,-49.7407},{-19,-49.7407},{-19,-61.5}},color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out, HX_cog_LP.fluidInletI)
      annotation (Line(points={{100,-22.5185},{100,-22.8},{114.2,-22.8}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, HX_cog_LP.fluidOutletI1)
      annotation (Line(points={{100,-26.4074},{100,-26},{114.2,-26}}, color={0,0,255}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro6.steam_outlet)
      annotation (Line(points={{127.8,-22.8},{130,-22.8},{130,-9.00175},{134.035,-9.00175}}, color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI, fluid2TSPro3.steam_inlet) annotation (Line(points={{127.8,-25.8},{128,-25},{136,-25}}, color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP, adaptorRealModelicaTSP2.outputReal)
      annotation (Line(points={{127.6,-29.8},{132,-29.8},{132,-40},{153.6,-40}}, color={0,0,255}));
    connect(fluid2TSPro1.steam_outlet, HX_cog_IP.Water_Cooling_In)
      annotation (Line(points={{39.0018,74.035},{39.0018,66},{28.8,66},{28.8,61.8}}, color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet, HX_cog_IP.Water_Cooling_Out) annotation (Line(points={{55,74},{55,64},{31.8,64},{31.8,61.8}}, color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow, adaptorRealModelicaTSP1.outputReal)
      annotation (Line(points={{35.8,61.6},{86,61.6},{86,83.6}}, color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
      annotation (Line(points={{28.8,48.2},{28.8,24},{20.4074,24},{20.4074,20.2593}}, color={0,0,255}));
    connect(HX_cog_IP.Condensate_Tapping_Out, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
      annotation (Line(points={{32,48.2},{32,20.2593},{24.5556,20.2593}}, color={255,0,0}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{19.6296,-50},{20,-50},{20,-56},{66.8,-56},{66.8,-58.2}}, color={255,0,0}));
    connect(HX_HeatInput.Turb_IP_In, StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In)
      annotation (Line(points={{70,-58.2},{72,-58.2},{72,-56},{106,-56},{106,9.37037},{100,9.37037}}, color={255,0,0}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine, Set_Liquid_Tapping_line_Flowrate.y)
      annotation (Line(points={{73.8,-71.6},{73.8,-78},{81,-78},{81,-83.5}}, color={0,0,255}));
    connect(Set_P_In_TurbHP.y, CTR_P_Turb_HP.Set) annotation (Line(points={{4,47.6},{4.3,48},{4.3,37.76}}, color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine, ThermalPower_InputToRankine.y)
      annotation (Line(points={{68.4,-71.6},{68.4,-80},{61,-80},{61,-83.5}}, color={0,0,255}));
    connect(NSSSctrl.actuatorBus,nsss. actuatorBus) annotation (Line(
        points={{-166.4,22},{-166.4,2},{-172.4,2},{-172.4,-4.36}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus,nsss. sensorBus) annotation (Line(
        points={{-143.6,22},{-143.6,2},{-141.467,2},{-141.467,-4.36}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.flangeB, fluid2TSPro7.port_a) annotation (Line(points={{-126,
            -11.2},{-106,-11.2},{-106,-14},{-99.84,-14}}, color={0,0,255}));
    connect(nsss.flangeA, fluid2TSPro8.port_b) annotation (Line(points={{-126,
            -33.16},{-122,-33.16},{-122,-34},{-114,-34},{-114,-30},{-98,-30}},
          color={0,0,255}));
    connect(StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out, fluid2TSPro7.steam_outlet)
      annotation (Line(points={{-40.2593,-14.2222},{-86,-14.2222},{-86,-14.002},{-84.04,-14.002}},
                               color={0,0,255}));
    connect(fluid2TSPro8.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
      annotation (Line(points={{-82,-30},{-68,-30},{-68,-21.7407},{-40.2593,-21.7407}},
                        color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-200,
              -100},{200,120}})),
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-200,-100},
              {200,120}})));
  end StaticBoP3_T_Plugged_N3Sbypass_TMPower_Tail;

  package WorkingDirectory
    model StaticBOP_Plugged_TSProAndMSL_BC
      "Static BOP demo, plugged to TSPro&MSL Boundary Conditions, respectively for SG (secondary side) and HeatNetwork"
      BOP_2Plug.WorkingDirectory.StaticBOP_PlugSGTSP_PlugCogSML StaticBOP_PlugSGTSP_PlugCogSML(
        Bache_a(h(start=487332.0296812735)),
        Bache_b(h(start=487332.9610530506)),
        CsBP1a(start=24083.992175129988),
        CsBP1b(start=1692.3189652681808),
        CsBP2(start=647.2741629776367),
        CsHP(start=687735.9288020331),
        Extraction_TurbBP1a_Outlet(Cex(Q(start=0.0))),
        HeatSink(proe(d(start=990.7810255201107))),
        Pe_SG(C1(h_vol(start=690757.7165042452))),
        Pump_BP(
          C2(h(start=164184.75305878202), h_vol(start=164184.75305878313)),
          Qv(start=0.18344895868379218),
          h(start=163775.1341310154)),
        Pump_HP(
          C2(h(start=492431.020761233), h_vol(start=492431.020761233)),
          Pm(start=2787393.218219025),
          Qv(start=0.2529319780567345),
          h(start=489881.9909071418)),
        ReHeat_HP(
          HDesF(start=690757.7165042452),
          HeiF(start=493420.13551916863),
          Se(h_vol(start=690757.7165042452)),
          promeF(d(start=928.6767861731782))),
        Reheat_BP(
          HDesF(start=370916.7084424497),
          HeiF(start=170840.9661031777),
          promeF(d(start=981.490836901295))),
        SPurgeBP(start=182.47701461201012),
        SPurgeHP(start=31.441056077587675),
        ScondesBP(start=1194.212269048435),
        ScondesHP(start=1581.8736802819424),
        SuperHeat(DPfc(start=39275.26460512825)),
        Te_SG(C1(h_vol(start=690757.7165042452))),
        Turb_BP2(
          Ps(start=7055.7307659071),
          pros(d(start=0.05475351720156039)),
          xm(start=0.9365574436163608)),
        Valve_HP(Q(start=218.56552299472145)),
        sensorP1(C1(h_vol(start=370916.7084424497))),
        singularPressureLoss6(Pm(start=694743.2182200279)),
        volumeC1(h(start=163365.51520324874)),
        volumeC2(h(start=2142123.310722838))) annotation (Placement(transformation(extent={{-66,-54},{88,50}})));

      ThermoSysPro.WaterSteam.BoundaryConditions.SourceQ SG_Secondary_Out(Q0=
            239.642, h0=2.94411e6) annotation (Placement(transformation(
              extent={{-104,20},{-84,40}})));
    ThermoSysPro.WaterSteam.BoundaryConditions.SinkP SG_Secondary_In(
        P0=4900000,
        h0=694000,
        mode=0) annotation (Placement(visible=true, transformation(
            origin={-92,4},
            extent={{-10,-10},{10,10}},
            rotation=180)));
      Modelica.Fluid.Sources.MassFlowSource_h Heat_Network_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=true,
        use_h_in=false,
        m_flow=100,
        h=135000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={39,70})));
      Modelica.Blocks.Sources.Ramp ramp(
        height=29,
        duration=900,
        offset=0.01,
        startTime=100) annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=270,
            origin={34,92})));
      Modelica.Fluid.Sources.Boundary_ph Heat_Network_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=1500000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=270,
            origin={81,70})));
    equation
      connect(SG_Secondary_Out.C, StaticBOP_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
        annotation (Line(points={{-84,30},{-74,30},{-74,21.2471},{-65.384,21.2471}},
                         color={0,0,255}));
      connect(SG_Secondary_In.C, StaticBOP_PlugSGTSP_PlugCogSML.SG_Secondary_In)
        annotation (Line(points={{-82,4},{-74,4},{-74,12.6824},{-65.384,12.6824}},
                         color={0,0,255}));
      connect(Heat_Network_Out.ports[1], StaticBOP_PlugSGTSP_PlugCogSML.Heat_NetWork_Out)
        annotation (Line(points={{39,63},{40,63},{40,60},{56,60},{56,50},{56.584,
              50},{56.584,49.3882}},         color={0,127,255}));
      connect(ramp.y, Heat_Network_Out.m_flow_in) annotation (Line(points={{34,87.6},
              {43.8,87.6},{43.8,77}}, color={0,0,127}));
      connect(Heat_Network_In.ports[1], StaticBOP_PlugSGTSP_PlugCogSML.Heat_NetWork_In)
        annotation (Line(points={{81,65},{81,60.5},{66.44,60.5},{66.44,49.3882}},
                         color={0,127,255}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
          Diagram(coordinateSystem(preserveAspectRatio=false)));
    end StaticBOP_Plugged_TSProAndMSL_BC;

    model StaticBOP_Plugged_MSL_BC "Static BOP demo, plugged to MSL Boundary Conditions for SG (secondary side) and HeatNetwork"
      Modelica.Fluid.Sources.Boundary_ph Heat_Network_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=1500000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=270,
            origin={77,66})));
      Modelica.Fluid.Sources.MassFlowSource_h Heat_Network_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=true,
        use_h_in=false,
        m_flow=100,
        h=135000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={43,66})));
      Modelica.Blocks.Sources.Ramp ramp(
        height=29,
        duration=900,
        offset=0.01,
        startTime=100) annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=270,
            origin={48,88})));
      Modelica.Fluid.Sources.MassFlowSource_h SG_Secondary_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=false,
        use_h_in=false,
        m_flow=239.642,
        h=2.94411e6,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={-91,20})));
      Modelica.Fluid.Sources.Boundary_ph SG_Secondary_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=4900000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=90,
            origin={-91,-22})));
      BOP_2Plug.WorkingDirectory.StaticBOP_PlugSGMSL_PlugCogSML StaticBOP_PlugSGMSL_PlugCogSML(
        Bache_a(Ce3(Q(start=21.076476996289127)), h(start=487332.0254840841)),
        Bache_b(h(start=487332.95685587695)),
        CsBP1a(start=24083.992164119794),
        CsBP1b(start=1692.3189644945173),
        CsBP2(start=647.274162652389),
        CsHP(start=687735.9296062593),
        Extraction_TurbBP1a_Outlet(Cex(Q(start=8.526512829121202E-14))),
        HeatSink(proe(d(start=990.7810255233652))),
        Pe_SG(C1(h_vol(start=690757.7130269211))),
        Pump_BP(C2(h(start=164184.75305852963), h_vol(start=164184.75305852963)), Qv(start=0.18344895901144723)),
        Pump_HP(
          C2(h(start=492431.01656085387), h_vol(start=492431.01656085387)),
          Pm(start=2787393.218157292),
          Qv(start=0.25293197833742753)),
        ReHeat_HP(
          HDesF(start=690757.7130269211),
          HeiF(start=493420.13131620403),
          Se(h_vol(start=690757.7130269211)),
          promeF(d(start=928.6767869690904))),
        Reheat_BP(HDesF(start=370916.7066875473), HeiF(start=170840.96608047135)),
        SG_Secondary_In(h_outflow(start=690757.7130269211)),
        SG_Secondary_Out(h_outflow(start=2944110.0)),
        SPurgeBP(start=182.47701457606448),
        SPurgeHP(start=31.44105545499017),
        ScondesBP(start=1194.2122299506973),
        ScondesHP(start=1581.8735921365774),
        SuperHeat(DPfc(start=39275.26457145513)),
        Te_SG(C1(h_vol(start=690757.7130269211))),
        Turb_BP2(
          Ps(start=7055.73076582384),
          pros(d(start=0.05475351719790078)),
          xm(start=0.9365574436684199)),
        Valve_HP(Q(start=218.5655230037108)),
        sensorP1(C1(h_vol(start=370916.7066875473))),
        volumeC1(h(start=163365.51520324877)),
        volumeC2(h(start=2142123.3107644753))) annotation (Placement(transformation(extent={{-66,-64},{86,40}})));

    equation
      connect(ramp.y, Heat_Network_Out.m_flow_in) annotation (Line(points={{48,83.6},
              {48,76},{47.8,76},{47.8,73}}, color={0,0,127}));
      connect(Heat_Network_In.ports[1], StaticBOP_PlugSGMSL_PlugCogSML.Heat_NetWork_In)
        annotation (Line(points={{77,61},{77,39.3882},{65.328,39.3882}},
            color={0,127,255}));
      connect(SG_Secondary_In.ports[1], StaticBOP_PlugSGMSL_PlugCogSML.SG_Secondary_In)
        annotation (Line(points={{-91,-17},{-91,-6.49412},{-66,-6.49412}},
            color={0,127,255}));
      connect(SG_Secondary_Out.ports[1], StaticBOP_PlugSGMSL_PlugCogSML.SG_Secondary_Out)
        annotation (Line(points={{-91,13},{-91,3.90588},{-66,3.90588}},
            color={0,127,255}));
      connect(Heat_Network_Out.ports[1], StaticBOP_PlugSGMSL_PlugCogSML.Heat_NetWork_Out)
        annotation (Line(points={{43,59},{43,39.3882},{54.384,39.3882}},
            color={0,127,255}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
          Diagram(coordinateSystem(preserveAspectRatio=false)));
    end StaticBOP_Plugged_MSL_BC;

    model StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML
      "Static BOP2 demo, plugged to TSPro OD heat source & MSL Boundary Conditions, respectively mimicking SG and HeatNetwork"
      BOP_2Plug.WorkingDirectory.StaticBOP2_PlugSGTSP_PlugCogSML StaticBOP2_PlugSGTSP_PlugCogSML(
        Bache_a(h(start=487332.0296812735)),
        Bache_b(h(start=487332.9610530506)),
        CsBP1a(start=24083.992175129988),
        CsBP1b(start=1692.3189652681808),
        CsBP2(start=647.2741629776367),
        CsHP(start=687735.9288020331),
        Extraction_TurbBP1a_Outlet(Cex(Q(start=0.0))),
        HeatSink(proe(d(start=990.7810255201107))),
        Pe_SG(C1(h_vol(start=690757.7165042452))),
        Pump_BP(
          C2(h(start=164184.75305878202), h_vol(start=164184.75305878313)),
          Qv(start=0.18344895868379218),
          h(start=163775.1341310154)),
        Pump_HP(
          C2(h(start=492431.020761233), h_vol(start=492431.020761233)),
          Pm(start=2787393.218219025),
          Qv(start=0.2529319780567345),
          h(start=489881.9909071418)),
        ReHeat_HP(
          HDesF(start=690757.7165042452),
          HeiF(start=493420.13551916863),
          Se(h_vol(start=690757.7165042452)),
          promeF(d(start=928.6767861731782))),
        Reheat_BP(
          HDesF(start=370916.7084424497),
          HeiF(start=170840.9661031777),
          promeF(d(start=981.490836901295))),
        SPurgeBP(start=182.47701461201012),
        SPurgeHP(start=31.441056077587675),
        ScondesBP(start=1194.212269048435),
        ScondesHP(start=1581.8736802819424),
        SuperHeat(DPfc(start=39275.26460512825)),
        Te_SG(C1(h_vol(start=690757.7165042452))),
        Turb_BP2(
          Ps(start=7055.7307659071),
          pros(d(start=0.05475351720156039)),
          xm(start=0.9365574436163608)),
        Valve_HP(Q(start=218.56552299472145)),
        sensorP1(C1(h_vol(start=370916.7084424497))),
        singularPressureLoss6(Pm(start=694743.2182200279)),
        volumeC1(h(start=163365.51520324874)),
        volumeC2(h(start=2142123.310722838))) annotation (Placement(transformation(extent={{-66,-54},{88,50}})));
      Modelica.Fluid.Sources.MassFlowSource_h Heat_Network_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=true,
        use_h_in=false,
        m_flow=100,
        h=135000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={39,70})));
      Modelica.Blocks.Sources.Ramp ramp(
        height=29,
        duration=900,
        offset=0.01,
        startTime=100) annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=270,
            origin={34,92})));
      Modelica.Fluid.Sources.Boundary_ph Heat_Network_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=1500000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=270,
            origin={81,70})));
      Added_Component.OD_HeatSource OD_HeatSource(
        Ce(h(start=690705.965184411), h_vol(start=690705.9651844109)),
        Q(start=240.02949730772582, fixed=false),
        Te(start=318.64),
        Ts(start=583.15, fixed=false),
        deltaP=400000,
        eta=100,
        mode=0,
        Cs(
          h_vol(start=2944121.464781758),
          h(start=2944106.3643035735),
          P(start=4502408.835922457))) annotation (Placement(visible=true,
            transformation(
            origin={-85,15},
            extent={{-11,-11},{11,11}},
            rotation=90)));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampe(
        Starttime=20,
        Duration=1000,
        Initialvalue=540E6,
        Finalvalue=1*540E6)                                                                                                                     annotation (
        Placement(visible = true, transformation(origin={-104,12},   extent={{-4,-4},
                {4,4}},                                                                               rotation=0)));
    equation
      connect(Heat_Network_Out.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_Out)
        annotation (Line(points={{39,63},{40,63},{40,60},{56,60},{56,50},{56.584,
              50},{56.584,49.3882}},
                                 color={0,127,255}));
      connect(ramp.y, Heat_Network_Out.m_flow_in) annotation (Line(points={{34,87.6},
              {43.8,87.6},{43.8,77}}, color={0,0,127}));
      connect(Heat_Network_In.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_In)
        annotation (Line(points={{81,65},{81,60.5},{66.44,60.5},{66.44,49.3882}},
            color={0,127,255}));
      connect(OD_HeatSource.Signal_Elec, rampe.y) annotation (Line(points={{-94.68,15},
              {-98,15},{-98,12},{-99.6,12}}, color={0,0,255}));
      connect(OD_HeatSource.Cs, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
        annotation (Line(points={{-85.88,24.57},{-65.384,24.57},{-65.384,21.2471}},
            color={0,0,255}));
      connect(OD_HeatSource.Ce, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
        annotation (Line(points={{-85.88,5.43},{-65.384,5.43},{-65.384,12.6824}},
            color={0,0,255}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
          Diagram(coordinateSystem(preserveAspectRatio=false)));
    end StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML;

    model StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML_LoadFollow_Fast
      "Static BOP2 demo, plugged to TSPro OD heat source & MSL Boundary Conditions, respectively mimicking SG and HeatNetwork"
      BOP_2Plug.WorkingDirectory.StaticBOP2_PlugSGTSP_PlugCogSML StaticBOP2_PlugSGTSP_PlugCogSML(
        rampe7(
          Starttime=10,
          Duration=90,
          Initialvalue=0.001,
          Finalvalue=28),
        rampe1(
          Starttime=10,
          Duration=90,
          Initialvalue=7.147E5,
          Finalvalue=5.9E5),
        Bache_a(h(start=487332.0296812735)),
        Bache_b(h(start=487332.9610530506)),
        CsBP1a(start=24083.992175129988),
        CsBP1b(start=1692.3189652681808),
        CsBP2(start=647.2741629776367),
        CsHP(start=687735.9288020331),
        Extraction_TurbBP1a_Outlet(Cex(Q(start=0.0))),
        HeatSink(proe(d(start=990.7810255201107))),
        Pe_SG(C1(h_vol(start=690757.7165042452))),
        Pump_BP(
          C2(h(start=164184.75305878202), h_vol(start=164184.75305878313)),
          Qv(start=0.18344895868379218),
          h(start=163775.1341310154)),
        Pump_HP(
          C2(h(start=492431.020761233), h_vol(start=492431.020761233)),
          Pm(start=2787393.218219025),
          Qv(start=0.2529319780567345),
          h(start=489881.9909071418)),
        ReHeat_HP(
          HDesF(start=690757.7165042452),
          HeiF(start=493420.13551916863),
          Se(h_vol(start=690757.7165042452)),
          promeF(d(start=928.6767861731782))),
        Reheat_BP(
          HDesF(start=370916.7084424497),
          HeiF(start=170840.9661031777),
          promeF(d(start=981.490836901295))),
        SPurgeBP(start=182.47701461201012),
        SPurgeHP(start=31.441056077587675),
        ScondesBP(start=1194.212269048435),
        ScondesHP(start=1581.8736802819424),
        SuperHeat(DPfc(start=39275.26460512825)),
        Te_SG(C1(h_vol(start=690757.7165042452))),
        Turb_BP2(
          Ps(start=7055.7307659071),
          pros(d(start=0.05475351720156039)),
          xm(start=0.9365574436163608)),
        Valve_HP(Q(start=218.56552299472145)),
        sensorP1(C1(h_vol(start=370916.7084424497))),
        singularPressureLoss6(Pm(start=694743.2182200279)),
        volumeC1(h(start=163365.51520324874)),
        volumeC2(h(start=2142123.310722838))) annotation (Placement(transformation(extent={{-66,-54},{88,50}})));
      Modelica.Fluid.Sources.MassFlowSource_h Heat_Network_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=true,
        use_h_in=false,
        m_flow=100,
        h=135000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={39,70})));
      Modelica.Blocks.Sources.Ramp ramp(
        height=29,
        duration=90,
        offset=0.01,
        startTime=10)  annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=270,
            origin={34,92})));
      Modelica.Fluid.Sources.Boundary_ph Heat_Network_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=1500000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=270,
            origin={81,70})));
      Added_Component.OD_HeatSource OD_HeatSource(
        Ce(h(start=690705.965184411), h_vol(start=690705.9651844109)),
        Q(start=240.02949730772582, fixed=false),
        Te(start=318.64),
        Ts(start=583.15, fixed=false),
        deltaP=400000,
        eta=100,
        mode=0,
        Cs(
          h_vol(start=2944121.464781758),
          h(start=2944106.3643035735),
          P(start=4502408.835922457))) annotation (Placement(visible=true,
            transformation(
            origin={-85,15},
            extent={{-11,-11},{11,11}},
            rotation=90)));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampe(
        Starttime=110,
        Duration=90,
        Initialvalue=540E6,
        Finalvalue=0.9*540E6)                                                                                                                   annotation (
        Placement(visible = true, transformation(origin={-104,12},   extent={{-4,-4},
                {4,4}},                                                                               rotation=0)));
    equation
      connect(Heat_Network_Out.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_Out)
        annotation (Line(points={{39,63},{40,63},{40,60},{56,60},{56,50},{56.584,
              50},{56.584,49.3882}},
                                 color={0,127,255}));
      connect(ramp.y, Heat_Network_Out.m_flow_in) annotation (Line(points={{34,87.6},
              {43.8,87.6},{43.8,77}}, color={0,0,127}));
      connect(Heat_Network_In.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_In)
        annotation (Line(points={{81,65},{81,60.5},{66.44,60.5},{66.44,49.3882}},
            color={0,127,255}));
      connect(OD_HeatSource.Signal_Elec, rampe.y) annotation (Line(points={{-94.68,15},
              {-98,15},{-98,12},{-99.6,12}}, color={0,0,255}));
      connect(OD_HeatSource.Cs, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
        annotation (Line(points={{-85.88,24.57},{-65.384,24.57},{-65.384,21.2471}},
            color={0,0,255}));
      connect(OD_HeatSource.Ce, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
        annotation (Line(points={{-85.88,5.43},{-65.384,5.43},{-65.384,12.6824}},
            color={0,0,255}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
          Diagram(coordinateSystem(preserveAspectRatio=false)),
        Documentation(info="<html>
<p>Scenario: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\"><tr>
<td><p>10s steady state</p></td>
<td><p>steady state: SG = full-load ; e-Grid : full power</p></td>
</tr>
<tr>
<td><p>90s ramp</p></td>
<td><p>cogeneration ramp</p></td>
</tr>
<tr>
<td><p>10s steady state</p></td>
<td><p>steady state: SG = full load ; cogeneration: ~10&percnt; of SG power; e-grid: balance </p></td>
</tr>
<tr>
<td><p>90s ramp</p></td>
<td><p>electrical load following</p></td>
</tr>
<tr>
<td><p>xx steady state</p></td>
<td><p>steady state: SG = 90&percnt; of full load ; cogeneration: &gt;10&percnt; of SG power; e-grid: balance</p></td>
</tr>
</table>
</html>"));
    end StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML_LoadFollow_Fast;

    model StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML_LoadFollow_Slow
      "Static BOP2 demo, plugged to TSPro OD heat source & MSL Boundary Conditions, respectively mimicking SG and HeatNetwork"
      BOP_2Plug.WorkingDirectory.StaticBOP2_PlugSGTSP_PlugCogSML StaticBOP2_PlugSGTSP_PlugCogSML(
        rampe7(
          Starttime=1000,
          Duration=100,
          Initialvalue=0.001,
          Finalvalue=28),
        rampe1(
          Starttime=1000,
          Duration=100,
          Initialvalue=7.147E5,
          Finalvalue=5.9E5),
        Bache_a(h(start=487332.0296812735)),
        Bache_b(h(start=487332.9610530506)),
        CsBP1a(start=24083.992175129988),
        CsBP1b(start=1692.3189652681808),
        CsBP2(start=647.2741629776367),
        CsHP(start=687735.9288020331),
        Extraction_TurbBP1a_Outlet(Cex(Q(start=0.0))),
        HeatSink(proe(d(start=990.7810255201107))),
        Pe_SG(C1(h_vol(start=690757.7165042452))),
        Pump_BP(
          C2(h(start=164184.75305878202), h_vol(start=164184.75305878313)),
          Qv(start=0.18344895868379218),
          h(start=163775.1341310154)),
        Pump_HP(
          C2(h(start=492431.020761233), h_vol(start=492431.020761233)),
          Pm(start=2787393.218219025),
          Qv(start=0.2529319780567345),
          h(start=489881.9909071418)),
        ReHeat_HP(
          HDesF(start=690757.7165042452),
          HeiF(start=493420.13551916863),
          Se(h_vol(start=690757.7165042452)),
          promeF(d(start=928.6767861731782))),
        Reheat_BP(
          HDesF(start=370916.7084424497),
          HeiF(start=170840.9661031777),
          promeF(d(start=981.490836901295))),
        SPurgeBP(start=182.47701461201012),
        SPurgeHP(start=31.441056077587675),
        ScondesBP(start=1194.212269048435),
        ScondesHP(start=1581.8736802819424),
        SuperHeat(DPfc(start=39275.26460512825)),
        Te_SG(C1(h_vol(start=690757.7165042452))),
        Turb_BP2(
          Ps(start=7055.7307659071),
          pros(d(start=0.05475351720156039)),
          xm(start=0.9365574436163608)),
        Valve_HP(Q(start=218.56552299472145)),
        sensorP1(C1(h_vol(start=370916.7084424497))),
        singularPressureLoss6(Pm(start=694743.2182200279)),
        volumeC1(h(start=163365.51520324874)),
        volumeC2(h(start=2142123.310722838))) annotation (Placement(transformation(extent={{-66,-54},{88,50}})));
      Modelica.Fluid.Sources.MassFlowSource_h Heat_Network_Out(
        redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
        use_m_flow_in=true,
        use_h_in=false,
        m_flow=100,
        h=135000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-7,-6},{7,6}},
            rotation=270,
            origin={39,70})));
      Modelica.Blocks.Sources.Ramp ramp(
        height=29,
        duration=90,
        offset=0.01,
        startTime=10)  annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=270,
            origin={34,92})));
      Modelica.Fluid.Sources.Boundary_ph Heat_Network_In(
        redeclare package Medium = Modelica.Media.Water.StandardWater,
        use_p_in=false,
        p=1500000,
        nPorts=1) annotation (Placement(transformation(
            extent={{-5,-6},{5,6}},
            rotation=270,
            origin={81,70})));
      Added_Component.OD_HeatSource OD_HeatSource(
        Ce(h(start=690705.965184411), h_vol(start=690705.9651844109)),
        Q(start=240.02949730772582, fixed=false),
        Te(start=318.64),
        Ts(start=583.15, fixed=false),
        deltaP=400000,
        eta=100,
        mode=0,
        Cs(
          h_vol(start=2944121.464781758),
          h(start=2944106.3643035735),
          P(start=4502408.835922457))) annotation (Placement(visible=true,
            transformation(
            origin={-85,15},
            extent={{-11,-11},{11,11}},
            rotation=90)));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe rampe(
        Starttime=1300,
        Duration=100,
        Initialvalue=540E6,
        Finalvalue=0.9*540E6)                                                                                                                   annotation (
        Placement(visible = true, transformation(origin={-104,12},   extent={{-4,-4},
                {4,4}},                                                                               rotation=0)));
    equation
      connect(Heat_Network_Out.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_Out)
        annotation (Line(points={{39,63},{40,63},{40,60},{56,60},{56,50},{56.584,
              50},{56.584,49.3882}},
                                 color={0,127,255}));
      connect(ramp.y, Heat_Network_Out.m_flow_in) annotation (Line(points={{34,87.6},
              {43.8,87.6},{43.8,77}}, color={0,0,127}));
      connect(Heat_Network_In.ports[1], StaticBOP2_PlugSGTSP_PlugCogSML.Heat_NetWork_In)
        annotation (Line(points={{81,65},{81,60.5},{66.44,60.5},{66.44,49.3882}},
            color={0,127,255}));
      connect(OD_HeatSource.Signal_Elec, rampe.y) annotation (Line(points={{-94.68,15},
              {-98,15},{-98,12},{-99.6,12}}, color={0,0,255}));
      connect(OD_HeatSource.Cs, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
        annotation (Line(points={{-85.88,24.57},{-65.384,24.57},{-65.384,21.2471}},
            color={0,0,255}));
      connect(OD_HeatSource.Ce, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
        annotation (Line(points={{-85.88,5.43},{-65.384,5.43},{-65.384,12.6824}},
            color={0,0,255}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
          Diagram(coordinateSystem(preserveAspectRatio=false)),
        Documentation(info="<html>
<p>Scenario: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\"><tr>
<td><p>1000s steady state (needed by Cathare to reach steady state)</p></td>
<td><p>steady state: SG = full-load ; e-Grid : full power</p></td>
</tr>
<tr>
<td><p>100s ramp</p></td>
<td><p>cogeneration ramp</p></td>
</tr>
<tr>
<td><p>200s steady state</p></td>
<td><p>steady state: SG = full load ; cogeneration: ~10&percnt; of SG power; e-grid: balance </p></td>
</tr>
<tr>
<td><p>100s ramp</p></td>
<td><p>electrical load following</p></td>
</tr>
<tr>
<td><p>xx steady state</p></td>
<td><p>steady state: SG = 90&percnt; of full load ; cogeneration: &gt;10&percnt; of SG power; e-grid: balance</p></td>
</tr>
</table>
</html>"));
    end StaticBOP2_Plugged_ODHeatSourceTSP_BCCogSML_LoadFollow_Slow;

  end WorkingDirectory;
  annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023 - Mai 2024</p>
</html>"));
end Demo_BOP_Plugged;
