within TANDEM.SMR.BOP.BOP_TSPro;
package FMU_Coupling "Coupling with FMU from fluid ports"

  package Adaptor4FMU "AdaptorForFMU"

    model AdaptorRealModelicaTSP "AdaptorModelicaTSP for Real"

      Modelica.Blocks.Interfaces.RealInput u
        annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
            iconTransformation(extent={{-140,-20},{-100,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal outputReal
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));
    equation
      outputReal.signal = u;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,100}}), graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={0,0,127},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              smooth=Smooth.None,
              fillColor={0,255,255},
              fillPattern=FillPattern.Solid)}), Diagram(graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              smooth=Smooth.None,
              fillColor={0,255,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              smooth=Smooth.None,
              fillColor={0,0,175},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorRealModelicaTSP;

    model AdaptorRealTSPModelica "AdaptorTSPModelica for Real"

      Modelica.Blocks.Interfaces.RealOutput y
        annotation (Placement(transformation(extent={{100,-20},{140,20}}),
            iconTransformation(extent={{100,-20},{140,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.InputReal inputReal
        annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
    equation
      inputReal.signal =y;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
                {100,100}}), graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              smooth=Smooth.None,
              fillColor={0,0,175},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={0,127,255},
              fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
              preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                                                        graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={0,127,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              smooth=Smooth.None,
              fillColor={0,0,175},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorRealTSPModelica;

    model AdaptorLogicalModelicaTSP "AdaptorModelicaTSP for Boolean"

      Modelica.Blocks.Interfaces.BooleanInput
                                           u
        annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
            iconTransformation(extent={{-140,-20},{-100,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.OutputLogical
                                                                   outputLogical
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));
    equation
      outputLogical.signal = u;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,100}}), graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={255,0,255},
              fillColor={255,0,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={127,255,0},
              fillPattern=FillPattern.Solid)}), Diagram(graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={128,255,0},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={217,67,180},
              fillColor={255,0,255},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorLogicalModelicaTSP;

    model AdaptorLogicalTSPModelica "AdaptorTSPModelica for Boolean"

      Modelica.Blocks.Interfaces.BooleanOutput
                                            y
        annotation (Placement(transformation(extent={{100,-20},{140,20}}),
            iconTransformation(extent={{100,-20},{140,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.InputLogical
                                                                  inputLogical
        annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
    equation
      inputLogical.signal = y;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
                {100,100}}), graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={217,67,180},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={255,255,0},
              fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
              preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                                                        graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={255,255,0},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={217,67,180},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorLogicalTSPModelica;

    model AdaptorIntegerModelicaTSP "AdaptorModelicaTSP for Integer"

      Modelica.Blocks.Interfaces.IntegerInput
                                           u
        annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
            iconTransformation(extent={{-140,-20},{-100,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.OutputInteger
                                                                   outputInteger
        annotation (Placement(transformation(extent={{100,-10},{120,10}})));
    equation
      outputInteger.signal = u;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,100}}), graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={255,127,0},
              fillColor={250,125,0},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={255,0,128},
              fillPattern=FillPattern.Solid)}), Diagram(graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={0,0,255},
              fillColor={255,0,128},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={244,125,35},
              fillColor={244,125,35},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorIntegerModelicaTSP;

    model AdaptorIntegerTSPModelica "AdaptorTSPModelica for Integer"

      Modelica.Blocks.Interfaces.IntegerOutput
                                            y
        annotation (Placement(transformation(extent={{100,-20},{140,20}}),
            iconTransformation(extent={{100,-20},{140,20}})));

      ThermoSysPro.InstrumentationAndControl.Connectors.InputInteger
                                                                  inputInteger
        annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
    equation
      inputInteger.signal = y;
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
                {100,100}}), graphics={Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={244,125,35},
              fillColor={244,125,35},
              fillPattern=FillPattern.Solid), Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={255,0,255},
              fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
              preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                                                        graphics={Polygon(
              points={{-100,100},{0,0},{-100,-100},{-100,100}},
              lineColor={0,0,255},
              fillColor={255,0,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{0,100},{100,0},{0,-100},{0,100}},
              lineColor={244,125,35},
              fillColor={244,125,35},
              fillPattern=FillPattern.Solid)}),
        Documentation(info="<html>
<p><b>Version 3.2</b></p>
</html>
"));
    end AdaptorIntegerTSPModelica;
  annotation (                                                              Icon(
        graphics={
          Text(
            extent={{-102,0},{24,-26}},
            lineColor={242,148,0},
            textString=
                 "Thermo"),
          Text(
            extent={{-4,8},{68,-34}},
            lineColor={46,170,220},
            textString=
                 "SysPro"),
          Polygon(
            points={{-62,2},{-58,4},{-48,8},{-32,12},{-16,14},{6,14},{26,12},{42,
                8},{52,2},{42,6},{28,10},{6,12},{-12,12},{-16,12},{-34,10},{-50,6},
                {-62,2}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-44,38},{-24,38},{-26,30},{-26,22},{-24,14},{-24,12},{-46,8},
                {-42,22},{-42,30},{-44,38}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-26,20},{-20,20},{-20,22},{-14,22},{-14,20},{-12,20},{-12,12},
                {-26,12},{-28,12},{-26,20}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-8,14},{-8,24},{-6,24},{-6,14},{-8,14}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-8,30},{-6,26}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-8,36},{-6,32}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-8,42},{-6,38}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-8,48},{-6,44}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-4,14},{-4,26},{-2,26},{-2,14},{-4,14}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-4,32},{-2,28}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-4,38},{-2,34}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-4,44},{-2,40}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-4,50},{-2,46}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-2,20},{8,20},{8,22},{10,22},{18,22},{18,12},{-4,14},{-2,20}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Polygon(
            points={{-62,2},{-58,4},{-48,8},{-36,10},{-18,12},{6,12},{26,10},{42,
                6},{52,0},{42,4},{28,8},{6,10},{-12,10},{-18,10},{-38,8},{-50,6},
                {-62,2}},
            lineColor={242,148,0},
            fillColor={242,148,0},
            fillPattern=FillPattern.Solid),
          Line(
            points={{22,12},{22,14},{22,16},{24,14},{20,18}},
            color={46,170,220},
            thickness=0.5),
          Line(
            points={{26,12},{26,14},{26,16},{28,14},{24,18}},
            color={46,170,220},
            thickness=0.5),
          Line(
            points={{30,10},{30,12},{30,14},{32,12},{28,16}},
            color={46,170,220},
            thickness=0.5),
          Polygon(
            points={{36,8},{36,30},{34,34},{36,38},{40,38},{40,8},{36,8}},
            lineColor={46,170,220},
            fillColor={46,170,220},
            fillPattern=FillPattern.Solid),
          Rectangle(extent={{-100,80},{80,-100}}, lineColor={0,0,255}),
          Line(
            points={{-100,80},{-80,100},{100,100},{100,-80},{80,-100}},
            color={0,0,255},
            smooth=Smooth.None),
          Line(
            points={{80,80},{100,100}},
            color={0,0,255},
            smooth=Smooth.None)}), Documentation(info="<html>
<p>Credit EDF, TSPro 3.2</p>
</html>"));
  end Adaptor4FMU;

  package Adaptator4FMU_Flow
    "Modified flow source and pressure sink to allow FMU creation with Fluid port"
    model SourceQ_4FMU
      "Water/steam source with set mass flow rate and enthalpy"
      parameter Modelica.Units.SI.MassFlowRate Q0=100
        "Mass flow (active if IMassFlow connector is not connected)";
      parameter Modelica.Units.SI.SpecificEnthalpy h0=100000
        "Fluid specific enthalpy (active if IEnthalpy connector is not connected)";

    public
      Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
      Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
      Modelica.Units.SI.SpecificEnthalpy h "Fluid specific enthalpy";

    public
      ThermoSysPro.InstrumentationAndControl.Connectors.InputReal IMassFlow
        annotation (Placement(transformation(
            origin={0,50},
            extent={{-10,-10},{10,10}},
            rotation=270)));
      ThermoSysPro.InstrumentationAndControl.Connectors.InputReal ISpecificEnthalpy
        annotation (Placement(transformation(
            origin={0,-50},
            extent={{10,-10},{-10,10}},
            rotation=270)));
      ThermoSysPro.WaterSteam.Connectors.FluidOutlet C annotation (Placement(
            transformation(extent={{90,-10},{110,10}}, rotation=0)));
      ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal OPressure
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=180,
            origin={-50,2})));
    equation

      C.P = P;
      C.Q = Q;
      C.h_vol = h;
      OPressure.signal = P;

      /* Mass flow */
      if (cardinality(IMassFlow) == 0) then
        IMassFlow.signal = Q0;
      end if;

      Q = IMassFlow.signal;

      /* Specific enthalpy */
      if (cardinality(ISpecificEnthalpy) == 0) then
        ISpecificEnthalpy.signal = h0;
      end if;

      h = ISpecificEnthalpy.signal;

      annotation (
        Diagram(coordinateSystem(
            preserveAspectRatio=false,
            extent={{-100,-100},{100,100}},
            grid={2,2}), graphics={
            Line(points={{40,0},{90,0},{72,10}}),
            Line(points={{90,0},{72,-10}}),
            Text(extent={{-28,60},{-10,40}}, textString=
                                                 "Q"),
            Text(extent={{-30,-40},{-12,-60}}, textString=
                                                 "h"),
            Rectangle(
              extent={{-40,40},{40,-40}},
              lineColor={0,0,255},
              fillColor={255,255,0},
              fillPattern=FillPattern.Solid),
            Text(
              extent={{-20,22},{18,-20}},
              lineColor={0,0,255},
              textString="Q, H"),
            Text(extent={{-78,14},{-60,-6}},
              textString="P",
              lineColor={0,0,0})}),
        Icon(coordinateSystem(
            preserveAspectRatio=false,
            extent={{-100,-100},{100,100}},
            grid={2,2}), graphics={
            Line(points={{40,0},{90,0},{72,10}}),
            Rectangle(
              extent={{-40,40},{40,-40}},
              lineColor={0,0,255},
              fillColor={255,255,0},
              fillPattern=FillPattern.Solid),
            Line(points={{90,0},{72,-10}}),
            Text(extent={{-30,60},{-10,40}}, textString=
                                                 "Q"),
            Text(extent={{-32,-40},{-12,-60}}, textString=
                                                 "h"),
            Text(
              extent={{-20,22},{18,-20}},
              lineColor={0,0,255},
              textString=
                   "Q")}),
        Window(
          x=0.23,
          y=0.15,
          width=0.81,
          height=0.71),
        Documentation(info="<html>
</html>",
       revisions="<html>
<p><u><b>Authors</b></u></p>
<ul>
<li>Baligh El Hefni</li>
<li>Daniel Bouskela </li>
</ul>
</html>"));
    end SourceQ_4FMU;

    model SinkP_4FMU "Water/steam sink with fixed pressure"
      parameter Modelica.Units.SI.AbsolutePressure P0=100000
        "Sink pressure";
      parameter Modelica.Units.SI.Temperature T0=290
        "Sink temperature (active if option_temperature=1)";
      parameter Modelica.Units.SI.SpecificEnthalpy h0=100000
        "Sink specific enthalpy (active if option_temperature=2)";
      parameter Integer option_temperature=1
        "1:temperature fixed - 2:specific enthalpy fixed";
      parameter Integer mode=1
        "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";

    public
      Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
      Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
      Modelica.Units.SI.Temperature T "Fluid temperature";
      Modelica.Units.SI.SpecificEnthalpy h "Fluid enthalpy";
      ThermoSysPro.Properties.WaterSteam.Common.ThermoProperties_ph pro
        "Propriétés de l'eau"
        annotation (Placement(transformation(extent={{-100,80},{-80,100}}, rotation=
               0)));
    public
      ThermoSysPro.InstrumentationAndControl.Connectors.InputReal IPressure
        annotation (Placement(transformation(
            origin={50,0},
            extent={{-10,-10},{10,10}},
            rotation=180)));
      ThermoSysPro.InstrumentationAndControl.Connectors.InputReal ISpecificEnthalpy
        annotation (Placement(transformation(
            origin={0,-50},
            extent={{10,-10},{-10,10}},
            rotation=270)));
      ThermoSysPro.WaterSteam.Connectors.FluidInlet C annotation (Placement(
            transformation(extent={{-110,-10},{-90,10}}, rotation=0)));
      ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal OSpecificEnthalpy
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={-22,50})));
      ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal OFlowRate
        annotation (Placement(transformation(
            extent={{-10,-10},{10,10}},
            rotation=90,
            origin={20,50})));
    equation

      C.P = P;
      C.Q = Q;
      C.h_vol = h;

      OSpecificEnthalpy.signal = C.h;

      OFlowRate.signal = C.Q;

      if (cardinality(IPressure) == 0) then
        IPressure.signal = P0;
      end if;

      P = IPressure.signal;

    //  if (cardinality(ITemperature) == 0) then
    //      ITemperature.signal = T0;
    //  end if;

      if (cardinality(ISpecificEnthalpy) == 0) then
          ISpecificEnthalpy.signal = h0;
      end if;

    //  if (option_temperature == 1) then
    //    T = ITemperature.signal;
    //    h = ThermoSysPro.Properties.WaterSteam.IF97.SpecificEnthalpy_PT(P, T, 0);
    //  elseif (option_temperature == 2) then
        h = ISpecificEnthalpy.signal;
        T = pro.T;
     // else
     //   assert(false, "SinkPressureWaterSteam: incorrect option");
    //  end if;

      pro = ThermoSysPro.Properties.WaterSteam.IF97.Water_Ph(P, h, mode);

      connect(OSpecificEnthalpy, OSpecificEnthalpy) annotation (Line(points={{-22,50},
              {-20,50},{-20,50},{-22,50}}, color={0,0,255}));
      annotation (
        Diagram(coordinateSystem(
            preserveAspectRatio=false,
            extent={{-100,-100},{100,100}},
            grid={2,2}), graphics={
            Line(points={{-90,0},{-40,0},{-58,10}}),
            Line(points={{-40,0},{-58,-10}}),
            Text(extent={{40,28},{58,8}}, textString=
                                              "P"),
            Text(extent={{-40,-40},{-10,-60}},
              textString="hvol",
              lineColor={0,0,0}),
            Rectangle(
              extent={{-40,40},{40,-40}},
              lineColor={0,0,255},
              fillColor={127,255,0},
              fillPattern=FillPattern.Solid),
            Text(extent={{-96,26},{96,-30}}, textString=
                                                 "P"),
            Text(extent={{-56,62},{-26,42}},
              lineColor={0,0,0},
              textString="h"),
            Text(extent={{-12,62},{18,42}},
              lineColor={0,0,0},
              textString="Q")}),
        Window(
          x=0.06,
          y=0.16,
          width=0.67,
          height=0.71),
        Icon(coordinateSystem(
            preserveAspectRatio=false,
            extent={{-100,-100},{100,100}},
            grid={2,2}), graphics={
            Line(points={{-90,0},{-40,0},{-58,10}}),
            Rectangle(
              extent={{-40,40},{40,-40}},
              lineColor={0,0,255},
              fillColor={127,255,0},
              fillPattern=FillPattern.Solid),
            Line(points={{-40,0},{-58,-10}}),
            Text(extent={{-94,26},{98,-30}}, textString=
                                                 "P"),
            Text(extent={{40,28},{58,8}}, textString=
                                              "P"),
            Text(extent={{-40,-40},{-10,-60}}, textString=
                                                 "h / T")}),
        Documentation(info="<html>
</html>",
       revisions="<html>
<p><u><b>Authors</b></u></p>
<ul>
<li>Daniel Bouskela</li>
<li>Baligh El Hefni </li>
</ul>
</html>"));
    end SinkP_4FMU;
    annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"));
  end Adaptator4FMU_Flow;

  package WorkingDirectory

    package FMI_2generate "To generate FMI"
      model FMI_Boiler "Boiler with modified Fluid connection allowing to build Fluid FMU"

        //Variables de calibration

        Added_Component.OD_HeatSource                       e_Boiler(
          Ce(h(start=690704.9343977855), h_vol(start=690704.9343977853)),
          Q(start=240.02949730772582, fixed=false),
          Te(start=436.15),
          Ts(start=573.15, fixed=false),
          deltaP=400000,
          eta=100,
          mode=0,
          Cs(h_vol(start=2944106.3643035735), h(start=2944106.3643035735),
            P(start=4502408.835922457)))
          annotation (Placement(visible=true, transformation(
              origin={-53,23},
              extent={{-11,-11},{11,11}},
              rotation=0)));
      ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe InputPower_Ramp(
          Starttime=1300,
          Duration=100,
          Initialvalue=540E6,
          Finalvalue=0.9*540E6) annotation (Placement(visible=true,
              transformation(
              origin={-56,64},
              extent={{-4,-4},{4,4}},
              rotation=270)));
       //   DPff(start=79999.999999991),

        //  SCondDes(fixed=true, start=1438.0),
        //  SPurge(fixed=true, start=1238.0),
        //  Sp(Q(fixed=false, start=13.3), h(fixed=false, start=147.55E3)),

        //  Cv(start=119, fixed=true),
        //  Cv(start=15, fixed=false),
       //   Q(fixed=false, start=20),
        //  Q(fixed=true, start=20.57),
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Qin_set annotation (Placement(transformation(extent={{-104,38},
                  {-96,44}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Hin_set annotation (Placement(transformation(extent={{-104,10},
                  {-96,16}})));
        FMU_Coupling.Adaptator4FMU_Flow.SourceQ_4FMU
          sourceQ_4FMU annotation (Placement(transformation(extent={{-98,
                  20},{-78,40}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Pin_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=180,
              origin={-102,30})));
        Modelica.Blocks.Interfaces.RealInput Hin_BC(start=691000) annotation (
            Placement(transformation(extent={{-128,4},{-110,22}}),
              iconTransformation(extent={{-40,-50},{-22,-32}})));
        Modelica.Blocks.Interfaces.RealInput Qin_BC(start=240)
          "Valve openinn (Real)" annotation (Placement(transformation(extent={{-130,32},
                  {-112,50}}),    iconTransformation(extent={{-90,-56},{-72,-38}})));
        Modelica.Blocks.Interfaces.RealOutput Pin_TSPro annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-145,29}),iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-29,-7})));
        FMU_Coupling.Adaptator4FMU_Flow.SinkP_4FMU
          sinkP_4FMU(h0=2.944e6, mode=0) annotation (Placement(
              transformation(extent={{-26,20},{-6,40}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Qout_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=90,
              origin={-8,42})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Hout_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=90,
              origin={-22,44})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Pout_calc annotation (Placement(transformation(
              extent={{-4,-3},{4,3}},
              rotation=180,
              origin={0,29})));
        Modelica.Blocks.Interfaces.RealInput Pout_BC(start=45e5)   annotation (
            Placement(transformation(
              extent={{-9,-9},{9,9}},
              rotation=180,
              origin={17,29}),  iconTransformation(extent={{-40,-50},{-22,-32}})));
        Modelica.Blocks.Interfaces.RealOutput Qout_TSPro annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=90,
              origin={-9,61}), iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-39,-5})));
        Modelica.Blocks.Interfaces.RealOutput Hout_TSPro annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=90,
              origin={-23,59}),iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-39,-5})));
      equation

        connect(Qin_BC,Qin_set. u) annotation (Line(points={{-121,41},{-104.8,41}},
                                     color={0,0,127}));
        connect(Hin_set.u,Hin_BC)  annotation (Line(points={{-104.8,13},{-119,13}},
                            color={0,0,127}));
        connect(Qin_set.outputReal, sourceQ_4FMU.IMassFlow) annotation (
            Line(points={{-95.6,41},{-88,41},{-88,35}}, color={0,0,255}));
        connect(Hin_set.outputReal, sourceQ_4FMU.ISpecificEnthalpy)
          annotation (Line(points={{-95.6,13},{-88,13},{-88,25}}, color={
                0,0,255}));
        connect(Pin_TSPro,Pin_TSPro)
          annotation (Line(points={{-145,29},{-145,29}},
                                                       color={0,0,127}));
        connect(Pin_TSPro,Pin_calc. y) annotation (Line(points={{-145,29},
                {-132,29},{-132,30},{-106.8,30}}, color={0,0,127}));
        connect(Qout_calc.inputReal, sinkP_4FMU.OFlowRate) annotation (
            Line(points={{-8,37.6},{-8,35},{-14,35}}, color={0,0,255}));
        connect(Hout_calc.inputReal, sinkP_4FMU.OSpecificEnthalpy)
          annotation (Line(points={{-22,39.6},{-22,35},{-18.2,35}}, color=
               {0,0,255}));
        connect(sinkP_4FMU.IPressure, Pout_calc.outputReal) annotation (
            Line(points={{-11,30},{-4.4,30},{-4.4,29}}, color={0,0,255}));
        connect(Pout_calc.u,Pout_BC)  annotation (Line(points={{4.8,29},{17,29}},
                                    color={0,0,127}));
        connect(Hout_TSPro,Hout_TSPro)
          annotation (Line(points={{-23,59},{-23,59}},
                                                     color={0,0,127}));
        connect(Hout_TSPro,Hout_calc. y) annotation (Line(points={{-23,59},{-23,53.5},
                {-22,53.5},{-22,48.8}},     color={0,0,127}));
        connect(Qout_TSPro,Qout_calc. y) annotation (Line(points={{-9,61},{-9,54.5},{-8,
                54.5},{-8,46.8}},           color={0,0,127}));
        connect(InputPower_Ramp.y, e_Boiler.Signal_Elec) annotation (Line(
              points={{-56,59.6},{-56,32.68},{-53,32.68}}, color={0,0,255}));
        connect(sourceQ_4FMU.C, e_Boiler.Ce) annotation (Line(points={{-78,
                30},{-72,30},{-72,23.88},{-62.57,23.88}}, color={0,0,255}));
        connect(e_Boiler.Cs, sinkP_4FMU.C) annotation (Line(points={{-43.43,
                23.88},{-43.43,30},{-26,30}}, color={0,0,255}));
        connect(Pin_calc.inputReal, sourceQ_4FMU.OPressure) annotation (
            Line(points={{-97.6,30},{-96,30},{-96,30.2},{-93,30.2}},
              color={0,0,255}));
        annotation (Diagram(coordinateSystem(extent={{-160,-20},{60,100}})),   Icon(
              coordinateSystem(extent={{-160,-20},{60,100}})),
          experiment(Tolerance=1e-06, __Dymola_Algorithm="Dassl"));
      end FMI_Boiler;

      model FMI_BOP "BoP with modified Fluid connection allowing to build BOP with Fluid FMU"

        parameter Real Tmax_synop = 1000;

        Real Eff_Cycle_net;
        Real Eff_Cycle_brut;
        Real Pw_eGrid;
        Real Pwth_COG;
        Real T_COG;
        Real Heat_Power_ratio;
        Real Tin_SG;
        Real Nuclear_Pw_load;
        Real Pwth_SG;

        //Variables de calibration
        parameter Real CsHP( fixed=false, start=685708.1898310621);
                                                          // stodola
        parameter Real CsBP1a( fixed=false, start=24004.035334331);
                                                           // stodola
        parameter Real CsBP1b( fixed=false, start=1686.7006077673466);
                                                          // stodola
        parameter Real CsBP2( fixed=false, start=644.9127937152352);
                                                        // stodola

        parameter Real ScondesHP( fixed=false, start=1586.6591598184393);
                                                           //surface réchauffeur
        parameter Real SPurgeHP( fixed=false, start=31.466846045765656);
                                                         //surface réchauffeur
        parameter Real ScondesBP( fixed=false, start=1195.1810801870483);
                                                        //surface réchauffeur
        parameter Real SPurgeBP( fixed=false, start=182.47794479770502);
                                                          //surface réchauffeur
        parameter Real SpHP_Q(fixed=true)= 24.51;
                                                          //débit réchauffeur
        parameter Real SpHP_H(fixed=true) = 700.8e3;
                                                          //enthalpie réchauffeur
        parameter Real SpBP_Q(fixed=true) = 16.41;
                                                          //débit réchauffeur
        parameter Real SpBP_H(fixed=true) = 317.89e3;
                                                          //enthalpie réchauffeur

      //  parameter Real CvmaxSurch(fixed=false, start=207);
                                                           //Cvmax Surchauffe

        parameter Real CvmaxSurch=200;
                                                           //Cvmax Surchauffe

        parameter Real kfric_Surch(fixed=false, start=8);  // coef de friction

        parameter Real kfric_SortieGV(fixed=false, start=13.9);  // coef de friction

      ThermoSysPro.WaterSteam.PressureLosses.ControlValve Valve_HP(
          continuous_flow_reversal=false,
          Cvmax=125000,
          Pm(fixed=false, start=4045476.4188144),
          C1(h_vol(start=2944110.0)),
          Q(fixed=false, start=218.8884495827999),
          T(fixed=false, start=558.15),
          fluid=1,
          h(fixed=false, start=2985800.4338921653),
          mode=0,
          option_rho_water=1,
          p_rho(displayUnit="kg/m3") = 0,
          rho(
            fixed=false,
            start=15.63,
            displayUnit="kg/m3")) annotation (Placement(visible=true,
              transformation(
              origin={10,91},
              extent={{-4,-5},{4,5}},
              rotation=0)));
      ThermoSysPro.WaterSteam.Machines.StaticCentrifugalPump Pump_HP(
          VRot=1400.0,
          VRotn=1400,
          rm=0.88,
          fixed_rot_or_power=1,
          adiabatic_compression=false,
          Pm(fixed=false, start=2787448.563511),
          Q(start=239, fixed=false),
          Qv(fixed=false, start=0.25294928050897675),
          a1=-2551,
          a3=594,
          b1=-16.5,
          b2=7.65,
          h(fixed=false, start=490056.0454434941),
          hn(fixed=false, start=454.3),
          C1(P(start=554266.93940273)),
          C2(h_vol(start=492605.1139489484), P(start=4902408.737046973),
            h(start=492605.11394894833)))    annotation (Placement(visible=true,
              transformation(
              origin={-208,8},
              extent={{-6,6},{6,-6}},
              rotation=90)));
      ThermoSysPro.WaterSteam.BoundaryConditions.SourceQ sourceQ1(Q0 = 4000, h0=
              137770)                                                                     annotation (
          Placement(visible = true, transformation(origin={30,-126}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP1 annotation (
          Placement(visible = true, transformation(origin={90,-126}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoSysPro.WaterSteam.HeatExchangers.SimpleDynamicCondenser BOP_HeatSink(
          A=100,
          Ccond=1,
          D=0.018,
          P0=7000,
          V=1000,
          Vf0=0.5,
          mode=0,
          ntubes=28700,
          steady_state=false,
          proe(d(start=990.7730249550118)),
          Cl(h(start=93158.8975545407), Q(start=176.88363316813704)),
          Pfond(start=7000),
          Cv(Q(start=175.84668052425536), h(start=2319048.8977629677)))
          annotation (Placement(visible=true, transformation(
              origin={61,-124},
              extent={{-9,-10},{9,10}},
              rotation=0)));
        ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                                  Turb_HP(
          Cst=CsHP,
          W_fric=1,
          eta_is_nom=0.90,
          mode_e=0,
          eta_is(start=0.90),
          Pe(start=4400000, fixed=true),
          Ps(start=754701.12073853),
          rhos(start=86, displayUnit="kg/m3"),
          pros(d(start=12.181371168968184)),
          Cs(h(start=2639678.799598451), h_vol(start=2634009.304269357)))
          annotation (Placement(transformation(
              extent={{-5,-7},{5,7}},
              rotation=270,
              origin={55,119})));
        ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                                  Turb_LP1_a(
          Cst=CsBP1a,
          W_fric=1,
          eta_is_nom=0.89,
          mode_e=0,
          eta_is(start=0.89),
          Pe(start=674700, fixed=true),
          Ps(start=5000),
          rhos(start=86, displayUnit="kg/m3"),
          pros(d(start=0.3188866622217379)),
          xm(start=0.9475591259592065),
          Ce(h_vol(start=2977121.8265122357)),
          Cs(h(start=2724624.7568167527), h_vol(start=2724624.756816755)))
          annotation (Placement(transformation(
              extent={{-7,-9},{7,9}},
              rotation=270,
              origin={55,11})));
        ThermoSysPro.WaterSteam.Sensors.SensorT Tout_SG(C2(h_vol(start=
                  2944110.0))) annotation (Placement(transformation(
                extent={{-170,86},{-154,102}})));
        ThermoSysPro.WaterSteam.Sensors.SensorP Pe_ValveHP(C1(h_vol(start=2944110.0)))
          annotation (Placement(transformation(extent={{-32,86},{-16,102}})));
      ThermoSysPro.WaterSteam.Machines.StaticCentrifugalPump Pump_BP(
          VRot=1400,
          VRotn=1400,
          rm=0.88,
          fixed_rot_or_power=1,
          adiabatic_compression=false,
          Pm(fixed=false, start=360907.39267732),
          Q(start=181.649685154607,   fixed=false),
          Qv(fixed=false, start=0.1834019039955073),
          a1=-477.7,
          a3=72.8,
          b1=-25.2,
          b2=9.47,
          h(fixed=false, start=163775.22431113676),
          hn(fixed=false, start=59.1),
          C1(P(start=6999.996697349103)),
          C2(h_vol(start=164184.93341902483), h(start=164184.9334190248)))
                                               annotation (Placement(visible=true,
              transformation(
              origin={-52,-114},
              extent={{-6,6},{6,-6}},
              rotation=90)));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss3(C2(h_vol(start=2639678.799598451),  P(start=755921.1331388752)))
                                annotation (Placement(transformation(
              extent={{-5,-6},{5,6}},
              rotation=270,
              origin={42,81})));
        ThermoSysPro.WaterSteam.Junctions.SteamExtractionSplitter Extraction_ReheatHP(
          mode_e=0,
          x_ex(start=1),
          Cex(Q(start=9.179560851779371)),
          Cs(h(start=2639676.091421155))) annotation (Placement(transformation(
              extent={{-6.5,-3.5},{6.5,3.5}},
              rotation=270,
              origin={40.5,62.5})));
        ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe
          lumpedStraightPipe(L=1,
          D=0.4,
          lambda_fixed=false,C2(h_vol(start=2639678.799598451),  P(start=
                  754564.1414536542)))                            annotation (
            Placement(transformation(
              extent={{-9,-7},{9,7}},
              rotation=180,
              origin={13,55})));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss2(
          K=1E-6,
          Q(start=22.3, fixed=false),
          Pm(start=754564.1413820141),
          C2(h_vol(start=700800.0000000022)))
                               annotation (Placement(transformation(
              extent={{-6,-6},{6,6}},
              rotation=180,
              origin={-14,54})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante PressureSet_TurbHP_In(k=44.5E5)
          annotation (Placement(transformation(
              extent={{-4,-5},{4,5}},
              rotation=90,
              origin={-8,111})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback feedback1
          annotation (Placement(transformation(
              extent={{-7,-7},{7,7}},
              rotation=0,
              origin={-9,127})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI pI1(permanent=
              true) annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=270,
              origin={10,118})));
        ThermoSysPro.WaterSteam.Machines.Generator Generator(eta=98)
          annotation (Placement(transformation(extent={{92,0},{122,30}})));
        ThermoSysPro.WaterSteam.Sensors.SensorP sensorP1(C1(h_vol(start=
                  370969.92329333595)))
          annotation (Placement(transformation(extent={{-10,-10},{10,10}},
              rotation=180,
              origin={-104,-96})));
        ThermoSysPro.WaterSteam.Junctions.SteamDryer Dryer(
          Csv(h(start=2765900.246437356)), Cev(h(start=2639678.799598451)),
          h(start=2765900.246437356))       annotation (Placement(transformation(
              extent={{-4.5,-6.5},{4.5,6.5}},
              rotation=270,
              origin={37.5,40.5})));
        ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss
          invSingularPressureLoss annotation (Placement(transformation(
              extent={{-6,-8},{6,8}},
              rotation=180,
              origin={2,32})));
        ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                                  Turb_LP2(
          Cst= CsBP2,
          W_fric=1,
          eta_is_nom=0.89,
          mode_e=0,
          eta_is(start=0.93),
          Pe(start=80100, fixed=true),
          Ps(start=7055.9242141804),
          rhos(displayUnit="kg/m3"),
          pros(d(start=0.054754887914703565)),
          xm(start=0.9365577998712189),
          Ce(h_vol(start=2612098.433622263)))  annotation (Placement(
              transformation(
              extent={{-8,-11},{8,11}},
              rotation=270,
              origin={56,-79})));
        ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe
          lumpedStraightPipe1(
          L=1,
          D=0.8,
          lambda_fixed=false,
          C2(h_vol(start=2612098.433622263),  P(start=80083.75057360485)),
          Q(start=13.785457418931628))                            annotation (
            Placement(transformation(
              extent={{-9,-7},{9,7}},
              rotation=180,
              origin={19,-69})));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss4(
          K=1E-6,
          Q(fixed=false, start=13.3),
          Pm(start=80083.75029940932),
          C2(h_vol(start=317889.9999999945)))
                               annotation (Placement(transformation(
              extent={{-6,-6},{6,6}},
              rotation=180,
              origin={0,-68})));
        ThermoSysPro.WaterSteam.Volumes.VolumeC volumeC1(
          P0=500000,
          h0=138000,
          dynamic_mass_balance=false,
          P(fixed=false),
          h(start=163365.51520324877))
          annotation (Placement(transformation(
              extent={{-8,-7},{8,7}},
              rotation=180,
              origin={-22,-129})));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss5 annotation (Placement(transformation(
              extent={{-6,-6.5},{6,6.5}},
              rotation=180,
              origin={30,-147.5})));
        ThermoSysPro.WaterSteam.Volumes.VolumeI Bache_a(h(start=
                487506.04632433486),
            Ce3(Q(start=21.020048599606543)))
          annotation (Placement(transformation(extent={{-174,-84},{-192,-62}})));
        ThermoSysPro.WaterSteam.HeatExchangers.SimpleStaticCondenser SuperHeat(
          Kc=3547,
          Kf = kfric_Surch,
          z1c=0,
          z2c=0,
          z1f=0,
          z2f=0,
          DPf(fixed=true, start=80000),
          Qc(fixed=false),
          Ec(h(start=2944110.0)),
          Sc(h_vol(start=1118785.4784898118)),
          Sf(h_vol(start=2977121.8265122357)),
          DPfc(start=39406.755951177),
          DPgc(start=0))                       annotation (Placement(
              transformation(
              extent={{-8,-7},{8,7}},
              rotation=270,
              origin={-47,8})));
       //   DPff(start=79999.999999991),
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss6(
          K=658,
          Q(start=38),
          Pm(start=694855.9578583837),
          C2(P(start=674700, fixed=false)))
                               annotation (Placement(transformation(
              extent={{-6,-6},{6,6}},
              rotation=90,
              origin={-202,-54})));
        ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                                  Turb_LP1_b(
          Cst=CsBP1b,
          W_fric=1,
          eta_is_nom=0.89,
          mode_e=0,
          eta_is(start=0.93),
          Pe(start=170000, fixed=true),
          Ps(start=5000),
          rhos(start=86, displayUnit="kg/m3"),
          pros(d(start=0.039408233894734135)),
          xm(start=0.9367375120787198),
          Cs(h(start=2612098.4336222624))) annotation (Placement(transformation(
              extent={{-8,-11},{8,11}},
              rotation=270,
              origin={56,-39})));
        ThermoSysPro.WaterSteam.Junctions.SteamExtractionSplitter Extraction_ReheatBP(
          mode_e=0,
          x_ex(start=1),
          P(start=600000),
          Cex(Q(start=9.179560851779371))) annotation (Placement(transformation(
              extent={{-6.5,-3.5},{6.5,3.5}},
              rotation=270,
              origin={38.5,-65.5})));

        ThermoSysPro.WaterSteam.Junctions.Splitter3 splitter_SGoutlet
          annotation (Placement(transformation(extent={{-86,78},{-72,96}})));
        ThermoSysPro.WaterSteam.Volumes.VolumeC Bache_b(
          P0=500000,
          h0=138000,
          dynamic_mass_balance=false,
          P(start=600000, fixed=false),
          h(start=487506.9769380398),
          Ce1(Q(start=0.09907550047151403))) annotation (Placement(transformation(
              extent={{-8,-7},{8,7}},
              rotation=180,
              origin={-202,-33})));
        ThermoSysPro.WaterSteam.Junctions.SteamExtractionSplitter Extraction_TurbBP1a_Outlet(
          mode_e=0,
          x_ex(start=1),
          P(start=600000),
          Cex(Q(start=-9.8731119139536E-33),
                             h(start=2724624.756816755))) annotation (Placement(
              transformation(
              extent={{-6.5,-3.5},{6.5,3.5}},
              rotation=270,
              origin={38.5,-15.5})));
        ThermoSysPro.WaterSteam.HeatExchangers.NTUWaterHeating Reheat_BP(
          SCondDes=ScondesBP,
          SPurge=SPurgeBP,
          Sp(Q(start=SpBP_Q, fixed=true), h(start=SpBP_H, fixed=true)),
          KCond=1500,
          KPurge=150,
          Ev(Q(start=13.970501574743839)),
          HDesF(start=370969.92329333595),
          SDes(start=1E-09),
          promeF(d(start=981.492569613173)),
          HeiF(start=170842.85406545497),
          Hep(start=391757.3376993904))
          annotation (Placement(transformation(extent={{-10,-100},{-32,-76}})));
        //  SCondDes(fixed=true, start=1438.0),
        //  SPurge(fixed=true, start=1238.0),
        //  Sp(Q(fixed=false, start=13.3), h(fixed=false, start=147.55E3)),
        ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss
          invSingularPressureLoss1 annotation (Placement(transformation(
              extent={{-4,-5},{4,5}},
              rotation=270,
              origin={-4,-113})));
        ThermoSysPro.WaterSteam.HeatExchangers.NTUWaterHeating ReHeat_HP(
          SCondDes=ScondesHP,
          SPurge=SPurgeHP,
          Sp(Q(start=SpHP_Q, fixed=true), h(start=SpHP_H, fixed=true)),
          KCond=1500,
          KPurge=150,
          Ev(Q(start=13.970501574743839)),
          HDesF(start=690925.0264666991),
          SDes(start=1E-09),
          promeF(d(start=928.6413914051141)),
          HeiF(start=493594.194870379),
          Hep(start=710469.7699779422))
          annotation (Placement(transformation(extent={{-28,34},{-50,58}})));
        ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss
          invSingularPressureLoss2 annotation (Placement(transformation(
              extent={{-4,-5},{4,5}},
              rotation=180,
              origin={-152,-65})));

        Modelica.Blocks.Interaction.Show.RealValue Rend_Cycle(
          use_numberPort=false,
          number=Eff_Cycle_net,
          significantDigits=4)
          annotation (Placement(transformation(extent={{-38,148},{2,178}})));

         Modelica.Blocks.Interaction.Show.RealValue Pw_reseau(
          use_numberPort=false,
          number=Pw_eGrid,
          significantDigits=4)
          annotation (Placement(transformation(extent={{-216,148},{-164,178}})));

         Modelica.Blocks.Interaction.Show.RealValue Pw_COG(
          use_numberPort=false,
          number=Pwth_COG,
          significantDigits=4)
          annotation (Placement(transformation(extent={{-160,148},{-110,178}})));

          Modelica.Blocks.Interaction.Show.RealValue Temp_COG(
          use_numberPort=false,
          number=T_COG,
          significantDigits=4)
          annotation (Placement(transformation(extent={{6,148},{34,178}})));

          Modelica.Blocks.Interaction.Show.RealValue Heat_Power(
          use_numberPort=false,
          number= Heat_Power_ratio,
          significantDigits=4)
          annotation (Placement(transformation(extent={{-106,148},{-42,178}})));

          Modelica.Blocks.Interaction.Show.RealValue Temp_inletGV(
          use_numberPort=false,
          number=Tin_SG,
          significantDigits=4)
          annotation (Placement(transformation(extent={{38,148},{62,178}})));

         Modelica.Blocks.Interaction.Show.RealValue Pw_GV(
          use_numberPort=false,
          number=Pwth_SG,
          significantDigits=4)
          annotation (Placement(transformation(extent={{-258,148},{-220,178}})));
        ThermoSysPro.WaterSteam.Volumes.VolumeC volumeC2(
          P0=5000,
          h0=138000,
          dynamic_mass_balance=false,
          steady_state=true,
          P(fixed=false),
          h(start=2142425.9091542354),
          rho(start=30))
          annotation (Placement(transformation(
              extent={{-8,-7},{8,7}},
              rotation=270,
              origin={32,-97})));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss
          singularPressureLoss1 annotation (Placement(transformation(
              extent={{-6,-6.5},{6,6.5}},
              rotation=0,
              origin={56,-105.5})));
      ThermoSysPro.WaterSteam.PressureLosses.ControlValve Valve_superheat(
          continuous_flow_reversal=false,
          Cvmax=CvmaxSurch,
          Pm(fixed=false, start=1500000),
          T(fixed=false, start=518.15),
          fluid=1,
          h(fixed=false, start=1084E3),
          mode=0,
          option_rho_water=1,
          p_rho(displayUnit="kg/m3") = 0,
          rho(
            fixed=false,
            start=1000,
            displayUnit="kg/m3"))  annotation (Placement(visible=true, transformation(
              origin={-52.5,-32.5},
              extent={{-5.5,-4.5},{5.5,4.5}},
              rotation=270)));
        //  Cv(start=119, fixed=true),
        //  Cv(start=15, fixed=false),
       //   Q(fixed=false, start=20),
        //  Q(fixed=true, start=20.57),
        ThermoSysPro.WaterSteam.Sensors.SensorT Tin_Turb_LP1a(Q(start=174))
          annotation (Placement(transformation(extent={{-4,2},{10,-10}})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback feedback5
          annotation (Placement(transformation(
              extent={{-6,-6},{6,6}},
              rotation=180,
              origin={-6,-38})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI pI5(
          k=1.2,
          Ti=1,
          permanent=true)
                    annotation (Placement(transformation(
              extent={{3.5,-4.5},{-3.5,4.5}},
              rotation=0,
              origin={-24.5,-38.5})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe
          rampe3(
          Starttime=20,
          Duration=1000,
          Initialvalue=260.44 + 273.15,
          Finalvalue=260.44 + 273.15)
                                annotation (Placement(transformation(
              extent={{-3.5,-3.5},{3.5,3.5}},
              rotation=270,
              origin={-6.5,-25.5})));
        ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Pipe_SGs(
          L=20,
          D=0.5,
          lambda_fixed=false,
          C1(P(start=4468560.778186667), h_vol(start=2944110.0)))
          annotation (Placement(transformation(
              extent={{-9,-7},{9,7}},
              rotation=0,
              origin={-105,87})));
        ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss singularPressureLoss_SG_Out(
          K=kfric_SortieGV,
          Pm(start=4450000.1220833),
          C1(P(start=4500000, fixed=true), h_vol(start=2944110.0)))
          annotation (Placement(transformation(
              extent={{-5,-6},{5,6}},
              rotation=0,
              origin={-126,87})));
        ThermoSysPro.WaterSteam.Sensors.SensorP Pout_SG annotation (
            Placement(transformation(extent={{-152,86},{-136,102}})));
        ThermoSysPro.WaterSteam.Junctions.SteamExtractionSplitter Extraction_TurbHP_Oulet(
          mode_e=0,
          x_ex(start=1),
          Cex(h(start=2639678.799598451)))
                                   annotation (Placement(transformation(
              extent={{-6.5,-3.5},{6.5,3.5}},
              rotation=270,
              origin={110.5,60.5})));
        ThermoSysPro.WaterSteam.HeatExchangers.SimpleStaticCondenser Ideal_BOP2HeatNetWork_HX(
          Kf=5885,
          DPc(start=0.0011978797265468),
          DPf(start=0.059666399287179),
          Qc(fixed=false, start=0.1),
          Sc(h_vol(start=710502.0457445232))) annotation (Placement(
              transformation(
              extent={{-10,-8},{10,8}},
              rotation=0,
              origin={146,48})));
      ThermoSysPro.WaterSteam.PressureLosses.ControlValve Valve_Hybrid_IP(
          continuous_flow_reversal=true,
          Cvmax=1000,
          Pm(fixed=false),
          Q(fixed=false),
          T(fixed=false),
          fluid=1,
          h(fixed=false),
          mode=0,
          option_rho_water=1,
          p_rho(displayUnit="kg/m3") = 0,
          rho(fixed=false, displayUnit="kg/m3"),
          C2(h_vol(start=710502.0457445232)),
          C1(h_vol(start=710502.0457445232))) annotation (Placement(visible=
                true, transformation(
              origin={183,-3},
              extent={{-5,5},{5,-5}},
              rotation=180)));
        ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback feedback6
          annotation (Placement(transformation(
              extent={{-5,-5},{5,5}},
              rotation=0,
              origin={169,17})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI pI6(permanent=
              true) annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=0,
              origin={184,16})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Vapor_MassFlowRamp_extracted4Cogeneration(
          Starttime=1000,
          Duration=100,
          Initialvalue=0.001,
          Finalvalue=28) annotation (Placement(transformation(
              extent={{-3.5,-3.5},{3.5,3.5}},
              rotation=90,
              origin={171.5,6.5})));
        ThermoSysPro.WaterSteam.Sensors.SensorQ Q_Extract_Hybrid_IP annotation (
           Placement(transformation(
              extent={{-7,7},{7,-7}},
              rotation=180,
              origin={151,-1})));
        ThermoSysPro.WaterSteam.Sensors.SensorT T_Outlet_BOP_HX_HeatNetWork(Q(start=
                174)) annotation (Placement(transformation(extent={{180,
                  42},{192,30}})));
        ThermoSysPro.WaterSteam.Sensors.SensorP Pout_Turb_HP(C1(h_vol(
                start=2639678.799598451))) annotation (Placement(
              transformation(extent={{92,86},{108,102}})));
        ThermoSysPro.WaterSteam.Sensors.SensorT Tout_Turb_HP annotation (
            Placement(transformation(extent={{54,72},{66,60}})));
      ThermoSysPro.WaterSteam.PressureLosses.ControlValve Valve_superheat1(
          continuous_flow_reversal=false,
          Cvmax=CvmaxSurch,
          Pm(fixed=false, start=1500000),
          T(fixed=false, start=518.15),
          fluid=1,
          h(fixed=false, start=1084E3),
          mode=0,
          option_rho_water=1,
          p_rho(displayUnit="kg/m3") = 0,
          rho(
            fixed=false,
            start=1000,
            displayUnit="kg/m3"))  annotation (Placement(visible=true, transformation(
              origin={-80.5,111.5},
              extent={{-5.5,-4.5},{5.5,4.5}},
              rotation=90)));
        ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe
          rampe2(
          Starttime=100,
          Duration=900,
          Initialvalue=0,
          Finalvalue=0)         annotation (Placement(transformation(
              extent={{-3.5,-3.5},{3.5,3.5}},
              rotation=0,
              origin={-96.5,112.5})));
        ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP
          annotation (Placement(transformation(extent={{-76,118},{-56,138}})));
      ThermoSysPro.WaterSteam.PressureLosses.ControlValve Valve_superheat2(
          continuous_flow_reversal=false,
          Cvmax=CvmaxSurch,
          Pm(fixed=false, start=1500000),
          T(fixed=false, start=518.15),
          fluid=1,
          h(fixed=false, start=1084E3),
          mode=0,
          option_rho_water=1,
          p_rho(displayUnit="kg/m3") = 0,
          rho(
            fixed=false,
            start=1000,
            displayUnit="kg/m3"))  annotation (Placement(visible=true, transformation(
              origin={19.5,-34.5},
              extent={{-5.5,-4.5},{5.5,4.5}},
              rotation=270)));
        ThermoSysPro.WaterSteam.BoundaryConditions.SinkP sinkP2 annotation (
            Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=270,
              origin={16,-54})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe
          rampe4(
          Starttime=100,
          Duration=900,
          Initialvalue=0,
          Finalvalue=0)         annotation (Placement(transformation(
              extent={{-3.5,-3.5},{3.5,3.5}},
              rotation=180,
              origin={31.5,-41.5})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Qin_set annotation (Placement(transformation(extent={{-222,130},
                  {-214,136}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Hin_set annotation (Placement(transformation(extent={{-222,102},
                  {-214,108}})));
        FMU_Coupling.Adaptator4FMU_Flow.SourceQ_4FMU
          sourceQ_4FMU annotation (Placement(transformation(extent={{-216,
                  112},{-196,132}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Pin_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=180,
              origin={-222,122})));
        Modelica.Blocks.Interfaces.RealInput Hin_BC(start=2.94411e6)
                                                                  annotation (
            Placement(transformation(extent={{-244,96},{-226,114}}),
              iconTransformation(extent={{-40,-50},{-22,-32}})));
        Modelica.Blocks.Interfaces.RealInput Qin_BC(start=240)
          "Valve openinn (Real)" annotation (Placement(transformation(extent={{-246,
                  124},{-228,142}}),
                                  iconTransformation(extent={{-90,-56},{-72,-38}})));
        Modelica.Blocks.Interfaces.RealOutput Pin_TSPro annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-237,123}),
                                iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-29,-7})));
        FMU_Coupling.Adaptator4FMU_Flow.SinkP_4FMU
          sinkP_4FMU(   h0=691000, mode=0) annotation (Placement(
              transformation(extent={{-316,36},{-296,56}})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Qout_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=90,
              origin={-298,58})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealTSPModelica
          Hout_calc annotation (Placement(transformation(
              extent={{-4,-4},{4,4}},
              rotation=90,
              origin={-312,60})));
        FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
          Pout_calc annotation (Placement(transformation(
              extent={{-4,-3},{4,3}},
              rotation=180,
              origin={-290,45})));
        Modelica.Blocks.Interfaces.RealInput Pout_BC(start=49e5)   annotation (
            Placement(transformation(
              extent={{-9,-9},{9,9}},
              rotation=180,
              origin={-273,45}),iconTransformation(extent={{-40,-50},{-22,-32}})));
        Modelica.Blocks.Interfaces.RealOutput Qout_TSPro annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=90,
              origin={-291,83}),
                               iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-39,-5})));
        Modelica.Blocks.Interfaces.RealOutput Hout_TSPro(start=
              690925.0264666991)                         annotation (Placement(
              transformation(
              extent={{-7,-7},{7,7}},
              rotation=90,
              origin={-317,81}),
                               iconTransformation(
              extent={{-7,-7},{7,7}},
              rotation=180,
              origin={-39,-5})));
        ThermoSysPro.WaterSteam.Sensors.SensorT T_2HeatNetWork(Q(start=
                174)) annotation (Placement(transformation(extent={{172,
                  56},{184,44}})));
        AdaptatorForMSL.Fluid.Fluid2TSPro
          fluid2TSPro1 annotation (Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=270,
              origin={130,70})));
        Modelica.Fluid.Sources.MassFlowSource_h InletFlow_FromHeatNetwork(
          redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
          use_m_flow_in=true,
          use_h_in=false,
          m_flow=100,
          h=135000,
          nPorts=1) annotation (Placement(transformation(
              extent={{-7,-6},{7,6}},
              rotation=270,
              origin={129,100})));
        Modelica.Blocks.Sources.Ramp InletFlowRate_FromHeatNetWork(
          height=29,
          duration=100,
          offset=0.01,
          startTime=1000)
                        annotation (Placement(transformation(
              extent={{-6,-6},{6,6}},
              rotation=270,
              origin={132,122})));
        Modelica.Fluid.Sources.Boundary_ph BackPressure_FromHeatNetWork(
          redeclare package Medium = Modelica.Media.Water.StandardWater,
          use_p_in=false,
          p=1500000,
          nPorts=1) annotation (Placement(transformation(
              extent={{-5,-6},{5,6}},
              rotation=270,
              origin={179,102})));
        AdaptatorForMSL.Fluid.TSPro2Fluid
          fluid2TSPro annotation (Placement(transformation(
              extent={{-10,-10},{10,10}},
              rotation=90,
              origin={180,70})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1DTemps
          Test_Timetable_Rotation_PumpHP(Table=[0,1428.0319; 1000,
              1428.0319; 1100,1441.9839; 1101,1442.0339; 1299,1442.103;
              1300,1442.103; 1398,1402.0459; 1400,1401.2559; 1500,
              1400.9297])  annotation (Placement(transformation(extent={{
                  -258,-62},{-238,-42}})));
        ThermoSysPro.InstrumentationAndControl.Blocks.Tables.Table1DTemps
          Test_Timetable_Rotation_PumpLP(Table=[0,1546.0057; 1000,
              1546.0051; 1100,1393.8423; 1101,1393.8666; 1299,1393.8936;
              1300,1393.8936; 1398,1368.1257; 1400,1367.624; 1500,
              1367.5062])  annotation (Placement(transformation(extent={{
                  -188,-146},{-168,-126}})));
      equation

       // Pwth_SG = e_Boiler.Signal_Elec.signal;
        Pwth_SG =Tout_SG.C1.Q*Tout_SG.C1.h - ReHeat_HP.Se.Q*ReHeat_HP.Se.h;
        Pw_eGrid = Generator.Welec - Pump_BP.Wh/0.99 - Pump_HP.Wh/0.99;
        Eff_Cycle_net = Pw_eGrid/Pwth_SG;
        Eff_Cycle_brut = Generator.Welec/(Generator.eta/100)/Pwth_SG;
        T_COG =T_Outlet_BOP_HX_HeatNetWork.T - 273.15;
        Pwth_COG =Ideal_BOP2HeatNetWork_HX.W;
        Heat_Power_ratio = Pwth_COG / Pw_eGrid;
        // Tin_SG = e_Boiler.Te - 273.1;
        Tin_SG = ReHeat_HP.proseF.T-273.15;
        Nuclear_Pw_load = Pwth_SG/540E6;
        connect(BOP_HeatSink.Cse, sinkP1.C) annotation (Line(points={{70,
                -126},{80,-126}}, color={0,0,255}));
        connect(sourceQ1.C, BOP_HeatSink.Cee) annotation (Line(points={{
                40,-126},{40,-126.2},{52,-126.2}}, color={0,0,255}));
        connect(Valve_HP.C2, Turb_HP.Ce) annotation (Line(points={{14,88},{26,
                88},{26,124.05},{55,124.05}}, color={0,0,255}));
        connect(Pe_ValveHP.C2, Valve_HP.C1) annotation (Line(points={{-15.84,
                87.6},{-16,87.6},{-16,88},{6,88}}, color={0,0,255}));
        connect(Extraction_ReheatHP.Cex, lumpedStraightPipe.C1) annotation (
            Line(points={{37,59.9},{27.5,59.9},{27.5,55},{22,55}}, color={0,0,
                255}));
        connect(Pe_ValveHP.Measure, feedback1.u1) annotation (Line(points={{-24,
                102.16},{-24,127},{-16.7,127}}, color={0,0,255}));
        connect(feedback1.u2, PressureSet_TurbHP_In.y) annotation (Line(
              points={{-9,119.3},{-9,115.4},{-8,115.4}}, color={0,0,255}));
        connect(feedback1.y, pI1.u) annotation (Line(points={{-1.3,127},{10,127},
                {10,122.4}},color={0,0,255}));
        connect(pI1.y, Valve_HP.Ouv)
          annotation (Line(points={{10,113.6},{10,96.5}}, color={0,0,255}));
        connect(Turb_HP.MechPower, Generator.Wmec1) annotation (Line(points={{48.7,113.5},
                {86,113.5},{86,27},{92,27}},             color={0,0,255}));
        connect(Dryer.Csl, invSingularPressureLoss.C1) annotation (Line(points={{31,
                40.455},{31,32},{8,32}},    color={0,0,255}));
        connect(volumeC1.Ce1, singularPressureLoss5.C2) annotation (Line(points={{-14,
                -129},{-14,-147.5},{24,-147.5}},  color={0,0,255}));
        connect(singularPressureLoss4.C1, lumpedStraightPipe1.C2) annotation (
            Line(points={{6,-68},{6,-69},{10,-69}},        color={0,0,255}));
        connect(singularPressureLoss3.C2, Extraction_ReheatHP.Ce) annotation (
            Line(points={{42,76},{42,69.195},{40.5,69.195}}, color={0,0,255}));
        connect(singularPressureLoss5.C1, BOP_HeatSink.Cl) annotation (
            Line(points={{36,-147.5},{36,-148},{62,-148},{62,-136},{61.18,
                -136},{61.18,-134}}, color={0,0,255}));
        connect(volumeC1.Cs, Pump_BP.C1) annotation (Line(points={{-30,-129},{-52,
                -129},{-52,-120}}, color={0,0,255}));
        connect(singularPressureLoss2.C1, lumpedStraightPipe.C2) annotation (Line(
              points={{-8,54},{-8,55},{4,55}},          color={0,0,255}));
        connect(Turb_LP1_a.MechPower, Generator.Wmec3) annotation (Line(points={{46.9,
                3.3},{82,3.3},{82,16},{88,16},{88,15},{92,15}}, color={0,0,255}));
        connect(Turb_LP2.MechPower, Generator.Wmec5) annotation (Line(points={{
                46.1,-87.8},{86,-87.8},{86,3},{92,3}}, color={0,0,255}));
        connect(Bache_a.Ce4, sensorP1.C2) annotation (Line(points={{-183,-84},{
                -186,-84},{-186,-88},{-114.2,-88}}, color={0,0,255}));
        connect(invSingularPressureLoss.C2,Bache_a. Ce2) annotation (Line(points={{-4,32},
                {-68,32},{-68,-72},{-122,-72},{-122,-73},{-174,-73}},
              color={0,0,255}));
        connect(Dryer.Csv, SuperHeat.Ef) annotation (Line(points={{40.1,36.045},{
                50,36.045},{50,26},{-26,26},{-26,16},{-47,16}},
                                           color={0,0,255}));
        connect(singularPressureLoss6.C1,Bache_a. Cs1) annotation (Line(points={{-202,
                -60},{-202,-64.2},{-192,-64.2}},      color={0,0,255}));
        connect(Extraction_ReheatBP.Cs,Turb_LP2. Ce) annotation (Line(points={{
                38.5,-72.195},{46.25,-72.195},{46.25,-70.92},{56,-70.92}},
              color={0,0,255}));
        connect(Extraction_ReheatBP.Cex, lumpedStraightPipe1.C1) annotation (
            Line(points={{35,-68.1},{29.5,-68.1},{29.5,-69},{28,-69}}, color={0,
                0,255}));
        connect(Turb_HP.Cs, singularPressureLoss3.C1) annotation (Line(points={{55,113.95},
                {55,86},{42,86}},            color={0,0,255}));
        connect(Turb_LP1_b.Cs, Extraction_ReheatBP.Ce) annotation (Line(points=
                {{56,-47.08},{56,-58.805},{38.5,-58.805}}, color={0,0,255}));
        connect(Turb_LP1_b.MechPower, Generator.Wmec4) annotation (Line(points={{46.1,
                -47.8},{84,-47.8},{84,9},{92,9}}, color={0,0,255}));
        connect(splitter_SGoutlet.Cs2, SuperHeat.Ec) annotation (Line(points={{
                -76.2,78},{-86,78},{-86,12.8},{-54,12.8}}, color={0,0,255}));
        connect(singularPressureLoss6.C2, Bache_b.Ce2)
          annotation (Line(points={{-202,-48},{-202,-39.3}}, color={0,0,255}));
        connect(Pump_HP.C1, Bache_b.Cs) annotation (Line(points={{-208,2},{-208,-33},
                {-210,-33}}, color={0,0,255}));
        connect(Extraction_TurbBP1a_Outlet.Ce,Turb_LP1_a. Cs) annotation (Line(
              points={{38.5,-8.805},{46.25,-8.805},{46.25,3.93},{55,3.93}},
              color={0,0,255}));
        connect(singularPressureLoss4.C2, Reheat_BP.Ev) annotation (Line(points=
               {{-6,-68},{-27.6,-68},{-27.6,-84.16}}, color={0,0,255}));
        connect(Reheat_BP.Sp, invSingularPressureLoss1.C1) annotation (Line(
              points={{-14.4,-91.96},{-14.4,-101.98},{-4,-101.98},{-4,-109}},
              color={0,0,255}));
        connect(Reheat_BP.Se, sensorP1.C1)
          annotation (Line(points={{-32,-88},{-94,-88}}, color={0,0,255}));
        connect(Pump_BP.C2, Reheat_BP.Ee) annotation (Line(points={{-52,-108},{
                -52,-96},{-2,-96},{-2,-88},{-9.78,-88}}, color={0,0,255}));
        connect(singularPressureLoss2.C2, ReHeat_HP.Ev) annotation (Line(points=
               {{-20,54},{-45.6,54},{-45.6,49.84}}, color={0,0,255}));
        connect(ReHeat_HP.Sp, invSingularPressureLoss2.C1) annotation (Line(
              points={{-32.4,42.04},{-32.4,-65},{-148,-65}}, color={0,0,255}));
        connect(invSingularPressureLoss2.C2,Bache_a. Ce1) annotation (Line(points=
               {{-156,-65},{-168,-65},{-168,-64.2},{-174,-64.2}}, color={0,0,255}));
        connect(Pump_HP.C2, ReHeat_HP.Ee) annotation (Line(points={{-208,14},{-210,
                14},{-210,34},{-22,34},{-22,46},{-27.78,46}}, color={0,0,255}));

        connect(Extraction_TurbBP1a_Outlet.Cs,Turb_LP1_b. Ce) annotation (Line(
              points={{38.5,-22.195},{38.5,-30.92},{56,-30.92}}, color={0,0,255}));
        connect(Turb_LP2.Cs, volumeC2.Ce1) annotation (Line(points={{56,-87.08},{
                50,-87.08},{50,-89},{32,-89}}, color={0,0,255}));
        connect(volumeC2.Cs, singularPressureLoss1.C1) annotation (Line(points={{
                32,-105},{42,-105},{42,-105.5},{50,-105.5}}, color={0,0,255}));
        connect(singularPressureLoss1.C2, BOP_HeatSink.Cv) annotation (
            Line(points={{62,-105.5},{62,-114},{61,-114}}, color={0,0,255}));
        connect(invSingularPressureLoss1.C2, volumeC2.Ce3) annotation (Line(
              points={{-4,-117},{10,-117},{10,-97},{25,-97}}, color={0,0,255}));
        connect(SuperHeat.Sc, Valve_superheat.C1) annotation (Line(points={{-54,3.2},
                {-54,-27},{-55.2,-27}},color={0,0,255}));
        connect(Valve_superheat.C2, Bache_a.Ce3) annotation (Line(points={{-55.2,-38},
                {-55.2,-81.8},{-174,-81.8}}, color={0,0,255}));
        connect(Turb_LP1_a.Ce, Tin_Turb_LP1a.C2) annotation (Line(points=
                {{55,18.07},{10.14,18.07},{10.14,0.8}}, color={0,0,255}));
        connect(SuperHeat.Sf, Tin_Turb_LP1a.C1) annotation (Line(points={
                {-47.07,-1.77636e-15},{-26.535,-1.77636e-15},{-26.535,0.8},
                {-4,0.8}}, color={0,0,255}));
        connect(Tin_Turb_LP1a.Measure, feedback5.u1) annotation (Line(
              points={{3,-10},{3,-38},{0.6,-38}}, color={0,0,255}));
        connect(pI5.u, feedback5.y) annotation (Line(points={{-20.65,-38.5},{
                -14.325,-38.5},{-14.325,-38},{-12.6,-38}}, color={0,0,255}));
        connect(Valve_superheat.Ouv, pI5.y) annotation (Line(points={{-47.55,
                -32.5},{-47.55,-38.5},{-28.35,-38.5}}, color={0,0,255}));
        connect(feedback5.u2, rampe3.y) annotation (Line(points={{-6,-31.4},{-6,
                -29.35},{-6.5,-29.35}}, color={0,0,255}));
        connect(splitter_SGoutlet.Cs3, Pe_ValveHP.C1) annotation (Line(points={
                {-72,87},{-52,87},{-52,87.6},{-32,87.6}}, color={0,0,255}));
        connect(Tout_SG.C2, Pout_SG.C1) annotation (Line(points={{-153.84,
                87.6},{-152,87.6}}, color={0,0,255}));
        connect(Pout_SG.C2, singularPressureLoss_SG_Out.C1) annotation (
            Line(points={{-135.84,87.6},{-135.84,87},{-131,87}}, color={0,
                0,255}));
        connect(singularPressureLoss_SG_Out.C2, Pipe_SGs.C1)
          annotation (Line(points={{-121,87},{-114,87}}, color={0,0,255}));
        connect(Pipe_SGs.C2, splitter_SGoutlet.Ce) annotation (Line(points={{-96,
                87},{-92,87},{-92,87},{-85.86,87}}, color={0,0,255}));
        connect(Extraction_TurbHP_Oulet.Cs, Dryer.Cev) annotation (Line(points=
                {{110.5,53.805},{110.5,44.955},{40.1,44.955}}, color={0,0,255}));
        connect(Valve_Hybrid_IP.C2, Q_Extract_Hybrid_IP.C1) annotation (Line(
              points={{178,-6},{172,-6},{172,-6.6},{158,-6.6}}, color={0,0,255}));
        connect(feedback6.u2, Vapor_MassFlowRamp_extracted4Cogeneration.y)
          annotation (Line(points={{169,11.5},{168,11.5},{168,10.35},{
                171.5,10.35}}, color={0,0,255}));
        connect(pI6.y, Valve_Hybrid_IP.Ouv) annotation (Line(points={{188.4,16},
                {194,16},{194,2.5},{183,2.5}}, color={0,0,255}));
        connect(feedback6.y,pI6. u) annotation (Line(points={{174.5,17},{177.25,
                17},{177.25,16},{179.6,16}},color={0,0,255}));
        connect(Q_Extract_Hybrid_IP.Measure, feedback6.u1) annotation (Line(
              points={{151,6.14},{152,6.14},{152,17},{163.5,17}}, color={0,0,
                255}));
        connect(Q_Extract_Hybrid_IP.C2, Bache_b.Ce1) annotation (Line(points={{
                143.86,-6.6},{143.86,-16},{-110,-16},{-110,-33},{-194,-33}},
              color={0,0,255}));
        connect(Extraction_TurbHP_Oulet.Cex, Ideal_BOP2HeatNetWork_HX.Ec)
          annotation (Line(points={{107,57.9},{100,57.9},{100,40},{140,40}},
              color={0,0,255}));
        connect(Ideal_BOP2HeatNetWork_HX.Sc, T_Outlet_BOP_HX_HeatNetWork.C1)
          annotation (Line(points={{152,40},{164,40},{164,40.8},{180,40.8}},
              color={0,0,255}));
        connect(T_Outlet_BOP_HX_HeatNetWork.C2, Valve_Hybrid_IP.C1)
          annotation (Line(points={{192.12,40.8},{210,40.8},{210,-6},{188,
                -6}}, color={0,0,255}));
        connect(Extraction_ReheatHP.Cs, Tout_Turb_HP.C1) annotation (Line(
              points={{40.5,55.805},{54,55.805},{54,70.8}}, color={0,0,
                255}));
        connect(Tout_Turb_HP.C2, Pout_Turb_HP.C1) annotation (Line(points=
               {{66.12,70.8},{70.06,70.8},{70.06,87.6},{92,87.6}}, color=
                {0,0,255}));
        connect(Pout_Turb_HP.C2, Extraction_TurbHP_Oulet.Ce) annotation (
            Line(points={{108.16,87.6},{110.5,87.6},{110.5,67.195}},
              color={0,0,255}));
        connect(rampe2.y, Valve_superheat1.Ouv) annotation (Line(points={{
                -92.65,112.5},{-89.325,112.5},{-89.325,111.5},{-85.45,111.5}},
              color={0,0,255}));
        connect(Valve_superheat1.C1, splitter_SGoutlet.Cs1) annotation (Line(
              points={{-77.8,106},{-76.2,106},{-76.2,96}}, color={0,0,255}));
        connect(sinkP.C, Valve_superheat1.C2) annotation (Line(points={{-76,128},
                {-77.8,128},{-77.8,117}}, color={0,0,255}));
        connect(Valve_superheat2.C2, sinkP2.C) annotation (Line(points={{16.8,
                -40},{16,-40},{16,-44}}, color={0,0,255}));
        connect(Valve_superheat2.C1, Extraction_TurbBP1a_Outlet.Cex)
          annotation (Line(points={{16.8,-29},{16.8,-18.1},{35,-18.1}}, color={
                0,0,255}));
        connect(Valve_superheat2.Ouv, rampe4.y) annotation (Line(points={{24.45,
                -34.5},{24.45,-38.25},{27.65,-38.25},{27.65,-41.5}}, color={0,0,
                255}));
        connect(Qin_BC,Qin_set. u) annotation (Line(points={{-237,133},{
                -222.8,133}},        color={0,0,127}));
        connect(Hin_set.u,Hin_BC)  annotation (Line(points={{-222.8,105},{
                -235,105}}, color={0,0,127}));
        connect(Qin_set.outputReal, sourceQ_4FMU.IMassFlow) annotation (
            Line(points={{-213.6,133},{-206,133},{-206,127}}, color={0,0,
                255}));
        connect(Hin_set.outputReal, sourceQ_4FMU.ISpecificEnthalpy)
          annotation (Line(points={{-213.6,105},{-206,105},{-206,117}},
              color={0,0,255}));
        connect(Pin_calc.inputReal, sourceQ_4FMU.OPressure) annotation (
            Line(points={{-217.6,122},{-218,122},{-218,122.2},{-211,122.2}},
              color={0,0,255}));
        connect(Pin_TSPro,Pin_TSPro)
          annotation (Line(points={{-237,123},{-237,123}},
                                                       color={0,0,127}));
        connect(Pin_TSPro,Pin_calc. y) annotation (Line(points={{-237,123},{
                -230.5,123},{-230.5,122},{-226.8,122}}, color={0,0,127}));
        connect(sourceQ_4FMU.C, Tout_SG.C1) annotation (Line(points={{-196,
                122},{-184,122},{-184,87.6},{-170,87.6}}, color={0,0,255}));
        connect(Qout_calc.inputReal, sinkP_4FMU.OFlowRate) annotation (
            Line(points={{-298,53.6},{-298,51},{-304,51}}, color={0,0,255}));
        connect(Hout_calc.inputReal, sinkP_4FMU.OSpecificEnthalpy)
          annotation (Line(points={{-312,55.6},{-312,51},{-308.2,51}},
              color={0,0,255}));
        connect(sinkP_4FMU.IPressure, Pout_calc.outputReal) annotation (
            Line(points={{-301,46},{-294.4,46},{-294.4,45}}, color={0,0,
                255}));
        connect(Pout_calc.u,Pout_BC)  annotation (Line(points={{-285.2,45},{
                -273,45}},          color={0,0,127}));
        connect(Qout_TSPro,Qout_calc. y) annotation (Line(points={{-291,83},{
                -291,70.5},{-298,70.5},{-298,62.8}},
                                            color={0,0,127}));
        connect(Hout_TSPro,Hout_calc. y) annotation (Line(points={{-317,81},{
                -317,64.8},{-312,64.8}}, color={0,0,127}));
        connect(sinkP_4FMU.C, ReHeat_HP.Se) annotation (Line(points={{-316,
                46},{-316,20},{-244,20},{-244,56},{-50,56},{-50,46}},
              color={0,0,255}));
        connect(fluid2TSPro1.steam_outlet, Ideal_BOP2HeatNetWork_HX.Ef)
          annotation (Line(points={{129.998,60.05},{129.998,48},{136,48}},
              color={0,0,255}));
        connect(InletFlow_FromHeatNetwork.ports[1], fluid2TSPro1.port_a)
          annotation (Line(points={{129,93},{129,86.5},{130,86.5},{130,
                79.8}}, color={0,127,255}));
        connect(Ideal_BOP2HeatNetWork_HX.Sf, T_2HeatNetWork.C1)
          annotation (Line(points={{156,47.92},{172,47.92},{172,54.8}},
              color={0,0,255}));
        connect(BackPressure_FromHeatNetWork.ports[1], fluid2TSPro.port_b)
          annotation (Line(points={{179,97},{179,92.5},{180,92.5},{180,80}},
              color={0,127,255}));
        connect(fluid2TSPro.steam_inlet, T_2HeatNetWork.C2) annotation (
            Line(points={{180,60},{184.12,60},{184.12,54.8}}, color={0,0,
                255}));
        connect(InletFlowRate_FromHeatNetWork.y,
          InletFlow_FromHeatNetwork.m_flow_in) annotation (Line(points={{
                132,115.4},{132,112},{132,107},{133.8,107}}, color={0,0,
                127}));
        connect(Pump_HP.rpm_or_mpower, Test_Timetable_Rotation_PumpHP.y)
          annotation (Line(points={{-214.6,8},{-230,8},{-230,-52},{-237,
                -52}}, color={0,0,255}));
        connect(Pump_BP.rpm_or_mpower, Test_Timetable_Rotation_PumpLP.y)
          annotation (Line(points={{-58.6,-114},{-84,-114},{-84,-140},{
                -167,-140},{-167,-136}}, color={0,0,255}));
        annotation (Diagram(coordinateSystem(extent={{-340,-160},{220,180}})), Icon(
              coordinateSystem(extent={{-340,-160},{220,180}})),
          experiment(Tolerance=1e-06, __Dymola_Algorithm="Dassl"));
      end FMI_BOP;

      annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"));
    end FMI_2generate;

    package Demo_FMU_Coupling "BOP demo, plugged to FMU Boundary Conditions"

     model BOP_fmu
        "BoP with modified Fluid connection allowing to build BOP with Fluid FMU"
     extends fmuIcon;
     // Model automatically generated by Dymola from FMI model description
      public
       type Modelica_Blocks_Interfaces_RealInput = Real;
       type Modelica_Blocks_Interfaces_RealOutput = Real;
       type ThermoSysPro_Units_SI_AbsolutePressure = Real (unit = "Pa", displayUnit = "bar", nominal = 100000.0, quantity = "Pressure", min = 0.0);
       type ThermoSysPro_Units_SI_AngularVelocity = Real (unit = "rad/s", quantity = "AngularVelocity");
       type ThermoSysPro_Units_SI_Area = Real (unit = "m2", quantity = "Area");
       type ThermoSysPro_Units_SI_CoefficientOfHeatTransfer = Real (unit = "W/(m2.K)", quantity = "CoefficientOfHeatTransfer");
       type ThermoSysPro_Units_SI_Density = Real (unit = "kg/m3", displayUnit = "g/cm3", quantity = "Density", min = 0.0);
       type ThermoSysPro_Units_SI_DerDensityByEnthalpy = Real (unit = "kg.s2/m5");
       type ThermoSysPro_Units_SI_DerDensityByPressure = Real (unit = "s2/m2");
       type ThermoSysPro_Units_SI_Diameter = Real (unit = "m", quantity = "Length", min = 0.0);
       type ThermoSysPro_Units_SI_DynamicViscosity = Real (unit = "Pa.s", quantity = "DynamicViscosity", min = 0.0);
       type ThermoSysPro_Units_SI_Height = Real (unit = "m", quantity = "Length", min = 0.0);
       type ThermoSysPro_Units_SI_Length = Real (unit = "m", quantity = "Length");
       type ThermoSysPro_Units_SI_MassFlowRate = Real (unit = "kg/s", quantity = "MassFlowRate");
       type ThermoSysPro_Units_SI_MassFraction = Real (quantity = "MassFraction", min = 0.0, max = 1.0);
       type ThermoSysPro_Units_SI_Position = Real (unit = "m", quantity = "Length");
       type ThermoSysPro_Units_SI_Power = Real (unit = "W", quantity = "Power");
       type ThermoSysPro_Units_SI_PressureDifference = Real (unit = "Pa", displayUnit = "bar", quantity = "Pressure");
       type ThermoSysPro_Units_SI_ReynoldsNumber = Real (quantity = "ReynoldsNumber");
       type ThermoSysPro_Units_SI_SpecificEnergy = Real (unit = "J/kg", quantity = "SpecificEnergy");
       type ThermoSysPro_Units_SI_SpecificEnthalpy = Real (unit = "J/kg", quantity = "SpecificEnergy");
       type ThermoSysPro_Units_SI_SpecificEntropy = Real (unit = "J/(kg.K)", quantity = "SpecificEntropy");
       type ThermoSysPro_Units_SI_SpecificHeatCapacity = Real (unit = "J/(kg.K)", quantity = "SpecificHeatCapacity");
       type ThermoSysPro_Units_SI_Temperature = Real (unit = "K", displayUnit = "degC", nominal = 300.0, quantity = "ThermodynamicTemperature", min = 0.0);
       type ThermoSysPro_Units_SI_Torque = Real (unit = "N.m", quantity = "Torque");
       type ThermoSysPro_Units_SI_Velocity = Real (unit = "m/s", quantity = "Velocity");
       type ThermoSysPro_Units_SI_Volume = Real (unit = "m3", quantity = "Volume");
       type ThermoSysPro_Units_SI_VolumeFlowRate = Real (unit = "m3/s", quantity = "VolumeFlowRate");
       type ThermoSysPro_Units_nonSI_AngularVelocity_rpm = Real (quantity = "Angular velocity");
       type ThermoSysPro_Units_xSI_Cv = Real (unit = "m4/(s.N5)", quantity = "Cv U.S.");
       type ThermoSysPro_Units_xSI_DerDensityByEntropy = Real (unit = "kg2.K/(m3.J)", quantity = "DerDensityByEntropy");
       parameter Real Tmax_synop = 1000;
       Real Eff_Cycle_net;
       Real Eff_Cycle_brut;
       Real Pw_eGrid;
       Real Pwth_COG;
       Real T_COG;
       Real Heat_Power_ratio;
       Real Tin_SG;
       Real Nuclear_Pw_load;
       Real Pwth_SG;
       parameter Real _CsHP_start = 685708.1898310621
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real CsHP(start = _CsHP_start, fixed=false);
       parameter Real _CsBP1a_start = 24004.035334331
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real CsBP1a(start = _CsBP1a_start, fixed=false);
       parameter Real _CsBP1b_start = 1686.7006077673466
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real CsBP1b(start = _CsBP1b_start, fixed=false);
       parameter Real _CsBP2_start = 644.9127937152352
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real CsBP2(start = _CsBP2_start, fixed=false);
       parameter Real _ScondesHP_start = 1586.6591598184393
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real ScondesHP(start = _ScondesHP_start, fixed=false);
       parameter Real _SPurgeHP_start = 31.466846045765656
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real SPurgeHP(start = _SPurgeHP_start, fixed=false);
       parameter Real _ScondesBP_start = 1195.1810801870483
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real ScondesBP(start = _ScondesBP_start, fixed=false);
       parameter Real _SPurgeBP_start = 182.47794479770502
       annotation(Dialog(tab = "Initial", group = "Approximate"));
       Real SPurgeBP(start = _SPurgeBP_start, fixed=false);
       parameter Real SpHP_Q = 24.51;
       parameter Real SpHP_H = 700800.0;
       parameter Real SpBP_Q = 16.41;
       parameter Real SpBP_H = 317890.0;
       parameter Real CvmaxSurch = 200;
       parameter Real kfric_Surch(fixed=false);
       parameter Real kfric_SortieGV(fixed=false);
      protected
       record Valve_HP_rec
         parameter ThermoSysPro_Units_xSI_Cv Cvmax = 125000 "Maximum CV (active if mode_caract=0)";
         constant Real 'caract[1,1]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[1,2]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[2,1]' = 1.0 "Position vs. Cv characteristics (active if mode_caract=1)";
         Real 'caract[2,2]' "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Integer mode_caract = 0 "0:linear characteristics - 1:characteristics is given by caract[]";
         parameter Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation (active if mode_caract=1)";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant Integer option_rho_water = 1 "1: using (deltaP*Cv^2=A.Q^2/rho^2) - 2: using (deltaP*Cv^2=A.Q^2/(rho*rho_15)); with rho_15 is the density of the water at 15.5556 °C)";
         constant ThermoSysPro_Units_SI_Density p_rho(displayUnit = "kg/m3") = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_xSI_Cv Cv "Cv";
         parameter ThermoSysPro_Units_SI_MassFlowRate _Q_start = 218.8884495827999
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_MassFlowRate Q(start = _Q_start, fixed=false) "Mass flow rate";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_Density rho(displayUnit = "kg/m3") "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ouv_rec
           Real signal;
         end Ouv_rec;
        public
         Ouv_rec Ouv;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Valve_HP_rec;
      public
       Valve_HP_rec Valve_HP annotation(Dialog);
      protected
       record Pump_HP_rec
         parameter Real VRot_inBaseUnit(unit = "rad/s") = 146.60765716752368;
         parameter ThermoSysPro_Units_nonSI_AngularVelocity_rpm VRot = fmi_Functions.'to_rev/min'(VRot_inBaseUnit) "Fixed rotational speed (active if fixed_rot_or_power=1 and rpm_or_mpower connector not connected)";
         parameter ThermoSysPro_Units_SI_Power MPower = 100000.0 "Fixed mechanical power (active if fixed_rot_or_power=2 and rpm_or_mpower connector not connected)";
         parameter Real VRotn_inBaseUnit(unit = "rad/s") = 146.60765716752368;
         parameter ThermoSysPro_Units_nonSI_AngularVelocity_rpm VRotn = fmi_Functions.'to_rev/min'(VRotn_inBaseUnit) "Nominal rotational speed";
         parameter Real rm = 0.88 "Product of the pump mechanical and electrical efficiencies";
         constant Integer fixed_rot_or_power = 1 "1: fixed rotational speed - 2: fixed mechanical power";
         constant Boolean adiabatic_compression = false "true: compression at constant enthalpy - false: compression with varying enthalpy";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Real a1 = -2551 "x^2 coef. of the pump characteristics hn = f(vol_flow) (s2/m5)";
         parameter Real a2 = 0 "x coef. of the pump characteristics hn = f(vol_flow) (s/m2)";
         parameter Real a3 = 594 "Constant coef. of the pump characteristics hn = f(vol_flow) (m)";
         parameter Real b1 = -16.5 "x^2 coef. of the pump efficiency characteristics rh = f(vol_flow) (s2/m6)";
         parameter Real b2 = 7.65 "x coef. of the pump efficiency characteristics rh = f(vol_flow) (s/m3)";
         parameter Real b3 = -0.0075464 "Constant coef. of the pump efficiency characteristics rh = f(vol_flow) (s.u.)";
         Real rh "Hydraulic efficiency";
         ThermoSysPro_Units_SI_Height hn "Pump head";
         Real R "Reduced rotational speed";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         parameter ThermoSysPro_Units_SI_VolumeFlowRate _Qv_start = 0.25294928050897675
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_VolumeFlowRate Qv(start = _Qv_start, fixed=false) "Volume flow rate";
         ThermoSysPro_Units_SI_Power Wh "Hydraulic power";
         ThermoSysPro_Units_SI_Power Wm "Mechanical power";
         Real Vr_inBaseUnit(unit = "rad/s") = fmi_Functions.'from_rev/min'(Vr);
         ThermoSysPro_Units_nonSI_AngularVelocity_rpm Vr "Rotational speed";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Pressure variation between the outlet and the inlet";
         ThermoSysPro_Units_SI_SpecificEnthalpy deltaH "Specific enthalpy variation between the outlet and the inlet";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pm_start = 2787448.563511
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pm(start = _Pm_start, fixed=false) "Fluid average pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 490056.0454434941
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid average specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 492605.1139489484
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 492605.11394894833
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record rpm_or_mpower_rec
           Real signal;
         end rpm_or_mpower_rec;
        public
         rpm_or_mpower_rec rpm_or_mpower;
       end Pump_HP_rec;
      public
       Pump_HP_rec Pump_HP annotation(Dialog);
      protected
       record sourceQ1_rec
         parameter ThermoSysPro_Units_SI_MassFlowRate Q0 = 4000 "Mass flow (active if IMassFlow connector is not connected)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 137770 "Fluid specific enthalpy (active if IEnthalpy connector is not connected)";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record IMassFlow_rec
           Real signal;
         end IMassFlow_rec;
        public
         IMassFlow_rec IMassFlow;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
       end sourceQ1_rec;
      public
       sourceQ1_rec sourceQ1 annotation(Dialog);
      protected
       record sinkP1_rec
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 100000 "Sink pressure";
         parameter ThermoSysPro_Units_SI_Temperature T0 = 290 "Sink temperature (active if option_temperature=1)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 100000 "Sink specific enthalpy (active if option_temperature=2)";
         constant Integer option_temperature = 1 "1:temperature fixed - 2:specific enthalpy fixed";
         parameter Integer mode = 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record IPressure_rec
           Real signal;
         end IPressure_rec;
        public
         IPressure_rec IPressure;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record ITemperature_rec
           Real signal;
         end ITemperature_rec;
        public
         ITemperature_rec ITemperature;
       end sinkP1_rec;
      public
       sinkP1_rec sinkP1 annotation(Dialog);
      protected
       record BOP_HeatSink_rec
         parameter ThermoSysPro_Units_SI_Volume V = 1000 "Cavity volume";
         parameter ThermoSysPro_Units_SI_Area A = 100 "Cavity cross-sectional area";
         parameter Real Vf0 = 0.5 "Fraction of initial water volume in the drum (active if steady_state=false)";
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 7000 "Fluid initial pressure (active if steady_state=false)";
         parameter Boolean gravity_pressure = false "true: fluid pressure at the bottom of the cavity includes gravity term - false: without gravity term";
         parameter Real Ccond = 1 "Condensation coefficient";
         parameter Real Cevap = 0.09 "Evaporation coefficient";
         parameter Real Xlo = 0.0025 "Vapor mass fraction in the liquid phase from which the liquid starts to evaporate";
         parameter Real Xvo = 0.9975 "Vapor mass fraction in the gas phase from which the liquid starts to condensate";
         ThermoSysPro_Units_SI_Area Avl "Heat exchange surface between the liquid and gas phases";
         parameter Real Kvl = 1000 "Heat exchange coefficient between the liquid and gas phases";
         parameter ThermoSysPro_Units_SI_Length L = 10.0 "Pipe length";
         parameter ThermoSysPro_Units_SI_Diameter D = 0.018 "Pipe internal diameter";
         parameter ThermoSysPro_Units_SI_Length e = 0.002 "Wall thickness";
         parameter ThermoSysPro_Units_SI_Position z1 = 0 "Inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2 = 0 "Outlet altitude";
         parameter ThermoSysPro_Units_SI_Length rugosrel = 0.0007 "Pipe roughness";
         parameter Real lambda = 0.03 "Friction pressure loss coefficient (active if lambda_fixed=true)";
         parameter Integer ntubes = 28700 "Number of pipes in parallel";
         ThermoSysPro_Units_SI_Area At "Internal pipe cross-section area (cooling fluid)";
         constant Boolean steady_state = false "true: start from steady state - false: start from (P0, Vl0)";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         ThermoSysPro_Units_SI_Density rhom "Liquid phase density";
         ThermoSysPro_Units_SI_PressureDifference dpf "Friction pressure loss";
         ThermoSysPro_Units_SI_PressureDifference dpg "Gravity pressure loss";
         Real khi "Hydraulic pressure loss coefficient";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pfond_start = 7000
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pfond(start = _Pfond_start, fixed=false) "Fluid pressure at the bottom of the cavity";
         ThermoSysPro_Units_SI_SpecificEnthalpy hl "Liquid phase spepcific enthalpy";
         ThermoSysPro_Units_SI_SpecificEnthalpy hv "Gas phase spepcific enthalpy";
         ThermoSysPro_Units_SI_Temperature Tl "Liquid phase temperature";
         ThermoSysPro_Units_SI_Temperature Tv "Gas phase temperature";
         ThermoSysPro_Units_SI_Volume Vl "Liquid phase volume";
         Real _Vl_der(unit = "m3/s") "der(Liquid phase volume)";
         ThermoSysPro_Units_SI_Volume Vv "Gas phase volume";
         Real xl "Mass vapor fraction in the liquid phase";
         Real xv "Mass vapor fraction in the gas phase";
         ThermoSysPro_Units_SI_Density rhol "Liquid phase density";
         ThermoSysPro_Units_SI_Density rhov "Gas phase density";
         ThermoSysPro_Units_SI_MassFlowRate BQl "Right hand side of the mass balance equation of the liquid phase";
         ThermoSysPro_Units_SI_MassFlowRate BQv "Right hand side of the mass balance equation of the gas phaser";
         ThermoSysPro_Units_SI_Power BHl "Right hand side of the energy balance equation of the liquid phase";
         ThermoSysPro_Units_SI_Power BHv "Right hand side of the energy balance equation of the gas phase";
         ThermoSysPro_Units_SI_MassFlowRate Qcond "Condensation mass flow rate from the vapor phase";
         ThermoSysPro_Units_SI_MassFlowRate Qevap "Evaporation mass flow rate from the liquid phase";
         ThermoSysPro_Units_SI_Power Wvl "Thermal power exchanged from the gas phase to the liquid phase";
         ThermoSysPro_Units_SI_Power Wout "Thermal power exchanged from the steam to the pipes";
        protected
         record prol_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prol_rec;
        public
         prol_rec prol;
        protected
         record prov_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prov_rec;
        public
         prov_rec prov;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record Cv_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cv_rec;
        public
         Cv_rec Cv;
        protected
         record Cl_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cl_rec;
        public
         Cl_rec Cl;
        protected
         record yNiveau_rec
           Real signal;
         end yNiveau_rec;
        public
         yNiveau_rec yNiveau;
        protected
         record prod_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prod_rec;
        public
         prod_rec prod;
        protected
         record Cee_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cee_rec;
        public
         Cee_rec Cee;
        protected
         record Cse_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cse_rec;
        public
         Cse_rec Cse;
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           parameter ThermoSysPro_Units_SI_Density _d_start = 990.7730249550118
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0,start = _d_start, fixed=false) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe annotation(Dialog);
       end BOP_HeatSink_rec;
      public
       BOP_HeatSink_rec BOP_HeatSink annotation(Dialog);
      protected
       record Turb_HP_rec
         Real Cst "Stodola's ellipse coefficient";
         parameter Real W_fric = 1 "Power losses due to hydrodynamic friction (percent)";
         parameter Real eta_stato = 1.0 "Efficiency to account for cinetic losses (<= 1) (s.u.)";
         parameter ThermoSysPro_Units_SI_Area area_nz = 1 "Nozzle area";
         parameter Real eta_nz = 1.0 "Nozzle efficency (eta_nz < 1 - turbine with nozzle - eta_nz = 1 - turbine without nozzle)";
         parameter ThermoSysPro_Units_SI_MassFlowRate Qmax = 1 "Maximum mass flow through the turbine";
         parameter Real eta_is_nom = 0.9 "Nominal isentropic efficiency";
         parameter Real eta_is_min = 0.35 "Minimum isentropic efficiency";
         parameter Real a = -1.3889 "x^2 coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real b = 2.6944 "x coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real c = -0.5056 "Constant coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         parameter Integer mode_e = 0 "IF97 region before expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_s = 0 "IF97 region after expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_ps = 0 "IF97 region after isentropic expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real eta_is "Isentropic efficiency";
         Real eta_is_wet "Isentropic efficiency for wet steam";
         ThermoSysPro_Units_SI_Power W "Mechanical power produced by the turbine";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_SpecificEnthalpy His "Fluid specific enthalpy after isentropic expansion";
         ThermoSysPro_Units_SI_SpecificEnthalpy Hrs "Fluid specific enthalpy after the real expansion";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pe_start = 4400000
         annotation(Dialog(tab = "Initial", group = "States"));
         ThermoSysPro_Units_SI_AbsolutePressure Pe(start = _Pe_start) "Pressure at the inlet";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Ps_start = 754701.12073853
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Ps(start = _Ps_start, fixed=false) "Pressure at the outlet";
         ThermoSysPro_Units_SI_Temperature Te "Temperature at the inlet";
         ThermoSysPro_Units_SI_Temperature Ts "Temperature at the outlet";
         ThermoSysPro_Units_SI_Velocity Vs "Fluid velocity at the outlet";
         ThermoSysPro_Units_SI_Density rhos(displayUnit = "kg/m3") "Fluid density at the outlet";
         Real xm(min = 0.0) "Average vapor mass fraction";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record pros_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros_rec;
        public
         pros_rec pros;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs annotation(Dialog);
        protected
         record props_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(nominal = 1000000.0, min = -1000000.0, max = 100000000.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant pressure";
           ThermoSysPro_Units_xSI_DerDensityByEntropy ddsp "Derivative of the density wrt. specific entropy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddps "Derivative of the density wrt. pressure at constant specific entropy";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end props_rec;
        public
         props_rec props;
        protected
         record M_rec
           constant ThermoSysPro_Units_SI_Torque Ctr = 0 "Torque";
           constant ThermoSysPro_Units_SI_AngularVelocity w = 0 "Angular velocity";
         end M_rec;
        public
         M_rec M;
        protected
         record MechPower_rec
           Real signal;
         end MechPower_rec;
        public
         MechPower_rec MechPower;
        protected
         record pros1_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros1_rec;
        public
         pros1_rec pros1;
       end Turb_HP_rec;
      public
       Turb_HP_rec Turb_HP annotation(Dialog);
      protected
       record Turb_LP1_a_rec
         Real Cst "Stodola's ellipse coefficient";
         parameter Real W_fric = 1 "Power losses due to hydrodynamic friction (percent)";
         parameter Real eta_stato = 1.0 "Efficiency to account for cinetic losses (<= 1) (s.u.)";
         parameter ThermoSysPro_Units_SI_Area area_nz = 1 "Nozzle area";
         parameter Real eta_nz = 1.0 "Nozzle efficency (eta_nz < 1 - turbine with nozzle - eta_nz = 1 - turbine without nozzle)";
         parameter ThermoSysPro_Units_SI_MassFlowRate Qmax = 1 "Maximum mass flow through the turbine";
         parameter Real eta_is_nom = 0.89 "Nominal isentropic efficiency";
         parameter Real eta_is_min = 0.35 "Minimum isentropic efficiency";
         parameter Real a = -1.3889 "x^2 coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real b = 2.6944 "x coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real c = -0.5056 "Constant coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         parameter Integer mode_e = 0 "IF97 region before expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_s = 0 "IF97 region after expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_ps = 0 "IF97 region after isentropic expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real eta_is "Isentropic efficiency";
         Real eta_is_wet "Isentropic efficiency for wet steam";
         ThermoSysPro_Units_SI_Power W "Mechanical power produced by the turbine";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_SpecificEnthalpy His "Fluid specific enthalpy after isentropic expansion";
         ThermoSysPro_Units_SI_SpecificEnthalpy Hrs "Fluid specific enthalpy after the real expansion";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pe_start = 674700
         annotation(Dialog(tab = "Initial", group = "States"));
         ThermoSysPro_Units_SI_AbsolutePressure Pe(start = _Pe_start) "Pressure at the inlet";
         ThermoSysPro_Units_SI_AbsolutePressure Ps "Pressure at the outlet";
         ThermoSysPro_Units_SI_Temperature Te "Temperature at the inlet";
         ThermoSysPro_Units_SI_Temperature Ts "Temperature at the outlet";
         ThermoSysPro_Units_SI_Velocity Vs "Fluid velocity at the outlet";
         ThermoSysPro_Units_SI_Density rhos(displayUnit = "kg/m3") "Fluid density at the outlet";
         Real xm(min = 0.0) "Average vapor mass fraction";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record pros_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros_rec;
        public
         pros_rec pros;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2977121.8265122357
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce annotation(Dialog);
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2724624.756816755
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2724624.7568167527
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs annotation(Dialog);
        protected
         record props_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(nominal = 1000000.0, min = -1000000.0, max = 100000000.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant pressure";
           ThermoSysPro_Units_xSI_DerDensityByEntropy ddsp "Derivative of the density wrt. specific entropy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddps "Derivative of the density wrt. pressure at constant specific entropy";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end props_rec;
        public
         props_rec props;
        protected
         record M_rec
           constant ThermoSysPro_Units_SI_Torque Ctr = 0 "Torque";
           constant ThermoSysPro_Units_SI_AngularVelocity w = 0 "Angular velocity";
         end M_rec;
        public
         M_rec M;
        protected
         record MechPower_rec
           Real signal;
         end MechPower_rec;
        public
         MechPower_rec MechPower;
        protected
         record pros1_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros1_rec;
        public
         pros1_rec pros1;
       end Turb_LP1_a_rec;
      public
       Turb_LP1_a_rec Turb_LP1_a annotation(Dialog);
      protected
       record Tout_SG_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
       end Tout_SG_rec;
      public
       Tout_SG_rec Tout_SG annotation(Dialog);
      protected
       record Pe_ValveHP_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Pe_ValveHP_rec;
      public
       Pe_ValveHP_rec Pe_ValveHP annotation(Dialog);
      protected
       record Pump_BP_rec
         parameter Real VRot_inBaseUnit(unit = "rad/s") = 146.60765716752368;
         parameter ThermoSysPro_Units_nonSI_AngularVelocity_rpm VRot = fmi_Functions.'to_rev/min'(VRot_inBaseUnit) "Fixed rotational speed (active if fixed_rot_or_power=1 and rpm_or_mpower connector not connected)";
         parameter ThermoSysPro_Units_SI_Power MPower = 100000.0 "Fixed mechanical power (active if fixed_rot_or_power=2 and rpm_or_mpower connector not connected)";
         parameter Real VRotn_inBaseUnit(unit = "rad/s") = 146.60765716752368;
         parameter ThermoSysPro_Units_nonSI_AngularVelocity_rpm VRotn = fmi_Functions.'to_rev/min'(VRotn_inBaseUnit) "Nominal rotational speed";
         parameter Real rm = 0.88 "Product of the pump mechanical and electrical efficiencies";
         constant Integer fixed_rot_or_power = 1 "1: fixed rotational speed - 2: fixed mechanical power";
         constant Boolean adiabatic_compression = false "true: compression at constant enthalpy - false: compression with varying enthalpy";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Real a1 = -477.7 "x^2 coef. of the pump characteristics hn = f(vol_flow) (s2/m5)";
         parameter Real a2 = 0 "x coef. of the pump characteristics hn = f(vol_flow) (s/m2)";
         parameter Real a3 = 72.8 "Constant coef. of the pump characteristics hn = f(vol_flow) (m)";
         parameter Real b1 = -25.2 "x^2 coef. of the pump efficiency characteristics rh = f(vol_flow) (s2/m6)";
         parameter Real b2 = 9.47 "x coef. of the pump efficiency characteristics rh = f(vol_flow) (s/m3)";
         parameter Real b3 = -0.0075464 "Constant coef. of the pump efficiency characteristics rh = f(vol_flow) (s.u.)";
         Real rh "Hydraulic efficiency";
         ThermoSysPro_Units_SI_Height hn "Pump head";
         Real R "Reduced rotational speed";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         parameter ThermoSysPro_Units_SI_VolumeFlowRate _Qv_start = 0.1834019039955073
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_VolumeFlowRate Qv(start = _Qv_start, fixed=false) "Volume flow rate";
         ThermoSysPro_Units_SI_Power Wh "Hydraulic power";
         ThermoSysPro_Units_SI_Power Wm "Mechanical power";
         Real Vr_inBaseUnit(unit = "rad/s") = fmi_Functions.'from_rev/min'(Vr);
         ThermoSysPro_Units_nonSI_AngularVelocity_rpm Vr "Rotational speed";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Pressure variation between the outlet and the inlet";
         ThermoSysPro_Units_SI_SpecificEnthalpy deltaH "Specific enthalpy variation between the outlet and the inlet";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pm_start = 360907.39267732
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pm(start = _Pm_start, fixed=false) "Fluid average pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 163775.22431113676
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid average specific enthalpy";
        protected
         record C1_rec
           parameter ThermoSysPro_Units_SI_AbsolutePressure _P_start = 6999.996697349103
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_AbsolutePressure P(start = _P_start, fixed=false) "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 164184.93341902483
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 164184.9334190248
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record rpm_or_mpower_rec
           Real signal;
         end rpm_or_mpower_rec;
        public
         rpm_or_mpower_rec rpm_or_mpower;
       end Pump_BP_rec;
      public
       Pump_BP_rec Pump_BP annotation(Dialog);
      protected
       record singularPressureLoss3_rec
         parameter Real K = 0.0001 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss3_rec;
      public
       singularPressureLoss3_rec singularPressureLoss3 annotation(Dialog);
      protected
       record Extraction_ReheatHP_rec
         parameter Real alpha = 1 "Vapor mass fraction at the extraction/Vapor mass fraction at the inlet (0 <= alpha <= 1)";
         parameter Integer mode_e = 0 "IF97 region at the inlet. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real x_ex "Vapor mass fraction at the extraction outlet";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record Cex_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cex_rec;
        public
         Cex_rec Cex;
       end Extraction_ReheatHP_rec;
      public
       Extraction_ReheatHP_rec Extraction_ReheatHP annotation(Dialog);
      protected
       record lumpedStraightPipe_rec
         parameter ThermoSysPro_Units_SI_Length L = 1 "Pipe length";
         parameter ThermoSysPro_Units_SI_Diameter D = 0.4 "Pipe internal diameter";
         parameter Integer ntubes = 1 "Number of pipes in parallel";
         parameter Real lambda = 0.03 "Friction pressure loss coefficient (active if lambda_fixed=true)";
         constant Real rugosrel = 0.0001 "Pipe roughness (active if lambda_fixed=false)";
         parameter ThermoSysPro_Units_SI_Position z1 = 0 "Inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2 = 0 "Outlet altitude";
         constant Boolean lambda_fixed = false "true: lambda given by parameter - false: lambde computed using Idel'Cik correlation";
         constant Boolean inertia = false "true: momentum balance equation with inertia - false: without inertia";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real khi "Hydraulic pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaPf "Friction pressure loss";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Total pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_ReynoldsNumber Re "Reynolds number";
         constant ThermoSysPro_Units_SI_ReynoldsNumber Relim = 5600000.0 "Limit Reynolds number";
         constant Real lam = 0.011979797083255311 "Friction pressure loss coefficient";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_DynamicViscosity mu "Fluid dynamic viscosity";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           parameter ThermoSysPro_Units_SI_AbsolutePressure _P_start = 754564.1414536542
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_AbsolutePressure P(start = _P_start, fixed=false) "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end lumpedStraightPipe_rec;
      public
       lumpedStraightPipe_rec lumpedStraightPipe annotation(Dialog);
      protected
       record singularPressureLoss2_rec
         parameter Real K = 1E-06 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pm_start = 754564.1413820141
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pm(start = _Pm_start, fixed=false) "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 700800.0000000022
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss2_rec;
      public
       singularPressureLoss2_rec singularPressureLoss2 annotation(Dialog);
      protected
       record PressureSet_TurbHP_In_rec
         parameter Real k = 4450000.0 "Valeur de la sortie";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end PressureSet_TurbHP_In_rec;
      public
       PressureSet_TurbHP_In_rec PressureSet_TurbHP_In annotation(Dialog);
      protected
       record feedback1_rec
        protected
         record u2_rec
           Real signal;
         end u2_rec;
        public
         u2_rec u2;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record u1_rec
           Real signal;
         end u1_rec;
        public
         u1_rec u1;
       end feedback1_rec;
      public
       feedback1_rec feedback1;
      protected
       record pI1_rec
         parameter Real k = 1 "Gain";
         parameter Real Ti = 1 "Constante de temps (s)";
         parameter Real ureset0 = 0 "Valeur de la sortie sur reset (si ureset non connecté)";
         constant Boolean permanent = true "Calcul du permanent";
         Real x;
        protected
         record u_rec
           Real signal;
         end u_rec;
        public
         u_rec u;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record ureset_rec
           Real signal;
         end ureset_rec;
        public
         ureset_rec ureset;
        protected
         record reset_rec
           constant Boolean signal = false;
         end reset_rec;
        public
         reset_rec reset;
       end pI1_rec;
      public
       pI1_rec pI1 annotation(Dialog);
      protected
       record Generator_rec
         parameter Real eta = 98 "Efficiency (percent)";
         ThermoSysPro_Units_SI_Power Welec "Electrical power produced by the generator";
        protected
         record Wmec2_rec
           constant Real signal = 0;
         end Wmec2_rec;
        public
         Wmec2_rec Wmec2;
        protected
         record Wmec1_rec
           Real signal;
         end Wmec1_rec;
        public
         Wmec1_rec Wmec1;
        protected
         record Wmec3_rec
           Real signal;
         end Wmec3_rec;
        public
         Wmec3_rec Wmec3;
        protected
         record Wmec4_rec
           Real signal;
         end Wmec4_rec;
        public
         Wmec4_rec Wmec4;
        protected
         record Wmec5_rec
           Real signal;
         end Wmec5_rec;
        public
         Wmec5_rec Wmec5;
       end Generator_rec;
      public
       Generator_rec Generator annotation(Dialog);
      protected
       record sensorP1_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
        protected
         record Measure_rec
           Real signal(nominal = 100000.0, min = 0.0);
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 370969.92329333595
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end sensorP1_rec;
      public
       sensorP1_rec sensorP1 annotation(Dialog);
      protected
       record Dryer_rec
         parameter Real eta = 1 "Vapor mass fraction at outlet (0 < eta <= 1 and eta > Vapor mass fraction at the inlet)";
         parameter Integer mode_e = 0 "IF97 region at the inlet. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2765900.246437356
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid specific enthalpy";
         Real xe "Vapor mass fraction at the inlet";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record Cev_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cev_rec;
        public
         Cev_rec Cev annotation(Dialog);
        protected
         record Csv_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2765900.246437356
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Csv_rec;
        public
         Csv_rec Csv annotation(Dialog);
        protected
         record lsat1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat1_rec;
        public
         lsat1_rec lsat1;
        protected
         record vsat1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat1_rec;
        public
         vsat1_rec vsat1;
        protected
         record Csl_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Csl_rec;
        public
         Csl_rec Csl;
       end Dryer_rec;
      public
       Dryer_rec Dryer annotation(Dialog);
      protected
       record invSingularPressureLoss_rec
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real K "Pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end invSingularPressureLoss_rec;
      public
       invSingularPressureLoss_rec invSingularPressureLoss annotation(Dialog);
      protected
       record Turb_LP2_rec
         Real Cst "Stodola's ellipse coefficient";
         parameter Real W_fric = 1 "Power losses due to hydrodynamic friction (percent)";
         parameter Real eta_stato = 1.0 "Efficiency to account for cinetic losses (<= 1) (s.u.)";
         parameter ThermoSysPro_Units_SI_Area area_nz = 1 "Nozzle area";
         parameter Real eta_nz = 1.0 "Nozzle efficency (eta_nz < 1 - turbine with nozzle - eta_nz = 1 - turbine without nozzle)";
         parameter ThermoSysPro_Units_SI_MassFlowRate Qmax = 1 "Maximum mass flow through the turbine";
         parameter Real eta_is_nom = 0.89 "Nominal isentropic efficiency";
         parameter Real eta_is_min = 0.35 "Minimum isentropic efficiency";
         parameter Real a = -1.3889 "x^2 coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real b = 2.6944 "x coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real c = -0.5056 "Constant coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         parameter Integer mode_e = 0 "IF97 region before expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_s = 0 "IF97 region after expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_ps = 0 "IF97 region after isentropic expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real eta_is "Isentropic efficiency";
         Real eta_is_wet "Isentropic efficiency for wet steam";
         ThermoSysPro_Units_SI_Power W "Mechanical power produced by the turbine";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_SpecificEnthalpy His "Fluid specific enthalpy after isentropic expansion";
         ThermoSysPro_Units_SI_SpecificEnthalpy Hrs "Fluid specific enthalpy after the real expansion";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pe_start = 80100
         annotation(Dialog(tab = "Initial", group = "States"));
         ThermoSysPro_Units_SI_AbsolutePressure Pe(start = _Pe_start) "Pressure at the inlet";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Ps_start = 7055.9242141804
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Ps(start = _Ps_start, fixed=false) "Pressure at the outlet";
         ThermoSysPro_Units_SI_Temperature Te "Temperature at the inlet";
         ThermoSysPro_Units_SI_Temperature Ts "Temperature at the outlet";
         ThermoSysPro_Units_SI_Velocity Vs "Fluid velocity at the outlet";
         ThermoSysPro_Units_SI_Density rhos(displayUnit = "kg/m3") "Fluid density at the outlet";
         parameter Real _xm_start = 0.9365577998712189
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         Real xm(min = 0.0,start = _xm_start, fixed=false) "Average vapor mass fraction";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record pros_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           parameter ThermoSysPro_Units_SI_Density _d_start = 0.054754887914703565
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0,start = _d_start, fixed=false) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros_rec;
        public
         pros_rec pros annotation(Dialog);
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2612098.433622263
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce annotation(Dialog);
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record props_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(nominal = 1000000.0, min = -1000000.0, max = 100000000.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant pressure";
           ThermoSysPro_Units_xSI_DerDensityByEntropy ddsp "Derivative of the density wrt. specific entropy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddps "Derivative of the density wrt. pressure at constant specific entropy";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end props_rec;
        public
         props_rec props;
        protected
         record M_rec
           constant ThermoSysPro_Units_SI_Torque Ctr = 0 "Torque";
           constant ThermoSysPro_Units_SI_AngularVelocity w = 0 "Angular velocity";
         end M_rec;
        public
         M_rec M;
        protected
         record MechPower_rec
           Real signal;
         end MechPower_rec;
        public
         MechPower_rec MechPower;
        protected
         record pros1_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros1_rec;
        public
         pros1_rec pros1;
       end Turb_LP2_rec;
      public
       Turb_LP2_rec Turb_LP2 annotation(Dialog);
      protected
       record lumpedStraightPipe1_rec
         parameter ThermoSysPro_Units_SI_Length L = 1 "Pipe length";
         parameter ThermoSysPro_Units_SI_Diameter D = 0.8 "Pipe internal diameter";
         parameter Integer ntubes = 1 "Number of pipes in parallel";
         parameter Real lambda = 0.03 "Friction pressure loss coefficient (active if lambda_fixed=true)";
         constant Real rugosrel = 0.0001 "Pipe roughness (active if lambda_fixed=false)";
         parameter ThermoSysPro_Units_SI_Position z1 = 0 "Inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2 = 0 "Outlet altitude";
         constant Boolean lambda_fixed = false "true: lambda given by parameter - false: lambde computed using Idel'Cik correlation";
         constant Boolean inertia = false "true: momentum balance equation with inertia - false: without inertia";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real khi "Hydraulic pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaPf "Friction pressure loss";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Total pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_ReynoldsNumber Re "Reynolds number";
         constant ThermoSysPro_Units_SI_ReynoldsNumber Relim = 5600000.0 "Limit Reynolds number";
         constant Real lam = 0.011979797083255311 "Friction pressure loss coefficient";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_DynamicViscosity mu "Fluid dynamic viscosity";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           parameter ThermoSysPro_Units_SI_AbsolutePressure _P_start = 80083.75057360485
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_AbsolutePressure P(start = _P_start, fixed=false) "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2612098.433622263
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end lumpedStraightPipe1_rec;
      public
       lumpedStraightPipe1_rec lumpedStraightPipe1 annotation(Dialog);
      protected
       record singularPressureLoss4_rec
         parameter Real K = 1E-06 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pm_start = 80083.75029940932
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pm(start = _Pm_start, fixed=false) "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 317889.9999999945
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss4_rec;
      public
       singularPressureLoss4_rec singularPressureLoss4 annotation(Dialog);
      protected
       record volumeC1_rec
         parameter ThermoSysPro_Units_SI_Volume V = 1 "Volume";
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 500000 "Initial fluid pressure (active if dynamic_mass_balance=true and steady_state=false)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 138000 "Initial fluid specific enthalpy (active if steady_state=false)";
         constant Boolean dynamic_mass_balance = false "true: dynamic mass balance equation - false: static mass balance equation";
         constant Boolean steady_state = true "true: start from steady state - false: start from (P0, h0)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 163365.51520324877
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         constant ThermoSysPro_Units_SI_MassFlowRate BQ = 0 "Right hand side of the mass balance equation";
         ThermoSysPro_Units_SI_Power BH "Right hand side of the energybalance equation";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ce1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce1_rec;
        public
         Ce1_rec Ce1;
        protected
         record Ce2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce2_rec;
        public
         Ce2_rec Ce2;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record Ce3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce3_rec;
        public
         Ce3_rec Ce3;
       end volumeC1_rec;
      public
       volumeC1_rec volumeC1 annotation(Dialog);
      protected
       record singularPressureLoss5_rec
         parameter Real K = 0.0001 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss5_rec;
      public
       singularPressureLoss5_rec singularPressureLoss5 annotation(Dialog);
      protected
       record Bache_a_rec
         parameter ThermoSysPro_Units_SI_Volume V = 1 "Volume";
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 100000.0 "Initial fluid pressure (active if dynamic_mass_balance=true and steady_state=false)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 100000.0 "Initial fluid specific enthalpy (active if steady_state=false)";
         constant Boolean dynamic_mass_balance = false "true: dynamic mass balance equation - false: static mass balance equation";
         constant Boolean steady_state = true "true: start from steady state - false: start from (P0, h0)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 487506.04632433486
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         constant ThermoSysPro_Units_SI_MassFlowRate BQ = 0 "Right hand side of the mass balance equation";
         ThermoSysPro_Units_SI_Power BH "Right hand side of the energybalance equation";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ce2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce2_rec;
        public
         Ce2_rec Ce2;
        protected
         record Ce3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce3_rec;
        public
         Ce3_rec Ce3;
        protected
         record Ce4_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce4_rec;
        public
         Ce4_rec Ce4;
        protected
         record Ce1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce1_rec;
        public
         Ce1_rec Ce1;
        protected
         record Cs4_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs4_rec;
        public
         Cs4_rec Cs4;
        protected
         record Cs1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs1_rec;
        public
         Cs1_rec Cs1;
        protected
         record Cs2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs2_rec;
        public
         Cs2_rec Cs2;
        protected
         record Cs3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs3_rec;
        public
         Cs3_rec Cs3;
       end Bache_a_rec;
      public
       Bache_a_rec Bache_a annotation(Dialog);
      protected
       record SuperHeat_rec
         parameter Real Kc = 3547 "Friction pressure loss coefficient for the hot side";
         Real Kf "Friction pressure loss coefficient for the cold side";
         parameter ThermoSysPro_Units_SI_Position z1c = 0 "Hot inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2c = 0 "Hot outlet altitude";
         parameter ThermoSysPro_Units_SI_Position z1f = 0 "Cold inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2f = 0 "Cold outlet altitude";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         constant ThermoSysPro_Units_SI_Density p_rhoc = 0 "If > 0, fixed fluid density for the hot side";
         constant ThermoSysPro_Units_SI_Density p_rhof = 0 "If > 0, fixed fluid density for the cold side";
         parameter Integer modec = 0 "IF97 region of the water for the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer modecs = 0 "IF97 region of the water at the outlet of the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer modef = 0 "IF97 region of the water for the cold side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Power W "Power exchanged from the hot side to the cold side";
         ThermoSysPro_Units_SI_Temperature Tec "Fluid temperature at the inlet of the hot side";
         ThermoSysPro_Units_SI_Temperature Tsc "Fluid temperature at the outlet of the hot side";
         ThermoSysPro_Units_SI_Temperature Tef "Fluid temperature at the inlet of the cold side";
         ThermoSysPro_Units_SI_Temperature Tsf "Fluid temperature at the outlet of the cold side";
         parameter ThermoSysPro_Units_SI_PressureDifference _DPfc_start = 39406.755951177
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_PressureDifference DPfc(start = _DPfc_start, fixed=false) "Friction pressure loss in the hot side";
         parameter ThermoSysPro_Units_SI_PressureDifference _DPgc_start = 0
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_PressureDifference DPgc(start = _DPgc_start, fixed=false) "Gravity pressure loss in the hot side";
         ThermoSysPro_Units_SI_PressureDifference DPc "Total pressure loss in the hot side";
         ThermoSysPro_Units_SI_PressureDifference DPff "Friction pressure loss in the cold side";
         ThermoSysPro_Units_SI_PressureDifference DPgf "Gravity pressure loss in the cold side";
         parameter ThermoSysPro_Units_SI_PressureDifference _DPf_start = 80000
         annotation(Dialog(tab = "Initial", group = "States"));
         ThermoSysPro_Units_SI_PressureDifference DPf(start = _DPf_start) "Total pressure loss in the cold side";
         ThermoSysPro_Units_SI_Density rhoc "Density of the fluid in the hot side";
         ThermoSysPro_Units_SI_Density rhof "Density of the fluid in the cold side";
         ThermoSysPro_Units_SI_MassFlowRate Qc "Hot fluid mass flow rate";
         ThermoSysPro_Units_SI_MassFlowRate Qf "Cold fluid mass flow rate";
        protected
         record Ec_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ec_rec;
        public
         Ec_rec Ec annotation(Dialog);
        protected
         record Ef_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ef_rec;
        public
         Ef_rec Ef;
        protected
         record Sf_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2977121.8265122357
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sf_rec;
        public
         Sf_rec Sf annotation(Dialog);
        protected
         record Sc_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 1118785.4784898118
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sc_rec;
        public
         Sc_rec Sc annotation(Dialog);
        protected
         record proce_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proce_rec;
        public
         proce_rec proce;
        protected
         record procs_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end procs_rec;
        public
         procs_rec procs;
        protected
         record profe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end profe_rec;
        public
         profe_rec profe;
        protected
         record promf_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promf_rec;
        public
         promf_rec promf;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record promc_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promc_rec;
        public
         promc_rec promc;
        protected
         record profs_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end profs_rec;
        public
         profs_rec profs;
       end SuperHeat_rec;
      public
       SuperHeat_rec SuperHeat annotation(Dialog);
      protected
       record singularPressureLoss6_rec
         parameter Real K = 658 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pm_start = 694855.9578583837
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_AbsolutePressure Pm(start = _Pm_start, fixed=false) "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss6_rec;
      public
       singularPressureLoss6_rec singularPressureLoss6 annotation(Dialog);
      protected
       record Turb_LP1_b_rec
         Real Cst "Stodola's ellipse coefficient";
         parameter Real W_fric = 1 "Power losses due to hydrodynamic friction (percent)";
         parameter Real eta_stato = 1.0 "Efficiency to account for cinetic losses (<= 1) (s.u.)";
         parameter ThermoSysPro_Units_SI_Area area_nz = 1 "Nozzle area";
         parameter Real eta_nz = 1.0 "Nozzle efficency (eta_nz < 1 - turbine with nozzle - eta_nz = 1 - turbine without nozzle)";
         parameter ThermoSysPro_Units_SI_MassFlowRate Qmax = 1 "Maximum mass flow through the turbine";
         parameter Real eta_is_nom = 0.89 "Nominal isentropic efficiency";
         parameter Real eta_is_min = 0.35 "Minimum isentropic efficiency";
         parameter Real a = -1.3889 "x^2 coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real b = 2.6944 "x coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Real c = -0.5056 "Constant coefficient of the isentropic efficiency characteristics eta_is=f(Q/Qmax)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         parameter Integer mode_e = 0 "IF97 region before expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_s = 0 "IF97 region after expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_ps = 0 "IF97 region after isentropic expansion. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real eta_is "Isentropic efficiency";
         Real eta_is_wet "Isentropic efficiency for wet steam";
         ThermoSysPro_Units_SI_Power W "Mechanical power produced by the turbine";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_SpecificEnthalpy His "Fluid specific enthalpy after isentropic expansion";
         ThermoSysPro_Units_SI_SpecificEnthalpy Hrs "Fluid specific enthalpy after the real expansion";
         parameter ThermoSysPro_Units_SI_AbsolutePressure _Pe_start = 170000
         annotation(Dialog(tab = "Initial", group = "States"));
         ThermoSysPro_Units_SI_AbsolutePressure Pe(start = _Pe_start) "Pressure at the inlet";
         ThermoSysPro_Units_SI_AbsolutePressure Ps "Pressure at the outlet";
         ThermoSysPro_Units_SI_Temperature Te "Temperature at the inlet";
         ThermoSysPro_Units_SI_Temperature Ts "Temperature at the outlet";
         ThermoSysPro_Units_SI_Velocity Vs "Fluid velocity at the outlet";
         ThermoSysPro_Units_SI_Density rhos(displayUnit = "kg/m3") "Fluid density at the outlet";
         Real xm(min = 0.0) "Average vapor mass fraction";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record pros_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros_rec;
        public
         pros_rec pros;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2612098.4336222624
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs annotation(Dialog);
        protected
         record props_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(nominal = 1000000.0, min = -1000000.0, max = 100000000.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant pressure";
           ThermoSysPro_Units_xSI_DerDensityByEntropy ddsp "Derivative of the density wrt. specific entropy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddps "Derivative of the density wrt. pressure at constant specific entropy";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end props_rec;
        public
         props_rec props;
        protected
         record M_rec
           constant ThermoSysPro_Units_SI_Torque Ctr = 0 "Torque";
           constant ThermoSysPro_Units_SI_AngularVelocity w = 0 "Angular velocity";
         end M_rec;
        public
         M_rec M;
        protected
         record MechPower_rec
           Real signal;
         end MechPower_rec;
        public
         MechPower_rec MechPower;
        protected
         record pros1_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pros1_rec;
        public
         pros1_rec pros1;
       end Turb_LP1_b_rec;
      public
       Turb_LP1_b_rec Turb_LP1_b annotation(Dialog);
      protected
       record Extraction_ReheatBP_rec
         parameter Real alpha = 1 "Vapor mass fraction at the extraction/Vapor mass fraction at the inlet (0 <= alpha <= 1)";
         parameter Integer mode_e = 0 "IF97 region at the inlet. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real x_ex "Vapor mass fraction at the extraction outlet";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record Cex_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cex_rec;
        public
         Cex_rec Cex;
       end Extraction_ReheatBP_rec;
      public
       Extraction_ReheatBP_rec Extraction_ReheatBP annotation(Dialog);
      protected
       record splitter_SGoutlet_rec
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real alpha1 "Extraction coefficient for outlet 1 (<=1)";
         Real alpha2 "Extraction coefficient for outlet 2 (<=1)";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs3_rec;
        public
         Cs3_rec Cs3;
        protected
         record Cs1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs1_rec;
        public
         Cs1_rec Cs1;
        protected
         record Cs2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs2_rec;
        public
         Cs2_rec Cs2;
        protected
         record Ialpha1_rec
           constant Real signal = 0.3;
         end Ialpha1_rec;
        public
         Ialpha1_rec Ialpha1;
        protected
         record Ialpha2_rec
           constant Real signal = 0.3;
         end Ialpha2_rec;
        public
         Ialpha2_rec Ialpha2;
        protected
         record Oalpha1_rec
           Real signal;
         end Oalpha1_rec;
        public
         Oalpha1_rec Oalpha1;
        protected
         record Oalpha2_rec
           Real signal;
         end Oalpha2_rec;
        public
         Oalpha2_rec Oalpha2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end splitter_SGoutlet_rec;
      public
       splitter_SGoutlet_rec splitter_SGoutlet annotation(Dialog);
      protected
       record Bache_b_rec
         parameter ThermoSysPro_Units_SI_Volume V = 1 "Volume";
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 500000 "Initial fluid pressure (active if dynamic_mass_balance=true and steady_state=false)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 138000 "Initial fluid specific enthalpy (active if steady_state=false)";
         constant Boolean dynamic_mass_balance = false "true: dynamic mass balance equation - false: static mass balance equation";
         constant Boolean steady_state = true "true: start from steady state - false: start from (P0, h0)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 487506.9769380398
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         constant ThermoSysPro_Units_SI_MassFlowRate BQ = 0 "Right hand side of the mass balance equation";
         ThermoSysPro_Units_SI_Power BH "Right hand side of the energybalance equation";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ce1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce1_rec;
        public
         Ce1_rec Ce1;
        protected
         record Ce2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce2_rec;
        public
         Ce2_rec Ce2;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record Ce3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce3_rec;
        public
         Ce3_rec Ce3;
       end Bache_b_rec;
      public
       Bache_b_rec Bache_b annotation(Dialog);
      protected
       record Extraction_TurbBP1a_Outlet_rec
         parameter Real alpha = 1 "Vapor mass fraction at the extraction/Vapor mass fraction at the inlet (0 <= alpha <= 1)";
         parameter Integer mode_e = 0 "IF97 region at the inlet. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real x_ex "Vapor mass fraction at the extraction outlet";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record Cex_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           parameter ThermoSysPro_Units_SI_MassFlowRate _Q_start = -9.8731119139536E-33
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_MassFlowRate Q(start = _Q_start, fixed=false) "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cex_rec;
        public
         Cex_rec Cex annotation(Dialog);
       end Extraction_TurbBP1a_Outlet_rec;
      public
       Extraction_TurbBP1a_Outlet_rec Extraction_TurbBP1a_Outlet annotation(Dialog);
      protected
       record Reheat_BP_rec
         parameter Real lambdaE = 0 "Pressure loss coefficient on the water side";
         ThermoSysPro_Units_SI_Area SCondDes "Exchange surface for the condensation and deheating";
         parameter ThermoSysPro_Units_SI_CoefficientOfHeatTransfer KCond = 1500 "Heat transfer coefficient for the condensation";
         ThermoSysPro_Units_SI_Area SPurge "Drain surface - if > 0: with drain cooling";
         parameter ThermoSysPro_Units_SI_CoefficientOfHeatTransfer KPurge = 150 "Heat transfer coefficient for the drain cooling";
         parameter Integer mode_eeF = 0 "IF97 region at the inlet of the water side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_seF = 0 "IF97 region at the outlet of the water side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_evC = 0 "IF97 region at the inlet of the vapor side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_mF = 0 "IF97 region in the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_epC = 0 "IF97 region at the inlet of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_spC = 0 "IF97 region at the outlet of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_flash = 0 "IF97 region in the flash zone of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_SpecificEnthalpy HsateC(min = 0.0) "Saturation specific enthalpy of the water at the pressure of the vapor inlet";
         ThermoSysPro_Units_SI_SpecificEnthalpy HsatvC(min = 0.0) "Saturation specific enthalpy of the vapor at the pressure of the vapor inlet";
         parameter ThermoSysPro_Units_SI_Area _SDes_start = 1E-09
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_Area SDes(start = _SDes_start, fixed=false) "Heat exchange surface for deheating";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _HeiF_start = 170842.85406545497
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy HeiF(start = _HeiF_start, fixed=false) "Fluid specific enthalpy after drain cooling";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _HDesF_start = 370969.92329333595
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy HDesF(start = _HDesF_start, fixed=false) "Fluid specific enthalpy after deheating";
         ThermoSysPro_Units_SI_Temperature TeiF "Fluid temperature after drain cooling";
         ThermoSysPro_Units_SI_Temperature TsatC "Saturation temperature";
         ThermoSysPro_Units_SI_Power W "Total heat power transfered to the cooling water";
         ThermoSysPro_Units_SI_Power Wdes "Energy transfer during deheating";
         ThermoSysPro_Units_SI_Power Wcond "Energy transfer during condensation";
         ThermoSysPro_Units_SI_Power Wflash "Energy transfer during partial vaporisation in the drain";
         ThermoSysPro_Units_SI_Power Wpurge "Energy transfer during drain cooling";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _Hep_start = 391757.3376993904
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy Hep(start = _Hep_start, fixed=false) "Mixing specific enthalpy of the drain and the condensate";
         ThermoSysPro_Units_SI_Density rho "Average water density";
        protected
         record proeeF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proeeF_rec;
        public
         proeeF_rec proeeF;
        protected
         record proseF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proseF_rec;
        public
         proseF_rec proseF;
        protected
         record prospC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prospC_rec;
        public
         prospC_rec prospC;
        protected
         record Ee_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ee_rec;
        public
         Ee_rec Ee;
        protected
         record Se_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Se_rec;
        public
         Se_rec Se;
        protected
         record Ep_rec
           constant ThermoSysPro_Units_SI_AbsolutePressure P = 100000.0 "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 1E-06 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ep_rec;
        public
         Ep_rec Ep;
        protected
         record Sp_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sp_rec;
        public
         Sp_rec Sp;
        protected
         record Ev_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ev_rec;
        public
         Ev_rec Ev;
        protected
         record proevC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proevC_rec;
        public
         proevC_rec proevC;
        protected
         record lsatC_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(min = 0.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsatC_rec;
        public
         lsatC_rec lsatC;
        protected
         record vsatC_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(min = 0.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsatC_rec;
        public
         vsatC_rec vsatC;
        protected
         record promeF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promeF_rec;
        public
         promeF_rec promeF;
        protected
         record prodesmC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesmC_rec;
        public
         prodesmC_rec prodesmC;
        protected
         record promcF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promcF_rec;
        public
         promcF_rec promcF;
        protected
         record prodesF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesF_rec;
        public
         prodesF_rec prodesF;
        protected
         record prodesmF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesmF_rec;
        public
         prodesmF_rec prodesmF;
        protected
         record prosp_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prosp_rec;
        public
         prosp_rec prosp;
        protected
         record prompC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prompC_rec;
        public
         prompC_rec prompC;
        protected
         record prompF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prompF_rec;
        public
         prompF_rec prompF;
        protected
         record proecF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proecF_rec;
        public
         proecF_rec proecF;
        protected
         record flashepC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end flashepC_rec;
        public
         flashepC_rec flashepC;
       end Reheat_BP_rec;
      public
       Reheat_BP_rec Reheat_BP annotation(Dialog);
      protected
       record invSingularPressureLoss1_rec
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real K "Pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end invSingularPressureLoss1_rec;
      public
       invSingularPressureLoss1_rec invSingularPressureLoss1 annotation(Dialog);
      protected
       record ReHeat_HP_rec
         parameter Real lambdaE = 0 "Pressure loss coefficient on the water side";
         ThermoSysPro_Units_SI_Area SCondDes "Exchange surface for the condensation and deheating";
         parameter ThermoSysPro_Units_SI_CoefficientOfHeatTransfer KCond = 1500 "Heat transfer coefficient for the condensation";
         ThermoSysPro_Units_SI_Area SPurge "Drain surface - if > 0: with drain cooling";
         parameter ThermoSysPro_Units_SI_CoefficientOfHeatTransfer KPurge = 150 "Heat transfer coefficient for the drain cooling";
         parameter Integer mode_eeF = 0 "IF97 region at the inlet of the water side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_seF = 0 "IF97 region at the outlet of the water side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_evC = 0 "IF97 region at the inlet of the vapor side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_mF = 0 "IF97 region in the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_epC = 0 "IF97 region at the inlet of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_spC = 0 "IF97 region at the outlet of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer mode_flash = 0 "IF97 region in the flash zone of the drain. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_SpecificEnthalpy HsateC(min = 0.0) "Saturation specific enthalpy of the water at the pressure of the vapor inlet";
         ThermoSysPro_Units_SI_SpecificEnthalpy HsatvC(min = 0.0) "Saturation specific enthalpy of the vapor at the pressure of the vapor inlet";
         parameter ThermoSysPro_Units_SI_Area _SDes_start = 1E-09
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_Area SDes(start = _SDes_start, fixed=false) "Heat exchange surface for deheating";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _HeiF_start = 493594.194870379
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy HeiF(start = _HeiF_start, fixed=false) "Fluid specific enthalpy after drain cooling";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _HDesF_start = 690925.0264666991
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy HDesF(start = _HDesF_start, fixed=false) "Fluid specific enthalpy after deheating";
         ThermoSysPro_Units_SI_Temperature TeiF "Fluid temperature after drain cooling";
         ThermoSysPro_Units_SI_Temperature TsatC "Saturation temperature";
         ThermoSysPro_Units_SI_Power W "Total heat power transfered to the cooling water";
         ThermoSysPro_Units_SI_Power Wdes "Energy transfer during deheating";
         ThermoSysPro_Units_SI_Power Wcond "Energy transfer during condensation";
         ThermoSysPro_Units_SI_Power Wflash "Energy transfer during partial vaporisation in the drain";
         ThermoSysPro_Units_SI_Power Wpurge "Energy transfer during drain cooling";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _Hep_start = 710469.7699779422
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy Hep(start = _Hep_start, fixed=false) "Mixing specific enthalpy of the drain and the condensate";
         ThermoSysPro_Units_SI_Density rho "Average water density";
        protected
         record proeeF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proeeF_rec;
        public
         proeeF_rec proeeF;
        protected
         record proseF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proseF_rec;
        public
         proseF_rec proseF;
        protected
         record prospC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prospC_rec;
        public
         prospC_rec prospC;
        protected
         record Ee_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ee_rec;
        public
         Ee_rec Ee;
        protected
         record Se_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Se_rec;
        public
         Se_rec Se;
        protected
         record Ep_rec
           constant ThermoSysPro_Units_SI_AbsolutePressure P = 100000.0 "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 1E-06 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ep_rec;
        public
         Ep_rec Ep;
        protected
         record Sp_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sp_rec;
        public
         Sp_rec Sp;
        protected
         record Ev_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ev_rec;
        public
         Ev_rec Ev;
        protected
         record proevC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proevC_rec;
        public
         proevC_rec proevC;
        protected
         record lsatC_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(min = 0.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsatC_rec;
        public
         lsatC_rec lsatC;
        protected
         record vsatC_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h(min = 0.0) "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsatC_rec;
        public
         vsatC_rec vsatC;
        protected
         record promeF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           parameter ThermoSysPro_Units_SI_Density _d_start = 928.6413914051141
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0,start = _d_start, fixed=false) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promeF_rec;
        public
         promeF_rec promeF annotation(Dialog);
        protected
         record prodesmC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesmC_rec;
        public
         prodesmC_rec prodesmC;
        protected
         record promcF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promcF_rec;
        public
         promcF_rec promcF;
        protected
         record prodesF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesF_rec;
        public
         prodesF_rec prodesF;
        protected
         record prodesmF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prodesmF_rec;
        public
         prodesmF_rec prodesmF;
        protected
         record prosp_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prosp_rec;
        public
         prosp_rec prosp;
        protected
         record prompC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prompC_rec;
        public
         prompC_rec prompC;
        protected
         record prompF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end prompF_rec;
        public
         prompF_rec prompF;
        protected
         record proecF_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proecF_rec;
        public
         proecF_rec proecF;
        protected
         record flashepC_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end flashepC_rec;
        public
         flashepC_rec flashepC;
       end ReHeat_HP_rec;
      public
       ReHeat_HP_rec ReHeat_HP annotation(Dialog);
      protected
       record invSingularPressureLoss2_rec
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real K "Pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end invSingularPressureLoss2_rec;
      public
       invSingularPressureLoss2_rec invSingularPressureLoss2 annotation(Dialog);
      protected
       record Rend_Cycle_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Rend_Cycle_rec;
      public
       Rend_Cycle_rec Rend_Cycle annotation(Dialog);
      protected
       record Pw_reseau_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Pw_reseau_rec;
      public
       Pw_reseau_rec Pw_reseau annotation(Dialog);
      protected
       record Pw_COG_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Pw_COG_rec;
      public
       Pw_COG_rec Pw_COG annotation(Dialog);
      protected
       record Temp_COG_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Temp_COG_rec;
      public
       Temp_COG_rec Temp_COG annotation(Dialog);
      protected
       record Heat_Power_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Heat_Power_rec;
      public
       Heat_Power_rec Heat_Power annotation(Dialog);
      protected
       record Temp_inletGV_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Temp_inletGV_rec;
      public
       Temp_inletGV_rec Temp_inletGV annotation(Dialog);
      protected
       record Pw_GV_rec
         parameter Integer significantDigits(min = 1) = 4 "Number of significant digits to be shown";
         Modelica_Blocks_Interfaces_RealOutput showNumber;
       end Pw_GV_rec;
      public
       Pw_GV_rec Pw_GV annotation(Dialog);
      protected
       record volumeC2_rec
         parameter ThermoSysPro_Units_SI_Volume V = 1 "Volume";
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 5000 "Initial fluid pressure (active if dynamic_mass_balance=true and steady_state=false)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 138000 "Initial fluid specific enthalpy (active if steady_state=false)";
         constant Boolean dynamic_mass_balance = false "true: dynamic mass balance equation - false: static mass balance equation";
         constant Boolean steady_state = true "true: start from steady state - false: start from (P0, h0)";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2142425.9091542354
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Fluid specific enthalpy";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         constant ThermoSysPro_Units_SI_MassFlowRate BQ = 0 "Right hand side of the mass balance equation";
         ThermoSysPro_Units_SI_Power BH "Right hand side of the energybalance equation";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ce1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce1_rec;
        public
         Ce1_rec Ce1;
        protected
         record Ce2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           constant ThermoSysPro_Units_SI_MassFlowRate Q = 0 "Mass flow rate of the fluid crossing the boundary of the control volume";
           constant ThermoSysPro_Units_SI_SpecificEnthalpy h = 100000.0 "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce2_rec;
        public
         Ce2_rec Ce2;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record Ce3_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce3_rec;
        public
         Ce3_rec Ce3;
       end volumeC2_rec;
      public
       volumeC2_rec volumeC2 annotation(Dialog);
      protected
       record singularPressureLoss1_rec
         parameter Real K = 0.0001 "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss1_rec;
      public
       singularPressureLoss1_rec singularPressureLoss1 annotation(Dialog);
      protected
       record Valve_superheat_rec
         ThermoSysPro_Units_xSI_Cv Cvmax "Maximum CV (active if mode_caract=0)";
         constant Real 'caract[1,1]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[1,2]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[2,1]' = 1.0 "Position vs. Cv characteristics (active if mode_caract=1)";
         Real 'caract[2,2]' "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Integer mode_caract = 0 "0:linear characteristics - 1:characteristics is given by caract[]";
         parameter Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation (active if mode_caract=1)";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant Integer option_rho_water = 1 "1: using (deltaP*Cv^2=A.Q^2/rho^2) - 2: using (deltaP*Cv^2=A.Q^2/(rho*rho_15)); with rho_15 is the density of the water at 15.5556 °C)";
         constant ThermoSysPro_Units_SI_Density p_rho(displayUnit = "kg/m3") = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_xSI_Cv Cv "Cv";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_Density rho(displayUnit = "kg/m3") "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ouv_rec
           Real signal;
         end Ouv_rec;
        public
         Ouv_rec Ouv;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Valve_superheat_rec;
      public
       Valve_superheat_rec Valve_superheat annotation(Dialog);
      protected
       record Tin_Turb_LP1a_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Tin_Turb_LP1a_rec;
      public
       Tin_Turb_LP1a_rec Tin_Turb_LP1a annotation(Dialog);
      protected
       record feedback5_rec
        protected
         record u2_rec
           Real signal;
         end u2_rec;
        public
         u2_rec u2;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record u1_rec
           Real signal;
         end u1_rec;
        public
         u1_rec u1;
       end feedback5_rec;
      public
       feedback5_rec feedback5;
      protected
       record pI5_rec
         parameter Real k = 1.2 "Gain";
         parameter Real Ti = 1 "Constante de temps (s)";
         parameter Real ureset0 = 0 "Valeur de la sortie sur reset (si ureset non connecté)";
         constant Boolean permanent = true "Calcul du permanent";
         Real x;
        protected
         record u_rec
           Real signal;
         end u_rec;
        public
         u_rec u;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record ureset_rec
           Real signal;
         end ureset_rec;
        public
         ureset_rec ureset;
        protected
         record reset_rec
           constant Boolean signal = false;
         end reset_rec;
        public
         reset_rec reset;
       end pI5_rec;
      public
       pI5_rec pI5 annotation(Dialog);
      protected
       record rampe3_rec
         parameter Real Starttime = 20 "Instant de départ de la rampe (s)";
         parameter Real Duration = 1000 "Durée de la rampe (s)";
         parameter Real Initialvalue = 533.5899999999999 "Valeur initiale de la sortie";
         parameter Real Finalvalue = 533.5899999999999 "Valeur finale de la sortie";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end rampe3_rec;
      public
       rampe3_rec rampe3 annotation(Dialog);
      protected
       record Pipe_SGs_rec
         parameter ThermoSysPro_Units_SI_Length L = 20 "Pipe length";
         parameter ThermoSysPro_Units_SI_Diameter D = 0.5 "Pipe internal diameter";
         parameter Integer ntubes = 1 "Number of pipes in parallel";
         parameter Real lambda = 0.03 "Friction pressure loss coefficient (active if lambda_fixed=true)";
         constant Real rugosrel = 0.0001 "Pipe roughness (active if lambda_fixed=false)";
         parameter ThermoSysPro_Units_SI_Position z1 = 0 "Inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2 = 0 "Outlet altitude";
         constant Boolean lambda_fixed = false "true: lambda given by parameter - false: lambde computed using Idel'Cik correlation";
         constant Boolean inertia = false "true: momentum balance equation with inertia - false: without inertia";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real khi "Hydraulic pressure loss coefficient";
         ThermoSysPro_Units_SI_PressureDifference deltaPf "Friction pressure loss";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Total pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_ReynoldsNumber Re "Reynolds number";
         constant ThermoSysPro_Units_SI_ReynoldsNumber Relim = 5600000.0 "Limit Reynolds number";
         constant Real lam = 0.011979797083255311 "Friction pressure loss coefficient";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_DynamicViscosity mu "Fluid dynamic viscosity";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           parameter ThermoSysPro_Units_SI_AbsolutePressure _P_start = 4468560.778186667
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_AbsolutePressure P(start = _P_start, fixed=false) "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end Pipe_SGs_rec;
      public
       Pipe_SGs_rec Pipe_SGs annotation(Dialog);
      protected
       record singularPressureLoss_SG_Out_rec
         Real K "Pressure loss coefficient";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant ThermoSysPro_Units_SI_Density p_rho = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Density rho "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Average fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record C1_rec
           parameter ThermoSysPro_Units_SI_AbsolutePressure _P_start = 4500000
           annotation(Dialog(tab = "Initial", group = "States"));
           ThermoSysPro_Units_SI_AbsolutePressure P(start = _P_start) "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2944110.0
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
       end singularPressureLoss_SG_Out_rec;
      public
       singularPressureLoss_SG_Out_rec singularPressureLoss_SG_Out annotation(Dialog);
      protected
       record Pout_SG_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Pout_SG_rec;
      public
       Pout_SG_rec Pout_SG;
      protected
       record Extraction_TurbHP_Oulet_rec
         parameter Real alpha = 1 "Vapor mass fraction at the extraction/Vapor mass fraction at the inlet (0 <= alpha <= 1)";
         parameter Integer mode_e = 0 "IF97 region at the inlet. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Real x_ex "Vapor mass fraction at the extraction outlet";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record proe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proe_rec;
        public
         proe_rec proe;
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record Cex_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h(start = _h_start, fixed=false) "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cex_rec;
        public
         Cex_rec Cex annotation(Dialog);
       end Extraction_TurbHP_Oulet_rec;
      public
       Extraction_TurbHP_Oulet_rec Extraction_TurbHP_Oulet annotation(Dialog);
      protected
       record Ideal_BOP2HeatNetWork_HX_rec
         parameter Real Kc = 10 "Friction pressure loss coefficient for the hot side";
         parameter Real Kf = 5885 "Friction pressure loss coefficient for the cold side";
         parameter ThermoSysPro_Units_SI_Position z1c = 0 "Hot inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2c = 0 "Hot outlet altitude";
         parameter ThermoSysPro_Units_SI_Position z1f = 0 "Cold inlet altitude";
         parameter ThermoSysPro_Units_SI_Position z2f = 0 "Cold outlet altitude";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         constant ThermoSysPro_Units_SI_Density p_rhoc = 0 "If > 0, fixed fluid density for the hot side";
         constant ThermoSysPro_Units_SI_Density p_rhof = 0 "If > 0, fixed fluid density for the cold side";
         parameter Integer modec = 0 "IF97 region of the water for the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer modecs = 0 "IF97 region of the water at the outlet of the hot side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         parameter Integer modef = 0 "IF97 region of the water for the cold side. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_Power W "Power exchanged from the hot side to the cold side";
         ThermoSysPro_Units_SI_Temperature Tec "Fluid temperature at the inlet of the hot side";
         ThermoSysPro_Units_SI_Temperature Tsc "Fluid temperature at the outlet of the hot side";
         ThermoSysPro_Units_SI_Temperature Tef "Fluid temperature at the inlet of the cold side";
         ThermoSysPro_Units_SI_Temperature Tsf "Fluid temperature at the outlet of the cold side";
         ThermoSysPro_Units_SI_PressureDifference DPfc "Friction pressure loss in the hot side";
         ThermoSysPro_Units_SI_PressureDifference DPgc "Gravity pressure loss in the hot side";
         parameter ThermoSysPro_Units_SI_PressureDifference _DPc_start = 0.0011978797265468
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_PressureDifference DPc(start = _DPc_start, fixed=false) "Total pressure loss in the hot side";
         ThermoSysPro_Units_SI_PressureDifference DPff "Friction pressure loss in the cold side";
         ThermoSysPro_Units_SI_PressureDifference DPgf "Gravity pressure loss in the cold side";
         parameter ThermoSysPro_Units_SI_PressureDifference _DPf_start = 0.059666399287179
         annotation(Dialog(tab = "Initial", group = "Approximate"));
         ThermoSysPro_Units_SI_PressureDifference DPf(start = _DPf_start, fixed=false) "Total pressure loss in the cold side";
         ThermoSysPro_Units_SI_Density rhoc "Density of the fluid in the hot side";
         ThermoSysPro_Units_SI_Density rhof "Density of the fluid in the cold side";
         ThermoSysPro_Units_SI_MassFlowRate Qc "Hot fluid mass flow rate";
         ThermoSysPro_Units_SI_MassFlowRate Qf "Cold fluid mass flow rate";
        protected
         record Ec_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ec_rec;
        public
         Ec_rec Ec;
        protected
         record Ef_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(nominal = 500000.0, min = -10000000000.0, max = 10000000000.0) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ef_rec;
        public
         Ef_rec Ef;
        protected
         record Sf_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sf_rec;
        public
         Sf_rec Sf;
        protected
         record Sc_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 710502.0457445232
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Sc_rec;
        public
         Sc_rec Sc annotation(Dialog);
        protected
         record proce_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end proce_rec;
        public
         proce_rec proce;
        protected
         record procs_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end procs_rec;
        public
         procs_rec procs;
        protected
         record profe_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end profe_rec;
        public
         profe_rec profe;
        protected
         record promf_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promf_rec;
        public
         promf_rec promf;
        protected
         record lsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end lsat_rec;
        public
         lsat_rec lsat;
        protected
         record vsat_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Pressure";
           ThermoSysPro_Units_SI_Temperature T "Temperature";
           ThermoSysPro_Units_SI_Density rho "Density";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp "Specific heat capacity at constant pressure";
           Real pt "Derivative of pressure wrt. temperature";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cv "Specific heat capacity at constant volume";
         end vsat_rec;
        public
         vsat_rec vsat;
        protected
         record promc_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end promc_rec;
        public
         promc_rec promc;
        protected
         record profs_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end profs_rec;
        public
         profs_rec profs;
       end Ideal_BOP2HeatNetWork_HX_rec;
      public
       Ideal_BOP2HeatNetWork_HX_rec Ideal_BOP2HeatNetWork_HX annotation(Dialog);
      protected
       record Valve_Hybrid_IP_rec
         parameter ThermoSysPro_Units_xSI_Cv Cvmax = 1000 "Maximum CV (active if mode_caract=0)";
         constant Real 'caract[1,1]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[1,2]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[2,1]' = 1.0 "Position vs. Cv characteristics (active if mode_caract=1)";
         Real 'caract[2,2]' "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Integer mode_caract = 0 "0:linear characteristics - 1:characteristics is given by caract[]";
         parameter Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation (active if mode_caract=1)";
         constant Boolean continuous_flow_reversal = true "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant Integer option_rho_water = 1 "1: using (deltaP*Cv^2=A.Q^2/rho^2) - 2: using (deltaP*Cv^2=A.Q^2/(rho*rho_15)); with rho_15 is the density of the water at 15.5556 °C)";
         constant ThermoSysPro_Units_SI_Density p_rho(displayUnit = "kg/m3") = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_xSI_Cv Cv "Cv";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_Density rho(displayUnit = "kg/m3") "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ouv_rec
           Real signal;
         end Ouv_rec;
        public
         Ouv_rec Ouv;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 710502.0457445232
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 710502.0457445232
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2 annotation(Dialog);
       end Valve_Hybrid_IP_rec;
      public
       Valve_Hybrid_IP_rec Valve_Hybrid_IP annotation(Dialog);
      protected
       record feedback6_rec
        protected
         record u2_rec
           Real signal;
         end u2_rec;
        public
         u2_rec u2;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record u1_rec
           Real signal;
         end u1_rec;
        public
         u1_rec u1;
       end feedback6_rec;
      public
       feedback6_rec feedback6;
      protected
       record pI6_rec
         parameter Real k = 1 "Gain";
         parameter Real Ti = 1 "Constante de temps (s)";
         parameter Real ureset0 = 0 "Valeur de la sortie sur reset (si ureset non connecté)";
         constant Boolean permanent = true "Calcul du permanent";
         Real x;
        protected
         record u_rec
           Real signal;
         end u_rec;
        public
         u_rec u;
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
        protected
         record ureset_rec
           Real signal;
         end ureset_rec;
        public
         ureset_rec ureset;
        protected
         record reset_rec
           constant Boolean signal = false;
         end reset_rec;
        public
         reset_rec reset;
       end pI6_rec;
      public
       pI6_rec pI6 annotation(Dialog);
      protected
       record Vapor_MassFlowRamp_extracted4Cogeneration_rec
         parameter Real Starttime = 1000 "Instant de départ de la rampe (s)";
         parameter Real Duration = 100 "Durée de la rampe (s)";
         parameter Real Initialvalue = 0.001 "Valeur initiale de la sortie";
         parameter Real Finalvalue = 28 "Valeur finale de la sortie";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end Vapor_MassFlowRamp_extracted4Cogeneration_rec;
      public
       Vapor_MassFlowRamp_extracted4Cogeneration_rec Vapor_MassFlowRamp_extracted4Cogeneration annotation(Dialog);
      protected
       record Q_Extract_Hybrid_IP_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Q_Extract_Hybrid_IP_rec;
      public
       Q_Extract_Hybrid_IP_rec Q_Extract_Hybrid_IP;
      protected
       record T_Outlet_BOP_HX_HeatNetWork_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end T_Outlet_BOP_HX_HeatNetWork_rec;
      public
       T_Outlet_BOP_HX_HeatNetWork_rec T_Outlet_BOP_HX_HeatNetWork annotation(Dialog);
      protected
       record Pout_Turb_HP_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           parameter ThermoSysPro_Units_SI_SpecificEnthalpy _h_vol_start = 2639678.799598451
           annotation(Dialog(tab = "Initial", group = "Approximate"));
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(start = _h_vol_start, fixed=false) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1 annotation(Dialog);
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Pout_Turb_HP_rec;
      public
       Pout_Turb_HP_rec Pout_Turb_HP annotation(Dialog);
      protected
       record Tout_Turb_HP_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Tout_Turb_HP_rec;
      public
       Tout_Turb_HP_rec Tout_Turb_HP annotation(Dialog);
      protected
       record Valve_superheat1_rec
         ThermoSysPro_Units_xSI_Cv Cvmax "Maximum CV (active if mode_caract=0)";
         constant Real 'caract[1,1]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[1,2]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[2,1]' = 1.0 "Position vs. Cv characteristics (active if mode_caract=1)";
         Real 'caract[2,2]' "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Integer mode_caract = 0 "0:linear characteristics - 1:characteristics is given by caract[]";
         parameter Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation (active if mode_caract=1)";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant Integer option_rho_water = 1 "1: using (deltaP*Cv^2=A.Q^2/rho^2) - 2: using (deltaP*Cv^2=A.Q^2/(rho*rho_15)); with rho_15 is the density of the water at 15.5556 °C)";
         constant ThermoSysPro_Units_SI_Density p_rho(displayUnit = "kg/m3") = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_xSI_Cv Cv "Cv";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_Density rho(displayUnit = "kg/m3") "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ouv_rec
           Real signal;
         end Ouv_rec;
        public
         Ouv_rec Ouv;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Valve_superheat1_rec;
      public
       Valve_superheat1_rec Valve_superheat1 annotation(Dialog);
      protected
       record rampe2_rec
         parameter Real Starttime = 100 "Instant de départ de la rampe (s)";
         parameter Real Duration = 900 "Durée de la rampe (s)";
         parameter Real Initialvalue = 0 "Valeur initiale de la sortie";
         parameter Real Finalvalue = 0 "Valeur finale de la sortie";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end rampe2_rec;
      public
       rampe2_rec rampe2 annotation(Dialog);
      protected
       record sinkP_rec
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 100000 "Sink pressure";
         parameter ThermoSysPro_Units_SI_Temperature T0 = 290 "Sink temperature (active if option_temperature=1)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 100000 "Sink specific enthalpy (active if option_temperature=2)";
         constant Integer option_temperature = 1 "1:temperature fixed - 2:specific enthalpy fixed";
         parameter Integer mode = 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record IPressure_rec
           Real signal;
         end IPressure_rec;
        public
         IPressure_rec IPressure;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record ITemperature_rec
           Real signal;
         end ITemperature_rec;
        public
         ITemperature_rec ITemperature;
       end sinkP_rec;
      public
       sinkP_rec sinkP annotation(Dialog);
      protected
       record Valve_superheat2_rec
         ThermoSysPro_Units_xSI_Cv Cvmax "Maximum CV (active if mode_caract=0)";
         constant Real 'caract[1,1]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[1,2]' = 0 "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Real 'caract[2,1]' = 1.0 "Position vs. Cv characteristics (active if mode_caract=1)";
         Real 'caract[2,2]' "Position vs. Cv characteristics (active if mode_caract=1)";
         constant Integer mode_caract = 0 "0:linear characteristics - 1:characteristics is given by caract[]";
         parameter Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation (active if mode_caract=1)";
         constant Boolean continuous_flow_reversal = false "true: continuous flow reversal - false: discontinuous flow reversal";
         parameter Integer fluid = 1 "1: water/steam - 2: C3H3F5";
         constant Integer option_rho_water = 1 "1: using (deltaP*Cv^2=A.Q^2/rho^2) - 2: using (deltaP*Cv^2=A.Q^2/(rho*rho_15)); with rho_15 is the density of the water at 15.5556 °C)";
         constant ThermoSysPro_Units_SI_Density p_rho(displayUnit = "kg/m3") = 0 "If > 0, fixed fluid density";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_xSI_Cv Cv "Cv";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_PressureDifference deltaP "Singular pressure loss";
         ThermoSysPro_Units_SI_Density rho(displayUnit = "kg/m3") "Fluid density";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure Pm "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Ouv_rec
           Real signal;
         end Ouv_rec;
        public
         Ouv_rec Ouv;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end Valve_superheat2_rec;
      public
       Valve_superheat2_rec Valve_superheat2 annotation(Dialog);
      protected
       record sinkP2_rec
         parameter ThermoSysPro_Units_SI_AbsolutePressure P0 = 100000 "Sink pressure";
         parameter ThermoSysPro_Units_SI_Temperature T0 = 290 "Sink temperature (active if option_temperature=1)";
         parameter ThermoSysPro_Units_SI_SpecificEnthalpy h0 = 100000 "Sink specific enthalpy (active if option_temperature=2)";
         constant Integer option_temperature = 1 "1:temperature fixed - 2:specific enthalpy fixed";
         parameter Integer mode = 1 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record IPressure_rec
           Real signal;
         end IPressure_rec;
        public
         IPressure_rec IPressure;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record ITemperature_rec
           Real signal;
         end ITemperature_rec;
        public
         ITemperature_rec ITemperature;
       end sinkP2_rec;
      public
       sinkP2_rec sinkP2 annotation(Dialog);
      protected
       record rampe4_rec
         parameter Real Starttime = 100 "Instant de départ de la rampe (s)";
         parameter Real Duration = 900 "Durée de la rampe (s)";
         parameter Real Initialvalue = 0 "Valeur initiale de la sortie";
         parameter Real Finalvalue = 0 "Valeur finale de la sortie";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end rampe4_rec;
      public
       rampe4_rec rampe4 annotation(Dialog);
      protected
       record Qin_set_rec
         Modelica_Blocks_Interfaces_RealInput u;
        protected
         record outputReal_rec
           Real signal;
         end outputReal_rec;
        public
         outputReal_rec outputReal;
       end Qin_set_rec;
      public
       Qin_set_rec Qin_set;
      protected
       record Hin_set_rec
         Modelica_Blocks_Interfaces_RealInput u;
        protected
         record outputReal_rec
           Real signal;
         end outputReal_rec;
        public
         outputReal_rec outputReal;
       end Hin_set_rec;
      public
       Hin_set_rec Hin_set;
      protected
       record sourceQ_4FMU_rec
         parameter Modelica.Units.SI.MassFlowRate Q0 = 100 "Mass flow (active if IMassFlow connector is not connected)";
         parameter Modelica.Units.SI.SpecificEnthalpy h0 = 100000 "Fluid specific enthalpy (active if IEnthalpy connector is not connected)";
         Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
         Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
         Modelica.Units.SI.SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record IMassFlow_rec
           Real signal;
         end IMassFlow_rec;
        public
         IMassFlow_rec IMassFlow;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record OPressure_rec
           Real signal;
         end OPressure_rec;
        public
         OPressure_rec OPressure;
       end sourceQ_4FMU_rec;
      public
       sourceQ_4FMU_rec sourceQ_4FMU annotation(Dialog);
      protected
       record Pin_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y;
        protected
         record inputReal_rec
           Real signal;
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Pin_calc_rec;
      public
       Pin_calc_rec Pin_calc;
      protected
       record sinkP_4FMU_rec
         parameter Modelica.Units.SI.AbsolutePressure P0 = 100000 "Sink pressure";
         parameter Modelica.Units.SI.Temperature T0 = 290 "Sink temperature (active if option_temperature=1)";
         parameter Modelica.Units.SI.SpecificEnthalpy h0 = 691000 "Sink specific enthalpy (active if option_temperature=2)";
         parameter Integer option_temperature = 1 "1:temperature fixed - 2:specific enthalpy fixed";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
         Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
         Modelica.Units.SI.Temperature T "Fluid temperature";
         Modelica.Units.SI.SpecificEnthalpy h "Fluid enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record IPressure_rec
           Real signal;
         end IPressure_rec;
        public
         IPressure_rec IPressure;
        protected
         record ISpecificEnthalpy_rec
           Real signal;
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record OSpecificEnthalpy_rec
           Real signal;
         end OSpecificEnthalpy_rec;
        public
         OSpecificEnthalpy_rec OSpecificEnthalpy;
        protected
         record OFlowRate_rec
           Real signal;
         end OFlowRate_rec;
        public
         OFlowRate_rec OFlowRate;
       end sinkP_4FMU_rec;
      public
       sinkP_4FMU_rec sinkP_4FMU annotation(Dialog);
      protected
       record Qout_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y;
        protected
         record inputReal_rec
           Real signal;
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Qout_calc_rec;
      public
       Qout_calc_rec Qout_calc;
      protected
       record Hout_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y;
        protected
         record inputReal_rec
           Real signal;
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Hout_calc_rec;
      public
       Hout_calc_rec Hout_calc;
      protected
       record Pout_calc_rec
         Modelica_Blocks_Interfaces_RealInput u;
        protected
         record outputReal_rec
           Real signal;
         end outputReal_rec;
        public
         outputReal_rec outputReal;
       end Pout_calc_rec;
      public
       Pout_calc_rec Pout_calc;
      protected
       record T_2HeatNetWork_rec
         constant Boolean continuous_flow_reversal = false "true : continuous flow reversal - false : discontinuous flow reversal";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate";
         ThermoSysPro_Units_SI_Temperature T "Fluid temperature";
         ThermoSysPro_Units_SI_AbsolutePressure P "Fluid average pressure";
         ThermoSysPro_Units_SI_SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record Measure_rec
           Real signal;
         end Measure_rec;
        public
         Measure_rec Measure;
        protected
         record C1_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C1_rec;
        public
         C1_rec C1;
        protected
         record C2_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol(nominal = 500000.0, min = -10000000000.0, max = 10000000000.0) "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C2_rec;
        public
         C2_rec C2;
       end T_2HeatNetWork_rec;
      public
       T_2HeatNetWork_rec T_2HeatNetWork annotation(Dialog);
      protected
       record fluid2TSPro1_rec
        protected
         record port_a_rec
           Real m_flow(unit = "kg/s", quantity = "MassFlowRate.WaterIF97", min = -100000.0, max = 100000.0) "Mass flow rate from the connection point into the component";
           Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Thermodynamic pressure in the connection point";
           Real h_outflow(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific thermodynamic enthalpy close to the connection point if m_flow < 0";
         end port_a_rec;
        public
         port_a_rec port_a;
        protected
         record steam_outlet_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end steam_outlet_rec;
        public
         steam_outlet_rec steam_outlet;
       end fluid2TSPro1_rec;
      public
       fluid2TSPro1_rec fluid2TSPro1;
      protected
       record InletFlow_FromHeatNetwork_rec
         constant Integer nPorts = 1 "Number of ports";
         parameter Real m_flow(unit = "kg/s", quantity = "MassFlowRate.WaterIF97", min = -100000.0, max = 100000.0) = 100 "Fixed mass flow rate going out of the fluid port";
         parameter Real h(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) = 135000 "Fixed value of specific enthalpy";
         parameter Real 'X[1]'(nominal = 0.1, quantity = "MassFraction", min = 0.0, max = 1.0) = 1.0 "Fixed value of composition";
         Modelica_Blocks_Interfaces_RealInput m_flow_in(unit = "kg/s") "Prescribed mass flow rate";
        protected
         record medium_rec
           Real p(unit = "Pa", displayUnit = "bar", nominal = 100000.0, quantity = "Pressure", min = 0.0) "Absolute pressure of medium";
           Real h(unit = "J/kg", quantity = "SpecificEnergy") "Specific enthalpy of medium";
           Real d(unit = "kg/m3", displayUnit = "g/cm3", nominal = 500.0, quantity = "Density", min = 0.0, max = 100000.0) "Density of medium";
           Real T(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Temperature of medium";
           constant Real 'X[1]'(nominal = 0.1, quantity = "MassFraction", min = 0.0, max = 1.0) = 1.0 "Mass fractions (= (component mass)/total mass  m_i/m)";
           Real u(unit = "J/kg", nominal = 1000000.0, quantity = "SpecificEnergy", min = -100000000.0, max = 100000000.0) "Specific internal energy of medium";
           constant Real R_s(unit = "J/(kg.K)", nominal = 1000.0, quantity = "SpecificHeatCapacity", min = 0.0, max = 10000000.0) = 461.5231157345669 "Gas constant (of mixture if applicable)";
           constant Real MM(unit = "kg/mol", nominal = 0.032, quantity = "MolarMass", min = 0.001, max = 0.25) = 0.018015268 "Molar mass (of mixture or single fluid)";
           constant Boolean preferredMediumStates = false "= true if StateSelect.prefer shall be used for the independent property variables of the medium";
           constant Boolean standardOrderComponents = true "If true, and reducedX = true, the last element of X will be computed from the other ones";
           Modelica.Units.NonSI.Temperature_degC T_degC "Temperature of medium in [degC]";
           Modelica.Units.NonSI.Pressure_bar p_bar "Absolute pressure of medium in [bar]";
           Integer phase(min = 0, max = 2) "2 for two-phase, 1 for one-phase, 0 if not known";
          protected
           record state_rec
             Integer phase(min = 0, max = 2) "Phase of the fluid: 1 for 1-phase, 2 for two-phase, 0 for not known, e.g., interactive use";
             Real h(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific enthalpy";
             Real d(unit = "kg/m3", displayUnit = "g/cm3", nominal = 500.0, quantity = "Density", min = 0.0, max = 100000.0) "Density";
             Real T(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Temperature";
             Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Pressure";
           end state_rec;
          public
           state_rec state;
          protected
           record sat_rec
             Real psat(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Saturation pressure";
             Real Tsat(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Saturation temperature";
           end sat_rec;
          public
           sat_rec sat;
         end medium_rec;
        public
         medium_rec medium;
        protected
         record 'ports[1]_rec'
           Real m_flow(unit = "kg/s", quantity = "MassFlowRate.WaterIF97", min = -100000.0, max = 100000.0) "Mass flow rate from the connection point into the component";
           Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Thermodynamic pressure in the connection point";
           Real h_outflow(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific thermodynamic enthalpy close to the connection point if m_flow < 0";
         end 'ports[1]_rec';
        public
         'ports[1]_rec' 'ports[1]';
       end InletFlow_FromHeatNetwork_rec;
      public
       InletFlow_FromHeatNetwork_rec InletFlow_FromHeatNetwork annotation(Dialog);
      protected
       record InletFlowRate_FromHeatNetWork_rec
         parameter Real height = 29 "Height of ramps";
         parameter Modelica.Units.SI.Time duration(min = 0.0) = 100 "Duration of ramp (= 0.0 gives a Step)";
         Modelica_Blocks_Interfaces_RealOutput y "Connector of Real output signal";
         parameter Real offset = 0.01 "Offset of output signal y";
         parameter Modelica.Units.SI.Time startTime = 1000 "Output y = offset for time < startTime";
       end InletFlowRate_FromHeatNetWork_rec;
      public
       InletFlowRate_FromHeatNetWork_rec InletFlowRate_FromHeatNetWork annotation(Dialog);
      protected
       record BackPressure_FromHeatNetWork_rec
         constant Integer nPorts = 1 "Number of ports";
         parameter Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) = 1500000 "Fixed value of pressure";
         parameter Real h(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) = 84013.0581525969 "Fixed value of specific enthalpy";
         parameter Real 'X[1]'(nominal = 0.1, quantity = "MassFraction", min = 0.0, max = 1.0) = 1.0 "Fixed value of composition";
        protected
         record medium_rec
           Real p(unit = "Pa", displayUnit = "bar", nominal = 100000.0, quantity = "Pressure", min = 0.0) "Absolute pressure of medium";
           Real h(unit = "J/kg", quantity = "SpecificEnergy") "Specific enthalpy of medium";
           Real d(unit = "kg/m3", displayUnit = "g/cm3", nominal = 500.0, quantity = "Density", min = 0.0, max = 100000.0) "Density of medium";
           Real T(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Temperature of medium";
           constant Real 'X[1]'(nominal = 0.1, quantity = "MassFraction", min = 0.0, max = 1.0) = 1.0 "Mass fractions (= (component mass)/total mass  m_i/m)";
           Real u(unit = "J/kg", nominal = 1000000.0, quantity = "SpecificEnergy", min = -100000000.0, max = 100000000.0) "Specific internal energy of medium";
           constant Real R_s(unit = "J/(kg.K)", nominal = 1000.0, quantity = "SpecificHeatCapacity", min = 0.0, max = 10000000.0) = 461.5231157345669 "Gas constant (of mixture if applicable)";
           constant Real MM(unit = "kg/mol", nominal = 0.032, quantity = "MolarMass", min = 0.001, max = 0.25) = 0.018015268 "Molar mass (of mixture or single fluid)";
           constant Boolean preferredMediumStates = false "= true if StateSelect.prefer shall be used for the independent property variables of the medium";
           constant Boolean standardOrderComponents = true "If true, and reducedX = true, the last element of X will be computed from the other ones";
           Modelica.Units.NonSI.Temperature_degC T_degC "Temperature of medium in [degC]";
           Modelica.Units.NonSI.Pressure_bar p_bar "Absolute pressure of medium in [bar]";
           Integer phase(min = 0, max = 2) "2 for two-phase, 1 for one-phase, 0 if not known";
          protected
           record state_rec
             Integer phase(min = 0, max = 2) "Phase of the fluid: 1 for 1-phase, 2 for two-phase, 0 for not known, e.g., interactive use";
             Real h(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific enthalpy";
             Real d(unit = "kg/m3", displayUnit = "g/cm3", nominal = 500.0, quantity = "Density", min = 0.0, max = 100000.0) "Density";
             Real T(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Temperature";
             Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Pressure";
           end state_rec;
          public
           state_rec state;
          protected
           record sat_rec
             Real psat(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Saturation pressure";
             Real Tsat(unit = "K", displayUnit = "degC", nominal = 500.0, quantity = "ThermodynamicTemperature", min = 273.15, max = 2273.15) "Saturation temperature";
           end sat_rec;
          public
           sat_rec sat;
         end medium_rec;
        public
         medium_rec medium;
        protected
         record 'ports[1]_rec'
           Real m_flow(unit = "kg/s", quantity = "MassFlowRate.WaterIF97", min = -1E+60, max = 1E+60) "Mass flow rate from the connection point into the component";
           Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Thermodynamic pressure in the connection point";
           Real h_outflow(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific thermodynamic enthalpy close to the connection point if m_flow < 0";
         end 'ports[1]_rec';
        public
         'ports[1]_rec' 'ports[1]';
       end BackPressure_FromHeatNetWork_rec;
      public
       BackPressure_FromHeatNetWork_rec BackPressure_FromHeatNetWork annotation(Dialog);
      protected
       record fluid2TSPro_rec
        protected
         record steam_inlet_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end steam_inlet_rec;
        public
         steam_inlet_rec steam_inlet;
        protected
         record port_b_rec
           Real m_flow(unit = "kg/s", quantity = "MassFlowRate.WaterIF97", min = -100000.0, max = 100000.0) "Mass flow rate from the connection point into the component";
           Real p(unit = "Pa", displayUnit = "bar", nominal = 1000000.0, quantity = "Pressure", min = 611.657, max = 100000000.0) "Thermodynamic pressure in the connection point";
           Real h_outflow(unit = "J/kg", nominal = 500000.0, quantity = "SpecificEnergy", min = -10000000000.0, max = 10000000000.0) "Specific thermodynamic enthalpy close to the connection point if m_flow < 0";
         end port_b_rec;
        public
         port_b_rec port_b;
       end fluid2TSPro_rec;
      public
       fluid2TSPro_rec fluid2TSPro;
      protected
       record Test_Timetable_Rotation_PumpHP_rec
         parameter Real 'Table[1,1]' = 0.0 "Table (temps = première colonne)";
         parameter Real 'Table[1,2]' = 1428.0319 "Table (temps = première colonne)";
         parameter Real 'Table[2,1]' = 1000.0 "Table (temps = première colonne)";
         parameter Real 'Table[2,2]' = 1428.0319 "Table (temps = première colonne)";
         parameter Real 'Table[3,1]' = 1100.0 "Table (temps = première colonne)";
         parameter Real 'Table[3,2]' = 1441.9839 "Table (temps = première colonne)";
         parameter Real 'Table[4,1]' = 1101.0 "Table (temps = première colonne)";
         parameter Real 'Table[4,2]' = 1442.0339 "Table (temps = première colonne)";
         parameter Real 'Table[5,1]' = 1299.0 "Table (temps = première colonne)";
         parameter Real 'Table[5,2]' = 1442.103 "Table (temps = première colonne)";
         parameter Real 'Table[6,1]' = 1300.0 "Table (temps = première colonne)";
         parameter Real 'Table[6,2]' = 1442.103 "Table (temps = première colonne)";
         parameter Real 'Table[7,1]' = 1398.0 "Table (temps = première colonne)";
         parameter Real 'Table[7,2]' = 1402.0459 "Table (temps = première colonne)";
         parameter Real 'Table[8,1]' = 1400.0 "Table (temps = première colonne)";
         parameter Real 'Table[8,2]' = 1401.2559 "Table (temps = première colonne)";
         parameter Real 'Table[9,1]' = 1500.0 "Table (temps = première colonne)";
         parameter Real 'Table[9,2]' = 1400.9297 "Table (temps = première colonne)";
         constant Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y;
       end Test_Timetable_Rotation_PumpHP_rec;
      public
       Test_Timetable_Rotation_PumpHP_rec Test_Timetable_Rotation_PumpHP annotation(Dialog);
      protected
       record Test_Timetable_Rotation_PumpLP_rec
         parameter Real 'Table[1,1]' = 0.0 "Table (temps = première colonne)";
         parameter Real 'Table[1,2]' = 1546.0057 "Table (temps = première colonne)";
         parameter Real 'Table[2,1]' = 1000.0 "Table (temps = première colonne)";
         parameter Real 'Table[2,2]' = 1546.0051 "Table (temps = première colonne)";
         parameter Real 'Table[3,1]' = 1100.0 "Table (temps = première colonne)";
         parameter Real 'Table[3,2]' = 1393.8423 "Table (temps = première colonne)";
         parameter Real 'Table[4,1]' = 1101.0 "Table (temps = première colonne)";
         parameter Real 'Table[4,2]' = 1393.8666 "Table (temps = première colonne)";
         parameter Real 'Table[5,1]' = 1299.0 "Table (temps = première colonne)";
         parameter Real 'Table[5,2]' = 1393.8936 "Table (temps = première colonne)";
         parameter Real 'Table[6,1]' = 1300.0 "Table (temps = première colonne)";
         parameter Real 'Table[6,2]' = 1393.8936 "Table (temps = première colonne)";
         parameter Real 'Table[7,1]' = 1398.0 "Table (temps = première colonne)";
         parameter Real 'Table[7,2]' = 1368.1257 "Table (temps = première colonne)";
         parameter Real 'Table[8,1]' = 1400.0 "Table (temps = première colonne)";
         parameter Real 'Table[8,2]' = 1367.624 "Table (temps = première colonne)";
         parameter Real 'Table[9,1]' = 1500.0 "Table (temps = première colonne)";
         parameter Real 'Table[9,2]' = 1367.5062 "Table (temps = première colonne)";
         constant Integer option_interpolation = 1 "1: linear interpolation - 2: spline interpolation";
        protected
         record y_rec
           Real signal;
         end y_rec;
        public
         y_rec y annotation(Dialog);
       end Test_Timetable_Rotation_PumpLP_rec;
      public
       Test_Timetable_Rotation_PumpLP_rec Test_Timetable_Rotation_PumpLP annotation(Dialog);
       parameter Real _Hin_BC_start = 2944110.0
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Hin_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Hin_BC(start = _Hin_BC_start)
       annotation (Placement(transformation(extent={{-124,30},{-84,70}})));
       parameter Real _Qin_BC_start = 240
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Qin_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Qin_BC(start = _Qin_BC_start) "Valve openinn (Real)"
       annotation (Placement(transformation(extent={{-124,-20},{-84,20}})));
       parameter Real _Pout_BC_start = 4900000.0
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Pout_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Pout_BC(nominal = 100000.0, min = 0.0, start = _Pout_BC_start)
       annotation (Placement(transformation(extent={{-124,-70},{-84,-30}})));
       Modelica.Blocks.Interfaces.RealOutput Pin_TSPro(nominal = 100000.0, min = 0.0)
       annotation (Placement(transformation(extent={{100,30},{140,70}})));
       Modelica.Blocks.Interfaces.RealOutput Qout_TSPro
       annotation (Placement(transformation(extent={{100,-20},{140,20}})));
       Modelica.Blocks.Interfaces.RealOutput Hout_TSPro
       annotation (Placement(transformation(extent={{100,-70},{140,-30}})));
      public
       parameter String fmi_instanceName="BOP_fmu"
       annotation (Dialog(tab="FMI", group="Instance name"));
       parameter Boolean fmi_loggingOn=false
       annotation (Dialog(tab="FMI", group="Enable logging"));
       parameter Boolean fmi_InputTime=false
       "Time point of input used when calling doStep."
       annotation (Evaluate=true,Dialog(tab="FMI", group="Input Handling"),choices(choice= false "StepEnd", choice= true "StepStart"));
       parameter Boolean fmi_UsePreOnInputSignals=true
       annotation (Evaluate=true,Dialog(tab="FMI", group="Input Handling"));
       parameter Real fmi_StartTime = 0.0
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_StopTime = 1.0
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_NumberOfSteps = 500
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_CommunicationStepSize=(fmi_StopTime-fmi_StartTime)/fmi_NumberOfSteps
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Integer stepSizeScaleFactor = 1 "Number of doSteps called between two CommunicationStepSize"
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Boolean fmi_forceShutDownAtStopTime=false
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_rTol=1e-6 "relative tolerance for the internal solver of the fmu"
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter String fmi_resourceLocation="file:///"+ModelicaServices.ExternalReferences.loadResource(
            "modelica://TANDEM/Resources/Library/FMU/BOP/resources")
       annotation (Dialog(tab="FMI", group="Instantiation"));
      protected
       fmi_Functions.fmiModel fmi;
       Boolean fmi_exitInit(start=false,fixed=true);
       Boolean fmi_flip(start=false,fixed=true);
       parameter Real fmi_rdum(start=0,fixed=false);
       parameter Integer fmi_idum(start=0,fixed=false);
       Boolean fmi_StepOK;
       parameter Real zeroOffset = 0;
       parameter Real myTimeStart(fixed=false);
       record 'Internal '
         Real Pin_TSPro;
         Real Qout_TSPro;
         Real Hout_TSPro;
       end 'Internal ';
       'Internal ' internal;
     Real RealVariables1[100];
     Real RealVariables2[100];
     Real RealVariables3[100];
     Real RealVariables4[100];
     Real RealVariables5[100];
     Real RealVariables6[100];
     Real RealVariables7[100];
     Real RealVariables8[100];
     Real RealVariables9[100];
     Real RealVariables10[100];
     Real RealVariables11[100];
     Real RealVariables12[100];
     Real RealVariables13[100];
     Real RealVariables14[100];
     Real RealVariables15[37];
     Real RealDependentParameters[2];
     Real RealFixedLocal[88];
     Integer IntegerVariables[1];
     Integer IntegerFixedLocal[1];
       parameter String fmi_xNames[11]= {"BOP_HeatSink.P", "BOP_HeatSink.hl", "BOP_HeatSink.hv", "BOP_HeatSink.Vv", "pI1.x", "volumeC1.h", "Bache_a.h", "Bache_b.h", "volumeC2.h", "pI5.x", "pI6.x"};
       parameter Integer fmi_xVrs[11]= {33554432, 33554433, 33554434, 33554435, 33554436, 33554437, 33554438, 33554439, 33554440, 33554441, 33554442};
       parameter String fmi_dxNames[11]= {"der(BOP_HeatSink.P)", "der(BOP_HeatSink.hl)", "der(BOP_HeatSink.hv)", "der(BOP_HeatSink.Vv)", "der(pI1.x)", "der(volumeC1.h)", "der(Bache_a.h)", "der(Bache_b.h)", "der(volumeC2.h)", "der(pI5.x)", "der(pI6.x)"};
       parameter Integer fmi_dxVrs[11]= {587202560, 587202561, 587202562, 587202563, 587202564, 587202565, 587202566, 587202567, 587202568, 587202569, 587202570};
       parameter String fmi_uNames[3]= {"Hin_BC", "Qin_BC", "Pout_BC"};
       parameter Integer fmi_uVrs[3]= {352321536, 352321537, 352321538};
       parameter String fmi_yNames[3]= {"Pin_TSPro", "Qout_TSPro", "Hout_TSPro"};
       parameter Integer fmi_yVrs[3]= {335544320, 335544321, 335544322};
     package fmi_Functions
         class fmiModel
           extends ExternalObject;
           function constructor "Initialize FMI model"
             extends Modelica.Icons.Function;
             input String instanceName;
             input Boolean loggingOn;
             input String resourceLocation;
             output fmiModel fmi;
             external"C" fmi = BOP65666263844538138976893362_fmiInstantiateModel2(instanceName, loggingOn, resourceLocation)
             annotation(Include="
#ifndef BOP65666263844538138976893362_Instantiate_C
#define BOP65666263844538138976893362_Instantiate_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void BOP65666263844538138976893362Logger(fmi2ComponentEnvironment componentEnvironment, fmi2String instanceName, fmi2Status status,
  fmi2String category, fmi2String message, ...) {
  char msg[4096];
  char buf[4096];
  va_list ap;
  int len;
  va_start(ap,message);
#if defined(_MSC_VER) && _MSC_VER>=1200
  len = _snprintf(msg, sizeof(msg)/sizeof(*msg), \"%s: %s\", instanceName, message);
  if (len < 0) goto fail;
  len = _vsnprintf(buf, sizeof(buf)/sizeof(*buf) - 2, msg, ap);
  if (len < 0) goto fail;
#else
  len = snprintf(msg, sizeof(msg)/sizeof(*msg), \"%s: %s\", instanceName, message);
  if (len < 0) goto fail;
  len = vsnprintf(buf, sizeof(buf)/sizeof(*buf) - 2, msg, ap);
  if (len < 0) goto fail;
#endif
  if( len>0 && len < 4096 && buf[len - 1]!='\\n'){
    buf[len] = '\\n';
    buf[len + 1] = 0;
  }
  va_end(ap);
  switch (status) {
    case fmi2Fatal:
      ModelicaMessage(\"[fmi2Fatal]: \");
      break;
    case fmi2Error:
      ModelicaMessage(\"[fmi2Error]: \");
      break;
    case fmi2Discard:
      ModelicaMessage(\"[fmi2Discard]: \");
      break;
    case fmi2Warning:
      ModelicaMessage(\"[fmi2Warning]: \");
      break;
    case fmi2OK:
      ModelicaMessage(\"[fmi2OK]: \");
      break;
  }
  ModelicaMessage(buf);
  return;
fail:
  ModelicaMessage(\"Logger failed, message too long?\");
}
void * BOP65666263844538138976893362_fmiInstantiateModel2(const char*instanceName, fmi2Boolean loggingOn, fmi2String resourceLocation) {
  static fmi2CallbackFunctions funcs = {&BOP65666263844538138976893362Logger, &calloc, &free, NULL, NULL};
  struct dy_fmi2Extended* res;
  res = calloc(1, sizeof(struct dy_fmi2Extended));
  if (res!=0) {
    if (!(res->hInst=LoadLibraryW(L\"BOP.dll\"))) {
      ModelicaError(\"Loading of FMU dynamic link library (BOP.dll) failed!\");
      return 0;
    }
    if (!(res->dyFmiInstantiate=(fmi2InstantiateFunc)GetProcAddress(res->hInst,\"fmi2Instantiate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Instantiate!\");
      return 0;
    }
    if (!(res->dyFmiFreeInstance=(fmi2FreeInstanceFunc)GetProcAddress(res->hInst,\"fmi2FreeInstance\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2FreeInstance!\");
      return 0;
    }
    if (!(res->dyFmiSetupExperiment=(fmi2SetupExperimentFunc)GetProcAddress(res->hInst,\"fmi2SetupExperiment\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetupExperiment!\");
      return 0;
    }
    if (!(res->dyFmiEnterInitializationMode=(fmi2EnterInitializationModeFunc)GetProcAddress(res->hInst,\"fmi2EnterInitializationMode\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2EnterInitializationMode!\");
      return 0;
    }
    if (!(res->dyFmiExitInitializationMode=(fmi2ExitInitializationModeFunc)GetProcAddress(res->hInst,\"fmi2ExitInitializationMode\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2ExitInitializationMode!\");
      return 0;
    }
    if (!(res->dyFmiTerminate=(fmi2TerminateFunc)GetProcAddress(res->hInst,\"fmi2Terminate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Terminate!\");
      return 0;
    }
    if (!(res->dyFmiReset=(fmi2ResetFunc)GetProcAddress(res->hInst,\"fmi2Reset\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Reset!\");
      return 0;
    }
    if (!(res->dyFmiSetReal=(fmi2SetRealFunc)GetProcAddress(res->hInst,\"fmi2SetReal\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetReal!\");
      return 0;
    }
    if (!(res->dyFmiGetReal=(fmi2GetRealFunc)GetProcAddress(res->hInst,\"fmi2GetReal\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetReal!\");
      return 0;
    }
    if (!(res->dyFmiSetInteger=(fmi2SetIntegerFunc)GetProcAddress(res->hInst,\"fmi2SetInteger\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetInteger!\");
      return 0;
    }
    if (!(res->dyFmiGetInteger=(fmi2GetIntegerFunc)GetProcAddress(res->hInst,\"fmi2GetInteger\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetInteger!\");
      return 0;
    }
    if (!(res->dyFmiSetBoolean=(fmi2SetBooleanFunc)GetProcAddress(res->hInst,\"fmi2SetBoolean\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetBoolean!\");
      return 0;
    }
    if (!(res->dyFmiGetBoolean=(fmi2GetBooleanFunc)GetProcAddress(res->hInst,\"fmi2GetBoolean\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetBoolean!\");
      return 0;
    }
    if (!(res->dyFmiSetDebugLogging=(fmi2SetDebugLoggingFunc)GetProcAddress(res->hInst,\"fmi2SetDebugLogging\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetDebugLogging!\");
      return 0;
    }
    if (!(res->dyFmiSetString=(fmi2SetStringFunc)GetProcAddress(res->hInst,\"fmi2SetString\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetString!\");
      return 0;
    }
    if (!(res->dyFmiGetString=(fmi2GetStringFunc)GetProcAddress(res->hInst,\"fmi2GetString\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetString!\");
      return 0;
    }
    if(!(res->dyFmiDoStep=(fmi2DoStepFunc)GetProcAddress(res->hInst,\"fmi2DoStep\"))){
      ModelicaError(\"GetProcAddress failed for fmi2DoStep!\\n The model was imported as a Co-Simulation FMU but could not load the CS specific function fmiDoStep\\n Verify that the FMU supports Co-Simulation\");
      return 0;
    }
    if(!(res->dyFmiGetBooleanStatus=(fmi2GetBooleanStatusFunc)GetProcAddress(res->hInst,\"fmi2GetBooleanStatus\"))){
      ModelicaError(\"GetProcAddress failed for fmi2GetBooleanStatus!\\n The model was imported as a Co-Simulation FMU but could not load the CS specific function fmiGetBooleanStatus\\n Verify that the FMU supports Co-Simulation\");
      return 0;
    }
    if (!(res->dyFmiGetDirectionalDerivative=(fmi2GetDirectionalDerivativeFunc)GetProcAddress(res->hInst,\"fmi2GetDirectionalDerivative\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetDirectionalDerivative!\");
      return 0;
    }
    if (!(res->dyFmiGetFMUstate=(fmi2GetFMUstateFunc)GetProcAddress(res->hInst,\"fmi2GetFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiSetFMUstate=(fmi2SetFMUstateFunc)GetProcAddress(res->hInst,\"fmi2SetFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiFreeFMUstate=(fmi2FreeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2FreeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2FreeFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiSerializedFMUstateSize=(fmi2SerializedFMUstateSizeFunc)GetProcAddress(res->hInst,\"fmi2SerializedFMUstateSize\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SerializedFMUstateSize!\");
      return 0;
    }
    if (!(res->dyFmiSerializeFMUstate=(fmi2SerializeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2SerializeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SerializeFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiDeSerializeFMUstate=(fmi2DeSerializeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2DeSerializeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2DeSerializeFMUstate!\");
      return 0;
    }
    res->m=res->dyFmiInstantiate(instanceName, fmi2CoSimulation, \"{65b6662b-638a-445f-a381-3897689336e2}\",resourceLocation, &funcs, fmi2False, loggingOn);
    if (0==res->m) {free(res);res=0;ModelicaError(\"InstantiateModel failed\");}
    else {res->dyTriggered=0;res->dyTime=res->dyLastTime=-1e37;res->discreteInputChanged=1;res->currentMode=dyfmi2InstantiationMode;res->dyLastStepTime=0;res->dyFMUstate=NULL;}
  }
  return res;
}
#endif",      Library="BOP", LibraryDirectory=
                    "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
              annotation (__Dymola_CriticalRegion="BOP");
           end constructor;

           function destructor "Release storage of FMI model"
             extends Modelica.Icons.Function;
             input fmiModel fmi;
             external"C"
                        BOP65666263844538138976893362_fmiFreeModelInstance2(fmi)
             annotation (Include="
#ifndef BOP65666263844538138976893362_Free_C
#define BOP65666263844538138976893362_Free_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void BOP65666263844538138976893362_fmiFreeModelInstance2(void*m) {
  struct dy_fmi2Extended*a=m;
  if (a) {
    if(a->dyFMUstate)
    a->dyFmiFreeFMUstate(a->m, &a->dyFMUstate);
    /*a->dyFmiSetDebugLogging(a->m,fmi2True,0,NULL);*/
    a->dyFmiTerminate(a->m);
    a->dyFmiFreeInstance(a->m);
    FreeLibrary(a->hInst);
    free(a);
  }
}
#endif",      Library="BOP", LibraryDirectory=
                    "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
              annotation (__Dymola_CriticalRegion="BOP");
           end destructor;
         end fmiModel;

         function fmiDoStep
         input fmiModel fmi;
         input Real currentTime;
         input Real stepSize;
         input Real preAvailable;
         output Boolean stepOK;
         output Real postAvailable=preAvailable;
         external"C" stepOK= BOP65666263844538138976893362_fmiDoStep2(fmi, currentTime, stepSize)
           annotation (Include="
#ifndef BOP65666263844538138976893362_DoStep_C
#define BOP65666263844538138976893362_DoStep_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
double BOP65666263844538138976893362_fmiDoStep2(void*m, double currentTime, double stepSize) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  fmi2Boolean value=fmi2False;
  if (a) {
    status=a->dyFmiDoStep(a->m, currentTime, stepSize, fmi2True);
    if(status==fmi2Discard){
      status = a->dyFmiGetBooleanStatus(a->m, fmi2Terminated, &value);
      if(value==fmi2True){
        terminate(\"Terminate signaled by the FMU\");
      }
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning){    ModelicaFormatError(\"The call of fmi2DoStep(%f, %f) failed in FMU: ErrorModel\\r\\nNote: setting fmi_loggingOn in the FMU component may produce more information from the FMU.\",currentTime, stepSize);  }  return 1.0;
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiDoStep;

         function fmiReset
           input fmiModel fmi;
           output Boolean resetOK;
           external"C" resetOK = BOP65666263844538138976893362_fmiReset2(fmi)
           annotation (Include="
#ifndef BOP65666263844538138976893362_Reset_C
#define BOP65666263844538138976893362_Reset_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
double BOP65666263844538138976893362_fmiReset2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiReset(a->m);
    a->currentMode=dyfmi2InstantiationMode;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiReset failed\");
    return 1.0;
  }
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiReset;

         function fmiGetRealScalar
           input fmiModel fmi;
           input Integer ref;
           output Real val;
         algorithm
             val := scalar(fmiGetReal(fmi, {ref}));
         end fmiGetRealScalar;

         function fmiGetReal
           input fmiModel fmi;
           input Integer refs[:];
           output Real vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_GetReal_C
#define BOP65666263844538138976893362_GetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiGetReal2(void*m, const int*refs, size_t nrefs, double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetReal failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetReal;

         function fmiGetRealwf
           input fmiModel fmi;
           input Integer refs[:];
           input Real preAvailable;
           output Real vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_GetReal_C
#define BOP65666263844538138976893362_GetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiGetReal2(void*m, const int*refs, size_t nrefs, double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetReal failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetRealwf;

         function fmiSetReal
           input fmiModel fmi;
           input Integer refs[:];
           input Real vals[size(refs, 1)];
           external"C"
                      BOP65666263844538138976893362_fmiSetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef BOP65666263844538138976893362_SetReal_C
#define BOP65666263844538138976893362_SetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetReal2(void*m, const int*refs, size_t nrefs, const double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetReal failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetReal;

         function fmiSetRealwf
           input fmiModel fmi;
           input Integer refs[:];
           input Real vals[size(refs, 1)];
           input Real preAvailable;
           output Real postAvailable=preAvailable;
           external"C"
                      BOP65666263844538138976893362_fmiSetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef BOP65666263844538138976893362_SetReal_C
#define BOP65666263844538138976893362_SetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetReal2(void*m, const int*refs, size_t nrefs, const double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetReal failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetRealwf;

         function fmiGetIntegerScalar
           input fmiModel fmi;
           input Integer ref;
           output Integer val;
         algorithm
             val := scalar(fmiGetInteger(fmi, {ref}));
         end fmiGetIntegerScalar;

         function fmiGetInteger
           input fmiModel fmi;
           input Integer refs[:];
           output Integer vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_GetInteger_C
#define BOP65666263844538138976893362_GetInteger_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiGetInteger2(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetInteger;

         function fmiGetIntegerwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer preAvailable;
           output Integer vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_GetInteger_C
#define BOP65666263844538138976893362_GetInteger_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void BOP65666263844538138976893362_fmiGetInteger2(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetIntegerwf;

         function fmiSetInteger
           input fmiModel fmi;
           input Integer refs[:];
           input Integer vals[size(refs, 1)];
          protected
           Integer oldVals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiSetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals,
             oldVals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_SetInteger_C
#define BOP65666263844538138976893362_SetInteger_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetInteger2(void*m, const int*refs, size_t nrefs, int*vals, int*oldVals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    if(!a->discreteInputChanged){
      status=a->dyFmiGetInteger(a->m, refs, nrefs, oldVals);
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed in SetInteger\");
      for( i = 0; i < nrefs; ++i){
        if(oldVals[i] != vals[i]){
          a->discreteInputChanged = 1;
          break;
        }
      }
    }
    if(a->discreteInputChanged){
      if(a->currentMode == dyfmi2ContinuousTimeMode){
        status = a->dyFmiEnterEventMode(a->m);
        if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
        a->currentMode = dyfmi2EventMode;
      }
      status=a->dyFmiSetInteger(a->m, refs, nrefs, vals);
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetInteger failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetInteger;

         function fmiSetIntegerwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer vals[size(refs, 1)];
           input Integer preAvailable;
           output Integer postAvailable=preAvailable;
           external"C" BOP65666263844538138976893362_fmiSetInteger2wf(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_SetIntegerwf_C
#define BOP65666263844538138976893362_SetIntegerwf_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetInteger2wf(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetInteger failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetIntegerwf;

         function fmiGetBooleanScalar
           input fmiModel fmi;
           input Integer ref;
           output Boolean val;
         algorithm
             val := scalar(fmiGetBoolean(fmi, {ref}));
         end fmiGetBooleanScalar;

         function fmiGetBoolean
           input fmiModel fmi;
           input Integer refs[:];
           output Boolean vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef BOP65666263844538138976893362_GetBoolean_C
#define BOP65666263844538138976893362_GetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiGetBoolean2(void*m, const int* refs, size_t nr, int* vals) {
  int i;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(vals));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed\");
    for(i=nr-1;i>=0;i--) vals[i]=((fmi2Boolean*)(vals))[i];
  }
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetBoolean;

         function fmiGetBooleanwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer preAvailable;
           output Boolean vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiGetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef BOP65666263844538138976893362_GetBoolean_C
#define BOP65666263844538138976893362_GetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiGetBoolean2(void*m, const int* refs, size_t nr, int* vals) {
  int i;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(vals));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed\");
    for(i=nr-1;i>=0;i--) vals[i]=((fmi2Boolean*)(vals))[i];
  }
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetBooleanwf;

         function fmiSetBoolean
           input fmiModel fmi;
           input Integer refs[:];
           input Boolean vals[size(refs, 1)];
          protected
           Boolean dummy[size(refs, 1)];
           Boolean oldVals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiSetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals,
             dummy,
             oldVals)
             annotation (Include="
#ifndef BOP65666263844538138976893362_SetBoolean_C
#define BOP65666263844538138976893362_SetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetBoolean2(void*m, const int* refs, size_t nr, const int* vals, int* dummy, int* oldVals) {
  size_t i;
  int j;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nr){return;}
  for(i=0;i<nr;++i) ((fmi2Boolean*)(dummy))[i]=vals[i];
  if (a) {
    if(!a->discreteInputChanged){
      status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(oldVals));
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed in SetBoolean\");
      for(j=nr-1;j>=0;j--){
        oldVals[j]=((fmi2Boolean*)(oldVals))[j];
        if(oldVals[j] != dummy[j]){
          a->discreteInputChanged = 1;
          break;
        }
      }
    }
    if(a->discreteInputChanged){
      if(a->currentMode == dyfmi2ContinuousTimeMode){
        status = a->dyFmiEnterEventMode(a->m);
        if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
        a->currentMode = dyfmi2EventMode;
      }
      status=a->dyFmiSetBoolean(a->m, refs, nr, (fmi2Boolean*)(dummy));
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetBoolean failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetBoolean;

         function fmiSetString
           input fmiModel fmi;
           input Integer refs[:];
           input String vals[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiSetString2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef BOP65666263844538138976893362_SetString_C
#define BOP65666263844538138976893362_SetString_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetString2(void*m, const int*refs, size_t nrefs,const fmi2String vals[]) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    if(a->currentMode == dyfmi2ContinuousTimeMode){
      status = a->dyFmiEnterEventMode(a->m);
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
      a->currentMode = dyfmi2EventMode;
    }
    status=a->dyFmiSetString(a->m, refs, nrefs, vals);
    a->discreteInputChanged = fmi2True;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetString failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetString;

         function fmiSetBooleanwf
           input fmiModel fmi;
           input Integer refs[:];
           input Boolean vals[size(refs, 1)];
           input Integer preAvailable;
           output Integer postAvailable=preAvailable;
          protected
           Boolean dummy[size(refs, 1)];
           external"C" BOP65666263844538138976893362_fmiSetBoolean2wf(
             fmi,
             refs,
             size(refs, 1),
             vals,
             dummy)
             annotation (Include="
#ifndef BOP65666263844538138976893362_SetBooleanwf_C
#define BOP65666263844538138976893362_SetBooleanwf_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSetBoolean2wf(void*m, const int* refs, size_t nr, const int* vals, int* dummy) {
  size_t i;
  int j;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nr){return;}
  for(i=0;i<nr;++i) ((fmi2Boolean*)(dummy))[i]=vals[i];
  if (a) {
    status=a->dyFmiSetBoolean(a->m, refs, nr, (fmi2Boolean*)(dummy));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetBoolean failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (
              __Dymola_CriticalRegion="BOP",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetBooleanwf;

         function fmiGetDirectionalDerivative
           input fmiModel fmi;
           input Integer z_refs[:];
           input Integer v_refs[:];
           input Real dv[size(v_refs, 1)];
           output Real dz[size(z_refs, 1)];
           external"C" BOP65666263844538138976893362_GetDirectionalDerivative2(
             fmi,
             z_refs,
             size(z_refs, 1),
             v_refs,
             size(v_refs, 1),
             dv,
             dz)
           annotation (Include="
#ifndef BOP65666263844538138976893362_GetDirectionalDerivative2_C
#define BOP65666263844538138976893362_GetDirectionalDerivative2_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_GetDirectionalDerivative2(void*m, const int* zref, size_t nzr, const int* vrefs, size_t nvr, const double *dv, double *dz) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetDirectionalDerivative(a->m, zref, nzr, vrefs, nvr, dv, dz);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiGetDirectionalDerivative failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiGetDirectionalDerivative;

         function GetRealVariable
           input fmiModel fmi;
           input Real Time;
           input Real realInputs[:];
           input Real realLinearDependentInputs[:];
           input Real states[:];
           input Real linearDependentStates[:];
           input Real linearOffset=0;
           input Boolean booleanInputs[:];
           input Integer integerInputs[:];
           //input String stringInputs[:];
           input Integer realInputValueReferences[:];
           input Integer realLinearDependentInputsValueReferences[:];
           input Integer statesValueRefernces[:];
           input Integer booleanInputValueReferences[:];
           input Integer integerInputValueReferences[:];
           //input Integer stringInputValueReferences[:];
           input Integer outputValueReference[:];
           output Real outputVariable;
         algorithm
           //fmi_Functions.fmiSetTime(fmi, Time);
           fmiSetReal(fmi,realInputValueReferences,realInputs);
           fmiSetReal(fmi,realLinearDependentInputsValueReferences,realLinearDependentInputs);
           fmiSetBoolean(fmi,booleanInputValueReferences,booleanInputs);
           fmiSetInteger(fmi,integerInputValueReferences,integerInputs);
           //SetString(fmi,stringInputValueReferences,stringInputs);
           outputVariable:=fmiGetRealScalar(fmi,outputValueReference[1]);
           annotation(derivative(noDerivative=realLinearDependentInputs,noDerivative=linearDependentStates)=derGetRealVariable, LateInline=true);
         end GetRealVariable;

         function derGetRealVariable
           input fmiModel fmi;
           input Real Time;
           input Real realInputs[:];
           input Real realLinearDependentInputs[:];
           input Real states[:];
           input Real linearDependentStates[:];
           input Real linearOffset=0;
           input Boolean booleanInputs[:];
           input Integer integerInputs[:];
           //input String stringInputs[:];
           input Integer realInputValueReferences[:];
           input Integer realLinearDependentInputsValueReferences[:];
           input Integer statesValueRefernces[:];
           input Integer booleanInputValueReferences[:];
           input Integer integerInputValueReferences[:];
           //input Integer stringInputValueReferences[:];
           input Integer outputValueReference[:];
           input Real derRealInputs[:];
           input Real derStates[:];
           input Real derLinearOffsets;
           output Real derOutputVariable;
          protected
           Real dummy[1];
         algorithm
           //fmi_Functions.fmiSetTime(fmi, Time);
           fmiSetReal(fmi,realInputValueReferences,realInputs);
           fmiSetReal(fmi,realLinearDependentInputsValueReferences,realLinearDependentInputs);
           fmiSetBoolean(fmi,booleanInputValueReferences,booleanInputs);
           fmiSetInteger(fmi,integerInputValueReferences,integerInputs);
           //setString(fmi,stringInputValueReferences,stringInputs);
           dummy:=fmiGetDirectionalDerivative(fmi, outputValueReference,  cat(1,realInputValueReferences,statesValueRefernces), cat(1,derRealInputs,derStates));
           derOutputVariable:=dummy[1]+derLinearOffsets;
           annotation(LateInline=true);
         end derGetRealVariable;

         function linearizeFMU
           input fmiModel fmi;
           input Integer xVr[:];
           input Integer dxVr[:];
           input Integer uVr[:];
           input Integer yVr[:];
           output Real A[size(dxVr, 1), size(xVr, 1)];
           output Real B[size(dxVr, 1), size(uVr, 1)];
           output Real C[size(yVr, 1), size(xVr, 1)];
           output Real D[size(yVr, 1), size(uVr, 1)];
          protected
           parameter Integer nD = size(dxVr, 1);
           parameter Integer nY = size(yVr, 1);
           parameter Integer nU = size(uVr, 1);
           parameter Integer nX = size(xVr, 1);
           parameter Integer zRef[nD + nY] = cat(1, dxVr, yVr);
           Real vec[nD + nY];
           parameter Real one[1] = { 1.0};
         algorithm
           for i in 1:nX loop
             vec := fmiGetDirectionalDerivative(
               fmi,zRef,{ xVr[i]}, one);
             A[:, i] := vec[1:nD];
             C[:, i] := vec[nD + 1:end];
           end for;
           for i in 1:nU loop
             vec := fmiGetDirectionalDerivative(
             fmi,zRef,{ uVr[i]}, one);
             B[:, i] := vec[1:nD];
             D[:, i] := vec[nD + 1:end];
           end for;
         end linearizeFMU;

         function fmiSaveFMUState
           input fmiModel fmi;
           external"C" BOP65666263844538138976893362_fmiSaveFMUState2(fmi)
             annotation (Include="
#ifndef BOP65666263844538138976893362_fmiSaveFMUState_C
#define BOP65666263844538138976893362_fmiSaveFMUState_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSaveFMUState2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiGetFMUstate(a->m, &a->dyFMUstate);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiGetFMUstate failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiSaveFMUState;

         function fmiRestoreFMUState
           input fmiModel fmi;
           external"C" BOP65666263844538138976893362_fmiRestoreFMUState2(fmi)
             annotation (Include="
#ifndef BOP65666263844538138976893362_fmiRestoreFMUState_C
#define BOP65666263844538138976893362_fmiRestoreFMUState_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiRestoreFMUState2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiSetFMUstate(a->m, a->dyFMUstate);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiSetFMUstate failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiRestoreFMUState;

         function fmiSerializeFMUstate
           input fmiModel fmi;
           external"C" BOP65666263844538138976893362_fmiSerializeFMUstate2(fmi)
             annotation (Include="
#ifndef BOP65666263844538138976893362_fmiSerializeFMUstate_C
#define BOP65666263844538138976893362_fmiSerializeFMUstate_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiSerializeFMUstate2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiGetFMUstate(a->m, &a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiGetFMUstate failed\");
    status = a->dyFmiSerializedFMUstateSize(a->m, a->dyFMUstate, &a->dyFMUStateSize);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSerializedFMUstateSize failed\");
    if( a->dySerializeFMUstate) free(a->dySerializeFMUstate); a->dySerializeFMUstate = NULL;
    a->dySerializeFMUstate = malloc(a->dyFMUStateSize);
    if(!a->dySerializeFMUstate)  ModelicaError(\"malloc call to allocate SerializeFMUstate failed\");
    status = a->dyFmiSerializeFMUstate(a->m, a->dyFMUstate, a->dySerializeFMUstate, a->dyFMUStateSize);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSerializeFMUstate failed\");
  }
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiSerializeFMUstate;

         function fmiDeSerializeFMUstate
           input fmiModel fmi;
           external"C" BOP65666263844538138976893362_fmiDeSerializeFMUstate2(fmi)
             annotation (Include="
#ifndef BOP65666263844538138976893362_fmiDeSerializeFMUstate_C
#define BOP65666263844538138976893362_fmiDeSerializeFMUstate_C 1
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiDeSerializeFMUstate2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    if(!a->dySerializeFMUstate) ModelicaError(\"serializedFmuState is not allocated!!!\");
    if(a->dyFMUstate){
      a->dyFmiFreeFMUstate(a->m, &a->dyFMUstate);
      a->dyFMUstate = NULL;    }
    status = a->dyFmiDeSerializeFMUstate(a->m, a->dySerializeFMUstate, a->dyFMUStateSize, &a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiDeSerializeFMUstate failed\");
    status = a->dyFmiSetFMUstate(a->m, a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSetFMUstate failed\");
  }
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiDeSerializeFMUstate;

         function fmiEnterSlaveInitializationMode
           input fmiModel fmi;
           input Real relativeTolerance;
           input Real tStart;
           input Boolean forceShutDownAtTStop;
           input Real tStop;
           input Real preAvailable;
           output Real postAvailable = preAvailable;
           external"C" BOP65666263844538138976893362_fmiEnterSlaveInitializationMode2(fmi, relativeTolerance, tStart, forceShutDownAtTStop, tStop)
           annotation (Include="
#ifndef BOP65666263844538138976893362_fmiEnterSlaveInitializationMode_C
#define BOP65666263844538138976893362_fmiEnterSlaveInitializationMode_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiEnterSlaveInitializationMode2(void*m, double relativeTolerance, double tStart, int forceShutDownAtTStop, double tStop) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    if(a->currentMode==dyfmi2InstantiationMode){
      status=a->dyFmiSetupExperiment(a->m, fmi2True, relativeTolerance, tStart, forceShutDownAtTStop, tStop);
      status=a->dyFmiEnterInitializationMode(a->m);
      a->dyTriggered=0;
      a->dyLastTime=a->dyTime;
      a->currentMode=dyfmi2InitializationMode;
    }else{
      status=fmi2OK;
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"InitializeSlave failed\");
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiEnterSlaveInitializationMode;

         function fmiExitSlaveInitializationMode
           input fmiModel fmi;
           input Real preAvailable;
           output Real postAvailable = preAvailable;
           external"C" BOP65666263844538138976893362_fmiExitSlaveInitializationMode2(fmi)
           annotation (Include="
#ifndef BOP65666263844538138976893362_fmiExitSlaveInitializationMode_C
#define BOP65666263844538138976893362_fmiExitSlaveInitializationMode_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void BOP65666263844538138976893362_fmiExitSlaveInitializationMode2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiExitInitializationMode(a->m);
    a->dyTriggered=0;
    a->dyLastTime=a->dyTime;
    a->currentMode = dyfmi2EventMode;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiExitModelInitialization failed!\");
  return;
}
#endif",      Library="BOP", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/BOP/binaries");
            annotation (__Dymola_CriticalRegion="BOP");
         end fmiExitSlaveInitializationMode;

         function 'from_rev/min'
           input Real 'rev/min';
           output Real BaseUnit(unit="rad/s");
         algorithm
           BaseUnit := 0.10471975511965977*'rev/min';
         end 'from_rev/min';

         function 'to_rev/min'
           input Real BaseUnit(unit="rad/s");
           output Real 'rev/min';
         algorithm
           'rev/min' := 9.549296585513721*BaseUnit;
         end 'to_rev/min';
     end fmi_Functions;
     initial equation
     equation
       when initial() then
         fmi = fmi_Functions.fmiModel(fmi_instanceName, fmi_loggingOn, fmi_resourceLocation);
       end when;
     initial algorithm
       fmi_Functions.fmiSetReal(fmi, {16777216, 16777217, 16777218, 16777219, 16777220, 16777221, 16777222, 16777229, 16777230, 16777231, 16777232, 16777235, 16777236, 16777237, 16777238, 16777239, 16777240, 16777244, 16777245, 16777246, 16777247, 16777248, 16777250, 16777251, 16777252, 16777253, 16777255, 16777256, 16777257, 16777258, 16777259, 16777260, 16777261, 16777262, 16777263, 16777264, 16777265, 16777266, 16777271, 16777272, 16777273, 16777274, 16777275, 16777276, 16777277, 16777278, 16777279, 16777280, 16777285, 16777286, 16777287, 16777288, 16777289, 16777290, 16777291, 16777292, 16777293, 16777294, 16777302, 16777303, 16777304, 16777305, 16777308, 16777309, 16777310, 16777311, 16777312, 16777313, 16777317, 16777322, 16777324, 16777325, 16777327, 16777328, 16777329, 16777334, 16777339, 16777340, 16777341, 16777342, 16777343, 16777345, 16777351, 16777352, 16777353, 16777354, 16777355, 16777356, 16777357, 16777358, 16777359, 16777360, 16777365, 16777366, 16777368, 16777369, 16777370, 16777375, 16777380, 16777381}, {Tmax_synop, SpHP_Q, SpHP_H, SpBP_Q, SpBP_H, CvmaxSurch, Valve_HP.Cvmax, Pump_HP.VRot, Pump_HP.MPower, Pump_HP.VRotn, Pump_HP.rm, Pump_HP.a1, Pump_HP.a2, Pump_HP.a3, Pump_HP.b1, Pump_HP.b2, Pump_HP.b3, sourceQ1.Q0, sourceQ1.h0, sinkP1.P0, sinkP1.T0, sinkP1.h0, BOP_HeatSink.V, BOP_HeatSink.A, BOP_HeatSink.Vf0, BOP_HeatSink.P0, BOP_HeatSink.Ccond, BOP_HeatSink.Cevap, BOP_HeatSink.Xlo, BOP_HeatSink.Xvo, BOP_HeatSink.Kvl, BOP_HeatSink.L, BOP_HeatSink.D, BOP_HeatSink.e, BOP_HeatSink.z1, BOP_HeatSink.z2, BOP_HeatSink.rugosrel, BOP_HeatSink.lambda, Turb_HP.W_fric, Turb_HP.eta_stato, Turb_HP.area_nz, Turb_HP.eta_nz, Turb_HP.Qmax, Turb_HP.eta_is_nom, Turb_HP.eta_is_min, Turb_HP.a, Turb_HP.b, Turb_HP.c, Turb_LP1_a.W_fric, Turb_LP1_a.eta_stato, Turb_LP1_a.area_nz, Turb_LP1_a.eta_nz, Turb_LP1_a.Qmax, Turb_LP1_a.eta_is_nom, Turb_LP1_a.eta_is_min, Turb_LP1_a.a, Turb_LP1_a.b, Turb_LP1_a.c, Pump_BP.VRot, Pump_BP.MPower, Pump_BP.VRotn, Pump_BP.rm, Pump_BP.a1, Pump_BP.a2, Pump_BP.a3, Pump_BP.b1, Pump_BP.b2, Pump_BP.b3, singularPressureLoss3.K, Extraction_ReheatHP.alpha, lumpedStraightPipe.L, lumpedStraightPipe.D, lumpedStraightPipe.lambda, lumpedStraightPipe.z1, lumpedStraightPipe.z2, singularPressureLoss2.K, PressureSet_TurbHP_In.k, pI1.k, pI1.Ti, pI1.ureset0, Generator.eta, Dryer.eta, Turb_LP2.W_fric, Turb_LP2.eta_stato, Turb_LP2.area_nz, Turb_LP2.eta_nz, Turb_LP2.Qmax, Turb_LP2.eta_is_nom, Turb_LP2.eta_is_min, Turb_LP2.a, Turb_LP2.b, Turb_LP2.c, lumpedStraightPipe1.L, lumpedStraightPipe1.D, lumpedStraightPipe1.lambda, lumpedStraightPipe1.z1, lumpedStraightPipe1.z2, singularPressureLoss4.K, volumeC1.V, volumeC1.P0});
       fmi_Functions.fmiSetReal(fmi, {16777382, 16777385, 16777390, 16777391, 16777392, 16777395, 16777396, 16777397, 16777398, 16777399, 16777405, 16777410, 16777411, 16777412, 16777413, 16777414, 16777415, 16777416, 16777417, 16777418, 16777419, 16777424, 16777428, 16777429, 16777430, 16777433, 16777435, 16777436, 16777437, 16777450, 16777451, 16777452, 16777472, 16777473, 16777474, 16777477, 16777490, 16777491, 16777492, 16777493, 16777494, 16777495, 16777496, 16777497, 16777498, 16777500, 16777501, 16777502, 16777512, 16777514, 16777515, 16777516, 16777517, 16777518, 16777519, 16777525, 16777532, 16777533, 16777534, 16777535, 16777536, 16777537, 16777538, 16777551, 16777552, 16777553, 16777554, 16777555, 16777556, 16777557, 16777565, 16777566, 16777567, 16777569, 16777570, 16777571, 16777572, 16777573, 16777574, 16777575, 16777576, 16777577, 16777582, 16777583, 16777584, 16777585, 16777586, 16777587, 16777588, 16777589, 16777590, 16777591, 16777592, 16777593, 16777594, 16777595, 16777596, 16777597, 16777598, 16777599}, {volumeC1.h0, singularPressureLoss5.K, Bache_a.V, Bache_a.P0, Bache_a.h0, SuperHeat.Kc, SuperHeat.z1c, SuperHeat.z2c, SuperHeat.z1f, SuperHeat.z2f, singularPressureLoss6.K, Turb_LP1_b.W_fric, Turb_LP1_b.eta_stato, Turb_LP1_b.area_nz, Turb_LP1_b.eta_nz, Turb_LP1_b.Qmax, Turb_LP1_b.eta_is_nom, Turb_LP1_b.eta_is_min, Turb_LP1_b.a, Turb_LP1_b.b, Turb_LP1_b.c, Extraction_ReheatBP.alpha, Bache_b.V, Bache_b.P0, Bache_b.h0, Extraction_TurbBP1a_Outlet.alpha, Reheat_BP.lambdaE, Reheat_BP.KCond, Reheat_BP.KPurge, ReHeat_HP.lambdaE, ReHeat_HP.KCond, ReHeat_HP.KPurge, volumeC2.V, volumeC2.P0, volumeC2.h0, singularPressureLoss1.K, pI5.k, pI5.Ti, pI5.ureset0, rampe3.Starttime, rampe3.Duration, rampe3.Initialvalue, rampe3.Finalvalue, Pipe_SGs.L, Pipe_SGs.D, Pipe_SGs.lambda, Pipe_SGs.z1, Pipe_SGs.z2, Extraction_TurbHP_Oulet.alpha, Ideal_BOP2HeatNetWork_HX.Kc, Ideal_BOP2HeatNetWork_HX.Kf, Ideal_BOP2HeatNetWork_HX.z1c, Ideal_BOP2HeatNetWork_HX.z2c, Ideal_BOP2HeatNetWork_HX.z1f, Ideal_BOP2HeatNetWork_HX.z2f, Valve_Hybrid_IP.Cvmax, pI6.k, pI6.Ti, pI6.ureset0, Vapor_MassFlowRamp_extracted4Cogeneration.Starttime, Vapor_MassFlowRamp_extracted4Cogeneration.Duration, Vapor_MassFlowRamp_extracted4Cogeneration.Initialvalue, Vapor_MassFlowRamp_extracted4Cogeneration.Finalvalue, rampe2.Starttime, rampe2.Duration, rampe2.Initialvalue, rampe2.Finalvalue, sinkP.P0, sinkP.T0, sinkP.h0, sinkP2.P0, sinkP2.T0, sinkP2.h0, rampe4.Starttime, rampe4.Duration, rampe4.Initialvalue, rampe4.Finalvalue, sourceQ_4FMU.Q0, sourceQ_4FMU.h0, sinkP_4FMU.P0, sinkP_4FMU.T0, sinkP_4FMU.h0, InletFlow_FromHeatNetwork.m_flow, InletFlow_FromHeatNetwork.h, InletFlow_FromHeatNetwork.'X[1]', InletFlowRate_FromHeatNetWork.height, InletFlowRate_FromHeatNetWork.duration, InletFlowRate_FromHeatNetWork.offset, InletFlowRate_FromHeatNetWork.startTime, BackPressure_FromHeatNetWork.p, BackPressure_FromHeatNetWork.h, BackPressure_FromHeatNetWork.'X[1]', Test_Timetable_Rotation_PumpHP.'Table[1,1]', Test_Timetable_Rotation_PumpHP.'Table[1,2]', Test_Timetable_Rotation_PumpHP.'Table[2,1]', Test_Timetable_Rotation_PumpHP.'Table[2,2]', Test_Timetable_Rotation_PumpHP.'Table[3,1]', Test_Timetable_Rotation_PumpHP.'Table[3,2]', Test_Timetable_Rotation_PumpHP.'Table[4,1]', Test_Timetable_Rotation_PumpHP.'Table[4,2]'});
       fmi_Functions.fmiSetReal(fmi, {16777600, 16777601, 16777602, 16777603, 16777604, 16777605, 16777606, 16777607, 16777608, 16777609, 16777610, 16777611, 16777612, 16777613, 16777614, 16777615, 16777616, 16777617, 16777618, 16777619, 16777620, 16777621, 16777622, 16777623, 16777624, 16777625, 16777626, 16777627}, {Test_Timetable_Rotation_PumpHP.'Table[5,1]', Test_Timetable_Rotation_PumpHP.'Table[5,2]', Test_Timetable_Rotation_PumpHP.'Table[6,1]', Test_Timetable_Rotation_PumpHP.'Table[6,2]', Test_Timetable_Rotation_PumpHP.'Table[7,1]', Test_Timetable_Rotation_PumpHP.'Table[7,2]', Test_Timetable_Rotation_PumpHP.'Table[8,1]', Test_Timetable_Rotation_PumpHP.'Table[8,2]', Test_Timetable_Rotation_PumpHP.'Table[9,1]', Test_Timetable_Rotation_PumpHP.'Table[9,2]', Test_Timetable_Rotation_PumpLP.'Table[1,1]', Test_Timetable_Rotation_PumpLP.'Table[1,2]', Test_Timetable_Rotation_PumpLP.'Table[2,1]', Test_Timetable_Rotation_PumpLP.'Table[2,2]', Test_Timetable_Rotation_PumpLP.'Table[3,1]', Test_Timetable_Rotation_PumpLP.'Table[3,2]', Test_Timetable_Rotation_PumpLP.'Table[4,1]', Test_Timetable_Rotation_PumpLP.'Table[4,2]', Test_Timetable_Rotation_PumpLP.'Table[5,1]', Test_Timetable_Rotation_PumpLP.'Table[5,2]', Test_Timetable_Rotation_PumpLP.'Table[6,1]', Test_Timetable_Rotation_PumpLP.'Table[6,2]', Test_Timetable_Rotation_PumpLP.'Table[7,1]', Test_Timetable_Rotation_PumpLP.'Table[7,2]', Test_Timetable_Rotation_PumpLP.'Table[8,1]', Test_Timetable_Rotation_PumpLP.'Table[8,2]', Test_Timetable_Rotation_PumpLP.'Table[9,1]', Test_Timetable_Rotation_PumpLP.'Table[9,2]'});
       fmi_Functions.fmiSetInteger(fmi, {16777223, 16777224, 16777225, 16777233, 16777234, 16777249, 16777267, 16777268, 16777281, 16777282, 16777283, 16777284, 16777295, 16777296, 16777297, 16777298, 16777299, 16777306, 16777307, 16777318, 16777319, 16777323, 16777326, 16777330, 16777331, 16777335, 16777336, 16777346, 16777347, 16777348, 16777361, 16777362, 16777363, 16777364, 16777367, 16777371, 16777372, 16777376, 16777377, 16777383, 16777384, 16777386, 16777387, 16777393, 16777394, 16777400, 16777401, 16777402, 16777406, 16777407, 16777420, 16777421, 16777422, 16777423, 16777425, 16777426, 16777427, 16777431, 16777432, 16777434, 16777438, 16777439, 16777440, 16777441, 16777442, 16777443, 16777444, 16777446, 16777447, 16777453, 16777454, 16777455, 16777456, 16777457, 16777458, 16777459, 16777461, 16777462, 16777465, 16777466, 16777467, 16777468, 16777469, 16777470, 16777471, 16777475, 16777476, 16777478, 16777479, 16777482, 16777483, 16777484, 16777488, 16777499, 16777503, 16777504, 16777507, 16777508, 16777513, 16777520}, {Valve_HP.option_interpolation, Valve_HP.fluid, Valve_HP.mode, Pump_HP.fluid, Pump_HP.mode, sinkP1.mode, BOP_HeatSink.ntubes, BOP_HeatSink.mode, Turb_HP.fluid, Turb_HP.mode_e, Turb_HP.mode_s, Turb_HP.mode_ps, Turb_LP1_a.fluid, Turb_LP1_a.mode_e, Turb_LP1_a.mode_s, Turb_LP1_a.mode_ps, Tout_SG.mode, Pump_BP.fluid, Pump_BP.mode, singularPressureLoss3.fluid, singularPressureLoss3.mode, Extraction_ReheatHP.mode_e, lumpedStraightPipe.ntubes, lumpedStraightPipe.fluid, lumpedStraightPipe.mode, singularPressureLoss2.fluid, singularPressureLoss2.mode, Dryer.mode_e, invSingularPressureLoss.fluid, invSingularPressureLoss.mode, Turb_LP2.fluid, Turb_LP2.mode_e, Turb_LP2.mode_s, Turb_LP2.mode_ps, lumpedStraightPipe1.ntubes, lumpedStraightPipe1.fluid, lumpedStraightPipe1.mode, singularPressureLoss4.fluid, singularPressureLoss4.mode, volumeC1.fluid, volumeC1.mode, singularPressureLoss5.fluid, singularPressureLoss5.mode, Bache_a.fluid, Bache_a.mode, SuperHeat.modec, SuperHeat.modecs, SuperHeat.modef, singularPressureLoss6.fluid, singularPressureLoss6.mode, Turb_LP1_b.fluid, Turb_LP1_b.mode_e, Turb_LP1_b.mode_s, Turb_LP1_b.mode_ps, Extraction_ReheatBP.mode_e, splitter_SGoutlet.fluid, splitter_SGoutlet.mode, Bache_b.fluid, Bache_b.mode, Extraction_TurbBP1a_Outlet.mode_e, Reheat_BP.mode_eeF, Reheat_BP.mode_seF, Reheat_BP.mode_evC, Reheat_BP.mode_mF, Reheat_BP.mode_epC, Reheat_BP.mode_spC, Reheat_BP.mode_flash, invSingularPressureLoss1.fluid, invSingularPressureLoss1.mode, ReHeat_HP.mode_eeF, ReHeat_HP.mode_seF, ReHeat_HP.mode_evC, ReHeat_HP.mode_mF, ReHeat_HP.mode_epC, ReHeat_HP.mode_spC, ReHeat_HP.mode_flash, invSingularPressureLoss2.fluid, invSingularPressureLoss2.mode, Rend_Cycle.significantDigits, Pw_reseau.significantDigits, Pw_COG.significantDigits, Temp_COG.significantDigits, Heat_Power.significantDigits, Temp_inletGV.significantDigits, Pw_GV.significantDigits, volumeC2.fluid, volumeC2.mode, singularPressureLoss1.fluid, singularPressureLoss1.mode, Valve_superheat.option_interpolation, Valve_superheat.fluid, Valve_superheat.mode, Tin_Turb_LP1a.mode, Pipe_SGs.ntubes, Pipe_SGs.fluid, Pipe_SGs.mode, singularPressureLoss_SG_Out.fluid, singularPressureLoss_SG_Out.mode, Extraction_TurbHP_Oulet.mode_e, Ideal_BOP2HeatNetWork_HX.modec});
       fmi_Functions.fmiSetInteger(fmi, {16777521, 16777522, 16777526, 16777527, 16777528, 16777540, 16777543, 16777545, 16777546, 16777547, 16777558, 16777559, 16777560, 16777561, 16777568, 16777578, 16777579, 16777580}, {Ideal_BOP2HeatNetWork_HX.modecs, Ideal_BOP2HeatNetWork_HX.modef, Valve_Hybrid_IP.option_interpolation, Valve_Hybrid_IP.fluid, Valve_Hybrid_IP.mode, T_Outlet_BOP_HX_HeatNetWork.mode, Tout_Turb_HP.mode, Valve_superheat1.option_interpolation, Valve_superheat1.fluid, Valve_superheat1.mode, sinkP.mode, Valve_superheat2.option_interpolation, Valve_superheat2.fluid, Valve_superheat2.mode, sinkP2.mode, sinkP_4FMU.option_temperature, sinkP_4FMU.mode, T_2HeatNetWork.mode});
       fmi_Functions.fmiSetBoolean(fmi, {16777254}, {BOP_HeatSink.gravity_pressure});
       fmi_Functions.fmiSetReal(fmi, {100663305, 100663306, 100663307, 100663308, 100663309, 100663310, 100663311, 100663312, 369098781, 369098798, 369098812, 369098818, 369098819, 369098825, 369098826, 369098873, 905969854, 369098958, 369098959, 369098986, 369099017, 369099041, 369099044, 369099045, 369099085, 369099091, 369099105, 369099111, 369099112, 369099113, 369099118, 369099119, 369099141, 369099209, 369099210, 369099227, 369099231, 369099257, 369099263, 369099275, 369099278, 369099328, 369099329, 637534788, 637534800, 369099353, 369099401, 369099402, 369099419, 369099423, 33554437, 33554438, 369099530, 369099531, 369099535, 369099536, 369099541, 369099545, 369099626, 369099649, 369099675, 33554439, 369099810, 369099816, 369099817, 369099818, 369099825, 369100017, 369100018, 369100019, 369100026, 369100099, 33554440, 369100337, 369100338, 905971272, 369100361, 369100412, 369100423, 905971338, 369100438, 369100539, 369100542, 369100577}, {_CsHP_start, _CsBP1a_start, _CsBP1b_start, _CsBP2_start, _ScondesHP_start, _SPurgeHP_start, _ScondesBP_start, _SPurgeBP_start, Valve_HP._Q_start, Valve_HP.C1._h_vol_start, Pump_HP._Qv_start, Pump_HP._Pm_start, Pump_HP._h_start, Pump_HP.C2._h_vol_start, Pump_HP.C2._h_start, BOP_HeatSink._Pfond_start, BOP_HeatSink.proe._d_start, Turb_HP._Pe_start, Turb_HP._Ps_start, Turb_HP.Cs._h_start, Turb_LP1_a._Pe_start, Turb_LP1_a.Ce._h_vol_start, Turb_LP1_a.Cs._h_vol_start, Turb_LP1_a.Cs._h_start, Tout_SG.C2._h_vol_start, Pe_ValveHP.C1._h_vol_start, Pump_BP._Qv_start, Pump_BP._Pm_start, Pump_BP._h_start, Pump_BP.C1._P_start, Pump_BP.C2._h_vol_start, Pump_BP.C2._h_start, singularPressureLoss3.C2._h_vol_start, lumpedStraightPipe.C2._P_start, lumpedStraightPipe.C2._h_vol_start, singularPressureLoss2._Pm_start, singularPressureLoss2.C2._h_vol_start, sensorP1.C1._h_vol_start, Dryer._h_start, Dryer.Cev._h_start, Dryer.Csv._h_start, Turb_LP2._Pe_start, Turb_LP2._Ps_start, Turb_LP2._xm_start, Turb_LP2.pros._d_start, Turb_LP2.Ce._h_vol_start, lumpedStraightPipe1.C2._P_start, lumpedStraightPipe1.C2._h_vol_start, singularPressureLoss4._Pm_start, singularPressureLoss4.C2._h_vol_start, volumeC1._h_start, Bache_a._h_start, SuperHeat._DPfc_start, SuperHeat._DPgc_start, SuperHeat._DPf_start, SuperHeat.Ec._h_start, SuperHeat.Sf._h_vol_start, SuperHeat.Sc._h_vol_start, singularPressureLoss6._Pm_start, Turb_LP1_b._Pe_start, Turb_LP1_b.Cs._h_start, Bache_b._h_start, Extraction_TurbBP1a_Outlet.Cex._Q_start, Reheat_BP._SDes_start, Reheat_BP._HeiF_start, Reheat_BP._HDesF_start, Reheat_BP._Hep_start, ReHeat_HP._SDes_start, ReHeat_HP._HeiF_start, ReHeat_HP._HDesF_start, ReHeat_HP._Hep_start, ReHeat_HP.promeF._d_start, volumeC2._h_start, Pipe_SGs.C1._P_start, Pipe_SGs.C1._h_vol_start, singularPressureLoss_SG_Out.C1._P_start, singularPressureLoss_SG_Out.C1._h_vol_start, Extraction_TurbHP_Oulet.Cex._h_start, Ideal_BOP2HeatNetWork_HX._DPc_start, Ideal_BOP2HeatNetWork_HX._DPf_start, Ideal_BOP2HeatNetWork_HX.Sc._h_vol_start, Valve_Hybrid_IP.C1._h_vol_start, Valve_Hybrid_IP.C2._h_vol_start, Pout_Turb_HP.C1._h_vol_start});
         fmi_Functions.fmiSetReal(fmi, {352321536}, {_Hin_BC_start});
         fmi_Functions.fmiSetReal(fmi, {352321537}, {_Qin_BC_start});
         fmi_Functions.fmiSetReal(fmi, {352321538}, {_Pout_BC_start});
         fmi_Functions.fmiEnterSlaveInitializationMode(fmi, fmi_rTol, fmi_StartTime, fmi_forceShutDownAtStopTime, fmi_StopTime, 1);
       fmi_rdum := 1;
       fmi_idum := 1;
       myTimeStart :=time;
     algorithm
     assert(fmi_CommunicationStepSize > 0.0,"The parameter fmi_CommunicationStepSize has an invalid value, please set a positive value larger than 0.0");
       when {initial(), sample(fmi_StartTime, fmi_CommunicationStepSize)} then
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321536}, {_Hin_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321536}, {pre(Hin_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321536}, {Hin_BC});
           end if;
         end if;
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321537}, {_Qin_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321537}, {pre(Qin_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321537}, {Qin_BC});
           end if;
         end if;
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321538}, {_Pout_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321538}, {pre(Pout_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321538}, {Pout_BC});
           end if;
         end if;
           if fmi_rdum >= 0  and fmi_idum >= 0 and not fmi_exitInit and not initial() then
             fmi_Functions.fmiExitSlaveInitializationMode(fmi, 1);
             fmi_exitInit:=true;
     RealFixedLocal :=fmi_Functions.fmiGetRealwf(
                 fmi,
                 {100663305,100663306,100663307,100663308,100663309,100663310,
                100663311,100663312,100663318,234881052,234881053,234881054,
                234881055,100663391,100663392,100663393,100663394,100663395,
                100663396,100663397,100663398,100663399,100663400,100663401,
                100663408,100663409,100663416,100663495,100663553,100663743,
                234881147,234881150,100663864,100663932,100664067,100664185,
                100664358,100664359,100664559,100664560,234881385,100664813,
                100664817,234881300,100664873,100664898,234881397,100665062,
                234881342,100665143,100665147,234881363,100665170,234881364,
                100665174,100665175,100665176,100665177,100665178,100665179,
                100665180,100665181,100665182,100665183,100665189,100665193,
                234881373,100665216,234881374,100665220,100665221,100665222,
                100665223,100665224,100665225,100665226,100665227,100665228,
                100665229,234881391,100665286,234881398,100665295,100665296,
                100665298,100665304,100665305,100665306},
                 fmi_rdum);
     IntegerFixedLocal :=fmi_Functions.fmiGetIntegerwf(
                 fmi,
                 {100665301},
                 fmi_idum);
           end if;
         if time>=fmi_CommunicationStepSize +fmi_StartTime then
           for stepSizeIndex in 1:stepSizeScaleFactor loop
             fmi_StepOK :=fmi_Functions.fmiDoStep(
                   fmi,
                   time + (stepSizeIndex - 1 - stepSizeScaleFactor)*
                  fmi_CommunicationStepSize/stepSizeScaleFactor,
                   fmi_CommunicationStepSize/stepSizeScaleFactor,
                   1);
           end for;
           fmi_flip :=not pre(fmi_flip);
         end if;
         if not initial() then
           internal.Pin_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544320);
           internal.Qout_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544321);
           internal.Hout_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544322);
         end if;
     RealVariables1 :=fmi_Functions.fmiGetReal(fmi, {905969664,905969665,
            905969666,905969667,905969668,905969669,905969670,905969671,
            905969672,369098780,369098781,369098782,369098783,369098786,
            369098784,369098785,369098787,369098788,369098789,369098790,
            369098791,369098792,369098793,369098794,369098795,369098796,
            369098797,369098798,369098809,369098810,369098811,369099773,
            369098812,905969725,905969726,369098815,369098830,369098816,
            369098817,369098818,369098819,369098820,369098821,369098824,
            369098825,369098826,369098829,369098831,369098832,369098833,
            369098834,369098835,369098836,369098837,369098838,369098839,
            905969752,905969753,905969754,905969755,905969770,905969771,
            905969772,905969775,905969782,905969783,33554432,369098873,
            33554433,33554434,637534340,637534350,369098874,637534331,33554435,
            637534349,637534359,637534341,637534351,637534332,637534333,
            637534334,637534335,637534336,637534337,637534338,637534339,
            637534342,637534343,637534344,637534345,637534346,637534347,
            637534348,637534352,637534353,637534354,637534355,637534356,
            637534357});
     RealVariables2 :=fmi_Functions.fmiGetReal(fmi, {637534358,637534360,
            637534361,637534362,637534363,637534364,637534365,637534366,
            637534367,637534368,637534369,637534370,637534371,637534372,
            637534373,369098918,369098919,369098922,369098923,905969838,
            369098927,369098928,369098929,369098930,369098931,369098932,
            369098933,369098934,369098935,369098936,905969853,905969854,
            905969855,905969856,905969857,905969858,905969859,905969860,
            905969861,905969862,369098954,905969867,905969868,369098992,
            369098957,369098958,369098959,369098963,369098973,369098960,
            369098961,369098962,369098964,369098965,369098966,369098967,
            369098968,369098969,369098970,369098971,369098972,369098974,
            369098975,369098976,369098977,369098978,369098979,369098980,
            369098981,369098982,905969897,369098986,369098989,369098990,
            369098991,369098993,369098994,369098995,369098996,905970162,
            369098999,369099000,369099001,369099002,369099003,369099004,
            369099005,369099006,369099007,369099008,369099012,905969925,
            905969926,369099015,369099051,369099016,369099017,369099021,
            369099031,369099018});
     RealVariables3 :=fmi_Functions.fmiGetReal(fmi, {369099019,369099020,
            369099022,369099023,369099024,369099025,369099026,369099027,
            369099028,369099029,369099030,369099032,369099033,369099034,
            369099035,369099036,369099037,369099038,369099039,369099040,
            369099041,369099044,369099045,369099048,369099049,369099050,
            369099052,369099053,369099054,369099055,905970163,369099058,
            369099059,369099060,369099061,369099062,369099063,369099064,
            369099065,369099066,369099067,905971251,905969983,369099070,
            905969984,905969985,905969986,905969987,905969988,905969989,
            905969990,905969991,905969992,905969993,905969994,369099085,
            369099090,369099091,369099102,369099103,369099104,369099105,
            905970018,905970019,369099108,369099123,369099109,369099110,
            369099111,369099112,369099113,369099114,369099117,369099118,
            369099119,369099122,369099124,369099125,369099126,369099127,
            369099128,369099129,369099130,369099131,369099132,369099136,
            369099145,369099144,369099137,369099140,369099141,369099146,
            369099147,369099148,369099149,369099150,369099151,369099152,
            369099153,369099154});
     RealVariables4 :=fmi_Functions.fmiGetReal(fmi, {369099155,369099156,
            369099157,369099158,369099159,369099160,369099161,369099162,
            369099163,369099164,369099167,369099168,369099171,369099172,
            369099173,369099174,369099175,369099176,369099177,369099178,
            369099179,369099180,369099181,369099182,369099183,369099184,
            369099185,369099186,369099187,369099200,369099201,905970114,
            369099214,905970117,369099213,369099206,369099209,369099210,
            369099215,369099216,369099217,369099218,369099219,369099220,
            369099221,369099222,369099226,369099235,369099234,369099227,
            369099230,369099231,369099236,369099237,369099238,369099239,
            369099240,369099241,369099242,369099243,369099244,33554436,
            905970160,905970164,905970165,369099256,369099504,369099257,
            369099258,369099263,369099273,369099264,369099265,369099266,
            369099267,369099268,369099269,369099270,369099271,369099272,
            369099274,369099275,369099278,369099281,369099282,369099283,
            369099284,369099285,369099286,369099287,369099288,369099289,
            369099290,369099291,369099292,369099293,369099294,369099295,
            369099296,369099297});
     RealVariables5 :=fmi_Functions.fmiGetReal(fmi, {905970215,905970216,
            905970223,905970222,905970217,905970224,905970225,905970226,
            905970227,905970228,905970229,905970230,905970231,637534779,
            905970236,905970237,369099326,637534818,637534783,369099328,
            369099329,369099333,637534799,637534786,637534787,637534788,
            369099334,369099335,369099336,369099337,369099338,369099339,
            369099340,369099341,369099342,637534800,637534801,637534802,
            637534803,637534804,637534805,637534806,637534807,637534808,
            369099353,637534812,637534815,637534816,637534817,637534819,
            637534820,637534821,637534822,637534825,637534826,637534827,
            637534828,637534829,637534830,637534831,637534832,637534833,
            637534834,369099389,369099390,369099391,905970304,369099406,
            905970307,369099405,369099396,369099397,369099398,369099401,
            369099402,369099407,369099408,369099409,369099410,369099411,
            369099412,369099413,369099414,369099418,369099427,369099426,
            369099419,369099422,369099423,369099428,369099429,369099430,
            369099431,369099432,369099433,369099434,369099435,637534897,
            33554437,637534898});
     RealVariables6 :=fmi_Functions.fmiGetReal(fmi, {637534896,637534899,
            637534900,637534901,637534902,637534903,637534904,637534905,
            637534906,369099466,369099473,369099472,369099467,369099474,
            369099475,369099476,369099477,369099478,369099479,369099480,
            369099481,637534943,33554438,637534944,637534942,637534945,
            637534946,637534947,637534948,637534949,637534950,637534951,
            637534952,369099499,369099591,369100068,369100069,369099511,
            369099512,369099529,905970460,905970470,905970480,905970524,
            369099530,369099531,369099532,369099533,369099534,369099535,
            369099603,369099579,369099536,369099541,369099544,369099545,
            905970461,905970462,905970463,905970464,905970465,905970466,
            905970467,905970468,905970469,905970471,905970472,905970473,
            905970474,905970475,905970476,905970477,905970478,905970479,
            905970481,905970482,905970483,905970484,905970485,905970486,
            905970487,905970488,905970489,369099578,369099580,369099581,
            369099582,369099583,369099584,369099585,369099586,369099587,
            369099588,369099589,369099590,369099592,369099593,369099594,
            369099595,369099596});
     RealVariables7 :=fmi_Functions.fmiGetReal(fmi, {369099597,369099598,
            369099599,369099600,369099601,369099602,369099604,369099605,
            369099606,369099607,369099608,369099609,369099610,369099611,
            905970525,905970526,905970527,905970528,905970529,905970530,
            905970531,905970532,905970533,369099625,369099632,369099631,
            369099626,369099633,369099634,369099635,369099636,369099637,
            369099638,369099639,369099640,369099644,905970557,905970558,
            369099647,369099681,369099648,369099649,369099653,369099663,
            369099650,369099651,369099652,369099654,369099655,369099656,
            369099657,369099658,369099659,369099660,369099661,369099662,
            369099664,369099665,369099666,369099667,369099668,369099669,
            369099670,369099671,369099672,369099675,369099678,369099679,
            369099680,369099682,369099683,369099684,369099685,369099688,
            369099689,369099690,369099691,369099692,369099693,369099694,
            369099695,369099696,369099697,369099698,369099699,369099700,
            369099701,369099702,369099703,369099704,369099705,369099706,
            369099707,369099708,369099713,369099714,369099715,369099716,
            369099717,369099718});
     RealVariables8 :=fmi_Functions.fmiGetReal(fmi, {369099719,369099720,
            369099721,369099722,369099723,369099724,369099725,369099726,
            905970641,905970642,905970655,369099735,369099736,905970656,
            905970657,905970658,905970659,905970660,905970661,905970662,
            905970663,905970664,637535214,33554439,637535215,637535213,
            637535216,637535217,637535218,637535219,637535220,637535221,
            637535222,637535223,369099768,369100484,369099780,369099781,
            369099782,369099783,369099784,369099785,369099786,369099787,
            369099788,369099789,369099790,369099795,369099796,369099797,
            369099798,369099799,369099800,369099801,369099802,369099803,
            369099804,369099805,369099806,369099807,369099808,369099809,
            369099810,369099811,369099884,369099891,369099816,369099817,
            369099818,369099819,369099882,905970732,369099821,369099822,
            369099823,369099824,369099825,369099896,369099826,369099827,
            369099828,369099829,369099830,369099831,369099832,369099833,
            369099834,369099835,905970748,905970749,905970750,905970751,
            905970752,905970753,905970754,905970755,905970756,905970757,
            905970758,905970759});
     RealVariables9 :=fmi_Functions.fmiGetReal(fmi, {905970760,905970761,
            905970762,905970763,905970764,905970765,905970766,905970767,
            369099865,369099866,369099871,369099872,369099873,369099874,
            369099875,369099876,369099877,369099878,369099879,369099880,
            369099881,369099883,369099885,369099886,369099887,369099888,
            369099889,369099890,369099892,369099893,369099894,369099895,
            369099897,369099898,369099899,369099900,369099901,369099902,
            369099903,369099904,369099905,369099906,369099907,369099908,
            369099909,369099910,369099911,369099912,369099913,369099914,
            369099915,369099916,369099917,369099918,369099919,369099920,
            369099921,369099922,369099923,369099924,369099925,369099926,
            369099927,369099928,369099929,369099930,369099931,369099932,
            369099933,369099934,369099935,369099936,369099937,369099938,
            369099939,369099940,369099941,369099942,369099943,369099944,
            369099945,369099946,369099947,369099948,369099949,369099950,
            369099951,369099952,369099953,369099954,369099955,369099956,
            369099957,369099958,369099959,369099960,369099961,369099962,
            369099963,369099964});
     RealVariables10 :=fmi_Functions.fmiGetReal(fmi, {369099965,369099966,
            369099967,369099968,369099969,369099970,369099971,369099972,
            369099973,369099974,369099975,369099976,369099977,369099978,
            369099979,369099980,369099981,369099982,369099983,369099984,
            369099985,369099986,369099987,369099988,369099989,369099990,
            369099991,369099992,369099993,369099994,905970910,905970911,
            905970918,905970917,905970912,905970919,905970920,905970921,
            905970922,905970923,905970924,905970925,905970926,369100087,
            369100094,369100017,369100018,369100019,369100020,369100085,
            905970933,369100022,369100023,369100024,369100025,369100026,
            369100027,369100028,369100029,369100030,369100031,369100032,
            369100033,369100034,369100035,369100036,905970949,905970950,
            905970951,905970952,905970953,905970954,905970955,905970956,
            905970957,905970958,905970959,905970960,905970961,905970962,
            905970963,905970964,905970965,905970966,905970967,905970968,
            905970971,369100060,369100074,369100075,369100076,369100077,
            369100078,369100079,369100080,369100081,369100082,369100083,
            369100084,369100086});
     RealVariables11 :=fmi_Functions.fmiGetReal(fmi, {369100088,369100089,
            369100090,369100091,369100092,369100093,369100095,369100096,
            369100097,369100098,369100099,369100100,369100101,369100102,
            369100103,369100104,369100105,369100106,369100107,369100108,
            369100109,369100110,369100111,369100112,369100113,369100114,
            369100115,369100116,369100117,369100118,369100119,369100120,
            369100121,369100122,369100123,369100124,369100125,369100126,
            369100127,369100128,369100129,369100130,369100131,369100132,
            369100133,369100134,369100135,369100136,369100137,369100138,
            369100139,369100140,369100141,369100142,369100143,369100144,
            369100145,369100146,369100147,369100148,369100149,369100150,
            369100151,369100152,369100153,369100154,369100155,369100156,
            369100157,369100158,369100159,369100160,369100161,369100162,
            369100163,369100164,369100165,369100166,369100167,369100168,
            369100169,369100170,369100171,369100172,369100173,369100174,
            369100175,369100176,369100177,369100178,369100179,369100180,
            369100181,369100182,369100183,369100184,369100185,369100186,
            369100187,369100188});
     RealVariables12 :=fmi_Functions.fmiGetReal(fmi, {369100189,369100190,
            369100191,369100192,369100193,369100194,369100195,369100196,
            369100197,905971113,905971114,905971121,905971120,905971115,
            905971122,905971123,905971124,905971125,905971126,905971127,
            905971128,905971129,637535686,33554440,637535687,637535685,
            637535688,637535689,637535690,637535691,637535692,637535693,
            637535694,637535695,369100253,369100260,369100259,369100254,
            369100261,369100262,369100263,369100264,369100265,369100266,
            369100267,369100268,369100279,369100280,369100281,369100283,
            369100282,369100284,369100285,369100286,369100287,369100288,
            369100289,369100290,369100291,369100292,369100293,369100300,
            369100301,369100302,369100303,369100304,369100305,369100306,
            369100307,369100308,369100309,369100310,369100315,369100316,
            33554441,369100330,369100331,905971244,369100345,905971247,
            369100344,369100336,369100337,369100338,369100346,369100347,
            369100348,369100349,369100350,369100351,369100352,369100353,
            369100358,369100367,369100366,369100359,905971272,369100361,
            369100368,369100369});
     RealVariables13 :=fmi_Functions.fmiGetReal(fmi, {369100370,369100371,
            369100372,369100373,369100374,369100375,905971605,369100382,
            369100383,369100384,369100385,369100386,369100387,369100388,
            369100389,369100390,369100391,369100392,369100397,369100398,
            369100399,369100400,369100401,369100402,369100403,369100404,
            369100405,369100406,369100407,369100408,369100409,369100410,
            369100411,369100412,905971332,905971353,905971363,905971373,
            905971417,369100421,369100422,369100423,905971336,905971337,
            905971338,369100496,905971384,905971675,905971653,905971341,
            905971342,905971345,905971346,369100437,369100438,905971354,
            905971355,905971356,905971357,905971358,905971359,905971360,
            905971361,905971362,905971364,905971365,905971366,905971367,
            905971368,905971369,905971370,905971371,905971372,905971374,
            905971375,905971376,905971377,905971378,905971379,905971380,
            905971381,905971382,905971383,905971385,905971386,905971387,
            905971388,905971389,905971390,905971391,905971392,369100481,
            369100482,369100483,369100485,369100486,369100487,369100488,
            369100489,369100490});
     RealVariables14 :=fmi_Functions.fmiGetReal(fmi, {369100491,369100492,
            369100493,369100494,369100495,369100497,369100498,369100499,
            369100500,369100501,369100502,369100503,369100504,905971418,
            905971419,905971420,905971421,905971422,905971423,905971424,
            905971425,905971426,369100524,369100525,369100526,369100528,
            369100527,369100529,369100530,369100531,369100532,369100533,
            369100534,369100535,369100536,369100537,369100538,369100539,
            369100542,369100545,369100546,369100552,33554442,905971471,
            905971472,905971473,905971474,905971475,905971476,905971477,
            905971478,905971479,905971480,905971481,369100576,369100577,
            905971496,905971497,905971498,905971499,905971500,905971501,
            905971502,905971503,905971504,905971505,905971506,369100609,
            369100610,369100611,369100613,369100612,369100614,369100615,
            369100616,369100617,369100618,369100619,369100620,369100621,
            369100622,369100623,905971552,905971553,905971556,369100655,
            369100656,369100657,369100659,369100658,369100660,369100661,
            369100662,369100663,369100664,369100665,369100666,369100667,
            369100668,369100669});
     RealVariables15 :=fmi_Functions.fmiGetReal(fmi, {905971598,905971599,
            905971602,436207617,436207616,905971606,905971607,905971608,
            905971609,905971610,905971611,905971612,905971613,905971614,
            905971615,436207618,905971616,905971621,905971622,905971623,
            905971624,905971625,905971626,905971627,905971628,905971629,
            905971630,905971631,905971634,905971640,905971641,905971643,
            905971649,905971650,905971651,905971652,905971661});
     IntegerVariables :=fmi_Functions.fmiGetInteger(fmi, {905971646});
       end when;
       when {pre(fmi_flip), not pre(fmi_flip)} then
         _Hin_BC_old := pre(Hin_BC);
         _Qin_BC_old := pre(Qin_BC);
         _Pout_BC_old := pre(Pout_BC);
       end when;
     equation
       if initial() then
         Pin_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, fill(0.0,0), fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, fill(false,0), fill(0,0), fill(0,0), fill(0,0), fill(0,0), fill(0,0), fill(0,0), {335544320});
       else
         Pin_TSPro = internal.Pin_TSPro;
       end if;
       if initial() then
         Qout_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, {SpHP_Q, SpHP_H, SpBP_Q, SpBP_H, CvmaxSurch, Pump_HP.VRotn, Pump_HP.a1, Pump_HP.a2, Pump_HP.a3, Pump_HP.b1, Pump_HP.b2, Pump_HP.b3, BOP_HeatSink.V, BOP_HeatSink.A, BOP_HeatSink.Vf0, BOP_HeatSink.P0, Turb_HP.area_nz, Turb_HP.eta_nz, Turb_HP.Qmax, Turb_HP.eta_is_nom, Turb_HP.eta_is_min, Turb_HP.a, Turb_HP.b, Turb_HP.c, Turb_LP1_a.area_nz, Turb_LP1_a.eta_nz, Turb_LP1_a.Qmax, Turb_LP1_a.eta_is_nom, Turb_LP1_a.eta_is_min, Turb_LP1_a.a, Turb_LP1_a.b, Turb_LP1_a.c, Pump_BP.VRotn, Pump_BP.a1, Pump_BP.a2, Pump_BP.a3, Pump_BP.b1, Pump_BP.b2, Pump_BP.b3, singularPressureLoss3.K, Extraction_ReheatHP.alpha, lumpedStraightPipe.L, lumpedStraightPipe.D, lumpedStraightPipe.z1, lumpedStraightPipe.z2, singularPressureLoss2.K, PressureSet_TurbHP_In.k, pI1.Ti, Dryer.eta, Turb_LP2.area_nz, Turb_LP2.eta_nz, Turb_LP2.Qmax, Turb_LP2.eta_is_nom, Turb_LP2.eta_is_min, Turb_LP2.a, Turb_LP2.b, Turb_LP2.c, lumpedStraightPipe1.L, lumpedStraightPipe1.D, lumpedStraightPipe1.z1, lumpedStraightPipe1.z2, singularPressureLoss4.K, singularPressureLoss5.K, singularPressureLoss6.K, Turb_LP1_b.area_nz, Turb_LP1_b.eta_nz, Turb_LP1_b.Qmax, Turb_LP1_b.eta_is_nom, Turb_LP1_b.eta_is_min, Turb_LP1_b.a, Turb_LP1_b.b, Turb_LP1_b.c, Extraction_ReheatBP.alpha, Extraction_TurbBP1a_Outlet.alpha, Reheat_BP.lambdaE, Reheat_BP.KPurge, ReHeat_HP.lambdaE, ReHeat_HP.KPurge, singularPressureLoss1.K, pI5.Ti, rampe3.Starttime, rampe3.Duration, rampe3.Initialvalue, rampe3.Finalvalue, Extraction_TurbHP_Oulet.alpha, pI6.Ti, Vapor_MassFlowRamp_extracted4Cogeneration.Starttime, Vapor_MassFlowRamp_extracted4Cogeneration.Duration, Vapor_MassFlowRamp_extracted4Cogeneration.Initialvalue, Vapor_MassFlowRamp_extracted4Cogeneration.Finalvalue, rampe2.Starttime, rampe2.Duration, rampe2.Initialvalue, rampe2.Finalvalue, sinkP.P0, sinkP.T0, sinkP2.P0, sinkP2.T0, rampe4.Starttime, rampe4.Duration, rampe4.Initialvalue, rampe4.Finalvalue, Hin_BC, Qin_BC, sinkP_4FMU.h0, Pout_BC, Test_Timetable_Rotation_PumpHP.'Table[1,1]', Test_Timetable_Rotation_PumpHP.'Table[1,2]', Test_Timetable_Rotation_PumpHP.'Table[2,1]', Test_Timetable_Rotation_PumpHP.'Table[2,2]', Test_Timetable_Rotation_PumpHP.'Table[3,1]', Test_Timetable_Rotation_PumpHP.'Table[3,2]', Test_Timetable_Rotation_PumpHP.'Table[4,1]', Test_Timetable_Rotation_PumpHP.'Table[4,2]', Test_Timetable_Rotation_PumpHP.'Table[5,1]', Test_Timetable_Rotation_PumpHP.'Table[5,2]', Test_Timetable_Rotation_PumpHP.'Table[6,1]', Test_Timetable_Rotation_PumpHP.'Table[6,2]', Test_Timetable_Rotation_PumpHP.'Table[7,1]', Test_Timetable_Rotation_PumpHP.'Table[7,2]', Test_Timetable_Rotation_PumpHP.'Table[8,1]', Test_Timetable_Rotation_PumpHP.'Table[8,2]', Test_Timetable_Rotation_PumpHP.'Table[9,1]', Test_Timetable_Rotation_PumpHP.'Table[9,2]', Test_Timetable_Rotation_PumpLP.'Table[1,1]', Test_Timetable_Rotation_PumpLP.'Table[1,2]', Test_Timetable_Rotation_PumpLP.'Table[2,1]', Test_Timetable_Rotation_PumpLP.'Table[2,2]', Test_Timetable_Rotation_PumpLP.'Table[3,1]', Test_Timetable_Rotation_PumpLP.'Table[3,2]', Test_Timetable_Rotation_PumpLP.'Table[4,1]', Test_Timetable_Rotation_PumpLP.'Table[4,2]', Test_Timetable_Rotation_PumpLP.'Table[5,1]', Test_Timetable_Rotation_PumpLP.'Table[5,2]', Test_Timetable_Rotation_PumpLP.'Table[6,1]', Test_Timetable_Rotation_PumpLP.'Table[6,2]', Test_Timetable_Rotation_PumpLP.'Table[7,1]', Test_Timetable_Rotation_PumpLP.'Table[7,2]', Test_Timetable_Rotation_PumpLP.'Table[8,1]', Test_Timetable_Rotation_PumpLP.'Table[8,2]', Test_Timetable_Rotation_PumpLP.'Table[9,1]', Test_Timetable_Rotation_PumpLP.'Table[9,2]'}, fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, {BOP_HeatSink.gravity_pressure}, {Pump_HP.fluid, Pump_HP.mode, Turb_HP.fluid, Turb_HP.mode_e, Turb_HP.mode_s, Turb_HP.mode_ps, Turb_LP1_a.fluid, Turb_LP1_a.mode_e, Turb_LP1_a.mode_s, Turb_LP1_a.mode_ps, Pump_BP.fluid, Pump_BP.mode, singularPressureLoss3.fluid, singularPressureLoss3.mode, Extraction_ReheatHP.mode_e, lumpedStraightPipe.ntubes, lumpedStraightPipe.fluid, lumpedStraightPipe.mode, singularPressureLoss2.fluid, singularPressureLoss2.mode, Dryer.mode_e, Turb_LP2.fluid, Turb_LP2.mode_e, Turb_LP2.mode_s, Turb_LP2.mode_ps, lumpedStraightPipe1.ntubes, lumpedStraightPipe1.fluid, lumpedStraightPipe1.mode, singularPressureLoss4.fluid, singularPressureLoss4.mode, singularPressureLoss5.fluid, singularPressureLoss5.mode, singularPressureLoss6.fluid, singularPressureLoss6.mode, Turb_LP1_b.fluid, Turb_LP1_b.mode_e, Turb_LP1_b.mode_s, Turb_LP1_b.mode_ps, Extraction_ReheatBP.mode_e, Extraction_TurbBP1a_Outlet.mode_e, Reheat_BP.mode_eeF, Reheat_BP.mode_spC, Reheat_BP.mode_flash, ReHeat_HP.mode_eeF, ReHeat_HP.mode_spC, ReHeat_HP.mode_flash, singularPressureLoss1.fluid, singularPressureLoss1.mode, Tin_Turb_LP1a.mode, Extraction_TurbHP_Oulet.mode_e, Valve_superheat1.fluid, Valve_superheat1.mode, Valve_superheat2.fluid, Valve_superheat2.mode}, {16777217, 16777218, 16777219, 16777220, 16777221, 16777231, 16777235, 16777236, 16777237, 16777238, 16777239, 16777240, 16777250, 16777251, 16777252, 16777253, 16777273, 16777274, 16777275, 16777276, 16777277, 16777278, 16777279, 16777280, 16777287, 16777288, 16777289, 16777290, 16777291, 16777292, 16777293, 16777294, 16777304, 16777308, 16777309, 16777310, 16777311, 16777312, 16777313, 16777317, 16777322, 16777324, 16777325, 16777328, 16777329, 16777334, 16777339, 16777341, 16777345, 16777353, 16777354, 16777355, 16777356, 16777357, 16777358, 16777359, 16777360, 16777365, 16777366, 16777369, 16777370, 16777375, 16777385, 16777405, 16777412, 16777413, 16777414, 16777415, 16777416, 16777417, 16777418, 16777419, 16777424, 16777433, 16777435, 16777437, 16777450, 16777452, 16777477, 16777491, 16777493, 16777494, 16777495, 16777496, 16777512, 16777533, 16777535, 16777536, 16777537, 16777538, 16777551, 16777552, 16777553, 16777554, 16777555, 16777556, 16777565, 16777566, 16777569, 16777570, 16777571, 16777572, 352321536, 352321537, 16777577, 352321538, 16777592, 16777593, 16777594, 16777595, 16777596, 16777597, 16777598, 16777599, 16777600, 16777601, 16777602, 16777603, 16777604, 16777605, 16777606, 16777607, 16777608, 16777609, 16777610, 16777611, 16777612, 16777613, 16777614, 16777615, 16777616, 16777617, 16777618, 16777619, 16777620, 16777621, 16777622, 16777623, 16777624, 16777625, 16777626, 16777627}, fill(0,0), fill(0,0), {16777254}, {16777233, 16777234, 16777281, 16777282, 16777283, 16777284, 16777295, 16777296, 16777297, 16777298, 16777306, 16777307, 16777318, 16777319, 16777323, 16777326, 16777330, 16777331, 16777335, 16777336, 16777346, 16777361, 16777362, 16777363, 16777364, 16777367, 16777371, 16777372, 16777376, 16777377, 16777386, 16777387, 16777406, 16777407, 16777420, 16777421, 16777422, 16777423, 16777425, 16777434, 16777438, 16777443, 16777444, 16777453, 16777458, 16777459, 16777478, 16777479, 16777488, 16777513, 16777546, 16777547, 16777560, 16777561}, {335544321});
       else
         Qout_TSPro = internal.Qout_TSPro;
       end if;
       if initial() then
         Hout_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, {SpHP_Q, SpHP_H, SpBP_Q, SpBP_H, CvmaxSurch, Pump_HP.VRotn, Pump_HP.a1, Pump_HP.a2, Pump_HP.a3, Pump_HP.b1, Pump_HP.b2, Pump_HP.b3, BOP_HeatSink.V, BOP_HeatSink.A, BOP_HeatSink.Vf0, BOP_HeatSink.P0, Turb_HP.area_nz, Turb_HP.eta_nz, Turb_HP.Qmax, Turb_HP.eta_is_nom, Turb_HP.eta_is_min, Turb_HP.a, Turb_HP.b, Turb_HP.c, Turb_LP1_a.area_nz, Turb_LP1_a.eta_nz, Turb_LP1_a.Qmax, Turb_LP1_a.eta_is_nom, Turb_LP1_a.eta_is_min, Turb_LP1_a.a, Turb_LP1_a.b, Turb_LP1_a.c, Pump_BP.VRotn, Pump_BP.a1, Pump_BP.a2, Pump_BP.a3, Pump_BP.b1, Pump_BP.b2, Pump_BP.b3, singularPressureLoss3.K, Extraction_ReheatHP.alpha, lumpedStraightPipe.L, lumpedStraightPipe.D, lumpedStraightPipe.z1, lumpedStraightPipe.z2, singularPressureLoss2.K, PressureSet_TurbHP_In.k, pI1.Ti, Dryer.eta, Turb_LP2.area_nz, Turb_LP2.eta_nz, Turb_LP2.Qmax, Turb_LP2.eta_is_nom, Turb_LP2.eta_is_min, Turb_LP2.a, Turb_LP2.b, Turb_LP2.c, lumpedStraightPipe1.L, lumpedStraightPipe1.D, lumpedStraightPipe1.z1, lumpedStraightPipe1.z2, singularPressureLoss4.K, singularPressureLoss5.K, singularPressureLoss6.K, Turb_LP1_b.area_nz, Turb_LP1_b.eta_nz, Turb_LP1_b.Qmax, Turb_LP1_b.eta_is_nom, Turb_LP1_b.eta_is_min, Turb_LP1_b.a, Turb_LP1_b.b, Turb_LP1_b.c, Extraction_ReheatBP.alpha, Extraction_TurbBP1a_Outlet.alpha, Reheat_BP.lambdaE, Reheat_BP.KPurge, ReHeat_HP.lambdaE, ReHeat_HP.KPurge, singularPressureLoss1.K, pI5.Ti, rampe3.Starttime, rampe3.Duration, rampe3.Initialvalue, rampe3.Finalvalue, Extraction_TurbHP_Oulet.alpha, pI6.Ti, Vapor_MassFlowRamp_extracted4Cogeneration.Starttime, Vapor_MassFlowRamp_extracted4Cogeneration.Duration, Vapor_MassFlowRamp_extracted4Cogeneration.Initialvalue, Vapor_MassFlowRamp_extracted4Cogeneration.Finalvalue, rampe2.Starttime, rampe2.Duration, rampe2.Initialvalue, rampe2.Finalvalue, sinkP.P0, sinkP.T0, sinkP2.P0, sinkP2.T0, rampe4.Starttime, rampe4.Duration, rampe4.Initialvalue, rampe4.Finalvalue, Hin_BC, Qin_BC, sinkP_4FMU.h0, Pout_BC, Test_Timetable_Rotation_PumpHP.'Table[1,1]', Test_Timetable_Rotation_PumpHP.'Table[1,2]', Test_Timetable_Rotation_PumpHP.'Table[2,1]', Test_Timetable_Rotation_PumpHP.'Table[2,2]', Test_Timetable_Rotation_PumpHP.'Table[3,1]', Test_Timetable_Rotation_PumpHP.'Table[3,2]', Test_Timetable_Rotation_PumpHP.'Table[4,1]', Test_Timetable_Rotation_PumpHP.'Table[4,2]', Test_Timetable_Rotation_PumpHP.'Table[5,1]', Test_Timetable_Rotation_PumpHP.'Table[5,2]', Test_Timetable_Rotation_PumpHP.'Table[6,1]', Test_Timetable_Rotation_PumpHP.'Table[6,2]', Test_Timetable_Rotation_PumpHP.'Table[7,1]', Test_Timetable_Rotation_PumpHP.'Table[7,2]', Test_Timetable_Rotation_PumpHP.'Table[8,1]', Test_Timetable_Rotation_PumpHP.'Table[8,2]', Test_Timetable_Rotation_PumpHP.'Table[9,1]', Test_Timetable_Rotation_PumpHP.'Table[9,2]', Test_Timetable_Rotation_PumpLP.'Table[1,1]', Test_Timetable_Rotation_PumpLP.'Table[1,2]', Test_Timetable_Rotation_PumpLP.'Table[2,1]', Test_Timetable_Rotation_PumpLP.'Table[2,2]', Test_Timetable_Rotation_PumpLP.'Table[3,1]', Test_Timetable_Rotation_PumpLP.'Table[3,2]', Test_Timetable_Rotation_PumpLP.'Table[4,1]', Test_Timetable_Rotation_PumpLP.'Table[4,2]', Test_Timetable_Rotation_PumpLP.'Table[5,1]', Test_Timetable_Rotation_PumpLP.'Table[5,2]', Test_Timetable_Rotation_PumpLP.'Table[6,1]', Test_Timetable_Rotation_PumpLP.'Table[6,2]', Test_Timetable_Rotation_PumpLP.'Table[7,1]', Test_Timetable_Rotation_PumpLP.'Table[7,2]', Test_Timetable_Rotation_PumpLP.'Table[8,1]', Test_Timetable_Rotation_PumpLP.'Table[8,2]', Test_Timetable_Rotation_PumpLP.'Table[9,1]', Test_Timetable_Rotation_PumpLP.'Table[9,2]'}, fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, {BOP_HeatSink.gravity_pressure}, {Pump_HP.fluid, Pump_HP.mode, Turb_HP.fluid, Turb_HP.mode_e, Turb_HP.mode_s, Turb_HP.mode_ps, Turb_LP1_a.fluid, Turb_LP1_a.mode_e, Turb_LP1_a.mode_s, Turb_LP1_a.mode_ps, Pump_BP.fluid, Pump_BP.mode, singularPressureLoss3.fluid, singularPressureLoss3.mode, Extraction_ReheatHP.mode_e, lumpedStraightPipe.ntubes, lumpedStraightPipe.fluid, lumpedStraightPipe.mode, singularPressureLoss2.fluid, singularPressureLoss2.mode, Dryer.mode_e, Turb_LP2.fluid, Turb_LP2.mode_e, Turb_LP2.mode_s, Turb_LP2.mode_ps, lumpedStraightPipe1.ntubes, lumpedStraightPipe1.fluid, lumpedStraightPipe1.mode, singularPressureLoss4.fluid, singularPressureLoss4.mode, singularPressureLoss5.fluid, singularPressureLoss5.mode, singularPressureLoss6.fluid, singularPressureLoss6.mode, Turb_LP1_b.fluid, Turb_LP1_b.mode_e, Turb_LP1_b.mode_s, Turb_LP1_b.mode_ps, Extraction_ReheatBP.mode_e, Extraction_TurbBP1a_Outlet.mode_e, Reheat_BP.mode_eeF, Reheat_BP.mode_spC, Reheat_BP.mode_flash, ReHeat_HP.mode_eeF, ReHeat_HP.mode_spC, ReHeat_HP.mode_flash, singularPressureLoss1.fluid, singularPressureLoss1.mode, Tin_Turb_LP1a.mode, Extraction_TurbHP_Oulet.mode_e, Valve_superheat1.fluid, Valve_superheat1.mode, Valve_superheat2.fluid, Valve_superheat2.mode}, {16777217, 16777218, 16777219, 16777220, 16777221, 16777231, 16777235, 16777236, 16777237, 16777238, 16777239, 16777240, 16777250, 16777251, 16777252, 16777253, 16777273, 16777274, 16777275, 16777276, 16777277, 16777278, 16777279, 16777280, 16777287, 16777288, 16777289, 16777290, 16777291, 16777292, 16777293, 16777294, 16777304, 16777308, 16777309, 16777310, 16777311, 16777312, 16777313, 16777317, 16777322, 16777324, 16777325, 16777328, 16777329, 16777334, 16777339, 16777341, 16777345, 16777353, 16777354, 16777355, 16777356, 16777357, 16777358, 16777359, 16777360, 16777365, 16777366, 16777369, 16777370, 16777375, 16777385, 16777405, 16777412, 16777413, 16777414, 16777415, 16777416, 16777417, 16777418, 16777419, 16777424, 16777433, 16777435, 16777437, 16777450, 16777452, 16777477, 16777491, 16777493, 16777494, 16777495, 16777496, 16777512, 16777533, 16777535, 16777536, 16777537, 16777538, 16777551, 16777552, 16777553, 16777554, 16777555, 16777556, 16777565, 16777566, 16777569, 16777570, 16777571, 16777572, 352321536, 352321537, 16777577, 352321538, 16777592, 16777593, 16777594, 16777595, 16777596, 16777597, 16777598, 16777599, 16777600, 16777601, 16777602, 16777603, 16777604, 16777605, 16777606, 16777607, 16777608, 16777609, 16777610, 16777611, 16777612, 16777613, 16777614, 16777615, 16777616, 16777617, 16777618, 16777619, 16777620, 16777621, 16777622, 16777623, 16777624, 16777625, 16777626, 16777627}, fill(0,0), fill(0,0), {16777254}, {16777233, 16777234, 16777281, 16777282, 16777283, 16777284, 16777295, 16777296, 16777297, 16777298, 16777306, 16777307, 16777318, 16777319, 16777323, 16777326, 16777330, 16777331, 16777335, 16777336, 16777346, 16777361, 16777362, 16777363, 16777364, 16777367, 16777371, 16777372, 16777376, 16777377, 16777386, 16777387, 16777406, 16777407, 16777420, 16777421, 16777422, 16777423, 16777425, 16777434, 16777438, 16777443, 16777444, 16777453, 16777458, 16777459, 16777478, 16777479, 16777488, 16777513, 16777546, 16777547, 16777560, 16777561}, {335544322});
       else
         Hout_TSPro = internal.Hout_TSPro;
       end if;
     initial equation
     RealDependentParameters = fmi_Functions.fmiGetRealwf(fmi, {100663313, 100663314},fmi_rdum);
     equation
       Eff_Cycle_net = RealVariables1[1];
       Eff_Cycle_brut = RealVariables1[2];
       Pw_eGrid = RealVariables1[3];
       Pwth_COG = RealVariables1[4];
       T_COG = RealVariables1[5];
       Heat_Power_ratio = RealVariables1[6];
       Tin_SG = RealVariables1[7];
       Nuclear_Pw_load = RealVariables1[8];
       Pwth_SG = RealVariables1[9];
       Valve_HP.Cv = RealVariables1[10];
       Valve_HP.Q = RealVariables1[11];
       Valve_HP.deltaP = RealVariables1[12];
       Valve_HP.rho = RealVariables1[13];
       Valve_HP.T = RealVariables1[14];
       Valve_HP.Pm = RealVariables1[15];
       Valve_HP.h = RealVariables1[16];
       Valve_HP.pro.d = RealVariables1[17];
       Valve_HP.pro.u = RealVariables1[18];
       Valve_HP.pro.s = RealVariables1[19];
       Valve_HP.pro.cp = RealVariables1[20];
       Valve_HP.pro.ddhp = RealVariables1[21];
       Valve_HP.pro.ddph = RealVariables1[22];
       Valve_HP.pro.duph = RealVariables1[23];
       Valve_HP.pro.duhp = RealVariables1[24];
       Valve_HP.pro.x = RealVariables1[25];
       Valve_HP.Ouv.signal = RealVariables1[26];
       Valve_HP.C1.P = RealVariables1[27];
       Valve_HP.C1.h_vol = RealVariables1[28];
       Pump_HP.rh = RealVariables1[29];
       Pump_HP.hn = RealVariables1[30];
       Pump_HP.R = RealVariables1[31];
       Pump_HP.Q = RealVariables1[32];
       Pump_HP.Qv = RealVariables1[33];
       Pump_HP.Wh = RealVariables1[34];
       Pump_HP.Wm = RealVariables1[35];
       Pump_HP.Vr = RealVariables1[36];
       Pump_HP.rho = RealVariables1[37];
       Pump_HP.deltaP = RealVariables1[38];
       Pump_HP.deltaH = RealVariables1[39];
       Pump_HP.Pm = RealVariables1[40];
       Pump_HP.h = RealVariables1[41];
       Pump_HP.C1.P = RealVariables1[42];
       Pump_HP.C1.h = RealVariables1[43];
       Pump_HP.C2.P = RealVariables1[44];
       Pump_HP.C2.h_vol = RealVariables1[45];
       Pump_HP.C2.h = RealVariables1[46];
       Pump_HP.pro.T = RealVariables1[47];
       Pump_HP.pro.u = RealVariables1[48];
       Pump_HP.pro.s = RealVariables1[49];
       Pump_HP.pro.cp = RealVariables1[50];
       Pump_HP.pro.ddhp = RealVariables1[51];
       Pump_HP.pro.ddph = RealVariables1[52];
       Pump_HP.pro.duph = RealVariables1[53];
       Pump_HP.pro.duhp = RealVariables1[54];
       Pump_HP.pro.x = RealVariables1[55];
       Pump_HP.rpm_or_mpower.signal = RealVariables1[56];
       sourceQ1.P = RealVariables1[57];
       sourceQ1.IMassFlow.signal = RealVariables1[58];
       sourceQ1.ISpecificEnthalpy.signal = RealVariables1[59];
       sourceQ1.C.h = RealVariables1[60];
       sinkP1.IPressure.signal = RealVariables1[61];
       sinkP1.ISpecificEnthalpy.signal = RealVariables1[62];
       sinkP1.C.h = RealVariables1[63];
       sinkP1.ITemperature.signal = RealVariables1[64];
       BOP_HeatSink.dpf = RealVariables1[65];
       BOP_HeatSink.dpg = RealVariables1[66];
       BOP_HeatSink.P = RealVariables1[67];
       BOP_HeatSink.Pfond = RealVariables1[68];
       BOP_HeatSink.hl = RealVariables1[69];
       BOP_HeatSink.hv = RealVariables1[70];
       BOP_HeatSink.Tl = RealVariables1[71];
       BOP_HeatSink.Tv = RealVariables1[72];
       BOP_HeatSink.Vl = RealVariables1[73];
       BOP_HeatSink._Vl_der = RealVariables1[74];
       BOP_HeatSink.Vv = RealVariables1[75];
       BOP_HeatSink.xl = RealVariables1[76];
       BOP_HeatSink.xv = RealVariables1[77];
       BOP_HeatSink.rhol = RealVariables1[78];
       BOP_HeatSink.rhov = RealVariables1[79];
       BOP_HeatSink.BQl = RealVariables1[80];
       BOP_HeatSink.BQv = RealVariables1[81];
       BOP_HeatSink.BHl = RealVariables1[82];
       BOP_HeatSink.BHv = RealVariables1[83];
       BOP_HeatSink.Qcond = RealVariables1[84];
       BOP_HeatSink.Qevap = RealVariables1[85];
       BOP_HeatSink.Wvl = RealVariables1[86];
       BOP_HeatSink.Wout = RealVariables1[87];
       BOP_HeatSink.prol.u = RealVariables1[88];
       BOP_HeatSink.prol.s = RealVariables1[89];
       BOP_HeatSink.prol.cp = RealVariables1[90];
       BOP_HeatSink.prol.ddhp = RealVariables1[91];
       BOP_HeatSink.prol.ddph = RealVariables1[92];
       BOP_HeatSink.prol.duph = RealVariables1[93];
       BOP_HeatSink.prol.duhp = RealVariables1[94];
       BOP_HeatSink.prov.u = RealVariables1[95];
       BOP_HeatSink.prov.s = RealVariables1[96];
       BOP_HeatSink.prov.cp = RealVariables1[97];
       BOP_HeatSink.prov.ddhp = RealVariables1[98];
       BOP_HeatSink.prov.ddph = RealVariables1[99];
       BOP_HeatSink.prov.duph = RealVariables1[100];
       BOP_HeatSink.prov.duhp = RealVariables2[1];
       BOP_HeatSink.lsat.P = RealVariables2[2];
       BOP_HeatSink.lsat.T = RealVariables2[3];
       BOP_HeatSink.lsat.rho = RealVariables2[4];
       BOP_HeatSink.lsat.h = RealVariables2[5];
       BOP_HeatSink.lsat.cp = RealVariables2[6];
       BOP_HeatSink.lsat.pt = RealVariables2[7];
       BOP_HeatSink.lsat.cv = RealVariables2[8];
       BOP_HeatSink.vsat.P = RealVariables2[9];
       BOP_HeatSink.vsat.T = RealVariables2[10];
       BOP_HeatSink.vsat.rho = RealVariables2[11];
       BOP_HeatSink.vsat.h = RealVariables2[12];
       BOP_HeatSink.vsat.cp = RealVariables2[13];
       BOP_HeatSink.vsat.pt = RealVariables2[14];
       BOP_HeatSink.vsat.cv = RealVariables2[15];
       BOP_HeatSink.Cv.Q = RealVariables2[16];
       BOP_HeatSink.Cv.h = RealVariables2[17];
       BOP_HeatSink.Cl.Q = RealVariables2[18];
       BOP_HeatSink.Cl.h = RealVariables2[19];
       BOP_HeatSink.yNiveau.signal = RealVariables2[20];
       BOP_HeatSink.prod.T = RealVariables2[21];
       BOP_HeatSink.prod.d = RealVariables2[22];
       BOP_HeatSink.prod.u = RealVariables2[23];
       BOP_HeatSink.prod.s = RealVariables2[24];
       BOP_HeatSink.prod.cp = RealVariables2[25];
       BOP_HeatSink.prod.ddhp = RealVariables2[26];
       BOP_HeatSink.prod.ddph = RealVariables2[27];
       BOP_HeatSink.prod.duph = RealVariables2[28];
       BOP_HeatSink.prod.duhp = RealVariables2[29];
       BOP_HeatSink.prod.x = RealVariables2[30];
       BOP_HeatSink.proe.T = RealVariables2[31];
       BOP_HeatSink.proe.d = RealVariables2[32];
       BOP_HeatSink.proe.u = RealVariables2[33];
       BOP_HeatSink.proe.s = RealVariables2[34];
       BOP_HeatSink.proe.cp = RealVariables2[35];
       BOP_HeatSink.proe.ddhp = RealVariables2[36];
       BOP_HeatSink.proe.ddph = RealVariables2[37];
       BOP_HeatSink.proe.duph = RealVariables2[38];
       BOP_HeatSink.proe.duhp = RealVariables2[39];
       BOP_HeatSink.proe.x = RealVariables2[40];
       Turb_HP.eta_is = RealVariables2[41];
       Turb_HP.eta_is_wet = RealVariables2[42];
       Turb_HP.W = RealVariables2[43];
       Turb_HP.His = RealVariables2[44];
       Turb_HP.Hrs = RealVariables2[45];
       Turb_HP.Pe = RealVariables2[46];
       Turb_HP.Ps = RealVariables2[47];
       Turb_HP.Te = RealVariables2[48];
       Turb_HP.Ts = RealVariables2[49];
       Turb_HP.Vs = RealVariables2[50];
       Turb_HP.rhos = RealVariables2[51];
       Turb_HP.xm = RealVariables2[52];
       Turb_HP.proe.d = RealVariables2[53];
       Turb_HP.proe.u = RealVariables2[54];
       Turb_HP.proe.s = RealVariables2[55];
       Turb_HP.proe.cp = RealVariables2[56];
       Turb_HP.proe.ddhp = RealVariables2[57];
       Turb_HP.proe.ddph = RealVariables2[58];
       Turb_HP.proe.duph = RealVariables2[59];
       Turb_HP.proe.duhp = RealVariables2[60];
       Turb_HP.proe.x = RealVariables2[61];
       Turb_HP.pros.d = RealVariables2[62];
       Turb_HP.pros.u = RealVariables2[63];
       Turb_HP.pros.s = RealVariables2[64];
       Turb_HP.pros.cp = RealVariables2[65];
       Turb_HP.pros.ddhp = RealVariables2[66];
       Turb_HP.pros.ddph = RealVariables2[67];
       Turb_HP.pros.duph = RealVariables2[68];
       Turb_HP.pros.duhp = RealVariables2[69];
       Turb_HP.pros.x = RealVariables2[70];
       Turb_HP.Cs.h_vol = RealVariables2[71];
       Turb_HP.Cs.h = RealVariables2[72];
       Turb_HP.props.T = RealVariables2[73];
       Turb_HP.props.d = RealVariables2[74];
       Turb_HP.props.u = RealVariables2[75];
       Turb_HP.props.cp = RealVariables2[76];
       Turb_HP.props.ddsp = RealVariables2[77];
       Turb_HP.props.ddps = RealVariables2[78];
       Turb_HP.props.x = RealVariables2[79];
       Turb_HP.MechPower.signal = RealVariables2[80];
       Turb_HP.pros1.T = RealVariables2[81];
       Turb_HP.pros1.d = RealVariables2[82];
       Turb_HP.pros1.u = RealVariables2[83];
       Turb_HP.pros1.s = RealVariables2[84];
       Turb_HP.pros1.cp = RealVariables2[85];
       Turb_HP.pros1.ddhp = RealVariables2[86];
       Turb_HP.pros1.ddph = RealVariables2[87];
       Turb_HP.pros1.duph = RealVariables2[88];
       Turb_HP.pros1.duhp = RealVariables2[89];
       Turb_HP.pros1.x = RealVariables2[90];
       Turb_LP1_a.eta_is = RealVariables2[91];
       Turb_LP1_a.eta_is_wet = RealVariables2[92];
       Turb_LP1_a.W = RealVariables2[93];
       Turb_LP1_a.Q = RealVariables2[94];
       Turb_LP1_a.His = RealVariables2[95];
       Turb_LP1_a.Hrs = RealVariables2[96];
       Turb_LP1_a.Pe = RealVariables2[97];
       Turb_LP1_a.Te = RealVariables2[98];
       Turb_LP1_a.Ts = RealVariables2[99];
       Turb_LP1_a.Vs = RealVariables2[100];
       Turb_LP1_a.rhos = RealVariables3[1];
       Turb_LP1_a.xm = RealVariables3[2];
       Turb_LP1_a.proe.d = RealVariables3[3];
       Turb_LP1_a.proe.u = RealVariables3[4];
       Turb_LP1_a.proe.s = RealVariables3[5];
       Turb_LP1_a.proe.cp = RealVariables3[6];
       Turb_LP1_a.proe.ddhp = RealVariables3[7];
       Turb_LP1_a.proe.ddph = RealVariables3[8];
       Turb_LP1_a.proe.duph = RealVariables3[9];
       Turb_LP1_a.proe.duhp = RealVariables3[10];
       Turb_LP1_a.proe.x = RealVariables3[11];
       Turb_LP1_a.pros.d = RealVariables3[12];
       Turb_LP1_a.pros.u = RealVariables3[13];
       Turb_LP1_a.pros.s = RealVariables3[14];
       Turb_LP1_a.pros.cp = RealVariables3[15];
       Turb_LP1_a.pros.ddhp = RealVariables3[16];
       Turb_LP1_a.pros.ddph = RealVariables3[17];
       Turb_LP1_a.pros.duph = RealVariables3[18];
       Turb_LP1_a.pros.duhp = RealVariables3[19];
       Turb_LP1_a.pros.x = RealVariables3[20];
       Turb_LP1_a.Ce.h_vol = RealVariables3[21];
       Turb_LP1_a.Cs.h_vol = RealVariables3[22];
       Turb_LP1_a.Cs.h = RealVariables3[23];
       Turb_LP1_a.props.T = RealVariables3[24];
       Turb_LP1_a.props.d = RealVariables3[25];
       Turb_LP1_a.props.u = RealVariables3[26];
       Turb_LP1_a.props.cp = RealVariables3[27];
       Turb_LP1_a.props.ddsp = RealVariables3[28];
       Turb_LP1_a.props.ddps = RealVariables3[29];
       Turb_LP1_a.props.x = RealVariables3[30];
       Turb_LP1_a.MechPower.signal = RealVariables3[31];
       Turb_LP1_a.pros1.T = RealVariables3[32];
       Turb_LP1_a.pros1.d = RealVariables3[33];
       Turb_LP1_a.pros1.u = RealVariables3[34];
       Turb_LP1_a.pros1.s = RealVariables3[35];
       Turb_LP1_a.pros1.cp = RealVariables3[36];
       Turb_LP1_a.pros1.ddhp = RealVariables3[37];
       Turb_LP1_a.pros1.ddph = RealVariables3[38];
       Turb_LP1_a.pros1.duph = RealVariables3[39];
       Turb_LP1_a.pros1.duhp = RealVariables3[40];
       Turb_LP1_a.pros1.x = RealVariables3[41];
       Tout_SG.Q = RealVariables3[42];
       Tout_SG.T = RealVariables3[43];
       Tout_SG.h = RealVariables3[44];
       Tout_SG.pro.d = RealVariables3[45];
       Tout_SG.pro.u = RealVariables3[46];
       Tout_SG.pro.s = RealVariables3[47];
       Tout_SG.pro.cp = RealVariables3[48];
       Tout_SG.pro.ddhp = RealVariables3[49];
       Tout_SG.pro.ddph = RealVariables3[50];
       Tout_SG.pro.duph = RealVariables3[51];
       Tout_SG.pro.duhp = RealVariables3[52];
       Tout_SG.pro.x = RealVariables3[53];
       Tout_SG.Measure.signal = RealVariables3[54];
       Tout_SG.C1.h_vol = RealVariables3[55];
       Tout_SG.C2.h_vol = RealVariables3[56];
       Pe_ValveHP.Measure.signal = RealVariables3[57];
       Pe_ValveHP.C1.h_vol = RealVariables3[58];
       Pump_BP.rh = RealVariables3[59];
       Pump_BP.hn = RealVariables3[60];
       Pump_BP.R = RealVariables3[61];
       Pump_BP.Qv = RealVariables3[62];
       Pump_BP.Wh = RealVariables3[63];
       Pump_BP.Wm = RealVariables3[64];
       Pump_BP.Vr = RealVariables3[65];
       Pump_BP.rho = RealVariables3[66];
       Pump_BP.deltaP = RealVariables3[67];
       Pump_BP.deltaH = RealVariables3[68];
       Pump_BP.Pm = RealVariables3[69];
       Pump_BP.h = RealVariables3[70];
       Pump_BP.C1.P = RealVariables3[71];
       Pump_BP.C1.h = RealVariables3[72];
       Pump_BP.C2.P = RealVariables3[73];
       Pump_BP.C2.h_vol = RealVariables3[74];
       Pump_BP.C2.h = RealVariables3[75];
       Pump_BP.pro.T = RealVariables3[76];
       Pump_BP.pro.u = RealVariables3[77];
       Pump_BP.pro.s = RealVariables3[78];
       Pump_BP.pro.cp = RealVariables3[79];
       Pump_BP.pro.ddhp = RealVariables3[80];
       Pump_BP.pro.ddph = RealVariables3[81];
       Pump_BP.pro.duph = RealVariables3[82];
       Pump_BP.pro.duhp = RealVariables3[83];
       Pump_BP.pro.x = RealVariables3[84];
       Pump_BP.rpm_or_mpower.signal = RealVariables3[85];
       singularPressureLoss3.deltaP = RealVariables3[86];
       singularPressureLoss3.rho = RealVariables3[87];
       singularPressureLoss3.T = RealVariables3[88];
       singularPressureLoss3.Pm = RealVariables3[89];
       singularPressureLoss3.C2.P = RealVariables3[90];
       singularPressureLoss3.C2.h_vol = RealVariables3[91];
       singularPressureLoss3.pro.u = RealVariables3[92];
       singularPressureLoss3.pro.s = RealVariables3[93];
       singularPressureLoss3.pro.cp = RealVariables3[94];
       singularPressureLoss3.pro.ddhp = RealVariables3[95];
       singularPressureLoss3.pro.ddph = RealVariables3[96];
       singularPressureLoss3.pro.duph = RealVariables3[97];
       singularPressureLoss3.pro.duhp = RealVariables3[98];
       singularPressureLoss3.pro.x = RealVariables3[99];
       Extraction_ReheatHP.x_ex = RealVariables3[100];
       Extraction_ReheatHP.proe.T = RealVariables4[1];
       Extraction_ReheatHP.proe.d = RealVariables4[2];
       Extraction_ReheatHP.proe.u = RealVariables4[3];
       Extraction_ReheatHP.proe.s = RealVariables4[4];
       Extraction_ReheatHP.proe.cp = RealVariables4[5];
       Extraction_ReheatHP.proe.ddhp = RealVariables4[6];
       Extraction_ReheatHP.proe.ddph = RealVariables4[7];
       Extraction_ReheatHP.proe.duph = RealVariables4[8];
       Extraction_ReheatHP.proe.duhp = RealVariables4[9];
       Extraction_ReheatHP.proe.x = RealVariables4[10];
       Extraction_ReheatHP.Cs.Q = RealVariables4[11];
       Extraction_ReheatHP.Cs.h = RealVariables4[12];
       Extraction_ReheatHP.lsat.P = RealVariables4[13];
       Extraction_ReheatHP.lsat.T = RealVariables4[14];
       Extraction_ReheatHP.lsat.rho = RealVariables4[15];
       Extraction_ReheatHP.lsat.h = RealVariables4[16];
       Extraction_ReheatHP.lsat.cp = RealVariables4[17];
       Extraction_ReheatHP.lsat.pt = RealVariables4[18];
       Extraction_ReheatHP.lsat.cv = RealVariables4[19];
       Extraction_ReheatHP.vsat.P = RealVariables4[20];
       Extraction_ReheatHP.vsat.T = RealVariables4[21];
       Extraction_ReheatHP.vsat.rho = RealVariables4[22];
       Extraction_ReheatHP.vsat.h = RealVariables4[23];
       Extraction_ReheatHP.vsat.cp = RealVariables4[24];
       Extraction_ReheatHP.vsat.pt = RealVariables4[25];
       Extraction_ReheatHP.vsat.cv = RealVariables4[26];
       Extraction_ReheatHP.Cex.h_vol = RealVariables4[27];
       Extraction_ReheatHP.Cex.Q = RealVariables4[28];
       Extraction_ReheatHP.Cex.h = RealVariables4[29];
       lumpedStraightPipe.deltaPf = RealVariables4[30];
       lumpedStraightPipe.deltaP = RealVariables4[31];
       lumpedStraightPipe.Re = RealVariables4[32];
       lumpedStraightPipe.rho = RealVariables4[33];
       lumpedStraightPipe.mu = RealVariables4[34];
       lumpedStraightPipe.T = RealVariables4[35];
       lumpedStraightPipe.Pm = RealVariables4[36];
       lumpedStraightPipe.C2.P = RealVariables4[37];
       lumpedStraightPipe.C2.h_vol = RealVariables4[38];
       lumpedStraightPipe.pro.u = RealVariables4[39];
       lumpedStraightPipe.pro.s = RealVariables4[40];
       lumpedStraightPipe.pro.cp = RealVariables4[41];
       lumpedStraightPipe.pro.ddhp = RealVariables4[42];
       lumpedStraightPipe.pro.ddph = RealVariables4[43];
       lumpedStraightPipe.pro.duph = RealVariables4[44];
       lumpedStraightPipe.pro.duhp = RealVariables4[45];
       lumpedStraightPipe.pro.x = RealVariables4[46];
       singularPressureLoss2.deltaP = RealVariables4[47];
       singularPressureLoss2.rho = RealVariables4[48];
       singularPressureLoss2.T = RealVariables4[49];
       singularPressureLoss2.Pm = RealVariables4[50];
       singularPressureLoss2.C2.P = RealVariables4[51];
       singularPressureLoss2.C2.h_vol = RealVariables4[52];
       singularPressureLoss2.pro.u = RealVariables4[53];
       singularPressureLoss2.pro.s = RealVariables4[54];
       singularPressureLoss2.pro.cp = RealVariables4[55];
       singularPressureLoss2.pro.ddhp = RealVariables4[56];
       singularPressureLoss2.pro.ddph = RealVariables4[57];
       singularPressureLoss2.pro.duph = RealVariables4[58];
       singularPressureLoss2.pro.duhp = RealVariables4[59];
       singularPressureLoss2.pro.x = RealVariables4[60];
       feedback1.y.signal = RealVariables4[61];
       pI1.x = RealVariables4[62];
       Generator.Welec = RealVariables4[63];
       Generator.Wmec4.signal = RealVariables4[64];
       Generator.Wmec5.signal = RealVariables4[65];
       sensorP1.Measure.signal = RealVariables4[66];
       sensorP1.C1.P = RealVariables4[67];
       sensorP1.C1.h_vol = RealVariables4[68];
       sensorP1.C1.h = RealVariables4[69];
       Dryer.h = RealVariables4[70];
       Dryer.xe = RealVariables4[71];
       Dryer.proe.T = RealVariables4[72];
       Dryer.proe.d = RealVariables4[73];
       Dryer.proe.u = RealVariables4[74];
       Dryer.proe.s = RealVariables4[75];
       Dryer.proe.cp = RealVariables4[76];
       Dryer.proe.ddhp = RealVariables4[77];
       Dryer.proe.ddph = RealVariables4[78];
       Dryer.proe.duph = RealVariables4[79];
       Dryer.proe.duhp = RealVariables4[80];
       Dryer.Cev.Q = RealVariables4[81];
       Dryer.Cev.h = RealVariables4[82];
       Dryer.Csv.h = RealVariables4[83];
       Dryer.lsat1.P = RealVariables4[84];
       Dryer.lsat1.T = RealVariables4[85];
       Dryer.lsat1.rho = RealVariables4[86];
       Dryer.lsat1.h = RealVariables4[87];
       Dryer.lsat1.cp = RealVariables4[88];
       Dryer.lsat1.pt = RealVariables4[89];
       Dryer.lsat1.cv = RealVariables4[90];
       Dryer.vsat1.P = RealVariables4[91];
       Dryer.vsat1.T = RealVariables4[92];
       Dryer.vsat1.rho = RealVariables4[93];
       Dryer.vsat1.h = RealVariables4[94];
       Dryer.vsat1.cp = RealVariables4[95];
       Dryer.vsat1.pt = RealVariables4[96];
       Dryer.vsat1.cv = RealVariables4[97];
       Dryer.Csl.h_vol = RealVariables4[98];
       Dryer.Csl.Q = RealVariables4[99];
       Dryer.Csl.h = RealVariables4[100];
       invSingularPressureLoss.K = RealVariables5[1];
       invSingularPressureLoss.deltaP = RealVariables5[2];
       invSingularPressureLoss.rho = RealVariables5[3];
       invSingularPressureLoss.T = RealVariables5[4];
       invSingularPressureLoss.Pm = RealVariables5[5];
       invSingularPressureLoss.pro.u = RealVariables5[6];
       invSingularPressureLoss.pro.s = RealVariables5[7];
       invSingularPressureLoss.pro.cp = RealVariables5[8];
       invSingularPressureLoss.pro.ddhp = RealVariables5[9];
       invSingularPressureLoss.pro.ddph = RealVariables5[10];
       invSingularPressureLoss.pro.duph = RealVariables5[11];
       invSingularPressureLoss.pro.duhp = RealVariables5[12];
       invSingularPressureLoss.pro.x = RealVariables5[13];
       Turb_LP2.eta_is = RealVariables5[14];
       Turb_LP2.eta_is_wet = RealVariables5[15];
       Turb_LP2.W = RealVariables5[16];
       Turb_LP2.Q = RealVariables5[17];
       Turb_LP2.His = RealVariables5[18];
       Turb_LP2.Hrs = RealVariables5[19];
       Turb_LP2.Pe = RealVariables5[20];
       Turb_LP2.Ps = RealVariables5[21];
       Turb_LP2.Te = RealVariables5[22];
       Turb_LP2.Ts = RealVariables5[23];
       Turb_LP2.Vs = RealVariables5[24];
       Turb_LP2.rhos = RealVariables5[25];
       Turb_LP2.xm = RealVariables5[26];
       Turb_LP2.proe.d = RealVariables5[27];
       Turb_LP2.proe.u = RealVariables5[28];
       Turb_LP2.proe.s = RealVariables5[29];
       Turb_LP2.proe.cp = RealVariables5[30];
       Turb_LP2.proe.ddhp = RealVariables5[31];
       Turb_LP2.proe.ddph = RealVariables5[32];
       Turb_LP2.proe.duph = RealVariables5[33];
       Turb_LP2.proe.duhp = RealVariables5[34];
       Turb_LP2.proe.x = RealVariables5[35];
       Turb_LP2.pros.d = RealVariables5[36];
       Turb_LP2.pros.u = RealVariables5[37];
       Turb_LP2.pros.s = RealVariables5[38];
       Turb_LP2.pros.cp = RealVariables5[39];
       Turb_LP2.pros.ddhp = RealVariables5[40];
       Turb_LP2.pros.ddph = RealVariables5[41];
       Turb_LP2.pros.duph = RealVariables5[42];
       Turb_LP2.pros.duhp = RealVariables5[43];
       Turb_LP2.pros.x = RealVariables5[44];
       Turb_LP2.Ce.h_vol = RealVariables5[45];
       Turb_LP2.Cs.h = RealVariables5[46];
       Turb_LP2.props.T = RealVariables5[47];
       Turb_LP2.props.d = RealVariables5[48];
       Turb_LP2.props.u = RealVariables5[49];
       Turb_LP2.props.cp = RealVariables5[50];
       Turb_LP2.props.ddsp = RealVariables5[51];
       Turb_LP2.props.ddps = RealVariables5[52];
       Turb_LP2.props.x = RealVariables5[53];
       Turb_LP2.pros1.T = RealVariables5[54];
       Turb_LP2.pros1.d = RealVariables5[55];
       Turb_LP2.pros1.u = RealVariables5[56];
       Turb_LP2.pros1.s = RealVariables5[57];
       Turb_LP2.pros1.cp = RealVariables5[58];
       Turb_LP2.pros1.ddhp = RealVariables5[59];
       Turb_LP2.pros1.ddph = RealVariables5[60];
       Turb_LP2.pros1.duph = RealVariables5[61];
       Turb_LP2.pros1.duhp = RealVariables5[62];
       Turb_LP2.pros1.x = RealVariables5[63];
       lumpedStraightPipe1.deltaPf = RealVariables5[64];
       lumpedStraightPipe1.deltaP = RealVariables5[65];
       lumpedStraightPipe1.Q = RealVariables5[66];
       lumpedStraightPipe1.Re = RealVariables5[67];
       lumpedStraightPipe1.rho = RealVariables5[68];
       lumpedStraightPipe1.mu = RealVariables5[69];
       lumpedStraightPipe1.T = RealVariables5[70];
       lumpedStraightPipe1.Pm = RealVariables5[71];
       lumpedStraightPipe1.h = RealVariables5[72];
       lumpedStraightPipe1.C1.h_vol = RealVariables5[73];
       lumpedStraightPipe1.C2.P = RealVariables5[74];
       lumpedStraightPipe1.C2.h_vol = RealVariables5[75];
       lumpedStraightPipe1.pro.u = RealVariables5[76];
       lumpedStraightPipe1.pro.s = RealVariables5[77];
       lumpedStraightPipe1.pro.cp = RealVariables5[78];
       lumpedStraightPipe1.pro.ddhp = RealVariables5[79];
       lumpedStraightPipe1.pro.ddph = RealVariables5[80];
       lumpedStraightPipe1.pro.duph = RealVariables5[81];
       lumpedStraightPipe1.pro.duhp = RealVariables5[82];
       lumpedStraightPipe1.pro.x = RealVariables5[83];
       singularPressureLoss4.deltaP = RealVariables5[84];
       singularPressureLoss4.rho = RealVariables5[85];
       singularPressureLoss4.T = RealVariables5[86];
       singularPressureLoss4.Pm = RealVariables5[87];
       singularPressureLoss4.C2.P = RealVariables5[88];
       singularPressureLoss4.C2.h_vol = RealVariables5[89];
       singularPressureLoss4.pro.u = RealVariables5[90];
       singularPressureLoss4.pro.s = RealVariables5[91];
       singularPressureLoss4.pro.cp = RealVariables5[92];
       singularPressureLoss4.pro.ddhp = RealVariables5[93];
       singularPressureLoss4.pro.ddph = RealVariables5[94];
       singularPressureLoss4.pro.duph = RealVariables5[95];
       singularPressureLoss4.pro.duhp = RealVariables5[96];
       singularPressureLoss4.pro.x = RealVariables5[97];
       volumeC1.T = RealVariables5[98];
       volumeC1.h = RealVariables5[99];
       volumeC1.rho = RealVariables5[100];
       volumeC1.BH = RealVariables6[1];
       volumeC1.pro.u = RealVariables6[2];
       volumeC1.pro.s = RealVariables6[3];
       volumeC1.pro.cp = RealVariables6[4];
       volumeC1.pro.ddhp = RealVariables6[5];
       volumeC1.pro.ddph = RealVariables6[6];
       volumeC1.pro.duph = RealVariables6[7];
       volumeC1.pro.duhp = RealVariables6[8];
       volumeC1.pro.x = RealVariables6[9];
       singularPressureLoss5.deltaP = RealVariables6[10];
       singularPressureLoss5.rho = RealVariables6[11];
       singularPressureLoss5.T = RealVariables6[12];
       singularPressureLoss5.Pm = RealVariables6[13];
       singularPressureLoss5.pro.u = RealVariables6[14];
       singularPressureLoss5.pro.s = RealVariables6[15];
       singularPressureLoss5.pro.cp = RealVariables6[16];
       singularPressureLoss5.pro.ddhp = RealVariables6[17];
       singularPressureLoss5.pro.ddph = RealVariables6[18];
       singularPressureLoss5.pro.duph = RealVariables6[19];
       singularPressureLoss5.pro.duhp = RealVariables6[20];
       singularPressureLoss5.pro.x = RealVariables6[21];
       Bache_a.T = RealVariables6[22];
       Bache_a.h = RealVariables6[23];
       Bache_a.rho = RealVariables6[24];
       Bache_a.BH = RealVariables6[25];
       Bache_a.pro.u = RealVariables6[26];
       Bache_a.pro.s = RealVariables6[27];
       Bache_a.pro.cp = RealVariables6[28];
       Bache_a.pro.ddhp = RealVariables6[29];
       Bache_a.pro.ddph = RealVariables6[30];
       Bache_a.pro.duph = RealVariables6[31];
       Bache_a.pro.duhp = RealVariables6[32];
       Bache_a.pro.x = RealVariables6[33];
       Bache_a.Ce3.Q = RealVariables6[34];
       Bache_a.Ce3.h = RealVariables6[35];
       Bache_a.Ce1.Q = RealVariables6[36];
       Bache_a.Ce1.h = RealVariables6[37];
       Bache_a.Cs1.Q = RealVariables6[38];
       Bache_a.Cs1.h = RealVariables6[39];
       SuperHeat.W = RealVariables6[40];
       SuperHeat.Tec = RealVariables6[41];
       SuperHeat.Tsc = RealVariables6[42];
       SuperHeat.Tef = RealVariables6[43];
       SuperHeat.Tsf = RealVariables6[44];
       SuperHeat.DPfc = RealVariables6[45];
       SuperHeat.DPgc = RealVariables6[46];
       SuperHeat.DPc = RealVariables6[47];
       SuperHeat.DPff = RealVariables6[48];
       SuperHeat.DPgf = RealVariables6[49];
       SuperHeat.DPf = RealVariables6[50];
       SuperHeat.rhoc = RealVariables6[51];
       SuperHeat.rhof = RealVariables6[52];
       SuperHeat.Ec.h = RealVariables6[53];
       SuperHeat.Sf.h_vol = RealVariables6[54];
       SuperHeat.Sc.P = RealVariables6[55];
       SuperHeat.Sc.h_vol = RealVariables6[56];
       SuperHeat.proce.d = RealVariables6[57];
       SuperHeat.proce.u = RealVariables6[58];
       SuperHeat.proce.s = RealVariables6[59];
       SuperHeat.proce.cp = RealVariables6[60];
       SuperHeat.proce.ddhp = RealVariables6[61];
       SuperHeat.proce.ddph = RealVariables6[62];
       SuperHeat.proce.duph = RealVariables6[63];
       SuperHeat.proce.duhp = RealVariables6[64];
       SuperHeat.proce.x = RealVariables6[65];
       SuperHeat.procs.d = RealVariables6[66];
       SuperHeat.procs.u = RealVariables6[67];
       SuperHeat.procs.s = RealVariables6[68];
       SuperHeat.procs.cp = RealVariables6[69];
       SuperHeat.procs.ddhp = RealVariables6[70];
       SuperHeat.procs.ddph = RealVariables6[71];
       SuperHeat.procs.duph = RealVariables6[72];
       SuperHeat.procs.duhp = RealVariables6[73];
       SuperHeat.procs.x = RealVariables6[74];
       SuperHeat.profe.d = RealVariables6[75];
       SuperHeat.profe.u = RealVariables6[76];
       SuperHeat.profe.s = RealVariables6[77];
       SuperHeat.profe.cp = RealVariables6[78];
       SuperHeat.profe.ddhp = RealVariables6[79];
       SuperHeat.profe.ddph = RealVariables6[80];
       SuperHeat.profe.duph = RealVariables6[81];
       SuperHeat.profe.duhp = RealVariables6[82];
       SuperHeat.profe.x = RealVariables6[83];
       SuperHeat.promf.T = RealVariables6[84];
       SuperHeat.promf.u = RealVariables6[85];
       SuperHeat.promf.s = RealVariables6[86];
       SuperHeat.promf.cp = RealVariables6[87];
       SuperHeat.promf.ddhp = RealVariables6[88];
       SuperHeat.promf.ddph = RealVariables6[89];
       SuperHeat.promf.duph = RealVariables6[90];
       SuperHeat.promf.duhp = RealVariables6[91];
       SuperHeat.promf.x = RealVariables6[92];
       SuperHeat.lsat.P = RealVariables6[93];
       SuperHeat.lsat.T = RealVariables6[94];
       SuperHeat.lsat.rho = RealVariables6[95];
       SuperHeat.lsat.cp = RealVariables6[96];
       SuperHeat.lsat.pt = RealVariables6[97];
       SuperHeat.lsat.cv = RealVariables6[98];
       SuperHeat.vsat.P = RealVariables6[99];
       SuperHeat.vsat.T = RealVariables6[100];
       SuperHeat.vsat.rho = RealVariables7[1];
       SuperHeat.vsat.h = RealVariables7[2];
       SuperHeat.vsat.cp = RealVariables7[3];
       SuperHeat.vsat.pt = RealVariables7[4];
       SuperHeat.vsat.cv = RealVariables7[5];
       SuperHeat.promc.T = RealVariables7[6];
       SuperHeat.promc.u = RealVariables7[7];
       SuperHeat.promc.s = RealVariables7[8];
       SuperHeat.promc.cp = RealVariables7[9];
       SuperHeat.promc.ddhp = RealVariables7[10];
       SuperHeat.promc.ddph = RealVariables7[11];
       SuperHeat.promc.duph = RealVariables7[12];
       SuperHeat.promc.duhp = RealVariables7[13];
       SuperHeat.promc.x = RealVariables7[14];
       SuperHeat.profs.d = RealVariables7[15];
       SuperHeat.profs.u = RealVariables7[16];
       SuperHeat.profs.s = RealVariables7[17];
       SuperHeat.profs.cp = RealVariables7[18];
       SuperHeat.profs.ddhp = RealVariables7[19];
       SuperHeat.profs.ddph = RealVariables7[20];
       SuperHeat.profs.duph = RealVariables7[21];
       SuperHeat.profs.duhp = RealVariables7[22];
       SuperHeat.profs.x = RealVariables7[23];
       singularPressureLoss6.deltaP = RealVariables7[24];
       singularPressureLoss6.rho = RealVariables7[25];
       singularPressureLoss6.T = RealVariables7[26];
       singularPressureLoss6.Pm = RealVariables7[27];
       singularPressureLoss6.pro.u = RealVariables7[28];
       singularPressureLoss6.pro.s = RealVariables7[29];
       singularPressureLoss6.pro.cp = RealVariables7[30];
       singularPressureLoss6.pro.ddhp = RealVariables7[31];
       singularPressureLoss6.pro.ddph = RealVariables7[32];
       singularPressureLoss6.pro.duph = RealVariables7[33];
       singularPressureLoss6.pro.duhp = RealVariables7[34];
       singularPressureLoss6.pro.x = RealVariables7[35];
       Turb_LP1_b.eta_is = RealVariables7[36];
       Turb_LP1_b.eta_is_wet = RealVariables7[37];
       Turb_LP1_b.W = RealVariables7[38];
       Turb_LP1_b.Q = RealVariables7[39];
       Turb_LP1_b.His = RealVariables7[40];
       Turb_LP1_b.Hrs = RealVariables7[41];
       Turb_LP1_b.Pe = RealVariables7[42];
       Turb_LP1_b.Te = RealVariables7[43];
       Turb_LP1_b.Ts = RealVariables7[44];
       Turb_LP1_b.Vs = RealVariables7[45];
       Turb_LP1_b.rhos = RealVariables7[46];
       Turb_LP1_b.xm = RealVariables7[47];
       Turb_LP1_b.proe.d = RealVariables7[48];
       Turb_LP1_b.proe.u = RealVariables7[49];
       Turb_LP1_b.proe.s = RealVariables7[50];
       Turb_LP1_b.proe.cp = RealVariables7[51];
       Turb_LP1_b.proe.ddhp = RealVariables7[52];
       Turb_LP1_b.proe.ddph = RealVariables7[53];
       Turb_LP1_b.proe.duph = RealVariables7[54];
       Turb_LP1_b.proe.duhp = RealVariables7[55];
       Turb_LP1_b.proe.x = RealVariables7[56];
       Turb_LP1_b.pros.d = RealVariables7[57];
       Turb_LP1_b.pros.u = RealVariables7[58];
       Turb_LP1_b.pros.s = RealVariables7[59];
       Turb_LP1_b.pros.cp = RealVariables7[60];
       Turb_LP1_b.pros.ddhp = RealVariables7[61];
       Turb_LP1_b.pros.ddph = RealVariables7[62];
       Turb_LP1_b.pros.duph = RealVariables7[63];
       Turb_LP1_b.pros.duhp = RealVariables7[64];
       Turb_LP1_b.pros.x = RealVariables7[65];
       Turb_LP1_b.Cs.h = RealVariables7[66];
       Turb_LP1_b.props.T = RealVariables7[67];
       Turb_LP1_b.props.d = RealVariables7[68];
       Turb_LP1_b.props.u = RealVariables7[69];
       Turb_LP1_b.props.cp = RealVariables7[70];
       Turb_LP1_b.props.ddsp = RealVariables7[71];
       Turb_LP1_b.props.ddps = RealVariables7[72];
       Turb_LP1_b.props.x = RealVariables7[73];
       Turb_LP1_b.pros1.T = RealVariables7[74];
       Turb_LP1_b.pros1.d = RealVariables7[75];
       Turb_LP1_b.pros1.u = RealVariables7[76];
       Turb_LP1_b.pros1.s = RealVariables7[77];
       Turb_LP1_b.pros1.cp = RealVariables7[78];
       Turb_LP1_b.pros1.ddhp = RealVariables7[79];
       Turb_LP1_b.pros1.ddph = RealVariables7[80];
       Turb_LP1_b.pros1.duph = RealVariables7[81];
       Turb_LP1_b.pros1.duhp = RealVariables7[82];
       Turb_LP1_b.pros1.x = RealVariables7[83];
       Extraction_ReheatBP.x_ex = RealVariables7[84];
       Extraction_ReheatBP.proe.T = RealVariables7[85];
       Extraction_ReheatBP.proe.d = RealVariables7[86];
       Extraction_ReheatBP.proe.u = RealVariables7[87];
       Extraction_ReheatBP.proe.s = RealVariables7[88];
       Extraction_ReheatBP.proe.cp = RealVariables7[89];
       Extraction_ReheatBP.proe.ddhp = RealVariables7[90];
       Extraction_ReheatBP.proe.ddph = RealVariables7[91];
       Extraction_ReheatBP.proe.duph = RealVariables7[92];
       Extraction_ReheatBP.proe.duhp = RealVariables7[93];
       Extraction_ReheatBP.proe.x = RealVariables7[94];
       Extraction_ReheatBP.lsat.P = RealVariables7[95];
       Extraction_ReheatBP.lsat.T = RealVariables7[96];
       Extraction_ReheatBP.lsat.rho = RealVariables7[97];
       Extraction_ReheatBP.lsat.h = RealVariables7[98];
       Extraction_ReheatBP.lsat.cp = RealVariables7[99];
       Extraction_ReheatBP.lsat.pt = RealVariables7[100];
       Extraction_ReheatBP.lsat.cv = RealVariables8[1];
       Extraction_ReheatBP.vsat.P = RealVariables8[2];
       Extraction_ReheatBP.vsat.T = RealVariables8[3];
       Extraction_ReheatBP.vsat.rho = RealVariables8[4];
       Extraction_ReheatBP.vsat.h = RealVariables8[5];
       Extraction_ReheatBP.vsat.cp = RealVariables8[6];
       Extraction_ReheatBP.vsat.pt = RealVariables8[7];
       Extraction_ReheatBP.vsat.cv = RealVariables8[8];
       splitter_SGoutlet.alpha1 = RealVariables8[9];
       splitter_SGoutlet.alpha2 = RealVariables8[10];
       splitter_SGoutlet.T = RealVariables8[11];
       splitter_SGoutlet.Cs1.Q = RealVariables8[12];
       splitter_SGoutlet.Cs1.h = RealVariables8[13];
       splitter_SGoutlet.pro.d = RealVariables8[14];
       splitter_SGoutlet.pro.u = RealVariables8[15];
       splitter_SGoutlet.pro.s = RealVariables8[16];
       splitter_SGoutlet.pro.cp = RealVariables8[17];
       splitter_SGoutlet.pro.ddhp = RealVariables8[18];
       splitter_SGoutlet.pro.ddph = RealVariables8[19];
       splitter_SGoutlet.pro.duph = RealVariables8[20];
       splitter_SGoutlet.pro.duhp = RealVariables8[21];
       splitter_SGoutlet.pro.x = RealVariables8[22];
       Bache_b.T = RealVariables8[23];
       Bache_b.h = RealVariables8[24];
       Bache_b.rho = RealVariables8[25];
       Bache_b.BH = RealVariables8[26];
       Bache_b.pro.u = RealVariables8[27];
       Bache_b.pro.s = RealVariables8[28];
       Bache_b.pro.cp = RealVariables8[29];
       Bache_b.pro.ddhp = RealVariables8[30];
       Bache_b.pro.ddph = RealVariables8[31];
       Bache_b.pro.duph = RealVariables8[32];
       Bache_b.pro.duhp = RealVariables8[33];
       Bache_b.pro.x = RealVariables8[34];
       Bache_b.Ce1.Q = RealVariables8[35];
       Bache_b.Ce1.h = RealVariables8[36];
       Extraction_TurbBP1a_Outlet.x_ex = RealVariables8[37];
       Extraction_TurbBP1a_Outlet.proe.T = RealVariables8[38];
       Extraction_TurbBP1a_Outlet.proe.d = RealVariables8[39];
       Extraction_TurbBP1a_Outlet.proe.u = RealVariables8[40];
       Extraction_TurbBP1a_Outlet.proe.s = RealVariables8[41];
       Extraction_TurbBP1a_Outlet.proe.cp = RealVariables8[42];
       Extraction_TurbBP1a_Outlet.proe.ddhp = RealVariables8[43];
       Extraction_TurbBP1a_Outlet.proe.ddph = RealVariables8[44];
       Extraction_TurbBP1a_Outlet.proe.duph = RealVariables8[45];
       Extraction_TurbBP1a_Outlet.proe.duhp = RealVariables8[46];
       Extraction_TurbBP1a_Outlet.proe.x = RealVariables8[47];
       Extraction_TurbBP1a_Outlet.lsat.P = RealVariables8[48];
       Extraction_TurbBP1a_Outlet.lsat.T = RealVariables8[49];
       Extraction_TurbBP1a_Outlet.lsat.rho = RealVariables8[50];
       Extraction_TurbBP1a_Outlet.lsat.h = RealVariables8[51];
       Extraction_TurbBP1a_Outlet.lsat.cp = RealVariables8[52];
       Extraction_TurbBP1a_Outlet.lsat.pt = RealVariables8[53];
       Extraction_TurbBP1a_Outlet.lsat.cv = RealVariables8[54];
       Extraction_TurbBP1a_Outlet.vsat.P = RealVariables8[55];
       Extraction_TurbBP1a_Outlet.vsat.T = RealVariables8[56];
       Extraction_TurbBP1a_Outlet.vsat.rho = RealVariables8[57];
       Extraction_TurbBP1a_Outlet.vsat.h = RealVariables8[58];
       Extraction_TurbBP1a_Outlet.vsat.cp = RealVariables8[59];
       Extraction_TurbBP1a_Outlet.vsat.pt = RealVariables8[60];
       Extraction_TurbBP1a_Outlet.vsat.cv = RealVariables8[61];
       Extraction_TurbBP1a_Outlet.Cex.h_vol = RealVariables8[62];
       Extraction_TurbBP1a_Outlet.Cex.Q = RealVariables8[63];
       Extraction_TurbBP1a_Outlet.Cex.h = RealVariables8[64];
       Reheat_BP.HsateC = RealVariables8[65];
       Reheat_BP.HsatvC = RealVariables8[66];
       Reheat_BP.SDes = RealVariables8[67];
       Reheat_BP.HeiF = RealVariables8[68];
       Reheat_BP.HDesF = RealVariables8[69];
       Reheat_BP.TeiF = RealVariables8[70];
       Reheat_BP.TsatC = RealVariables8[71];
       Reheat_BP.W = RealVariables8[72];
       Reheat_BP.Wdes = RealVariables8[73];
       Reheat_BP.Wcond = RealVariables8[74];
       Reheat_BP.Wflash = RealVariables8[75];
       Reheat_BP.Wpurge = RealVariables8[76];
       Reheat_BP.Hep = RealVariables8[77];
       Reheat_BP.rho = RealVariables8[78];
       Reheat_BP.proeeF.T = RealVariables8[79];
       Reheat_BP.proeeF.d = RealVariables8[80];
       Reheat_BP.proeeF.u = RealVariables8[81];
       Reheat_BP.proeeF.s = RealVariables8[82];
       Reheat_BP.proeeF.cp = RealVariables8[83];
       Reheat_BP.proeeF.ddhp = RealVariables8[84];
       Reheat_BP.proeeF.ddph = RealVariables8[85];
       Reheat_BP.proeeF.duph = RealVariables8[86];
       Reheat_BP.proeeF.duhp = RealVariables8[87];
       Reheat_BP.proeeF.x = RealVariables8[88];
       Reheat_BP.proseF.T = RealVariables8[89];
       Reheat_BP.proseF.d = RealVariables8[90];
       Reheat_BP.proseF.u = RealVariables8[91];
       Reheat_BP.proseF.s = RealVariables8[92];
       Reheat_BP.proseF.cp = RealVariables8[93];
       Reheat_BP.proseF.ddhp = RealVariables8[94];
       Reheat_BP.proseF.ddph = RealVariables8[95];
       Reheat_BP.proseF.duph = RealVariables8[96];
       Reheat_BP.proseF.duhp = RealVariables8[97];
       Reheat_BP.proseF.x = RealVariables8[98];
       Reheat_BP.prospC.T = RealVariables8[99];
       Reheat_BP.prospC.d = RealVariables8[100];
       Reheat_BP.prospC.u = RealVariables9[1];
       Reheat_BP.prospC.s = RealVariables9[2];
       Reheat_BP.prospC.cp = RealVariables9[3];
       Reheat_BP.prospC.ddhp = RealVariables9[4];
       Reheat_BP.prospC.ddph = RealVariables9[5];
       Reheat_BP.prospC.duph = RealVariables9[6];
       Reheat_BP.prospC.duhp = RealVariables9[7];
       Reheat_BP.prospC.x = RealVariables9[8];
       Reheat_BP.Sp.Q = RealVariables9[9];
       Reheat_BP.Sp.h = RealVariables9[10];
       Reheat_BP.proevC.T = RealVariables9[11];
       Reheat_BP.proevC.d = RealVariables9[12];
       Reheat_BP.proevC.u = RealVariables9[13];
       Reheat_BP.proevC.s = RealVariables9[14];
       Reheat_BP.proevC.cp = RealVariables9[15];
       Reheat_BP.proevC.ddhp = RealVariables9[16];
       Reheat_BP.proevC.ddph = RealVariables9[17];
       Reheat_BP.proevC.duph = RealVariables9[18];
       Reheat_BP.proevC.duhp = RealVariables9[19];
       Reheat_BP.proevC.x = RealVariables9[20];
       Reheat_BP.lsatC.P = RealVariables9[21];
       Reheat_BP.lsatC.rho = RealVariables9[22];
       Reheat_BP.lsatC.cp = RealVariables9[23];
       Reheat_BP.lsatC.pt = RealVariables9[24];
       Reheat_BP.lsatC.cv = RealVariables9[25];
       Reheat_BP.vsatC.P = RealVariables9[26];
       Reheat_BP.vsatC.T = RealVariables9[27];
       Reheat_BP.vsatC.rho = RealVariables9[28];
       Reheat_BP.vsatC.cp = RealVariables9[29];
       Reheat_BP.vsatC.pt = RealVariables9[30];
       Reheat_BP.vsatC.cv = RealVariables9[31];
       Reheat_BP.promeF.T = RealVariables9[32];
       Reheat_BP.promeF.u = RealVariables9[33];
       Reheat_BP.promeF.s = RealVariables9[34];
       Reheat_BP.promeF.cp = RealVariables9[35];
       Reheat_BP.promeF.ddhp = RealVariables9[36];
       Reheat_BP.promeF.ddph = RealVariables9[37];
       Reheat_BP.promeF.duph = RealVariables9[38];
       Reheat_BP.promeF.duhp = RealVariables9[39];
       Reheat_BP.promeF.x = RealVariables9[40];
       Reheat_BP.prodesmC.T = RealVariables9[41];
       Reheat_BP.prodesmC.d = RealVariables9[42];
       Reheat_BP.prodesmC.u = RealVariables9[43];
       Reheat_BP.prodesmC.s = RealVariables9[44];
       Reheat_BP.prodesmC.cp = RealVariables9[45];
       Reheat_BP.prodesmC.ddhp = RealVariables9[46];
       Reheat_BP.prodesmC.ddph = RealVariables9[47];
       Reheat_BP.prodesmC.duph = RealVariables9[48];
       Reheat_BP.prodesmC.duhp = RealVariables9[49];
       Reheat_BP.prodesmC.x = RealVariables9[50];
       Reheat_BP.promcF.T = RealVariables9[51];
       Reheat_BP.promcF.d = RealVariables9[52];
       Reheat_BP.promcF.u = RealVariables9[53];
       Reheat_BP.promcF.s = RealVariables9[54];
       Reheat_BP.promcF.cp = RealVariables9[55];
       Reheat_BP.promcF.ddhp = RealVariables9[56];
       Reheat_BP.promcF.ddph = RealVariables9[57];
       Reheat_BP.promcF.duph = RealVariables9[58];
       Reheat_BP.promcF.duhp = RealVariables9[59];
       Reheat_BP.promcF.x = RealVariables9[60];
       Reheat_BP.prodesF.T = RealVariables9[61];
       Reheat_BP.prodesF.d = RealVariables9[62];
       Reheat_BP.prodesF.u = RealVariables9[63];
       Reheat_BP.prodesF.s = RealVariables9[64];
       Reheat_BP.prodesF.cp = RealVariables9[65];
       Reheat_BP.prodesF.ddhp = RealVariables9[66];
       Reheat_BP.prodesF.ddph = RealVariables9[67];
       Reheat_BP.prodesF.duph = RealVariables9[68];
       Reheat_BP.prodesF.duhp = RealVariables9[69];
       Reheat_BP.prodesF.x = RealVariables9[70];
       Reheat_BP.prodesmF.T = RealVariables9[71];
       Reheat_BP.prodesmF.d = RealVariables9[72];
       Reheat_BP.prodesmF.u = RealVariables9[73];
       Reheat_BP.prodesmF.s = RealVariables9[74];
       Reheat_BP.prodesmF.cp = RealVariables9[75];
       Reheat_BP.prodesmF.ddhp = RealVariables9[76];
       Reheat_BP.prodesmF.ddph = RealVariables9[77];
       Reheat_BP.prodesmF.duph = RealVariables9[78];
       Reheat_BP.prodesmF.duhp = RealVariables9[79];
       Reheat_BP.prodesmF.x = RealVariables9[80];
       Reheat_BP.prosp.T = RealVariables9[81];
       Reheat_BP.prosp.d = RealVariables9[82];
       Reheat_BP.prosp.u = RealVariables9[83];
       Reheat_BP.prosp.s = RealVariables9[84];
       Reheat_BP.prosp.cp = RealVariables9[85];
       Reheat_BP.prosp.ddhp = RealVariables9[86];
       Reheat_BP.prosp.ddph = RealVariables9[87];
       Reheat_BP.prosp.duph = RealVariables9[88];
       Reheat_BP.prosp.duhp = RealVariables9[89];
       Reheat_BP.prosp.x = RealVariables9[90];
       Reheat_BP.prompC.T = RealVariables9[91];
       Reheat_BP.prompC.d = RealVariables9[92];
       Reheat_BP.prompC.u = RealVariables9[93];
       Reheat_BP.prompC.s = RealVariables9[94];
       Reheat_BP.prompC.cp = RealVariables9[95];
       Reheat_BP.prompC.ddhp = RealVariables9[96];
       Reheat_BP.prompC.ddph = RealVariables9[97];
       Reheat_BP.prompC.duph = RealVariables9[98];
       Reheat_BP.prompC.duhp = RealVariables9[99];
       Reheat_BP.prompC.x = RealVariables9[100];
       Reheat_BP.prompF.T = RealVariables10[1];
       Reheat_BP.prompF.d = RealVariables10[2];
       Reheat_BP.prompF.u = RealVariables10[3];
       Reheat_BP.prompF.s = RealVariables10[4];
       Reheat_BP.prompF.cp = RealVariables10[5];
       Reheat_BP.prompF.ddhp = RealVariables10[6];
       Reheat_BP.prompF.ddph = RealVariables10[7];
       Reheat_BP.prompF.duph = RealVariables10[8];
       Reheat_BP.prompF.duhp = RealVariables10[9];
       Reheat_BP.prompF.x = RealVariables10[10];
       Reheat_BP.proecF.T = RealVariables10[11];
       Reheat_BP.proecF.d = RealVariables10[12];
       Reheat_BP.proecF.u = RealVariables10[13];
       Reheat_BP.proecF.s = RealVariables10[14];
       Reheat_BP.proecF.cp = RealVariables10[15];
       Reheat_BP.proecF.ddhp = RealVariables10[16];
       Reheat_BP.proecF.ddph = RealVariables10[17];
       Reheat_BP.proecF.duph = RealVariables10[18];
       Reheat_BP.proecF.duhp = RealVariables10[19];
       Reheat_BP.proecF.x = RealVariables10[20];
       Reheat_BP.flashepC.T = RealVariables10[21];
       Reheat_BP.flashepC.d = RealVariables10[22];
       Reheat_BP.flashepC.u = RealVariables10[23];
       Reheat_BP.flashepC.s = RealVariables10[24];
       Reheat_BP.flashepC.cp = RealVariables10[25];
       Reheat_BP.flashepC.ddhp = RealVariables10[26];
       Reheat_BP.flashepC.ddph = RealVariables10[27];
       Reheat_BP.flashepC.duph = RealVariables10[28];
       Reheat_BP.flashepC.duhp = RealVariables10[29];
       Reheat_BP.flashepC.x = RealVariables10[30];
       invSingularPressureLoss1.K = RealVariables10[31];
       invSingularPressureLoss1.deltaP = RealVariables10[32];
       invSingularPressureLoss1.rho = RealVariables10[33];
       invSingularPressureLoss1.T = RealVariables10[34];
       invSingularPressureLoss1.Pm = RealVariables10[35];
       invSingularPressureLoss1.pro.u = RealVariables10[36];
       invSingularPressureLoss1.pro.s = RealVariables10[37];
       invSingularPressureLoss1.pro.cp = RealVariables10[38];
       invSingularPressureLoss1.pro.ddhp = RealVariables10[39];
       invSingularPressureLoss1.pro.ddph = RealVariables10[40];
       invSingularPressureLoss1.pro.duph = RealVariables10[41];
       invSingularPressureLoss1.pro.duhp = RealVariables10[42];
       invSingularPressureLoss1.pro.x = RealVariables10[43];
       ReHeat_HP.HsateC = RealVariables10[44];
       ReHeat_HP.HsatvC = RealVariables10[45];
       ReHeat_HP.SDes = RealVariables10[46];
       ReHeat_HP.HeiF = RealVariables10[47];
       ReHeat_HP.HDesF = RealVariables10[48];
       ReHeat_HP.TeiF = RealVariables10[49];
       ReHeat_HP.TsatC = RealVariables10[50];
       ReHeat_HP.W = RealVariables10[51];
       ReHeat_HP.Wdes = RealVariables10[52];
       ReHeat_HP.Wcond = RealVariables10[53];
       ReHeat_HP.Wflash = RealVariables10[54];
       ReHeat_HP.Wpurge = RealVariables10[55];
       ReHeat_HP.Hep = RealVariables10[56];
       ReHeat_HP.proeeF.T = RealVariables10[57];
       ReHeat_HP.proeeF.d = RealVariables10[58];
       ReHeat_HP.proeeF.u = RealVariables10[59];
       ReHeat_HP.proeeF.s = RealVariables10[60];
       ReHeat_HP.proeeF.cp = RealVariables10[61];
       ReHeat_HP.proeeF.ddhp = RealVariables10[62];
       ReHeat_HP.proeeF.ddph = RealVariables10[63];
       ReHeat_HP.proeeF.duph = RealVariables10[64];
       ReHeat_HP.proeeF.duhp = RealVariables10[65];
       ReHeat_HP.proeeF.x = RealVariables10[66];
       ReHeat_HP.proseF.T = RealVariables10[67];
       ReHeat_HP.proseF.d = RealVariables10[68];
       ReHeat_HP.proseF.u = RealVariables10[69];
       ReHeat_HP.proseF.s = RealVariables10[70];
       ReHeat_HP.proseF.cp = RealVariables10[71];
       ReHeat_HP.proseF.ddhp = RealVariables10[72];
       ReHeat_HP.proseF.ddph = RealVariables10[73];
       ReHeat_HP.proseF.duph = RealVariables10[74];
       ReHeat_HP.proseF.duhp = RealVariables10[75];
       ReHeat_HP.proseF.x = RealVariables10[76];
       ReHeat_HP.prospC.T = RealVariables10[77];
       ReHeat_HP.prospC.d = RealVariables10[78];
       ReHeat_HP.prospC.u = RealVariables10[79];
       ReHeat_HP.prospC.s = RealVariables10[80];
       ReHeat_HP.prospC.cp = RealVariables10[81];
       ReHeat_HP.prospC.ddhp = RealVariables10[82];
       ReHeat_HP.prospC.ddph = RealVariables10[83];
       ReHeat_HP.prospC.duph = RealVariables10[84];
       ReHeat_HP.prospC.duhp = RealVariables10[85];
       ReHeat_HP.prospC.x = RealVariables10[86];
       ReHeat_HP.Se.P = RealVariables10[87];
       ReHeat_HP.Se.h = RealVariables10[88];
       ReHeat_HP.proevC.T = RealVariables10[89];
       ReHeat_HP.proevC.d = RealVariables10[90];
       ReHeat_HP.proevC.u = RealVariables10[91];
       ReHeat_HP.proevC.s = RealVariables10[92];
       ReHeat_HP.proevC.cp = RealVariables10[93];
       ReHeat_HP.proevC.ddhp = RealVariables10[94];
       ReHeat_HP.proevC.ddph = RealVariables10[95];
       ReHeat_HP.proevC.duph = RealVariables10[96];
       ReHeat_HP.proevC.duhp = RealVariables10[97];
       ReHeat_HP.proevC.x = RealVariables10[98];
       ReHeat_HP.lsatC.P = RealVariables10[99];
       ReHeat_HP.lsatC.rho = RealVariables10[100];
       ReHeat_HP.lsatC.cp = RealVariables11[1];
       ReHeat_HP.lsatC.pt = RealVariables11[2];
       ReHeat_HP.lsatC.cv = RealVariables11[3];
       ReHeat_HP.vsatC.P = RealVariables11[4];
       ReHeat_HP.vsatC.T = RealVariables11[5];
       ReHeat_HP.vsatC.rho = RealVariables11[6];
       ReHeat_HP.vsatC.cp = RealVariables11[7];
       ReHeat_HP.vsatC.pt = RealVariables11[8];
       ReHeat_HP.vsatC.cv = RealVariables11[9];
       ReHeat_HP.promeF.T = RealVariables11[10];
       ReHeat_HP.promeF.d = RealVariables11[11];
       ReHeat_HP.promeF.u = RealVariables11[12];
       ReHeat_HP.promeF.s = RealVariables11[13];
       ReHeat_HP.promeF.cp = RealVariables11[14];
       ReHeat_HP.promeF.ddhp = RealVariables11[15];
       ReHeat_HP.promeF.ddph = RealVariables11[16];
       ReHeat_HP.promeF.duph = RealVariables11[17];
       ReHeat_HP.promeF.duhp = RealVariables11[18];
       ReHeat_HP.promeF.x = RealVariables11[19];
       ReHeat_HP.prodesmC.T = RealVariables11[20];
       ReHeat_HP.prodesmC.d = RealVariables11[21];
       ReHeat_HP.prodesmC.u = RealVariables11[22];
       ReHeat_HP.prodesmC.s = RealVariables11[23];
       ReHeat_HP.prodesmC.cp = RealVariables11[24];
       ReHeat_HP.prodesmC.ddhp = RealVariables11[25];
       ReHeat_HP.prodesmC.ddph = RealVariables11[26];
       ReHeat_HP.prodesmC.duph = RealVariables11[27];
       ReHeat_HP.prodesmC.duhp = RealVariables11[28];
       ReHeat_HP.prodesmC.x = RealVariables11[29];
       ReHeat_HP.promcF.T = RealVariables11[30];
       ReHeat_HP.promcF.d = RealVariables11[31];
       ReHeat_HP.promcF.u = RealVariables11[32];
       ReHeat_HP.promcF.s = RealVariables11[33];
       ReHeat_HP.promcF.cp = RealVariables11[34];
       ReHeat_HP.promcF.ddhp = RealVariables11[35];
       ReHeat_HP.promcF.ddph = RealVariables11[36];
       ReHeat_HP.promcF.duph = RealVariables11[37];
       ReHeat_HP.promcF.duhp = RealVariables11[38];
       ReHeat_HP.promcF.x = RealVariables11[39];
       ReHeat_HP.prodesF.T = RealVariables11[40];
       ReHeat_HP.prodesF.d = RealVariables11[41];
       ReHeat_HP.prodesF.u = RealVariables11[42];
       ReHeat_HP.prodesF.s = RealVariables11[43];
       ReHeat_HP.prodesF.cp = RealVariables11[44];
       ReHeat_HP.prodesF.ddhp = RealVariables11[45];
       ReHeat_HP.prodesF.ddph = RealVariables11[46];
       ReHeat_HP.prodesF.duph = RealVariables11[47];
       ReHeat_HP.prodesF.duhp = RealVariables11[48];
       ReHeat_HP.prodesF.x = RealVariables11[49];
       ReHeat_HP.prodesmF.T = RealVariables11[50];
       ReHeat_HP.prodesmF.d = RealVariables11[51];
       ReHeat_HP.prodesmF.u = RealVariables11[52];
       ReHeat_HP.prodesmF.s = RealVariables11[53];
       ReHeat_HP.prodesmF.cp = RealVariables11[54];
       ReHeat_HP.prodesmF.ddhp = RealVariables11[55];
       ReHeat_HP.prodesmF.ddph = RealVariables11[56];
       ReHeat_HP.prodesmF.duph = RealVariables11[57];
       ReHeat_HP.prodesmF.duhp = RealVariables11[58];
       ReHeat_HP.prodesmF.x = RealVariables11[59];
       ReHeat_HP.prosp.T = RealVariables11[60];
       ReHeat_HP.prosp.d = RealVariables11[61];
       ReHeat_HP.prosp.u = RealVariables11[62];
       ReHeat_HP.prosp.s = RealVariables11[63];
       ReHeat_HP.prosp.cp = RealVariables11[64];
       ReHeat_HP.prosp.ddhp = RealVariables11[65];
       ReHeat_HP.prosp.ddph = RealVariables11[66];
       ReHeat_HP.prosp.duph = RealVariables11[67];
       ReHeat_HP.prosp.duhp = RealVariables11[68];
       ReHeat_HP.prosp.x = RealVariables11[69];
       ReHeat_HP.prompC.T = RealVariables11[70];
       ReHeat_HP.prompC.d = RealVariables11[71];
       ReHeat_HP.prompC.u = RealVariables11[72];
       ReHeat_HP.prompC.s = RealVariables11[73];
       ReHeat_HP.prompC.cp = RealVariables11[74];
       ReHeat_HP.prompC.ddhp = RealVariables11[75];
       ReHeat_HP.prompC.ddph = RealVariables11[76];
       ReHeat_HP.prompC.duph = RealVariables11[77];
       ReHeat_HP.prompC.duhp = RealVariables11[78];
       ReHeat_HP.prompC.x = RealVariables11[79];
       ReHeat_HP.prompF.T = RealVariables11[80];
       ReHeat_HP.prompF.d = RealVariables11[81];
       ReHeat_HP.prompF.u = RealVariables11[82];
       ReHeat_HP.prompF.s = RealVariables11[83];
       ReHeat_HP.prompF.cp = RealVariables11[84];
       ReHeat_HP.prompF.ddhp = RealVariables11[85];
       ReHeat_HP.prompF.ddph = RealVariables11[86];
       ReHeat_HP.prompF.duph = RealVariables11[87];
       ReHeat_HP.prompF.duhp = RealVariables11[88];
       ReHeat_HP.prompF.x = RealVariables11[89];
       ReHeat_HP.proecF.T = RealVariables11[90];
       ReHeat_HP.proecF.d = RealVariables11[91];
       ReHeat_HP.proecF.u = RealVariables11[92];
       ReHeat_HP.proecF.s = RealVariables11[93];
       ReHeat_HP.proecF.cp = RealVariables11[94];
       ReHeat_HP.proecF.ddhp = RealVariables11[95];
       ReHeat_HP.proecF.ddph = RealVariables11[96];
       ReHeat_HP.proecF.duph = RealVariables11[97];
       ReHeat_HP.proecF.duhp = RealVariables11[98];
       ReHeat_HP.proecF.x = RealVariables11[99];
       ReHeat_HP.flashepC.T = RealVariables11[100];
       ReHeat_HP.flashepC.d = RealVariables12[1];
       ReHeat_HP.flashepC.u = RealVariables12[2];
       ReHeat_HP.flashepC.s = RealVariables12[3];
       ReHeat_HP.flashepC.cp = RealVariables12[4];
       ReHeat_HP.flashepC.ddhp = RealVariables12[5];
       ReHeat_HP.flashepC.ddph = RealVariables12[6];
       ReHeat_HP.flashepC.duph = RealVariables12[7];
       ReHeat_HP.flashepC.duhp = RealVariables12[8];
       ReHeat_HP.flashepC.x = RealVariables12[9];
       invSingularPressureLoss2.K = RealVariables12[10];
       invSingularPressureLoss2.deltaP = RealVariables12[11];
       invSingularPressureLoss2.rho = RealVariables12[12];
       invSingularPressureLoss2.T = RealVariables12[13];
       invSingularPressureLoss2.Pm = RealVariables12[14];
       invSingularPressureLoss2.pro.u = RealVariables12[15];
       invSingularPressureLoss2.pro.s = RealVariables12[16];
       invSingularPressureLoss2.pro.cp = RealVariables12[17];
       invSingularPressureLoss2.pro.ddhp = RealVariables12[18];
       invSingularPressureLoss2.pro.ddph = RealVariables12[19];
       invSingularPressureLoss2.pro.duph = RealVariables12[20];
       invSingularPressureLoss2.pro.duhp = RealVariables12[21];
       invSingularPressureLoss2.pro.x = RealVariables12[22];
       volumeC2.T = RealVariables12[23];
       volumeC2.h = RealVariables12[24];
       volumeC2.rho = RealVariables12[25];
       volumeC2.BH = RealVariables12[26];
       volumeC2.pro.u = RealVariables12[27];
       volumeC2.pro.s = RealVariables12[28];
       volumeC2.pro.cp = RealVariables12[29];
       volumeC2.pro.ddhp = RealVariables12[30];
       volumeC2.pro.ddph = RealVariables12[31];
       volumeC2.pro.duph = RealVariables12[32];
       volumeC2.pro.duhp = RealVariables12[33];
       volumeC2.pro.x = RealVariables12[34];
       singularPressureLoss1.deltaP = RealVariables12[35];
       singularPressureLoss1.rho = RealVariables12[36];
       singularPressureLoss1.T = RealVariables12[37];
       singularPressureLoss1.Pm = RealVariables12[38];
       singularPressureLoss1.pro.u = RealVariables12[39];
       singularPressureLoss1.pro.s = RealVariables12[40];
       singularPressureLoss1.pro.cp = RealVariables12[41];
       singularPressureLoss1.pro.ddhp = RealVariables12[42];
       singularPressureLoss1.pro.ddph = RealVariables12[43];
       singularPressureLoss1.pro.duph = RealVariables12[44];
       singularPressureLoss1.pro.duhp = RealVariables12[45];
       singularPressureLoss1.pro.x = RealVariables12[46];
       Valve_superheat.Cv = RealVariables12[47];
       Valve_superheat.deltaP = RealVariables12[48];
       Valve_superheat.rho = RealVariables12[49];
       Valve_superheat.T = RealVariables12[50];
       Valve_superheat.Pm = RealVariables12[51];
       Valve_superheat.pro.d = RealVariables12[52];
       Valve_superheat.pro.u = RealVariables12[53];
       Valve_superheat.pro.s = RealVariables12[54];
       Valve_superheat.pro.cp = RealVariables12[55];
       Valve_superheat.pro.ddhp = RealVariables12[56];
       Valve_superheat.pro.ddph = RealVariables12[57];
       Valve_superheat.pro.duph = RealVariables12[58];
       Valve_superheat.pro.duhp = RealVariables12[59];
       Valve_superheat.pro.x = RealVariables12[60];
       Valve_superheat.Ouv.signal = RealVariables12[61];
       Tin_Turb_LP1a.T = RealVariables12[62];
       Tin_Turb_LP1a.pro.d = RealVariables12[63];
       Tin_Turb_LP1a.pro.u = RealVariables12[64];
       Tin_Turb_LP1a.pro.s = RealVariables12[65];
       Tin_Turb_LP1a.pro.cp = RealVariables12[66];
       Tin_Turb_LP1a.pro.ddhp = RealVariables12[67];
       Tin_Turb_LP1a.pro.ddph = RealVariables12[68];
       Tin_Turb_LP1a.pro.duph = RealVariables12[69];
       Tin_Turb_LP1a.pro.duhp = RealVariables12[70];
       Tin_Turb_LP1a.pro.x = RealVariables12[71];
       Tin_Turb_LP1a.Measure.signal = RealVariables12[72];
       feedback5.u2.signal = RealVariables12[73];
       feedback5.y.signal = RealVariables12[74];
       pI5.x = RealVariables12[75];
       Pipe_SGs.deltaPf = RealVariables12[76];
       Pipe_SGs.deltaP = RealVariables12[77];
       Pipe_SGs.Re = RealVariables12[78];
       Pipe_SGs.rho = RealVariables12[79];
       Pipe_SGs.mu = RealVariables12[80];
       Pipe_SGs.T = RealVariables12[81];
       Pipe_SGs.Pm = RealVariables12[82];
       Pipe_SGs.C1.P = RealVariables12[83];
       Pipe_SGs.C1.h_vol = RealVariables12[84];
       Pipe_SGs.pro.u = RealVariables12[85];
       Pipe_SGs.pro.s = RealVariables12[86];
       Pipe_SGs.pro.cp = RealVariables12[87];
       Pipe_SGs.pro.ddhp = RealVariables12[88];
       Pipe_SGs.pro.ddph = RealVariables12[89];
       Pipe_SGs.pro.duph = RealVariables12[90];
       Pipe_SGs.pro.duhp = RealVariables12[91];
       Pipe_SGs.pro.x = RealVariables12[92];
       singularPressureLoss_SG_Out.deltaP = RealVariables12[93];
       singularPressureLoss_SG_Out.rho = RealVariables12[94];
       singularPressureLoss_SG_Out.T = RealVariables12[95];
       singularPressureLoss_SG_Out.Pm = RealVariables12[96];
       singularPressureLoss_SG_Out.C1.P = RealVariables12[97];
       singularPressureLoss_SG_Out.C1.h_vol = RealVariables12[98];
       singularPressureLoss_SG_Out.pro.u = RealVariables12[99];
       singularPressureLoss_SG_Out.pro.s = RealVariables12[100];
       singularPressureLoss_SG_Out.pro.cp = RealVariables13[1];
       singularPressureLoss_SG_Out.pro.ddhp = RealVariables13[2];
       singularPressureLoss_SG_Out.pro.ddph = RealVariables13[3];
       singularPressureLoss_SG_Out.pro.duph = RealVariables13[4];
       singularPressureLoss_SG_Out.pro.duhp = RealVariables13[5];
       singularPressureLoss_SG_Out.pro.x = RealVariables13[6];
       Pout_SG.Measure.signal = RealVariables13[7];
       Extraction_TurbHP_Oulet.x_ex = RealVariables13[8];
       Extraction_TurbHP_Oulet.proe.T = RealVariables13[9];
       Extraction_TurbHP_Oulet.proe.d = RealVariables13[10];
       Extraction_TurbHP_Oulet.proe.u = RealVariables13[11];
       Extraction_TurbHP_Oulet.proe.s = RealVariables13[12];
       Extraction_TurbHP_Oulet.proe.cp = RealVariables13[13];
       Extraction_TurbHP_Oulet.proe.ddhp = RealVariables13[14];
       Extraction_TurbHP_Oulet.proe.ddph = RealVariables13[15];
       Extraction_TurbHP_Oulet.proe.duph = RealVariables13[16];
       Extraction_TurbHP_Oulet.proe.duhp = RealVariables13[17];
       Extraction_TurbHP_Oulet.proe.x = RealVariables13[18];
       Extraction_TurbHP_Oulet.lsat.P = RealVariables13[19];
       Extraction_TurbHP_Oulet.lsat.T = RealVariables13[20];
       Extraction_TurbHP_Oulet.lsat.rho = RealVariables13[21];
       Extraction_TurbHP_Oulet.lsat.h = RealVariables13[22];
       Extraction_TurbHP_Oulet.lsat.cp = RealVariables13[23];
       Extraction_TurbHP_Oulet.lsat.pt = RealVariables13[24];
       Extraction_TurbHP_Oulet.lsat.cv = RealVariables13[25];
       Extraction_TurbHP_Oulet.vsat.P = RealVariables13[26];
       Extraction_TurbHP_Oulet.vsat.T = RealVariables13[27];
       Extraction_TurbHP_Oulet.vsat.rho = RealVariables13[28];
       Extraction_TurbHP_Oulet.vsat.h = RealVariables13[29];
       Extraction_TurbHP_Oulet.vsat.cp = RealVariables13[30];
       Extraction_TurbHP_Oulet.vsat.pt = RealVariables13[31];
       Extraction_TurbHP_Oulet.vsat.cv = RealVariables13[32];
       Extraction_TurbHP_Oulet.Cex.h_vol = RealVariables13[33];
       Extraction_TurbHP_Oulet.Cex.h = RealVariables13[34];
       Ideal_BOP2HeatNetWork_HX.W = RealVariables13[35];
       Ideal_BOP2HeatNetWork_HX.Tec = RealVariables13[36];
       Ideal_BOP2HeatNetWork_HX.Tsc = RealVariables13[37];
       Ideal_BOP2HeatNetWork_HX.Tef = RealVariables13[38];
       Ideal_BOP2HeatNetWork_HX.Tsf = RealVariables13[39];
       Ideal_BOP2HeatNetWork_HX.DPfc = RealVariables13[40];
       Ideal_BOP2HeatNetWork_HX.DPgc = RealVariables13[41];
       Ideal_BOP2HeatNetWork_HX.DPc = RealVariables13[42];
       Ideal_BOP2HeatNetWork_HX.DPff = RealVariables13[43];
       Ideal_BOP2HeatNetWork_HX.DPgf = RealVariables13[44];
       Ideal_BOP2HeatNetWork_HX.DPf = RealVariables13[45];
       Ideal_BOP2HeatNetWork_HX.rhoc = RealVariables13[46];
       Ideal_BOP2HeatNetWork_HX.rhof = RealVariables13[47];
       Ideal_BOP2HeatNetWork_HX.Qf = RealVariables13[48];
       Ideal_BOP2HeatNetWork_HX.Ef.P = RealVariables13[49];
       Ideal_BOP2HeatNetWork_HX.Ef.h_vol = RealVariables13[50];
       Ideal_BOP2HeatNetWork_HX.Ef.h = RealVariables13[51];
       Ideal_BOP2HeatNetWork_HX.Sf.h_vol = RealVariables13[52];
       Ideal_BOP2HeatNetWork_HX.Sf.h = RealVariables13[53];
       Ideal_BOP2HeatNetWork_HX.Sc.P = RealVariables13[54];
       Ideal_BOP2HeatNetWork_HX.Sc.h_vol = RealVariables13[55];
       Ideal_BOP2HeatNetWork_HX.proce.d = RealVariables13[56];
       Ideal_BOP2HeatNetWork_HX.proce.u = RealVariables13[57];
       Ideal_BOP2HeatNetWork_HX.proce.s = RealVariables13[58];
       Ideal_BOP2HeatNetWork_HX.proce.cp = RealVariables13[59];
       Ideal_BOP2HeatNetWork_HX.proce.ddhp = RealVariables13[60];
       Ideal_BOP2HeatNetWork_HX.proce.ddph = RealVariables13[61];
       Ideal_BOP2HeatNetWork_HX.proce.duph = RealVariables13[62];
       Ideal_BOP2HeatNetWork_HX.proce.duhp = RealVariables13[63];
       Ideal_BOP2HeatNetWork_HX.proce.x = RealVariables13[64];
       Ideal_BOP2HeatNetWork_HX.procs.d = RealVariables13[65];
       Ideal_BOP2HeatNetWork_HX.procs.u = RealVariables13[66];
       Ideal_BOP2HeatNetWork_HX.procs.s = RealVariables13[67];
       Ideal_BOP2HeatNetWork_HX.procs.cp = RealVariables13[68];
       Ideal_BOP2HeatNetWork_HX.procs.ddhp = RealVariables13[69];
       Ideal_BOP2HeatNetWork_HX.procs.ddph = RealVariables13[70];
       Ideal_BOP2HeatNetWork_HX.procs.duph = RealVariables13[71];
       Ideal_BOP2HeatNetWork_HX.procs.duhp = RealVariables13[72];
       Ideal_BOP2HeatNetWork_HX.procs.x = RealVariables13[73];
       Ideal_BOP2HeatNetWork_HX.profe.d = RealVariables13[74];
       Ideal_BOP2HeatNetWork_HX.profe.u = RealVariables13[75];
       Ideal_BOP2HeatNetWork_HX.profe.s = RealVariables13[76];
       Ideal_BOP2HeatNetWork_HX.profe.cp = RealVariables13[77];
       Ideal_BOP2HeatNetWork_HX.profe.ddhp = RealVariables13[78];
       Ideal_BOP2HeatNetWork_HX.profe.ddph = RealVariables13[79];
       Ideal_BOP2HeatNetWork_HX.profe.duph = RealVariables13[80];
       Ideal_BOP2HeatNetWork_HX.profe.duhp = RealVariables13[81];
       Ideal_BOP2HeatNetWork_HX.profe.x = RealVariables13[82];
       Ideal_BOP2HeatNetWork_HX.promf.T = RealVariables13[83];
       Ideal_BOP2HeatNetWork_HX.promf.u = RealVariables13[84];
       Ideal_BOP2HeatNetWork_HX.promf.s = RealVariables13[85];
       Ideal_BOP2HeatNetWork_HX.promf.cp = RealVariables13[86];
       Ideal_BOP2HeatNetWork_HX.promf.ddhp = RealVariables13[87];
       Ideal_BOP2HeatNetWork_HX.promf.ddph = RealVariables13[88];
       Ideal_BOP2HeatNetWork_HX.promf.duph = RealVariables13[89];
       Ideal_BOP2HeatNetWork_HX.promf.duhp = RealVariables13[90];
       Ideal_BOP2HeatNetWork_HX.promf.x = RealVariables13[91];
       Ideal_BOP2HeatNetWork_HX.lsat.P = RealVariables13[92];
       Ideal_BOP2HeatNetWork_HX.lsat.T = RealVariables13[93];
       Ideal_BOP2HeatNetWork_HX.lsat.rho = RealVariables13[94];
       Ideal_BOP2HeatNetWork_HX.lsat.cp = RealVariables13[95];
       Ideal_BOP2HeatNetWork_HX.lsat.pt = RealVariables13[96];
       Ideal_BOP2HeatNetWork_HX.lsat.cv = RealVariables13[97];
       Ideal_BOP2HeatNetWork_HX.vsat.P = RealVariables13[98];
       Ideal_BOP2HeatNetWork_HX.vsat.T = RealVariables13[99];
       Ideal_BOP2HeatNetWork_HX.vsat.rho = RealVariables13[100];
       Ideal_BOP2HeatNetWork_HX.vsat.h = RealVariables14[1];
       Ideal_BOP2HeatNetWork_HX.vsat.cp = RealVariables14[2];
       Ideal_BOP2HeatNetWork_HX.vsat.pt = RealVariables14[3];
       Ideal_BOP2HeatNetWork_HX.vsat.cv = RealVariables14[4];
       Ideal_BOP2HeatNetWork_HX.promc.T = RealVariables14[5];
       Ideal_BOP2HeatNetWork_HX.promc.u = RealVariables14[6];
       Ideal_BOP2HeatNetWork_HX.promc.s = RealVariables14[7];
       Ideal_BOP2HeatNetWork_HX.promc.cp = RealVariables14[8];
       Ideal_BOP2HeatNetWork_HX.promc.ddhp = RealVariables14[9];
       Ideal_BOP2HeatNetWork_HX.promc.ddph = RealVariables14[10];
       Ideal_BOP2HeatNetWork_HX.promc.duph = RealVariables14[11];
       Ideal_BOP2HeatNetWork_HX.promc.duhp = RealVariables14[12];
       Ideal_BOP2HeatNetWork_HX.promc.x = RealVariables14[13];
       Ideal_BOP2HeatNetWork_HX.profs.d = RealVariables14[14];
       Ideal_BOP2HeatNetWork_HX.profs.u = RealVariables14[15];
       Ideal_BOP2HeatNetWork_HX.profs.s = RealVariables14[16];
       Ideal_BOP2HeatNetWork_HX.profs.cp = RealVariables14[17];
       Ideal_BOP2HeatNetWork_HX.profs.ddhp = RealVariables14[18];
       Ideal_BOP2HeatNetWork_HX.profs.ddph = RealVariables14[19];
       Ideal_BOP2HeatNetWork_HX.profs.duph = RealVariables14[20];
       Ideal_BOP2HeatNetWork_HX.profs.duhp = RealVariables14[21];
       Ideal_BOP2HeatNetWork_HX.profs.x = RealVariables14[22];
       Valve_Hybrid_IP.Cv = RealVariables14[23];
       Valve_Hybrid_IP.deltaP = RealVariables14[24];
       Valve_Hybrid_IP.rho = RealVariables14[25];
       Valve_Hybrid_IP.T = RealVariables14[26];
       Valve_Hybrid_IP.Pm = RealVariables14[27];
       Valve_Hybrid_IP.pro.d = RealVariables14[28];
       Valve_Hybrid_IP.pro.u = RealVariables14[29];
       Valve_Hybrid_IP.pro.s = RealVariables14[30];
       Valve_Hybrid_IP.pro.cp = RealVariables14[31];
       Valve_Hybrid_IP.pro.ddhp = RealVariables14[32];
       Valve_Hybrid_IP.pro.ddph = RealVariables14[33];
       Valve_Hybrid_IP.pro.duph = RealVariables14[34];
       Valve_Hybrid_IP.pro.duhp = RealVariables14[35];
       Valve_Hybrid_IP.pro.x = RealVariables14[36];
       Valve_Hybrid_IP.Ouv.signal = RealVariables14[37];
       Valve_Hybrid_IP.C1.h_vol = RealVariables14[38];
       Valve_Hybrid_IP.C2.h_vol = RealVariables14[39];
       feedback6.u2.signal = RealVariables14[40];
       feedback6.y.signal = RealVariables14[41];
       feedback6.u1.signal = RealVariables14[42];
       pI6.x = RealVariables14[43];
       T_Outlet_BOP_HX_HeatNetWork.T = RealVariables14[44];
       T_Outlet_BOP_HX_HeatNetWork.pro.d = RealVariables14[45];
       T_Outlet_BOP_HX_HeatNetWork.pro.u = RealVariables14[46];
       T_Outlet_BOP_HX_HeatNetWork.pro.s = RealVariables14[47];
       T_Outlet_BOP_HX_HeatNetWork.pro.cp = RealVariables14[48];
       T_Outlet_BOP_HX_HeatNetWork.pro.ddhp = RealVariables14[49];
       T_Outlet_BOP_HX_HeatNetWork.pro.ddph = RealVariables14[50];
       T_Outlet_BOP_HX_HeatNetWork.pro.duph = RealVariables14[51];
       T_Outlet_BOP_HX_HeatNetWork.pro.duhp = RealVariables14[52];
       T_Outlet_BOP_HX_HeatNetWork.pro.x = RealVariables14[53];
       T_Outlet_BOP_HX_HeatNetWork.Measure.signal = RealVariables14[54];
       Pout_Turb_HP.Measure.signal = RealVariables14[55];
       Pout_Turb_HP.C1.h_vol = RealVariables14[56];
       Tout_Turb_HP.T = RealVariables14[57];
       Tout_Turb_HP.pro.d = RealVariables14[58];
       Tout_Turb_HP.pro.u = RealVariables14[59];
       Tout_Turb_HP.pro.s = RealVariables14[60];
       Tout_Turb_HP.pro.cp = RealVariables14[61];
       Tout_Turb_HP.pro.ddhp = RealVariables14[62];
       Tout_Turb_HP.pro.ddph = RealVariables14[63];
       Tout_Turb_HP.pro.duph = RealVariables14[64];
       Tout_Turb_HP.pro.duhp = RealVariables14[65];
       Tout_Turb_HP.pro.x = RealVariables14[66];
       Tout_Turb_HP.Measure.signal = RealVariables14[67];
       Valve_superheat1.Cv = RealVariables14[68];
       Valve_superheat1.deltaP = RealVariables14[69];
       Valve_superheat1.rho = RealVariables14[70];
       Valve_superheat1.T = RealVariables14[71];
       Valve_superheat1.Pm = RealVariables14[72];
       Valve_superheat1.pro.d = RealVariables14[73];
       Valve_superheat1.pro.u = RealVariables14[74];
       Valve_superheat1.pro.s = RealVariables14[75];
       Valve_superheat1.pro.cp = RealVariables14[76];
       Valve_superheat1.pro.ddhp = RealVariables14[77];
       Valve_superheat1.pro.ddph = RealVariables14[78];
       Valve_superheat1.pro.duph = RealVariables14[79];
       Valve_superheat1.pro.duhp = RealVariables14[80];
       Valve_superheat1.pro.x = RealVariables14[81];
       Valve_superheat1.Ouv.signal = RealVariables14[82];
       sinkP.IPressure.signal = RealVariables14[83];
       sinkP.ISpecificEnthalpy.signal = RealVariables14[84];
       sinkP.ITemperature.signal = RealVariables14[85];
       Valve_superheat2.Cv = RealVariables14[86];
       Valve_superheat2.deltaP = RealVariables14[87];
       Valve_superheat2.rho = RealVariables14[88];
       Valve_superheat2.T = RealVariables14[89];
       Valve_superheat2.Pm = RealVariables14[90];
       Valve_superheat2.pro.d = RealVariables14[91];
       Valve_superheat2.pro.u = RealVariables14[92];
       Valve_superheat2.pro.s = RealVariables14[93];
       Valve_superheat2.pro.cp = RealVariables14[94];
       Valve_superheat2.pro.ddhp = RealVariables14[95];
       Valve_superheat2.pro.ddph = RealVariables14[96];
       Valve_superheat2.pro.duph = RealVariables14[97];
       Valve_superheat2.pro.duhp = RealVariables14[98];
       Valve_superheat2.pro.x = RealVariables14[99];
       Valve_superheat2.Ouv.signal = RealVariables14[100];
       sinkP2.IPressure.signal = RealVariables15[1];
       sinkP2.ISpecificEnthalpy.signal = RealVariables15[2];
       sinkP2.ITemperature.signal = RealVariables15[3];
       Qin_set.u = RealVariables15[4];
       Hin_set.u = RealVariables15[5];
       sinkP_4FMU.T = RealVariables15[6];
       sinkP_4FMU.pro.d = RealVariables15[7];
       sinkP_4FMU.pro.u = RealVariables15[8];
       sinkP_4FMU.pro.s = RealVariables15[9];
       sinkP_4FMU.pro.cp = RealVariables15[10];
       sinkP_4FMU.pro.ddhp = RealVariables15[11];
       sinkP_4FMU.pro.ddph = RealVariables15[12];
       sinkP_4FMU.pro.duph = RealVariables15[13];
       sinkP_4FMU.pro.duhp = RealVariables15[14];
       sinkP_4FMU.pro.x = RealVariables15[15];
       sinkP_4FMU.IPressure.signal = RealVariables15[16];
       sinkP_4FMU.ISpecificEnthalpy.signal = RealVariables15[17];
       T_2HeatNetWork.T = RealVariables15[18];
       T_2HeatNetWork.pro.d = RealVariables15[19];
       T_2HeatNetWork.pro.u = RealVariables15[20];
       T_2HeatNetWork.pro.s = RealVariables15[21];
       T_2HeatNetWork.pro.cp = RealVariables15[22];
       T_2HeatNetWork.pro.ddhp = RealVariables15[23];
       T_2HeatNetWork.pro.ddph = RealVariables15[24];
       T_2HeatNetWork.pro.duph = RealVariables15[25];
       T_2HeatNetWork.pro.duhp = RealVariables15[26];
       T_2HeatNetWork.pro.x = RealVariables15[27];
       T_2HeatNetWork.Measure.signal = RealVariables15[28];
       T_2HeatNetWork.C2.h_vol = RealVariables15[29];
       InletFlow_FromHeatNetwork.medium.d = RealVariables15[30];
       InletFlow_FromHeatNetwork.medium.T = RealVariables15[31];
       InletFlow_FromHeatNetwork.medium.u = RealVariables15[32];
       InletFlow_FromHeatNetwork.medium.T_degC = RealVariables15[33];
       InletFlow_FromHeatNetwork.medium.p_bar = RealVariables15[34];
       InletFlow_FromHeatNetwork.medium.sat.Tsat = RealVariables15[35];
       InletFlow_FromHeatNetwork.'ports[1]'.m_flow = RealVariables15[36];
       InletFlowRate_FromHeatNetWork.y = RealVariables15[37];
       kfric_Surch = RealDependentParameters[1];
       kfric_SortieGV = RealDependentParameters[2];
       CsHP = RealFixedLocal[1];
       CsBP1a = RealFixedLocal[2];
       CsBP1b = RealFixedLocal[3];
       CsBP2 = RealFixedLocal[4];
       ScondesHP = RealFixedLocal[5];
       SPurgeHP = RealFixedLocal[6];
       ScondesBP = RealFixedLocal[7];
       SPurgeBP = RealFixedLocal[8];
       Valve_HP.'caract[2,2]' = RealFixedLocal[9];
       sourceQ1.Q = RealFixedLocal[10];
       sourceQ1.h = RealFixedLocal[11];
       sinkP1.P = RealFixedLocal[12];
       sinkP1.T = RealFixedLocal[13];
       sinkP1.h = RealFixedLocal[14];
       sinkP1.pro.T = RealFixedLocal[15];
       sinkP1.pro.d = RealFixedLocal[16];
       sinkP1.pro.u = RealFixedLocal[17];
       sinkP1.pro.s = RealFixedLocal[18];
       sinkP1.pro.cp = RealFixedLocal[19];
       sinkP1.pro.ddhp = RealFixedLocal[20];
       sinkP1.pro.ddph = RealFixedLocal[21];
       sinkP1.pro.duph = RealFixedLocal[22];
       sinkP1.pro.duhp = RealFixedLocal[23];
       sinkP1.pro.x = RealFixedLocal[24];
       BOP_HeatSink.Avl = RealFixedLocal[25];
       BOP_HeatSink.At = RealFixedLocal[26];
       BOP_HeatSink.khi = RealFixedLocal[27];
       Turb_HP.Cst = RealFixedLocal[28];
       Turb_LP1_a.Cst = RealFixedLocal[29];
       lumpedStraightPipe.khi = RealFixedLocal[30];
       PressureSet_TurbHP_In.y.signal = RealFixedLocal[31];
       pI1.ureset.signal = RealFixedLocal[32];
       Turb_LP2.Cst = RealFixedLocal[33];
       lumpedStraightPipe1.khi = RealFixedLocal[34];
       SuperHeat.Kf = RealFixedLocal[35];
       Turb_LP1_b.Cst = RealFixedLocal[36];
       Reheat_BP.SCondDes = RealFixedLocal[37];
       Reheat_BP.SPurge = RealFixedLocal[38];
       ReHeat_HP.SCondDes = RealFixedLocal[39];
       ReHeat_HP.SPurge = RealFixedLocal[40];
       ReHeat_HP.Se.h_vol = RealFixedLocal[41];
       Valve_superheat.Cvmax = RealFixedLocal[42];
       Valve_superheat.'caract[2,2]' = RealFixedLocal[43];
       pI5.ureset.signal = RealFixedLocal[44];
       Pipe_SGs.khi = RealFixedLocal[45];
       singularPressureLoss_SG_Out.K = RealFixedLocal[46];
       Ideal_BOP2HeatNetWork_HX.Sf.P = RealFixedLocal[47];
       Valve_Hybrid_IP.'caract[2,2]' = RealFixedLocal[48];
       pI6.ureset.signal = RealFixedLocal[49];
       Valve_superheat1.Cvmax = RealFixedLocal[50];
       Valve_superheat1.'caract[2,2]' = RealFixedLocal[51];
       Valve_superheat1.C2.P = RealFixedLocal[52];
       Valve_superheat1.C2.h_vol = RealFixedLocal[53];
       sinkP.T = RealFixedLocal[54];
       sinkP.pro.T = RealFixedLocal[55];
       sinkP.pro.d = RealFixedLocal[56];
       sinkP.pro.u = RealFixedLocal[57];
       sinkP.pro.s = RealFixedLocal[58];
       sinkP.pro.cp = RealFixedLocal[59];
       sinkP.pro.ddhp = RealFixedLocal[60];
       sinkP.pro.ddph = RealFixedLocal[61];
       sinkP.pro.duph = RealFixedLocal[62];
       sinkP.pro.duhp = RealFixedLocal[63];
       sinkP.pro.x = RealFixedLocal[64];
       Valve_superheat2.Cvmax = RealFixedLocal[65];
       Valve_superheat2.'caract[2,2]' = RealFixedLocal[66];
       Valve_superheat2.C2.P = RealFixedLocal[67];
       Valve_superheat2.C2.h_vol = RealFixedLocal[68];
       sinkP2.T = RealFixedLocal[69];
       sinkP2.pro.T = RealFixedLocal[70];
       sinkP2.pro.d = RealFixedLocal[71];
       sinkP2.pro.u = RealFixedLocal[72];
       sinkP2.pro.s = RealFixedLocal[73];
       sinkP2.pro.cp = RealFixedLocal[74];
       sinkP2.pro.ddhp = RealFixedLocal[75];
       sinkP2.pro.ddph = RealFixedLocal[76];
       sinkP2.pro.duph = RealFixedLocal[77];
       sinkP2.pro.duhp = RealFixedLocal[78];
       sinkP2.pro.x = RealFixedLocal[79];
       InletFlow_FromHeatNetwork.medium.h = RealFixedLocal[80];
       InletFlow_FromHeatNetwork.'ports[1]'.h_outflow = RealFixedLocal[81];
       BackPressure_FromHeatNetWork.medium.h = RealFixedLocal[82];
       BackPressure_FromHeatNetWork.medium.d = RealFixedLocal[83];
       BackPressure_FromHeatNetWork.medium.T = RealFixedLocal[84];
       BackPressure_FromHeatNetWork.medium.u = RealFixedLocal[85];
       BackPressure_FromHeatNetWork.medium.T_degC = RealFixedLocal[86];
       BackPressure_FromHeatNetWork.medium.p_bar = RealFixedLocal[87];
       BackPressure_FromHeatNetWork.medium.sat.Tsat = RealFixedLocal[88];
       InletFlow_FromHeatNetwork.medium.phase = IntegerVariables[1];
       BackPressure_FromHeatNetWork.medium.phase = IntegerFixedLocal[1];
     //alias Declarations
       sinkP1.C.h_vol = sinkP1.h;
       BOP_HeatSink.Cse.h_vol = sinkP1.h;
       sinkP.h = Valve_superheat1.C2.h_vol;
       sinkP.C.h_vol = Valve_superheat1.C2.h_vol;
       sinkP2.h = Valve_superheat2.C2.h_vol;
       sinkP2.C.h_vol = Valve_superheat2.C2.h_vol;
       BackPressure_FromHeatNetWork.medium.state.d = BackPressure_FromHeatNetWork.medium.d;
       BackPressure_FromHeatNetWork.medium.state.T = BackPressure_FromHeatNetWork.medium.T;
       sourceQ1.C.Q = sourceQ1.Q;
       sinkP1.Q = sourceQ1.Q;
       sinkP1.C.Q = sourceQ1.Q;
       BOP_HeatSink.Cee.Q = sourceQ1.Q;
       BOP_HeatSink.Cse.Q = sourceQ1.Q;
       sourceQ1.C.h_vol = sourceQ1.h;
       BOP_HeatSink.Cee.h_vol = sourceQ1.h;
       sinkP1.C.P = sinkP1.P;
       BOP_HeatSink.Cse.P = sinkP1.P;
       feedback1.u2.signal = PressureSet_TurbHP_In.y.signal;
       sinkP.P = Valve_superheat1.C2.P;
       sinkP.C.P = Valve_superheat1.C2.P;
       sinkP2.P = Valve_superheat2.C2.P;
       sinkP2.C.P = Valve_superheat2.C2.P;
       sinkP_4FMU.h = ReHeat_HP.Se.h_vol;
       sinkP_4FMU.C.h_vol = ReHeat_HP.Se.h_vol;
       InletFlow_FromHeatNetwork.medium.state.h = InletFlow_FromHeatNetwork.medium.h;
       T_2HeatNetWork.P = Ideal_BOP2HeatNetWork_HX.Sf.P;
       T_2HeatNetWork.C1.P = Ideal_BOP2HeatNetWork_HX.Sf.P;
       T_2HeatNetWork.C2.P = Ideal_BOP2HeatNetWork_HX.Sf.P;
       BackPressure_FromHeatNetWork.medium.p = Ideal_BOP2HeatNetWork_HX.Sf.P;
       BackPressure_FromHeatNetWork.medium.state.p = Ideal_BOP2HeatNetWork_HX.Sf.P;
       BackPressure_FromHeatNetWork.medium.sat.psat = Ideal_BOP2HeatNetWork_HX.Sf.P;
       BackPressure_FromHeatNetWork.'ports[1]'.p = Ideal_BOP2HeatNetWork_HX.Sf.P;
       fluid2TSPro.steam_inlet.P = Ideal_BOP2HeatNetWork_HX.Sf.P;
       fluid2TSPro.port_b.p = Ideal_BOP2HeatNetWork_HX.Sf.P;
       BackPressure_FromHeatNetWork.medium.state.h = BackPressure_FromHeatNetWork.medium.h;
       BackPressure_FromHeatNetWork.'ports[1]'.h_outflow = BackPressure_FromHeatNetWork.medium.h;
       BOP_HeatSink.Cv.P = BOP_HeatSink.P;
       singularPressureLoss1.C2.P = BOP_HeatSink.P;
       sinkP_4FMU.OFlowRate.signal = Qout_TSPro;
       Qout_calc.y = Qout_TSPro;
       Qout_calc.inputReal.signal = Qout_TSPro;
       sinkP_4FMU.OSpecificEnthalpy.signal = Hout_TSPro;
       Hout_calc.y = Hout_TSPro;
       Hout_calc.inputReal.signal = Hout_TSPro;
       BOP_HeatSink.Cl.h_vol = BOP_HeatSink.hl;
       singularPressureLoss5.C1.h_vol = BOP_HeatSink.hl;
       BOP_HeatSink.Cv.h_vol = BOP_HeatSink.hv;
       singularPressureLoss1.C2.h_vol = BOP_HeatSink.hv;
       Pump_BP.C1.h_vol = volumeC1.h;
       volumeC1.Ce1.h_vol = volumeC1.h;
       volumeC1.Ce2.h_vol = volumeC1.h;
       volumeC1.Cs.h_vol = volumeC1.h;
       volumeC1.Ce3.h_vol = volumeC1.h;
       singularPressureLoss5.C2.h_vol = volumeC1.h;
       sensorP1.C2.h_vol = Bache_a.h;
       invSingularPressureLoss.C2.h_vol = Bache_a.h;
       Bache_a.Ce2.h_vol = Bache_a.h;
       Bache_a.Ce3.h_vol = Bache_a.h;
       Bache_a.Ce4.h_vol = Bache_a.h;
       Bache_a.Ce1.h_vol = Bache_a.h;
       Bache_a.Cs4.h_vol = Bache_a.h;
       Bache_a.Cs1.h_vol = Bache_a.h;
       Bache_a.Cs2.h_vol = Bache_a.h;
       Bache_a.Cs3.h_vol = Bache_a.h;
       singularPressureLoss6.C1.h_vol = Bache_a.h;
       invSingularPressureLoss2.C2.h_vol = Bache_a.h;
       Valve_superheat.C2.h_vol = Bache_a.h;
       Pump_HP.C1.h_vol = Bache_b.h;
       singularPressureLoss6.C2.h_vol = Bache_b.h;
       Bache_b.Ce1.h_vol = Bache_b.h;
       Bache_b.Ce2.h_vol = Bache_b.h;
       Bache_b.Cs.h_vol = Bache_b.h;
       Bache_b.Ce3.h_vol = Bache_b.h;
       Q_Extract_Hybrid_IP.C2.h_vol = Bache_b.h;
       Turb_LP2.Cs.h_vol = volumeC2.h;
       invSingularPressureLoss1.C2.h_vol = volumeC2.h;
       volumeC2.Ce1.h_vol = volumeC2.h;
       volumeC2.Ce2.h_vol = volumeC2.h;
       volumeC2.Cs.h_vol = volumeC2.h;
       volumeC2.Ce3.h_vol = volumeC2.h;
       singularPressureLoss1.C1.h_vol = volumeC2.h;
       Valve_HP.C1.Q = Valve_HP.Q;
       Valve_HP.C2.Q = Valve_HP.Q;
       Turb_HP.Q = Valve_HP.Q;
       Turb_HP.Ce.Q = Valve_HP.Q;
       Turb_HP.Cs.Q = Valve_HP.Q;
       Pe_ValveHP.Q = Valve_HP.Q;
       Pe_ValveHP.C1.Q = Valve_HP.Q;
       Pe_ValveHP.C2.Q = Valve_HP.Q;
       singularPressureLoss3.Q = Valve_HP.Q;
       singularPressureLoss3.C1.Q = Valve_HP.Q;
       singularPressureLoss3.C2.Q = Valve_HP.Q;
       Extraction_ReheatHP.Ce.Q = Valve_HP.Q;
       splitter_SGoutlet.Cs3.Q = Valve_HP.Q;
       Valve_HP.C1.h = Valve_HP.h;
       Valve_HP.C2.h_vol = Valve_HP.h;
       Valve_HP.C2.h = Valve_HP.h;
       Turb_HP.Ce.h_vol = Valve_HP.h;
       Turb_HP.Ce.h = Valve_HP.h;
       Pe_ValveHP.C1.h = Valve_HP.h;
       Pe_ValveHP.C2.h = Valve_HP.h;
       splitter_SGoutlet.Cs3.h = Valve_HP.h;
       Valve_HP.pro.T = Valve_HP.T;
       pI1.y.signal = Valve_HP.Ouv.signal;
       Pe_ValveHP.C1.P = Valve_HP.C1.P;
       Pe_ValveHP.C2.P = Valve_HP.C1.P;
       SuperHeat.Ec.P = Valve_HP.C1.P;
       splitter_SGoutlet.P = Valve_HP.C1.P;
       splitter_SGoutlet.Ce.P = Valve_HP.C1.P;
       splitter_SGoutlet.Cs3.P = Valve_HP.C1.P;
       splitter_SGoutlet.Cs1.P = Valve_HP.C1.P;
       splitter_SGoutlet.Cs2.P = Valve_HP.C1.P;
       Pipe_SGs.C2.P = Valve_HP.C1.P;
       Valve_superheat1.C1.P = Valve_HP.C1.P;
       Pe_ValveHP.C2.h_vol = Valve_HP.C1.h_vol;
       singularPressureLoss6.C2.P = Pump_HP.C1.P;
       Bache_b.P = Pump_HP.C1.P;
       Bache_b.Ce1.P = Pump_HP.C1.P;
       Bache_b.Ce2.P = Pump_HP.C1.P;
       Bache_b.Cs.P = Pump_HP.C1.P;
       Bache_b.Ce3.P = Pump_HP.C1.P;
       Valve_Hybrid_IP.C2.P = Pump_HP.C1.P;
       Q_Extract_Hybrid_IP.C1.P = Pump_HP.C1.P;
       Q_Extract_Hybrid_IP.C2.P = Pump_HP.C1.P;
       Bache_b.Cs.h = Pump_HP.C1.h;
       ReHeat_HP.Ee.P = Pump_HP.C2.P;
       ReHeat_HP.Ee.h_vol = Pump_HP.C2.h_vol;
       ReHeat_HP.Ee.h = Pump_HP.C2.h;
       Pump_HP.pro.d = Pump_HP.rho;
       Test_Timetable_Rotation_PumpHP.y.signal = Pump_HP.rpm_or_mpower.signal;
       BOP_HeatSink.Cl.P = BOP_HeatSink.Pfond;
       singularPressureLoss5.C1.P = BOP_HeatSink.Pfond;
       volumeC2.Cs.Q = BOP_HeatSink.Cv.Q;
       singularPressureLoss1.Q = BOP_HeatSink.Cv.Q;
       singularPressureLoss1.C1.Q = BOP_HeatSink.Cv.Q;
       singularPressureLoss1.C2.Q = BOP_HeatSink.Cv.Q;
       volumeC2.Cs.h = BOP_HeatSink.Cv.h;
       singularPressureLoss1.h = BOP_HeatSink.Cv.h;
       singularPressureLoss1.C1.h = BOP_HeatSink.Cv.h;
       singularPressureLoss1.C2.h = BOP_HeatSink.Cv.h;
       Pump_BP.Q = BOP_HeatSink.Cl.Q;
       Pump_BP.C1.Q = BOP_HeatSink.Cl.Q;
       Pump_BP.C2.Q = BOP_HeatSink.Cl.Q;
       sensorP1.Q = BOP_HeatSink.Cl.Q;
       sensorP1.C1.Q = BOP_HeatSink.Cl.Q;
       sensorP1.C2.Q = BOP_HeatSink.Cl.Q;
       volumeC1.Ce1.Q = BOP_HeatSink.Cl.Q;
       volumeC1.Cs.Q = BOP_HeatSink.Cl.Q;
       singularPressureLoss5.Q = BOP_HeatSink.Cl.Q;
       singularPressureLoss5.C1.Q = BOP_HeatSink.Cl.Q;
       singularPressureLoss5.C2.Q = BOP_HeatSink.Cl.Q;
       Bache_a.Ce4.Q = BOP_HeatSink.Cl.Q;
       Reheat_BP.Ee.Q = BOP_HeatSink.Cl.Q;
       Reheat_BP.Se.Q = BOP_HeatSink.Cl.Q;
       volumeC1.Ce1.h = BOP_HeatSink.Cl.h;
       singularPressureLoss5.h = BOP_HeatSink.Cl.h;
       singularPressureLoss5.C1.h = BOP_HeatSink.Cl.h;
       singularPressureLoss5.C2.h = BOP_HeatSink.Cl.h;
       Valve_HP.C2.P = Turb_HP.Pe;
       Turb_HP.Ce.P = Turb_HP.Pe;
       Turb_HP.Cs.P = Turb_HP.Ps;
       singularPressureLoss3.C1.P = Turb_HP.Ps;
       Turb_HP.proe.T = Turb_HP.Te;
       Turb_HP.pros.T = Turb_HP.Ts;
       singularPressureLoss3.h = Turb_HP.Cs.h;
       singularPressureLoss3.C1.h = Turb_HP.Cs.h;
       singularPressureLoss3.C2.h = Turb_HP.Cs.h;
       Extraction_ReheatHP.Ce.h = Turb_HP.Cs.h;
       Turb_HP.props.h = Turb_HP.His;
       Turb_LP1_a.Ce.Q = Turb_LP1_a.Q;
       Turb_LP1_a.Cs.Q = Turb_LP1_a.Q;
       Dryer.Csv.Q = Turb_LP1_a.Q;
       SuperHeat.Qf = Turb_LP1_a.Q;
       SuperHeat.Ef.Q = Turb_LP1_a.Q;
       SuperHeat.Sf.Q = Turb_LP1_a.Q;
       Extraction_TurbBP1a_Outlet.Ce.Q = Turb_LP1_a.Q;
       Tin_Turb_LP1a.Q = Turb_LP1_a.Q;
       Tin_Turb_LP1a.C1.Q = Turb_LP1_a.Q;
       Tin_Turb_LP1a.C2.Q = Turb_LP1_a.Q;
       Turb_LP1_a.Ce.P = Turb_LP1_a.Pe;
       SuperHeat.Sf.P = Turb_LP1_a.Pe;
       Tin_Turb_LP1a.P = Turb_LP1_a.Pe;
       Tin_Turb_LP1a.C1.P = Turb_LP1_a.Pe;
       Tin_Turb_LP1a.C2.P = Turb_LP1_a.Pe;
       Turb_LP1_a.proe.T = Turb_LP1_a.Te;
       Turb_LP1_a.pros.T = Turb_LP1_a.Ts;
       Turb_LP1_a.Ce.h = Turb_LP1_a.Ce.h_vol;
       SuperHeat.Sf.h = Turb_LP1_a.Ce.h_vol;
       Tin_Turb_LP1a.h = Turb_LP1_a.Ce.h_vol;
       Tin_Turb_LP1a.C1.h = Turb_LP1_a.Ce.h_vol;
       Tin_Turb_LP1a.C2.h_vol = Turb_LP1_a.Ce.h_vol;
       Tin_Turb_LP1a.C2.h = Turb_LP1_a.Ce.h_vol;
       Turb_LP1_b.Ce.h_vol = Turb_LP1_a.Cs.h_vol;
       Turb_LP1_b.Ce.h = Turb_LP1_a.Cs.h_vol;
       Extraction_TurbBP1a_Outlet.h = Turb_LP1_a.Cs.h_vol;
       Extraction_TurbBP1a_Outlet.Ce.h_vol = Turb_LP1_a.Cs.h_vol;
       Extraction_TurbBP1a_Outlet.Cs.h_vol = Turb_LP1_a.Cs.h_vol;
       Extraction_TurbBP1a_Outlet.Cs.h = Turb_LP1_a.Cs.h_vol;
       Extraction_TurbBP1a_Outlet.Ce.h = Turb_LP1_a.Cs.h;
       Turb_LP1_a.props.h = Turb_LP1_a.His;
       Tout_SG.C1.h = Tout_SG.h;
       Tout_SG.C2.h = Tout_SG.h;
       splitter_SGoutlet.Ce.h = Tout_SG.h;
       Pipe_SGs.h = Tout_SG.h;
       Pipe_SGs.C1.h = Tout_SG.h;
       Pipe_SGs.C2.h = Tout_SG.h;
       singularPressureLoss_SG_Out.h = Tout_SG.h;
       singularPressureLoss_SG_Out.C1.h = Tout_SG.h;
       singularPressureLoss_SG_Out.C2.h = Tout_SG.h;
       Pout_SG.C1.h = Tout_SG.h;
       Pout_SG.C2.h = Tout_SG.h;
       sourceQ_4FMU.C.h = Tout_SG.h;
       Pout_SG.C1.h_vol = Tout_SG.C2.h_vol;
       feedback1.u1.signal = Pe_ValveHP.Measure.signal;
       SuperHeat.Ec.h_vol = Pe_ValveHP.C1.h_vol;
       splitter_SGoutlet.h = Pe_ValveHP.C1.h_vol;
       splitter_SGoutlet.Ce.h_vol = Pe_ValveHP.C1.h_vol;
       splitter_SGoutlet.Cs3.h_vol = Pe_ValveHP.C1.h_vol;
       splitter_SGoutlet.Cs1.h_vol = Pe_ValveHP.C1.h_vol;
       splitter_SGoutlet.Cs2.h_vol = Pe_ValveHP.C1.h_vol;
       Pipe_SGs.C2.h_vol = Pe_ValveHP.C1.h_vol;
       Valve_superheat1.C1.h_vol = Pe_ValveHP.C1.h_vol;
       volumeC1.P = Pump_BP.C1.P;
       volumeC1.Ce1.P = Pump_BP.C1.P;
       volumeC1.Ce2.P = Pump_BP.C1.P;
       volumeC1.Cs.P = Pump_BP.C1.P;
       volumeC1.Ce3.P = Pump_BP.C1.P;
       singularPressureLoss5.C2.P = Pump_BP.C1.P;
       volumeC1.Cs.h = Pump_BP.C1.h;
       Reheat_BP.Ee.P = Pump_BP.C2.P;
       Reheat_BP.Ee.h_vol = Pump_BP.C2.h_vol;
       Reheat_BP.Ee.h = Pump_BP.C2.h;
       Pump_BP.pro.d = Pump_BP.rho;
       Test_Timetable_Rotation_PumpLP.y.signal = Pump_BP.rpm_or_mpower.signal;
       Extraction_ReheatHP.P = singularPressureLoss3.C2.P;
       Extraction_ReheatHP.Ce.P = singularPressureLoss3.C2.P;
       Extraction_ReheatHP.Cs.P = singularPressureLoss3.C2.P;
       Extraction_ReheatHP.Cex.P = singularPressureLoss3.C2.P;
       lumpedStraightPipe.C1.P = singularPressureLoss3.C2.P;
       Dryer.P = singularPressureLoss3.C2.P;
       Dryer.Cev.P = singularPressureLoss3.C2.P;
       Dryer.Csv.P = singularPressureLoss3.C2.P;
       Dryer.Csl.P = singularPressureLoss3.C2.P;
       invSingularPressureLoss.C1.P = singularPressureLoss3.C2.P;
       SuperHeat.Ef.P = singularPressureLoss3.C2.P;
       Extraction_TurbHP_Oulet.P = singularPressureLoss3.C2.P;
       Extraction_TurbHP_Oulet.Ce.P = singularPressureLoss3.C2.P;
       Extraction_TurbHP_Oulet.Cs.P = singularPressureLoss3.C2.P;
       Extraction_TurbHP_Oulet.Cex.P = singularPressureLoss3.C2.P;
       Ideal_BOP2HeatNetWork_HX.Ec.P = singularPressureLoss3.C2.P;
       Pout_Turb_HP.C1.P = singularPressureLoss3.C2.P;
       Pout_Turb_HP.C2.P = singularPressureLoss3.C2.P;
       Tout_Turb_HP.P = singularPressureLoss3.C2.P;
       Tout_Turb_HP.C1.P = singularPressureLoss3.C2.P;
       Tout_Turb_HP.C2.P = singularPressureLoss3.C2.P;
       Extraction_ReheatHP.h = singularPressureLoss3.C2.h_vol;
       Extraction_ReheatHP.Ce.h_vol = singularPressureLoss3.C2.h_vol;
       Extraction_ReheatHP.Cs.h_vol = singularPressureLoss3.C2.h_vol;
       Tout_Turb_HP.C1.h_vol = singularPressureLoss3.C2.h_vol;
       singularPressureLoss3.pro.T = singularPressureLoss3.T;
       singularPressureLoss3.pro.d = singularPressureLoss3.rho;
       Extraction_TurbHP_Oulet.Ce.Q = Extraction_ReheatHP.Cs.Q;
       Pout_Turb_HP.Q = Extraction_ReheatHP.Cs.Q;
       Pout_Turb_HP.C1.Q = Extraction_ReheatHP.Cs.Q;
       Pout_Turb_HP.C2.Q = Extraction_ReheatHP.Cs.Q;
       Tout_Turb_HP.Q = Extraction_ReheatHP.Cs.Q;
       Tout_Turb_HP.C1.Q = Extraction_ReheatHP.Cs.Q;
       Tout_Turb_HP.C2.Q = Extraction_ReheatHP.Cs.Q;
       Extraction_TurbHP_Oulet.Ce.h = Extraction_ReheatHP.Cs.h;
       Pout_Turb_HP.C1.h = Extraction_ReheatHP.Cs.h;
       Pout_Turb_HP.C2.h = Extraction_ReheatHP.Cs.h;
       Tout_Turb_HP.h = Extraction_ReheatHP.Cs.h;
       Tout_Turb_HP.C1.h = Extraction_ReheatHP.Cs.h;
       Tout_Turb_HP.C2.h = Extraction_ReheatHP.Cs.h;
       lumpedStraightPipe.C1.h_vol = Extraction_ReheatHP.Cex.h_vol;
       lumpedStraightPipe.Q = Extraction_ReheatHP.Cex.Q;
       lumpedStraightPipe.C1.Q = Extraction_ReheatHP.Cex.Q;
       lumpedStraightPipe.C2.Q = Extraction_ReheatHP.Cex.Q;
       singularPressureLoss2.Q = Extraction_ReheatHP.Cex.Q;
       singularPressureLoss2.C1.Q = Extraction_ReheatHP.Cex.Q;
       singularPressureLoss2.C2.Q = Extraction_ReheatHP.Cex.Q;
       ReHeat_HP.Ev.Q = Extraction_ReheatHP.Cex.Q;
       lumpedStraightPipe.h = Extraction_ReheatHP.Cex.h;
       lumpedStraightPipe.C1.h = Extraction_ReheatHP.Cex.h;
       lumpedStraightPipe.C2.h = Extraction_ReheatHP.Cex.h;
       singularPressureLoss2.h = Extraction_ReheatHP.Cex.h;
       singularPressureLoss2.C1.h = Extraction_ReheatHP.Cex.h;
       singularPressureLoss2.C2.h = Extraction_ReheatHP.Cex.h;
       ReHeat_HP.Ev.h = Extraction_ReheatHP.Cex.h;
       singularPressureLoss2.C1.P = lumpedStraightPipe.C2.P;
       singularPressureLoss2.C1.h_vol = lumpedStraightPipe.C2.h_vol;
       lumpedStraightPipe.pro.T = lumpedStraightPipe.T;
       lumpedStraightPipe.pro.d = lumpedStraightPipe.rho;
       ReHeat_HP.P = singularPressureLoss2.C2.P;
       ReHeat_HP.Sp.P = singularPressureLoss2.C2.P;
       ReHeat_HP.Ev.P = singularPressureLoss2.C2.P;
       invSingularPressureLoss2.C1.P = singularPressureLoss2.C2.P;
       ReHeat_HP.h = singularPressureLoss2.C2.h_vol;
       ReHeat_HP.Ep.h_vol = singularPressureLoss2.C2.h_vol;
       ReHeat_HP.Sp.h_vol = singularPressureLoss2.C2.h_vol;
       ReHeat_HP.Ev.h_vol = singularPressureLoss2.C2.h_vol;
       invSingularPressureLoss2.C1.h_vol = singularPressureLoss2.C2.h_vol;
       singularPressureLoss2.pro.T = singularPressureLoss2.T;
       singularPressureLoss2.pro.d = singularPressureLoss2.rho;
       pI1.u.signal = feedback1.y.signal;
       Reheat_BP.Se.h_vol = sensorP1.C1.h_vol;
       sensorP1.C2.h = sensorP1.C1.h;
       Bache_a.Ce4.h = sensorP1.C1.h;
       Reheat_BP.Se.h = sensorP1.C1.h;
       Dryer.Cev.h_vol = Dryer.h;
       Dryer.Csv.h_vol = Dryer.h;
       SuperHeat.Ef.h_vol = Dryer.h;
       Extraction_TurbHP_Oulet.h = Dryer.h;
       Extraction_TurbHP_Oulet.Ce.h_vol = Dryer.h;
       Extraction_TurbHP_Oulet.Cs.h_vol = Dryer.h;
       Pout_Turb_HP.C2.h_vol = Dryer.h;
       Dryer.proe.x = Dryer.xe;
       Extraction_TurbHP_Oulet.Cs.Q = Dryer.Cev.Q;
       Extraction_TurbHP_Oulet.Cs.h = Dryer.Cev.h;
       SuperHeat.Ef.h = Dryer.Csv.h;
       invSingularPressureLoss.C1.h_vol = Dryer.Csl.h_vol;
       invSingularPressureLoss.Q = Dryer.Csl.Q;
       invSingularPressureLoss.C1.Q = Dryer.Csl.Q;
       invSingularPressureLoss.C2.Q = Dryer.Csl.Q;
       Bache_a.Ce2.Q = Dryer.Csl.Q;
       invSingularPressureLoss.h = Dryer.Csl.h;
       invSingularPressureLoss.C1.h = Dryer.Csl.h;
       invSingularPressureLoss.C2.h = Dryer.Csl.h;
       Bache_a.Ce2.h = Dryer.Csl.h;
       Turb_LP2.Ce.Q = Turb_LP2.Q;
       Turb_LP2.Cs.Q = Turb_LP2.Q;
       Extraction_ReheatBP.Cs.Q = Turb_LP2.Q;
       volumeC2.Ce1.Q = Turb_LP2.Q;
       Turb_LP2.Ce.P = Turb_LP2.Pe;
       lumpedStraightPipe1.C1.P = Turb_LP2.Pe;
       Turb_LP1_b.Ps = Turb_LP2.Pe;
       Turb_LP1_b.Cs.P = Turb_LP2.Pe;
       Extraction_ReheatBP.P = Turb_LP2.Pe;
       Extraction_ReheatBP.Ce.P = Turb_LP2.Pe;
       Extraction_ReheatBP.Cs.P = Turb_LP2.Pe;
       Extraction_ReheatBP.Cex.P = Turb_LP2.Pe;
       Turb_LP2.Cs.P = Turb_LP2.Ps;
       invSingularPressureLoss1.C2.P = Turb_LP2.Ps;
       volumeC2.P = Turb_LP2.Ps;
       volumeC2.Ce1.P = Turb_LP2.Ps;
       volumeC2.Ce2.P = Turb_LP2.Ps;
       volumeC2.Cs.P = Turb_LP2.Ps;
       volumeC2.Ce3.P = Turb_LP2.Ps;
       singularPressureLoss1.C1.P = Turb_LP2.Ps;
       Turb_LP2.proe.T = Turb_LP2.Te;
       Turb_LP2.Ce.h = Turb_LP2.Ce.h_vol;
       Turb_LP1_b.Cs.h_vol = Turb_LP2.Ce.h_vol;
       Extraction_ReheatBP.h = Turb_LP2.Ce.h_vol;
       Extraction_ReheatBP.Ce.h_vol = Turb_LP2.Ce.h_vol;
       Extraction_ReheatBP.Cs.h_vol = Turb_LP2.Ce.h_vol;
       Extraction_ReheatBP.Cs.h = Turb_LP2.Ce.h_vol;
       lumpedStraightPipe1.C1.Q = lumpedStraightPipe1.Q;
       lumpedStraightPipe1.C2.Q = lumpedStraightPipe1.Q;
       singularPressureLoss4.Q = lumpedStraightPipe1.Q;
       singularPressureLoss4.C1.Q = lumpedStraightPipe1.Q;
       singularPressureLoss4.C2.Q = lumpedStraightPipe1.Q;
       Extraction_ReheatBP.Cex.Q = lumpedStraightPipe1.Q;
       Reheat_BP.Ev.Q = lumpedStraightPipe1.Q;
       lumpedStraightPipe1.C1.h = lumpedStraightPipe1.h;
       lumpedStraightPipe1.C2.h = lumpedStraightPipe1.h;
       singularPressureLoss4.h = lumpedStraightPipe1.h;
       singularPressureLoss4.C1.h = lumpedStraightPipe1.h;
       singularPressureLoss4.C2.h = lumpedStraightPipe1.h;
       Extraction_ReheatBP.Cex.h = lumpedStraightPipe1.h;
       Reheat_BP.Ev.h = lumpedStraightPipe1.h;
       Extraction_ReheatBP.Cex.h_vol = lumpedStraightPipe1.C1.h_vol;
       singularPressureLoss4.C1.P = lumpedStraightPipe1.C2.P;
       singularPressureLoss4.C1.h_vol = lumpedStraightPipe1.C2.h_vol;
       lumpedStraightPipe1.pro.T = lumpedStraightPipe1.T;
       lumpedStraightPipe1.pro.d = lumpedStraightPipe1.rho;
       Reheat_BP.P = singularPressureLoss4.C2.P;
       Reheat_BP.Sp.P = singularPressureLoss4.C2.P;
       Reheat_BP.Ev.P = singularPressureLoss4.C2.P;
       invSingularPressureLoss1.C1.P = singularPressureLoss4.C2.P;
       Reheat_BP.h = singularPressureLoss4.C2.h_vol;
       Reheat_BP.Ep.h_vol = singularPressureLoss4.C2.h_vol;
       Reheat_BP.Sp.h_vol = singularPressureLoss4.C2.h_vol;
       Reheat_BP.Ev.h_vol = singularPressureLoss4.C2.h_vol;
       invSingularPressureLoss1.C1.h_vol = singularPressureLoss4.C2.h_vol;
       singularPressureLoss4.pro.T = singularPressureLoss4.T;
       singularPressureLoss4.pro.d = singularPressureLoss4.rho;
       singularPressureLoss5.pro.T = singularPressureLoss5.T;
       singularPressureLoss5.pro.d = singularPressureLoss5.rho;
       SuperHeat.Qc = Bache_a.Ce3.Q;
       SuperHeat.Ec.Q = Bache_a.Ce3.Q;
       SuperHeat.Sc.Q = Bache_a.Ce3.Q;
       splitter_SGoutlet.Cs2.Q = Bache_a.Ce3.Q;
       Valve_superheat.Q = Bache_a.Ce3.Q;
       Valve_superheat.C1.Q = Bache_a.Ce3.Q;
       Valve_superheat.C2.Q = Bache_a.Ce3.Q;
       sensorP1.C2.P = sensorP1.C1.P;
       invSingularPressureLoss.C2.P = sensorP1.C1.P;
       Bache_a.P = sensorP1.C1.P;
       Bache_a.Ce2.P = sensorP1.C1.P;
       Bache_a.Ce3.P = sensorP1.C1.P;
       Bache_a.Ce4.P = sensorP1.C1.P;
       Bache_a.Ce1.P = sensorP1.C1.P;
       Bache_a.Cs4.P = sensorP1.C1.P;
       Bache_a.Cs1.P = sensorP1.C1.P;
       Bache_a.Cs2.P = sensorP1.C1.P;
       Bache_a.Cs3.P = sensorP1.C1.P;
       singularPressureLoss6.C1.P = sensorP1.C1.P;
       Reheat_BP.Se.P = sensorP1.C1.P;
       invSingularPressureLoss2.C2.P = sensorP1.C1.P;
       Valve_superheat.C2.P = sensorP1.C1.P;
       singularPressureLoss6.Q = Bache_a.Cs1.Q;
       singularPressureLoss6.C1.Q = Bache_a.Cs1.Q;
       singularPressureLoss6.C2.Q = Bache_a.Cs1.Q;
       Bache_b.Ce2.Q = Bache_a.Cs1.Q;
       singularPressureLoss6.h = Bache_a.Cs1.h;
       singularPressureLoss6.C1.h = Bache_a.Cs1.h;
       singularPressureLoss6.C2.h = Bache_a.Cs1.h;
       Bache_b.Ce2.h = Bache_a.Cs1.h;
       splitter_SGoutlet.Cs2.h = SuperHeat.Ec.h;
       Tin_Turb_LP1a.C1.h_vol = SuperHeat.Sf.h_vol;
       Valve_superheat.C1.P = SuperHeat.Sc.P;
       Valve_superheat.C1.h_vol = SuperHeat.Sc.h_vol;
       SuperHeat.promf.d = SuperHeat.rhof;
       SuperHeat.Sc.h = Bache_a.Ce3.h;
       SuperHeat.lsat.h = Bache_a.Ce3.h;
       Valve_superheat.h = Bache_a.Ce3.h;
       Valve_superheat.C1.h = Bache_a.Ce3.h;
       Valve_superheat.C2.h = Bache_a.Ce3.h;
       SuperHeat.promc.d = SuperHeat.rhoc;
       singularPressureLoss6.pro.T = singularPressureLoss6.T;
       singularPressureLoss6.pro.d = singularPressureLoss6.rho;
       Turb_LP1_b.Ce.Q = Turb_LP1_b.Q;
       Turb_LP1_b.Cs.Q = Turb_LP1_b.Q;
       Extraction_ReheatBP.Ce.Q = Turb_LP1_b.Q;
       Extraction_TurbBP1a_Outlet.Cs.Q = Turb_LP1_b.Q;
       Turb_LP1_a.Ps = Turb_LP1_b.Pe;
       Turb_LP1_a.Cs.P = Turb_LP1_b.Pe;
       Turb_LP1_b.Ce.P = Turb_LP1_b.Pe;
       Extraction_TurbBP1a_Outlet.P = Turb_LP1_b.Pe;
       Extraction_TurbBP1a_Outlet.Ce.P = Turb_LP1_b.Pe;
       Extraction_TurbBP1a_Outlet.Cs.P = Turb_LP1_b.Pe;
       Extraction_TurbBP1a_Outlet.Cex.P = Turb_LP1_b.Pe;
       Valve_superheat2.C1.P = Turb_LP1_b.Pe;
       Turb_LP1_b.proe.T = Turb_LP1_b.Te;
       Turb_LP1_b.pros.T = Turb_LP1_b.Ts;
       Extraction_ReheatBP.Ce.h = Turb_LP1_b.Cs.h;
       Turb_LP1_b.props.h = Turb_LP1_b.His;
       Valve_superheat1.Q = splitter_SGoutlet.Cs1.Q;
       Valve_superheat1.C1.Q = splitter_SGoutlet.Cs1.Q;
       Valve_superheat1.C2.Q = splitter_SGoutlet.Cs1.Q;
       sinkP.Q = splitter_SGoutlet.Cs1.Q;
       sinkP.C.Q = splitter_SGoutlet.Cs1.Q;
       Valve_superheat1.h = splitter_SGoutlet.Cs1.h;
       Valve_superheat1.C1.h = splitter_SGoutlet.Cs1.h;
       Valve_superheat1.C2.h = splitter_SGoutlet.Cs1.h;
       sinkP.C.h = splitter_SGoutlet.Cs1.h;
       Extraction_TurbHP_Oulet.Cex.Q = Bache_b.Ce1.Q;
       Ideal_BOP2HeatNetWork_HX.Qc = Bache_b.Ce1.Q;
       Ideal_BOP2HeatNetWork_HX.Ec.Q = Bache_b.Ce1.Q;
       Ideal_BOP2HeatNetWork_HX.Sc.Q = Bache_b.Ce1.Q;
       Valve_Hybrid_IP.Q = Bache_b.Ce1.Q;
       Valve_Hybrid_IP.C1.Q = Bache_b.Ce1.Q;
       Valve_Hybrid_IP.C2.Q = Bache_b.Ce1.Q;
       Q_Extract_Hybrid_IP.Q = Bache_b.Ce1.Q;
       Q_Extract_Hybrid_IP.C1.Q = Bache_b.Ce1.Q;
       Q_Extract_Hybrid_IP.C2.Q = Bache_b.Ce1.Q;
       T_Outlet_BOP_HX_HeatNetWork.Q = Bache_b.Ce1.Q;
       T_Outlet_BOP_HX_HeatNetWork.C1.Q = Bache_b.Ce1.Q;
       T_Outlet_BOP_HX_HeatNetWork.C2.Q = Bache_b.Ce1.Q;
       Pump_HP.C1.Q = Pump_HP.Q;
       Pump_HP.C2.Q = Pump_HP.Q;
       Bache_b.Cs.Q = Pump_HP.Q;
       ReHeat_HP.Ee.Q = Pump_HP.Q;
       ReHeat_HP.Se.Q = Pump_HP.Q;
       sinkP_4FMU.Q = Pump_HP.Q;
       sinkP_4FMU.C.Q = Pump_HP.Q;
       Valve_superheat2.C1.h_vol = Extraction_TurbBP1a_Outlet.Cex.h_vol;
       Valve_superheat2.Q = Extraction_TurbBP1a_Outlet.Cex.Q;
       Valve_superheat2.C1.Q = Extraction_TurbBP1a_Outlet.Cex.Q;
       Valve_superheat2.C2.Q = Extraction_TurbBP1a_Outlet.Cex.Q;
       sinkP2.Q = Extraction_TurbBP1a_Outlet.Cex.Q;
       sinkP2.C.Q = Extraction_TurbBP1a_Outlet.Cex.Q;
       Valve_superheat2.h = Extraction_TurbBP1a_Outlet.Cex.h;
       Valve_superheat2.C1.h = Extraction_TurbBP1a_Outlet.Cex.h;
       Valve_superheat2.C2.h = Extraction_TurbBP1a_Outlet.Cex.h;
       sinkP2.C.h = Extraction_TurbBP1a_Outlet.Cex.h;
       invSingularPressureLoss1.Q = Reheat_BP.Sp.Q;
       invSingularPressureLoss1.C1.Q = Reheat_BP.Sp.Q;
       invSingularPressureLoss1.C2.Q = Reheat_BP.Sp.Q;
       volumeC2.Ce3.Q = Reheat_BP.Sp.Q;
       invSingularPressureLoss1.h = Reheat_BP.Sp.h;
       invSingularPressureLoss1.C1.h = Reheat_BP.Sp.h;
       invSingularPressureLoss1.C2.h = Reheat_BP.Sp.h;
       volumeC2.Ce3.h = Reheat_BP.Sp.h;
       Reheat_BP.lsatC.T = Reheat_BP.TsatC;
       Reheat_BP.lsatC.h = Reheat_BP.HsateC;
       Reheat_BP.vsatC.h = Reheat_BP.HsatvC;
       Reheat_BP.promeF.d = Reheat_BP.rho;
       sinkP_4FMU.C.h = ReHeat_HP.Se.h;
       ReHeat_HP.Sp.Q = Bache_a.Ce1.Q;
       invSingularPressureLoss2.Q = Bache_a.Ce1.Q;
       invSingularPressureLoss2.C1.Q = Bache_a.Ce1.Q;
       invSingularPressureLoss2.C2.Q = Bache_a.Ce1.Q;
       ReHeat_HP.Sp.h = Bache_a.Ce1.h;
       invSingularPressureLoss2.h = Bache_a.Ce1.h;
       invSingularPressureLoss2.C1.h = Bache_a.Ce1.h;
       invSingularPressureLoss2.C2.h = Bache_a.Ce1.h;
       ReHeat_HP.lsatC.T = ReHeat_HP.TsatC;
       ReHeat_HP.lsatC.h = ReHeat_HP.HsateC;
       ReHeat_HP.vsatC.h = ReHeat_HP.HsatvC;
       ReHeat_HP.rho = ReHeat_HP.promeF.d;
       singularPressureLoss1.pro.T = singularPressureLoss1.T;
       singularPressureLoss1.pro.d = singularPressureLoss1.rho;
       Valve_superheat.pro.T = Valve_superheat.T;
       pI5.y.signal = Valve_superheat.Ouv.signal;
       Tin_Turb_LP1a.pro.T = Tin_Turb_LP1a.T;
       feedback5.u1.signal = Tin_Turb_LP1a.Measure.signal;
       rampe3.y.signal = feedback5.u2.signal;
       pI5.u.signal = feedback5.y.signal;
       singularPressureLoss_SG_Out.C2.P = Pipe_SGs.C1.P;
       singularPressureLoss_SG_Out.C2.h_vol = Pipe_SGs.C1.h_vol;
       Pipe_SGs.pro.T = Pipe_SGs.T;
       Pipe_SGs.pro.d = Pipe_SGs.rho;
       Pout_SG.C2.h_vol = singularPressureLoss_SG_Out.C1.h_vol;
       singularPressureLoss_SG_Out.pro.T = singularPressureLoss_SG_Out.T;
       singularPressureLoss_SG_Out.pro.d = singularPressureLoss_SG_Out.rho;
       Ideal_BOP2HeatNetWork_HX.Ec.h_vol = Extraction_TurbHP_Oulet.Cex.h_vol;
       Ideal_BOP2HeatNetWork_HX.Ec.h = Extraction_TurbHP_Oulet.Cex.h;
       Valve_Hybrid_IP.C1.P = Ideal_BOP2HeatNetWork_HX.Sc.P;
       T_Outlet_BOP_HX_HeatNetWork.P = Ideal_BOP2HeatNetWork_HX.Sc.P;
       T_Outlet_BOP_HX_HeatNetWork.C1.P = Ideal_BOP2HeatNetWork_HX.Sc.P;
       T_Outlet_BOP_HX_HeatNetWork.C2.P = Ideal_BOP2HeatNetWork_HX.Sc.P;
       T_Outlet_BOP_HX_HeatNetWork.C1.h_vol = Ideal_BOP2HeatNetWork_HX.Sc.h_vol;
       Ideal_BOP2HeatNetWork_HX.Sc.h = Bache_b.Ce1.h;
       Ideal_BOP2HeatNetWork_HX.lsat.h = Bache_b.Ce1.h;
       Valve_Hybrid_IP.h = Bache_b.Ce1.h;
       Valve_Hybrid_IP.C1.h = Bache_b.Ce1.h;
       Valve_Hybrid_IP.C2.h = Bache_b.Ce1.h;
       Q_Extract_Hybrid_IP.C1.h = Bache_b.Ce1.h;
       Q_Extract_Hybrid_IP.C2.h = Bache_b.Ce1.h;
       T_Outlet_BOP_HX_HeatNetWork.h = Bache_b.Ce1.h;
       T_Outlet_BOP_HX_HeatNetWork.C1.h = Bache_b.Ce1.h;
       T_Outlet_BOP_HX_HeatNetWork.C2.h = Bache_b.Ce1.h;
       Ideal_BOP2HeatNetWork_HX.promc.d = Ideal_BOP2HeatNetWork_HX.rhoc;
       Valve_Hybrid_IP.pro.T = Valve_Hybrid_IP.T;
       pI6.y.signal = Valve_Hybrid_IP.Ouv.signal;
       T_Outlet_BOP_HX_HeatNetWork.C2.h_vol = Valve_Hybrid_IP.C1.h_vol;
       Q_Extract_Hybrid_IP.C1.h_vol = Valve_Hybrid_IP.C2.h_vol;
       Vapor_MassFlowRamp_extracted4Cogeneration.y.signal = feedback6.u2.signal;
       pI6.u.signal = feedback6.y.signal;
       Q_Extract_Hybrid_IP.Measure.signal = feedback6.u1.signal;
       Tout_Turb_HP.C2.h_vol = Pout_Turb_HP.C1.h_vol;
       Valve_superheat1.pro.T = Valve_superheat1.T;
       rampe2.y.signal = Valve_superheat1.Ouv.signal;
       Valve_superheat2.pro.T = Valve_superheat2.T;
       rampe4.y.signal = Valve_superheat2.Ouv.signal;
       Hin_set.outputReal.signal = Hin_set.u;
       sourceQ_4FMU.ISpecificEnthalpy.signal = Hin_set.u;
       Qin_set.outputReal.signal = Qin_set.u;
       sourceQ_4FMU.IMassFlow.signal = Qin_set.u;
       Pout_calc.u = sinkP_4FMU.IPressure.signal;
       Pout_calc.outputReal.signal = sinkP_4FMU.IPressure.signal;
       BOP_HeatSink.prol.T = BOP_HeatSink.Tl;
       BOP_HeatSink.prol.d = BOP_HeatSink.rhol;
       BOP_HeatSink.prol.x = BOP_HeatSink.xl;
       BOP_HeatSink.prov.T = BOP_HeatSink.Tv;
       BOP_HeatSink.prov.d = BOP_HeatSink.rhov;
       BOP_HeatSink.prov.x = BOP_HeatSink.xv;
       Turb_LP2.pros.T = Turb_LP2.Ts;
       volumeC2.Ce1.h = Turb_LP2.Cs.h;
       Turb_LP2.props.h = Turb_LP2.His;
       volumeC1.pro.T = volumeC1.T;
       volumeC1.pro.d = volumeC1.rho;
       Bache_a.pro.T = Bache_a.T;
       Bache_a.pro.d = Bache_a.rho;
       Bache_b.pro.T = Bache_b.T;
       Bache_b.pro.d = Bache_b.rho;
       volumeC2.pro.T = volumeC2.T;
       volumeC2.pro.d = volumeC2.rho;
       Rend_Cycle.showNumber = Eff_Cycle_net;
       Pw_reseau.showNumber = Pw_eGrid;
       Pw_COG.showNumber = Pwth_COG;
       Temp_COG.showNumber = T_COG;
       Heat_Power.showNumber = Heat_Power_ratio;
       Temp_inletGV.showNumber = Tin_SG;
       Pw_GV.showNumber = Pwth_SG;
       sourceQ1.C.P = sourceQ1.P;
       BOP_HeatSink.Cee.P = sourceQ1.P;
       BOP_HeatSink.Cee.h = sourceQ1.C.h;
       BOP_HeatSink.Cse.h = sinkP1.C.h;
       BOP_HeatSink.rhom = BOP_HeatSink.proe.d;
       singularPressureLoss3.C1.h_vol = Turb_HP.Cs.h_vol;
       Tout_SG.pro.T = Tout_SG.T;
       sourceQ_4FMU.h = Tout_SG.C1.h_vol;
       sourceQ_4FMU.C.h_vol = Tout_SG.C1.h_vol;
       Generator.Wmec1.signal = Turb_HP.MechPower.signal;
       Generator.Wmec3.signal = Turb_LP1_a.MechPower.signal;
       Turb_LP1_b.MechPower.signal = Generator.Wmec4.signal;
       Turb_LP2.MechPower.signal = Generator.Wmec5.signal;
       invSingularPressureLoss.pro.T = invSingularPressureLoss.T;
       invSingularPressureLoss.pro.d = invSingularPressureLoss.rho;
       SuperHeat.proce.T = SuperHeat.Tec;
       SuperHeat.procs.T = SuperHeat.Tsc;
       SuperHeat.profe.T = SuperHeat.Tef;
       SuperHeat.profs.T = SuperHeat.Tsf;
       splitter_SGoutlet.Oalpha1.signal = splitter_SGoutlet.alpha1;
       splitter_SGoutlet.Oalpha2.signal = splitter_SGoutlet.alpha2;
       splitter_SGoutlet.pro.T = splitter_SGoutlet.T;
       invSingularPressureLoss1.pro.T = invSingularPressureLoss1.T;
       invSingularPressureLoss1.pro.d = invSingularPressureLoss1.rho;
       sinkP_4FMU.P = ReHeat_HP.Se.P;
       sinkP_4FMU.C.P = ReHeat_HP.Se.P;
       invSingularPressureLoss2.pro.T = invSingularPressureLoss2.T;
       invSingularPressureLoss2.pro.d = invSingularPressureLoss2.rho;
       Tout_SG.C1.Q = Tout_SG.Q;
       Tout_SG.C2.Q = Tout_SG.Q;
       splitter_SGoutlet.Ce.Q = Tout_SG.Q;
       Pipe_SGs.Q = Tout_SG.Q;
       Pipe_SGs.C1.Q = Tout_SG.Q;
       Pipe_SGs.C2.Q = Tout_SG.Q;
       singularPressureLoss_SG_Out.Q = Tout_SG.Q;
       singularPressureLoss_SG_Out.C1.Q = Tout_SG.Q;
       singularPressureLoss_SG_Out.C2.Q = Tout_SG.Q;
       Pout_SG.Q = Tout_SG.Q;
       Pout_SG.C1.Q = Tout_SG.Q;
       Pout_SG.C2.Q = Tout_SG.Q;
       sourceQ_4FMU.Q = Tout_SG.Q;
       sourceQ_4FMU.C.Q = Tout_SG.Q;
       Tout_SG.P = singularPressureLoss_SG_Out.C1.P;
       Tout_SG.C1.P = singularPressureLoss_SG_Out.C1.P;
       Tout_SG.C2.P = singularPressureLoss_SG_Out.C1.P;
       Pout_SG.C1.P = singularPressureLoss_SG_Out.C1.P;
       Pout_SG.C2.P = singularPressureLoss_SG_Out.C1.P;
       sourceQ_4FMU.P = singularPressureLoss_SG_Out.C1.P;
       sourceQ_4FMU.C.P = singularPressureLoss_SG_Out.C1.P;
       fluid2TSPro1.port_a.h_outflow = Ideal_BOP2HeatNetWork_HX.Ef.h_vol;
       fluid2TSPro1.steam_outlet.h_vol = Ideal_BOP2HeatNetWork_HX.Ef.h_vol;
       fluid2TSPro1.steam_outlet.h = Ideal_BOP2HeatNetWork_HX.Ef.h;
       T_2HeatNetWork.C1.h_vol = Ideal_BOP2HeatNetWork_HX.Sf.h_vol;
       T_2HeatNetWork.h = Ideal_BOP2HeatNetWork_HX.Sf.h;
       T_2HeatNetWork.C1.h = Ideal_BOP2HeatNetWork_HX.Sf.h;
       T_2HeatNetWork.C2.h = Ideal_BOP2HeatNetWork_HX.Sf.h;
       fluid2TSPro.steam_inlet.h = Ideal_BOP2HeatNetWork_HX.Sf.h;
       Ideal_BOP2HeatNetWork_HX.proce.T = Ideal_BOP2HeatNetWork_HX.Tec;
       Ideal_BOP2HeatNetWork_HX.procs.T = Ideal_BOP2HeatNetWork_HX.Tsc;
       Ideal_BOP2HeatNetWork_HX.profe.T = Ideal_BOP2HeatNetWork_HX.Tef;
       Ideal_BOP2HeatNetWork_HX.promf.d = Ideal_BOP2HeatNetWork_HX.rhof;
       Ideal_BOP2HeatNetWork_HX.profs.T = Ideal_BOP2HeatNetWork_HX.Tsf;
       T_Outlet_BOP_HX_HeatNetWork.pro.T = T_Outlet_BOP_HX_HeatNetWork.T;
       Tout_Turb_HP.pro.T = Tout_Turb_HP.T;
       sourceQ_4FMU.OPressure.signal = Pout_SG.Measure.signal;
       Pin_calc.y = Pout_SG.Measure.signal;
       Pin_calc.inputReal.signal = Pout_SG.Measure.signal;
       sinkP_4FMU.pro.T = sinkP_4FMU.T;
       T_2HeatNetWork.pro.T = T_2HeatNetWork.T;
       fluid2TSPro.steam_inlet.h_vol = T_2HeatNetWork.C2.h_vol;
       fluid2TSPro.port_b.h_outflow = T_2HeatNetWork.C2.h_vol;
       InletFlow_FromHeatNetwork.medium.state.d = InletFlow_FromHeatNetwork.medium.d;
       InletFlow_FromHeatNetwork.medium.state.T = InletFlow_FromHeatNetwork.medium.T;
       fluid2TSPro.port_b.m_flow = InletFlow_FromHeatNetwork.'ports[1]'.m_flow;
       fluid2TSPro1.port_a.p = Ideal_BOP2HeatNetWork_HX.Ef.P;
       fluid2TSPro1.steam_outlet.P = Ideal_BOP2HeatNetWork_HX.Ef.P;
       InletFlow_FromHeatNetwork.medium.p = Ideal_BOP2HeatNetWork_HX.Ef.P;
       InletFlow_FromHeatNetwork.medium.state.p = Ideal_BOP2HeatNetWork_HX.Ef.P;
       InletFlow_FromHeatNetwork.medium.sat.psat = Ideal_BOP2HeatNetWork_HX.Ef.P;
       InletFlow_FromHeatNetwork.'ports[1]'.p = Ideal_BOP2HeatNetWork_HX.Ef.P;
       Ideal_BOP2HeatNetWork_HX.Ef.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       Ideal_BOP2HeatNetWork_HX.Sf.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       T_2HeatNetWork.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       T_2HeatNetWork.C1.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       T_2HeatNetWork.C2.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       fluid2TSPro1.port_a.m_flow = Ideal_BOP2HeatNetWork_HX.Qf;
       fluid2TSPro1.steam_outlet.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       InletFlow_FromHeatNetwork.m_flow_in = Ideal_BOP2HeatNetWork_HX.Qf;
       BackPressure_FromHeatNetWork.'ports[1]'.m_flow = Ideal_BOP2HeatNetWork_HX.Qf;
       fluid2TSPro.steam_inlet.Q = Ideal_BOP2HeatNetWork_HX.Qf;
       BackPressure_FromHeatNetWork.medium.state.phase = BackPressure_FromHeatNetWork.medium.phase;
       InletFlow_FromHeatNetwork.medium.state.phase = InletFlow_FromHeatNetwork.medium.phase;
       annotation (__Dymola_FMUImportVersion="Dymola 2021x", __Dymola_FMUImportPath="C:/Users/NA155421/Desktop/FMU_WorkingDirectory/BOP.fmu", __Dymola_FMUImportIncludeAllVariables="true", __Dymola_FMUImportIntegrate="false", experiment(StartTime=0.0, StopTime=1.0, Tolerance=1E-06),
         Icon(graphics={
           Text(extent={{-150,150},{150,110}},
             lineColor={0,0,255},
             textString="%name"),
           Text(extent={{-150,-110},{150,-150}},
             lineColor={95,95,95},
             textString="FMI 2.0 CS")}),
     Documentation(info="<html>
<h4>ModelDescription Attributes</h4>
<ul>
<li>fmiVersion = 2.0</li>
<li>modelName = BOP</li>
<li>generationTool = Dymola Version 2021x (64-bit), 2020-10-09 (requires license to execute)</li>
<li>generationDateAndTime = 2023-12-01T08:38:56Z</li>
</ul>
<p><br><b>Model Exchange Attributes</b></p>
<ul>
<li>needsExecutionTool = false</li>
<li>completedIntegratorStepNotNeeded = false</li>
<li>canBeInstantiatedOnlyOncePerProcess = false</li>
<li>canNotUseMemoryManagementFunctions = false</li>
<li>canGetAndSetFMUstate = true</li>
<li>canSerializeFMUstate = true</li>
<li>providesDirectionalDerivative = true</li>
</ul>
<p><br><b>Co-Simulation Attributes</b></p>
<ul>
<li>needsExecutionTool = false</li>
<li>canHandleVariableCommunicationStepSize = true</li>
<li>canInterpolateInputs = true</li>
<li>maxOutputDerivativeOrder = 1</li>
<li>canRunAsynchronuously = false</li>
<li>canBeInstantiatedOnlyOncePerProcess = false</li>
<li>canNotUseMemoryManagementFunctions = false</li>
<li>canGetAndSetFMUstate = true</li>
<li>canSerializeFMUstate = true</li>
<li>providesDirectionalDerivative = true</li>
</ul>
</html>"));
     end BOP_fmu;

     model Boiler_fmu
        "Boiler with modified Fluid connection allowing to build Fluid FMU"
     extends fmuIcon;
     // Model automatically generated by Dymola from FMI model description
      public
       type Modelica_Blocks_Interfaces_RealInput = Real;
       type Modelica_Blocks_Interfaces_RealOutput = Real;
       type ThermoSysPro_Units_SI_AbsolutePressure = Real (unit = "Pa", displayUnit = "bar", nominal = 100000.0, quantity = "Pressure", min = 0.0);
       type ThermoSysPro_Units_SI_Density = Real (unit = "kg/m3", displayUnit = "g/cm3", quantity = "Density", min = 0.0);
       type ThermoSysPro_Units_SI_DerDensityByEnthalpy = Real (unit = "kg.s2/m5");
       type ThermoSysPro_Units_SI_DerDensityByPressure = Real (unit = "s2/m2");
       type ThermoSysPro_Units_SI_MassFlowRate = Real (unit = "kg/s", quantity = "MassFlowRate");
       type ThermoSysPro_Units_SI_MassFraction = Real (quantity = "MassFraction", min = 0.0, max = 1.0);
       type ThermoSysPro_Units_SI_SpecificEnergy = Real (unit = "J/kg", quantity = "SpecificEnergy");
       type ThermoSysPro_Units_SI_SpecificEnthalpy = Real (unit = "J/kg", quantity = "SpecificEnergy");
       type ThermoSysPro_Units_SI_SpecificEntropy = Real (unit = "J/(kg.K)", quantity = "SpecificEntropy");
       type ThermoSysPro_Units_SI_SpecificHeatCapacity = Real (unit = "J/(kg.K)", quantity = "SpecificHeatCapacity");
       type ThermoSysPro_Units_SI_Temperature = Real (unit = "K", displayUnit = "degC", nominal = 300.0, quantity = "ThermodynamicTemperature", min = 0.0);
      protected
       record e_Boiler_rec
         parameter Modelica.Units.SI.Power W = 1000000.0 "Input electrical power";
         parameter Real eta = 100 "Boiler efficiency (percent)";
         parameter Modelica.Units.SI.Pressure deltaP = 400000 "Pressure loss";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Modelica.Units.SI.Temperature Te "Inlet temperature";
         Modelica.Units.SI.Temperature Ts "Outlet temperature";
         Modelica.Units.SI.MassFlowRate Q "Mass flow";
         Modelica.Units.SI.SpecificEnthalpy deltaH "Specific enthalpy variation between the outlet and the inlet";
         Modelica.Units.SI.Power We "Electrical power";
        protected
         record Ce_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Ce_rec;
        public
         Ce_rec Ce;
        protected
         record Cs_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end Cs_rec;
        public
         Cs_rec Cs;
        protected
         record Signal_Elec_rec
           Real signal(unit = "W");
         end Signal_Elec_rec;
        public
         Signal_Elec_rec Signal_Elec;
        protected
         record Prop_e_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end Prop_e_rec;
        public
         Prop_e_rec Prop_e;
        protected
         record Prop_s_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end Prop_s_rec;
        public
         Prop_s_rec Prop_s;
       end e_Boiler_rec;
      public
       e_Boiler_rec e_Boiler annotation(Dialog);
      protected
       record InputPower_Ramp_rec
         parameter Real Starttime(unit = "s") = 1300 "Instant de départ de la rampe (s)";
         parameter Real Duration(unit = "s") = 100 "Durée de la rampe (s)";
         parameter Real Initialvalue(unit = "W") = 540000000.0 "Valeur initiale de la sortie";
         parameter Real Finalvalue(unit = "W") = 486000000.0 "Valeur finale de la sortie";
        protected
         record y_rec
           Real signal(unit = "W");
         end y_rec;
        public
         y_rec y;
       end InputPower_Ramp_rec;
      public
       InputPower_Ramp_rec InputPower_Ramp annotation(Dialog);
      protected
       record Qin_set_rec
         Modelica_Blocks_Interfaces_RealInput u(unit = "kg/s");
        protected
         record outputReal_rec
           Real signal(unit = "kg/s");
         end outputReal_rec;
        public
         outputReal_rec outputReal;
       end Qin_set_rec;
      public
       Qin_set_rec Qin_set;
      protected
       record Hin_set_rec
         Modelica_Blocks_Interfaces_RealInput u(unit = "J/kg");
        protected
         record outputReal_rec
           Real signal(unit = "J/kg");
         end outputReal_rec;
        public
         outputReal_rec outputReal;
       end Hin_set_rec;
      public
       Hin_set_rec Hin_set;
      protected
       record sourceQ_4FMU_rec
         parameter Modelica.Units.SI.MassFlowRate Q0 = 100 "Mass flow (active if IMassFlow connector is not connected)";
         parameter Modelica.Units.SI.SpecificEnthalpy h0 = 100000 "Fluid specific enthalpy (active if IEnthalpy connector is not connected)";
         Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
         Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
         Modelica.Units.SI.SpecificEnthalpy h "Fluid specific enthalpy";
        protected
         record IMassFlow_rec
           Real signal(unit = "kg/s");
         end IMassFlow_rec;
        public
         IMassFlow_rec IMassFlow;
        protected
         record ISpecificEnthalpy_rec
           Real signal(unit = "J/kg");
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record OPressure_rec
           Real signal(unit = "Pa");
         end OPressure_rec;
        public
         OPressure_rec OPressure;
       end sourceQ_4FMU_rec;
      public
       sourceQ_4FMU_rec sourceQ_4FMU annotation(Dialog);
      protected
       record Pin_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y(unit = "Pa");
        protected
         record inputReal_rec
           Real signal(unit = "Pa");
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Pin_calc_rec;
      public
       Pin_calc_rec Pin_calc;
      protected
       record sinkP_4FMU_rec
         parameter Modelica.Units.SI.AbsolutePressure P0 = 100000 "Sink pressure";
         parameter Modelica.Units.SI.Temperature T0 = 290 "Sink temperature (active if option_temperature=1)";
         parameter Modelica.Units.SI.SpecificEnthalpy h0 = 2944000.0 "Sink specific enthalpy (active if option_temperature=2)";
         parameter Integer option_temperature = 1 "1:temperature fixed - 2:specific enthalpy fixed";
         parameter Integer mode = 0 "IF97 region. 1:liquid - 2:steam - 4:saturation line - 0:automatic";
         Modelica.Units.SI.AbsolutePressure P "Fluid pressure";
         Modelica.Units.SI.MassFlowRate Q "Mass flow rate";
         Modelica.Units.SI.Temperature T "Fluid temperature";
         Modelica.Units.SI.SpecificEnthalpy h "Fluid enthalpy";
        protected
         record pro_rec
           ThermoSysPro_Units_SI_Temperature T(nominal = 320.0, min = 200.0, max = 6000.0) "Temperature";
           ThermoSysPro_Units_SI_Density d(nominal = 998.0, min = 1E-09, max = 100000.0) "Density";
           ThermoSysPro_Units_SI_SpecificEnergy u(nominal = 1000000.0, min = -100000000.0, max = 100000000.0) "Specific inner energy";
           ThermoSysPro_Units_SI_SpecificEntropy s(nominal = 1000.0, min = -1000000.0, max = 1000000.0) "Specific entropy";
           ThermoSysPro_Units_SI_SpecificHeatCapacity cp(nominal = 1000.0, min = 1E-09, max = 1E+60) "Specific heat capacity at constant presure";
           ThermoSysPro_Units_SI_DerDensityByEnthalpy ddhp "Derivative of density wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_DerDensityByPressure ddph "Derivative of density wrt. pressure at constant specific enthalpy";
           Real duph(unit = "m3/kg") "Derivative of specific inner energy wrt. pressure at constant specific enthalpy";
           Real duhp "Derivative of specific inner energy wrt. specific enthalpy at constant pressure";
           ThermoSysPro_Units_SI_MassFraction x "Vapor mass fraction";
         end pro_rec;
        public
         pro_rec pro;
        protected
         record IPressure_rec
           Real signal(unit = "Pa");
         end IPressure_rec;
        public
         IPressure_rec IPressure;
        protected
         record ISpecificEnthalpy_rec
           Real signal(unit = "J/kg");
         end ISpecificEnthalpy_rec;
        public
         ISpecificEnthalpy_rec ISpecificEnthalpy;
        protected
         record C_rec
           ThermoSysPro_Units_SI_AbsolutePressure P "Fluid pressure in the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h_vol "Fluid specific enthalpy in the control volume";
           ThermoSysPro_Units_SI_MassFlowRate Q "Mass flow rate of the fluid crossing the boundary of the control volume";
           ThermoSysPro_Units_SI_SpecificEnthalpy h "Specific enthalpy of the fluid crossing the boundary of the control volume";
           constant Boolean a = true "Pseudo-variable for the verification of the connection orientation";
           constant Boolean b = true "Pseudo-variable for the verification of the connection orientation";
         end C_rec;
        public
         C_rec C;
        protected
         record OSpecificEnthalpy_rec
           Real signal(unit = "J/kg");
         end OSpecificEnthalpy_rec;
        public
         OSpecificEnthalpy_rec OSpecificEnthalpy;
        protected
         record OFlowRate_rec
           Real signal(unit = "kg/s");
         end OFlowRate_rec;
        public
         OFlowRate_rec OFlowRate;
       end sinkP_4FMU_rec;
      public
       sinkP_4FMU_rec sinkP_4FMU annotation(Dialog);
      protected
       record Qout_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y(unit = "kg/s");
        protected
         record inputReal_rec
           Real signal(unit = "kg/s");
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Qout_calc_rec;
      public
       Qout_calc_rec Qout_calc;
      protected
       record Hout_calc_rec
         Modelica_Blocks_Interfaces_RealOutput y(unit = "J/kg");
        protected
         record inputReal_rec
           Real signal(unit = "J/kg");
         end inputReal_rec;
        public
         inputReal_rec inputReal;
       end Hout_calc_rec;
      public
       Hout_calc_rec Hout_calc;
      protected
       record Pout_calc_rec
         Modelica_Blocks_Interfaces_RealInput u(unit = "Pa");
        protected
         record outputReal_rec
           Real signal(unit = "Pa");
         end outputReal_rec;
        public
         outputReal_rec outputReal annotation(Dialog);
       end Pout_calc_rec;
      public
       Pout_calc_rec Pout_calc annotation(Dialog);
       parameter Real _Hin_BC_start = 691000
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Hin_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Hin_BC(unit = "J/kg", start = _Hin_BC_start)
       annotation (Placement(transformation(extent={{-124,30},{-84,70}})));
       parameter Real _Qin_BC_start = 240
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Qin_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Qin_BC(unit = "kg/s", start = _Qin_BC_start) "Valve openinn (Real)"
       annotation (Placement(transformation(extent={{-124,-20},{-84,20}})));
       parameter Real _Pout_BC_start = 4500000.0
       annotation (Dialog( group="Start values for inputs "));
      protected
       Real _Pout_BC_old;
      public
       Modelica.Blocks.Interfaces.RealInput Pout_BC(unit = "Pa", nominal = 100000.0, min = 0.0, start = _Pout_BC_start)
       annotation (Placement(transformation(extent={{-124,-70},{-84,-30}})));
       Modelica.Blocks.Interfaces.RealOutput Pin_TSPro(unit = "Pa", nominal = 100000.0, min = 0.0)
       annotation (Placement(transformation(extent={{100,30},{140,70}})));
       Modelica.Blocks.Interfaces.RealOutput Qout_TSPro(unit = "kg/s")
       annotation (Placement(transformation(extent={{100,-20},{140,20}})));
       Modelica.Blocks.Interfaces.RealOutput Hout_TSPro(unit = "J/kg")
       annotation (Placement(transformation(extent={{100,-70},{140,-30}})));
      public
       parameter String fmi_instanceName="Boiler_fmu"
       annotation (Dialog(tab="FMI", group="Instance name"));
       parameter Boolean fmi_loggingOn=false
       annotation (Dialog(tab="FMI", group="Enable logging"));
       parameter Boolean fmi_InputTime=false
       "Time point of input used when calling doStep."
       annotation (Evaluate=true,Dialog(tab="FMI", group="Input Handling"),choices(choice= false "StepEnd", choice= true "StepStart"));
       parameter Boolean fmi_UsePreOnInputSignals=true
       annotation (Evaluate=true,Dialog(tab="FMI", group="Input Handling"));
       parameter Real fmi_StartTime = 0.0
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_StopTime = 1.0
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_NumberOfSteps = 500
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_CommunicationStepSize=(fmi_StopTime-fmi_StartTime)/fmi_NumberOfSteps
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Integer stepSizeScaleFactor = 1 "Number of doSteps called between two CommunicationStepSize"
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Boolean fmi_forceShutDownAtStopTime=false
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter Real fmi_rTol=1e-6 "relative tolerance for the internal solver of the fmu"
       annotation (Dialog(tab="FMI", group="Step time"));
       parameter String fmi_resourceLocation="file:///"+ModelicaServices.ExternalReferences.loadResource(
            "modelica://TANDEM/Resources/Library/FMU/Boiler/resources")
       annotation (Dialog(tab="FMI", group="Instantiation"));
      protected
       fmi_Functions.fmiModel fmi;
       Boolean fmi_exitInit(start=false,fixed=true);
       Boolean fmi_flip(start=false,fixed=true);
       parameter Real fmi_rdum(start=0,fixed=false);
       parameter Integer fmi_idum(start=0,fixed=false);
       Boolean fmi_StepOK;
       parameter Real zeroOffset = 0;
       parameter Real myTimeStart(fixed=false);
       record 'Internal '
         Real Pin_TSPro;
         Real Qout_TSPro;
         Real Hout_TSPro;
       end 'Internal ';
       'Internal ' internal;
     Real RealVariables[38];
     Real RealFixedLocal[1];
       parameter String fmi_xNames[0]= fill("",0);
       parameter Integer fmi_xVrs[0]= fill(0,0);
       parameter String fmi_dxNames[0]= fill("",0);
       parameter Integer fmi_dxVrs[0]= fill(0,0);
       parameter String fmi_uNames[3]= {"Hin_BC", "Qin_BC", "Pout_BC"};
       parameter Integer fmi_uVrs[3]= {352321536, 352321537, 352321538};
       parameter String fmi_yNames[3]= {"Pin_TSPro", "Qout_TSPro", "Hout_TSPro"};
       parameter Integer fmi_yVrs[3]= {335544320, 335544321, 335544322};
     package fmi_Functions
         class fmiModel
           extends ExternalObject;
           function constructor "Initialize FMI model"
             extends Modelica.Icons.Function;
             input String instanceName;
             input Boolean loggingOn;
             input String resourceLocation;
             output fmiModel fmi;
             external"C" fmi = Boiler59713845494939865538805_fmiInstantiateModel2(instanceName, loggingOn, resourceLocation)
             annotation(Include="
#ifndef Boiler59713845494939865538805_Instantiate_C
#define Boiler59713845494939865538805_Instantiate_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void Boiler59713845494939865538805Logger(fmi2ComponentEnvironment componentEnvironment, fmi2String instanceName, fmi2Status status,
  fmi2String category, fmi2String message, ...) {
  char msg[4096];
  char buf[4096];
  va_list ap;
  int len;
  va_start(ap,message);
#if defined(_MSC_VER) && _MSC_VER>=1200
  len = _snprintf(msg, sizeof(msg)/sizeof(*msg), \"%s: %s\", instanceName, message);
  if (len < 0) goto fail;
  len = _vsnprintf(buf, sizeof(buf)/sizeof(*buf) - 2, msg, ap);
  if (len < 0) goto fail;
#else
  len = snprintf(msg, sizeof(msg)/sizeof(*msg), \"%s: %s\", instanceName, message);
  if (len < 0) goto fail;
  len = vsnprintf(buf, sizeof(buf)/sizeof(*buf) - 2, msg, ap);
  if (len < 0) goto fail;
#endif
  if( len>0 && len < 4096 && buf[len - 1]!='\\n'){
    buf[len] = '\\n';
    buf[len + 1] = 0;
  }
  va_end(ap);
  switch (status) {
    case fmi2Fatal:
      ModelicaMessage(\"[fmi2Fatal]: \");
      break;
    case fmi2Error:
      ModelicaMessage(\"[fmi2Error]: \");
      break;
    case fmi2Discard:
      ModelicaMessage(\"[fmi2Discard]: \");
      break;
    case fmi2Warning:
      ModelicaMessage(\"[fmi2Warning]: \");
      break;
    case fmi2OK:
      ModelicaMessage(\"[fmi2OK]: \");
      break;
  }
  ModelicaMessage(buf);
  return;
fail:
  ModelicaMessage(\"Logger failed, message too long?\");
}
void * Boiler59713845494939865538805_fmiInstantiateModel2(const char*instanceName, fmi2Boolean loggingOn, fmi2String resourceLocation) {
  static fmi2CallbackFunctions funcs = {&Boiler59713845494939865538805Logger, &calloc, &free, NULL, NULL};
  struct dy_fmi2Extended* res;
  res = calloc(1, sizeof(struct dy_fmi2Extended));
  if (res!=0) {
    if (!(res->hInst=LoadLibraryW(L\"Boiler.dll\"))) {
      ModelicaError(\"Loading of FMU dynamic link library (Boiler.dll) failed!\");
      return 0;
    }
    if (!(res->dyFmiInstantiate=(fmi2InstantiateFunc)GetProcAddress(res->hInst,\"fmi2Instantiate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Instantiate!\");
      return 0;
    }
    if (!(res->dyFmiFreeInstance=(fmi2FreeInstanceFunc)GetProcAddress(res->hInst,\"fmi2FreeInstance\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2FreeInstance!\");
      return 0;
    }
    if (!(res->dyFmiSetupExperiment=(fmi2SetupExperimentFunc)GetProcAddress(res->hInst,\"fmi2SetupExperiment\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetupExperiment!\");
      return 0;
    }
    if (!(res->dyFmiEnterInitializationMode=(fmi2EnterInitializationModeFunc)GetProcAddress(res->hInst,\"fmi2EnterInitializationMode\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2EnterInitializationMode!\");
      return 0;
    }
    if (!(res->dyFmiExitInitializationMode=(fmi2ExitInitializationModeFunc)GetProcAddress(res->hInst,\"fmi2ExitInitializationMode\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2ExitInitializationMode!\");
      return 0;
    }
    if (!(res->dyFmiTerminate=(fmi2TerminateFunc)GetProcAddress(res->hInst,\"fmi2Terminate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Terminate!\");
      return 0;
    }
    if (!(res->dyFmiReset=(fmi2ResetFunc)GetProcAddress(res->hInst,\"fmi2Reset\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2Reset!\");
      return 0;
    }
    if (!(res->dyFmiSetReal=(fmi2SetRealFunc)GetProcAddress(res->hInst,\"fmi2SetReal\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetReal!\");
      return 0;
    }
    if (!(res->dyFmiGetReal=(fmi2GetRealFunc)GetProcAddress(res->hInst,\"fmi2GetReal\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetReal!\");
      return 0;
    }
    if (!(res->dyFmiSetInteger=(fmi2SetIntegerFunc)GetProcAddress(res->hInst,\"fmi2SetInteger\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetInteger!\");
      return 0;
    }
    if (!(res->dyFmiGetInteger=(fmi2GetIntegerFunc)GetProcAddress(res->hInst,\"fmi2GetInteger\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetInteger!\");
      return 0;
    }
    if (!(res->dyFmiSetBoolean=(fmi2SetBooleanFunc)GetProcAddress(res->hInst,\"fmi2SetBoolean\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetBoolean!\");
      return 0;
    }
    if (!(res->dyFmiGetBoolean=(fmi2GetBooleanFunc)GetProcAddress(res->hInst,\"fmi2GetBoolean\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetBoolean!\");
      return 0;
    }
    if (!(res->dyFmiSetDebugLogging=(fmi2SetDebugLoggingFunc)GetProcAddress(res->hInst,\"fmi2SetDebugLogging\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetDebugLogging!\");
      return 0;
    }
    if (!(res->dyFmiSetString=(fmi2SetStringFunc)GetProcAddress(res->hInst,\"fmi2SetString\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetString!\");
      return 0;
    }
    if (!(res->dyFmiGetString=(fmi2GetStringFunc)GetProcAddress(res->hInst,\"fmi2GetString\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetString!\");
      return 0;
    }
    if(!(res->dyFmiDoStep=(fmi2DoStepFunc)GetProcAddress(res->hInst,\"fmi2DoStep\"))){
      ModelicaError(\"GetProcAddress failed for fmi2DoStep!\\n The model was imported as a Co-Simulation FMU but could not load the CS specific function fmiDoStep\\n Verify that the FMU supports Co-Simulation\");
      return 0;
    }
    if(!(res->dyFmiGetBooleanStatus=(fmi2GetBooleanStatusFunc)GetProcAddress(res->hInst,\"fmi2GetBooleanStatus\"))){
      ModelicaError(\"GetProcAddress failed for fmi2GetBooleanStatus!\\n The model was imported as a Co-Simulation FMU but could not load the CS specific function fmiGetBooleanStatus\\n Verify that the FMU supports Co-Simulation\");
      return 0;
    }
    if (!(res->dyFmiGetDirectionalDerivative=(fmi2GetDirectionalDerivativeFunc)GetProcAddress(res->hInst,\"fmi2GetDirectionalDerivative\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetDirectionalDerivative!\");
      return 0;
    }
    if (!(res->dyFmiGetFMUstate=(fmi2GetFMUstateFunc)GetProcAddress(res->hInst,\"fmi2GetFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2GetFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiSetFMUstate=(fmi2SetFMUstateFunc)GetProcAddress(res->hInst,\"fmi2SetFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SetFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiFreeFMUstate=(fmi2FreeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2FreeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2FreeFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiSerializedFMUstateSize=(fmi2SerializedFMUstateSizeFunc)GetProcAddress(res->hInst,\"fmi2SerializedFMUstateSize\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SerializedFMUstateSize!\");
      return 0;
    }
    if (!(res->dyFmiSerializeFMUstate=(fmi2SerializeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2SerializeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2SerializeFMUstate!\");
      return 0;
    }
    if (!(res->dyFmiDeSerializeFMUstate=(fmi2DeSerializeFMUstateFunc)GetProcAddress(res->hInst,\"fmi2DeSerializeFMUstate\"))) {
      ModelicaError(\"GetProcAddress failed for fmi2DeSerializeFMUstate!\");
      return 0;
    }
    res->m=res->dyFmiInstantiate(instanceName, fmi2CoSimulation, \"{a597ad13-845a-49c4-93e9-db865538f805}\",resourceLocation, &funcs, fmi2False, loggingOn);
    if (0==res->m) {free(res);res=0;ModelicaError(\"InstantiateModel failed\");}
    else {res->dyTriggered=0;res->dyTime=res->dyLastTime=-1e37;res->discreteInputChanged=1;res->currentMode=dyfmi2InstantiationMode;res->dyLastStepTime=0;res->dyFMUstate=NULL;}
  }
  return res;
}
#endif",      Library="Boiler", LibraryDirectory=
                    "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
              annotation (__Dymola_CriticalRegion="Boiler");
           end constructor;

           function destructor "Release storage of FMI model"
             extends Modelica.Icons.Function;
             input fmiModel fmi;
             external"C"
                        Boiler59713845494939865538805_fmiFreeModelInstance2(fmi)
             annotation (Include="
#ifndef Boiler59713845494939865538805_Free_C
#define Boiler59713845494939865538805_Free_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void Boiler59713845494939865538805_fmiFreeModelInstance2(void*m) {
  struct dy_fmi2Extended*a=m;
  if (a) {
    if(a->dyFMUstate)
    a->dyFmiFreeFMUstate(a->m, &a->dyFMUstate);
    /*a->dyFmiSetDebugLogging(a->m,fmi2True,0,NULL);*/
    a->dyFmiTerminate(a->m);
    a->dyFmiFreeInstance(a->m);
    FreeLibrary(a->hInst);
    free(a);
  }
}
#endif",      Library="Boiler", LibraryDirectory=
                    "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
              annotation (__Dymola_CriticalRegion="Boiler");
           end destructor;
         end fmiModel;

         function fmiDoStep
         input fmiModel fmi;
         input Real currentTime;
         input Real stepSize;
         input Real preAvailable;
         output Boolean stepOK;
         output Real postAvailable=preAvailable;
         external"C" stepOK= Boiler59713845494939865538805_fmiDoStep2(fmi, currentTime, stepSize)
           annotation (Include="
#ifndef Boiler59713845494939865538805_DoStep_C
#define Boiler59713845494939865538805_DoStep_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
double Boiler59713845494939865538805_fmiDoStep2(void*m, double currentTime, double stepSize) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  fmi2Boolean value=fmi2False;
  if (a) {
    status=a->dyFmiDoStep(a->m, currentTime, stepSize, fmi2True);
    if(status==fmi2Discard){
      status = a->dyFmiGetBooleanStatus(a->m, fmi2Terminated, &value);
      if(value==fmi2True){
        terminate(\"Terminate signaled by the FMU\");
      }
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning){    ModelicaFormatError(\"The call of fmi2DoStep(%f, %f) failed in FMU: ErrorModel\\r\\nNote: setting fmi_loggingOn in the FMU component may produce more information from the FMU.\",currentTime, stepSize);  }  return 1.0;
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiDoStep;

         function fmiReset
           input fmiModel fmi;
           output Boolean resetOK;
           external"C" resetOK = Boiler59713845494939865538805_fmiReset2(fmi)
           annotation (Include="
#ifndef Boiler59713845494939865538805_Reset_C
#define Boiler59713845494939865538805_Reset_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
double Boiler59713845494939865538805_fmiReset2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiReset(a->m);
    a->currentMode=dyfmi2InstantiationMode;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiReset failed\");
    return 1.0;
  }
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiReset;

         function fmiGetRealScalar
           input fmiModel fmi;
           input Integer ref;
           output Real val;
         algorithm
             val := scalar(fmiGetReal(fmi, {ref}));
         end fmiGetRealScalar;

         function fmiGetReal
           input fmiModel fmi;
           input Integer refs[:];
           output Real vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_GetReal_C
#define Boiler59713845494939865538805_GetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiGetReal2(void*m, const int*refs, size_t nrefs, double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetReal failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetReal;

         function fmiGetRealwf
           input fmiModel fmi;
           input Integer refs[:];
           input Real preAvailable;
           output Real vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_GetReal_C
#define Boiler59713845494939865538805_GetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiGetReal2(void*m, const int*refs, size_t nrefs, double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetReal failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetRealwf;

         function fmiSetReal
           input fmiModel fmi;
           input Integer refs[:];
           input Real vals[size(refs, 1)];
           external"C"
                      Boiler59713845494939865538805_fmiSetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef Boiler59713845494939865538805_SetReal_C
#define Boiler59713845494939865538805_SetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetReal2(void*m, const int*refs, size_t nrefs, const double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetReal failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetReal;

         function fmiSetRealwf
           input fmiModel fmi;
           input Integer refs[:];
           input Real vals[size(refs, 1)];
           input Real preAvailable;
           output Real postAvailable=preAvailable;
           external"C"
                      Boiler59713845494939865538805_fmiSetReal2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef Boiler59713845494939865538805_SetReal_C
#define Boiler59713845494939865538805_SetReal_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetReal2(void*m, const int*refs, size_t nrefs, const double*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetReal(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetReal failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetRealwf;

         function fmiGetIntegerScalar
           input fmiModel fmi;
           input Integer ref;
           output Integer val;
         algorithm
             val := scalar(fmiGetInteger(fmi, {ref}));
         end fmiGetIntegerScalar;

         function fmiGetInteger
           input fmiModel fmi;
           input Integer refs[:];
           output Integer vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_GetInteger_C
#define Boiler59713845494939865538805_GetInteger_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiGetInteger2(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetInteger;

         function fmiGetIntegerwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer preAvailable;
           output Integer vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_GetInteger_C
#define Boiler59713845494939865538805_GetInteger_C 1
#include \"FMI/fmi2Import.h\"
#include <stdlib.h>
void Boiler59713845494939865538805_fmiGetInteger2(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetIntegerwf;

         function fmiSetInteger
           input fmiModel fmi;
           input Integer refs[:];
           input Integer vals[size(refs, 1)];
          protected
           Integer oldVals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiSetInteger2(
             fmi,
             refs,
             size(refs, 1),
             vals,
             oldVals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_SetInteger_C
#define Boiler59713845494939865538805_SetInteger_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetInteger2(void*m, const int*refs, size_t nrefs, int*vals, int*oldVals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    if(!a->discreteInputChanged){
      status=a->dyFmiGetInteger(a->m, refs, nrefs, oldVals);
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetInteger failed in SetInteger\");
      for( i = 0; i < nrefs; ++i){
        if(oldVals[i] != vals[i]){
          a->discreteInputChanged = 1;
          break;
        }
      }
    }
    if(a->discreteInputChanged){
      if(a->currentMode == dyfmi2ContinuousTimeMode){
        status = a->dyFmiEnterEventMode(a->m);
        if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
        a->currentMode = dyfmi2EventMode;
      }
      status=a->dyFmiSetInteger(a->m, refs, nrefs, vals);
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetInteger failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetInteger;

         function fmiSetIntegerwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer vals[size(refs, 1)];
           input Integer preAvailable;
           output Integer postAvailable=preAvailable;
           external"C" Boiler59713845494939865538805_fmiSetInteger2wf(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_SetIntegerwf_C
#define Boiler59713845494939865538805_SetIntegerwf_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetInteger2wf(void*m, const int*refs, size_t nrefs, int*vals) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    status=a->dyFmiSetInteger(a->m, refs, nrefs, vals);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetInteger failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetIntegerwf;

         function fmiGetBooleanScalar
           input fmiModel fmi;
           input Integer ref;
           output Boolean val;
         algorithm
             val := scalar(fmiGetBoolean(fmi, {ref}));
         end fmiGetBooleanScalar;

         function fmiGetBoolean
           input fmiModel fmi;
           input Integer refs[:];
           output Boolean vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef Boiler59713845494939865538805_GetBoolean_C
#define Boiler59713845494939865538805_GetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiGetBoolean2(void*m, const int* refs, size_t nr, int* vals) {
  int i;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(vals));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed\");
    for(i=nr-1;i>=0;i--) vals[i]=((fmi2Boolean*)(vals))[i];
  }
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetBoolean;

         function fmiGetBooleanwf
           input fmiModel fmi;
           input Integer refs[:];
           input Integer preAvailable;
           output Boolean vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiGetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals)
             annotation (Include="
#ifndef Boiler59713845494939865538805_GetBoolean_C
#define Boiler59713845494939865538805_GetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiGetBoolean2(void*m, const int* refs, size_t nr, int* vals) {
  int i;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(vals));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed\");
    for(i=nr-1;i>=0;i--) vals[i]=((fmi2Boolean*)(vals))[i];
  }
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetBooleanwf;

         function fmiSetBoolean
           input fmiModel fmi;
           input Integer refs[:];
           input Boolean vals[size(refs, 1)];
          protected
           Boolean dummy[size(refs, 1)];
           Boolean oldVals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiSetBoolean2(
             fmi,
             refs,
             size(refs, 1),
             vals,
             dummy,
             oldVals)
             annotation (Include="
#ifndef Boiler59713845494939865538805_SetBoolean_C
#define Boiler59713845494939865538805_SetBoolean_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetBoolean2(void*m, const int* refs, size_t nr, const int* vals, int* dummy, int* oldVals) {
  size_t i;
  int j;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nr){return;}
  for(i=0;i<nr;++i) ((fmi2Boolean*)(dummy))[i]=vals[i];
  if (a) {
    if(!a->discreteInputChanged){
      status=a->dyFmiGetBoolean(a->m, refs, nr, (fmi2Boolean*)(oldVals));
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"GetBoolean failed in SetBoolean\");
      for(j=nr-1;j>=0;j--){
        oldVals[j]=((fmi2Boolean*)(oldVals))[j];
        if(oldVals[j] != dummy[j]){
          a->discreteInputChanged = 1;
          break;
        }
      }
    }
    if(a->discreteInputChanged){
      if(a->currentMode == dyfmi2ContinuousTimeMode){
        status = a->dyFmiEnterEventMode(a->m);
        if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
        a->currentMode = dyfmi2EventMode;
      }
      status=a->dyFmiSetBoolean(a->m, refs, nr, (fmi2Boolean*)(dummy));
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetBoolean failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetBoolean;

         function fmiSetString
           input fmiModel fmi;
           input Integer refs[:];
           input String vals[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiSetString2(
             fmi,
             refs,
             size(refs, 1),
             vals)
           annotation (Include="
#ifndef Boiler59713845494939865538805_SetString_C
#define Boiler59713845494939865538805_SetString_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetString2(void*m, const int*refs, size_t nrefs,const fmi2String vals[]) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  size_t i = 0;
  if(!nrefs){return;}
  if (a) {
    if(a->currentMode == dyfmi2ContinuousTimeMode){
      status = a->dyFmiEnterEventMode(a->m);
      if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiEnterEventModeFailed\");
      a->currentMode = dyfmi2EventMode;
    }
    status=a->dyFmiSetString(a->m, refs, nrefs, vals);
    a->discreteInputChanged = fmi2True;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetString failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetString;

         function fmiSetBooleanwf
           input fmiModel fmi;
           input Integer refs[:];
           input Boolean vals[size(refs, 1)];
           input Integer preAvailable;
           output Integer postAvailable=preAvailable;
          protected
           Boolean dummy[size(refs, 1)];
           external"C" Boiler59713845494939865538805_fmiSetBoolean2wf(
             fmi,
             refs,
             size(refs, 1),
             vals,
             dummy)
             annotation (Include="
#ifndef Boiler59713845494939865538805_SetBooleanwf_C
#define Boiler59713845494939865538805_SetBooleanwf_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSetBoolean2wf(void*m, const int* refs, size_t nr, const int* vals, int* dummy) {
  size_t i;
  int j;
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if(!nr){return;}
  for(i=0;i<nr;++i) ((fmi2Boolean*)(dummy))[i]=vals[i];
  if (a) {
    status=a->dyFmiSetBoolean(a->m, refs, nr, (fmi2Boolean*)(dummy));
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"SetBoolean failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (
              __Dymola_CriticalRegion="Boiler",
              __Dymola_IdemPotent=true,
              __Dymola_VectorizedExceptFirst=true);
         end fmiSetBooleanwf;

         function fmiGetDirectionalDerivative
           input fmiModel fmi;
           input Integer z_refs[:];
           input Integer v_refs[:];
           input Real dv[size(v_refs, 1)];
           output Real dz[size(z_refs, 1)];
           external"C" Boiler59713845494939865538805_GetDirectionalDerivative2(
             fmi,
             z_refs,
             size(z_refs, 1),
             v_refs,
             size(v_refs, 1),
             dv,
             dz)
           annotation (Include="
#ifndef Boiler59713845494939865538805_GetDirectionalDerivative2_C
#define Boiler59713845494939865538805_GetDirectionalDerivative2_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_GetDirectionalDerivative2(void*m, const int* zref, size_t nzr, const int* vrefs, size_t nvr, const double *dv, double *dz) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiGetDirectionalDerivative(a->m, zref, nzr, vrefs, nvr, dv, dz);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiGetDirectionalDerivative failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiGetDirectionalDerivative;

         function GetRealVariable
           input fmiModel fmi;
           input Real Time;
           input Real realInputs[:];
           input Real realLinearDependentInputs[:];
           input Real states[:];
           input Real linearDependentStates[:];
           input Real linearOffset=0;
           input Boolean booleanInputs[:];
           input Integer integerInputs[:];
           //input String stringInputs[:];
           input Integer realInputValueReferences[:];
           input Integer realLinearDependentInputsValueReferences[:];
           input Integer statesValueRefernces[:];
           input Integer booleanInputValueReferences[:];
           input Integer integerInputValueReferences[:];
           //input Integer stringInputValueReferences[:];
           input Integer outputValueReference[:];
           output Real outputVariable;
         algorithm
           //fmi_Functions.fmiSetTime(fmi, Time);
           fmiSetReal(fmi,realInputValueReferences,realInputs);
           fmiSetReal(fmi,realLinearDependentInputsValueReferences,realLinearDependentInputs);
           fmiSetBoolean(fmi,booleanInputValueReferences,booleanInputs);
           fmiSetInteger(fmi,integerInputValueReferences,integerInputs);
           //SetString(fmi,stringInputValueReferences,stringInputs);
           outputVariable:=fmiGetRealScalar(fmi,outputValueReference[1]);
           annotation(derivative(noDerivative=realLinearDependentInputs,noDerivative=linearDependentStates)=derGetRealVariable, LateInline=true);
         end GetRealVariable;

         function derGetRealVariable
           input fmiModel fmi;
           input Real Time;
           input Real realInputs[:];
           input Real realLinearDependentInputs[:];
           input Real states[:];
           input Real linearDependentStates[:];
           input Real linearOffset=0;
           input Boolean booleanInputs[:];
           input Integer integerInputs[:];
           //input String stringInputs[:];
           input Integer realInputValueReferences[:];
           input Integer realLinearDependentInputsValueReferences[:];
           input Integer statesValueRefernces[:];
           input Integer booleanInputValueReferences[:];
           input Integer integerInputValueReferences[:];
           //input Integer stringInputValueReferences[:];
           input Integer outputValueReference[:];
           input Real derRealInputs[:];
           input Real derStates[:];
           input Real derLinearOffsets;
           output Real derOutputVariable;
          protected
           Real dummy[1];
         algorithm
           //fmi_Functions.fmiSetTime(fmi, Time);
           fmiSetReal(fmi,realInputValueReferences,realInputs);
           fmiSetReal(fmi,realLinearDependentInputsValueReferences,realLinearDependentInputs);
           fmiSetBoolean(fmi,booleanInputValueReferences,booleanInputs);
           fmiSetInteger(fmi,integerInputValueReferences,integerInputs);
           //setString(fmi,stringInputValueReferences,stringInputs);
           dummy:=fmiGetDirectionalDerivative(fmi, outputValueReference,  cat(1,realInputValueReferences,statesValueRefernces), cat(1,derRealInputs,derStates));
           derOutputVariable:=dummy[1]+derLinearOffsets;
           annotation(LateInline=true);
         end derGetRealVariable;

         function linearizeFMU
           input fmiModel fmi;
           input Integer xVr[:];
           input Integer dxVr[:];
           input Integer uVr[:];
           input Integer yVr[:];
           output Real A[size(dxVr, 1), size(xVr, 1)];
           output Real B[size(dxVr, 1), size(uVr, 1)];
           output Real C[size(yVr, 1), size(xVr, 1)];
           output Real D[size(yVr, 1), size(uVr, 1)];
          protected
           parameter Integer nD = size(dxVr, 1);
           parameter Integer nY = size(yVr, 1);
           parameter Integer nU = size(uVr, 1);
           parameter Integer nX = size(xVr, 1);
           parameter Integer zRef[nD + nY] = cat(1, dxVr, yVr);
           Real vec[nD + nY];
           parameter Real one[1] = { 1.0};
         algorithm
           for i in 1:nX loop
             vec := fmiGetDirectionalDerivative(
               fmi,zRef,{ xVr[i]}, one);
             A[:, i] := vec[1:nD];
             C[:, i] := vec[nD + 1:end];
           end for;
           for i in 1:nU loop
             vec := fmiGetDirectionalDerivative(
             fmi,zRef,{ uVr[i]}, one);
             B[:, i] := vec[1:nD];
             D[:, i] := vec[nD + 1:end];
           end for;
         end linearizeFMU;

         function fmiSaveFMUState
           input fmiModel fmi;
           external"C" Boiler59713845494939865538805_fmiSaveFMUState2(fmi)
             annotation (Include="
#ifndef Boiler59713845494939865538805_fmiSaveFMUState_C
#define Boiler59713845494939865538805_fmiSaveFMUState_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSaveFMUState2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiGetFMUstate(a->m, &a->dyFMUstate);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiGetFMUstate failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiSaveFMUState;

         function fmiRestoreFMUState
           input fmiModel fmi;
           external"C" Boiler59713845494939865538805_fmiRestoreFMUState2(fmi)
             annotation (Include="
#ifndef Boiler59713845494939865538805_fmiRestoreFMUState_C
#define Boiler59713845494939865538805_fmiRestoreFMUState_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiRestoreFMUState2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiSetFMUstate(a->m, a->dyFMUstate);
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiSetFMUstate failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiRestoreFMUState;

         function fmiSerializeFMUstate
           input fmiModel fmi;
           external"C" Boiler59713845494939865538805_fmiSerializeFMUstate2(fmi)
             annotation (Include="
#ifndef Boiler59713845494939865538805_fmiSerializeFMUstate_C
#define Boiler59713845494939865538805_fmiSerializeFMUstate_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiSerializeFMUstate2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status = a->dyFmiGetFMUstate(a->m, &a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiGetFMUstate failed\");
    status = a->dyFmiSerializedFMUstateSize(a->m, a->dyFMUstate, &a->dyFMUStateSize);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSerializedFMUstateSize failed\");
    if( a->dySerializeFMUstate) free(a->dySerializeFMUstate); a->dySerializeFMUstate = NULL;
    a->dySerializeFMUstate = malloc(a->dyFMUStateSize);
    if(!a->dySerializeFMUstate)  ModelicaError(\"malloc call to allocate SerializeFMUstate failed\");
    status = a->dyFmiSerializeFMUstate(a->m, a->dyFMUstate, a->dySerializeFMUstate, a->dyFMUStateSize);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSerializeFMUstate failed\");
  }
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiSerializeFMUstate;

         function fmiDeSerializeFMUstate
           input fmiModel fmi;
           external"C" Boiler59713845494939865538805_fmiDeSerializeFMUstate2(fmi)
             annotation (Include="
#ifndef Boiler59713845494939865538805_fmiDeSerializeFMUstate_C
#define Boiler59713845494939865538805_fmiDeSerializeFMUstate_C 1
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiDeSerializeFMUstate2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    if(!a->dySerializeFMUstate) ModelicaError(\"serializedFmuState is not allocated!!!\");
    if(a->dyFMUstate){
      a->dyFmiFreeFMUstate(a->m, &a->dyFMUstate);
      a->dyFMUstate = NULL;    }
    status = a->dyFmiDeSerializeFMUstate(a->m, a->dySerializeFMUstate, a->dyFMUStateSize, &a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiDeSerializeFMUstate failed\");
    status = a->dyFmiSetFMUstate(a->m, a->dyFMUstate);
    if (status != fmi2OK && status != fmi2Warning) ModelicaError(\"fmiSetFMUstate failed\");
  }
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiDeSerializeFMUstate;

         function fmiEnterSlaveInitializationMode
           input fmiModel fmi;
           input Real relativeTolerance;
           input Real tStart;
           input Boolean forceShutDownAtTStop;
           input Real tStop;
           input Real preAvailable;
           output Real postAvailable = preAvailable;
           external"C" Boiler59713845494939865538805_fmiEnterSlaveInitializationMode2(fmi, relativeTolerance, tStart, forceShutDownAtTStop, tStop)
           annotation (Include="
#ifndef Boiler59713845494939865538805_fmiEnterSlaveInitializationMode_C
#define Boiler59713845494939865538805_fmiEnterSlaveInitializationMode_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiEnterSlaveInitializationMode2(void*m, double relativeTolerance, double tStart, int forceShutDownAtTStop, double tStop) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    if(a->currentMode==dyfmi2InstantiationMode){
      status=a->dyFmiSetupExperiment(a->m, fmi2True, relativeTolerance, tStart, forceShutDownAtTStop, tStop);
      status=a->dyFmiEnterInitializationMode(a->m);
      a->dyTriggered=0;
      a->dyLastTime=a->dyTime;
      a->currentMode=dyfmi2InitializationMode;
    }else{
      status=fmi2OK;
    }
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"InitializeSlave failed\");
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiEnterSlaveInitializationMode;

         function fmiExitSlaveInitializationMode
           input fmiModel fmi;
           input Real preAvailable;
           output Real postAvailable = preAvailable;
           external"C" Boiler59713845494939865538805_fmiExitSlaveInitializationMode2(fmi)
           annotation (Include="
#ifndef Boiler59713845494939865538805_fmiExitSlaveInitializationMode_C
#define Boiler59713845494939865538805_fmiExitSlaveInitializationMode_C 1
#include <stdlib.h>
#include \"FMI/fmi2Import.h\"
void Boiler59713845494939865538805_fmiExitSlaveInitializationMode2(void*m) {
  struct dy_fmi2Extended*a=m;
  fmi2Status status=fmi2Fatal;
  if (a) {
    status=a->dyFmiExitInitializationMode(a->m);
    a->dyTriggered=0;
    a->dyLastTime=a->dyTime;
    a->currentMode = dyfmi2EventMode;
  }
  if (status!=fmi2OK && status!=fmi2Warning) ModelicaError(\"fmiExitModelInitialization failed!\");
  return;
}
#endif",      Library="Boiler", LibraryDirectory=
                  "modelica://TANDEM/Resources/Library/FMU/Boiler/binaries");
            annotation (__Dymola_CriticalRegion="Boiler");
         end fmiExitSlaveInitializationMode;
     end fmi_Functions;
     initial equation
     equation
       when initial() then
         fmi = fmi_Functions.fmiModel(fmi_instanceName, fmi_loggingOn, fmi_resourceLocation);
       end when;
     initial algorithm
       fmi_Functions.fmiSetReal(fmi, {16777216, 16777217, 16777218, 16777220, 16777221, 16777222, 16777223, 16777224, 16777225, 16777226, 16777227, 16777228}, {e_Boiler.W, e_Boiler.eta, e_Boiler.deltaP, InputPower_Ramp.Starttime, InputPower_Ramp.Duration, InputPower_Ramp.Initialvalue, InputPower_Ramp.Finalvalue, sourceQ_4FMU.Q0, sourceQ_4FMU.h0, sinkP_4FMU.P0, sinkP_4FMU.T0, sinkP_4FMU.h0});
       fmi_Functions.fmiSetInteger(fmi, {16777219, 16777229, 16777230}, {e_Boiler.mode, sinkP_4FMU.option_temperature, sinkP_4FMU.mode});
         fmi_Functions.fmiSetReal(fmi, {352321536}, {_Hin_BC_start});
         fmi_Functions.fmiSetReal(fmi, {352321537}, {_Qin_BC_start});
         fmi_Functions.fmiSetReal(fmi, {352321538}, {_Pout_BC_start});
         fmi_Functions.fmiEnterSlaveInitializationMode(fmi, fmi_rTol, fmi_StartTime, fmi_forceShutDownAtStopTime, fmi_StopTime, 1);
       fmi_rdum := 1;
       fmi_idum := 1;
       myTimeStart :=time;
     algorithm
     assert(fmi_CommunicationStepSize > 0.0,"The parameter fmi_CommunicationStepSize has an invalid value, please set a positive value larger than 0.0");
       when {initial(), sample(fmi_StartTime, fmi_CommunicationStepSize)} then
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321536}, {_Hin_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321536}, {pre(Hin_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321536}, {Hin_BC});
           end if;
         end if;
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321537}, {_Qin_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321537}, {pre(Qin_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321537}, {Qin_BC});
           end if;
         end if;
         if fmi_InputTime then
           fmi_Functions.fmiSetReal(fmi, {352321538}, {_Pout_BC_old});
         else
           if fmi_UsePreOnInputSignals then
             fmi_Functions.fmiSetReal(fmi, {352321538}, {pre(Pout_BC)});
           else
             fmi_Functions.fmiSetReal(fmi, {352321538}, {Pout_BC});
           end if;
         end if;
           if fmi_rdum >= 0  and fmi_idum >= 0 and not fmi_exitInit and not initial() then
             fmi_Functions.fmiExitSlaveInitializationMode(fmi, 1);
             fmi_exitInit:=true;
     RealFixedLocal :=fmi_Functions.fmiGetRealwf(
                 fmi,
                 {234881036},
                 fmi_rdum);
           end if;
         if time>=fmi_CommunicationStepSize +fmi_StartTime then
           for stepSizeIndex in 1:stepSizeScaleFactor loop
             fmi_StepOK :=fmi_Functions.fmiDoStep(
                   fmi,
                   time + (stepSizeIndex - 1 - stepSizeScaleFactor)*
                  fmi_CommunicationStepSize/stepSizeScaleFactor,
                   fmi_CommunicationStepSize/stepSizeScaleFactor,
                   1);
           end for;
           fmi_flip :=not pre(fmi_flip);
         end if;
         if not initial() then
           internal.Pin_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544320);
           internal.Qout_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544321);
           internal.Hout_TSPro :=fmi_Functions.fmiGetRealScalar(fmi, 335544322);
         end if;
     RealVariables :=fmi_Functions.fmiGetReal(fmi, {905969673,905969683,
            436207617,369098752,369098760,369098753,436207616,369098754,
            905969669,905969674,905969675,905969676,905969677,905969678,
            905969679,905969680,905969681,905969682,905969684,905969685,
            905969686,905969687,905969688,905969689,905969690,905969691,
            905969692,905969695,905969696,905969697,905969698,905969699,
            905969700,905969701,905969702,905969703,905969704,436207618});
       end when;
       when {pre(fmi_flip), not pre(fmi_flip)} then
         _Hin_BC_old := pre(Hin_BC);
         _Qin_BC_old := pre(Qin_BC);
         _Pout_BC_old := pre(Pout_BC);
       end when;
     equation
       if initial() then
         Pin_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, {e_Boiler.deltaP, Pout_BC}, fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, fill(false,0), fill(0,0), {16777218, 352321538}, fill(0,0), fill(0,0), fill(0,0), fill(0,0), {335544320});
       else
         Pin_TSPro = internal.Pin_TSPro;
       end if;
       if initial() then
         Qout_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, {Qin_BC}, fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, fill(false,0), fill(0,0), {352321537}, fill(0,0), fill(0,0), fill(0,0), fill(0,0), {335544321});
       else
         Qout_TSPro = internal.Qout_TSPro;
       end if;
       if initial() then
         Hout_TSPro = fmi_Functions.GetRealVariable(fmi,myTimeStart, {e_Boiler.eta, InputPower_Ramp.Starttime, InputPower_Ramp.Duration, InputPower_Ramp.Initialvalue, InputPower_Ramp.Finalvalue, Hin_BC, Qin_BC, sinkP_4FMU.h0}, fill(0.0,0), fill(0.0,0), fill(0.0,0), zeroOffset, fill(false,0), fill(0,0), {16777217, 16777220, 16777221, 16777222, 16777223, 352321536, 352321537, 16777228}, fill(0,0), fill(0,0), fill(0,0), fill(0,0), {335544322});
       else
         Hout_TSPro = internal.Hout_TSPro;
       end if;
       e_Boiler.Te = RealVariables[1];
       e_Boiler.Ts = RealVariables[2];
       e_Boiler.Q = RealVariables[3];
       e_Boiler.deltaH = RealVariables[4];
       e_Boiler.We = RealVariables[5];
       e_Boiler.Ce.P = RealVariables[6];
       e_Boiler.Ce.h_vol = RealVariables[7];
       e_Boiler.Ce.h = RealVariables[8];
       e_Boiler.Cs.P = RealVariables[9];
       e_Boiler.Prop_e.d = RealVariables[10];
       e_Boiler.Prop_e.u = RealVariables[11];
       e_Boiler.Prop_e.s = RealVariables[12];
       e_Boiler.Prop_e.cp = RealVariables[13];
       e_Boiler.Prop_e.ddhp = RealVariables[14];
       e_Boiler.Prop_e.ddph = RealVariables[15];
       e_Boiler.Prop_e.duph = RealVariables[16];
       e_Boiler.Prop_e.duhp = RealVariables[17];
       e_Boiler.Prop_e.x = RealVariables[18];
       e_Boiler.Prop_s.d = RealVariables[19];
       e_Boiler.Prop_s.u = RealVariables[20];
       e_Boiler.Prop_s.s = RealVariables[21];
       e_Boiler.Prop_s.cp = RealVariables[22];
       e_Boiler.Prop_s.ddhp = RealVariables[23];
       e_Boiler.Prop_s.ddph = RealVariables[24];
       e_Boiler.Prop_s.duph = RealVariables[25];
       e_Boiler.Prop_s.duhp = RealVariables[26];
       e_Boiler.Prop_s.x = RealVariables[27];
       sinkP_4FMU.T = RealVariables[28];
       sinkP_4FMU.pro.d = RealVariables[29];
       sinkP_4FMU.pro.u = RealVariables[30];
       sinkP_4FMU.pro.s = RealVariables[31];
       sinkP_4FMU.pro.cp = RealVariables[32];
       sinkP_4FMU.pro.ddhp = RealVariables[33];
       sinkP_4FMU.pro.ddph = RealVariables[34];
       sinkP_4FMU.pro.duph = RealVariables[35];
       sinkP_4FMU.pro.duhp = RealVariables[36];
       sinkP_4FMU.pro.x = RealVariables[37];
       sinkP_4FMU.IPressure.signal = RealVariables[38];
       e_Boiler.Cs.h_vol = RealFixedLocal[1];
     //alias Declarations
       sinkP_4FMU.h = e_Boiler.Cs.h_vol;
       sinkP_4FMU.ISpecificEnthalpy.signal = e_Boiler.Cs.h_vol;
       sinkP_4FMU.C.h_vol = e_Boiler.Cs.h_vol;
       sourceQ_4FMU.OPressure.signal = Pin_TSPro;
       Pin_calc.y = Pin_TSPro;
       Pin_calc.inputReal.signal = Pin_TSPro;
       e_Boiler.Cs.h = Hout_TSPro;
       sinkP_4FMU.C.h = Hout_TSPro;
       sinkP_4FMU.OSpecificEnthalpy.signal = Hout_TSPro;
       Hout_calc.y = Hout_TSPro;
       Hout_calc.inputReal.signal = Hout_TSPro;
       sourceQ_4FMU.P = e_Boiler.Ce.P;
       sourceQ_4FMU.C.P = e_Boiler.Ce.P;
       sourceQ_4FMU.C.h = e_Boiler.Ce.h;
       e_Boiler.Signal_Elec.signal = e_Boiler.We;
       InputPower_Ramp.y.signal = e_Boiler.We;
       Hin_set.u = e_Boiler.Ce.h_vol;
       Hin_set.outputReal.signal = e_Boiler.Ce.h_vol;
       sourceQ_4FMU.h = e_Boiler.Ce.h_vol;
       sourceQ_4FMU.ISpecificEnthalpy.signal = e_Boiler.Ce.h_vol;
       sourceQ_4FMU.C.h_vol = e_Boiler.Ce.h_vol;
       e_Boiler.Ce.Q = e_Boiler.Q;
       e_Boiler.Cs.Q = e_Boiler.Q;
       Qin_set.u = e_Boiler.Q;
       Qin_set.outputReal.signal = e_Boiler.Q;
       sourceQ_4FMU.Q = e_Boiler.Q;
       sourceQ_4FMU.IMassFlow.signal = e_Boiler.Q;
       sourceQ_4FMU.C.Q = e_Boiler.Q;
       sinkP_4FMU.Q = e_Boiler.Q;
       sinkP_4FMU.C.Q = e_Boiler.Q;
       sinkP_4FMU.OFlowRate.signal = e_Boiler.Q;
       Qout_calc.y = e_Boiler.Q;
       Qout_calc.inputReal.signal = e_Boiler.Q;
       Pout_calc.u = sinkP_4FMU.IPressure.signal;
       Pout_calc.outputReal.signal = sinkP_4FMU.IPressure.signal;
       sinkP_4FMU.P = e_Boiler.Cs.P;
       sinkP_4FMU.C.P = e_Boiler.Cs.P;
       e_Boiler.Prop_e.T = e_Boiler.Te;
       e_Boiler.Prop_s.T = e_Boiler.Ts;
       sinkP_4FMU.pro.T = sinkP_4FMU.T;
       annotation (__Dymola_FMUImportVersion="Dymola 2021x", __Dymola_FMUImportPath="C:/Users/NA155421/Desktop/FMU_WorkingDirectory/Boiler.fmu", __Dymola_FMUImportIncludeAllVariables="true", __Dymola_FMUImportIntegrate="false", experiment(StartTime=0.0, StopTime=1.0, Tolerance=1E-06),
         Icon(graphics={
           Text(extent={{-150,150},{150,110}},
             lineColor={0,0,255},
             textString="%name"),
           Text(extent={{-150,-110},{150,-150}},
             lineColor={95,95,95},
             textString="FMI 2.0 CS")}),
     Documentation(info="<html>
<h4>ModelDescription Attributes</h4>
<ul>
<li>fmiVersion = 2.0</li>
<li>modelName = Boiler</li>
<li>generationTool = Dymola Version 2021x (64-bit), 2020-10-09 (requires license to execute)</li>
<li>generationDateAndTime = 2023-12-01T08:38:24Z</li>
</ul>
<p><br><b>Model Exchange Attributes</b></p>
<ul>
<li>needsExecutionTool = false</li>
<li>completedIntegratorStepNotNeeded = false</li>
<li>canBeInstantiatedOnlyOncePerProcess = false</li>
<li>canNotUseMemoryManagementFunctions = false</li>
<li>canGetAndSetFMUstate = true</li>
<li>canSerializeFMUstate = true</li>
<li>providesDirectionalDerivative = true</li>
</ul>
<p><br><b>Co-Simulation Attributes</b></p>
<ul>
<li>needsExecutionTool = false</li>
<li>canHandleVariableCommunicationStepSize = true</li>
<li>canInterpolateInputs = true</li>
<li>maxOutputDerivativeOrder = 1</li>
<li>canRunAsynchronuously = false</li>
<li>canBeInstantiatedOnlyOncePerProcess = false</li>
<li>canNotUseMemoryManagementFunctions = false</li>
<li>canGetAndSetFMUstate = true</li>
<li>canSerializeFMUstate = true</li>
<li>providesDirectionalDerivative = true</li>
</ul>
</html>"));
     end Boiler_fmu;

      model FMUs_Coupling
          BOP_fmu bOP_fmu
          annotation (Placement(transformation(extent={{10,14},{30,34}})));
        Boiler_fmu boiler_fmu
          annotation (Placement(transformation(extent={{-52,16},{-32,36}})));
      equation
        connect(boiler_fmu.Qout_TSPro, bOP_fmu.Qin_BC) annotation (Line(
              points={{-30,26},{12,26},{12,24},{9.6,24}},  color={0,0,127}));
        connect(boiler_fmu.Qin_BC, bOP_fmu.Qout_TSPro) annotation (Line(
              points={{-52.4,26},{-46,26},{-46,-16},{54,-16},{54,24},{32,24}},
                      color={0,0,127}));
        connect(boiler_fmu.Pin_TSPro, bOP_fmu.Pout_BC) annotation (Line(
              points={{-30,31},{-6,31},{-6,36},{2,36},{2,19},{9.6,19}},
              color={0,0,127}));
        connect(boiler_fmu.Hout_TSPro, bOP_fmu.Hin_BC) annotation (Line(
              points={{-30,21},{-2,21},{-2,6},{8,6},{8,29},{9.6,29}},
              color={0,0,127}));
        connect(bOP_fmu.Hout_TSPro, boiler_fmu.Hin_BC) annotation (Line(
              points={{32,19},{46,19},{46,26},{48,26},{48,56},{-54,56},{-54,31},
                {-52.4,31}},         color={0,0,127}));
        connect(boiler_fmu.Pout_BC, bOP_fmu.Pin_TSPro) annotation (Line(
              points={{-52.4,21},{-64,21},{-64,-34},{70,-34},{70,29},{32,29}},
                      color={0,0,127}));
        annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
            Diagram(coordinateSystem(preserveAspectRatio=false)),
          Documentation(info="<html>
<p>Scenario: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\"><tr>
<td><p>1000s steady state (needed by Cathare to reach steady state)</p></td>
<td><p>steady state: SG = full-load ; e-Grid : full power</p></td>
</tr>
<tr>
<td><p>100s ramp</p></td>
<td><p>cogeneration ramp</p></td>
</tr>
<tr>
<td><p>200s steady state</p></td>
<td><p>steady state: SG = full load ; cogeneration: ~10&percnt; of SG power; e-grid: balance </p></td>
</tr>
<tr>
<td><p>100s ramp</p></td>
<td><p>electrical load following</p></td>
</tr>
<tr>
<td><p>xx steady state</p></td>
<td><p>steady state: SG = 90&percnt; of full load ; cogeneration: &gt;10&percnt; of SG power; e-grid: balance</p></td>
</tr>
</table>
</html>"));
      end FMUs_Coupling;
    end Demo_FMU_Coupling;
  end WorkingDirectory;
  annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"));
end FMU_Coupling;
