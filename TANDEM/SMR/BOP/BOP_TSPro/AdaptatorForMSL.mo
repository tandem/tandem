within TANDEM.SMR.BOP.BOP_TSPro;
package AdaptatorForMSL "Adaptators to connect TSPro to MSL"
  package Fluid "Fluid adaptators to connect TSPro to MSL"
    model Fluid2TSPro "A Modelica.Fluid to TSPro connector, water-steam"

    // IDNES //

      replaceable package ModelicaMedium =
          Modelica.Media.Water.WaterIF97_ph                                  "Medium model of the MSL part"
                                       annotation (choicesAllMatching=true, Dialog(group="Fundamental Definitions"));

    //  parameter TILMedia.VLEFluidTypes.BaseVLEFluid ClaRaMedium = simCenter.fluid1 "Medium in the component"
      Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
            ModelicaMedium)
        annotation (Placement(transformation(extent={{-108,-12},{-88,8}}),
            iconTransformation(extent={{-108,-10},{-88,10}})));
      // ClaRa.Basics.Interfaces.FluidPortOut steam_a(Medium=ClaRaMedium)                 annotation (Placement(
      //       transformation(extent={{90,-9.9},{109,9.9}}), iconTransformation(extent=
      //         {{90,-10},{109,9.95}})));
      ThermoSysPro.WaterSteam.Connectors.FluidOutlet steam_outlet annotation (Placement(
             transformation(extent={{90,-9.9},{109,9.9}}), iconTransformation(extent=
              {{90,-10},{109,9.95}})));

    equation
     // port_a.m_flow+steam_a.m_flow = 0;
      //port_a.h_outflow = inStream(steam_a.h_outflow);
     // inStream(port_a.h_outflow) = steam_a.h_outflow;

     // port_a.p = steam_a.p;
     // inStream(port_a.Xi_outflow) =steam_a.xi_outflow;
     // port_a.Xi_outflow = inStream(steam_a.xi_outflow);

     // port_a.C_outflow=zeros(ModelicaMedium.nC);

    port_a.p = steam_outlet.P;
    port_a.h_outflow = steam_outlet.h_vol;
    port_a.m_flow = steam_outlet.Q;
    //steam_outlet.h =steam_outlet.h_vol;
    0 = if (port_a.m_flow > 0) then inStream(port_a.h_outflow) - steam_outlet.h else steam_outlet.h - steam_outlet.h_vol;

    port_a.C_outflow=zeros(ModelicaMedium.nC);
    port_a.Xi_outflow=zeros(ModelicaMedium.nC);

        annotation(choices(choice=simCenter.fluid1 "First fluid defined in global simCenter",
                           choice=simCenter.fluid2 "Second fluid defined in global simCenter",
                           choice=simCenter.fluid3 "Third fluid defined in global simCenter"),
                                                              Dialog(group="Fundamental Definitions"),
                  defaultComponentName =     "fluid2TSPro",
              Icon(graphics={Polygon(
              points={{-96,10},{2,10},{36,-10},{-98,-10},{-96,10}},
              lineColor={0,0,0},
              fillColor={37,219,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{100,10},{100,10},{-20,10},{-20,10},{-12,0},{0,0},{
                  12,0},{20,-10},{20,-10},{100,-10},{100,-10},{100,10}},
              smooth=Smooth.Bezier,
              fillColor={255,83,129},
              fillPattern=FillPattern.Solid,
              lineColor={0,131,169})}));
    end Fluid2TSPro;

    model TSPro2Fluid "A Modelica.Fluid to TSPro connector, water-steam"

    // IDNES //

      replaceable package ModelicaMedium =
          Modelica.Media.Water.WaterIF97_ph                                  "Medium model of the MSL part"
                                       annotation (choicesAllMatching=true, Dialog(group="Fundamental Definitions"));

    //  parameter TILMedia.VLEFluidTypes.BaseVLEFluid ClaRaMedium = simCenter.fluid1 "Medium in the component"
      // ClaRa.Basics.Interfaces.FluidPortOut steam_a(Medium=ClaRaMedium)                 annotation (Placement(
      //       transformation(extent={{90,-9.9},{109,9.9}}), iconTransformation(extent=
      //         {{90,-10},{109,9.95}})));

      ThermoSysPro.WaterSteam.Connectors.FluidInlet steam_inlet
        annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
      Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium =
            ModelicaMedium)
        annotation (Placement(transformation(extent={{90,-10},{110,10}})));
    equation
     // port_b.m_flow+steam_a.m_flow = 0;
      //port_b.h_outflow = inStream(steam_a.h_outflow);
     // inStream(port_b.h_outflow) = steam_a.h_outflow;

     // port_b.p = steam_a.p;
     // inStream(port_b.Xi_outflow) =steam_a.xi_outflow;
     // port_b.Xi_outflow = inStream(steam_a.xi_outflow);

     // port_b.C_outflow=zeros(ModelicaMedium.nC);

    port_b.p = steam_inlet.P;
    port_b.h_outflow = steam_inlet.h_vol;
    port_b.m_flow + steam_inlet.Q = 0;
    //steam_inlet.h =steam_inlet.h_vol;
    //0 = if (port_a.m_flow > 0) then inStream(port_a.h_outflow) - steam_outlet.h else steam_outlet.h - steam_outlet.h_vol;
    0 = if (steam_inlet.Q > 0) then steam_inlet.h - steam_inlet.h_vol else inStream(port_b.h_outflow) - steam_inlet.h;

    port_b.C_outflow=zeros(ModelicaMedium.nC);
    port_b.Xi_outflow=zeros(ModelicaMedium.nC);

        annotation(choices(choice=simCenter.fluid1 "First fluid defined in global simCenter",
                           choice=simCenter.fluid2 "Second fluid defined in global simCenter",
                           choice=simCenter.fluid3 "Third fluid defined in global simCenter"),
                                                              Dialog(group="Fundamental Definitions"),
                  defaultComponentName =     "fluid2TSPro",
              Icon(graphics={Polygon(
              points={{-96,10},{2,10},{36,-10},{-98,-10},{-96,10}},
              lineColor={0,0,0},
              smooth=Smooth.None,
              fillColor={37,219,255},
              fillPattern=FillPattern.Solid), Polygon(
              points={{100,10},{100,10},{-20,10},{-20,10},{-12,0},{0,0},{
                  12,0},{20,-10},{20,-10},{100,-10},{100,-10},{100,10}},
              smooth=Smooth.Bezier,
              fillColor={255,83,129},
              fillPattern=FillPattern.Solid,
              lineColor={0,131,169})}));
    end TSPro2Fluid;
    annotation (Documentation(info="<html>
<p>Fluid connectors : TSPro &lt;-&gt; MSL : water-steam case</p>
</html>"));
  end Fluid;

  package Thermal
    model ThermalModelicaTSP

      parameter Integer N=1;

      ThermoSysPro.Thermal.Connectors.ThermalPort thermalPort[N]
        annotation (Placement(transformation(extent={{80,0},{100,20}})));
      Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a port_a[N]
        annotation (Placement(transformation(extent={{-100,0},{-80,20}})));

    equation

      thermalPort.T = port_a.T;
      thermalPort.W = - port_a.Q_flow;

      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
              Rectangle(
              extent={{-80,20},{0,0}},
              lineColor={28,108,200},
              fillColor={238,46,47},
              fillPattern=FillPattern.Forward), Rectangle(
              extent={{0,20},{80,0}},
              lineColor={28,108,200},
              fillColor={204,164,72},
              fillPattern=FillPattern.Backward)}), Diagram(coordinateSystem(
              preserveAspectRatio=false), graphics={Rectangle(
              extent={{0,20},{80,0}},
              lineColor={28,108,200},
              fillColor={204,164,72},
              fillPattern=FillPattern.Backward), Rectangle(
              extent={{-80,20},{0,0}},
              lineColor={28,108,200},
              fillColor={238,46,47},
              fillPattern=FillPattern.Forward)}),
        Documentation(info="<html>
<h4>Adaptateur Modelica &lt;-&gt; TSPro CEA </h4>
<h4>Pour ThermoSysPro 4.0 </h4>
<h4>Cr&eacute;&eacute; et modifi&eacute; par </h4>
<ul>
<li><h4>Huynh-Duc Nguyen </h4></li>
</ul>
</html>"));
    end ThermalModelicaTSP;

    model ThermalTSPModelica

      parameter Integer N=1;

      ThermoSysPro.Thermal.Connectors.ThermalPort thermalPort[N]
        annotation (Placement(transformation(extent={{-100,0},{-80,20}})));
      Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b port_b[N]
        annotation (Placement(transformation(extent={{80,0},{100,20}})));

    equation

      thermalPort.T = port_b.T;
      thermalPort.W = - port_b.Q_flow;

      annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
              Rectangle(
              extent={{-80,20},{0,0}},
              lineColor={28,108,200},
              fillColor={204,164,72},
              fillPattern=FillPattern.Backward), Rectangle(
              extent={{0,20},{80,0}},
              lineColor={28,108,200},
              fillColor={238,46,47},
              fillPattern=FillPattern.Forward)}), Diagram(coordinateSystem(
              preserveAspectRatio=false), graphics={Rectangle(
              extent={{-80,20},{0,0}},
              lineColor={28,108,200},
              fillColor={204,164,72},
              fillPattern=FillPattern.Backward), Rectangle(
              extent={{0,20},{80,0}},
              lineColor={28,108,200},
              fillColor={238,46,47},
              fillPattern=FillPattern.Forward)}),
        Documentation(info="<html>
<h4>Adaptateur TSPro &lt;-&gt; Modelica CEA </h4>
<h4>Pour ThermoSysPro 4.0 </h4>
<h4>Cr&eacute;&eacute; et modifi&eacute; par </h4>
<ul>
<li><h4>Huynh-Duc Nguyen </h4></li>
</ul>
</html>"));
    end ThermalTSPModelica;
  end Thermal;
  annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"));
end AdaptatorForMSL;
