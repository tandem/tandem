within TANDEM.SMR.BOP;
package BOP_TSPro "ThermoSysPro Balance Of Plant"





  annotation (Documentation(info="<html>
<p>Credit :TANDEM UE Project, CEA-IRESNE contribution</p>
<p>November 2023</p>
</html>"), uses(
    ThermoSysPro(version="4.0"),
    Modelica(version="4.0.0"),
    ModelicaServices(version="4.0.0")));
end BOP_TSPro;
