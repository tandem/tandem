within TANDEM.SMR.BOP.BOP_POLIMI;
model BOPdyn_fluid
  ThermoPower.Water.Pump FWpump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,0.2528158936911876,
            0.4504036332538314}, head_nom={631.0614850261779,432.23389385354653,
            0}),
    rho0(displayUnit="kg/m3") = 948.0416618615245,
    n0=1500,
    hstart=492540,
    noInitialPressure=false,
    w0=239.68,
    dp0=4019900,
    use_in_n=true,
    dp(start=4019900),
    outfl(
      p(start=4694600),
      m_flow(start=-239.68),
      h_outflow(start=492840)),
    infl(
      p(start=674700),
      m_flow(start=239.68),
      h_outflow(start=492840)))
    annotation (Placement(transformation(extent={{-36,-74},{-60,-50}})));
  TANDEM.SMR.BOP.BOP_POLIMI.Components.FWtank_vectPorts FWtank(
    N_in=5,
    N_out=2,
    V=100,
    FixedInitPressure=false,
    pstart=714700,
    dp_nom=40000)
    annotation (Placement(transformation(extent={{-14,-57},{20,-20}})));

  ThermoPower.Water.Pump CNDpump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,0.1835052371622873,
            0.3269233762650943}, head_nom={117.51681211147955,80.49096719964353,
            0}),
    rho0(displayUnit="kg/m3") = 992.8871939435003,
    n0=1500,
    hstart=164.25e3,
    noInitialPressure=false,
    w0=182.2,
    dp0=784000,
    use_in_n=true,
    dp(start=784000),
    outfl(
      p(start=791000),
      m_flow(start=-182.2),
      h_outflow(start=164250)),
    infl(
      p(start=7000),
      m_flow(start=182.2),
      h_outflow(start=164250)))
    annotation (Placement(transformation(extent={{210,-76},{184,-50}})));
  ThermoPower.Examples.RankineCycle.Models.PrescribedPressureCondenser Condenser(
    p=7000,
    Vtot=100,
    initOpt=ThermoPower.Choices.Init.Options.fixedState)
    annotation (Placement(transformation(extent={{212,-24},{232,-4}})));
  ThermoPower.Water.ValveVap HP_TAV(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=4500000,
    dpnom=100000,
    wnom=218.36,
    rhonom(displayUnit="kg/m3") = 19.46395772,
    CheckValve=true,                               w(start=218.36),
    rho(start=19.463957729),
    Tin(start=573.15),
    dp(start=100000))
    annotation (Placement(transformation(extent={{-154,34},{-136,52}})));
  Components.SteamTurbine_variableEta HPTurbine(
    PRstart=5.830,
    wnom=218.36,
    pnom=4400000,
    eta_iso_nom=0.87255,
    Kt=0.024224898,
    pin(start=4400000),
    w(start=218.36),
    corrWet=false,
    corrFlow=false)
    annotation (Placement(transformation(extent={{-130,24},{-110,44}})));
  ThermoPower.Water.FlowSplit flowSplit
    annotation (Placement(transformation(extent={{-208,40},{-196,52}})));
  TANDEM.SMR.BOP.BOP_POLIMI.Components.MoistureSeparator moistureSeparator
    annotation (Placement(transformation(extent={{-38,40},{-26,52}})));

  ThermoPower.Water.FlowSplit flowSplit1
    annotation (Placement(transformation(extent={{-106,36},{-94,48}})));
  Components.SteamTurbine_variableEta LPTurbine1(
    PRstart=8.42322,
    wnom=182.2,
    pnom=634700,
    eta_iso_nom=0.880655,
    pin(start=634700),
    w(start=182.2),
    Kt=0.1419294,
    corrWet=false,
    corrFlow=false)
    annotation (Placement(transformation(extent={{90,24},{110,44}})));
  Components.SteamTurbine_variableEta LPTurbine2(
    PRstart=11.442857,
    wnom=165.8,
    pnom=79100,
    eta_iso_nom=0.834375,
    pin(start=79100),
    w(start=165.8),
    Kt=0.85123104,
    corrWet=false,
    corrFlow=false)
    annotation (Placement(transformation(extent={{178,24},{198,44}})));
  ThermoPower.Water.FlowSplit flowSplit2
    annotation (Placement(transformation(extent={{146,52},{158,64}})));
  ThermoPower.Water.ValveLiq valve_SGin(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=4668000,
    dpnom=100000,
    wnom=239.68,
    rhonom=908.99456,
    CheckValve=true,
    w(start=239.68),
    dp(start=100000),
    rho(start=908.99456),
    Tin(start=434.06068)) annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=0,
        origin={-160,-54})));
  ThermoPower.Water.FlowSplit flowSplit3
    annotation (Placement(transformation(extent={{-90,38},{-78,50}})));
  ThermoPower.Water.FlangeA flangeA1(p(start=7.147e5))
    annotation (Placement(transformation(extent={{180,-150},{200,-130}})));
  ThermoPower.Water.FlangeB flangeB1(p(start=7.547e5))
    annotation (Placement(transformation(extent={{-200,-150},{-180,-130}})));
  ThermoPower.Water.SensP sensP
    annotation (Placement(transformation(extent={{-192,40},{-178,54}})));
  ThermoPower.Water.SensT1 sensT1_1
    annotation (Placement(transformation(extent={{72,38},{84,50}})));
  TANDEM.SMR.BOP.BOP_POLIMI.Control.SensorBus sensorBus
    annotation (Placement(transformation(extent={{110,130},{130,150}})));
  TANDEM.SMR.BOP.BOP_POLIMI.Control.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-130,130},{-110,150}})));
  ThermoPower.Water.SensT1 sensT1_2
    annotation (Placement(transformation(extent={{-178,40},{-166,52}})));
  ThermoPower.Water.SensP sensP1
    annotation (Placement(transformation(extent={{-84,60},{-72,72}})));
  ThermoPower.Water.ValveLiq     valveLiq(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=754700,
    wnom=24.51,
    dpnom=40000,
    rhonom(displayUnit="kg/m3") = 952.7205,
    w(start=24.51),
    dp(start=40000),
    rho(start=952.7205))                                      annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-56,-32})));
  TANDEM.SMR.BOP.BOP_POLIMI.Components.ShellAndTube_BOP hp_fw(
    Nt=831,
    Np=2,
    L=9.5,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=47.523e6,
    ms_nom=24.51,
    mt_nom=239.68,
    ps_nom=754700,
    pt_nom=4694600,
    hsi_nom=2639730,
    hso_nom=700800,
    hti_nom=492840,
    hto_nom=691110,
    dpt_nom=54600,
    Cf_t=862.0677,
    hshell=7009.4956,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (
          gamma=hp_fw.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter)
    annotation (Placement(transformation(extent={{-124,-66},{-98,-42}})));
  TANDEM.SMR.BOP.BOP_POLIMI.Components.ShellAndTube_BOP lp_fw(
    Nt=593,
    Np=2,
    L=11.397,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=37.722e6,
    ms_nom=16.41,
    mt_nom=182.2,
    ps_nom=80100,
    pt_nom=754700,
    hsi_nom=2616620,
    hso_nom=317890,
    hti_nom=164250,
    hto_nom=371280,
    dpt_nom=76300,
    Cf_t=2221.6003,
    hshell=6054.529,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.HeatTransfer2phDB (gamma_b=lp_fw.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter)
    annotation (Placement(transformation(extent={{144,-68},{168,-44}})));

  TANDEM.SMR.BOP.BOP_POLIMI.Components.ShellAndTube_BOP rh(
    Nt=12778,
    Np=2,
    L=5.733,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=38.902e6,
    ms_nom=21.32,
    mt_nom=182.2,
    ps_nom=4500000,
    pt_nom=674700,
    hsi_nom=2944130,
    hso_nom=1119460,
    hti_nom=2763620,
    hto_nom=2977140,
    dpt_nom=30100,
    Cf_t=2.6980236,
    hshell=909.22925,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.HeatTransfer2phDB (gamma_b=rh.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter,
    tube(FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Steam))
    annotation (Placement(transformation(extent={{62,42},{36,66}})));

  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{288,-10},{308,10}})));
  ThermoPower.Water.ValveVap LP_TAV(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=754700,
    dpnom=50000,
    wnom=194.6607,
    rhonom=4.226538,
    CheckValve=true,
    w(start=194.6607),
    rho(start=4.226538),
    dp(start=50000),
    Tin(start=441.44587))
    annotation (Placement(transformation(extent={{-72,58},{-60,70}})));
  ThermoPower.Water.SensP sensP2
    annotation (Placement(transformation(extent={{82,-60},{94,-48}})));
  ThermoPower.Water.PressDrop    pressDropLin3(
    wnom=11.65,
    FFtype=ThermoPower.Choices.PressDrop.FFtypes.OpPoint,
    dpnom=40000,
    rhonom(displayUnit="kg/m3") = 899.478342,
    w(start=11.65),
    pin(start=754700),
    pout(start=714700),
    dp(start=40000),
    rho(start=899.478342))                                    annotation (
      Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=270,
        origin={-31,5})));
  ThermoPower.Water.FlowSplit flowSplit4
    annotation (Placement(transformation(extent={{-222,52},{-210,40}})));
  ThermoPower.Water.FlangeB flangeB2(p(start=45e5))
    annotation (Placement(transformation(extent={{-270,-150},{-250,-130}})));
  ThermoPower.Water.FlangeA flangeA2(p(start=7.147e5))
    annotation (Placement(transformation(extent={{112,-150},{132,-130}})));
  ThermoPower.Water.FlangeA flangeA3(p(start=0.07e5))
    annotation (Placement(transformation(extent={{250,-150},{270,-130}})));
  ThermoPower.Water.FlowJoin flowJoin2
                                      annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=0,
        origin={222,-90})));
  ThermoPower.Water.FlangeB flangeB4(p(start=7.147e5))
    annotation (Placement(transformation(extent={{-60,-150},{-40,-130}})));
  ThermoPower.Water.Mixer mixer(
    V=1,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Steam,
    pstart=664700,
    hstart=2763620)
    annotation (Placement(transformation(extent={{0,38},{14,52}})));
  ThermoPower.Water.Mixer mixer1(
    V=1,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
    pstart=7000,
    hstart=2332.18e3) annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=270,
        origin={221,11})));
  ThermoPower.Water.ValveVap LP_TAV1(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=80100,
    dpnom=1000,
    wnom=165.8,
    rhonom=0.48513472,
    CheckValve=true,
    w(start=165.8),
    rho(start=0.48513472),
    dp(start=1000),
    Tin(start=366.6427))
    annotation (Placement(transformation(extent={{166,54},{178,66}})));
  ThermoPower.Water.SensT1 sensT1_3
    annotation (Placement(transformation(extent={{-266,-58},{-254,-46}})));
  ThermoPower.Water.SensP sensP3
    annotation (Placement(transformation(extent={{136,54},{148,66}})));
  ThermoPower.Water.PressDrop    pressDropLin4(
    wnom=182.01654,
    FFtype=ThermoPower.Choices.PressDrop.FFtypes.OpPoint,
    dpnom=40000,
    rhonom(displayUnit="g/cm3") = 3.4936115,
    w(start=182.01654),
    pin(start=704700),
    pout(start=664700),
    dp(start=40000),
    rho(start=3.4936115))                                     annotation (
      Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={-13,47})));
  ThermoPower.Water.ValveLiq velveRH(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=4500000,
    dpnom=3785300,
    wnom=21.32,
    rhonom=857.4084,
    w(start=21.32),
    dp(start=3785300),
    rho(start=857.4084),
    Tin(start=530.15)) annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={14,0})));
  ThermoPower.Water.Mixer mixer2(
    V=1,
    FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Steam,
    pstart=700000,
    hstart=691110)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=180,
        origin={-138,-58})));
  ThermoPower.Water.FlangeA flangeA4(p(start=7.147e5))
    annotation (Placement(transformation(extent={{40,-150},{60,-130}})));
  ThermoPower.Water.FlangeB flangeB3(p(start=7.147e5))
    annotation (Placement(transformation(extent={{-130,-150},{-110,-130}})));
  ThermoPower.Water.FlowSplit flowSplit5
    annotation (Placement(transformation(extent={{120,36},{132,48}})));
  ThermoPower.Water.FlangeA flangeA
    annotation (Placement(transformation(extent={{-310,40},{-290,60}})));
  ThermoPower.Water.FlangeB flangeB
    annotation (Placement(transformation(extent={{-310,-60},{-290,-40}})));
  ThermoPower.Water.SensT1 sensT1_4
    annotation (Placement(transformation(extent={{-32,-64},{-20,-52}})));
  Modelica.Mechanics.Rotational.Sensors.PowerSensor powerSensor
    annotation (Placement(transformation(extent={{-10,10},{10,-10}},
        rotation=0,
        origin={232,34})));
  ThermoPower.Water.ValveLiq     valveLiq1(
    CvData=ThermoPower.Choices.Valve.CvTypes.OpPoint,
    pnom=80100,
    wnom=16.41,
    dpnom=73100,
    rhonom(displayUnit="kg/m3") = 962.911,
    w(start=16.41),
    dp(start=73100),
    rho(start=962.911, displayUnit="kg/m3"))                  annotation (
      Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={179,-87})));
  ThermoPower.Electrical.Generator generator(Pnom=177.268e6, referenceGenerator
      =true) annotation (Placement(transformation(extent={{250,22},{274,46}})));
  ThermoPower.Electrical.FrequencySensor frequencySensor annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={258,128})));
  ThermoPower.Electrical.PowerSensor powerSensor1 annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=270,
        origin={278,20})));
equation
  connect(Condenser.waterOut, CNDpump.infl) annotation (Line(points={{222,-24},{
          222,-60.4},{207.4,-60.4}}, color={0,0,255}));
  connect(HPTurbine.outlet, flowSplit1.in1)
    annotation (Line(points={{-112,42},{-103.6,42}}, color={0,0,255}));
  connect(HPTurbine.shaft_b, LPTurbine1.shaft_a)
    annotation (Line(points={{-113.6,34},{93.4,34}}, color={0,0,0}));
  connect(LPTurbine1.shaft_b, LPTurbine2.shaft_a)
    annotation (Line(points={{106.4,34},{181.4,34}},color={0,0,0}));
  connect(HP_TAV.outlet, HPTurbine.inlet)
    annotation (Line(points={{-136,43},{-136,42},{-128,42}}, color={0,0,255}));
  connect(flowSplit1.out1, flowSplit3.in1) annotation (Line(points={{-96.4,44.4},
          {-96,44.4},{-96,44},{-87.6,44}},         color={0,0,255}));
  connect(flowSplit3.out2, flangeB1) annotation (Line(points={{-80.4,41.6},{-78,
          41.6},{-78,-82},{-190,-82},{-190,-140}},
                                   color={0,0,255}));
  connect(sensorBus.P_SG, sensP.p) annotation (Line(
      points={{120,140},{120,128},{-179.4,128},{-179.4,51.2}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.T_LPT, sensT1_1.T) annotation (Line(
      points={{120,140},{120,128},{82,128},{82,54},{82.8,54},{82.8,47.6}},
      color={255,219,88},
      thickness=0.5));
  connect(flowSplit.out2, sensP.flange) annotation (Line(points={{-198.4,43.6},
          {-198.4,44.2},{-185,44.2}}, color={0,0,255}));
  connect(sensP.flange, HP_TAV.inlet) annotation (Line(points={{-185,44.2},{-178,
          44.2},{-178,43},{-154,43}}, color={0,0,255}));
  connect(sensP.flange, sensT1_2.flange) annotation (Line(points={{-185,44.2},{
          -178,44.2},{-178,43.6},{-172,43.6}}, color={0,0,255}));
  connect(sensorBus.T_SGout, sensT1_2.T) annotation (Line(
      points={{120,140},{120,128},{-167.2,128},{-167.2,49.6}},
      color={255,219,88},
      thickness=0.5));
  connect(valveLiq.outlet, FWtank.inlet[4]) annotation (Line(points={{-50,-32},
          {-32,-32},{-32,-14},{3,-14},{3,-28.88}}, color={0,0,255}));
  connect(hp_fw.flangeA1, FWpump.outfl) annotation (Line(points={{-100.6,-54},{-96,
          -54},{-96,-53.6},{-55.2,-53.6}}, color={0,0,255}));
  connect(flowSplit1.out2, hp_fw.flangeA) annotation (Line(points={{-96.4,39.6},
          {-96.4,10},{-111,10},{-111,-44.64}}, color={0,0,255}));
  connect(hp_fw.flangeB, valveLiq.inlet) annotation (Line(points={{-111,-63.6},
          {-112,-63.6},{-112,-74},{-88,-74},{-88,-32},{-62,-32}}, color={0,0,
          255}));
  connect(lp_fw.flangeA1, CNDpump.outfl) annotation (Line(points={{165.6,-56},{189.2,
          -56},{189.2,-53.9}}, color={0,0,255}));
  connect(lp_fw.flangeB1, FWtank.inlet[1]) annotation (Line(points={{146.64,-56},
          {62,-56},{62,-14},{3,-14},{3,-29.99}},
                                            color={0,0,255}));
  connect(flowSplit2.out2, lp_fw.flangeA) annotation (Line(points={{155.6,55.6},
          {155.6,-14},{156,-14},{156,-46.64}},
                                             color={0,0,255}));
  connect(rh.flangeB1, sensT1_1.flange) annotation (Line(points={{59.14,54},{68,
          54},{68,34},{78,34},{78,41.6}},
                                    color={0,0,255}));
  connect(flowSplit.out1, rh.flangeA) annotation (Line(points={{-198.4,48.4},{-198.4,
          90},{50,90},{50,78},{49,78},{49,63.36}},
                                 color={0,0,255}));
  connect(actuatorBus.N_HPpump, FWpump.in_n) annotation (Line(
      points={{-120,140},{-120,124},{-44.88,124},{-44.88,-52.4}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.N_LPpump, CNDpump.in_n) annotation (Line(
      points={{-120,140},{-120,124},{162,124},{162,-36},{200.38,-36},{200.38,
          -52.6}},
      color={80,200,120},
      thickness=0.5));
  connect(lp_fw.flangeB1, sensP2.flange) annotation (Line(points={{146.64,-56},
          {88,-56},{88,-56.4}},                 color={0,0,255}));
  connect(sensorBus.P_FWtank, sensP2.p) annotation (Line(
      points={{120,140},{120,128},{114,128},{114,-50.4},{92.8,-50.4}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.P_LPTin, sensP1.p) annotation (Line(
      points={{120,140},{120,128},{-73.2,128},{-73.2,69.6}},
      color={255,219,88},
      thickness=0.5));
  connect(pressDropLin3.outlet, FWtank.inlet[3]) annotation (Line(points={{-31,0},
          {-32,0},{-32,-14},{4,-14},{4,-20},{3,-20},{3,-29.25}},
                                                  color={0,0,255}));
  connect(moistureSeparator.drain, pressDropLin3.inlet) annotation (Line(points={{-32,40},
          {-31,38},{-31,10}},                 color={0,0,255}));
  connect(actuatorBus.Kv_SGin, valve_SGin.theta) annotation (Line(
      points={{-120,140},{-120,124},{-160,124},{-160,-47.6}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_HP_TAV, HP_TAV.theta) annotation (Line(
      points={{-120,140},{-120,124},{-145,124},{-145,50.2}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_LP_TAV, LP_TAV.theta) annotation (Line(
      points={{-120,140},{-120,124},{-66,124},{-66,68.8}},
      color={80,200,120},
      thickness=0.5));
  connect(flowSplit4.out2, flowSplit.in1) annotation (Line(points={{-212.4,48.4},
          {-205.6,48.4},{-205.6,46}}, color={0,0,255}));
  connect(flowSplit4.out1, flangeB2) annotation (Line(points={{-212.4,43.6},{
          -212.4,-122},{-260,-122},{-260,-140}},
                                          color={0,0,255}));
  connect(flangeA3, flowJoin2.in1) annotation (Line(points={{260,-140},{260,
          -100},{212,-100},{212,-92.4},{218.4,-92.4}},
                                      color={0,0,255}));
  connect(FWtank.outlet[2], flangeB4) annotation (Line(points={{3,-47.2875},{3,
          -58},{2,-58},{2,-140},{-50,-140}},
                                 color={0,0,255}));
  connect(mixer.out, rh.flangeA1) annotation (Line(points={{14,45},{20,45},{20,
          54},{38.6,54}}, color={0,0,255}));
  connect(mixer.in2, flangeA2) annotation (Line(points={{1.4,40.8},{1.4,30},{2,
          30},{2,20},{118,20},{118,-140},{122,-140}},
                                       color={0,0,255}));
  connect(mixer1.out, Condenser.steamIn)
    annotation (Line(points={{221,4},{222,4},{222,-4}}, color={0,0,255}));
  connect(flowJoin2.out, mixer1.in1) annotation (Line(points={{225.6,-90},{236,
          -90},{236,16.6},{225.2,16.6}}, color={0,0,255}));
  connect(LPTurbine2.outlet, mixer1.in2) annotation (Line(points={{196,42},{
          216.8,42},{216.8,16.6}}, color={0,0,255}));
  connect(flowSplit2.out1, LP_TAV1.inlet) annotation (Line(points={{155.6,60.4},
          {155.6,60},{166,60}}, color={0,0,255}));
  connect(LP_TAV1.outlet, LPTurbine2.inlet)
    annotation (Line(points={{178,60},{180,60},{180,42}}, color={0,0,255}));
  connect(actuatorBus.Kv_LP2_TAV, LP_TAV1.theta) annotation (Line(
      points={{-120,140},{-120,124},{172,124},{172,64.8}},
      color={80,200,120},
      thickness=0.5));
  connect(valve_SGin.outlet, sensT1_3.flange) annotation (Line(points={{-168,
          -54},{-260,-54},{-260,-54.4}}, color={0,0,255}));
  connect(sensorBus.T_SGin, sensT1_3.T) annotation (Line(
      points={{120,140},{120,128},{-228,128},{-228,-48.4},{-255.2,-48.4}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.P_LPT2in, sensP3.p) annotation (Line(
      points={{120,140},{120,128},{146.8,128},{146.8,63.6}},
      color={255,219,88},
      thickness=0.5));
  connect(FWpump.infl, FWtank.outlet[1]) annotation (Line(points={{-38.4,-59.6},
          {3,-59.6},{3,-48.2125}}, color={0,0,255}));
  connect(flangeA1, FWtank.inlet[5]) annotation (Line(points={{190,-140},{190,
          -112},{62,-112},{62,-14},{4,-14},{4,-28},{3,-28},{3,-28.51}},
                                                 color={0,0,255}));
  connect(flowSplit3.out1, sensP1.flange) annotation (Line(points={{-80.4,46.4},
          {-80,46.4},{-80,64},{-78,64},{-78,63.6}},
                                                 color={0,0,255}));
  connect(flowSplit3.out1, LP_TAV.inlet) annotation (Line(points={{-80.4,46.4},
          {-80.4,64},{-72,64}}, color={0,0,255}));
  connect(LP_TAV.outlet, moistureSeparator.inlet) annotation (Line(points={{-60,64},
          {-42,64},{-42,46},{-38,46}},                       color={0,0,255}));
  connect(LPTurbine1.inlet, sensT1_1.flange) annotation (Line(points={{92,42},{
          92,41.6},{78,41.6}},                  color={0,0,255}));
  connect(pressDropLin4.outlet, mixer.in1) annotation (Line(points={{-8,47},{-8,
          48},{-4,48},{-4,49.2},{1.4,49.2}},  color={0,0,255}));
  connect(pressDropLin4.inlet, moistureSeparator.steam)
    annotation (Line(points={{-18,47},{-18,46},{-26,46}}, color={0,0,255}));
  connect(velveRH.outlet, FWtank.inlet[2]) annotation (Line(points={{14,-8},{14,
          -14},{3,-14},{3,-29.62}}, color={0,0,255}));
  connect(rh.flangeB, velveRH.inlet) annotation (Line(points={{49,44.4},{49,10},
          {14,10},{14,8}}, color={0,0,255}));
  connect(actuatorBus.Kv_RH, velveRH.theta) annotation (Line(
      points={{-120,140},{-120,124},{32,124},{32,0},{20.4,0}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_HPFW, valveLiq.theta) annotation (Line(
      points={{-120,140},{-120,124},{-56,124},{-56,-27.2}},
      color={80,200,120},
      thickness=0.5));
  connect(mixer2.in2, hp_fw.flangeB1) annotation (Line(points={{-133.2,-54.4},{
          -133.2,-54},{-121.14,-54}}, color={0,0,255}));
  connect(mixer2.out, valve_SGin.inlet) annotation (Line(points={{-144,-58},{
          -146,-58},{-146,-54},{-152,-54}}, color={0,0,255}));
  connect(flangeA4, mixer2.in1) annotation (Line(points={{50,-140},{50,-108},{
          -130,-108},{-130,-62},{-132,-62},{-132,-61.6},{-133.2,-61.6}}, color=
          {0,0,255}));
  connect(LPTurbine1.outlet, flowSplit5.in1)
    annotation (Line(points={{108,42},{122.4,42}}, color={0,0,255}));
  connect(flowSplit5.out1, sensP3.flange) annotation (Line(points={{129.6,44.4},
          {129.6,57.6},{142,57.6}}, color={0,0,255}));
  connect(sensP3.flange, flowSplit2.in1) annotation (Line(points={{142,57.6},{
          142,58},{148.4,58}}, color={0,0,255}));
  connect(flowSplit5.out2, flangeB3) annotation (Line(points={{129.6,39.6},{130,
          39.6},{130,-98},{-120,-98},{-120,-140}}, color={0,0,255}));
  connect(flowSplit4.in1, flangeA) annotation (Line(points={{-219.6,46},{-250,46},
          {-250,50},{-300,50}}, color={0,0,255}));
  connect(flangeB, sensT1_3.flange) annotation (Line(points={{-300,-50},{-276,-50},
          {-276,-54.4},{-260,-54.4}}, color={0,0,255}));
  connect(FWpump.infl, sensT1_4.flange) annotation (Line(points={{-38.4,-59.6},
          {-25.2,-59.6},{-25.2,-60.4},{-26,-60.4}}, color={0,0,255}));
  connect(sensorBus.T_FWtank, sensT1_4.T) annotation (Line(
      points={{120,140},{120,128},{-21.2,128},{-21.2,-54.4}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Pmech, powerSensor.power) annotation (Line(
      points={{120,140},{120,128},{224,128},{224,45}},
      color={255,219,88},
      thickness=0.5));
  connect(LPTurbine2.shaft_b, powerSensor.flange_a)
    annotation (Line(points={{194.4,34},{222,34}}, color={0,0,0}));
  connect(lp_fw.flangeB, valveLiq1.inlet) annotation (Line(points={{156,-65.6},
          {156,-87},{174,-87}}, color={0,0,255}));
  connect(valveLiq1.outlet, flowJoin2.in2) annotation (Line(points={{184,-87},{
          198,-87},{198,-87.6},{218.4,-87.6}}, color={0,0,255}));
  connect(actuatorBus.Kv_LPFW, valveLiq1.theta) annotation (Line(
      points={{-120,140},{-120,124},{162,124},{162,-36},{179,-36},{179,-83}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.Pel_out,powerSensor1. P) annotation (Line(
      points={{120,140},{120,128},{224,128},{224,60},{292,60},{292,20},{283.64,
          20}},
      color={255,219,88},
      thickness=0.5));
  connect(generator.port, powerSensor1.port_a) annotation (Line(
      points={{272.32,34},{278,34},{278,26}},
      color={0,0,255},
      thickness=0.5));
  connect(frequencySensor.port, powerSensor1.port_a) annotation (Line(
      points={{268,128},{278,128},{278,26}},
      color={0,0,255},
      thickness=0.5));
  connect(powerSensor.flange_b, generator.shaft)
    annotation (Line(points={{242,34},{251.68,34}}, color={0,0,0}));
  connect(powerSensor1.port_b, powerConnection) annotation (Line(
      points={{278.12,14},{278,14},{278,0},{298,0}},
      color={0,0,255},
      thickness=0.5));
  connect(sensorBus.Freq, frequencySensor.f) annotation (Line(
      points={{120,140},{120,128},{247.8,128}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-300,-140},
            {300,140}}), graphics={
        Rectangle(
          extent={{-300,140},{300,-142}},
          lineColor={0,140,72},
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-294,134},{294,-136}},
          lineColor={0,140,72},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-60.5,44.5},{60.5,-44.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={-260.5,-54.5},
          rotation=90,
          textString="HT extraction"),
        Text(
          extent={{-60.5,44.5},{60.5,-44.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={-190.5,-54.5},
          rotation=90,
          textString="IT extraction"),
        Text(
          extent={{-83.5,43.5},{83.5,-43.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={-51.5,-29.5},
          rotation=90,
          textString="Water extraction"),
        Text(
          extent={{-45.5,31.5},{45.5,-31.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={190.5,-69.5},
          rotation=90,
          textString="IT return"),
        Text(
          extent={{-47.5,29.5},{47.5,-29.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={122.5,-67.5},
          rotation=90,
          textString="HT return"),
        Text(
          extent={{-45.5,35.5},{45.5,-35.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={260.5,-67.5},
          rotation=90,
          textString="LT return"),
        Text(
          extent={{-47.5,29.5},{47.5,-29.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={50.5,-67.5},
          rotation=90,
          textString="HP return"),
        Text(
          extent={{-60.5,44.5},{60.5,-44.5}},
          textColor={0,0,0},
          fontName="Bahnschrift",
          rotate=45,
          origin={-120.5,-54.5},
          rotation=90,
          textString="LT extraction")}),
          Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-300,-140},{300,140}})));
end BOPdyn_fluid;
