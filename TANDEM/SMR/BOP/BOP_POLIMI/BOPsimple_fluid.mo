within TANDEM.SMR.BOP.BOP_POLIMI;
model BOPsimple_fluid
  ThermoPower.Water.Pump FWpump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,0.2528158936911876,
            0.4504036332538314}, head_nom={631.0614850261779,432.23389385354653,
            0}),
    rho0(displayUnit="kg/m3") = 948.0416618615245,
    n0=1500,
    hstart=492540,
    noInitialPressure=false,
    w0=239.68,
    dp0=4019900,
    use_in_n=true,
    dp(start=4019900),
    outfl(
      p(start=4694600),
      m_flow(start=-239.68),
      h_outflow(start=492840)),
    infl(
      p(start=674700),
      m_flow(start=239.68),
      h_outflow(start=492840)))
    annotation (Placement(transformation(extent={{-36,-74},{-60,-50}})));
  Components.FWtank_vectPorts FWtank(
    N_in=5,
    V=100,
    FixedInitPressure=false,
    pstart=714700,
    dp_nom=40000)
    annotation (Placement(transformation(extent={{-14,-57},{20,-20}})));

  ThermoPower.Water.Pump CNDpump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,0.1835052371622873,
            0.3269233762650943}, head_nom={117.51681211147955,80.49096719964353,
            0}),
    rho0(displayUnit="kg/m3") = 992.8871939435003,
    n0=1500,
    hstart=164.25e3,
    noInitialPressure=false,
    w0=182.2,
    dp0=784000,
    use_in_n=true,
    dp(start=784000),
    outfl(
      p(start=791000),
      m_flow(start=-182.2),
      h_outflow(start=164250)),
    infl(
      p(start=7000),
      m_flow(start=182.2),
      h_outflow(start=164250)))
    annotation (Placement(transformation(extent={{154,-76},{128,-50}})));
  ThermoPower.Examples.RankineCycle.Models.PrescribedPressureCondenser Condenser(
    p=7000,
    Vtot=100,
    initOpt=ThermoPower.Choices.Init.Options.fixedState)
    annotation (Placement(transformation(extent={{156,-24},{176,-4}})));
  ThermoPower.Water.FlowJoin flowJoin annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=270,
        origin={166,12})));
  ThermoPower.Water.PressDropLin pressDropLin5(R=(0.801 - 0.07)*1e5/16.41)
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={140,-86})));
  ThermoPower.Water.ValveLin HP_TAV(Kv=218.36/1e5, w(start=218.36))
    annotation (Placement(transformation(extent={{-154,34},{-136,52}})));
  ThermoPower.Water.SteamTurbineStodola HPTurbine(
    PRstart=5.830,
    wnom=218.36,
    pnom=4400000,
    eta_iso_nom=0.87255,
    Kt=0.024224898,
    pin(start=4400000),
    w(start=218.36))
    annotation (Placement(transformation(extent={{-130,24},{-110,44}})));
  ThermoPower.Water.FlowSplit flowSplit
    annotation (Placement(transformation(extent={{-208,40},{-196,52}})));
  Components.MoistureSeparator moistureSeparator
    annotation (Placement(transformation(extent={{-60,40},{-48,52}})));

  ThermoPower.Water.PressDropLin pressDropLin7(R=0.4e5/182.2)
    annotation (Placement(transformation(extent={{-30,42},{-22,50}})));
  ThermoPower.Water.FlowSplit flowSplit1
    annotation (Placement(transformation(extent={{-106,36},{-94,48}})));
  ThermoPower.Water.SteamTurbineStodola LPTurbine1(
    PRstart=8.42322,
    wnom=182.2,
    pnom=584700,
    eta_iso_nom=0.880655,
    Kt=0.15453698,
    pin(start=584700),
    w(start=182.2))
    annotation (Placement(transformation(extent={{66,24},{86,44}})));
  ThermoPower.Water.SteamTurbineStodola LPTurbine2(
    PRstart=11.442857,
    wnom=165.8,
    pnom=80100,
    eta_iso_nom=0.834375,
    Kt=0.8446199,
    pin(start=80100),
    w(start=165.8))
    annotation (Placement(transformation(extent={{118,24},{138,44}})));
  ThermoPower.Water.FlowSplit flowSplit2
    annotation (Placement(transformation(extent={{90,52},{102,64}})));
  Modelica.Mechanics.Rotational.Sensors.PowerSensor powerSensor
    annotation (Placement(transformation(extent={{-10,10},{10,-10}},
        rotation=270,
        origin={202,24})));
  ThermoPower.Water.FlangeA flangeA
    annotation (Placement(transformation(extent={{-250,36},{-230,56}})));
  ThermoPower.Water.FlangeB flangeB
    annotation (Placement(transformation(extent={{-250,-64},{-230,-44}})));
  ThermoPower.Water.PressDropLin pressDropLin1(R=0.4e5/11.65)
    annotation (Placement(transformation(extent={{-4,-4},{4,4}},
        rotation=270,
        origin={-54,8})));
  ThermoPower.Water.ValveLin valveLin1(Kv=21.32/(45e5 - 7.147e5),   w(start=
          21.32))
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=270,
        origin={10,0})));
  ThermoPower.Water.ValveLin valveLin2(Kv=239.68/1e5,   w(start=239.68))
    annotation (Placement(transformation(extent={{8,-8},{-8,8}},
        rotation=0,
        origin={-160,-54})));
  ThermoPower.Water.FlowSplit flowSplit3
    annotation (Placement(transformation(extent={{-90,38},{-78,50}})));
  ThermoPower.Water.FlangeA flangeA1
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ThermoPower.Water.FlangeB flangeB1
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  ThermoPower.Water.SensP sensP
    annotation (Placement(transformation(extent={{-192,40},{-178,54}})));
  ThermoPower.Water.SensT1 sensT1_1
    annotation (Placement(transformation(extent={{28,56},{40,68}})));
  Control.SensorBus              sensorBus
    annotation (Placement(transformation(extent={{110,90},{130,110}})));
  Control.ActuatorBus              actuatorBus
    annotation (Placement(transformation(extent={{-130,90},{-110,110}})));
  ThermoPower.Water.SensT1 sensT1_2
    annotation (Placement(transformation(extent={{-178,40},{-166,52}})));
  ThermoPower.Water.SensP sensP1
    annotation (Placement(transformation(extent={{38,38},{50,50}})));
  ThermoPower.Water.PressDropLin pressDropLin2(R=0.4e5/24.51) annotation (
      Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={-61,-31})));
  Components.ShellAndTube_BOP hp_fw(
    Nt=831,
    Np=2,
    L=9.5,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=47.523e6,
    ms_nom=24.51,
    mt_nom=239.68,
    ps_nom=754700,
    pt_nom=4694600,
    hsi_nom=2639730,
    hso_nom=700800,
    hti_nom=492840,
    hto_nom=691110,
    dpt_nom=54600,
    Cf_t=862.0677,
    hshell=7009.4956,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (
          gamma=hp_fw.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter)
    annotation (Placement(transformation(extent={{-124,-66},{-98,-42}})));
  Components.ShellAndTube_BOP lp_fw(
    Nt=593,
    Np=2,
    L=11.397,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=37.722e6,
    ms_nom=16.41,
    mt_nom=182.2,
    ps_nom=80100,
    pt_nom=754700,
    hsi_nom=2616620,
    hso_nom=317890,
    hti_nom=164250,
    hto_nom=371280,
    dpt_nom=76300,
    Cf_t=2221.6003,
    hshell=6054.529,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.HeatTransfer2phDB (gamma_b=lp_fw.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter)
    annotation (Placement(transformation(extent={{88,-68},{112,-44}})));

  ThermoPower.Electrical.Generator generator(Pnom=177.268e6, referenceGenerator
      =true) annotation (Placement(transformation(extent={{200,-14},{226,12}})));
  Components.ShellAndTube_BOP rh(
    Nt=12778,
    Np=2,
    L=5.733,
    di=0.01422,
    do=0.01905,
    pitch=27.62e-3,
    Bfrac=0.6,
    CL=1,
    CTP=0.9,
    km=17,
    Qnom=38.902e6,
    ms_nom=21.32,
    mt_nom=182.2,
    ps_nom=4500000,
    pt_nom=714700,
    hsi_nom=2944130,
    hso_nom=1119460,
    hti_nom=2763620,
    hto_nom=2977140,
    dpt_nom=30100,
    Cf_t=2.6980236,
    hshell=909.22925,
    redeclare model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.HeatTransfer2phDB (gamma_b=rh.hshell),
    redeclare model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.DittusBoelter,
    tube(FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Steam))
    annotation (Placement(transformation(extent={{22,42},{-4,66}})));

  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{232,-10},{252,10}})));
  ThermoPower.Water.ValveLin LP_TAV(Kv=182.2/1e5, w(start=182.2))
    annotation (Placement(transformation(extent={{50,36},{62,48}})));
  ThermoPower.Water.SensP sensP2
    annotation (Placement(transformation(extent={{36,-60},{48,-48}})));
equation
  connect(Condenser.waterOut, CNDpump.infl) annotation (Line(points={{166,-24},{
          166,-60.4},{151.4,-60.4}}, color={0,0,255}));
  connect(flowJoin.out, Condenser.steamIn)
    annotation (Line(points={{166,8.4},{166,-4}}, color={0,0,255}));
  connect(pressDropLin5.outlet, flowJoin.in2)
    annotation (Line(points={{146,-86},{184,-86},{184,22},{168.4,22},{168.4,
          15.6}},                                  color={0,0,255}));
  connect(moistureSeparator.steam, pressDropLin7.inlet)
    annotation (Line(points={{-48,46},{-30,46}}, color={0,0,255}));
  connect(HPTurbine.outlet, flowSplit1.in1)
    annotation (Line(points={{-112,42},{-103.6,42}}, color={0,0,255}));
  connect(HPTurbine.shaft_b, LPTurbine1.shaft_a)
    annotation (Line(points={{-113.6,34},{69.4,34}}, color={0,0,0}));
  connect(LPTurbine1.outlet, flowSplit2.in1) annotation (Line(points={{84,42},{86,
          42},{86,58},{92.4,58}}, color={0,0,255}));
  connect(flowSplit2.out1, LPTurbine2.inlet) annotation (Line(points={{99.6,60.4},
          {120,60.4},{120,42}}, color={0,0,255}));
  connect(LPTurbine1.shaft_b, LPTurbine2.shaft_a)
    annotation (Line(points={{82.4,34},{121.4,34}}, color={0,0,0}));
  connect(LPTurbine2.shaft_b, powerSensor.flange_a)
    annotation (Line(points={{134.4,34},{202,34}}, color={0,0,0}));
  connect(flangeA, flowSplit.in1)
    annotation (Line(points={{-240,46},{-205.6,46}}, color={0,0,255}));
  connect(HP_TAV.outlet, HPTurbine.inlet)
    annotation (Line(points={{-136,43},{-136,42},{-128,42}}, color={0,0,255}));
  connect(moistureSeparator.drain, pressDropLin1.inlet)
    annotation (Line(points={{-54,40},{-54,12}}, color={0,0,255}));
  connect(valveLin2.outlet, flangeB) annotation (Line(points={{-168,-54},{-240,
          -54}},            color={0,0,255}));
  connect(flowSplit1.out1, flowSplit3.in1) annotation (Line(points={{-96.4,44.4},
          {-96,44.4},{-96,44},{-87.6,44}},         color={0,0,255}));
  connect(flowSplit3.out2, flangeB1) annotation (Line(points={{-80.4,41.6},{
          -80.4,-100},{-60,-100}}, color={0,0,255}));
  connect(FWpump.infl, FWtank.outlet[1]) annotation (Line(points={{-38.4,-59.6},
          {3,-59.6},{3,-47.75}}, color={0,0,255}));
  connect(valveLin1.outlet, FWtank.inlet[2]) annotation (Line(points={{10,-8},{10,
          -20},{4,-20},{4,-24},{3,-24},{3,-29.62}}, color={0,0,255}));
  connect(pressDropLin1.outlet, FWtank.inlet[3]) annotation (Line(points={{-54,4},
          {-54,-20},{2,-20},{2,-29.25},{3,-29.25}}, color={0,0,255}));
  connect(flangeA1, FWtank.inlet[5]) annotation (Line(points={{60,-100},{60,-20},
          {24,-20},{24,-28.51},{3,-28.51}}, color={0,0,255}));
  connect(actuatorBus.Kv_HP_TAV, HP_TAV.cmd) annotation (Line(
      points={{-120,100},{-120,84},{-145,84},{-145,50.2}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_RH, valveLin1.cmd) annotation (Line(
      points={{-120,100},{-120,84},{28,84},{28,0},{16.4,0}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_SGin, valveLin2.cmd) annotation (Line(
      points={{-120,100},{-120,84},{-160,84},{-160,-47.6}},
      color={80,200,120},
      thickness=0.5));
  connect(LPTurbine2.outlet, flowJoin.in1) annotation (Line(points={{136,42},{136,
          60},{164,60},{164,15.6},{163.6,15.6}}, color={0,0,255}));
  connect(sensorBus.P_SG, sensP.p) annotation (Line(
      points={{120,100},{120,88},{-179.4,88},{-179.4,51.2}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.T_LPT, sensT1_1.T) annotation (Line(
      points={{120,100},{120,88},{38.8,88},{38.8,65.6}},
      color={255,219,88},
      thickness=0.5));
  connect(flowSplit.out2, sensP.flange) annotation (Line(points={{-198.4,43.6},
          {-198.4,44.2},{-185,44.2}}, color={0,0,255}));
  connect(sensP.flange, HP_TAV.inlet) annotation (Line(points={{-185,44.2},{-178,
          44.2},{-178,43},{-154,43}}, color={0,0,255}));
  connect(sensP.flange, sensT1_2.flange) annotation (Line(points={{-185,44.2},{
          -178,44.2},{-178,43.6},{-172,43.6}}, color={0,0,255}));
  connect(sensorBus.T_SGout, sensT1_2.T) annotation (Line(
      points={{120,100},{120,88},{-167.2,88},{-167.2,49.6}},
      color={255,219,88},
      thickness=0.5));
  connect(flowSplit3.out1, moistureSeparator.inlet) annotation (Line(points={{
          -80.4,46.4},{-68,46.4},{-68,46},{-60,46}}, color={0,0,255}));
  connect(pressDropLin2.outlet, FWtank.inlet[4]) annotation (Line(points={{-56,-31},
          {-56,-32},{-20,-32},{-20,-20},{3,-20},{3,-28.88}},
                                                   color={0,0,255}));
  connect(hp_fw.flangeB1, valveLin2.inlet)
    annotation (Line(points={{-121.14,-54},{-152,-54}}, color={0,0,255}));
  connect(hp_fw.flangeA1, FWpump.outfl) annotation (Line(points={{-100.6,-54},{-96,
          -54},{-96,-53.6},{-55.2,-53.6}}, color={0,0,255}));
  connect(flowSplit1.out2, hp_fw.flangeA) annotation (Line(points={{-96.4,39.6},
          {-96.4,10},{-111,10},{-111,-44.64}}, color={0,0,255}));
  connect(hp_fw.flangeB, pressDropLin2.inlet) annotation (Line(points={{-111,
          -63.6},{-112,-63.6},{-112,-74},{-88,-74},{-88,-31},{-66,-31}},
                                                     color={0,0,255}));
  connect(lp_fw.flangeB, pressDropLin5.inlet) annotation (Line(points={{100,-65.6},
          {100,-86},{134,-86}}, color={0,0,255}));
  connect(lp_fw.flangeA1, CNDpump.outfl) annotation (Line(points={{109.6,-56},{133.2,
          -56},{133.2,-53.9}}, color={0,0,255}));
  connect(lp_fw.flangeB1, FWtank.inlet[1]) annotation (Line(points={{90.64,-56},
          {28,-56},{28,-29.99},{3,-29.99}}, color={0,0,255}));
  connect(flowSplit2.out2, lp_fw.flangeA) annotation (Line(points={{99.6,55.6},{
          99.6,-14},{100,-14},{100,-46.64}}, color={0,0,255}));
  connect(pressDropLin7.outlet, rh.flangeA1) annotation (Line(points={{-22,46},
          {-16,46},{-16,54},{-1.4,54}}, color={0,0,255}));
  connect(rh.flangeB, valveLin1.inlet)
    annotation (Line(points={{9,44.4},{9,8},{10,8}}, color={0,0,255}));
  connect(rh.flangeB1, sensT1_1.flange) annotation (Line(points={{19.14,54},{24,
          54},{24,59.6},{34,59.6}}, color={0,0,255}));
  connect(flowSplit.out1, rh.flangeA) annotation (Line(points={{-198.4,48.4},{-198.4,
          82},{9,82},{9,63.36}}, color={0,0,255}));
  connect(powerSensor.flange_b, generator.shaft)
    annotation (Line(points={{202,14},{202,-1},{201.82,-1}}, color={0,0,0}));
  connect(generator.port, powerConnection) annotation (Line(
      points={{224.18,-1},{224.18,0},{242,0}},
      color={0,0,255},
      thickness=0.5));
  connect(LP_TAV.outlet, LPTurbine1.inlet)
    annotation (Line(points={{62,42},{68,42}}, color={0,0,255}));
  connect(actuatorBus.Kv_LP_TAV, LP_TAV.cmd) annotation (Line(
      points={{-120,100},{-120,84},{56,84},{56,46.8}},
      color={80,200,120},
      thickness=0.5));
  connect(LP_TAV.inlet, sensT1_1.flange) annotation (Line(points={{50,42},{40,42},
          {40,59.6},{34,59.6}}, color={0,0,255}));
  connect(LP_TAV.inlet, sensP1.flange) annotation (Line(points={{50,42},{47,42},
          {47,41.6},{44,41.6}}, color={0,0,255}));
  connect(actuatorBus.N_HPpump, FWpump.in_n) annotation (Line(
      points={{-120,100},{-120,84},{-44.88,84},{-44.88,-52.4}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.N_LPpump, CNDpump.in_n) annotation (Line(
      points={{-120,100},{-120,84},{144.38,84},{144.38,-52.6}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.Pel_out, powerSensor.power) annotation (Line(
      points={{120,100},{120,88},{212,88},{212,46},{213,46},{213,32}},
      color={255,219,88},
      thickness=0.5));
  connect(lp_fw.flangeB1, sensP2.flange) annotation (Line(points={{90.64,-56},{
          66.32,-56},{66.32,-56.4},{42,-56.4}}, color={0,0,255}));
  connect(sensorBus.P_FWtank, sensP2.p) annotation (Line(
      points={{120,100},{120,88},{64,88},{64,-50.4},{46.8,-50.4}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.P_LPTin, sensP1.p) annotation (Line(
      points={{120,100},{120,88},{50,88},{50,47.6},{48.8,47.6}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-240,
            -100},{240,100}}), graphics={
        Rectangle(
          extent={{-240,100},{240,-100}},
          lineColor={0,140,72},
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-234,94},{234,-94}},
          lineColor={0,140,72},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-56,42},{84,-40}},
          textColor={0,140,72},
          textString="BOP")}),
                          Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-240,-100},{240,100}})));
end BOPsimple_fluid;
