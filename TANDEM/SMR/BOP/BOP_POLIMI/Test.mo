within TANDEM.SMR.BOP.BOP_POLIMI;
package Test
  model Test_BOP_idealBC
    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    BOPsimple_fluid bop
      annotation (Placement(transformation(extent={{-6,-14},{72,28}})));
    ThermoPower.Water.ValveLin valveLin(Kv=50/0.4e5) annotation (Placement(
          transformation(
          extent={{10,-10},{-10,10}},
          rotation=270,
          origin={84,-36})));
    Modelica.Blocks.Sources.Ramp ramp(
      height=0,
      duration=900,
      offset=0.01,
      startTime=100)
      annotation (Placement(transformation(extent={{166,-46},{146,-26}})));
    ThermoPower.Water.Header header(
      V=10,
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
      pstart=754700,
      hstart=2639730,
      noInitialPressure=false,
      noInitialEnthalpy=false)
                      annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=0,
          origin={36,-52})));
    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow
      annotation (Placement(transformation(extent={{-6,-86},{14,-66}})));
    Modelica.Blocks.Sources.Ramp ramp1(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-50,-86},{-30,-66}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=1.2,
      Ti=1,
      yMax=1,
      yMin=0.01,
      initType=Modelica.Blocks.Types.Init.InitialOutput,
      y_start=0.01)
      annotation (Placement(transformation(extent={{128,-46},{108,-26}})));
    ThermoPower.Water.SensW sensW
      annotation (Placement(transformation(extent={{54,-46},{74,-66}})));
    ThermoPower.Water.SourcePressure sourcePressure(p0=4500000,
      h=2944.13e3)
      annotation (Placement(transformation(extent={{-94,6},{-74,26}})));
    ThermoPower.Water.SinkPressure sinkPressure(p0=4500000, h=691110)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-86,-18})));
    ThermoPower.Water.PressDropLin pressDropLin1(R=0.4e5/239.68)
                                                               annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=180,
          origin={-53,-19})));
    Control.BOPcontroller_SST controller
      annotation (Placement(transformation(extent={{16,50},{42,74}})));
    Modelica.Blocks.Sources.Ramp ramp2(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-74,64},{-54,84}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{118,-14},{98,6}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={142,-4})));
  equation
    connect(valveLin.outlet, bop.flangeA1) annotation (Line(points={{84,-26},{84,
            -16},{76,-16},{76,-22},{42.75,-22},{42.75,-14}},     color={0,0,255}));
    connect(bop.flangeB1, header.inlet) annotation (Line(points={{23.25,-14},{22,
            -14},{22,-26},{2,-26},{2,-52},{25.9,-52}},      color={0,0,255}));
    connect(header.thermalPort, prescribedHeatFlow.port)
      annotation (Line(points={{36,-57.7},{36,-76},{14,-76}},color={191,0,0}));
    connect(ramp1.y, prescribedHeatFlow.Q_flow)
      annotation (Line(points={{-29,-76},{-6,-76}},  color={0,0,127}));
    connect(PID.y, valveLin.cmd)
      annotation (Line(points={{107,-36},{92,-36}}, color={0,0,127}));
    connect(ramp.y, PID.u_s)
      annotation (Line(points={{145,-36},{130,-36}}, color={0,0,127}));
    connect(header.outlet, sensW.inlet)
      annotation (Line(points={{46,-52},{58,-52}}, color={0,0,255}));
    connect(sensW.outlet, valveLin.inlet)
      annotation (Line(points={{70,-52},{84,-52},{84,-46}}, color={0,0,255}));
    connect(sensW.w, PID.u_m)
      annotation (Line(points={{72,-62},{118,-62},{118,-48}}, color={0,0,127}));
    connect(pressDropLin1.outlet,sinkPressure. flange)
      annotation (Line(points={{-60,-19},{-60,-18},{-76,-18}}, color={0,0,255}));
    connect(sourcePressure.flange, bop.flangeA) annotation (Line(points={{-74,16},
            {-72,16.66},{-6,16.66}},                                color={0,0,
            255}));
    connect(pressDropLin1.inlet, bop.flangeB) annotation (Line(points={{-46,-19},
            {-44,-19},{-44,-18},{-36,-18},{-36,-4.34},{-6,-4.34}},    color={0,0,
            255}));
    connect(controller.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{24,50},{24,42},{16,42},{16,28},{13.5,28}},
        color={80,200,120},
        thickness=0.5));
    connect(controller.sensorBus, bop.sensorBus) annotation (Line(
        points={{36,50},{36,42},{52.5,42},{52.5,28}},
        color={255,219,88},
        thickness=0.5));
    connect(ramp2.y, controller.Kv_HP_TAV) annotation (Line(points={{-53,74},{8,
            74},{8,71},{15.2,71}},                 color={0,0,127}));
    connect(controller.Kv_SGin, ramp2.y) annotation (Line(points={{15.2,67},{
            -40,67},{-40,74},{-53,74}},         color={0,0,127}));
    connect(controller.Kv_RH, ramp2.y) annotation (Line(points={{15.4,63},{-40,
            63},{-40,74},{-53,74}},              color={0,0,127}));
    connect(controller.Kv_LP_TAV, ramp2.y) annotation (Line(points={{15.4,59},{
            -40,59},{-40,74},{-53,74}},               color={0,0,127}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{72.325,7},{108,7},{108,4.6}},
        color={0,0,255},
        thickness=0.5));
    connect(load.referencePower, realExpression.y)
      annotation (Line(points={{111.3,-4},{131,-4}}, color={0,0,127}));
    connect(controller.N_LPpump, ramp2.y) annotation (Line(points={{15.4,51.2},
            {-40,51.2},{-40,74},{-53,74}}, color={0,0,127}));
    connect(controller.N_HPpump, ramp2.y) annotation (Line(points={{15.4,55},{
            -40,55},{-40,74},{-53,74}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})));
  end Test_BOP_idealBC;

  model Test_BOP_SG


    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    BOPsimple_fluid bop
      annotation (Placement(transformation(extent={{-6,-14},{72,28}})));
    ThermoPower.Water.ValveLin valveLin(Kv=50/0.4e5) annotation (Placement(
          transformation(
          extent={{10,-10},{-10,10}},
          rotation=270,
          origin={84,-36})));
    Modelica.Blocks.Sources.Ramp ramp(
      height=0,
      duration=900,
      offset=0.01,
      startTime=100)
      annotation (Placement(transformation(extent={{166,-46},{146,-26}})));
    ThermoPower.Water.Header header(
      V=10,
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
      pstart=754700,
      hstart=2639730,
      noInitialPressure=false,
      noInitialEnthalpy=false)
                      annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=0,
          origin={36,-52})));
    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow
      annotation (Placement(transformation(extent={{-6,-86},{14,-66}})));
    Modelica.Blocks.Sources.Ramp ramp1(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-50,-86},{-30,-66}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=1.2,
      Ti=1,
      yMax=1,
      yMin=0.01,
      initType=Modelica.Blocks.Types.Init.InitialOutput,
      y_start=0.01)
      annotation (Placement(transformation(extent={{128,-46},{108,-26}})));
    ThermoPower.Water.SensW sensW
      annotation (Placement(transformation(extent={{54,-46},{74,-66}})));
    Control.BOPcontroller_SST controller
      annotation (Placement(transformation(extent={{16,50},{42,74}})));
    Modelica.Blocks.Sources.Ramp ramp2(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-62,58},{-42,78}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{118,-14},{98,6}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={142,-4})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Components.CompactSG
              compactSG_init(
      Nodes=10,
      L=2,
      Nch=28818,
      mdot2=240,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      redeclare model Primary_HT =
          ThermoPower.Thermal.HeatTransferFV.DittusBoelter,
      redeclare model Secondary_HT =
          ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (
            gamma(start=15000, fixed=false)),
      Secondary(noInitialPressure=false),
      Ts_out(T(start=300 + 273.15, fixed=true)))
      annotation (Placement(transformation(extent={{-94,-16},{-70,12}})));
    ThermoPower.Water.SourceMassFlow sourceMassFlow(
      w0=3700,
      p0=15000000,
      use_T=true,
      T=597.65) annotation (Placement(transformation(extent={{-140,12},{-120,32}})));
    ThermoPower.Water.SinkPressure sinkPressure(p0=15000000) annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={-128,-24})));
  equation
    connect(valveLin.outlet, bop.flangeA1) annotation (Line(points={{84,-26},{84,
            -16},{76,-16},{76,-22},{42.75,-22},{42.75,-14}},     color={0,0,255}));
    connect(bop.flangeB1, header.inlet) annotation (Line(points={{23.25,-14},{22,
            -14},{22,-26},{2,-26},{2,-52},{25.9,-52}},      color={0,0,255}));
    connect(header.thermalPort, prescribedHeatFlow.port)
      annotation (Line(points={{36,-57.7},{36,-76},{14,-76}},color={191,0,0}));
    connect(ramp1.y, prescribedHeatFlow.Q_flow)
      annotation (Line(points={{-29,-76},{-6,-76}},  color={0,0,127}));
    connect(PID.y, valveLin.cmd)
      annotation (Line(points={{107,-36},{92,-36}}, color={0,0,127}));
    connect(ramp.y, PID.u_s)
      annotation (Line(points={{145,-36},{130,-36}}, color={0,0,127}));
    connect(header.outlet, sensW.inlet)
      annotation (Line(points={{46,-52},{58,-52}}, color={0,0,255}));
    connect(sensW.outlet, valveLin.inlet)
      annotation (Line(points={{70,-52},{84,-52},{84,-46}}, color={0,0,255}));
    connect(sensW.w, PID.u_m)
      annotation (Line(points={{72,-62},{118,-62},{118,-48}}, color={0,0,127}));
    connect(controller.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{24,50},{24,42},{16,42},{16,28},{13.5,28}},
        color={80,200,120},
        thickness=0.5));
    connect(controller.sensorBus, bop.sensorBus) annotation (Line(
        points={{36,50},{36,42},{52.5,42},{52.5,28}},
        color={255,219,88},
        thickness=0.5));
    connect(controller.Kv_SGin, ramp2.y) annotation (Line(points={{15.2,67},{
            -36,67},{-36,68},{-41,68}},
                                color={0,0,127}));
    connect(controller.Kv_RH, ramp2.y) annotation (Line(points={{15.4,63},{-32,
            63},{-32,66},{-36,66},{-36,68},{-41,68}},
                                                  color={0,0,127}));
    connect(controller.Kv_LP_TAV, ramp2.y) annotation (Line(points={{15.4,59},{
            -32,59},{-32,66},{-36,66},{-36,68},{-41,68}},
                                                        color={0,0,127}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{72.325,7},{108,7},{108,4.6}},
        color={0,0,255},
        thickness=0.5));
    connect(load.referencePower, realExpression.y)
      annotation (Line(points={{111.3,-4},{131,-4}}, color={0,0,127}));
    connect(sourceMassFlow.flange,compactSG_init. flangeA) annotation (Line(
          points={{-120,22},{-102,22},{-102,6.9091},{-91.6,6.9091}},
                       color={0,0,255}));
    connect(sinkPressure.flange,compactSG_init. flangeB) annotation (Line(points={{-118,
            -24},{-102,-24},{-102,-13.4545},{-91.36,-13.4545}},     color={0,0,
            255}));
    connect(compactSG_init.flangeA1, bop.flangeB) annotation (Line(points={{-72.4,
            -13.4545},{-72.4,-28},{-20,-28},{-20,-4.34},{-6,-4.34}}, color={0,0,255}));
    connect(compactSG_init.flangeB1, bop.flangeA) annotation (Line(points={{-72.64,
            6.90909},{-72.64,38},{-22,38},{-22,16.66},{-6,16.66}}, color={0,0,255}));
    connect(controller.Kv_HP_TAV, ramp2.y) annotation (Line(points={{15.2,71},{-32,
            71},{-32,66},{-36,66},{-36,68},{-41,68}}, color={0,0,127}));
    connect(controller.N_LPpump, ramp2.y) annotation (Line(points={{15.4,51.2},
            {-32,51.2},{-32,66},{-36,66},{-36,68},{-41,68}}, color={0,0,127}));
    connect(controller.N_HPpump, ramp2.y) annotation (Line(points={{15.4,55},{
            -32,55},{-32,66},{-36,66},{-36,68},{-41,68}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})));
  end Test_BOP_SG;

  model Test_BOP_NSSS

    //parameter Real HTC(start=15000,fixed=false);

    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    BOPsimple_fluid bop
      annotation (Placement(transformation(extent={{-6,-14},{72,28}})));
    ThermoPower.Water.ValveLin valveLin(Kv=50/0.4e5) annotation (Placement(
          transformation(
          extent={{10,-10},{-10,10}},
          rotation=270,
          origin={84,-36})));
    Modelica.Blocks.Sources.Ramp ramp(
      height=0,
      duration=900,
      offset=0.01,
      startTime=100)
      annotation (Placement(transformation(extent={{166,-46},{146,-26}})));
    ThermoPower.Water.Header header(
      V=10,
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
      pstart=754700,
      hstart=2639730,
      noInitialPressure=false,
      noInitialEnthalpy=false)
                      annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=0,
          origin={36,-52})));
    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow
      annotation (Placement(transformation(extent={{-6,-86},{14,-66}})));
    Modelica.Blocks.Sources.Ramp ramp1(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-52,-86},{-32,-66}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=1.2,
      Ti=1,
      yMax=1,
      yMin=0.01,
      initType=Modelica.Blocks.Types.Init.InitialOutput,
      y_start=0.01)
      annotation (Placement(transformation(extent={{128,-46},{108,-26}})));
    ThermoPower.Water.SensW sensW
      annotation (Placement(transformation(extent={{54,-46},{74,-66}})));
    Control.BOPcontroller_SST controller
      annotation (Placement(transformation(extent={{16,50},{42,74}})));
    Modelica.Blocks.Sources.Ramp ramp2(
      height=0,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-58,78},{-38,98}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{118,-14},{98,6}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={142,-4})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-104,54},{-66,86}})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSsimplified_fluid nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      htc2=14654.18,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      SG(Secondary(noInitialPressure=false)))
      annotation (Placement(transformation(extent={{-114,-12},{-56,24}})));
  equation
    connect(valveLin.outlet, bop.flangeA1) annotation (Line(points={{84,-26},{84,
            -16},{76,-16},{76,-22},{42.75,-22},{42.75,-14}},     color={0,0,255}));
    connect(bop.flangeB1, header.inlet) annotation (Line(points={{23.25,-14},{22,
            -14},{22,-26},{2,-26},{2,-52},{25.9,-52}},      color={0,0,255}));
    connect(header.thermalPort, prescribedHeatFlow.port)
      annotation (Line(points={{36,-57.7},{36,-76},{14,-76}},color={191,0,0}));
    connect(ramp1.y, prescribedHeatFlow.Q_flow)
      annotation (Line(points={{-31,-76},{-6,-76}},  color={0,0,127}));
    connect(PID.y, valveLin.cmd)
      annotation (Line(points={{107,-36},{92,-36}}, color={0,0,127}));
    connect(ramp.y, PID.u_s)
      annotation (Line(points={{145,-36},{130,-36}}, color={0,0,127}));
    connect(header.outlet, sensW.inlet)
      annotation (Line(points={{46,-52},{58,-52}}, color={0,0,255}));
    connect(sensW.outlet, valveLin.inlet)
      annotation (Line(points={{70,-52},{84,-52},{84,-46}}, color={0,0,255}));
    connect(sensW.w, PID.u_m)
      annotation (Line(points={{72,-62},{118,-62},{118,-48}}, color={0,0,127}));
    connect(controller.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{24,50},{24,42},{16,42},{16,28},{13.5,28}},
        color={80,200,120},
        thickness=0.5));
    connect(controller.sensorBus, bop.sensorBus) annotation (Line(
        points={{36,50},{36,42},{52.5,42},{52.5,28}},
        color={255,219,88},
        thickness=0.5));
    connect(controller.Kv_SGin, ramp2.y) annotation (Line(points={{15.2,67},{
            -34,67},{-34,88},{-37,88}},
                                color={0,0,127}));
    connect(controller.Kv_RH, ramp2.y) annotation (Line(points={{15.4,63},{-34,
            63},{-34,88},{-37,88}},
                                color={0,0,127}));
    connect(controller.Kv_LP_TAV, ramp2.y) annotation (Line(points={{15.4,59},{
            -34,59},{-34,88},{-37,88}},
                                      color={0,0,127}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{72.325,7},{108,7},{108,4.6}},
        color={0,0,255},
        thickness=0.5));
    connect(load.referencePower, realExpression.y)
      annotation (Line(points={{111.3,-4},{131,-4}}, color={0,0,127}));
    connect(controller.Kv_HP_TAV, ramp2.y) annotation (Line(points={{15.2,71},{-34,
            71},{-34,88},{-37,88}}, color={0,0,127}));
    connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
        points={{-96.4,54},{-96,54},{-96,40},{-99.5,40},{-99.5,23.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
        points={{-73.6,54},{-74,54},{-74,42},{-70.5,42},{-70.5,23.64}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.flangeB, bop.flangeA) annotation (Line(points={{-56,16.8},{-52,16.8},
            {-52,16.66},{-6,16.66}}, color={0,0,255}));
    connect(nsss.flangeA, bop.flangeB) annotation (Line(points={{-56,-5.16},{-46,-5.16},
            {-46,-4.34},{-6,-4.34}}, color={0,0,255}));
    connect(controller.N_HPpump, ramp2.y) annotation (Line(points={{15.4,55},{
            -34,55},{-34,88},{-37,88}}, color={0,0,127}));
    connect(controller.N_LPpump, ramp2.y) annotation (Line(points={{15.4,51.2},
            {-36,51.2},{-36,56},{-32,56},{-32,55},{-34,55},{-34,88},{-37,88}},
          color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})));
  end Test_BOP_NSSS;

  model Test_BOP_NSSS_TRANS

    //parameter Real HTC(start=15000,fixed=false);

    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    BOPsimple_fluid bop
      annotation (Placement(transformation(extent={{-6,-14},{72,28}})));
    ThermoPower.Water.ValveLin valveLin(Kv=50/0.4e5) annotation (Placement(
          transformation(
          extent={{10,-10},{-10,10}},
          rotation=270,
          origin={84,-36})));
    Modelica.Blocks.Sources.Ramp ramp(
      height=29,
      duration=900,
      offset=0.01,
      startTime=100)
      annotation (Placement(transformation(extent={{166,-46},{146,-26}})));
    ThermoPower.Water.Header header(
      V=10,
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
      pstart=754700,
      hstart=2639730,
      noInitialPressure=false,
      noInitialEnthalpy=false)
                      annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=0,
          origin={36,-52})));
    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow
      annotation (Placement(transformation(extent={{-6,-86},{14,-66}})));
    Modelica.Blocks.Sources.Ramp ramp1(
      height=-54e6,
      duration=900,
      offset=0,
      startTime=100)
      annotation (Placement(transformation(extent={{-52,-86},{-32,-66}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=1.2,
      Ti=1,
      yMax=1,
      yMin=0.01,
      initType=Modelica.Blocks.Types.Init.InitialOutput,
      y_start=0.01)
      annotation (Placement(transformation(extent={{128,-46},{108,-26}})));
    ThermoPower.Water.SensW sensW
      annotation (Placement(transformation(extent={{54,-46},{74,-66}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{118,-14},{98,6}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={142,-4})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-104,54},{-66,86}})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSsimplified_fluid nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      htc2=14654.18,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      SG(Secondary(noInitialPressure=false)))
      annotation (Placement(transformation(extent={{-114,-12},{-56,24}})));
    Control.BOPcontroller_ex2 BOP_ctrl
      annotation (Placement(transformation(extent={{16,46},{48,74}})));
  equation
    connect(valveLin.outlet, bop.flangeA1) annotation (Line(points={{84,-26},{
            84,-16},{76,-16},{76,-22},{42.75,-22},{42.75,-14}},  color={0,0,255}));
    connect(bop.flangeB1, header.inlet) annotation (Line(points={{23.25,-14},{
            22,-14},{22,-26},{2,-26},{2,-52},{25.9,-52}},   color={0,0,255}));
    connect(header.thermalPort, prescribedHeatFlow.port)
      annotation (Line(points={{36,-57.7},{36,-76},{14,-76}},color={191,0,0}));
    connect(ramp1.y, prescribedHeatFlow.Q_flow)
      annotation (Line(points={{-31,-76},{-6,-76}},  color={0,0,127}));
    connect(PID.y, valveLin.cmd)
      annotation (Line(points={{107,-36},{92,-36}}, color={0,0,127}));
    connect(ramp.y, PID.u_s)
      annotation (Line(points={{145,-36},{130,-36}}, color={0,0,127}));
    connect(header.outlet, sensW.inlet)
      annotation (Line(points={{46,-52},{58,-52}}, color={0,0,255}));
    connect(sensW.outlet, valveLin.inlet)
      annotation (Line(points={{70,-52},{84,-52},{84,-46}}, color={0,0,255}));
    connect(sensW.w, PID.u_m)
      annotation (Line(points={{72,-62},{118,-62},{118,-48}}, color={0,0,127}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{72.325,7},{108,7},{108,4.6}},
        color={0,0,255},
        thickness=0.5));
    connect(load.referencePower, realExpression.y)
      annotation (Line(points={{111.3,-4},{131,-4}}, color={0,0,127}));
    connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
        points={{-96.4,54},{-96,54},{-96,40},{-99.5,40},{-99.5,23.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
        points={{-73.6,54},{-74,54},{-74,42},{-70.5,42},{-70.5,23.64}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.flangeB, bop.flangeA) annotation (Line(points={{-56,16.8},{-52,
            16.8},{-52,16.66},{-6,16.66}},
                                     color={0,0,255}));
    connect(nsss.flangeA, bop.flangeB) annotation (Line(points={{-56,-5.16},{
            -46,-5.16},{-46,-4.34},{-6,-4.34}},
                                     color={0,0,255}));
    connect(BOP_ctrl.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{26,46},{26,42},{13.5,42},{13.5,28}},
        color={80,200,120},
        thickness=0.5));
    connect(BOP_ctrl.sensorBus, bop.sensorBus) annotation (Line(
        points={{38,46},{38,44},{52.5,44},{52.5,28}},
        color={255,219,88},
        thickness=0.5));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})));
  end Test_BOP_NSSS_TRANS;

  model Test_BOPth_0Dsource

    //parameter Real HTC(start=15000,fixed=false);

    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{120,-10},{100,10}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={144,0})));
    Control.BOPcontroller_ex3 BOP_ctrl(
      PID(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.5227240775694498))),
      PID1(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.0001948147728988664))),
      PID2(initType=Modelica.Blocks.Types.Init.InitialOutput),
      PID3(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.019371085366220786))),
      PID4(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.001088129835934586))),
      const4(k=7.547e5),
      PID5(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.010324016636530323))),
      add1(u2(start=0.0001948147728988664)),
      add8(u1(start=0.001088129835934586)))
      annotation (Placement(transformation(extent={{10,48},{42,76}})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-193.5,-28.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-192,-16})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-175,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro1
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={-195,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro2
      annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-179,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP adaptorRealModelicaTSP1
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-36})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-16})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.195832316411424E-07, displayUnit="bar"),
        DPf(start=0.05966626304582995, displayUnit="bar"),
        Ec(h(start=2639206.4220304517)),
        Sc(h_vol(start=711100.0990898232)),
        DPfc(start=1.1937225602255773E-07, displayUnit="bar"),
        DPff(start=0.05966602553248424, displayUnit="bar")),
      TCond_Tap(C2(h_vol(start=711100.0990898232))),
      Vv_Tap(C2(h_vol(start=711100.0990898232))),
      Condensate_Tapping_Out(h_vol(start=711100.0990898232)),
      Steam_Tapping_In(h(start=2639206.422030454)),
      Vol_Tap(h(start=2639206.4220304517)))
      annotation (Placement(transformation(extent={{-186,-74},{-172,-60}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro3(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-96,-92})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro4
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-94,-66})));

    BOPdyn_thermal bop(
      valveLiq1(state(d(start=972.0341639710512, displayUnit="g/cm3"))),
      LP_TAV(allowFlowReversal=false, dp(start=49973.89202029549, displayUnit=
              "bar")),
      CNDpump(dp(start=784299.8869726756, displayUnit="bar"), h(start=
              164353.24411294493)),
      FWpump(
        dp(start=4036093.3876811517, displayUnit="bar"),
        h(start=470727.0044986359),
        inletFluidState(h(start=465419.35915172385), p(start=674700.0,
              displayUnit="bar")),
        q_single(start=0.2524955423236547)),
      FWtank(hout(start=465419.35915172385), inlet(m_flow(start={
                182.55809058420135,20.779072368963686,11.665027087211513,
                25.003329372318213,0.0010000000474974513}))),
      HP_TAV(
        dp(start=100369.6181962993, displayUnit="bar"),
        w(start=216.87287270012862),
        fluidState(h(start=2943518.375611178)),
        outlet(p(start=4415967.665083364, displayUnit="bar"))),
      LPTurbine1(
        steamState_in(p(start=637334.9462045254, displayUnit="bar")),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.8914142171471136)),
      LPTurbine2(
        w(start=166.054858090678),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        steamState_in(p(start=79249.28507813525, displayUnit="bar")),
        eta_iso(start=0.8459044124543271)),
      hp_fw(shell_2ph(h(start={2639206.422030454,2574295.640026931,
                2492337.8309075935,2388883.076562288,2258341.2160948976,
                2093708.0242936306,1886242.892363846,1625107.7732566881,
                1297002.5867260818,885888.8659001164,558587.6037271791}))),
      lp_fw(shell_2ph(h(start={2622037.533721652,2557054.8944600723,
                2474310.369945459,2369067.504761911,2235409.4443701273,
                2066006.3814000231,1851888.8822487814,1582284.2344484632,
                1244635.7050382618,825046.9553612076,333357.2131973008}))),
      mixer(h(start=2763183.6460852847)),
      moistureSeparator(steam(h_outflow(start=2763183.646085285), p(start=
                707810.5498582933, displayUnit="bar")), inlet(p(start=
                707248.8182221306, displayUnit="bar"))),
      pressDropLin3(state(d(start=902.151327199094, displayUnit="g/cm3"))),
      rh(shell_2ph(h(start={2943518.375611178,2890011.039717612,
                2854226.3056660737,2829819.535999826,2812938.386602925,
                2768909.3453550083,2684090.3218274876,2534634.226356297,
                2272544.9896826628,1816210.8525184214,1049537.3503093375}))),
      valve_SGin(dp(start=100419.5305422004, displayUnit="bar"), fluidState(p(
              start=4656083.644885505, displayUnit="bar"))),
      HPTurbine(
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.872172529115319)),
      sensT1_2(T(start=300 + 273.15)),
      LP_TAV1(dp(start=993.4137074937462, displayUnit="bar")),
      flangeA1(h_outflow(start=710502.0457445189)),
      HTC(start=14808.052479522732, fixed=true),
      Secondary(heatTransfer(gamma(start=14808.052479522732)), h(start={
              687481.1065937818,1066212.0575940826,1343099.4073463783,
              1586653.8993621576,1770336.8416530385,1962437.6326528054,
              2196071.879168668,2420041.9002103126,2620821.9836065182,
              2819837.144412142,2943518.375623026})))
      annotation (Placement(transformation(extent={{-22,-12},{72,34}})));
    ThermoPower.Thermal.HeatSource1DFV heatSource1DFV(Nw=10) annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={-48,10})));
    Modelica.Blocks.Sources.Constant const(k=540e6)
      annotation (Placement(transformation(extent={{-82,0},{-62,20}})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-102,104})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-105.5,91.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-81,93})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro5 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-99,79})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro6
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-85,77})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-56,84})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-56,104})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog
                     HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.479524994983914E-08, displayUnit="bar"),
        DPf(start=0.05963019607638345, displayUnit="bar"),
        Sc(h_vol(start=1118785.4784898118)),
        Ec(h(start=2943650.4153317516))),
      Vv_Tap(C2(h_vol(start=1118785.4784898118))),
      TCond_Tap(C2(h_vol(start=1118785.4784898118)))) annotation (Placement(transformation(extent={{-84,46},
              {-70,60}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro7
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-88,-24})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro8(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-88,-36})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={147.5,-32})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-34})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={149,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro9
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={133,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro10
                                                                                     annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={131,-35})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={148,-66})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-66})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog_LP
                        HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.006183525146145E-06, displayUnit="bar"),
        DPf(start=0.059773554781532685, displayUnit="bar"),
        Ec(h(start=2621755.7202959014)),
        Sc(h_vol(start=393747.5804603163))),
      TCond_Tap_LP(C2(h_vol(start=393747.5804603163))),
      Vv_Tap_LP(C2(h_vol(start=393747.5804603163))))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={111,-53})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro11
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={84,-56})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro12(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={84,-50})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_HeatInput
                           HX_HeatInput(T_HeatInput(C2(h_vol(start=
                487995.7996310873))), Vv_HeatInput(C2(h_vol(start=
                487995.7996310873))))
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={1,-123})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={11,-147})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-9,-147})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro13
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=90,
          origin={4,-98})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro14(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=270,
          origin={-6,-98})));
  equation
    connect(load.referencePower,realExpression. y)
      annotation (Line(points={{113.3,0},{133,0}},   color={0,0,127}));
    connect(HeatNwk_IP_In.y,Heat_IP_Network_Out. m_flow_in)
      annotation (Line(points={{-192,-20.4},{-189.9,-20.4},{-189.9,-24}},
          color={0,0,127}));
    connect(Heat_IP_Network_Out.ports[1],fluid2TSPro1. port_a)
      annotation (Line(
        points={{-193.5,-33},{-195,-33},{-195,-36.14}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1],fluid2TSPro2. port_b) annotation (
        Line(points={{-175,-30},{-174,-30},{-174,-36},{-179,-36}},
                                                       color={0,127,
            255}));
    connect(Set_Flow_TapSteam_IP.y,adaptorRealModelicaTSP1. u)
      annotation (Line(points={{-148,-20.4},{-148,-31.2}},
                                                      color={0,0,127}));
    connect(fluid2TSPro1.steam_outlet,HX_cog_IP. Water_Cooling_In)
      annotation (Line(points={{-194.998,-49.965},{-194.998,-60.2},{-183.2,
            -60.2}},                                                                 color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet,HX_cog_IP. Water_Cooling_Out) annotation (Line(points={{-179,
            -50},{-178,-50},{-178,-54},{-180.2,-54},{-180.2,-60.2}},                                                                color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow,adaptorRealModelicaTSP1. outputReal)
      annotation (Line(points={{-176.2,-60.4},{-148,-60.4},{-148,-40.4}},
                                                                 color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, fluid2TSPro3.steam_outlet) annotation (
        Line(points={{-183.2,-73.8},{-183.2,-91.998},{-103.96,-91.998}}, color=
            {0,0,255}));
    connect(fluid2TSPro4.steam_inlet, HX_cog_IP.Condensate_Tapping_Out)
      annotation (Line(points={{-102,-66},{-116,-66},{-116,-82},{-180,-82},{
            -180,-73.8}}, color={0,0,255}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{71.6867,11},{94,11},{94,16},{110,16},{110,8.6}},
        color={0,0,255},
        thickness=0.5));
    connect(bop.flangeB1, fluid2TSPro3.port_a) annotation (Line(points={{
            -4.76667,-12},{-4.76667,-60},{-80,-60},{-80,-92},{-88.16,-92}},
                                                                  color={0,0,255}));
    connect(bop.flangeA1, fluid2TSPro4.port_b) annotation (Line(points={{54.7667,
            -12},{52,-12},{52,-66},{-86,-66}},                     color={0,0,255}));
    connect(BOP_ctrl.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{20.6667,48},{20.6667,42},{6.2,42},{6.2,34}},
        color={80,200,120},
        thickness=0.5));
    connect(BOP_ctrl.sensorBus, bop.sensorBus) annotation (Line(
        points={{31.3333,48},{31.3333,42},{43.8,42},{43.8,34}},
        color={255,219,88},
        thickness=0.5));
    connect(const.y,heatSource1DFV. power)
      annotation (Line(points={{-61,10},{-52,10}}, color={0,0,127}));
    connect(heatSource1DFV.wall, bop.dHTVolumes) annotation (Line(points={{-45,10},
            {-34,10},{-34,11},{-22,11}}, color={255,127,0}));
    connect(HeatNwk_HP_In.y,Heat_HP_Network_Out. m_flow_in)
      annotation (Line(points={{-102,99.6},{-101.9,100},{-101.9,96}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1],fluid2TSPro5. port_a)
      annotation (Line(
        points={{-105.5,87},{-105.5,85.86},{-99,85.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1],fluid2TSPro6. port_b) annotation (
        Line(points={{-81,90},{-80,90},{-80,86},{-85,86},{-85,84}},
          color={0,127,255}));
    connect(Set_Flow_TapSteam_HP.y,adaptorRealModelicaTSP. u)
      annotation (Line(points={{-56,99.6},{-56,88.8}},color={0,0,127}));
    connect(fluid2TSPro5.steam_outlet,HX_cog_HP. Water_Cooling_In)
      annotation (Line(points={{-99.0018,72.035},{-99.0018,59.8},{-81.2,59.8}},color={0,0,255}));
    connect(HX_cog_HP.Water_Cooling_Out,fluid2TSPro6. steam_inlet)
      annotation (Line(points={{-78.2,59.8},{-78.2,64},{-85,64},{-85,70}},          color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow,adaptorRealModelicaTSP. outputReal)
      annotation (Line(points={{-74.2,59.6},{-56,59.6},{-56,79.6}},        color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, fluid2TSPro8.steam_outlet) annotation (
        Line(points={{-81.2,46.2},{-81.2,38},{-112,38},{-112,-35.998},{-95.96,
            -35.998}}, color={0,0,255}));
    connect(fluid2TSPro7.steam_inlet, HX_cog_HP.Condensate_Tapping_Out)
      annotation (Line(points={{-96,-24},{-108,-24},{-108,32},{-78,32},{-78,
            46.2}}, color={0,0,255}));
    connect(bop.flangeB2, fluid2TSPro8.port_a) annotation (Line(points={{
            -15.7333,-12},{-15.7333,-36},{-80.16,-36}}, color={0,0,255}));
    connect(bop.flangeA4, fluid2TSPro7.port_b) annotation (Line(points={{32.8333,
            -12},{34,-12},{34,-24},{-80,-24}},         color={0,0,255}));
    connect(rampLP.y,HeatNwk_LP_In. m_flow_in) annotation (Line(points={{161.6,
            -34},{162,-35.2},{152,-35.2}},     color={0,0,127}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro10.port_a) annotation (Line(
          points={{143,-32},{137.86,-32},{137.86,-35}}, color={0,127,255}));
    connect(HeatNwk_LP_Out.ports[1],fluid2TSPro9. port_b) annotation (
        Line(points={{146,-51},{140,-51}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u,Set_Flow_TapSteam_LP. y)
      annotation (Line(points={{152.8,-66},{161.6,-66}}, color={0,0,
            127}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro10.steam_outlet) annotation (
        Line(points={{117.8,-48.8},{120,-48.8},{120,-35.0018},{124.035,-35.0018}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI,fluid2TSPro9. steam_inlet) annotation (Line(points={{117.8,
            -51.8},{118,-51},{126,-51}},                                                                                   color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP,adaptorRealModelicaTSP2. outputReal)
      annotation (Line(points={{117.6,-55.8},{122,-55.8},{122,-66},{143.6,-66}}, color={0,0,255}));
    connect(fluid2TSPro12.steam_outlet, HX_cog_LP.fluidInletI) annotation (Line(
          points={{91.96,-50.002},{98,-50.002},{98,-48.8},{104.2,-48.8}}, color
          ={0,0,255}));
    connect(fluid2TSPro11.steam_inlet, HX_cog_LP.fluidOutletI1) annotation (
        Line(points={{92,-56},{96,-56},{96,-58},{100,-58},{100,-52},{104.2,-52}},
          color={0,0,255}));
    connect(bop.flangeA3, fluid2TSPro11.port_b) annotation (Line(points={{65.7333,
            -12},{66,-12},{66,-56},{76,-56}},         color={0,0,255}));
    connect(bop.flangeB3, fluid2TSPro12.port_a) annotation (Line(points={{6.2,
            -12},{6.2,-50},{76.16,-50}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine,
      Set_Liquid_Tapping_line_Flowrate.                                                 y)
      annotation (Line(points={{3.8,-129.6},{3.8,-136},{11,-136},{11,-141.5}},
                                                                             color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine,ThermalPower_InputToRankine. y)
      annotation (Line(points={{-1.6,-129.6},{-1.6,-138},{-9,-138},{-9,-141.5}},
                                                                             color={0,0,255}));
    connect(fluid2TSPro14.steam_outlet, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{-6.002,-105.96},{-6.002,-112},{-3.2,-112},{-3.2,
            -116.2}}, color={0,0,255}));
    connect(fluid2TSPro13.steam_inlet, HX_HeatInput.Turb_IP_In) annotation (
        Line(points={{4,-106},{4,-112},{0,-112},{0,-116.2}}, color={0,0,255}));
    connect(bop.flangeA2, fluid2TSPro13.port_b) annotation (Line(points={{44.1133,
            -12},{44,-12},{44,-76},{4,-76},{4,-90}},         color={0,0,255}));
    connect(bop.flangeB4, fluid2TSPro14.port_a) annotation (Line(points={{17.1667,
            -12},{16,-12},{16,-68},{-6,-68},{-6,-90.16}},         color={0,0,
            255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})));
  end Test_BOPth_0Dsource;

  model Test_BOPth_NSSS

    //parameter Real HTC(start=15000,fixed=false);

    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{120,-10},{100,10}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={144,0})));
    Control.BOPcontroller_ex3 BOP_ctrl(
      PID(
        yMax=0.2,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.12705081662073317)),
        limiter(u(start=0.09602129350596252))),
      PID1(
        yMax=0.001,
        yMin=-0.5,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.0001867413642363174)),
        limiter(u(start=-0.001385750479891688))),
      PID2(initType=Modelica.Blocks.Types.Init.SteadyState, limiter(u(start=
                0.0004075396511989877))),
      PID3(
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.01934365384592018)),
        limiter(u(start=0.018804450891975088))),
      PID4(
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.0010869785070580086)),
        limiter(u(start=0.001119043476838879))),
      const4(k=7.547e5),
      PID5(
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.010270321206872924)),
        limiter(u(start=0.010420871961200631))),
      add1(u2(start=-0.001385750479891688)),
      add8(u1(start=0.001119043476838879)))
      annotation (Placement(transformation(extent={{10,48},{42,76}})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-193.5,-28.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-192,-16})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-175,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro1
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={-195,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro2
      annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-179,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP adaptorRealModelicaTSP1
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-36})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-16})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.1940829455563923E-07,displayUnit="bar"),
        DPf(start=0.05966617296012886, displayUnit="bar"),
        Ec(h(start=2639610.136857773)),
        Sc(h_vol(start=711082.626635898)),
        DPfc(start=1.1937287376309207E-07, displayUnit="bar"),
        DPff(start=0.05966602553248424, displayUnit="bar")),
      TCond_Tap(C2(h_vol(start=711082.626635898))),
      Vv_Tap(C2(h_vol(start=711082.626635898))),
      Condensate_Tapping_Out(h_vol(start=711082.626635898)),
      Steam_Tapping_In(h(start=2639610.136857381)),
      Vol_Tap(h(start=2639610.136857773)))
      annotation (Placement(transformation(extent={{-186,-74},{-172,-60}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro3(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-96,-92})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro4
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-94,-66})));

    BOPdyn_thermal bop(
      LP_TAV(allowFlowReversal=false, dp(start=49976.15459620149, displayUnit="bar")),
      CNDpump(dp(start=784312.2193723671, displayUnit="bar"), h(start=
              164353.2596440803)),
      FWpump(
        dp(start=4019700.053627746, displayUnit="bar"),
        h(start=470048.16904780775),
        inletFluidState(h(start=464762.74452770816),p(start=674700.0,
              displayUnit="bar")),
        q_single(start=0.25238031865554933)),
      FWtank(hout(start=464762.74452770816),inlet(m_flow(start={
                182.55809058420135,20.779072368963686,11.665027087211513,
                25.003329372318213,0.0010000000474974513}))),
      HP_TAV(
        dp(start=83468.38568951283,  displayUnit="bar"),
        w(start=216.87287270012862),
        fluidState(h(start=2944106.364303575)),
        outlet(p(start=4416531.614310487, displayUnit="bar"))),
      LPTurbine1(
        steamState_in(p(start=636783.2762532821, displayUnit="bar")),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.8913592492841899)),
      LPTurbine2(
        w(start=166.06766731246455),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        steamState_in(p(start=79250.60705941985, displayUnit="bar")),
        eta_iso(start=0.845811081811772)),
      hp_fw(shell_2ph(h(start={2639610.136857381,2574398.6911774203,
                2492056.5434580203,2388110.8926207623,2256942.7322803335,
                2091511.738165602,1883032.6478808254,1620613.2455945762,
                1290889.9721959964,877754.7983572524,553815.7396726587}), p(
              start=757148.917031315,  displayUnit="bar")), flangeB1(h_outflow(
              start=687342.7188409794))),
      lp_fw(shell_2ph(h(start={2621766.536753741,2556772.5434357887,
                2474014.9230734445,2368757.1377552887,2235082.33993792,
                2065660.872271856,1851523.66657229,1581898.6840277603,
                1244230.2827270308,824623.739009895,333062.74404833495}),  p(
              start=80244.03752206039, displayUnit="bar")), flangeB(m_flow(
              start=-16.50412201408733))),
      mixer(h(start=2763166.530261766)),
      moistureSeparator(steam(h_outflow(start=2763179.113822877),  p(start=
                707810.5498582933, displayUnit="bar")), inlet(p(start=
                707172.7624351135, displayUnit="bar"))),
      pressDropLin3(state(d(start=902.1555302385668, displayUnit="g/cm3"))),
      rh(shell_2ph(h(start={2944106.364303575,2890364.758696105,
                2854460.031102937,2829994.8508293987,2813089.0952963904,
                2769066.8963323813,2684241.1813017344,2534788.3606152576,
                2272728.532176967,1816479.0360469588,1049549.8820976075}))),
      valve_SGin(dp(start=100351.7920709017,  displayUnit="bar"), fluidState(p(
              start=4656083.644885505, displayUnit="bar"))),
      HPTurbine(
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.8722649294586934)),
      sensT1_2(T(start=300 + 273.15)),
      LP_TAV1(dp(start=993.4304626405356, displayUnit="bar")),
      flangeA1(h_outflow(start=710502.0457445189)),
      HTC(start=14283.823876548315, fixed=false),
      Secondary(
        heatTransfer(gamma(start=14283.823876548315)),
        h(start={687481.1065937818,1066212.0575940826,1343099.4073463783,
              1586653.8993621576,1770336.8416530385,1962437.6326528054,
              2196071.879168668,2420041.9002103126,2620821.9836065182,
              2819837.144412142,2943518.375623026}),
        p(start=4500000.0,        displayUnit="bar")),
      mixer2(h(start=687344.5310518404)),
      valveLiq(dp(start=42448.91703131504,  displayUnit="bar")),
      dHTVolumes(T(start={522.6866104008592,558.1487045578555,560.0963175896175,
              552.9479061135486,553.9567077835491,558.9245065435548,
              557.7770569892594,555.0014084885878,557.4089976603292,
              569.6461434542309},displayUnit="degC")),
      flowSplit2(out2(m_flow(start=-16.503145774935035))),
      mixer1(h(start=2147066.3953712382)),
      valveLiq1(dp(start=73244.03752206039, displayUnit="bar")))
      annotation (Placement(transformation(extent={{-22,-12},{72,34}})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-186,116})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-189.5,103.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-165,105})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro5 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-183,91})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro6
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-169,89})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-140,96})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-140,116})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog
                     HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.4529809265499967E-08, displayUnit="bar"),
        DPf(start=0.05962919799262121, displayUnit="bar"),
        Sc(h_vol(start=1122142.992781656)),
        Ec(h(start=2944106.364303575)),
        DPfc(start=2.4435141865625154E-08, displayUnit="bar"),
        DPff(start=0.059628621498086114, displayUnit="bar")),
      Vv_Tap(C2(h_vol(start=1122142.992781656))),
      TCond_Tap(C2(h_vol(start=1122142.992781656))),
      Steam_Tapping_In(h(start=2944106.364303575)),
      Vol_Tap(h(start=2944106.364303575)))            annotation (Placement(transformation(extent={{-168,58},
              {-154,72}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro7
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-88,-24})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro8(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-88,-36})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={147.5,-32})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-34})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={149,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro9
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={133,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro10
                                                                                     annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={131,-35})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={148,-66})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-66})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog_LP
                        HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.0212227151827778E-06,displayUnit="bar"),
        DPf(start=0.05977420607015029, displayUnit="bar"),
        Ec(h(start=2621766.536753742)),
        Sc(h_vol(start=391984.05249364924)),
        DPfc(start=1.0213713375547676E-06, displayUnit="bar")),
      TCond_Tap_LP(C2(h_vol(start=391984.05249364924))),
      Vv_Tap_LP(C2(h_vol(start=391984.05249364924))),
      Vol7(h(start=2621766.536753742)),
      fluidInletI(h(start=2621766.536753741)),
      fluidOutletI1(h_vol(start=391984.05249364924)))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={111,-53})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro11
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={84,-56})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro12(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={84,-50})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_HeatInput
                           HX_HeatInput(
      T_HeatInput(C2(h_vol(start=465762.74452770816))),
      Vv_HeatInput(C2(h_vol(start=465762.74452770816))),
      Liquid_Tapping_line(h(start=464762.74452770816)),
      Vol_Tap(h(start=465762.74452770816)))
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={1,-123})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={11,-147})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={-9,-147})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro13
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=90,
          origin={4,-98})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro14(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=270,
          origin={-6,-98})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2 NSSSctrl
      annotation (Placement(transformation(extent={{-94,46},{-56,78}})));
    TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSsimplified_thermal nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      LowerPlenum(p(start=15058232.906347673, displayUnit="bar")),
      core(fuel(
          Tc(start={599.3813281269048,601.932965800155,604.4466426534807,
                606.9203717863056,609.3520778776347,611.7395941968686,
                614.0806542784615,616.3728743646318,618.6137217974884,
                620.8004642882819}, displayUnit="degC"),
          Tci(start={607.0776057571413,609.619267432264,612.123137024561,
                614.5872343909491,617.009491269015,619.3877482988424,
                621.7197467294894,624.0031109291223,626.2353168983278,
                628.4136417332387}, displayUnit="degC"),
          Tco(start={592.1956884024014,594.7566401794148,597.2794735948896,
                599.7621954391074,602.2027238008185,604.5988850677762,
                606.9484055716846,609.2488939679598,611.497809514712,
                613.6924111609837}, displayUnit="degC"),
          Tvol(start=[1106.5864686501259,960.0084210162546,901.3974954479078,
                846.8137303860727,795.2513531463707; 1109.5162105248237,
                962.600894740012,903.8578784616511,849.153014764787,
                797.4780201637886; 1112.405450334129,965.1573271121982,
                906.2839697190495,851.4596164537854,799.6735064641173;
                1115.2517464049433,967.6755672312178,908.6737318641102,
                853.7316019123904,801.8359747806775; 1118.05254951246,
                970.1533695299509,911.0250379309769,855.966952614744,
                803.9635071482268; 1120.8051993657966,972.5883906696747,
                913.335668399501,858.1635622538527,806.054102245029;
                1123.5069149569383,974.9781810040117,915.6033030976141,
                860.3192290461735,808.1056680714306; 1126.1547742340542,
                977.3201666008584,917.8255041447559,862.4316395200153,
                810.1160065238794; 1128.7456774646287,979.6116168431955,
                919.9996852139047,864.4983393004279,812.0827855955226;
                1131.276288329102,981.8495923446136,922.1230621202409,
                866.5166861479338,814.0034946923544], displayUnit="degC")),
          neutronicKinetics(D(start={2.6205726944706154E+17,
                6.403483943639837E+17,1.7197373192966294E+17,
                1.6670432548893075E+17,13257141437662816.0,1887667132682699.2}),
            P(start=540e6, fixed=true))),
      pressurizer(pressurizer(h(start=1862126.0574145121))),
      pump(h(start=1339588.5364921875), q_single(start=0.8497526499100627)),
      Primary(
        p(start=14767028.829727197,displayUnit="bar"),
        wall(T(start={589.9028599909016,588.0261750001409,585.7387666409575,
                583.5032399000027,584.0615128286494,580.3816395407692,
                578.0995090551672,576.0741525538564,574.1132585108202,
                565.9229630859465}, displayUnit="degC")),
        win(start=0.042740228275146425)))
      annotation (Placement(transformation(extent={{-102,-8},{-44,28}})));
  equation
    connect(load.referencePower,realExpression. y)
      annotation (Line(points={{113.3,0},{133,0}},   color={0,0,127}));
    connect(HeatNwk_IP_In.y,Heat_IP_Network_Out. m_flow_in)
      annotation (Line(points={{-192,-20.4},{-189.9,-20.4},{-189.9,-24}},
          color={0,0,127}));
    connect(Heat_IP_Network_Out.ports[1],fluid2TSPro1. port_a)
      annotation (Line(
        points={{-193.5,-33},{-195,-33},{-195,-36.14}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1],fluid2TSPro2. port_b) annotation (
        Line(points={{-175,-30},{-174,-30},{-174,-36},{-179,-36}},
                                                       color={0,127,
            255}));
    connect(Set_Flow_TapSteam_IP.y,adaptorRealModelicaTSP1. u)
      annotation (Line(points={{-148,-20.4},{-148,-31.2}},
                                                      color={0,0,127}));
    connect(fluid2TSPro1.steam_outlet,HX_cog_IP. Water_Cooling_In)
      annotation (Line(points={{-194.998,-49.965},{-194.998,-60.2},{-183.2,
            -60.2}},                                                                 color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet,HX_cog_IP. Water_Cooling_Out) annotation (Line(points={{-179,
            -50},{-178,-50},{-178,-54},{-180.2,-54},{-180.2,-60.2}},                                                                color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow,adaptorRealModelicaTSP1. outputReal)
      annotation (Line(points={{-176.2,-60.4},{-148,-60.4},{-148,-40.4}},
                                                                 color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, fluid2TSPro3.steam_outlet) annotation (
        Line(points={{-183.2,-73.8},{-183.2,-91.998},{-103.96,-91.998}}, color=
            {0,0,255}));
    connect(fluid2TSPro4.steam_inlet, HX_cog_IP.Condensate_Tapping_Out)
      annotation (Line(points={{-102,-66},{-116,-66},{-116,-82},{-180,-82},{
            -180,-73.8}}, color={0,0,255}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{71.6867,11},{94,11},{94,16},{110,16},{110,8.6}},
        color={0,0,255},
        thickness=0.5));
    connect(bop.flangeB1, fluid2TSPro3.port_a) annotation (Line(points={{
            -4.76667,-12},{-4.76667,-60},{-80,-60},{-80,-92},{-88.16,-92}},
                                                                  color={0,0,255}));
    connect(bop.flangeA1, fluid2TSPro4.port_b) annotation (Line(points={{54.7667,
            -12},{52,-12},{52,-66},{-86,-66}},                     color={0,0,255}));
    connect(BOP_ctrl.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{20.6667,48},{20.6667,42},{6.2,42},{6.2,34}},
        color={80,200,120},
        thickness=0.5));
    connect(BOP_ctrl.sensorBus, bop.sensorBus) annotation (Line(
        points={{31.3333,48},{31.3333,42},{43.8,42},{43.8,34}},
        color={255,219,88},
        thickness=0.5));
    connect(HeatNwk_HP_In.y,Heat_HP_Network_Out. m_flow_in)
      annotation (Line(points={{-186,111.6},{-185.9,112},{-185.9,108}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1],fluid2TSPro5. port_a)
      annotation (Line(
        points={{-189.5,99},{-189.5,97.86},{-183,97.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1],fluid2TSPro6. port_b) annotation (
        Line(points={{-165,102},{-164,102},{-164,98},{-169,98},{-169,96}},
          color={0,127,255}));
    connect(Set_Flow_TapSteam_HP.y,adaptorRealModelicaTSP. u)
      annotation (Line(points={{-140,111.6},{-140,100.8}},
                                                      color={0,0,127}));
    connect(fluid2TSPro5.steam_outlet,HX_cog_HP. Water_Cooling_In)
      annotation (Line(points={{-183.002,84.035},{-183.002,71.8},{-165.2,71.8}},
                                                                               color={0,0,255}));
    connect(HX_cog_HP.Water_Cooling_Out,fluid2TSPro6. steam_inlet)
      annotation (Line(points={{-162.2,71.8},{-162.2,76},{-169,76},{-169,82}},      color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow,adaptorRealModelicaTSP. outputReal)
      annotation (Line(points={{-158.2,71.6},{-140,71.6},{-140,91.6}},     color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, fluid2TSPro8.steam_outlet) annotation (
        Line(points={{-165.2,58.2},{-165.2,8},{-138,8},{-138,-35.998},{-95.96,
            -35.998}}, color={0,0,255}));
    connect(fluid2TSPro7.steam_inlet, HX_cog_HP.Condensate_Tapping_Out)
      annotation (Line(points={{-96,-24},{-126,-24},{-126,54},{-162,54},{-162,
            58.2}}, color={0,0,255}));
    connect(bop.flangeB2, fluid2TSPro8.port_a) annotation (Line(points={{
            -15.7333,-12},{-15.7333,-36},{-80.16,-36}}, color={0,0,255}));
    connect(bop.flangeA4, fluid2TSPro7.port_b) annotation (Line(points={{32.8333,
            -12},{34,-12},{34,-24},{-80,-24}},         color={0,0,255}));
    connect(rampLP.y,HeatNwk_LP_In. m_flow_in) annotation (Line(points={{161.6,
            -34},{162,-35.2},{152,-35.2}},     color={0,0,127}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro10.port_a) annotation (Line(
          points={{143,-32},{137.86,-32},{137.86,-35}}, color={0,127,255}));
    connect(HeatNwk_LP_Out.ports[1],fluid2TSPro9. port_b) annotation (
        Line(points={{146,-51},{140,-51}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u,Set_Flow_TapSteam_LP. y)
      annotation (Line(points={{152.8,-66},{161.6,-66}}, color={0,0,
            127}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro10.steam_outlet) annotation (
        Line(points={{117.8,-48.8},{120,-48.8},{120,-35.0018},{124.035,-35.0018}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI,fluid2TSPro9. steam_inlet) annotation (Line(points={{117.8,
            -51.8},{118,-51},{126,-51}},                                                                                   color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP,adaptorRealModelicaTSP2. outputReal)
      annotation (Line(points={{117.6,-55.8},{122,-55.8},{122,-66},{143.6,-66}}, color={0,0,255}));
    connect(fluid2TSPro12.steam_outlet, HX_cog_LP.fluidInletI) annotation (Line(
          points={{91.96,-50.002},{98,-50.002},{98,-48.8},{104.2,-48.8}}, color
          ={0,0,255}));
    connect(fluid2TSPro11.steam_inlet, HX_cog_LP.fluidOutletI1) annotation (
        Line(points={{92,-56},{96,-56},{96,-58},{100,-58},{100,-52},{104.2,-52}},
          color={0,0,255}));
    connect(bop.flangeA3, fluid2TSPro11.port_b) annotation (Line(points={{65.7333,
            -12},{66,-12},{66,-56},{76,-56}},         color={0,0,255}));
    connect(bop.flangeB3, fluid2TSPro12.port_a) annotation (Line(points={{6.2,
            -12},{6.2,-50},{76.16,-50}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine,
      Set_Liquid_Tapping_line_Flowrate.                                                 y)
      annotation (Line(points={{3.8,-129.6},{3.8,-136},{11,-136},{11,-141.5}},
                                                                             color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine,ThermalPower_InputToRankine. y)
      annotation (Line(points={{-1.6,-129.6},{-1.6,-138},{-9,-138},{-9,-141.5}},
                                                                             color={0,0,255}));
    connect(fluid2TSPro14.steam_outlet, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{-6.002,-105.96},{-6.002,-112},{-3.2,-112},{-3.2,
            -116.2}}, color={0,0,255}));
    connect(fluid2TSPro13.steam_inlet, HX_HeatInput.Turb_IP_In) annotation (
        Line(points={{4,-106},{4,-112},{0,-112},{0,-116.2}}, color={0,0,255}));
    connect(bop.flangeA2, fluid2TSPro13.port_b) annotation (Line(points={{44.1133,
            -12},{44,-12},{44,-76},{4,-76},{4,-90}},         color={0,0,255}));
    connect(bop.flangeB4, fluid2TSPro14.port_a) annotation (Line(points={{17.1667,
            -12},{16,-12},{16,-68},{-6,-68},{-6,-90.16}},         color={0,0,
            255}));
    connect(NSSSctrl.actuatorBus,nsss. actuatorBus) annotation (Line(
        points={{-86.4,46},{-86,46},{-86,38},{-87.5,38},{-87.5,27.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus,nsss. sensorBus) annotation (Line(
        points={{-63.6,46},{-62,46},{-62,38},{-58,38},{-58,27.64},{-58.5,27.64}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.dHTVolumes, bop.dHTVolumes) annotation (Line(points={{-44,
            11.62},{-36,11.62},{-36,11},{-22,11}}, color={255,127,0}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,
              -100},{180,100}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{180,
              100}})),
      experiment(StopTime=2000, __Dymola_Algorithm="Dassl"),
      __Dymola_experimentFlags(
        Advanced(
          GenerateAnalyticJacobian=false,
          GenerateVariableDependencies=false,
          OutputModelicaCode=false),
        Evaluate=false,
        OutputCPUtime=false,
        OutputFlatModelica=false));
  end Test_BOPth_NSSS;

  model Test_BOPfluid_NSSS

    //parameter Real HTC(start=15000,fixed=false);

    inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
      annotation (Placement(transformation(extent={{120,40},{140,60}})));
    ThermoPower.Electrical.Load load(Pnom=170e6, usePowerInput=true)
      annotation (Placement(transformation(extent={{120,-10},{100,10}})));
    Modelica.Blocks.Sources.RealExpression realExpression(y=bop.powerSensor.power)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={144,0})));
    Control.BOPcontroller_ex3 BOP_ctrl(
      PID(
        k=38.9612,
        yMax=0.2,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.09619690276158804)),
        limiter(u(start=0.12674306364303645))),
      PID1(
        k=0.7404,
        yMax=0.001,
        yMin=-0.5,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=-0.0013773747690960822)),
        limiter(u(start=0.00017034482301218518))),
      PID2(initType=Modelica.Blocks.Types.Init.SteadyState, gainPID(y(start=
                0.00040838211369530935))),
      PID3(
        k=6.439,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.018827072686346487)),
        limiter(u(start=0.01930797043684752))),
      PID4(
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.001120544468859484)),
        limiter(u(start=0.0010847754206750714))),
      const4(k=7.547e5),
      PID5(
        k=6.439,
        initType=Modelica.Blocks.Types.Init.SteadyState,
        gainPID(y(start=0.010444592132479027)),
        limiter(u(start=0.010234670709548541))),
      add1(u2(start=-0.0013773747690960822)),
      add8(u1(start=0.001120544468859484)),
      add4(k1=1, k2=-1),
      add5(k1=1, k2=-1),
      add7(k1=1, k2=-1),
      add11(k1=1, k2=-1))
      annotation (Placement(transformation(extent={{10,48},{42,76}})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-193.5,-28.5})));
    Modelica.Blocks.Sources.Ramp HeatNwk_IP_In(
      height=115,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-192,-16})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_IP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-175,-27})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro1
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=90,
          origin={-195,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro2
      annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-179,-43})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP adaptorRealModelicaTSP1
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-36})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_IP(
      height=28,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-148,-16})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog HX_cog_IP(
      Hx_Hybrid(
        DPc(start=1.1937366759301423E-07,displayUnit="bar"),
        DPf(start=0.059666027103045215,displayUnit="bar"),
        Ec(h(start=2639609.877779146)),
        Sc(h_vol(start=711083.3242149337)),
        DPfc(start=1.1940782842197216E-07, displayUnit="bar"),
        DPff(start=0.0596661726252597,  displayUnit="bar")),
      TCond_Tap(C2(h_vol(start=711083.3242149337))),
      Vv_Tap(C2(h_vol(start=711083.3242149337))),
      Condensate_Tapping_Out(h_vol(start=711083.3242149337)),
      Steam_Tapping_In(h(start=2639609.877779149)),
      Vol_Tap(h(start=2639609.877779146)))
      annotation (Placement(transformation(extent={{-186,-74},{-172,-60}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro3(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-96,-92})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro4
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-94,-66})));

    BOPdyn_fluid bop(
      LP_TAV(allowFlowReversal=false, dp(start=49976.37553876883,displayUnit="bar")),
      CNDpump(dp(start=784312.822079376,  displayUnit="bar"), h(start=
              164353.2604031154)),
      FWpump(
        dp(start=4019758.305767026,  displayUnit="bar"),
        h(start=470061.49518058944),
        inletFluidState(h(start=464775.9807109833), p(start=674700.0,
              displayUnit="bar")),
        q_single(start=0.25238236854303514)),
      FWtank(hout(start=465374.3984095717), inlet(m_flow(start={
                182.55809058420135,20.779072368963686,11.665027087211513,
                25.003329372318213,0.0010000000474974513}))),
      HP_TAV(
        dp(start=83442.00008360762,  displayUnit="bar"),
        w(start=216.87287270012862),
        fluidState(h(start=2944106.3643035744)),
        outlet(p(start=4416557.999916392, displayUnit="bar"))),
      LPTurbine1(
        steamState_in(p(start=636785.7186114555, displayUnit="bar")),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.8913591439340155)),
      LPTurbine2(
        w(start=166.06833809007537),
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        steamState_in(p(start=79250.93079842262,displayUnit="bar")),
        eta_iso(start=0.8458108953885707)),
      hp_fw(shell_2ph(h(start={2639609.877779149,2574406.4202620075,
                2492074.4400485354,2388141.7216321602,2256989.99451295,
                2091579.8528769857,1883127.1680789324,1620741.0982421204,
                1291059.696048189,877976.7084859293,553943.1633149498}), p(
              start=757151.8630837062, displayUnit="bar")), flangeB1(h_outflow(
              start=687346.0813621872))),
      lp_fw(shell_2ph(h(start={2621766.4783409717,2556771.9408406545,
                2474013.6913613672,2368755.1870585782,2235079.5792068997,
                2065657.2153023889,1851519.041052761,1581893.0443364035,
                1244223.6268732636,824616.1290039545,333057.2072612668})),
          flangeB(m_flow(start=-16.503080713268794))),
      mixer(h(start=2763166.692785703)),
      moistureSeparator(steam(h_outflow(start=2763179.276225944),  p(start=
                707810.5498582933, displayUnit="bar")), inlet(p(start=
                707175.4875449374, displayUnit="bar"))),
      pressDropLin3(state(d(start=902.1553727589877, displayUnit="g/cm3"))),
      rh(shell_2ph(h(start={2944106.3643035744,2890364.7032225598,
                2854459.9508629376,2829994.7620630753,2813089.006527776,
                2769066.4358113166,2684240.222313283,2534786.697338048,
                2272725.936148276,1816475.3270557425,1049546.5552879146}),p(
              start=4500000.0,         displayUnit="bar"))),
      valve_SGin(dp(start=100352.99480357696,displayUnit="bar"), fluidState(p(
              start=4656083.644885505, displayUnit="bar"))),
      HPTurbine(
        eta_iso_nom=0.9,
        corrWet=true,
        corrFlow=true,
        eta_iso(start=0.8722647179390841)),
      sensT1_2(T(start=300 + 273.15)),
      LP_TAV1(dp(start=993.4345843682822, displayUnit="bar")),
      flangeA1(h_outflow(start=710502.0457445189)),
      mixer2(h(start=687347.8935488948)),
      valveLiq(dp(start=42451.86308370624, displayUnit="bar")),
      flangeA(p(start=4500000), m_flow(start=240)),
      flangeB(m_flow(start=-240)),
      flowSplit2(out2(m_flow(start=-16.50420704143482))),
      mixer1(h(start=2147065.6951920846)))
      annotation (Placement(transformation(extent={{-22,-12},{72,34}})));
    Modelica.Blocks.Sources.Ramp HeatNwk_HP_In(
      height=15,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-186,116})));
    Modelica.Fluid.Sources.MassFlowSource_h Heat_HP_Network_Out(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4.5},{4.5,4.5}},
          rotation=270,
          origin={-189.5,103.5})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_HP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=270,
          origin={-165,105})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro5 annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={-183,91})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro6
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=90,
          origin={-169,89})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-140,96})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_HP(
      height=20,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=270,
          origin={-140,116})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog
                     HX_cog_HP(
      Hx_Hybrid(
        DPc(start=2.4435370387197308E-08, displayUnit="bar"),
        DPf(start=0.05962862289370117, displayUnit="bar"),
        Sc(h_vol(start=1122142.992781656)),
        Ec(h(start=2944106.3643035744)),
        DPfc(start=2.452980926549997E-08,  displayUnit="bar"),
        DPff(start=0.05962919799262121, displayUnit="bar")),
      Vv_Tap(C2(h_vol(start=1122142.992781656))),
      TCond_Tap(C2(h_vol(start=1122142.992781656))),
      Steam_Tapping_In(h(start=2944106.3643035744)),
      Vol_Tap(h(start=2944106.3643035744)))           annotation (Placement(transformation(extent={{-168,58},
              {-154,72}})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro7
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={-88,-24})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro8(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={-88,-36})));
    Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
      redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
      use_m_flow_in=true,
      use_h_in=false,
      m_flow=100,
      h=135000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-4.5,-4},{4.5,4}},
          rotation=180,
          origin={147.5,-32})));
    Modelica.Blocks.Sources.Ramp rampLP(
      height=250,
      duration=900,
      offset=0.01,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-34})));
    Modelica.Fluid.Sources.Boundary_ph HeatNwk_LP_Out(
      redeclare package Medium = Modelica.Media.Water.StandardWater,
      use_p_in=false,
      p=1500000,
      nPorts=1) annotation (Placement(transformation(
          extent={{-3,-3},{3,3}},
          rotation=180,
          origin={149,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro9
                                                           annotation (
        Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=0,
          origin={133,-51})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro10
                                                                                     annotation (
        Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=0,
          origin={131,-35})));
    TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
      adaptorRealModelicaTSP2
                             annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={148,-66})));
    Modelica.Blocks.Sources.Ramp Set_Flow_TapSteam_LP(
      height=25,
      duration=900,
      offset=0.001,
      startTime=100) annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={166,-66})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_cog_LP
                        HX_cog_LP(
      Hx_Hybrid_LP(
        DPc(start=1.0213770701459113E-06,displayUnit="bar"),
        DPf(start=0.05977430533560828, displayUnit="bar"),
        Ec(h(start=2621766.47834097)),
        Sc(h_vol(start=391984.5158094587)),
        DPfc(start=1.021218703891956E-06,  displayUnit="bar"),
        DPff(start=0.05977420587861938, displayUnit="bar")),
      TCond_Tap_LP(C2(h_vol(start=391984.5158094587))),
      Vv_Tap_LP(C2(h_vol(start=391984.5158094587))),
      Vol7(h(start=2621766.47834097)),
      fluidInletI(h(start=2621766.4783409717)),
      fluidOutletI1(h_vol(start=391984.5158094587)))
                                          annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={111,-53})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro11
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={84,-56})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro12(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=0,
          origin={84,-50})));
    TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.HX_HeatInput
                           HX_HeatInput(
      T_HeatInput(C2(h_vol(start=466374.3984095717))),
      Vv_HeatInput(C2(h_vol(start=466374.3984095717))),
      Liquid_Tapping_line(h(start=465374.3984095717)),
      Vol_Tap(h(start=466374.3984095717)))
      annotation (Placement(transformation(
          extent={{7,-7},{-7,7}},
          rotation=180,
          origin={31,-105})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Liquid_Tapping_line_Flowrate(k=1e-3)
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={41,-129})));
    ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante ThermalPower_InputToRankine
      annotation (Placement(transformation(
          extent={{-5,-5},{5,5}},
          rotation=90,
          origin={21,-129})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid fluid2TSPro13
      annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=90,
          origin={34,-80})));
    TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro fluid2TSPro14(
        steam_outlet(h(start=2944000.0)), port_a(h_outflow(start=
              2962802.891927479))) annotation (Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=270,
          origin={24,-80})));
    NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                         NSSSctrl
      annotation (Placement(transformation(extent={{-82,50},{-44,82}})));
    NSSS.NSSS_ThermoPower.NSSSsimplified_fluid            nsss(
      core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dpnom(displayUnit="Pa") = 22280,
      Cfnom=0.0037,
      Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp1=200000,
      Cfnom1=0.004,
      Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
      dp2=40000,
      Cfnom2=0.0086448975,
      rho_sg(displayUnit="kg/m3"),
      eta=0.9,
      q_nom={0,0.85035195,1.51494276},
      head_nom={52.9277496,36.251883,0},
      hstart_pump=1.33804e6,
      dp0=257900,
      SG(Secondary(noInitialPressure=false, heatTransfer(gamma(start=
                  14586.649512440295)),
          h(start={687347.8935488948,1064946.1647611712,1341716.155222091,
                1585307.4080712595,1769403.3275958602,1961873.102383314,
                2195618.6257879734,2419768.3172225566,2620868.6353172557,
                2820118.24037197,2944106.3643035744})),   Primary(p(start=
                14766715.218693856, displayUnit="bar"), wall(T(start={
                  589.4082287819939,587.5173057493705,585.2231053970795,
                  582.9825962654701,583.5859133445866,579.8971845995204,
                  577.5967200282101,575.5710566193154,573.6111123867778,
                  565.3798590723168}, displayUnit="degC")))),
      htc2(fixed=false, start=14586.649512440295),
      core(neutronicKinetics(P(start=540e6, fixed=true)), fuel(
          Tc(start={598.8485267209098,601.4035526585864,603.9211273787487,
                606.3992916001216,608.8359984035769,611.2291109638,
                613.576395652504,615.8755068844395,618.1239590989708,
                620.3190808301662},displayUnit="degC"),
          Tci(start={606.5468900444794,609.0919223973908,611.599670414378,
                614.0681814774575,616.495415629274,618.879243312943,
                621.2174385015434,623.5076636023115,625.7474415456983,
                627.934110031119},  displayUnit="degC"),
          Tco(start={591.6609396860848,594.2252961473115,596.7520455818084,
                599.2392224871089,601.6847734442164,604.0865548416372,
                606.4423259540589,608.7497337340378,611.0062846932235,
                613.2092988084463},displayUnit="degC"),
          Tvol(start=[1105.9751157884032,959.4674202468113,900.8840483513684,
                846.325544710997,794.7866604051272; 1108.908088571398,
                962.0627959886949,903.3472039996595,848.6674820220086,
                797.0158678417474; 1111.8011667195126,964.622666603949,
                905.7765762909835,850.9772195729826,799.2143538408656;
                1114.6519422463166,967.1449108512938,908.1701559527464,
                853.2528504644085,801.3803064399116; 1117.457900571859,
                969.6273136675054,910.5258449007742,855.4923835683683,
                803.5118336960317; 1120.2164178332905,972.0675637916177,
                912.8414539880546,857.6937413917203,805.6069616556396;
                1122.924752880846,974.4632466859515,915.1146962897845,
                859.854753695151,807.663628284036; 1125.5800297344547,
                976.8118290194394,917.3431723821283,861.9731435013649,
                809.6796701518058; 1128.1792051179127,979.1106299560945,
                919.5243431026096,864.0465012048671,811.6527977990784;
                1130.7190151479756,981.356774016376,921.6554848302269,
                866.0722420690554,813.58055529342],   displayUnit="degC"))),
      flangeB(h_outflow(start=2944106.3643035744)),
      pump(q_single(start=0.8498079349769984), h(start=1336699.0456428102)),
      flangeA(h_outflow(start=1064946.1647611712)),
      pressurizer(pressurizer(h(start=1862126.0574145121))))
      annotation (Placement(transformation(extent={{-92,-8},{-34,28}})));
  equation
    connect(load.referencePower,realExpression. y)
      annotation (Line(points={{113.3,0},{133,0}},   color={0,0,127}));
    connect(HeatNwk_IP_In.y,Heat_IP_Network_Out. m_flow_in)
      annotation (Line(points={{-192,-20.4},{-189.9,-20.4},{-189.9,-24}},
          color={0,0,127}));
    connect(Heat_IP_Network_Out.ports[1],fluid2TSPro1. port_a)
      annotation (Line(
        points={{-193.5,-33},{-195,-33},{-195,-36.14}},
        color={0,127,255}));
    connect(HeatNwk_IP_Out.ports[1],fluid2TSPro2. port_b) annotation (
        Line(points={{-175,-30},{-174,-30},{-174,-36},{-179,-36}},
                                                       color={0,127,
            255}));
    connect(Set_Flow_TapSteam_IP.y,adaptorRealModelicaTSP1. u)
      annotation (Line(points={{-148,-20.4},{-148,-31.2}},
                                                      color={0,0,127}));
    connect(fluid2TSPro1.steam_outlet,HX_cog_IP. Water_Cooling_In)
      annotation (Line(points={{-194.998,-49.965},{-194.998,-60.2},{-183.2,
            -60.2}},                                                                 color={0,0,255}));
    connect(fluid2TSPro2.steam_inlet,HX_cog_IP. Water_Cooling_Out) annotation (Line(points={{-179,
            -50},{-178,-50},{-178,-54},{-180.2,-54},{-180.2,-60.2}},                                                                color={0,0,255}));
    connect(HX_cog_IP.TapingSteamFlow,adaptorRealModelicaTSP1. outputReal)
      annotation (Line(points={{-176.2,-60.4},{-148,-60.4},{-148,-40.4}},
                                                                 color={0,0,255}));
    connect(HX_cog_IP.Steam_Tapping_In, fluid2TSPro3.steam_outlet) annotation (
        Line(points={{-183.2,-73.8},{-183.2,-91.998},{-103.96,-91.998}}, color=
            {0,0,255}));
    connect(fluid2TSPro4.steam_inlet, HX_cog_IP.Condensate_Tapping_Out)
      annotation (Line(points={{-102,-66},{-116,-66},{-116,-82},{-180,-82},{
            -180,-73.8}}, color={0,0,255}));
    connect(bop.powerConnection, load.port) annotation (Line(
        points={{71.6867,11},{94,11},{94,16},{110,16},{110,8.6}},
        color={0,0,255},
        thickness=0.5));
    connect(bop.flangeB1, fluid2TSPro3.port_a) annotation (Line(points={{
            -4.76667,-12},{-4.76667,-60},{-80,-60},{-80,-92},{-88.16,-92}},
                                                                  color={0,0,255}));
    connect(bop.flangeA1, fluid2TSPro4.port_b) annotation (Line(points={{54.7667,
            -12},{52,-12},{52,-66},{-86,-66}},                     color={0,0,255}));
    connect(BOP_ctrl.actuatorBus, bop.actuatorBus) annotation (Line(
        points={{20.6667,48},{20.6667,42},{6.2,42},{6.2,34}},
        color={80,200,120},
        thickness=0.5));
    connect(BOP_ctrl.sensorBus, bop.sensorBus) annotation (Line(
        points={{31.3333,48},{31.3333,42},{43.8,42},{43.8,34}},
        color={255,219,88},
        thickness=0.5));
    connect(HeatNwk_HP_In.y,Heat_HP_Network_Out. m_flow_in)
      annotation (Line(points={{-186,111.6},{-185.9,112},{-185.9,108}},
          color={0,0,127}));
    connect(Heat_HP_Network_Out.ports[1],fluid2TSPro5. port_a)
      annotation (Line(
        points={{-189.5,99},{-189.5,97.86},{-183,97.86}},
        color={0,127,255}));
    connect(HeatNwk_HP_Out.ports[1],fluid2TSPro6. port_b) annotation (
        Line(points={{-165,102},{-164,102},{-164,98},{-169,98},{-169,96}},
          color={0,127,255}));
    connect(Set_Flow_TapSteam_HP.y,adaptorRealModelicaTSP. u)
      annotation (Line(points={{-140,111.6},{-140,100.8}},
                                                      color={0,0,127}));
    connect(fluid2TSPro5.steam_outlet,HX_cog_HP. Water_Cooling_In)
      annotation (Line(points={{-183.002,84.035},{-183.002,71.8},{-165.2,71.8}},
                                                                               color={0,0,255}));
    connect(HX_cog_HP.Water_Cooling_Out,fluid2TSPro6. steam_inlet)
      annotation (Line(points={{-162.2,71.8},{-162.2,76},{-169,76},{-169,82}},      color={255,0,0}));
    connect(HX_cog_HP.TapingSteamFlow,adaptorRealModelicaTSP. outputReal)
      annotation (Line(points={{-158.2,71.6},{-140,71.6},{-140,91.6}},     color={0,0,255}));
    connect(HX_cog_HP.Steam_Tapping_In, fluid2TSPro8.steam_outlet) annotation (
        Line(points={{-165.2,58.2},{-165.2,8},{-138,8},{-138,-35.998},{-95.96,
            -35.998}}, color={0,0,255}));
    connect(fluid2TSPro7.steam_inlet, HX_cog_HP.Condensate_Tapping_Out)
      annotation (Line(points={{-96,-24},{-126,-24},{-126,54},{-162,54},{-162,
            58.2}}, color={0,0,255}));
    connect(bop.flangeB2, fluid2TSPro8.port_a) annotation (Line(points={{
            -15.7333,-12},{-15.7333,-36},{-80.16,-36}}, color={0,0,255}));
    connect(bop.flangeA4, fluid2TSPro7.port_b) annotation (Line(points={{32.8333,
            -12},{34,-12},{34,-24},{-80,-24}},         color={0,0,255}));
    connect(rampLP.y,HeatNwk_LP_In. m_flow_in) annotation (Line(points={{161.6,
            -34},{162,-35.2},{152,-35.2}},     color={0,0,127}));
    connect(HeatNwk_LP_In.ports[1], fluid2TSPro10.port_a) annotation (Line(
          points={{143,-32},{137.86,-32},{137.86,-35}}, color={0,127,255}));
    connect(HeatNwk_LP_Out.ports[1],fluid2TSPro9. port_b) annotation (
        Line(points={{146,-51},{140,-51}}, color={0,127,255}));
    connect(adaptorRealModelicaTSP2.u,Set_Flow_TapSteam_LP. y)
      annotation (Line(points={{152.8,-66},{161.6,-66}}, color={0,0,
            127}));
    connect(HX_cog_LP.fluidInletI1, fluid2TSPro10.steam_outlet) annotation (
        Line(points={{117.8,-48.8},{120,-48.8},{120,-35.0018},{124.035,-35.0018}},
          color={0,0,255}));
    connect(HX_cog_LP.fluidOutletI,fluid2TSPro9. steam_inlet) annotation (Line(points={{117.8,
            -51.8},{118,-51},{126,-51}},                                                                                   color={255,0,0}));
    connect(HX_cog_LP.TapingSteamFlow_CogHP,adaptorRealModelicaTSP2. outputReal)
      annotation (Line(points={{117.6,-55.8},{122,-55.8},{122,-66},{143.6,-66}}, color={0,0,255}));
    connect(fluid2TSPro12.steam_outlet, HX_cog_LP.fluidInletI) annotation (Line(
          points={{91.96,-50.002},{98,-50.002},{98,-48.8},{104.2,-48.8}}, color
          ={0,0,255}));
    connect(fluid2TSPro11.steam_inlet, HX_cog_LP.fluidOutletI1) annotation (
        Line(points={{92,-56},{96,-56},{96,-58},{100,-58},{100,-52},{104.2,-52}},
          color={0,0,255}));
    connect(bop.flangeA3, fluid2TSPro11.port_b) annotation (Line(points={{65.7333,
            -12},{66,-12},{66,-56},{76,-56}},         color={0,0,255}));
    connect(bop.flangeB3, fluid2TSPro12.port_a) annotation (Line(points={{6.2,
            -12},{6.2,-50},{76.16,-50}}, color={0,0,255}));
    connect(HX_HeatInput.FlowControl_LiquidTapingLine,
      Set_Liquid_Tapping_line_Flowrate.                                                 y)
      annotation (Line(points={{33.8,-111.6},{33.8,-118},{41,-118},{41,-123.5}},
                                                                             color={0,0,255}));
    connect(HX_HeatInput.HeatInput2Rankine,ThermalPower_InputToRankine. y)
      annotation (Line(points={{28.4,-111.6},{28.4,-120},{21,-120},{21,-123.5}},
                                                                             color={0,0,255}));
    connect(fluid2TSPro14.steam_outlet, HX_HeatInput.Liquid_Tapping_line)
      annotation (Line(points={{23.998,-87.96},{23.998,-94},{26.8,-94},{26.8,
            -98.2}},  color={0,0,255}));
    connect(fluid2TSPro13.steam_inlet, HX_HeatInput.Turb_IP_In) annotation (
        Line(points={{34,-88},{34,-94},{30,-94},{30,-98.2}}, color={0,0,255}));
    connect(bop.flangeA2, fluid2TSPro13.port_b) annotation (Line(points={{44.1133,
            -12},{44.1133,-64},{34,-64},{34,-72}},           color={0,0,255}));
    connect(bop.flangeB4, fluid2TSPro14.port_a) annotation (Line(points={{17.1667,
            -12},{17.1667,-64},{24,-64},{24,-72.16}},             color={0,0,
            255}));
    connect(NSSSctrl.actuatorBus,nsss. actuatorBus) annotation (Line(
        points={{-74.4,50},{-74,50},{-74,44},{-77.5,44},{-77.5,27.64}},
        color={80,200,120},
        thickness=0.5));
    connect(NSSSctrl.sensorBus,nsss. sensorBus) annotation (Line(
        points={{-51.6,50},{-52,50},{-52,46},{-48.5,46},{-48.5,27.64}},
        color={255,219,88},
        thickness=0.5));
    connect(nsss.flangeA, bop.flangeB) annotation (Line(points={{-34,-1.16},{
            -32,-1.16},{-32,-2},{-28,-2},{-28,2.78571},{-22,2.78571}}, color={0,
            0,255}));
    connect(nsss.flangeB, bop.flangeA) annotation (Line(points={{-34,20.8},{-32,
            20.8},{-32,22},{-26,22},{-26,19.2143},{-22,19.2143}}, color={0,0,
            255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-220,
              -140},{220,140}})),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-220,-140},{220,
              140}})),
      experiment(StopTime=2000, __Dymola_Algorithm="Dassl"),
      __Dymola_experimentFlags(
        Advanced(
          EvaluateAlsoTop=false,
          GenerateAnalyticJacobian=false,
          GenerateVariableDependencies=false,
          OutputModelicaCode=false),
        Evaluate=true,
        OutputCPUtime=false,
        OutputFlatModelica=false));
  end Test_BOPfluid_NSSS;
end Test;
