within TANDEM.SMR.BOP.BOP_POLIMI;
package Components
  model ShellAndTube_BOP
    replaceable package ShellMedium = ThermoPower.Water.StandardWater constrainedby
      Modelica.Media.Interfaces.PartialMedium "Liquid medium model"
      annotation(choicesAllMatching = true);
    replaceable package TubeMedium =
        ThermoPower.Water.StandardWater                                 constrainedby
      Modelica.Media.Interfaces.PartialMedium "Liquid medium model"
      annotation(choicesAllMatching = true);

    parameter Integer Nodes=11;
    parameter Integer Nt;
    parameter Integer Np;
    parameter Modelica.Units.SI.Length L;
    parameter Modelica.Units.SI.Length di;
    parameter Modelica.Units.SI.Length do;
    parameter Modelica.Units.SI.Length pitch;
    parameter Real Bfrac "Baffle fraction";

    parameter Real CL;
    parameter Real CTP;

    parameter ThermoPower.Units.SpecificThermalResistance Rfl=6.5e-5 "Fouling factor";

    parameter Modelica.Units.SI.Density rhom = 7763;
    parameter Modelica.Units.SI.SpecificHeatCapacity cpm = 510.4;
    parameter Modelica.Units.SI.ThermalConductivity km = 57.45;

    parameter Modelica.Units.SI.Power Qnom;
    parameter Modelica.Units.SI.MassFlowRate ms_nom;
    parameter Modelica.Units.SI.MassFlowRate mt_nom;
    parameter Modelica.Units.SI.Pressure ps_nom;
    parameter Modelica.Units.SI.Pressure pt_nom;
    parameter Modelica.Units.SI.SpecificEnthalpy hsi_nom;
    parameter Modelica.Units.SI.SpecificEnthalpy hso_nom;
    parameter Modelica.Units.SI.SpecificEnthalpy hti_nom;
    parameter Modelica.Units.SI.SpecificEnthalpy hto_nom;

    parameter Modelica.Units.SI.PressureDifference dpt_nom = 0;
    parameter Real Cf_t;
    parameter Modelica.Units.SI.CoefficientOfHeatTransfer hshell;

    replaceable model ShellHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer
      constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
      annotation (choicesAllMatching=true);

    replaceable model TubeHeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer
      constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV
      annotation (choicesAllMatching=true);

    final parameter Real PR = pitch/do;
    final parameter Modelica.Units.SI.Area Ao = Np*pi*do*Nt*L;
    final parameter Modelica.Units.SI.Length Ds = 0.637*sqrt(CL/CTP)*sqrt(Ao*PR^2*do/L);
    final parameter Modelica.Units.SI.Length C = pitch-do;
    final parameter Modelica.Units.SI.Length B = Bfrac*Ds;
    final parameter Modelica.Units.SI.Area As = Ds*C*B/pitch;
    final parameter Modelica.Units.SI.Area As_tot = pi*(Ds/2)^2-Nt*Np*pi*(do/2)^2;

    ThermoPower.Water.FlangeB flangeB(redeclare package Medium = ShellMedium,
      m_flow(start=-ms_nom),
      p(start=ps_nom),
      h_outflow(start=hso_nom))
      annotation (Placement(transformation(extent={{-10,-90},{10,-70}}),
          iconTransformation(extent={{-10,-90},{10,-70}})));
    ThermoPower.Water.FlangeA flangeA(redeclare package Medium = ShellMedium,
      p(start=ps_nom),
      m_flow(start=ms_nom),
      h_outflow(start=hsi_nom))
      annotation (Placement(transformation(extent={{-10,68},{10,88}}),
          iconTransformation(extent={{-10,68},{10,88}})));
    ThermoPower.Water.FlangeA flangeA1(redeclare package Medium = TubeMedium,
      p(start=pt_nom),
      m_flow(start=mt_nom),
      h_outflow(start=hti_nom))
      annotation (Placement(transformation(extent={{70,-10},{90,10}}),
          iconTransformation(extent={{70,-10},{90,10}})));
    ThermoPower.Water.FlangeB flangeB1(redeclare package Medium = TubeMedium,
      p(start=pt_nom - dpt_nom),
      m_flow(start=-mt_nom),
      h_outflow(start=hto_nom))
      annotation (Placement(transformation(extent={{-88,-10},{-68,10}}),
          iconTransformation(extent={{-88,-10},{-68,10}})));
    ThermoPower.Water.Flow1DFV2ph shell_2ph(
      redeclare package Medium = ShellMedium,
      N=Nodes,
      L=L,
      A=As,
      omega=Np*Nt*pi*do,
      Dhyd=4*(pitch^2 - pi*do^2/4)/pi/do,
      wnom=ms_nom,
      FFtype=ThermoPower.Choices.Flow1D.FFtypes.NoFriction,
      FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases,
      pstart=ps_nom,
      hstartin=hsi_nom,
      hstartout=hso_nom,
      redeclare model HeatTransfer =
          ShellHeatTransfer,
      fixedMassFlowSimplified=true,
      Q(start=-Qnom))                       annotation (Placement(transformation(
          extent={{14,-14},{-14,14}},
          rotation=180,
          origin={0,50})));
    ThermoPower.Water.Flow1DFV tube(
      redeclare package Medium = TubeMedium,
      N=Nodes,
      Nt=Nt,
      L=L*Np,
      A=pi*di^2/4,
      omega=pi*di,
      Dhyd=di,
      wnom=mt_nom,
      FFtype=ThermoPower.Choices.Flow1D.FFtypes.NoFriction,
      dpnom=dpt_nom,
      Kfnom=Cf_t,
      e=1e-6,
      pstart=pt_nom,
      hstartin=hti_nom,
      hstartout=hto_nom,
      fixedMassFlowSimplified=true,
      redeclare model HeatTransfer =
          TubeHeatTransfer,
      Q(start=Qnom))
      annotation (Placement(transformation(extent={{14,-64},{-14,-36}})));
    ThermoPower.Thermal.MetalTubeFV metalTubeFV(
      Nw=Nodes - 1,
      Nt=Np*Nt,
      L=L,
      rint=di/2,
      rext=do/2,
      rhomcm=rhom*cpm,
      lambda=km,
      Tstartbar=ShellMedium.temperature_ph(ps_nom, hso_nom))
                                                annotation (Placement(
          transformation(extent={{-14,-14},{14,14}}, rotation=180,
          origin={0,-10})));
    ThermoPower.Thermal.HeatExchangerTopologyFV heatExchangerTopologyFV(Nw=Nodes -
          1, redeclare model HeatExchangerTopology =
          ThermoPower.Thermal.HeatExchangerTopologies.CounterCurrentFlow)
                                                                    annotation (
        Placement(transformation(
          extent={{-12,-12},{12,12}},
          rotation=180,
          origin={0,16})));

    ThermoPower.Water.SensT1 sensT1_1(redeclare package Medium = TubeMedium, T(
          start=TubeMedium.temperature_ph(pt_nom,hto_nom)))
      annotation (Placement(transformation(extent={{-54,-6},{-34,14}})));
    ThermoPower.Water.PressDrop pressDrop1(
      wnom=mt_nom,
      FFtype=ThermoPower.Choices.PressDrop.FFtypes.Kf,
      Kf=Cf_t,
      dp(start=dpt_nom),
      dpnom=dpt_nom,
      w(start=mt_nom))
      annotation (Placement(transformation(extent={{-10,-10},{10,10}},
          rotation=90,
          origin={-26,-26})));
  protected
      constant Real pi=Modelica.Constants.pi;

  equation
    connect(flangeA1, tube.infl) annotation (Line(points={{80,0},{60,0},{60,-50},{
            14,-50}},  color={0,0,255}));
    connect(shell_2ph.wall, heatExchangerTopologyFV.side2)
      annotation (Line(points={{0,43},{0,19.72}}, color={255,127,0}));
    connect(heatExchangerTopologyFV.side1, metalTubeFV.ext)
      annotation (Line(points={{0,12.4},{0,-5.66}},color={255,127,0}));
    connect(flangeA, shell_2ph.infl) annotation (Line(points={{0,78},{-60,78},{-60,
            50},{-14,50}}, color={0,0,255}));
    connect(metalTubeFV.int, tube.wall)
      annotation (Line(points={{0,-14.2},{0,-43}}, color={255,127,0}));
    connect(flangeB1, sensT1_1.flange)
      annotation (Line(points={{-78,0},{-44,0}}, color={0,0,255}));
    connect(shell_2ph.outfl, flangeB) annotation (Line(points={{14,50},{40,50},{40,
            -80},{0,-80}},                    color={0,0,255}));
    connect(flangeB1, pressDrop1.outlet)
      annotation (Line(points={{-78,0},{-26,0},{-26,-16}}, color={0,0,255}));
    connect(pressDrop1.inlet, tube.outfl)
      annotation (Line(points={{-26,-36},{-26,-50},{-14,-50}}, color={0,0,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-80,80},{80,-80}},
            lineColor={0,0,0},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid,
            lineThickness=0.5),
          Line(
            points={{-2,80},{-2,40},{-22,20},{22,-20},{-2,-40},{-2,-80}},
            color={0,0,255},
            thickness=1,
            rotation=90,
            origin={0,2})}),                                       Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end ShellAndTube_BOP;

  model MoistureSeparator
    replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
      Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model"
      annotation(choicesAllMatching = true);

    parameter Modelica.Units.SI.PerUnit etaMS = 1 "Separator efficiency";
    parameter Modelica.Units.SI.MassFlowRate min_nom= 193.85;
    parameter Modelica.Units.SI.PressureDifference dp_nom = 0;
    Modelica.Units.SI.PressureDifference dp;

    ThermoPower.Water.FlangeA inlet
      annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
    ThermoPower.Water.FlangeB steam
      annotation (Placement(transformation(extent={{90,-10},{110,10}})));
    ThermoPower.Water.FlangeB drain
      annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));

    Medium.SpecificEnthalpy h_in "Feedwater specific enthalpy";
    Medium.SpecificEnthalpy h_steam "Steam specific enthalpy";
    Medium.SpecificEnthalpy h_drain "Steam specific enthalpy";
    Medium.SpecificEnthalpy hl "Specific enthalpy of saturated liquid";
    Medium.SpecificEnthalpy hv "Specific enthalpy of saturated steam";

    Modelica.Units.SI.Pressure p;

    Medium.SaturationProperties sat "Saturation conditions";
    Medium.MassFraction x "Steam quality";
    //Medium.MassFraction x_out "Steam quality";

    Modelica.Units.SI.MassFlowRate m_in(start=min_nom);
    Modelica.Units.SI.MassFlowRate m_drain;
    Modelica.Units.SI.MassFlowRate m_steam;

  equation
    // Fluid properties
    sat.psat = p+dp;
    sat.Tsat = Medium.saturationTemperature(p+dp);
    hl = Medium.bubbleEnthalpy(sat);
    hv = Medium.dewEnthalpy(sat);
    x = if h_in<=hl then 0 elseif h_in>=hv then 1 else (h_in-hl)/(hv-hl);

    // Boundary conditions
    h_in = inlet.h_outflow;
    h_in = inStream(inlet.h_outflow);
    //h_steam = actualStream(steam.h_outflow);
    //h_drain = actualStream(drain.h_outflow);
    h_steam = steam.h_outflow;
    h_drain = drain.h_outflow;

    p+dp = inlet.p;
    p = steam.p;
    //p = drain.p;
    dp = dp_nom;//*m_in/min_nom;

    m_in = inlet.m_flow;
    m_steam = steam.m_flow;
    m_drain = drain.m_flow;

    // Steady state balance equations
    0 = m_in + m_steam + m_drain;
    0 = m_in*h_in + m_steam*h_steam + m_drain*h_drain;

    // Closure relations
    //m_drain = -m_in*(1-x)*etaMS;
    m_steam = if x>0 then -m_in*x*etaMS else 0;
    h_drain = if x>0 then hl else h_in;
    //h_steam = hl+x_out*(hv-hl);
    //m_steam = m_in*(1-(1-x)*etaMS);
    //x_out = x/(x+(1-x)*(1-etaMS));

    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Rectangle(
            extent={{-61.4881,61.488},{79.9333,-79.9333}},
            lineColor={0,0,0},
            rotation=45,
            origin={-13.0427,7.10543e-15},
            lineThickness=1,
            fillColor={192,220,255},
            fillPattern=FillPattern.Solid), Polygon(
            points={{-102,0},{102,0},{0,-102},{-102,0}},
            lineColor={0,0,0},
            lineThickness=1,
            fillPattern=FillPattern.Sphere,
            fillColor={0,0,255})}),                                Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end MoistureSeparator;

  model FWtank_vectPorts
      replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
      Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model"
      annotation(choicesAllMatching = true);

      parameter Integer N_in = 4;
      parameter Integer N_out=1;

      parameter Modelica.Units.SI.Volume V;

      parameter ThermoPower.Choices.Init.Options initOpt=system.initOpt;

      parameter Boolean FixedInitPressure = true;
      parameter Modelica.Units.SI.Pressure pstart = 7.679e5;

      //parameter Modelica.Units.SI.SpecificEnthalpy hin_start[N_in] = {713.6e3,713.6e3, 1122.1e3,375.122e3} ;
      parameter Modelica.Units.SI.SpecificEnthalpy hout_start = 487.011e3;

      parameter Modelica.Units.SI.PressureDifference dp_nom=0;
      //parameter Modelica.Units.SI.MassFlowRate mout_nom[N_out] = {239.68};

      Modelica.Units.SI.PressureDifference dp(start=dp_nom);

      outer ThermoPower.System system "System wide properties";

       Medium.ThermodynamicState fluidState;

    Modelica.Units.SI.Mass M;
    Modelica.Units.SI.Energy E;

    ThermoPower.Water.FlangeA inlet[N_in] annotation (Placement(transformation(extent={{
              -10,40},{10,60}}), iconTransformation(extent={{-10,40},{10,60}})));
    ThermoPower.Water.FlangeB outlet[N_out]
      annotation (Placement(transformation(extent={{-10,-60},{10,-40}}),
          iconTransformation(extent={{-10,-60},{10,-40}})));

    Medium.SpecificEnthalpy hin[N_in];
    Medium.SpecificEnthalpy hout(start = hout_start,stateSelect=StateSelect.prefer);
    /*
  Medium.SpecificEnthalpy hin_1(start = hin1_start);
  Medium.SpecificEnthalpy hin_2(start = hin2_start);
  Medium.SpecificEnthalpy hin_3(start = hin3_start);
  Medium.SpecificEnthalpy hin_4(start = hin4_start);
  Medium.SpecificEnthalpy hout(start = hout_start); */

    Modelica.Units.SI.Pressure p(start=pstart,stateSelect=StateSelect.prefer);

    Modelica.Units.SI.MassFlowRate min[N_in];
    Modelica.Units.SI.MassFlowRate mout[N_out];

    /*Modelica.Units.SI.MassFlowRate min_1;
  Modelica.Units.SI.MassFlowRate min_2;
  Modelica.Units.SI.MassFlowRate min_3;
  Modelica.Units.SI.MassFlowRate min_4;
  Modelica.Units.SI.MassFlowRate mout;*/

  equation

    // Boundary conditions
    for i in 1:N_in loop
      hin[i] = inlet[i].h_outflow;
      hin[i] =inStream(inlet[i].h_outflow);
      min[i] = inlet[i].m_flow;
      p = inlet[i].p;

    end for;

    for i in 1:N_out loop
      hout = outlet[i].h_outflow;
      mout[i]  = outlet[i].m_flow;
      p-dp = outlet[i].p;

    end for;

    dp = dp_nom;

    fluidState = Medium.setState_phX(p, hout);
    M = V*Medium.density(fluidState);
    E = M*Medium.specificInternalEnergy(fluidState);

    // Steady state balance equations
    if V>0 then
      der(M) = sum(min) + sum(mout);
      der(E) = sum(min.*hin)+sum(mout*hout); //min_1*hin_1 + min_2*hin_2 + min_3*hin_3 + min_4*hin_4 + mout*hout;
    else
      0 = sum(min) + sum(mout);
      0 =  sum(min.*hin)+sum(mout*hout);
    end if;

  initial equation
    if initOpt == ThermoPower.Choices.Init.Options.steadyState then
      //der(M)=0;
      //der(E)=0;
      der(p)=0;
      der(hout)=0;


      if FixedInitPressure then
        p=pstart;
      end if;

    else
      p = pstart;
    end if;


    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Ellipse(
            extent={{-85,50},{85,-50}},
            lineColor={0,0,0},
            fillColor={192,220,255},
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=360,
            lineThickness=0.5), Ellipse(
            extent={{-85,50},{85,-50}},
            lineColor={0,0,0},
            fillColor={0,0,255},
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=180,
            lineThickness=0.5)}),                                  Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end FWtank_vectPorts;

  model SteamTurbine_variableEta
    "Steam turbine: Stodola's ellipse law and variable isentropic efficiency"
    extends ThermoPower.Water.BaseClasses.SteamTurbineBase(eta_iso(start=eta_iso_nom));
    parameter Modelica.Units.SI.PerUnit eta_iso_nom=0.92 "Nominal isentropic efficiency";
    parameter Modelica.Units.SI.Area Kt "Kt coefficient of Stodola's law";

    parameter Boolean corrWet = true "Wettness losses" annotation(Dialog(group="Efficiency losses"));
    parameter Boolean corrFlow = true "Flow losses" annotation(Dialog(group="Efficiency losses"));
    parameter Real C_Bau = 1 "Baumann coefficient" annotation(Dialog(group="Efficiency losses",enable=corrWet));
    parameter Real alpha = 0.14 "Flow loss coefficient" annotation(Dialog(group="Efficiency losses",enable=corrFlow));
    parameter Real xsi = 1.4 "Flow loss exponent" annotation(Dialog(group="Efficiency losses",enable=corrFlow));

    Medium.Density rho "Inlet density";

    // New variables
    Medium.SaturationProperties sat_in;
    Medium.SaturationProperties sat_out;
    Real x_in;
    Real x_out;
    Real xm;

  protected
    Real effWet_factor;
    Real effFlow_factor;

  equation
    sat_in.psat = pin;
    sat_in.Tsat = Medium.saturationTemperature(pin);
    sat_out.psat = pout;
    sat_out.Tsat = Medium.saturationTemperature(pout);

    x_in = if hin> Medium.dewEnthalpy(sat_in) then 1 elseif hin<Medium.bubbleEnthalpy(sat_in) then 0 else (hin-Medium.bubbleEnthalpy(sat_in))/( Medium.dewEnthalpy(sat_in)-Medium.bubbleEnthalpy(sat_in));
    x_out = if hout> Medium.dewEnthalpy(sat_out) then 1 elseif hout<Medium.bubbleEnthalpy(sat_out) then 0 else (hout-Medium.bubbleEnthalpy(sat_out))/( Medium.dewEnthalpy(sat_out)-Medium.bubbleEnthalpy(sat_out));
    xm = homotopy((x_in+x_out)/2,1);

    rho =  Medium.density(steamState_in);
    w = homotopy(Kt*theta*sqrt(pin*rho)*ThermoPower.Functions.sqrtReg(1 - (1/PR)^2),
                 theta*wnom/pnom*pin) "Stodola's law";

     // Wettness correction using Baumann's law
     effWet_factor = if corrWet then (1-(1-xm)*C_Bau) else 1;

     // Flow losses
     effFlow_factor = if corrFlow then homotopy((1-alpha*(abs(1-w/wnom))^xsi),1) else 1;

     eta_iso = homotopy(eta_iso_nom*effWet_factor*effFlow_factor,eta_iso_nom) "Efficiency correction";

    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end SteamTurbine_variableEta;
end Components;
