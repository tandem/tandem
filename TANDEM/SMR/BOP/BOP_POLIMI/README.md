
# Dynamic BOP model in ThermoPower

A third dynamic model of the BOP has been developed in the [ThermoPower](https://github.com/casella/ThermoPower) library. The same architecture and operating conditions obtained from the CYCLOP optimisation have been implemented, despite some differences in terms of pressure levels due to different pressure drops in the heat exchanger components.

The BOP model features several steam extraction and return points. The following extraction points are included:
- High temperature steam extraction at the NSSS-SG outlet (45 bar, 300°C)
- Intermediate temperature steam extraction at the HP turbine outlet (7.547 bar, 168°C)
- Low temperature steam extraction at the LP-turbine (0.8 bar, 92°C)
- Liquid water extraction from the feedwater tank (7.147 bar, 108°C)

Return points:
- High temperature return point at the LP turbine inlet
- High pressure return point at the SG inlet
- Intermediate temperature return to the feedwater tank
- Low temperature return to the condenser

## Package description
- The `Test` subpackage provides several demonstration cases devoted to showing possible utilisation of the BOP models
- Two versions of the BOP are available, the first featuring only one heat extraction point and a fluid exchange interface with the NSSS and the second, more detailed, with several extraction and return points and relying on thermal power as exchange variable with the NSSS.
- The main components developed for the BOP, such as the heat exchanger and the moisture separator, are gathered in the `Components` package.
- Lastly, the `Control` encompasses some illustrative control systems to regulate the BOP operation according to user requirements.

## Component description
The component described in this section will be the `BOPdyn_thermal` component, which results from advancing the developments started with the `BOPdyn_fluid` model.

Most of the subcomponents on which the model is built stem from the ThermoPower library and are based on the following assumptions:
- [ValveVap](https://build.openmodelica.org/Documentation/ThermoPower.Water.ValveVap.html) and [ValveLiq](https://build.openmodelica.org/Documentation/ThermoPower.Water.ValveLiq.html) components to model the valves in the system
- Turbine stage models based on [SteamTurbineStodola](https://build.openmodelica.org/Documentation/ThermoPower.Water.SteamTurbineStodola.html), extended to be able to include wettness losses (through Baumann's rule) and flow losses
- HP and LP pumps modelled adopting ThermoPower's [Pump](https://build.openmodelica.org/Documentation/ThermoPower.Water.Pump.html), implementing a quadratic characteristic curve
- Ideal condenser, characterised by a fixed condensation pressure, based on the [PrescribedPressureCondenser](https://build.openmodelica.org/Documentation/ThermoPower.Water.Pump.html) component
- Ideal moisture separator draining the liquid fraction of the two-phase mixture with unitary efficiency
- Concentrated pressure losses are proportional to the square of the fluid velocity
- Feedwater tank model based on dynamic mass and energy balance equations, encompass a user-defined number of inlets and outlets to be able to flexiblely accommodate multiple fluid extractions and returns
- Generator modelled with the ThermoPower [Generator](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.Generator.html) component, used to convert the mechanical power into active power generation, accounting for the intertia of the turbo-generator group.
- Dynamic, one-dimensional shell and tube heat exchangers are used to model the HP and LP preheaters as well as the reheater. They have been sized according to the nominal conditions proposed by the CYCLOP optmization tool. The shell and tube sides are modelled through a finite volume discretization approach, using the [Flow1DFV2ph](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV2ph.html) and the [Flow1DFV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html) components, respectively. The heat transfer model relies on the Dittus-Boelter correlation in the case of single-phase fluid conditions, whereas a constant heat transfer coefficient is assumed during phase transition. The shell and tube side flow components are thermally connected through a component accounting for the counterflow disposition and the [MetalTubeFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.MetalTubeFV.html), used to simulate the thermal resistance and inertia of the heat exchanger's solid structure.

## Control strategy
In addition to illustrative controllers developed to showcase potential applications of the model, the `Control` package encompasses an actuator and sensor signal bus used to collect the main control signals of the BOP. In this way, the user is facilitated to develop its own external control system and couple it to the BOP model to test the dynamic response of the system.

At this stage, the following actuator and sensor signals are included in the model:

| Sensor signal | Actuator signal |
|-- | -- |
| Steam generator inlet temperature | Steam generator admission valve opening |
| Steam generator outlet temperature | HP-turbine admission valve opening |
| Steam generator pressure | HP-turbine outlet control valve |
| HP-turbine outlet extraction pressure | Reheater hot side flow control valve |
| LP-turbine inlet temperature | LP-turbine extraction valve |
| LP-turbine outlet extraction pressure | HP pump rotational speed |
| Feedwater tank pressure | LP pump rotationl speed |
| BOP power output | HP-preheater control valve |

The user is free to choose the process and control variable pairs that best suit their application and design the corresponding controller.

## Provided examples
Test cases to showcase the behaviour of the BOP models in different scenarios are available in the `Test` package. In particular, the models are tested with different heat sources (ideal heat source, steam generator, or NSSS) and different cogeneration requirements. For the latter, the thermal power extractions in `Test_BOPth_0Dsource` and `Test_BOPth_NSSS` correspond to those considered in the `BOP_TSPro` package.
