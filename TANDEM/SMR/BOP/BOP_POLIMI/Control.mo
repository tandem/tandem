within TANDEM.SMR.BOP.BOP_POLIMI;
package Control
  model BOPcontroller_SST
    ActuatorBus actuatorBus
      annotation (Placement(transformation(extent={{-70,-130},{-50,-110}})));
    SensorBus sensorBus
      annotation (Placement(transformation(extent={{50,-130},{70,-110}})));
    Modelica.Blocks.Interfaces.RealInput Kv_HP_TAV
      annotation (Placement(transformation(extent={{-168,70},{-128,110}})));
    Modelica.Blocks.Interfaces.RealInput Kv_SGin
      annotation (Placement(transformation(extent={{-168,30},{-128,70}})));
    Modelica.Blocks.Interfaces.RealInput Kv_RH
      annotation (Placement(transformation(extent={{-166,-10},{-126,30}})));
    Modelica.Blocks.Interfaces.RealInput Kv_LP_TAV
      annotation (Placement(transformation(extent={{-166,-50},{-126,-10}})));
    Modelica.Blocks.Math.Add add
      annotation (Placement(transformation(extent={{-86,80},{-74,92}})));
    Modelica.Blocks.Sources.Constant const(k=1)
      annotation (Placement(transformation(extent={{-120,100},{-106,114}})));
    Modelica.Blocks.Math.Add add1
      annotation (Placement(transformation(extent={{-86,44},{-74,56}})));
    Modelica.Blocks.Math.Add add2
      annotation (Placement(transformation(extent={{-86,4},{-74,16}})));
    Modelica.Blocks.Math.Add add3
      annotation (Placement(transformation(extent={{-86,-34},{-74,-22}})));
    Modelica.Blocks.Math.Add add4(k1=-1)
      annotation (Placement(transformation(extent={{60,80},{72,92}})));
    Modelica.Blocks.Sources.Constant const1(k=45e5)
      annotation (Placement(transformation(extent={{26,96},{40,110}})));
    Modelica.Blocks.Math.Division division
      annotation (Placement(transformation(extent={{88,96},{100,84}})));
    Modelica.Blocks.Interfaces.RealOutput pSG
      annotation (Placement(transformation(extent={{116,80},{136,100}})));
    Modelica.Blocks.Math.Add add5(k1=-1)
      annotation (Placement(transformation(extent={{60,30},{72,42}})));
    Modelica.Blocks.Sources.Constant const2(k=300 + 273.15)
      annotation (Placement(transformation(extent={{26,46},{40,60}})));
    Modelica.Blocks.Math.Division division1
      annotation (Placement(transformation(extent={{88,46},{100,34}})));
    Modelica.Blocks.Interfaces.RealOutput T_SGout
      annotation (Placement(transformation(extent={{116,30},{136,50}})));
    Modelica.Blocks.Math.Add add6(k1=-1)
      annotation (Placement(transformation(extent={{60,-10},{72,2}})));
    Modelica.Blocks.Sources.Constant const3(k=260.44 + 273.15)
      annotation (Placement(transformation(extent={{26,6},{40,20}})));
    Modelica.Blocks.Math.Division division2
      annotation (Placement(transformation(extent={{88,6},{100,-6}})));
    Modelica.Blocks.Interfaces.RealOutput T_LPTin
      annotation (Placement(transformation(extent={{116,-10},{136,10}})));
    Modelica.Blocks.Math.Add add7(k1=-1)
      annotation (Placement(transformation(extent={{60,-52},{72,-40}})));
    Modelica.Blocks.Sources.Constant const4(k=7.147e5)
      annotation (Placement(transformation(extent={{26,-36},{40,-22}})));
    Modelica.Blocks.Math.Division division3
      annotation (Placement(transformation(extent={{88,-36},{100,-48}})));
    Modelica.Blocks.Interfaces.RealOutput P_LPTin
      annotation (Placement(transformation(extent={{116,-52},{136,-32}})));
    Modelica.Blocks.Math.Add add8
      annotation (Placement(transformation(extent={{-86,-66},{-74,-54}})));
    Modelica.Blocks.Interfaces.RealInput N_HPpump
      annotation (Placement(transformation(extent={{-166,-90},{-126,-50}})));
    Modelica.Blocks.Math.Add add9
      annotation (Placement(transformation(extent={{-86,-98},{-74,-86}})));
    Modelica.Blocks.Interfaces.RealInput N_LPpump
      annotation (Placement(transformation(extent={{-166,-128},{-126,-88}})));
    Modelica.Blocks.Math.Gain gain(k=1500)
      annotation (Placement(transformation(extent={{-62,-66},{-50,-54}})));
    Modelica.Blocks.Math.Gain gain1(k=1500)
      annotation (Placement(transformation(extent={{-64,-98},{-52,-86}})));
    Modelica.Blocks.Math.Add add10(k1=-1)
      annotation (Placement(transformation(extent={{60,-90},{72,-78}})));
    Modelica.Blocks.Sources.Constant const5(k=173.53e6)
      annotation (Placement(transformation(extent={{26,-74},{40,-60}})));
    Modelica.Blocks.Math.Division division4
      annotation (Placement(transformation(extent={{88,-74},{100,-86}})));
    Modelica.Blocks.Interfaces.RealOutput Pel
      annotation (Placement(transformation(extent={{116,-90},{136,-70}})));
  equation
    connect(add.u2, Kv_HP_TAV) annotation (Line(points={{-87.2,82.4},{-120,82.4},
            {-120,90},{-148,90}}, color={0,0,127}));
    connect(const.y, add.u1) annotation (Line(points={{-105.3,107},{-100,107},{
            -100,89.6},{-87.2,89.6}}, color={0,0,127}));
    connect(Kv_SGin, add1.u2) annotation (Line(points={{-148,50},{-112,50},{
            -112,46},{-87.2,46},{-87.2,46.4}},
                                          color={0,0,127}));
    connect(const.y, add1.u1) annotation (Line(points={{-105.3,107},{-100,107},
            {-100,53.6},{-87.2,53.6}},color={0,0,127}));
    connect(const.y, add2.u1) annotation (Line(points={{-105.3,107},{-100,107},
            {-100,13.6},{-87.2,13.6}},  color={0,0,127}));
    connect(const.y, add3.u1) annotation (Line(points={{-105.3,107},{-100,107},
            {-100,-24.4},{-87.2,-24.4}},color={0,0,127}));
    connect(Kv_RH, add2.u2) annotation (Line(points={{-146,10},{-112,10},{-112,
            6.4},{-87.2,6.4}},     color={0,0,127}));
    connect(Kv_LP_TAV, add3.u2) annotation (Line(points={{-146,-30},{-114,-30},
            {-114,-32},{-88,-32},{-88,-31.6},{-87.2,-31.6}},color={0,0,127}));
    connect(actuatorBus.Kv_RH, add2.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,10},{-73.4,10}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_SGin, add1.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,50},{-73.4,50}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_HP_TAV, add.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,86},{-73.4,86}},
        color={80,200,120},
        thickness=0.5));
    connect(add4.y, division.u1) annotation (Line(points={{72.6,86},{86.8,86},{
            86.8,86.4}}, color={0,0,127}));
    connect(const1.y, add4.u1) annotation (Line(points={{40.7,103},{50,103},{50,
            89.6},{58.8,89.6}}, color={0,0,127}));
    connect(division.u2, const1.y) annotation (Line(points={{86.8,93.6},{80,93.6},
            {80,103},{40.7,103}}, color={0,0,127}));
    connect(division.y, pSG)
      annotation (Line(points={{100.6,90},{126,90}}, color={0,0,127}));
    connect(add5.y, division1.u1) annotation (Line(points={{72.6,36},{86.8,36},{
            86.8,36.4}}, color={0,0,127}));
    connect(const2.y, add5.u1) annotation (Line(points={{40.7,53},{50,53},{50,
            39.6},{58.8,39.6}}, color={0,0,127}));
    connect(division1.u2, const2.y) annotation (Line(points={{86.8,43.6},{80,43.6},
            {80,53},{40.7,53}}, color={0,0,127}));
    connect(division1.y, T_SGout)
      annotation (Line(points={{100.6,40},{126,40}}, color={0,0,127}));
    connect(add6.y, division2.u1) annotation (Line(points={{72.6,-4},{86.8,-4},
            {86.8,-3.6}},  color={0,0,127}));
    connect(const3.y, add6.u1) annotation (Line(points={{40.7,13},{50,13},{50,
            -0.4},{58.8,-0.4}},   color={0,0,127}));
    connect(division2.u2, const3.y) annotation (Line(points={{86.8,3.6},{80,3.6},
            {80,13},{40.7,13}},        color={0,0,127}));
    connect(division2.y, T_LPTin)
      annotation (Line(points={{100.6,0},{126,0}},     color={0,0,127}));
    connect(add7.y, division3.u1) annotation (Line(points={{72.6,-46},{86.8,-46},
            {86.8,-45.6}}, color={0,0,127}));
    connect(const4.y, add7.u1) annotation (Line(points={{40.7,-29},{50,-29},{50,
            -42.4},{58.8,-42.4}}, color={0,0,127}));
    connect(division3.u2, const4.y) annotation (Line(points={{86.8,-38.4},{80,
            -38.4},{80,-29},{40.7,-29}}, color={0,0,127}));
    connect(division3.y, P_LPTin)
      annotation (Line(points={{100.6,-42},{126,-42}}, color={0,0,127}));
    connect(sensorBus.T_LPT, add6.u2) annotation (Line(
        points={{60,-120},{60,-100},{16,-100},{16,-7.6},{58.8,-7.6}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.T_SGout, add5.u2) annotation (Line(
        points={{60,-120},{60,-100},{16,-100},{16,32.4},{58.8,32.4}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.P_SG, add4.u2) annotation (Line(
        points={{60,-120},{60,-100},{16,-100},{16,82.4},{58.8,82.4}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.Kv_LP_TAV, add3.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,-28},{-73.4,-28}},
        color={80,200,120},
        thickness=0.5));
    connect(const.y,add8. u1) annotation (Line(points={{-105.3,107},{-100,107},
            {-100,-56.4},{-87.2,-56.4}},color={0,0,127}));
    connect(N_HPpump, add8.u2) annotation (Line(points={{-146,-70},{-114,-70},{
            -114,-72},{-88,-72},{-88,-63.6},{-87.2,-63.6}}, color={0,0,127}));
    connect(const.y,add9. u1) annotation (Line(points={{-105.3,107},{-100,107},
            {-100,-88.4},{-87.2,-88.4}},color={0,0,127}));
    connect(N_LPpump, add9.u2) annotation (Line(points={{-146,-108},{-94,-108},
            {-94,-95.6},{-87.2,-95.6}}, color={0,0,127}));
    connect(add8.y, gain.u)
      annotation (Line(points={{-73.4,-60},{-63.2,-60}}, color={0,0,127}));
    connect(add9.y, gain1.u)
      annotation (Line(points={{-73.4,-92},{-65.2,-92}}, color={0,0,127}));
    connect(actuatorBus.N_LPpump, gain1.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,-92},{-51.4,-92}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.N_HPpump, gain.y) annotation (Line(
        points={{-60,-120},{-60,-104},{-30,-104},{-30,-60},{-49.4,-60}},
        color={80,200,120},
        thickness=0.5));
    connect(add10.y, division4.u1) annotation (Line(points={{72.6,-84},{86.8,
            -84},{86.8,-83.6}}, color={0,0,127}));
    connect(const5.y, add10.u1) annotation (Line(points={{40.7,-67},{50,-67},{
            50,-80.4},{58.8,-80.4}}, color={0,0,127}));
    connect(division4.u2,const5. y) annotation (Line(points={{86.8,-76.4},{80,
            -76.4},{80,-67},{40.7,-67}}, color={0,0,127}));
    connect(division4.y, Pel)
      annotation (Line(points={{100.6,-80},{126,-80}}, color={0,0,127}));
    connect(sensorBus.Pel_out, add10.u2) annotation (Line(
        points={{60,-120},{60,-100},{16,-100},{16,-87.6},{58.8,-87.6}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.P_LPTin, add7.u2) annotation (Line(
        points={{60,-120},{60,-100},{16,-100},{16,-49.6},{58.8,-49.6}},
        color={255,219,88},
        thickness=0.5));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,
              -120},{120,120}}), graphics={
          Rectangle(
            extent={{-140,120},{120,-120}},
            lineColor={102,44,145},
            fillColor={102,44,145},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-134,114},{112,-114}},
            lineColor={102,44,145},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-98,76},{86,-64}},
            textColor={102,44,145},
            textString="BOP
control")}),                      Diagram(coordinateSystem(preserveAspectRatio=
              false, extent={{-140,-120},{120,120}})));
  end BOPcontroller_SST;

  model BOPcontroller_ex1

    ActuatorBus actuatorBus
      annotation (Placement(transformation(extent={{-70,-150},{-50,-130}})));
    SensorBus sensorBus
      annotation (Placement(transformation(extent={{50,-150},{70,-130}})));
    Modelica.Blocks.Math.Add add
      annotation (Placement(transformation(extent={{44,102},{56,114}})));
    Modelica.Blocks.Sources.Constant const(k=1)
      annotation (Placement(transformation(extent={{12,120},{26,134}})));
    Modelica.Blocks.Math.Add add1
      annotation (Placement(transformation(extent={{44,56},{56,68}})));
    Modelica.Blocks.Math.Add add2
      annotation (Placement(transformation(extent={{48,-12},{60,0}})));
    Modelica.Blocks.Math.Add add3
      annotation (Placement(transformation(extent={{44,-52},{56,-40}})));
    Modelica.Blocks.Math.Add add4(k1=-1)
      annotation (Placement(transformation(extent={{-60,104},{-48,116}})));
    Modelica.Blocks.Sources.Constant const1(k=45e5)
      annotation (Placement(transformation(extent={{-94,120},{-80,134}})));
    Modelica.Blocks.Math.Division division
      annotation (Placement(transformation(extent={{-32,120},{-20,108}})));
    Modelica.Blocks.Math.Add add5(k1=-1)
      annotation (Placement(transformation(extent={{-60,54},{-48,66}})));
    Modelica.Blocks.Sources.Constant const2(k=300 + 273.15)
      annotation (Placement(transformation(extent={{-94,70},{-80,84}})));
    Modelica.Blocks.Math.Division division1
      annotation (Placement(transformation(extent={{-32,70},{-20,58}})));
    Modelica.Blocks.Math.Add add6(k1=-1)
      annotation (Placement(transformation(extent={{-60,-6},{-48,6}})));
    Modelica.Blocks.Sources.Constant const3(k=260.44 + 273.15)
      annotation (Placement(transformation(extent={{-94,10},{-80,24}})));
    Modelica.Blocks.Math.Division division2
      annotation (Placement(transformation(extent={{-32,10},{-20,-2}})));
    Modelica.Blocks.Math.Add add7(k1=-1)
      annotation (Placement(transformation(extent={{-60,-54},{-48,-42}})));
    Modelica.Blocks.Sources.Constant const4(k=6.847e5)
      annotation (Placement(transformation(extent={{-94,-38},{-80,-24}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*38.9612,
      Ti=38.9612/67.6832,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-6,108},{8,94}})));
    Modelica.Blocks.Math.Division division3
      annotation (Placement(transformation(extent={{-32,-38},{-20,-50}})));
    Modelica.Blocks.Sources.Constant const5(k=0)
      annotation (Placement(transformation(extent={{-30,84},{-18,96}})));
    Modelica.Blocks.Continuous.LimPID PID1(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*1e-2,
      Ti=1e-2/0.7868,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-8,58},{6,44}})));
    Modelica.Blocks.Sources.Constant const6(k=0)
      annotation (Placement(transformation(extent={{-32,34},{-20,46}})));
    Modelica.Blocks.Continuous.LimPID PID2(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=12.3513,
      Ti=12.3513/0.078988,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-6,-6},{8,-20}})));
    Modelica.Blocks.Sources.Constant const7(k=0)
      annotation (Placement(transformation(extent={{-30,-30},{-18,-18}})));
    Modelica.Blocks.Continuous.LimPID PID3(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*6.439,
      Ti=6.439/88.2,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-8,-54},{6,-68}})));
    Modelica.Blocks.Sources.Constant const8(k=0)
      annotation (Placement(transformation(extent={{-32,-72},{-20,-60}})));
    Modelica.Blocks.Sources.Constant N_HPpump(k=1500) annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={144,-122})));
    Modelica.Blocks.Math.Add add8
      annotation (Placement(transformation(extent={{44,-84},{56,-96}})));
    Modelica.Blocks.Math.Gain gain(k=1500)
      annotation (Placement(transformation(extent={{76,-96},{88,-84}})));
    Modelica.Blocks.Math.Add add9(k1=-1)
      annotation (Placement(transformation(extent={{-56,-98},{-44,-86}})));
    Modelica.Blocks.Sources.Constant const10(k=7.147e5)
      annotation (Placement(transformation(extent={{-90,-82},{-76,-68}})));
    Modelica.Blocks.Math.Division division4
      annotation (Placement(transformation(extent={{-36,-82},{-24,-94}})));
    Modelica.Blocks.Continuous.LimPID PID4(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=0.66946,
      Ti=0.66946/6.1843,
      yMax=0.5,
      initType=Modelica.Blocks.Types.Init.SteadyState)
      annotation (Placement(transformation(extent={{-12,-98},{2,-112}})));
    Modelica.Blocks.Sources.Constant const11(k=0)
      annotation (Placement(transformation(extent={{-36,-116},{-24,-104}})));
  equation
    connect(const.y, add.u1) annotation (Line(points={{26.7,127},{30,127},{30,111.6},
            {42.8,111.6}},      color={0,0,127}));
    connect(const.y, add1.u1) annotation (Line(points={{26.7,127},{30,127},{30,65.6},
            {42.8,65.6}},       color={0,0,127}));
    connect(const.y, add2.u1) annotation (Line(points={{26.7,127},{30,127},{30,-2.4},
            {46.8,-2.4}},       color={0,0,127}));
    connect(const.y, add3.u1) annotation (Line(points={{26.7,127},{30,127},{30,
            -42.4},{42.8,-42.4}}, color={0,0,127}));
    connect(add4.y, division.u1) annotation (Line(points={{-47.4,110},{-33.2,110},
            {-33.2,110.4}},
                          color={0,0,127}));
    connect(const1.y, add4.u1) annotation (Line(points={{-79.3,127},{-70,127},{-70,
            113.6},{-61.2,113.6}},   color={0,0,127}));
    connect(division.u2, const1.y) annotation (Line(points={{-33.2,117.6},{-40,117.6},
            {-40,127},{-79.3,127}},        color={0,0,127}));
    connect(add5.y, division1.u1) annotation (Line(points={{-47.4,60},{-33.2,60},{
            -33.2,60.4}},  color={0,0,127}));
    connect(const2.y, add5.u1) annotation (Line(points={{-79.3,77},{-70,77},{-70,63.6},
            {-61.2,63.6}},       color={0,0,127}));
    connect(division1.u2, const2.y) annotation (Line(points={{-33.2,67.6},{-40,67.6},
            {-40,77},{-79.3,77}},       color={0,0,127}));
    connect(add6.y, division2.u1) annotation (Line(points={{-47.4,0},{-33.2,0},{-33.2,
            0.4}},          color={0,0,127}));
    connect(const3.y, add6.u1) annotation (Line(points={{-79.3,17},{-70,17},{-70,3.6},
            {-61.2,3.6}},          color={0,0,127}));
    connect(division2.u2, const3.y) annotation (Line(points={{-33.2,7.6},{-40,7.6},
            {-40,17},{-79.3,17}},     color={0,0,127}));
    connect(add7.y, division3.u1) annotation (Line(points={{-47.4,-48},{-48,
            -47.6},{-33.2,-47.6}},
                            color={0,0,127}));
    connect(const4.y, add7.u1) annotation (Line(points={{-79.3,-31},{-70,-31},{
            -70,-44.4},{-61.2,-44.4}},
                                   color={0,0,127}));
    connect(division3.u2, const4.y) annotation (Line(points={{-33.2,-40.4},{-40,
            -40.4},{-40,-31},{-79.3,-31}},
                                    color={0,0,127}));
    connect(sensorBus.T_LPT, add6.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-3.6},{-61.2,-3.6}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.T_SGout, add5.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,56},{-68,56},{-68,56.4},{
            -61.2,56.4}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.P_SG, add4.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,106.4},{-61.2,106.4}},
        color={255,219,88},
        thickness=0.5));
    connect(division.y, PID.u_m)
      annotation (Line(points={{-19.4,114},{1,114},{1,109.4}},color={0,0,127}));
    connect(const5.y, PID.u_s) annotation (Line(points={{-17.4,90},{-14,90},{-14,101},
            {-7.4,101}},    color={0,0,127}));
    connect(PID.y, add.u2) annotation (Line(points={{8.7,101},{16,101},{16,104.4},
            {42.8,104.4}},
                         color={0,0,127}));
    connect(const6.y, PID1.u_s) annotation (Line(points={{-19.4,40},{-16,40},{-16,
            51},{-9.4,51}}, color={0,0,127}));
    connect(const7.y, PID2.u_s) annotation (Line(points={{-17.4,-24},{-14,-24},{-14,
            -13},{-7.4,-13}},     color={0,0,127}));
    connect(const8.y, PID3.u_s) annotation (Line(points={{-19.4,-66},{-16,-66},
            {-16,-61},{-9.4,-61}},
                              color={0,0,127}));
    connect(division3.y, PID3.u_m) annotation (Line(points={{-19.4,-44},{-14,
            -44},{-14,-42},{-1,-42},{-1,-52.6}},
                                            color={0,0,127}));
    connect(PID3.y, add3.u2) annotation (Line(points={{6.7,-61},{20,-61},{20,
            -49.6},{42.8,-49.6}}, color={0,0,127}));
    connect(division2.y, PID2.u_m)
      annotation (Line(points={{-19.4,4},{1,4},{1,-4.6}},      color={0,0,127}));
    connect(division1.y, PID1.u_m)
      annotation (Line(points={{-19.4,64},{-1,64},{-1,59.4}}, color={0,0,127}));
    connect(PID1.y, add1.u2) annotation (Line(points={{6.7,51},{20,51},{20,58.4},{
            42.8,58.4}},  color={0,0,127}));
    connect(PID2.y, add2.u2) annotation (Line(points={{8.7,-13},{20,-13},{20,-9.6},
            {46.8,-9.6}},         color={0,0,127}));
    connect(actuatorBus.Kv_RH, add2.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-6},{60.6,-6}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_SGin, add1.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,62},{56.6,62}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_HP_TAV, add.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,108},{56.6,108}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LP_TAV, add3.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-46},{56.6,-46}},
        color={80,200,120},
        thickness=0.5));
    connect(sensorBus.P_LPTin, add7.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-51.6},{-61.2,-51.6}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.N_HPpump, N_HPpump.y) annotation (Line(
        points={{-60,-140},{-60,-122},{137.4,-122}},
        color={80,200,120},
        thickness=0.5));
    connect(add9.y,division4. u1) annotation (Line(points={{-43.4,-92},{-38,-92},
            {-38,-91.6},{-37.2,-91.6}},
                            color={0,0,127}));
    connect(const10.y, add9.u1) annotation (Line(points={{-75.3,-75},{-66,-75},
            {-66,-88.4},{-57.2,-88.4}}, color={0,0,127}));
    connect(division4.u2, const10.y) annotation (Line(points={{-37.2,-84.4},{
            -42,-84.4},{-42,-75},{-75.3,-75}}, color={0,0,127}));
    connect(const11.y, PID4.u_s) annotation (Line(points={{-23.4,-110},{-20,
            -110},{-20,-105},{-13.4,-105}}, color={0,0,127}));
    connect(division4.y,PID4. u_m) annotation (Line(points={{-23.4,-88},{-18,
            -88},{-18,-86},{-5,-86},{-5,-96.6}},
                                            color={0,0,127}));
    connect(PID4.y,add8. u1) annotation (Line(points={{2.7,-105},{2.7,-106},{36,
            -106},{36,-93.6},{42.8,-93.6}},color={0,0,127}));
    connect(add8.y,gain. u)
      annotation (Line(points={{56.6,-90},{74.8,-90}}, color={0,0,127}));
    connect(sensorBus.P_FWtank, add9.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-95.6},{-57.2,-95.6}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.N_LPpump, gain.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-90},{88.6,-90}},
        color={80,200,120},
        thickness=0.5));
    connect(add8.u2, const.y) annotation (Line(points={{42.8,-86.4},{30,-86.4},
            {30,127},{26.7,127}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,-140},
              {160,140}}), graphics={
          Rectangle(
            extent={{-160,140},{160,-140}},
            lineColor={102,44,145},
            fillColor={102,44,145},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-154,134},{154,-134}},
            lineColor={102,44,145},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-102,86},{114,-78}},
            textColor={102,44,145},
            textString="BOP
control")}),                      Diagram(coordinateSystem(preserveAspectRatio=
              false, extent={{-160,-140},{160,140}})));
  end BOPcontroller_ex1;

  model BOPcontroller_ex2

    ActuatorBus actuatorBus
      annotation (Placement(transformation(extent={{-70,-150},{-50,-130}})));
    SensorBus sensorBus
      annotation (Placement(transformation(extent={{50,-150},{70,-130}})));
    Modelica.Blocks.Math.Add add
      annotation (Placement(transformation(extent={{44,102},{56,114}})));
    Modelica.Blocks.Sources.Constant const(k=1)
      annotation (Placement(transformation(extent={{12,120},{26,134}})));
    Modelica.Blocks.Math.Add add1
      annotation (Placement(transformation(extent={{44,56},{56,68}})));
    Modelica.Blocks.Math.Add add2
      annotation (Placement(transformation(extent={{48,-12},{60,0}})));
    Modelica.Blocks.Math.Add add3
      annotation (Placement(transformation(extent={{44,-52},{56,-40}})));
    Modelica.Blocks.Math.Add add4(k1=-1)
      annotation (Placement(transformation(extent={{-60,104},{-48,116}})));
    Modelica.Blocks.Sources.Constant const1(k=45e5)
      annotation (Placement(transformation(extent={{-94,120},{-80,134}})));
    Modelica.Blocks.Math.Division division
      annotation (Placement(transformation(extent={{-32,120},{-20,108}})));
    Modelica.Blocks.Math.Add add5(k1=-1)
      annotation (Placement(transformation(extent={{-60,54},{-48,66}})));
    Modelica.Blocks.Sources.Constant const2(k=300 + 273.15)
      annotation (Placement(transformation(extent={{-94,70},{-80,84}})));
    Modelica.Blocks.Math.Division division1
      annotation (Placement(transformation(extent={{-32,70},{-20,58}})));
    Modelica.Blocks.Math.Add add6(k1=-1)
      annotation (Placement(transformation(extent={{-60,-6},{-48,6}})));
    Modelica.Blocks.Sources.Constant const3(k=260.44 + 273.15)
      annotation (Placement(transformation(extent={{-94,10},{-80,24}})));
    Modelica.Blocks.Math.Division division2
      annotation (Placement(transformation(extent={{-32,10},{-20,-2}})));
    Modelica.Blocks.Math.Add add7(k1=-1)
      annotation (Placement(transformation(extent={{-60,-54},{-48,-42}})));
    Modelica.Blocks.Sources.Constant const4(k=6.847e5)
      annotation (Placement(transformation(extent={{-94,-38},{-80,-24}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*38.9612,
      Ti=38.9612/67.6832,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-6,108},{8,94}})));
    Modelica.Blocks.Math.Division division3
      annotation (Placement(transformation(extent={{-32,-38},{-20,-50}})));
    Modelica.Blocks.Sources.Constant const5(k=0)
      annotation (Placement(transformation(extent={{-30,84},{-18,96}})));
    Modelica.Blocks.Continuous.LimPID PID1(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*1e-1,
      Ti=1e-1/0.003092,
      yMax=0.5,
      initType=Modelica.Blocks.Types.Init.SteadyState)
      annotation (Placement(transformation(extent={{-8,58},{6,44}})));
    Modelica.Blocks.Sources.Constant const6(k=0)
      annotation (Placement(transformation(extent={{-32,34},{-20,46}})));
    Modelica.Blocks.Continuous.LimPID PID2(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=12.3513,
      Ti=12.3513/0.078988,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-6,-6},{8,-20}})));
    Modelica.Blocks.Sources.Constant const7(k=0)
      annotation (Placement(transformation(extent={{-30,-30},{-18,-18}})));
    Modelica.Blocks.Continuous.LimPID PID3(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*6.439,
      Ti=6.439/88.2,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-8,-54},{6,-68}})));
    Modelica.Blocks.Sources.Constant const8(k=0)
      annotation (Placement(transformation(extent={{-32,-72},{-20,-60}})));
    Modelica.Blocks.Sources.Constant Kv_SGin(k=1)    annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={144,-122})));
    Modelica.Blocks.Math.Add add8
      annotation (Placement(transformation(extent={{44,-84},{56,-96}})));
    Modelica.Blocks.Math.Gain gain(k=1500)
      annotation (Placement(transformation(extent={{76,-96},{88,-84}})));
    Modelica.Blocks.Math.Add add9(k1=-1)
      annotation (Placement(transformation(extent={{-56,-98},{-44,-86}})));
    Modelica.Blocks.Sources.Constant const10(k=7.147e5)
      annotation (Placement(transformation(extent={{-90,-82},{-76,-68}})));
    Modelica.Blocks.Math.Division division4
      annotation (Placement(transformation(extent={{-36,-82},{-24,-94}})));
    Modelica.Blocks.Continuous.LimPID PID4(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=0.66946,
      Ti=0.66946/6.1843,
      yMax=0.5,
      initType=Modelica.Blocks.Types.Init.SteadyState)
      annotation (Placement(transformation(extent={{-12,-98},{2,-112}})));
    Modelica.Blocks.Sources.Constant const11(k=0)
      annotation (Placement(transformation(extent={{-36,-116},{-24,-104}})));
    Modelica.Blocks.Math.Gain gain1(k=1500)
      annotation (Placement(transformation(extent={{76,56},{88,68}})));
    Modelica.Blocks.Sources.Constant Kv_SGin1(k=1)   annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={144,-80})));
    Modelica.Blocks.Sources.Constant Kv_LPFW(k=1) annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={144,-32})));
    Modelica.Blocks.Sources.Constant Kv_LPFW1(k=1) annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={142,14})));
  equation
    connect(const.y, add.u1) annotation (Line(points={{26.7,127},{30,127},{30,111.6},
            {42.8,111.6}},      color={0,0,127}));
    connect(const.y, add1.u1) annotation (Line(points={{26.7,127},{30,127},{30,65.6},
            {42.8,65.6}},       color={0,0,127}));
    connect(const.y, add2.u1) annotation (Line(points={{26.7,127},{30,127},{30,-2.4},
            {46.8,-2.4}},       color={0,0,127}));
    connect(const.y, add3.u1) annotation (Line(points={{26.7,127},{30,127},{30,
            -42.4},{42.8,-42.4}}, color={0,0,127}));
    connect(add4.y, division.u1) annotation (Line(points={{-47.4,110},{-33.2,110},
            {-33.2,110.4}},
                          color={0,0,127}));
    connect(const1.y, add4.u1) annotation (Line(points={{-79.3,127},{-70,127},{-70,
            113.6},{-61.2,113.6}},   color={0,0,127}));
    connect(division.u2, const1.y) annotation (Line(points={{-33.2,117.6},{-40,117.6},
            {-40,127},{-79.3,127}},        color={0,0,127}));
    connect(add5.y, division1.u1) annotation (Line(points={{-47.4,60},{-33.2,60},{
            -33.2,60.4}},  color={0,0,127}));
    connect(const2.y, add5.u1) annotation (Line(points={{-79.3,77},{-70,77},{-70,63.6},
            {-61.2,63.6}},       color={0,0,127}));
    connect(division1.u2, const2.y) annotation (Line(points={{-33.2,67.6},{-40,67.6},
            {-40,77},{-79.3,77}},       color={0,0,127}));
    connect(add6.y, division2.u1) annotation (Line(points={{-47.4,0},{-33.2,0},{-33.2,
            0.4}},          color={0,0,127}));
    connect(const3.y, add6.u1) annotation (Line(points={{-79.3,17},{-70,17},{-70,3.6},
            {-61.2,3.6}},          color={0,0,127}));
    connect(division2.u2, const3.y) annotation (Line(points={{-33.2,7.6},{-40,7.6},
            {-40,17},{-79.3,17}},     color={0,0,127}));
    connect(add7.y, division3.u1) annotation (Line(points={{-47.4,-48},{-48,
            -47.6},{-33.2,-47.6}},
                            color={0,0,127}));
    connect(const4.y, add7.u1) annotation (Line(points={{-79.3,-31},{-70,-31},{
            -70,-44.4},{-61.2,-44.4}},
                                   color={0,0,127}));
    connect(division3.u2, const4.y) annotation (Line(points={{-33.2,-40.4},{-40,
            -40.4},{-40,-31},{-79.3,-31}},
                                    color={0,0,127}));
    connect(sensorBus.T_LPT, add6.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-3.6},{-61.2,-3.6}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.T_SGout, add5.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,56},{-68,56},{-68,56.4},{
            -61.2,56.4}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.P_SG, add4.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,106.4},{-61.2,106.4}},
        color={255,219,88},
        thickness=0.5));
    connect(division.y, PID.u_m)
      annotation (Line(points={{-19.4,114},{1,114},{1,109.4}},color={0,0,127}));
    connect(const5.y, PID.u_s) annotation (Line(points={{-17.4,90},{-14,90},{-14,101},
            {-7.4,101}},    color={0,0,127}));
    connect(PID.y, add.u2) annotation (Line(points={{8.7,101},{16,101},{16,104.4},
            {42.8,104.4}},
                         color={0,0,127}));
    connect(const6.y, PID1.u_s) annotation (Line(points={{-19.4,40},{-16,40},{-16,
            51},{-9.4,51}}, color={0,0,127}));
    connect(const7.y, PID2.u_s) annotation (Line(points={{-17.4,-24},{-14,-24},{-14,
            -13},{-7.4,-13}},     color={0,0,127}));
    connect(const8.y, PID3.u_s) annotation (Line(points={{-19.4,-66},{-16,-66},
            {-16,-61},{-9.4,-61}},
                              color={0,0,127}));
    connect(division3.y, PID3.u_m) annotation (Line(points={{-19.4,-44},{-14,
            -44},{-14,-42},{-1,-42},{-1,-52.6}},
                                            color={0,0,127}));
    connect(PID3.y, add3.u2) annotation (Line(points={{6.7,-61},{20,-61},{20,
            -49.6},{42.8,-49.6}}, color={0,0,127}));
    connect(division2.y, PID2.u_m)
      annotation (Line(points={{-19.4,4},{1,4},{1,-4.6}},      color={0,0,127}));
    connect(division1.y, PID1.u_m)
      annotation (Line(points={{-19.4,64},{-1,64},{-1,59.4}}, color={0,0,127}));
    connect(PID1.y, add1.u2) annotation (Line(points={{6.7,51},{20,51},{20,58.4},{
            42.8,58.4}},  color={0,0,127}));
    connect(PID2.y, add2.u2) annotation (Line(points={{8.7,-13},{20,-13},{20,-9.6},
            {46.8,-9.6}},         color={0,0,127}));
    connect(actuatorBus.Kv_RH, add2.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-6},{60.6,-6}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_HP_TAV, add.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,108},{56.6,108}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LP_TAV, add3.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-46},{56.6,-46}},
        color={80,200,120},
        thickness=0.5));
    connect(sensorBus.P_LPTin, add7.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-51.6},{-61.2,-51.6}},
        color={255,219,88},
        thickness=0.5));
    connect(add9.y,division4. u1) annotation (Line(points={{-43.4,-92},{-38,-92},
            {-38,-91.6},{-37.2,-91.6}},
                            color={0,0,127}));
    connect(const10.y, add9.u1) annotation (Line(points={{-75.3,-75},{-66,-75},
            {-66,-88.4},{-57.2,-88.4}}, color={0,0,127}));
    connect(division4.u2, const10.y) annotation (Line(points={{-37.2,-84.4},{
            -42,-84.4},{-42,-75},{-75.3,-75}}, color={0,0,127}));
    connect(const11.y, PID4.u_s) annotation (Line(points={{-23.4,-110},{-20,
            -110},{-20,-105},{-13.4,-105}}, color={0,0,127}));
    connect(division4.y,PID4. u_m) annotation (Line(points={{-23.4,-88},{-18,
            -88},{-18,-86},{-5,-86},{-5,-96.6}},
                                            color={0,0,127}));
    connect(PID4.y,add8. u1) annotation (Line(points={{2.7,-105},{2.7,-106},{36,
            -106},{36,-93.6},{42.8,-93.6}},color={0,0,127}));
    connect(add8.y,gain. u)
      annotation (Line(points={{56.6,-90},{74.8,-90}}, color={0,0,127}));
    connect(sensorBus.P_FWtank, add9.u2) annotation (Line(
        points={{60,-140},{60,-124},{-126,-124},{-126,-95.6},{-57.2,-95.6}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.N_LPpump, gain.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-90},{88.6,-90}},
        color={80,200,120},
        thickness=0.5));
    connect(add8.u2, const.y) annotation (Line(points={{42.8,-86.4},{30,-86.4},
            {30,127},{26.7,127}}, color={0,0,127}));
    connect(actuatorBus.Kv_SGin, Kv_SGin.y) annotation (Line(
        points={{-60,-140},{-60,-122},{137.4,-122}},
        color={80,200,120},
        thickness=0.5));
    connect(add1.y, gain1.u)
      annotation (Line(points={{56.6,62},{74.8,62}}, color={0,0,127}));
    connect(actuatorBus.N_HPpump, gain1.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,62},{88.6,62}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LP2_TAV, Kv_SGin1.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-80},{137.4,-80}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_HPFW, Kv_LPFW.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,-32},{137.4,-32}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LPFW, Kv_LPFW1.y) annotation (Line(
        points={{-60,-140},{-60,-122},{100,-122},{100,14},{135.4,14}},
        color={80,200,120},
        thickness=0.5));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,-140},
              {160,140}}), graphics={
          Rectangle(
            extent={{-160,140},{160,-140}},
            lineColor={102,44,145},
            fillColor={102,44,145},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-154,134},{154,-134}},
            lineColor={102,44,145},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-102,86},{114,-78}},
            textColor={102,44,145},
            textString="BOP
control")}),                      Diagram(coordinateSystem(preserveAspectRatio=
              false, extent={{-160,-140},{160,140}})));
  end BOPcontroller_ex2;

  model BOPcontroller_ex3

    TANDEM.SMR.BOP.BOP_POLIMI.Control.ActuatorBus actuatorBus
      annotation (Placement(transformation(extent={{-70,-170},{-50,-150}})));
    TANDEM.SMR.BOP.BOP_POLIMI.Control.SensorBus sensorBus
      annotation (Placement(transformation(extent={{50,-170},{70,-150}})));
    Modelica.Blocks.Math.Add add
      annotation (Placement(transformation(extent={{44,126},{56,138}})));
    Modelica.Blocks.Sources.Constant const(k=1)
      annotation (Placement(transformation(extent={{12,144},{26,158}})));
    Modelica.Blocks.Math.Add add1
      annotation (Placement(transformation(extent={{44,80},{56,92}})));
    Modelica.Blocks.Math.Add add2
      annotation (Placement(transformation(extent={{46,28},{58,40}})));
    Modelica.Blocks.Math.Add add3
      annotation (Placement(transformation(extent={{42,-12},{54,0}})));
    Modelica.Blocks.Math.Add add4(k1=-1)
      annotation (Placement(transformation(extent={{-60,128},{-48,140}})));
    Modelica.Blocks.Sources.Constant const1(k=45e5)
      annotation (Placement(transformation(extent={{-94,144},{-80,158}})));
    Modelica.Blocks.Math.Division division
      annotation (Placement(transformation(extent={{-32,144},{-20,132}})));
    Modelica.Blocks.Math.Add add5(k1=-1)
      annotation (Placement(transformation(extent={{-60,78},{-48,90}})));
    Modelica.Blocks.Sources.Constant const2(k=300 + 273.15)
      annotation (Placement(transformation(extent={{-94,94},{-80,108}})));
    Modelica.Blocks.Math.Division division1
      annotation (Placement(transformation(extent={{-32,94},{-20,82}})));
    Modelica.Blocks.Math.Add add6(k1=-1)
      annotation (Placement(transformation(extent={{-62,34},{-50,46}})));
    Modelica.Blocks.Sources.Constant const3(k=260.44 + 273.15)
      annotation (Placement(transformation(extent={{-96,50},{-82,64}})));
    Modelica.Blocks.Math.Division division2
      annotation (Placement(transformation(extent={{-34,50},{-22,38}})));
    Modelica.Blocks.Math.Add add7(k1=-1)
      annotation (Placement(transformation(extent={{-62,-14},{-50,-2}})));
    Modelica.Blocks.Sources.Constant const4(k=7.547e5)
      annotation (Placement(transformation(extent={{-96,2},{-82,16}})));
    Modelica.Blocks.Continuous.LimPID PID(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*38.9612,
      Ti=38.9612/67.6832,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-6,132},{8,118}})));
    Modelica.Blocks.Math.Division division3
      annotation (Placement(transformation(extent={{-34,2},{-22,-10}})));
    Modelica.Blocks.Sources.Constant const5(k=0)
      annotation (Placement(transformation(extent={{-30,108},{-18,120}})));
    Modelica.Blocks.Continuous.LimPID PID1(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*0.7404,
      Ti=0.7404/0.6557,
      yMax=0.5,
      initType=Modelica.Blocks.Types.Init.SteadyState)
      annotation (Placement(transformation(extent={{-8,82},{6,68}})));
    Modelica.Blocks.Sources.Constant const6(k=0)
      annotation (Placement(transformation(extent={{-36,60},{-24,72}})));
    Modelica.Blocks.Continuous.LimPID PID2(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=12.3513,
      Ti=12.3513/0.078988,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-8,34},{6,20}})));
    Modelica.Blocks.Sources.Constant const7(k=0)
      annotation (Placement(transformation(extent={{-32,10},{-20,22}})));
    Modelica.Blocks.Continuous.LimPID PID3(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*6.439,
      Ti=6.439/88.2,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-10,-14},{4,-28}})));
    Modelica.Blocks.Sources.Constant const8(k=0)
      annotation (Placement(transformation(extent={{-34,-32},{-22,-20}})));
    Modelica.Blocks.Sources.Constant Kv_SGin(k=1)    annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={168,-144})));
    Modelica.Blocks.Math.Add add8
      annotation (Placement(transformation(extent={{44,-100},{56,-112}})));
    Modelica.Blocks.Math.Gain gain(k=1500)
      annotation (Placement(transformation(extent={{76,-112},{88,-100}})));
    Modelica.Blocks.Math.Add add9(k1=-1)
      annotation (Placement(transformation(extent={{-56,-114},{-44,-102}})));
    Modelica.Blocks.Sources.Constant const10(k=7.147e5)
      annotation (Placement(transformation(extent={{-96,-98},{-82,-84}})));
    Modelica.Blocks.Math.Division division4
      annotation (Placement(transformation(extent={{-36,-98},{-24,-110}})));
    Modelica.Blocks.Continuous.LimPID PID4(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=0.66946,
      Ti=0.66946/6.1843,
      yMax=0.5,
      initType=Modelica.Blocks.Types.Init.SteadyState)
      annotation (Placement(transformation(extent={{-12,-114},{2,-128}})));
    Modelica.Blocks.Sources.Constant const11(k=0)
      annotation (Placement(transformation(extent={{-36,-132},{-24,-120}})));
    Modelica.Blocks.Math.Gain gain1(k=1500)
      annotation (Placement(transformation(extent={{76,80},{88,92}})));
    Modelica.Blocks.Math.Add add10
      annotation (Placement(transformation(extent={{44,-64},{56,-52}})));
    Modelica.Blocks.Math.Add add11(k1=-1)
      annotation (Placement(transformation(extent={{-60,-66},{-48,-54}})));
    Modelica.Blocks.Sources.Constant const9(k=0.801e5)
      annotation (Placement(transformation(extent={{-94,-50},{-80,-36}})));
    Modelica.Blocks.Math.Division division5
      annotation (Placement(transformation(extent={{-32,-50},{-20,-62}})));
    Modelica.Blocks.Continuous.LimPID PID5(
      controllerType=Modelica.Blocks.Types.SimpleController.PI,
      k=-1*6.439,
      Ti=6.439/88.2,
      yMax=0,
      yMin=-0.98,
      initType=Modelica.Blocks.Types.Init.SteadyState,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.UpperLimit)
      annotation (Placement(transformation(extent={{-8,-66},{6,-80}})));
    Modelica.Blocks.Sources.Constant const12(k=0)
      annotation (Placement(transformation(extent={{-32,-84},{-20,-72}})));
    Modelica.Blocks.Sources.Constant Kv_SGin1(k=1)   annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={168,-106})));
    Modelica.Blocks.Sources.Constant Kv_LPFW(k=1) annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={168,-58})));
  equation
    connect(const.y, add.u1) annotation (Line(points={{26.7,151},{28,151},{28,136},
            {34,136},{34,135.6},{42.8,135.6}},
                                color={0,0,127}));
    connect(const.y, add1.u1) annotation (Line(points={{26.7,151},{28,151},{28,90},
            {36,90},{36,89.6},{42.8,89.6}},
                                color={0,0,127}));
    connect(const.y, add2.u1) annotation (Line(points={{26.7,151},{28,151},{28,
            37.6},{44.8,37.6}}, color={0,0,127}));
    connect(const.y, add3.u1) annotation (Line(points={{26.7,151},{28,151},{28,
            -2.4},{40.8,-2.4}},   color={0,0,127}));
    connect(add4.y, division.u1) annotation (Line(points={{-47.4,134},{-33.2,134},
            {-33.2,134.4}},
                          color={0,0,127}));
    connect(const1.y, add4.u1) annotation (Line(points={{-79.3,151},{-70,151},{
            -70,137.6},{-61.2,137.6}},
                                     color={0,0,127}));
    connect(division.u2, const1.y) annotation (Line(points={{-33.2,141.6},{-40,
            141.6},{-40,151},{-79.3,151}}, color={0,0,127}));
    connect(add5.y, division1.u1) annotation (Line(points={{-47.4,84},{-33.2,84},
            {-33.2,84.4}}, color={0,0,127}));
    connect(const2.y, add5.u1) annotation (Line(points={{-79.3,101},{-70,101},{
            -70,87.6},{-61.2,87.6}},
                                 color={0,0,127}));
    connect(division1.u2, const2.y) annotation (Line(points={{-33.2,91.6},{-40,
            91.6},{-40,101},{-79.3,101}},
                                        color={0,0,127}));
    connect(add6.y, division2.u1) annotation (Line(points={{-49.4,40},{-35.2,40},
            {-35.2,40.4}},  color={0,0,127}));
    connect(const3.y, add6.u1) annotation (Line(points={{-81.3,57},{-72,57},{-72,
            43.6},{-63.2,43.6}},   color={0,0,127}));
    connect(division2.u2, const3.y) annotation (Line(points={{-35.2,47.6},{-42,
            47.6},{-42,57},{-81.3,57}},
                                      color={0,0,127}));
    connect(add7.y, division3.u1) annotation (Line(points={{-49.4,-8},{-50,-7.6},
            {-35.2,-7.6}},  color={0,0,127}));
    connect(const4.y, add7.u1) annotation (Line(points={{-81.3,9},{-72,9},{-72,
            -4.4},{-63.2,-4.4}},   color={0,0,127}));
    connect(division3.u2, const4.y) annotation (Line(points={{-35.2,-0.4},{-42,
            -0.4},{-42,9},{-81.3,9}},
                                    color={0,0,127}));
    connect(sensorBus.T_SGout, add5.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,80.4},{-61.2,80.4}},
        color={255,219,88},
        thickness=0.5));
    connect(sensorBus.P_SG, add4.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,130.4},{-61.2,130.4}},
        color={255,219,88},
        thickness=0.5));
    connect(division.y, PID.u_m)
      annotation (Line(points={{-19.4,138},{1,138},{1,133.4}},color={0,0,127}));
    connect(const5.y, PID.u_s) annotation (Line(points={{-17.4,114},{-14,114},{
            -14,125},{-7.4,125}},
                            color={0,0,127}));
    connect(PID.y, add.u2) annotation (Line(points={{8.7,125},{16,125},{16,128.4},
            {42.8,128.4}},
                         color={0,0,127}));
    connect(const6.y, PID1.u_s) annotation (Line(points={{-23.4,66},{-16,66},{-16,
            75},{-9.4,75}}, color={0,0,127}));
    connect(const7.y, PID2.u_s) annotation (Line(points={{-19.4,16},{-16,16},{-16,
            27},{-9.4,27}},       color={0,0,127}));
    connect(const8.y, PID3.u_s) annotation (Line(points={{-21.4,-26},{-11.4,-26},
            {-11.4,-21}},     color={0,0,127}));
    connect(division3.y, PID3.u_m) annotation (Line(points={{-21.4,-4},{-16,-4},{
            -16,-2},{-3,-2},{-3,-12.6}},    color={0,0,127}));
    connect(PID3.y, add3.u2) annotation (Line(points={{4.7,-21},{18,-21},{18,-9.6},
            {40.8,-9.6}},         color={0,0,127}));
    connect(division2.y, PID2.u_m)
      annotation (Line(points={{-21.4,44},{-1,44},{-1,35.4}},  color={0,0,127}));
    connect(division1.y, PID1.u_m)
      annotation (Line(points={{-19.4,88},{-1,88},{-1,83.4}}, color={0,0,127}));
    connect(PID1.y, add1.u2) annotation (Line(points={{6.7,75},{20,75},{20,82.4},
            {42.8,82.4}}, color={0,0,127}));
    connect(PID2.y, add2.u2) annotation (Line(points={{6.7,27},{18,27},{18,30.4},
            {44.8,30.4}},         color={0,0,127}));
    connect(actuatorBus.Kv_RH, add2.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,34},{58.6,34}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_HP_TAV, add.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,132},{56.6,132}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LP_TAV, add3.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,-6},{54.6,-6}},
        color={80,200,120},
        thickness=0.5));
    connect(sensorBus.P_LPTin, add7.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,-11.6},{-63.2,-11.6}},
        color={255,219,88},
        thickness=0.5));
    connect(add9.y,division4. u1) annotation (Line(points={{-43.4,-108},{-38,-108},
            {-38,-107.6},{-37.2,-107.6}},
                            color={0,0,127}));
    connect(const10.y, add9.u1) annotation (Line(points={{-81.3,-91},{-81.3,-92},
            {-74,-92},{-74,-104.4},{-57.2,-104.4}},
                                        color={0,0,127}));
    connect(division4.u2, const10.y) annotation (Line(points={{-37.2,-100.4},{
            -37.2,-91},{-81.3,-91}},           color={0,0,127}));
    connect(const11.y, PID4.u_s) annotation (Line(points={{-23.4,-126},{-20,-126},
            {-20,-121},{-13.4,-121}},       color={0,0,127}));
    connect(division4.y,PID4. u_m) annotation (Line(points={{-23.4,-104},{-18,
            -104},{-18,-102},{-5,-102},{-5,-112.6}},
                                            color={0,0,127}));
    connect(PID4.y,add8. u1) annotation (Line(points={{2.7,-121},{2.7,-122},{36,
            -122},{36,-109.6},{42.8,-109.6}},
                                           color={0,0,127}));
    connect(add8.y,gain. u)
      annotation (Line(points={{56.6,-106},{74.8,-106}},
                                                       color={0,0,127}));
    connect(sensorBus.P_FWtank, add9.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,-112},{-62,-112},{-62,
            -111.6},{-57.2,-111.6}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.N_LPpump, gain.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,-106},{88.6,-106}},
        color={80,200,120},
        thickness=0.5));
    connect(add8.u2, const.y) annotation (Line(points={{42.8,-102.4},{28,-102.4},
            {28,151},{26.7,151}}, color={0,0,127}));
    connect(actuatorBus.Kv_SGin, Kv_SGin.y) annotation (Line(
        points={{-60,-160},{-60,-144},{161.4,-144}},
        color={80,200,120},
        thickness=0.5));
    connect(add1.y, gain1.u)
      annotation (Line(points={{56.6,86},{74.8,86}}, color={0,0,127}));
    connect(actuatorBus.N_HPpump, gain1.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,86},{88.6,86}},
        color={80,200,120},
        thickness=0.5));
    connect(const.y, add10.u1) annotation (Line(points={{26.7,151},{28,151},{28,
            -54.4},{42.8,-54.4}},
                            color={0,0,127}));
    connect(add11.y, division5.u1) annotation (Line(points={{-47.4,-60},{-48,
            -59.6},{-33.2,-59.6}},  color={0,0,127}));
    connect(const9.y, add11.u1) annotation (Line(points={{-79.3,-43},{-70,-43},{
            -70,-56.4},{-61.2,-56.4}},    color={0,0,127}));
    connect(division5.u2,const9. y) annotation (Line(points={{-33.2,-52.4},{-40,
            -52.4},{-40,-43},{-79.3,-43}},
                                    color={0,0,127}));
    connect(const12.y, PID5.u_s) annotation (Line(points={{-19.4,-78},{-16,-78},{
            -16,-73},{-9.4,-73}},          color={0,0,127}));
    connect(division5.y,PID5. u_m) annotation (Line(points={{-19.4,-56},{-14,-56},
            {-14,-54},{-1,-54},{-1,-64.6}}, color={0,0,127}));
    connect(PID5.y, add10.u2) annotation (Line(points={{6.7,-73},{20,-73},{20,
            -61.6},{42.8,-61.6}},         color={0,0,127}));
    connect(add8.u2, const.y) annotation (Line(points={{42.8,-102.4},{28,-102.4},
            {28,151},{26.7,151}}, color={0,0,127}));
    connect(sensorBus.P_LPT2in, add11.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,-64},{-66,-64},{-66,-63.6},
            {-61.2,-63.6}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.Kv_LP2_TAV, add10.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,-58},{56.6,-58}},
        color={80,200,120},
        thickness=0.5));
    connect(sensorBus.T_LPT, add6.u2) annotation (Line(
        points={{60,-160},{60,-140},{-160,-140},{-160,36},{-66,36},{-66,36.4},{
            -63.2,36.4}},
        color={255,219,88},
        thickness=0.5));
    connect(actuatorBus.Kv_HPFW, Kv_SGin1.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,-106},{161.4,-106}},
        color={80,200,120},
        thickness=0.5));
    connect(actuatorBus.Kv_LPFW, Kv_LPFW.y) annotation (Line(
        points={{-60,-160},{-60,-144},{140,-144},{140,-58},{161.4,-58}},
        color={80,200,120},
        thickness=0.5));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-180,
              -160},{180,160}}), graphics={
          Rectangle(
            extent={{-180,160},{180,-160}},
            lineColor={102,44,145},
            fillColor={102,44,145},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-174,154},{174,-154}},
            lineColor={102,44,145},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-130,108},{128,-90}},
            textColor={102,44,145},
            textString="BOP
control")}),                      Diagram(coordinateSystem(preserveAspectRatio=
              false, extent={{-180,-160},{180,160}})));
  end BOPcontroller_ex3;

  expandable connector SensorBus

    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Line(
              points={{-14,0},{18,0}},
              color={255,219,88},
              thickness=0.5),
            Rectangle(
              lineColor={255,219,88},
              lineThickness=0.5,
              extent={{-8,-2},{10,6}}),
            Polygon(
            fillColor={255,219,88},
            fillPattern=FillPattern.Solid,
            points={{-78,48},{82,48},{102,28},{82,-42},{62,-52},{-58,-52},{-78,-42},
                {-98,28}},
            smooth=Smooth.Bezier),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{-53,13},{-43,23}}),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{47,13},{57,23}}),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{-3,-27},{7,-17}}),
            Rectangle(
            lineColor={255,219,88},
            lineThickness=0.5,
            extent={{-18,-2},{22,2}})}),                           Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end SensorBus;

  expandable connector ActuatorBus

    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Line(
              points={{-16,2},{16,2}},
              color={80,200,120},
              thickness=0.5),
            Rectangle(
              lineColor={80,200,120},
              lineThickness=0.5,
              extent={{-10,0},{8,8}}),
            Polygon(
            fillColor={80,200,120},
            fillPattern=FillPattern.Solid,
            points={{-80,50},{80,50},{100,30},{80,-40},{60,-50},{-60,-50},{-80,
                -40},{-100,30}},
            smooth=Smooth.Bezier),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{-55,15},{-45,25}}),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{45,15},{55,25}}),
            Ellipse(
              fillPattern=FillPattern.Solid,
              extent={{-5,-25},{5,-15}}),
            Rectangle(
              lineColor={80,200,120},
              lineThickness=0.5,
              extent={{-20,0},{20,4}})}),                          Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end ActuatorBus;
end Control;
