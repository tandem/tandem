
# Balance of Plant (BOP)
## Component Description
The balance of plant (BOP) component models a nuclear steam cycle converting the heat provided by the reactor coolant system (RCS) into electrical power and steam that can be supplied to various assets (HTSE, thermal storage, water desalination, etc.).

The steam cycle architecture considered includes a HP and a LP steam turbines (ST) with an intermediate extraction in the LP ST casing. Three pressure level (HP, MP and LP) are then available. A moisture separator reheater (MSR) is installed at the outlet of the HP turbine (MP). Two reheaters (MP and LP) offers a small performance gain over the basic Rankine cycle.

## Circuit Description 
The BOP is supplied in heat from the RCS model, produces an overheated steam flow in the steam generator (SG), converts it in mechanical power in the steam turbines (ST) and converts it in electrical power in the generator. Main equipment models of the BOP are based on ThermoPower 3.2 library :
- A steam generator (SG) : Drum simplified component ;
- A HP and two LP ST : Stodola steam turbines ;
- A moisture separator : built-in moisture separation component ;
- A condenser : Prescribed pressure condenser ;
- Two reheaters : built-in reheater drum component ;
- Valves, pressure drop piping and single-tube heat exchangers (HX).

## Interfaces 
The BOP is interfaced with the RCS, the electrical grid but also to several assets callable by the components-user (end-users). 

The inputs interfaces of the BOP are :
- Heat flux from the RCS (real inputs) :
	1. **SG_evaporator** : Heat provided for steam production ;
	2. **SG_superheating** : Heat provided for steam superheating ;
- Heat fluxes provided from end-users (real inputs) :
	1. **Int_HP**: End-user demand for heat extraction from HP steam at 240 °C (subcooled condensate temperature) ;
	2. **Int_MP**: End-user demand for heat extraction from MP steam at 150 °C (subcooled condensate temperature) ;
	3. **Int_LP**: End-user demand for heat extraction from LP steam at 80 °C (subcooled condensate temperature).

The outputs interfaces of the BOP are :
- Electrical power to the grid (real output) : 
	1. **Power** : Power produced at the electrical generator ;
- Heat fluxes really available for end-users :
	1. **heat_HP** : Model heat extraction from HP steam at 240 °C (subcooled condensate temperature) ;
	2. **heat_MP** : Model heat extraction from MP steam at 150 °C (subcooled condensate temperature) ;
	3. **heat_LP** : Model heat extraction from LP steam at 80 °C (subcooled condensate temperature).
 
## Main Modeling Assumptions
The BOP model nominal operating point is based on the CEA Cyclop optimization of the cycle. Working parameters of BOP components are retrofitted on CEA's results. The moisture separator of the MSR is modelled but its heat exchanger is only virtually implemented (imposed cooling heat fluxes on both cold and hot streams). Performance prediction of this component is then only realistic for near-to-nominal operation. Thermal inertia of main components (condenser and reheaters) are modelled based on a state-of-the-art sizing.

### Heat extraction
The 3 heat extraction interfaces follow the same modelling principle: 
- A perfect heat transfer is considered through a one-tube HX which includes a hydraulic capacity but no hydraulic resistance.
- A flowrate control of steam extracted from the BOP through a valve which is operated by a PID controller to keep a subcooling degree constant at the outlet of the HX.

The PID controller has been fine tuned to reach the targeted subcooling temperature with a latency of 10-15 min. These targeted subcooling temperature are:
- 240°C for the HP steam interface ;
- 150°C for the MP steam interface ;
- 80°C for the LP steam interface.

These temperatures correspond to a subcooling of 15°C relative to the saturated steam temperature under nominal operating conditions. Since the pressure gap between the steam extraction and the condensate reinjection from and to the BOP can be high, a condensate flash can happen in HP and LP control valves. Choked valves models are thus considered for these 2 interfaces. 
Remark: Due to the hydraulic capacitance of the modeled one-tube HX, singularities can be observed during sharp transients (and the mathematical transient at the beginning of the simulation), it is why a first order derivative with a time constant of 4 s is added to ***smooth*** the heat inputs.

### Control
The BOP control takes into account a variable heat load on the SG. A feedwater valve with control logic based on inlet/outlet flowrate as well as drum level is implemented. This control logic prevents instabilities from the swelling/shrinking phenomenon in the drum. However, no HP ST inlet control valve is implemented. Finally, the feedwater pumps are modelled with a constant rotational speed and a linear characteristic curve.

### Remarks and limitations
- The MSR heat exchanger modelling does not allow to give a realistic prediction of steam reheating performance far from the nominal operating point.
- The fine tuning of the PID controlers implemented to keep a constant subcooling/overheating temperature at the outlet of each heat interface of the BOP has been performed in order to reach the targeted temperature after +/- 10 to 15 min. **Within this time latency of 10-15 min, the ideal heat exchange can be quite unrealistic** since targeted temperature profiles in the HX have not reached a steady state (risk fof cross temperature).
- **Interface heat fluxes limiters are set to 20 MW**
- The BOP **pump maps are linear instead of quadratic** for real turbomachinery. Indeed, the model initialization is using a Newton solver combined with homotopy (mathematical tool used make the initialization problem continuous). This OpenModelica solver is submitted to mathematical instabilities with real quadratic pump curve. Therefore the **pump behavior prediction of the model is only realistic for small flowrate variations** (small heat demand < 10-15 MW from high and low temperature end-users).
- The **pressure gap between the inlet and the outlet of the MP steam extraction control valve is narrow. Under certain limit operating conditions**, the pressure gradient across this valve **could reverse**. The model check valve then makes null the flowrate across the valve but the hydraulic capacity of the HX discharge in upstream. This behavior leads to a crash of the model which remains unavoidable until now.
- Mathematical transient gives unphysical values: a **too small timestep (< 1-2 sec) during this mathematical transient can lead to fail at the beginning of the simulation ( < 20 - 50 sec for OpenModelica solver)**.

## Prerequisites
The model uses the [Buildings 7.0.0 Modelica library](https://simulationresearch.lbl.gov/modelica/), the [Thermopower 3.2 Modelica library](https://github.com/casella/ThermoPower) and some built-in components in the Water sub-class, which must be opened in order to compile the code. 

## Provided examples
An example of the model usage is provided in the Test subpackage.
 