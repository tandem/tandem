within TANDEM.SMR.BOP.BOP_TBL;
package BOP_TBL

  package Tests
    model Test_BOP
      BOP_TBL.Models.BOP_modelInterfaces BOPV_model annotation (
        Placement(visible = true, transformation(origin = {2.22045e-15, 3}, extent = {{-24, -23}, {24, 23}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp_HP(duration = 100, height = 0, offset = -10e6, startTime = 500) annotation (
        Placement(visible = true, transformation(origin = {-61, 51}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp_MP(duration = 250, height = 0, offset = -5e6, startTime = 500) annotation (
        Placement(visible = true, transformation(origin = {-61, 25}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp_LP(duration = 100, height = 0, offset = -1e6, startTime = 500) annotation (
        Placement(visible = true, transformation(origin = {-63, 1}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp superheating(duration = 100, height = 0, offset = 35e6, startTime = 500) annotation (
        Placement(visible = true, transformation(origin = {-61, -29}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp evaporation(duration = 100, height = 0, offset = 505e6, startTime = 500) annotation (
        Placement(visible = true, transformation(origin = {-63, -57}, extent = {{-7, -7}, {7, 7}}, rotation = 0)));
      Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R(displayUnit="kOhm") = 5) annotation (
        Placement(visible = true, transformation(origin = {78, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Buildings.Electrical.AC.OnePhase.Sources.Generator gen(f=50) annotation (
        Placement(visible = true, transformation(origin = {48, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(superheating.y, BOPV_model.SG_superheating) annotation (
        Line(points = {{-54, -28}, {-42, -28}, {-42, -8}, {-24, -8}}, color = {0, 0, 127}));
      connect(evaporation.y, BOPV_model.SG_evaporator) annotation (
        Line(points = {{-56, -56}, {-34, -56}, {-34, -14}, {-24, -14}}, color = {0, 0, 127}));
      connect(ramp_LP.y, BOPV_model.int_LP) annotation (
        Line(points = {{-55, 1}, {-24, 1}, {-24, 2}}, color = {0, 0, 127}));
      connect(ramp_MP.y, BOPV_model.int_MP) annotation (
        Line(points = {{-54, 26}, {-40, 26}, {-40, 10}, {-24, 10}}, color = {0, 0, 127}));
      connect(ramp_HP.y, BOPV_model.intHP) annotation (
        Line(points = {{-54, 52}, {-34, 52}, {-34, 20}, {-24, 20}}, color = {0, 0, 127}));
      connect(
         BOPV_model.Power, gen.P) annotation (
        Line(points = {{24, -12}, {38, -12}}, color = {0, 0, 127}));
      connect(
         gen.terminal, imp.terminal) annotation (
        Line(points = {{58, -12}, {68, -12}}));
    end Test_BOP;
  end Tests;

  package Models

    model BOP_no_interfaces
      replaceable package FlueGas = ThermoPower.Media.FlueGas constrainedby Modelica.Media.Interfaces.PartialMedium "Flue gas model";
      replaceable package Water = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialPureSubstance "Fluid model";
      Modelica.Units.SI.Power Pm_int "total intermediate pressure turbine power";
      Modelica.Units.SI.Power Pm_low "total low pressure turbine power";
      ThermoPower.Water.SteamTurbineStodola HPT1(Kt = 0.024345, redeclare package Medium = Water, PRstart = 5.830131, eta_iso_nom = 0.9, eta_mech = 0.92, phi(displayUnit = "rad"), pnom = 44e5, wnom = 218.36, wstart = 218.36) annotation (
        Placement(transformation(origin = {-196, 50}, extent = {{-29, -29}, {29, 29}})));
      inner ThermoPower.System system(allowFlowReversal = false, initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{812, 844}, {832, 864}}, rotation = 0)));
      Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(phi(fixed = true, start = 0), w_fixed = 157) annotation (
        Placement(visible = true, transformation(origin = {-110, 0}, extent = {{930, 40}, {910, 60}}, rotation = 0)));
      Modelica.Mechanics.Rotational.Sensors.PowerSensor powerSensor1 annotation (
        Placement(visible = true, transformation(origin = {-110, 0}, extent = {{868, 64}, {896, 36}}, rotation = 0)));
      ThermoPower.Water.FlowSplit flowSplit1 annotation (
        Placement(visible = true, transformation(origin = {-126, 102}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      ThermoPower.Water.SteamTurbineStodola LPT1(Kt = 0.133478, redeclare package Medium = Water, PRstart = 8.43375, eta_iso_nom = 0.89, eta_mech = 0.96, phi(displayUnit = "rad"), pnom = 6.747e5, wnom = 182.2, wstart = 182.2) annotation (
        Placement(visible = true, transformation(origin = {114, 50}, extent = {{-29, -29}, {29, 29}}, rotation = 0)));
      ThermoPower.Water.SteamTurbineStodola LPT2(Kt = 0.839, redeclare package Medium = Water, PRstart = 11.44, eta_iso_nom = 0.89, eta_mech = 0.96, phi(displayUnit = "rad"), pnom = 0.8e5, wnom = 165.8, wstart = 165.8) annotation (
        Placement(visible = true, transformation(origin = {408, 50}, extent = {{-29, -29}, {29, 29}}, rotation = 0)));
      ThermoPower.Water.FlowSplit flowSplit3 annotation (
        Placement(visible = true, transformation(origin = {282, 94}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      BOP_TBL.Water.PrescribedPressureCondenser condenser(redeclare package Medium = Water, initOpt = ThermoPower.Choices.Init.Options.fixedState, p(displayUnit = "Pa") = 7000, D_ext = 0.0254, L = 10.3, V_heatex = 210, N_tube = 7950, N_pass = 1) annotation (
        Placement(transformation(origin = {702, -90}, extent = {{-40, -40}, {40, 40}})));
      ThermoPower.Examples.RankineCycle.Models.PrescribedSpeedPump LP_pump(redeclare package FluidMedium = Water, head_nom = {90, 65, 20}, hstart = 1.5e5, n0 = 1500, nominalInletPressure(displayUnit = "Pa") = 7000, nominalMassFlowRate = 182.2, nominalOutletPressure(displayUnit = "bar") = 770000, q_nom = {0.027, 0.179, 0.257}, rho0(displayUnit = "kg/m3") = 1000) annotation (
        Placement(visible = true, transformation(origin = {702, -252}, extent = {{20, -20}, {-20, 20}}, rotation = 90)));
      BOP_TBL.Water.ReheaterV1 LP_reheater(T_start_prim = 312.19, dp_nom_prim(displayUnit = "Pa") = 40000, m_flow_nom_prim = 182.2, p_start_prim(displayUnit = "Pa") = 754699.9999999998, p_start_sec(displayUnit = "Pa") = 80100, Mass_me = 12897, V_prim = 14.9*497*2*3.14*(0.01382^2)/4, V_sec = ((1.26^2)/4)*14.9*3.14 - 14.9*497*2*3.14*(0.01588^2)/4) annotation (
        Placement(visible = true, transformation(origin = {366, -320}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      BOP_TBL.Water.ReheaterV1 HP_reheater(T_start_prim = 389.76, dp_nom_prim(displayUnit = "Pa") = 40000, m_flow_nom_prim = 239.68, p_start_prim(displayUnit = "bar") = 4850000, p_start_sec(displayUnit = "Pa") = 714700, Mass_me = 11287, V_prim = 12.04*982*2*3.14*(0.01382^2)/4, V_sec = ((1.1^2)/4)*12.04*3.14 - 12.04*982*2*3.14*(0.01588^2)/4) annotation (
        Placement(transformation(origin = {-114, -324}, extent = {{-20, -20}, {20, 20}})));
      ThermoPower.Examples.RankineCycle.Models.PrescribedSpeedPump HP_feedwater_pump(redeclare package FluidMedium = Water, head_nom = {850, 448.52, 107}, n0 = 1500, nominalInletPressure(displayUnit = "Pa") = 714700, nominalMassFlowRate = 239.68, nominalOutletPressure(displayUnit = "bar") = 4850000, q_nom = {0.036, 0.236, 0.334}, rho0(displayUnit = "kg/m3") = 1000) annotation (
        Placement(visible = true, transformation(origin = {74, -322}, extent = {{-10, 10}, {10, -10}}, rotation = 180)));
      ThermoPower.Water.FlowJoin flowJoin3 annotation (
        Placement(visible = true, transformation(origin = {156, -324}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowSplit MSRflowsplit annotation (
        Placement(visible = true, transformation(origin = {-478, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropIT(R = 100000/218.36) annotation (
        Placement(transformation(origin = {-364, 94}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.PressDropLin pressDropRHHT(R = 2e4/24.51) annotation (
        Placement(visible = true, transformation(origin = {-114, -104}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      ThermoPower.Water.PressDropLin pressDropRHLP(R = (8.01e4 - 7e3)/16.41) annotation (
        Placement(visible = true, transformation(origin = {516, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.ThroughMassFlow throughMassFlow1(w0 = 16.41) annotation (
        Placement(visible = true, transformation(origin = {426, -472}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp3(duration = 250, height = 0, offset = 500e6, startTime = 100) annotation (
        Placement(transformation(origin = {-114, -2}, extent = {{-550, -280}, {-530, -260}})));
      ThermoPower.Water.SensW sensW_SG annotation (
        Placement(visible = true, transformation(origin = {-554, -132}, extent = {{-10, 10}, {10, -10}}, rotation = 90)));
      Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow annotation (
        Placement(visible = true, transformation(origin = {-568, -272}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      BOP_TBL.Water.Drum_simplified drum_simplified(L = 8, initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = true, pstart = 45e5, rext = 5, rint = 5.1) annotation (
        Placement(transformation(origin = {-508, -212}, extent = {{-30, -30}, {30, 30}})));
      Modelica.Blocks.Nonlinear.Limiter limiter(limitsAtInit = true, uMax = 0.05) annotation (
        Placement(visible = true, transformation(origin = {-448, -152}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Blocks.Math.Add add(k1 = -1) annotation (
        Placement(visible = true, transformation(origin = {-370, -72}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Math.Product product annotation (
        Placement(visible = true, transformation(origin = {-420, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.SensW sensW_feedwater annotation (
        Placement(visible = true, transformation(origin = {-248, -318}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Examples.RankineCycle.Models.PID pid(CSmax = 1.0, CSmin = 0, CSstart = 0.8, Kp = 0.5, PVmax = 500, PVmin = 0, Ti = 5) annotation (
        Placement(transformation(origin = {-344, -284}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
      ThermoPower.Water.ValveLiq FeedwateValve(CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom(displayUnit = "bar") = 499999.9999999999, pnom = 4850000, wnom = 300) annotation (
        Placement(visible = true, transformation(origin = {-344, -324}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Water.Flow1DFV flow1DFV(A = 5, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Steam, L = 10, initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = true, omega = 1.3, pstart = 45e5, wnom = 239.68) annotation (
        Placement(visible = true, transformation(origin = {-558, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Blocks.Sources.Ramp ramp(duration = 250, height = 0, offset = 40e6, startTime = 100) annotation (
        Placement(transformation(origin = {-716, -46}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.ThroughMassFlow throughMassFlow(w0 = 24.51) annotation (
        Placement(visible = true, transformation(origin = {106, -478}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin flowJoin annotation (
        Placement(visible = true, transformation(origin = {554, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin flowJoin1 annotation (
        Placement(visible = true, transformation(origin = {214, -472}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp1(duration = 100, height = 0, offset = 1500, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {62, -296}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water state_HPT_LPT annotation (
        Placement(visible = true, transformation(origin = {70, 102}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water state_HPT_in annotation (
        Placement(visible = true, transformation(origin = {-276, 94}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp_feedPump(duration = 100, height = 0, offset = 1500, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {636, -230}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropLin(R = 2e4/24.51) annotation (
        Placement(visible = true, transformation(origin = {10, -478}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.Control.PID MSR_pid(CSmax = 1, CSmin = 0, Kp = 4, PVmax = 600, PVmin = 273.15, Ti = 250) annotation (
        Placement(transformation(origin = {-184, 276}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Sources.Ramp ramp2(duration = 100, height = 0, offset = 260 + 273.15, startTime = 50) annotation (
        Placement(visible = true, transformation(origin = {-236, 310}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Thermal.HeatSource1DFV heatSource1DFV annotation (
        Placement(visible = true, transformation(origin = {-640, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      ThermoPower.Water.ValveLiqChoked MSR_contVal(CheckValve = true, CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom = 45e5 - 7.147e5, pnom = 45e5, rhonom = 800, thetanom = 0.5, wnom = 20) annotation (
        Placement(visible = true, transformation(origin = {-156, 230}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin MSR_pressDrop(R = 0.8e4/200) annotation (
        Placement(visible = true, transformation(origin = {30, 102}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water T_HP_reheater_prim annotation (
        Placement(visible = true, transformation(origin = {-178, -322}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water T_LP_reheater_prim annotation (
        Placement(visible = true, transformation(origin = {314, -322}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      BOP_TBL.Water.Steam_dryer steam_dryer annotation (
        Placement(transformation(origin = {-72, 106}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.FlowJoin flowJoin2 annotation (
        Placement(visible = true, transformation(origin = {252, -318}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropMSR(R = 2e4/24.51) annotation (
        Placement(transformation(origin = {-80, -104}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      BOP_TBL.Water.HE_WW he_ww(Nt = 12778, Np = 2, L = 5.733, di = 0.01422, do = 0.01905, pitch = 27.62e-3, Bfrac = 0.6, CL = 1, CTP = 0.9, km = 17, Qnom = 38.902e6, ms_nom = 21.32, mt_nom = 182.2, ps_nom = 4500000, pt_nom = 714700, hsi_nom = 2944130, hso_nom = 1119460, hti_nom = 2763620, hto_nom = 2977140, dpt_nom = 30100, Cf_t = 2.6980236, hshell = 909.22925) annotation (
        Placement(transformation(origin = {-356, 204}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.Flow1DFV MSR_HX_sink(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, L = 10, allowFlowReversal = false, hstartin = ThermoPower.Water.StandardWater.dewEnthalpy(ThermoPower.Water.StandardWater.setSat_p(7.547e5)), hstartout = ThermoPower.Water.StandardWater.dewEnthalpy(ThermoPower.Water.StandardWater.setSat_p(7.547e5)), initOpt = ThermoPower.Choices.Init.Options.fixedState, omega = 1, pstart = 7.547e5, wnom = 150) annotation (
        Placement(visible = true, transformation(origin = {-14, 102}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Thermal.HeatSource1DFV heatSource1DFV2 annotation (
        Placement(visible = true, transformation(origin = {-14, 130}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp7(duration = 500, height = 0, offset = 0, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {-14, 176}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      ThermoPower.Water.SensT sensT annotation (
        Placement(transformation(origin = {-276, 206}, extent = {{-10, -10}, {10, 10}})));
    equation
      Pm_int = HPT1.Pm;
      Pm_low = LPT1.Pm + LPT2.Pm;
      connect(constantSpeed.flange, powerSensor1.flange_b) annotation (
        Line(points = {{800, 50}, {786, 50}}));
      connect(flowSplit3.out2, LPT2.inlet) annotation (
        Line(points = {{288, 98}, {385, 98}, {385, 73}}, color = {0, 0, 255}));
      connect(LPT2.shaft_b, powerSensor1.flange_a) annotation (
        Line(points = {{426.56, 50}, {757.56, 50}}));
      connect(HPT1.outlet, flowSplit1.in1) annotation (
        Line(points = {{-173, 73}, {-173, 102.2}, {-131.8, 102.2}}, color = {0, 0, 255}));
      connect(MSRflowsplit.out2, pressDropIT.inlet) annotation (
        Line(points = {{-472, 94}, {-374, 94}}, color = {0, 0, 255}));
      connect(LPT1.shaft_b, LPT2.shaft_a) annotation (
        Line(points = {{132.56, 50}, {388.56, 50}}));
      connect(LP_reheater.condensate, throughMassFlow1.inlet) annotation (
        Line(points = {{366, -330}, {366, -472}, {416, -472}}, color = {0, 0, 255}));
      connect(ramp3.y, prescribedHeatFlow.Q_flow) annotation (
        Line(points = {{-643, -272}, {-579, -272}}, color = {0, 0, 127}));
      connect(drum_simplified.steam, sensW_SG.inlet) annotation (
        Line(points = {{-490.6, -191}, {-490.6, -161}, {-558.6, -161}, {-558.6, -139}}, color = {0, 0, 255}));
      connect(prescribedHeatFlow.port, drum_simplified.heater) annotation (
        Line(points = {{-558, -272}, {-508, -272}, {-508, -238}}, color = {191, 0, 0}));
      connect(drum_simplified.level_centered, limiter.u) annotation (
        Line(points = {{-475, -227}, {-449, -227}, {-449, -165}}, color = {0, 0, 127}));
      connect(product.y, add.u1) annotation (
        Line(points = {{-409, -88}, {-399, -88}, {-399, -78}, {-383, -78}}, color = {0, 0, 127}));
      connect(limiter.y, product.u2) annotation (
        Line(points = {{-448, -141}, {-448, -95}, {-432, -95}}, color = {0, 0, 127}));
      connect(sensW_SG.w, product.u1) annotation (
        Line(points = {{-548, -124}, {-548, -82}, {-432, -82}}, color = {0, 0, 127}));
      connect(sensW_SG.w, add.u2) annotation (
        Line(points = {{-548, -124}, {-548, -66}, {-382, -66}}, color = {0, 0, 127}));
      connect(sensW_feedwater.w, pid.PV) annotation (
        Line(points = {{-256, -312}, {-278, -312}, {-278, -274}, {-340, -274}}, color = {0, 0, 127}));
      connect(add.y, pid.SP) annotation (
        Line(points = {{-359, -72}, {-359, -68}, {-348, -68}, {-348, -274}}, color = {0, 0, 127}));
      connect(pid.CS, FeedwateValve.theta) annotation (
        Line(points = {{-344, -294}, {-344, -316}}, color = {0, 0, 127}));
      connect(HPT1.shaft_b, LPT1.shaft_a) annotation (
        Line(points = {{-177, 50}, {94.56, 50}}));
      connect(sensW_SG.outlet, flow1DFV.infl) annotation (
        Line(points = {{-558, -126}, {-558, -56}}, color = {0, 0, 255}));
      connect(flowSplit3.out1, LP_reheater.vapour_in) annotation (
        Line(points = {{288, 90}, {366, 90}, {366, -310}, {366, -310}}, color = {0, 0, 255}));
      connect(pressDropRHLP.outlet, flowJoin.in2) annotation (
        Line(points = {{526, -8}, {548, -8}, {548, 26}}, color = {0, 0, 255}));
      connect(pressDropRHLP.inlet, throughMassFlow1.outlet) annotation (
        Line(points = {{506, -8}, {506, -7.25}, {478, -7.25}, {478, -472}, {436, -472}}, color = {0, 0, 255}));
      connect(HP_reheater.primary_in, HP_feedwater_pump.outlet) annotation (
        Line(points = {{-104, -324}, {-20, -324}, {-20, -322}, {64, -322}}, color = {0, 0, 255}));
      connect(sensW_feedwater.outlet, FeedwateValve.inlet) annotation (
        Line(points = {{-254, -322}, {-334, -322}, {-334, -324}}, color = {0, 0, 255}));
      connect(FeedwateValve.outlet, drum_simplified.feedwater) annotation (
        Line(points = {{-354, -324}, {-538, -324}, {-538, -216}}, color = {0, 0, 255}));
      connect(state_HPT_LPT.outlet, LPT1.inlet) annotation (
        Line(points = {{76, 102}, {90, 102}, {90, 73}, {91, 73}}, color = {0, 0, 255}));
      connect(pressDropIT.outlet, state_HPT_in.inlet) annotation (
        Line(points = {{-354, 94}, {-282, 94}}, color = {0, 0, 255}));
      connect(state_HPT_in.outlet, HPT1.inlet) annotation (
        Line(points = {{-270, 94}, {-270, 92}, {-219, 92}, {-219, 73}}, color = {0, 0, 255}));
      connect(pressDropRHHT.outlet, HP_reheater.vapour_in) annotation (
        Line(points = {{-114, -114}, {-114, -314}}, color = {0, 0, 255}));
      connect(ramp_feedPump.y, LP_pump.nPump) annotation (
        Line(points = {{647, -230}, {689, -230}, {689, -238}}, color = {0, 0, 127}));
      connect(ramp1.y, HP_feedwater_pump.nPump) annotation (
        Line(points = {{73, -296}, {100, -296}, {100, -316}, {82, -316}}, color = {0, 0, 127}));
      connect(HP_reheater.condensate, pressDropLin.inlet) annotation (
        Line(points = {{-114, -334}, {-114, -478}, {0, -478}}, color = {0, 0, 255}));
      connect(ramp2.y, MSR_pid.SP) annotation (
        Line(points = {{-225, 310}, {-209, 310}, {-209, 280}, {-194, 280}}, color = {0, 0, 127}));
      connect(heatSource1DFV.wall, flow1DFV.wall) annotation (
        Line(points = {{-637, -46}, {-563, -46}}, color = {255, 127, 0}));
      connect(ramp.y, heatSource1DFV.power) annotation (
        Line(points = {{-705, -46}, {-645, -46}}, color = {0, 0, 127}));
      connect(throughMassFlow.outlet, flowJoin1.in2) annotation (
        Line(points = {{116, -478}, {208, -478}, {208, -476}}, color = {0, 0, 255}));
      connect(throughMassFlow.inlet, pressDropLin.outlet) annotation (
        Line(points = {{96, -478}, {20, -478}}, color = {0, 0, 255}));
      connect(flowJoin1.out, flowJoin3.in2) annotation (
        Line(points = {{220, -472}, {258, -472}, {258, -328}, {162, -328}}, color = {0, 0, 255}));
      connect(MSR_pid.CS, MSR_contVal.theta) annotation (
        Line(points = {{-174, 276}, {-156, 276}, {-156, 238}}, color = {0, 0, 127}));
      connect(flowSplit1.out1, pressDropRHHT.inlet) annotation (
        Line(points = {{-120, 98}, {-114, 98}, {-114, -94}}, color = {0, 0, 255}));
      connect(LPT2.outlet, flowJoin.in1) annotation (
        Line(points = {{431.2, 73.2}, {431.2, 34.2}, {548.2, 34.2}}, color = {0, 0, 255}));
      connect(MSR_pressDrop.outlet, state_HPT_LPT.inlet) annotation (
        Line(points = {{40, 102}, {64, 102}}, color = {0, 0, 255}));
      assert(HP_reheater.drumEquilibrium.Ts < T_HP_reheater_prim.T, "There is a temperature cross in the    HP reheater", AssertionLevel.warning);
      assert(LP_reheater.drumEquilibrium.Ts < T_LP_reheater_prim.T, "There is a temperature cross in the HP reheater", AssertionLevel.warning);
      connect(T_HP_reheater_prim.inlet, HP_reheater.primary_out) annotation (
        Line(points = {{-172, -322}, {-148, -322}, {-148, -324}, {-124, -324}}, color = {0, 0, 255}));
      connect(T_HP_reheater_prim.outlet, sensW_feedwater.inlet) annotation (
        Line(points = {{-184, -322}, {-242, -322}}, color = {0, 0, 255}));
      connect(T_LP_reheater_prim.inlet, LP_reheater.primary_out) annotation (
        Line(points = {{320, -322}, {338, -322}, {338, -320}, {356, -320}}, color = {0, 0, 255}));
      connect(LP_reheater.primary_in, LP_pump.outlet) annotation (
        Line(points = {{376, -320}, {702, -320}, {702, -272}}, color = {0, 0, 255}));
      connect(LPT1.outlet, flowSplit3.in1) annotation (
        Line(points = {{138, 74}, {140, 74}, {140, 94}, {276, 94}}, color = {0, 0, 255}));
      connect(flow1DFV.outfl, MSRflowsplit.in1) annotation (
        Line(points = {{-558, -36}, {-558, 98}, {-484, 98}}, color = {0, 0, 255}));
      connect(flowJoin3.out, HP_feedwater_pump.inlet) annotation (
        Line(points = {{150, -324}, {82, -324}, {82, -322}, {84, -322}}, color = {0, 0, 255}));
      connect(flowJoin2.in2, T_LP_reheater_prim.outlet) annotation (
        Line(points = {{258, -322}, {308, -322}}, color = {0, 0, 255}));
      connect(flowJoin2.out, flowJoin3.in1) annotation (
        Line(points = {{246, -318}, {162, -318}, {162, -320}}, color = {0, 0, 255}));
      connect(pressDropMSR.outlet, flowJoin2.in1) annotation (
        Line(points = {{-80, -114}, {-80, -276}, {282, -276}, {282, -314}, {258, -314}}, color = {0, 0, 255}));
      connect(flowSplit1.out2, steam_dryer.Inlet) annotation (
        Line(points = {{-120, 106}, {-82, 106}}, color = {0, 0, 255}));
      connect(steam_dryer.Liquid, pressDropMSR.inlet) annotation (
        Line(points = {{-72, 96}, {-72, 2}, {-80, 2}, {-80, -94}}, color = {0, 0, 255}));
      connect(MSR_HX_sink.outfl, MSR_pressDrop.inlet) annotation (
        Line(points = {{-4, 102}, {20, 102}}, color = {0, 0, 255}));
      connect(heatSource1DFV2.wall, MSR_HX_sink.wall) annotation (
        Line(points = {{-14, 127}, {-14, 107}}, color = {255, 127, 0}));
      connect(heatSource1DFV2.power, ramp7.y) annotation (
        Line(points = {{-14, 134}, {-14, 165}}, color = {0, 0, 127}));
      connect(steam_dryer.Vapour, he_ww.flangeA1) annotation (
        Line(points = {{-72, 116}, {-394, 116}, {-394, 203}, {-366, 203}}, color = {0, 0, 255}));
      connect(MSRflowsplit.out1, he_ww.flangeA) annotation (
        Line(points = {{-472, 102}, {-470, 102}, {-470, 230}, {-373, 230}, {-373, 214}, {-356, 214}}, color = {0, 0, 255}));
      connect(MSR_contVal.outlet, flowJoin1.in1) annotation (
        Line(points = {{-146, 230}, {208, 230}, {208, -468}}, color = {0, 0, 255}));
      connect(he_ww.flangeB1, sensT.inlet) annotation (
        Line(points = {{-346, 199}, {-346, 202}, {-282, 202}}, color = {0, 0, 255}));
      connect(sensT.outlet, MSR_HX_sink.infl) annotation (
        Line(points = {{-270, 202}, {-246, 202}, {-246, 124}, {-36, 124}, {-36, 102}, {-24, 102}}, color = {0, 0, 255}));
      connect(sensT.T, MSR_pid.PV) annotation (
        Line(points = {{-268, 212}, {-222, 212}, {-222, 272}, {-194, 272}}, color = {0, 0, 127}));
      connect(he_ww.flangeB, MSR_contVal.inlet) annotation (
        Line(points = {{-356, 194}, {-356, 168}, {-196, 168}, {-196, 230}, {-166, 230}}, color = {0, 0, 255}));
      connect(flowJoin.out, condenser.steamIn) annotation (
        Line(points = {{560, 30}, {702, 30}, {702, -50}}, color = {0, 0, 255}));
      connect(condenser.waterOut, LP_pump.inlet) annotation (
        Line(points = {{702, -130}, {702, -232}}, color = {0, 0, 255}));
      annotation (
        Icon(coordinateSystem(extent = {{-1000, -1000}, {1000, 1000}})),
        uses(Modelica(version = "3.2.3"), ThermoPower(version = "3.1")),
        Diagram(coordinateSystem(extent = {{-1000, -1000}, {1000, 1000}})),
        version = "",
        experiment(StartTime = 0, StopTime = 2000, Tolerance = 1e-06, Interval = 4));
    end BOP_no_interfaces;

    model BOP_modelInterfaces
      import ThermoPower;
      replaceable package FlueGas = ThermoPower.Media.FlueGas constrainedby Modelica.Media.Interfaces.PartialMedium "Flue gas model";
      replaceable package Water = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialPureSubstance "Fluid model";
      Modelica.Units.SI.Power Pm_int "total intermediate pressure turbine power";
      Modelica.Units.SI.Power Pm_low "total low pressure turbine power";
      inner ThermoPower.System system(allowFlowReversal = false, initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{812, 844}, {832, 864}}, rotation = 0)));
      ThermoPower.Water.SteamTurbineStodola HPT1(Kt = 0.0242, redeclare package Medium = Water, PRstart = 5.83, eta_iso_nom = 0.9, eta_mech = 0.92, phi(displayUnit = "rad"), pnom = 44e5, wnom = 218.36, wstart = 218.36) annotation (
        Placement(transformation(origin = {-194, 48}, extent = {{-29, -29}, {29, 29}})));
      Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(phi(fixed = true, start = 0), w_fixed = 157) annotation (
        Placement(visible = true, transformation(origin = {-110, 0}, extent = {{930, 40}, {910, 60}}, rotation = 0)));
      Modelica.Mechanics.Rotational.Sensors.PowerSensor powerSensor1 annotation (
        Placement(visible = true, transformation(origin = {-110, 0}, extent = {{868, 64}, {896, 36}}, rotation = 0)));
      ThermoPower.Water.SteamTurbineStodola LPT1(Kt = 0.1335, redeclare package Medium = Water, PRstart = 8.42, eta_iso_nom = 0.89, eta_mech = 0.96, phi(displayUnit = "rad"), pnom = 6.747e5, wnom = 182.2, wstart = 182.2) annotation (
        Placement(visible = true, transformation(origin = {114, 50}, extent = {{-29, -29}, {29, 29}}, rotation = 0)));
      ThermoPower.Water.SteamTurbineStodola LPT2(Kt = 0.8401, redeclare package Medium = Water, PRstart = 11.44, eta_iso_nom = 0.89, eta_mech = 0.96, phi(displayUnit = "rad"), pnom = 0.801e5, wnom = 165.8, wstart = 165.8) annotation (
        Placement(visible = true, transformation(origin = {408, 50}, extent = {{-29, -29}, {29, 29}}, rotation = 0)));
      ThermoPower.Water.FlowSplit flowSplit1 annotation (
        Placement(transformation(origin = {-158, 96}, extent = {{-10, 10}, {10, -10}})));
      ThermoPower.Water.FlowSplit flowSplit3 annotation (
        Placement(visible = true, transformation(origin = {282, 94}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin flowJoin3 annotation (
        Placement(transformation(origin = {284, -326}, extent = {{10, -10}, {-10, 10}})));
      BOP_TBL.Water.PrescribedPressureCondenser condenser(redeclare package Medium = Water, initOpt = ThermoPower.Choices.Init.Options.fixedState, p(displayUnit = "Pa") = 7000, L = 10.3, N_tube = 7950, N_pass = 1, V_heatex = 210, D_ext = 0.0254) annotation (
        Placement(visible = true, transformation(origin = {702, -88}, extent = {{-40, -40}, {40, 40}}, rotation = 0)));
      ThermoPower.Examples.RankineCycle.Models.PrescribedSpeedPump LP_pump(redeclare package FluidMedium = Water, head_nom = {90, 65, 20}, hstart = 1.5e5, n0 = 1500, nominalInletPressure(displayUnit = "Pa") = 7000, nominalMassFlowRate = 182.2, nominalOutletPressure(displayUnit = "Pa") = 754699.9999999998, q_nom = {0.027, 0.179, 0.257}, rho0(displayUnit = "kg/m3") = 1000) annotation (
        Placement(visible = true, transformation(origin = {702, -252}, extent = {{20, -20}, {-20, 20}}, rotation = 90)));
      ThermoPower.Examples.RankineCycle.Models.PrescribedSpeedPump HP_feedwater_pump(redeclare package FluidMedium = Water, head_nom = {100, 500, 107}, n0 = 1500, nominalInletPressure(displayUnit = "Pa") = 714700, nominalMassFlowRate = 239.68, nominalOutletPressure(displayUnit = "Pa") = 4939999.999999999, q_nom = {0.036, 0.236, 0.334}, rho0(displayUnit = "kg/m3") = 1000) annotation (
        Placement(visible = true, transformation(origin = {74, -322}, extent = {{-10, 10}, {10, -10}}, rotation = 180)));
      BOP_TBL.Water.ReheaterV1 LP_reheater(T_start_prim = 312.19, dp_nom_prim(displayUnit = "Pa") = 40000, m_flow_nom_prim = 182.2, p_start_prim(displayUnit = "Pa") = 754699.9999999998, p_start_sec(displayUnit = "Pa") = 80100, Mass_me = 12897, N_tube = 497, N_pass = 2, Lperpass = 14.9, Din_tube = 0.01382, Dext_tube = 0.01588, Dshell = 1.26, Lshell = 14.9, V_sec = ((1.26^2)/4)*14.9*3.14 - 14.9*497*2*3.14*(0.01588^2)/4, V_prim = 497*2*14.9*3.14*((0.01382/2)^2)) annotation (
        Placement(transformation(origin = {370, -314}, extent = {{-20, -20}, {20, 20}})));
      BOP_TBL.Water.ReheaterV1 HP_reheater(T_start_prim = 389.76, dp_nom_prim(displayUnit = "Pa") = 40000, m_flow_nom_prim = 239.68, p_start_prim(displayUnit = "Pa") = 4939999.999999999, p_start_sec(displayUnit = "Pa") = 714700, Mass_me = 11287, N_tube = 982, N_pass = 2, Lperpass = 12.04, Din_tube = 0.01382, Dext_tube = 0.01588, Dshell = 1.1, Lshell = 12.04, V_sec = ((1.1^2)/4)*12.04*3.14 - 12.04*982*2*3.14*(0.01588^2)/4, V_prim = 982*2*12.04*3.14*((0.01382/2)^2)) annotation (
        Placement(transformation(origin = {-114, -320}, extent = {{-20, -20}, {20, 20}})));
      ThermoPower.Water.FlowSplit MSRflowsplit annotation (
        Placement(visible = true, transformation(origin = {-478, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropIT(R = 1e5/218.36) annotation (
        Placement(visible = true, transformation(origin = {-362, 94}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropRHHT(R = 2e4/24.51) annotation (
        Placement(visible = true, transformation(origin = {-114, -104}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      ThermoPower.Water.PressDropLin pressDropRHLP(R = (8.01e4 - 7e3)/16.41) annotation (
        Placement(visible = true, transformation(origin = {516, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.ThroughMassFlow throughMassFlow1(w0 = 16.41) annotation (
        Placement(visible = true, transformation(origin = {426, -472}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.SensW sensW_SG annotation (
        Placement(visible = true, transformation(origin = {-554, -132}, extent = {{-10, 10}, {10, -10}}, rotation = 90)));
      Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow prescribedHeatFlow annotation (
        Placement(visible = true, transformation(origin = {-568, -272}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      BOP_TBL.Water.Drum_simplified drum_simplified(L = 8, initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = true, pstart = 45e5, rext = 5, rint = 5.1) annotation (
        Placement(transformation(origin = {-508, -210}, extent = {{-30, -30}, {30, 30}})));
      Modelica.Blocks.Nonlinear.Limiter limiter( uMax = 0.05) annotation (
        Placement(visible = true, transformation(origin = {-448, -152}, extent = {{-10, -10}, {10, 10}}, rotation = 90))); //limitsAtInit = true,
      Modelica.Blocks.Math.Add add(k1 = -1) annotation (
        Placement(visible = true, transformation(origin = {-370, -72}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Math.Product product annotation (
        Placement(visible = true, transformation(origin = {-420, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.SensW sensW_feedwater annotation (
        Placement(visible = true, transformation(origin = {-248, -318}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Examples.RankineCycle.Models.PID pid(CSmax = 1.0, CSmin = 0, CSstart = 0.8, Kp = 4, PVmax = 500, PVmin = 0, Ti = 10) annotation (
        Placement(transformation(origin = {-344, -254}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
      ThermoPower.Water.ValveLiq FeedwateValve(CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom(displayUnit = "Pa") = 399999.9999999999, pnom = 49e5, wnom = 300) annotation (
        Placement(visible = true, transformation(origin = {-344, -324}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.Water.Flow1DFV flow1DFV(A = 5, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Steam, L = 10, initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = true, omega = 1.3, pstart = 45e5, wnom = 239.68) annotation (
        Placement(visible = true, transformation(origin = {-558, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      ThermoPower.Water.ThroughMassFlow throughMassFlow(w0 = 24.51) annotation (
        Placement(visible = true, transformation(origin = {106, -478}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.Flow1DFV MSR_HX_sink(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, L = 10, initOpt = ThermoPower.Choices.Init.Options.fixedState, omega = 1, pstart = 7.547e5, wnom = 150) annotation (
        Placement(transformation(origin = {-98, 134}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Sources.Ramp ramp7(duration = 500, height = 0, offset = 65e6, startTime = 100) annotation (
        Placement(transformation(origin = {-98, 182}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      ThermoPower.Water.FlowJoin flowJoin annotation (
        Placement(visible = true, transformation(origin = {554, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin flowJoin1 annotation (
        Placement(visible = true, transformation(origin = {214, -472}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp1(duration = 100, height = 0, offset = 1500, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {62, -296}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water state_HPT_LPT annotation (
        Placement(visible = true, transformation(origin = {70, 102}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water state_HPT_in annotation (
        Placement(visible = true, transformation(origin = {-276, 94}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp ramp_feedPump(duration = 100, height = 0, offset = 1500, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {636, -230}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin pressDropLin(R = 2e4/24.51) annotation (
        Placement(visible = true, transformation(origin = {10, -478}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.Flow1DFV MSR_HX_source(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, L = 10, hstartin = Water.specificEnthalpy_pT(T = 300 + 273.15, p = 45e5), hstartout = Water.specificEnthalpy_pT(T = 200 + 273.15, p = 45e5), initOpt = ThermoPower.Choices.Init.Options.fixedState, omega = 1, pstart = 45e5, wnom = 21.31) annotation (
        Placement(transformation(origin = {-336, 230}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Sources.Ramp ramp71(duration = 500, height = 0, offset = -38.9e6, startTime = 100) annotation (
        Placement(visible = true, transformation(origin = {-448, 272}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.SensT MSR_sensT annotation (
        Placement(transformation(origin = {-240, 238}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.PowerPlants.Control.PID MSR_pid(CSmax = 1, CSmin = 0, Kp = 2, PVmax = 600, PVmin = 273.15, Ti = 150) annotation (
        Placement(transformation(origin = {-184, 276}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Sources.Ramp ramp2(duration = 100, height = 0, offset = 200 + 273.15, startTime = 50) annotation (
        Placement(transformation(origin = {-236, 310}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Thermal.HeatSource1DFV heatSource1DFV annotation (
        Placement(visible = true, transformation(origin = {-640, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      ThermoPower.Thermal.HeatSource1DFV heatSource1DFV1 annotation (
        Placement(transformation(origin = {-336, 252}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Thermal.HeatSource1DFV heatSource1DFV2 annotation (
        Placement(transformation(origin = {-98, 152}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.FlowSplit intHP_ext annotation (
        Placement(visible = true, transformation(origin = {-558, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      ThermoPower.Thermal.HeatSource1DFV intHP_heat annotation (
        Placement(visible = true, transformation(origin = {-644, 302}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      ThermoPower.Water.Flow1DFV intHP_HX(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, HydraulicCapacitance = ThermoPower.Choices.Flow1D.HCtypes.Upstream, L = 10, hstartin = Water.specificEnthalpy_pT(T = 300 + 273.15, p = 45e5), hstartout = Water.specificEnthalpy_pT(T = 240 + 273.15, p = 45e5), initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = false, omega = 1, pstart = 45e5, wnom = 5.2) annotation (
        Placement(transformation(origin = {-562, 302}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Blocks.Sources.Ramp intHP_stePoint(duration = 100, height = 0, offset = 240 + 273.15, startTime = 50) annotation (
        Placement(transformation(origin = {-488, 408}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.PowerPlants.Control.PID intHP_pid(CSmax = 1, CSmin = 0, CSstart = 0, Kp = 4, PVmax = 600, PVmin = 273.15, Ti = 60) annotation (
        Placement(transformation(origin = {-436, 374}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.SensT intHP_sensT annotation (
        Placement(visible = true, transformation(origin = {-492, 336}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.ValveLiqChoked intHP_contVal(CheckValve = true, CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom = 45e5 - 7.147e5, pnom = 45e5, thetanom = 1, wnom = 5.2) annotation (
        Placement(visible = true, transformation(origin = {-408, 332}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.ValveLiqChoked MSR_contVal(CheckValve = true, CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom = 45e5 - 7.147e5, pnom = 45e5, rhonom = 800, thetanom = 0.5, wnom = 20) annotation (
        Placement(visible = true, transformation(origin = {-156, 230}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin intHP_MSR_join annotation (
        Placement(visible = true, transformation(origin = {-16, 232}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp intLP_stePoint(duration = 100, height = 0, offset = 80 + 273.15, startTime = 50) annotation (
        Placement(visible = true, transformation(origin = {320, 310}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.Control.PID intLP_pid(CSmax = 1, CSmin = 0, CSstart = 0, Kp = 4, PVmax = 600, PVmin = 273.15, Ti = 60) annotation (
        Placement(transformation(origin = {362, 276}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.SensT intLP_sensT annotation (
        Placement(visible = true, transformation(origin = {322, 236}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.ValveLiqChoked intLP_contVal(CheckValve = true, CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom = 0.8e5 - 0.07e5, pnom = 0.8e5, thetanom = 1, wnom = 15) annotation (
        Placement(transformation(origin = {394, 232}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.Flow1DFV intLP_HX(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, HydraulicCapacitance = ThermoPower.Choices.Flow1D.HCtypes.Upstream, L = 10, hstartin = Water.specificEnthalpy_pT(T = 80 + 273.15, p = 0.8e5), hstartout = Water.specificEnthalpy_pT(T = 80 + 273.15, p = 0.8e5), initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = true, omega = 1, pstart = 0.801e5, wnom = 2) annotation (
        Placement(visible = true, transformation(origin = {272, 232}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Thermal.HeatSource1DFV intLP_heat annotation (
        Placement(visible = true, transformation(origin = {272, 270}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowSplit intLP_ext annotation (
        Placement(visible = true, transformation(origin = {232, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin intLP_join annotation (
        Placement(visible = true, transformation(origin = {626, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Ramp outMP_stePoint(duration = 100, height = 0, offset = 150 + 273.15, startTime = 50) annotation (
        Placement(visible = true, transformation(origin = {42, 408}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Thermal.HeatSource1DFV outMP_heat annotation (
        Placement(visible = true, transformation(origin = {-4, 368}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.Control.PID outMP_pid(CSmax = 1, CSmin = 0, CSstart = 0, Kp = 2, PVmax = 600, PVmin = 273.15, Ti = 500) annotation (
        Placement(visible = true, transformation(origin = {80, 376}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.SensT outMP_sensT annotation (
        Placement(visible = true, transformation(origin = {44, 334}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.Flow1DFV outMP_HX(A = 2, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, HydraulicCapacitance = ThermoPower.Choices.Flow1D.HCtypes.Upstream, L = 10, allowFlowReversal = false, hstartin = Water.specificEnthalpy_pT(T = 170 + 273.15, p = 7.547e5), hstartout = Water.specificEnthalpy_pT(T = 100 + 273.15, p = 7.547e5), initOpt = ThermoPower.Choices.Init.Options.fixedState, noInitialPressure = false, omega = 1, pstart = 7.547e5, wnom = 2) annotation (
        Placement(visible = true, transformation(origin = {-4, 330}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.FlowJoin outMP_MSR_join annotation (
        Placement(visible = true, transformation(origin = {158, 236}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      ThermoPower.Water.PressDropLin MSR_pressDrop(R = 0.8e4/200) annotation (
        Placement(transformation(origin = {-54, 104}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.ValveLiq outMP_contVal(CheckValve = true, CvData = ThermoPower.Choices.Valve.CvTypes.OpPoint, dpnom(displayUnit = "Pa") = 399999.9999999999, pnom = 7.547e5, rhonom = 1000, thetanom = 1, wnom = 2.5) annotation (
        Placement(transformation(origin = {113, 331}, extent = {{-9, -9}, {9, 9}})));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water T_HP_reheater_prim annotation (
        Placement(visible = true, transformation(origin = {-178, -322}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      ThermoPower.PowerPlants.HRSG.Components.StateReader_water T_LP_reheater_prim annotation (
        Placement(transformation(origin = {316, -314}, extent = {{10, -10}, {-10, 10}})));
      Modelica.Blocks.Nonlinear.Limiter intHP_heatlimiter(uMax = 0, uMin = -20e6) annotation (
        Placement(transformation(origin = {-856, 302}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Interfaces.RealInput intHP annotation (
        Placement(visible = true, transformation(origin = {-1000, 302}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-98, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealInput int_MP annotation (
        Placement(visible = true, transformation(origin = {-996, 442}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-98, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Nonlinear.Limiter outMP_heatlimiter(uMax = 0, uMin = -20e6) annotation (
        Placement(transformation(origin = {-858, 442}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Interfaces.RealInput int_LP annotation (
        Placement(visible = true, transformation(origin = {-998, 558}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-98, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Nonlinear.Limiter intMP_heatlimiter(uMax = 0, uMin = -20e6) annotation (
        Placement(transformation(origin = {-860, 558}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Interfaces.RealInput SG_superheating annotation (
        Placement(transformation(origin = {-1020, -48}, extent = {{-20, -20}, {20, 20}}), iconTransformation(origin = {-98, -46}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Interfaces.RealInput SG_evaporator annotation (
        Placement(transformation(origin = {-1020, -272}, extent = {{-20, -20}, {20, 20}}), iconTransformation(origin = {-98, -78}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Interfaces.RealOutput Power annotation (
        Placement(visible = true, transformation(origin = {1010, -410}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {102, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealOutput heat_LP annotation (
        Placement(visible = true, transformation(origin = {1008, 560}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {102, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealOutput heat_MP annotation (
        Placement(visible = true, transformation(origin = {1012, 442}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {102, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealOutput heat_HP annotation (
        Placement(visible = true, transformation(origin = {1008, 300}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {102, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
       BOP_TBL.Water.Steam_dryer steam_dryer annotation (
        Placement(transformation(origin = {-114, 106}, extent = {{-10, -10}, {10, 10}})));
      ThermoPower.Water.FlowJoin flowJoin31 annotation (
        Placement(transformation(origin = {184, -322}, extent = {{10, -10}, {-10, 10}})));
      ThermoPower.Water.FlowSplit mSRflowsplit annotation (
        Placement(transformation(origin = {-50, 138}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Continuous.FirstOrder sG_evaporator_actuator1(T = 4) annotation (
        Placement(transformation(origin = {-804, 300}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Continuous.FirstOrder sG_evaporator_actuator11(T = 4) annotation (
        Placement(transformation(origin = {-800, 440}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Blocks.Continuous.FirstOrder sG_evaporator_actuator111(T = 4) annotation (
        Placement(transformation(origin = {-802, 554}, extent = {{-10, -10}, {10, 10}})));
    equation
      Pm_int = HPT1.Pm;
      Pm_low = LPT1.Pm + LPT2.Pm;
      connect(constantSpeed.flange, powerSensor1.flange_b) annotation (
        Line(points = {{800, 50}, {786, 50}}));
      connect(flowSplit3.out2, LPT2.inlet) annotation (
        Line(points = {{288, 98}, {385, 98}, {385, 73}}, color = {0, 0, 255}));
      connect(LPT2.shaft_b, powerSensor1.flange_a) annotation (
        Line(points = {{426.56, 50}, {757.56, 50}}));
      connect(HPT1.outlet, flowSplit1.in1) annotation (
        Line(points = {{-171, 71}, {-171, 96}, {-164, 96}}, color = {0, 0, 255}));
      connect(MSRflowsplit.out2, pressDropIT.inlet) annotation (
        Line(points = {{-472, 94}, {-372, 94}}, color = {0, 0, 255}));
      connect(LPT1.shaft_b, LPT2.shaft_a) annotation (
        Line(points = {{132.56, 50}, {388.56, 50}}));
      connect(drum_simplified.steam, sensW_SG.inlet) annotation (
        Line(points = {{-491, -189}, {-491, -161}, {-558.6, -161}, {-558.6, -139}}, color = {0, 0, 255}));
      connect(prescribedHeatFlow.port, drum_simplified.heater) annotation (
        Line(points = {{-558, -272}, {-558, -271}, {-508, -271}, {-508, -237}}, color = {191, 0, 0}));
      connect(drum_simplified.level_centered, limiter.u) annotation (
        Line(points = {{-475, -225}, {-475, -227}, {-449, -227}, {-449, -165}}, color = {0, 0, 127}));
      connect(product.y, add.u1) annotation (
        Line(points = {{-409, -88}, {-399, -88}, {-399, -78}, {-383, -78}}, color = {0, 0, 127}));
      connect(limiter.y, product.u2) annotation (
        Line(points = {{-448, -141}, {-448, -95}, {-432, -95}}, color = {0, 0, 127}));
      connect(sensW_SG.w, product.u1) annotation (
        Line(points = {{-548, -124}, {-548, -82}, {-432, -82}}, color = {0, 0, 127}));
      connect(sensW_SG.w, add.u2) annotation (
        Line(points = {{-548, -124}, {-548, -66}, {-382, -66}}, color = {0, 0, 127}));
      connect(sensW_feedwater.w, pid.PV) annotation (
        Line(points = {{-256, -312}, {-278, -312}, {-278, -244}, {-340, -244}}, color = {0, 0, 127}));
      connect(add.y, pid.SP) annotation (
        Line(points = {{-359, -72}, {-359, -68}, {-348, -68}, {-348, -244}}, color = {0, 0, 127}));
      connect(pid.CS, FeedwateValve.theta) annotation (
        Line(points = {{-344, -264}, {-344, -316}}, color = {0, 0, 127}));
      connect(HPT1.shaft_b, LPT1.shaft_a) annotation (
        Line(points = {{-175, 48}, {-40.44, 48}, {-40.44, 50}, {94.56, 50}}));
      connect(sensW_SG.outlet, flow1DFV.infl) annotation (
        Line(points = {{-558, -126}, {-558, -56}}, color = {0, 0, 255}));
      connect(condenser.waterOut, LP_pump.inlet) annotation (
        Line(points = {{702, -128}, {702, -232}}, color = {0, 0, 255}));
      connect(flowSplit3.out1, LP_reheater.vapour_in) annotation (
        Line(points = {{288, 90}, {288, 88}, {370, 88}, {370, -304}}, color = {0, 0, 255}));
      connect(pressDropRHLP.outlet, flowJoin.in2) annotation (
        Line(points = {{526, -8}, {548, -8}, {548, 26}}, color = {0, 0, 255}));
      connect(pressDropRHLP.inlet, throughMassFlow1.outlet) annotation (
        Line(points = {{506, -8}, {506, -7.25}, {478, -7.25}, {478, -472}, {436, -472}}, color = {0, 0, 255}));
      connect(HP_reheater.primary_in, HP_feedwater_pump.outlet) annotation (
        Line(points = {{-104, -320}, {-20, -320}, {-20, -322}, {64, -322}}, color = {0, 0, 255}));
      connect(sensW_feedwater.outlet, FeedwateValve.inlet) annotation (
        Line(points = {{-254, -322}, {-334, -322}, {-334, -324}}, color = {0, 0, 255}));
      connect(FeedwateValve.outlet, drum_simplified.feedwater) annotation (
        Line(points = {{-354, -324}, {-354, -214.5}, {-537, -214.5}}, color = {0, 0, 255}));
      connect(state_HPT_LPT.outlet, LPT1.inlet) annotation (
        Line(points = {{76, 102}, {90, 102}, {90, 73}, {91, 73}}, color = {0, 0, 255}));
      connect(pressDropIT.outlet, state_HPT_in.inlet) annotation (
        Line(points = {{-352, 94}, {-282, 94}}, color = {0, 0, 255}));
      connect(state_HPT_in.outlet, HPT1.inlet) annotation (
        Line(points = {{-270, 94}, {-270, 92}, {-217, 92}, {-217, 71}}, color = {0, 0, 255}));
      connect(pressDropRHHT.outlet, HP_reheater.vapour_in) annotation (
        Line(points = {{-114, -114}, {-114, -310}}, color = {0, 0, 255}));
      connect(ramp_feedPump.y, LP_pump.nPump) annotation (
        Line(points = {{647, -230}, {689, -230}, {689, -238}}, color = {0, 0, 127}));
      connect(ramp1.y, HP_feedwater_pump.nPump) annotation (
        Line(points = {{73, -296}, {100, -296}, {100, -316}, {82, -316}}, color = {0, 0, 127}));
      connect(HP_reheater.condensate, pressDropLin.inlet) annotation (
        Line(points = {{-114, -330}, {-114, -478}, {0, -478}}, color = {0, 0, 255}));
      connect(MSRflowsplit.out1, MSR_HX_source.infl) annotation (
        Line(points = {{-472, 102}, {-424, 102}, {-424, 230}, {-346, 230}}, color = {0, 0, 255}));
      connect(MSR_HX_source.outfl, MSR_sensT.inlet) annotation (
        Line(points = {{-326, 230}, {-248, 230}, {-248, 234}, {-246, 234}}, color = {0, 0, 255}));
      connect(MSR_sensT.T, MSR_pid.PV) annotation (
        Line(points = {{-232, 244}, {-216, 244}, {-216, 272}, {-194, 272}}, color = {0, 0, 127}));
      connect(ramp2.y, MSR_pid.SP) annotation (
        Line(points = {{-225, 310}, {-209, 310}, {-209, 280}, {-194, 280}}, color = {0, 0, 127}));
      connect(heatSource1DFV.wall, flow1DFV.wall) annotation (
        Line(points = {{-637, -46}, {-563, -46}}, color = {255, 127, 0}));
      connect(ramp71.y, heatSource1DFV1.power) annotation (
        Line(points = {{-437, 272}, {-335, 272}, {-335, 256}, {-336, 256}}, color = {0, 0, 127}));
      connect(heatSource1DFV1.wall, MSR_HX_source.wall) annotation (
        Line(points = {{-336, 249}, {-336, 235}}, color = {255, 127, 0}));
      connect(heatSource1DFV2.wall, MSR_HX_sink.wall) annotation (
        Line(points = {{-98, 149}, {-98, 139}}, color = {255, 127, 0}));
      connect(heatSource1DFV2.power, ramp7.y) annotation (
        Line(points = {{-98, 156}, {-98, 171}}, color = {0, 0, 127}));
      connect(throughMassFlow.outlet, flowJoin1.in2) annotation (
        Line(points = {{116, -478}, {208, -478}, {208, -476}}, color = {0, 0, 255}));
      connect(throughMassFlow.inlet, pressDropLin.outlet) annotation (
        Line(points = {{96, -478}, {20, -478}}, color = {0, 0, 255}));
      connect(intHP_ext.in1, flow1DFV.outfl) annotation (
        Line(points = {{-558, 30}, {-558, -36}}, color = {0, 0, 255}));
      connect(intHP_ext.out2, MSRflowsplit.in1) annotation (
        Line(points = {{-554, 42}, {-554, 98}, {-484, 98}}, color = {0, 0, 255}));
      connect(intHP_heat.wall, intHP_HX.wall) annotation (
        Line(points = {{-641, 302}, {-567, 302}}, color = {255, 127, 0}));
      connect(intHP_HX.outfl, intHP_sensT.inlet) annotation (
        Line(points = {{-562, 312}, {-562, 332}, {-498, 332}}, color = {0, 0, 255}));
      connect(intHP_sensT.T, intHP_pid.PV) annotation (
        Line(points = {{-484, 342}, {-474, 342}, {-474, 370}, {-446, 370}}, color = {0, 0, 127}));
      connect(intHP_stePoint.y, intHP_pid.SP) annotation (
        Line(points = {{-477, 408}, {-465, 408}, {-465, 378}, {-446, 378}}, color = {0, 0, 127}));
      connect(intHP_ext.out1, intHP_HX.infl) annotation (
        Line(points = {{-562, 42}, {-562, 292}}, color = {0, 0, 255}));
      connect(intHP_sensT.outlet, intHP_contVal.inlet) annotation (
        Line(points = {{-486, 332}, {-418, 332}}, color = {0, 0, 255}));
      connect(intHP_pid.CS, intHP_contVal.theta) annotation (
        Line(points = {{-426, 374}, {-407, 374}, {-407, 372}, {-408, 372}, {-408, 340}}, color = {0, 0, 127}));
      connect(MSR_contVal.inlet, MSR_sensT.outlet) annotation (
        Line(points = {{-166, 230}, {-234, 230}, {-234, 234}}, color = {0, 0, 255}));
      connect(MSR_pid.CS, MSR_contVal.theta) annotation (
        Line(points = {{-174, 276}, {-156, 276}, {-156, 238}}, color = {0, 0, 127}));
      connect(MSR_contVal.outlet, intHP_MSR_join.in2) annotation (
        Line(points = {{-146, 230}, {-22, 230}, {-22, 228}}, color = {0, 0, 255}));
      connect(intHP_contVal.outlet, intHP_MSR_join.in1) annotation (
        Line(points = {{-398, 332}, {-66, 332}, {-66, 236}, {-22, 236}}, color = {0, 0, 255}));
      connect(flowSplit1.out1, pressDropRHHT.inlet) annotation (
        Line(points = {{-152, 92}, {-120, 92}, {-120, -94}, {-114, -94}}, color = {0, 0, 255}));
      connect(intLP_heat.wall, intLP_HX.wall) annotation (
        Line(points = {{272, 267}, {272, 237}}, color = {255, 127, 0}));
      connect(intLP_HX.outfl, intLP_sensT.inlet) annotation (
        Line(points = {{282, 232}, {316, 232}}, color = {0, 0, 255}));
      connect(intLP_sensT.outlet, intLP_contVal.inlet) annotation (
        Line(points = {{328, 232}, {384, 232}}, color = {0, 0, 255}));
      connect(intLP_stePoint.y, intLP_pid.SP) annotation (
        Line(points = {{331, 310}, {341, 310}, {341, 280}, {352, 280}}, color = {0, 0, 127}));
      connect(intLP_pid.PV, intLP_sensT.T) annotation (
        Line(points = {{352, 272}, {342, 272}, {342, 242}, {330, 242}}, color = {0, 0, 127}));
      connect(intLP_pid.CS, intLP_contVal.theta) annotation (
        Line(points = {{372, 276}, {372, 278}, {394, 278}, {394, 240}}, color = {0, 0, 127}));
      connect(LPT1.outlet, intLP_ext.in1) annotation (
        Line(points = {{137.2, 73.2}, {137.2, 97.2}, {225.2, 97.2}}, color = {0, 0, 255}));
      connect(intLP_ext.out2, flowSplit3.in1) annotation (
        Line(points = {{238, 94}, {276, 94}}, color = {0, 0, 255}));
      connect(intLP_ext.out1, intLP_HX.infl) annotation (
        Line(points = {{238, 102}, {250, 102}, {250, 231.5}, {262, 231.5}, {262, 232}}, color = {0, 0, 255}));
      connect(LPT2.outlet, flowJoin.in1) annotation (
        Line(points = {{431.2, 73.2}, {431.2, 34.2}, {548.2, 34.2}}, color = {0, 0, 255}));
      connect(flowJoin.out, intLP_join.in2) annotation (
        Line(points = {{560, 30}, {620, 30}, {620, 34}}, color = {0, 0, 255}));
      connect(intLP_join.out, condenser.steamIn) annotation (
        Line(points = {{632, 38}, {702, 38}, {702, -48}}, color = {0, 0, 255}));
      connect(intLP_contVal.outlet, intLP_join.in1) annotation (
        Line(points = {{404, 232}, {590, 232}, {590, 42}, {620, 42}}, color = {0, 0, 255}));
      connect(outMP_heat.wall, outMP_HX.wall) annotation (
        Line(points = {{-4, 365}, {-4, 335}}, color = {255, 127, 0}));
      connect(outMP_sensT.T, outMP_pid.PV) annotation (
        Line(points = {{52, 340}, {60, 340}, {60, 372}, {70, 372}}, color = {0, 0, 127}));
      connect(outMP_stePoint.y, outMP_pid.SP) annotation (
        Line(points = {{53, 408}, {63, 408}, {63, 380}, {70, 380}}, color = {0, 0, 127}));
      connect(intHP_MSR_join.out, outMP_MSR_join.in2) annotation (
        Line(points = {{-10, 232}, {152, 232}}, color = {0, 0, 255}));
      connect(outMP_MSR_join.out, flowJoin1.in1) annotation (
        Line(points = {{164, 236}, {208, 236}, {208, -468}}, color = {0, 0, 255}));
      connect(outMP_HX.outfl, outMP_sensT.inlet) annotation (
        Line(points = {{6, 330}, {38, 330}}, color = {0, 0, 255}));
      connect(outMP_contVal.outlet, outMP_MSR_join.in1) annotation (
        Line(points = {{122, 331}, {154, 331}, {154, 240}, {152, 240}}, color = {0, 0, 255}));
      connect(outMP_pid.CS, outMP_contVal.theta) annotation (
        Line(points = {{90, 376}, {112, 376}, {112, 338}, {113, 338}}, color = {0, 0, 127}));
      connect(outMP_sensT.outlet, outMP_contVal.inlet) annotation (
        Line(points = {{50, 330}, {76, 330}, {76, 331}, {104, 331}}, color = {0, 0, 255}));
      connect(T_HP_reheater_prim.inlet, HP_reheater.primary_out) annotation (
        Line(points = {{-172, -322}, {-148, -322}, {-148, -320}, {-124, -320}}, color = {0, 0, 255}));
      connect(T_HP_reheater_prim.outlet, sensW_feedwater.inlet) annotation (
        Line(points = {{-184, -322}, {-242, -322}}, color = {0, 0, 255}));
      connect(T_LP_reheater_prim.inlet, LP_reheater.primary_out) annotation (
        Line(points = {{322, -314}, {360, -314}}, color = {0, 0, 255}));
      connect(T_LP_reheater_prim.outlet, flowJoin3.in1) annotation (
        Line(points = {{310, -314}, {300, -314}, {300, -322}, {290, -322}}, color = {0, 0, 255}));
      connect(LP_reheater.primary_in, LP_pump.outlet) annotation (
        Line(points = {{380, -314}, {700, -314}, {700, -272}, {702, -272}}, color = {0, 0, 255}));
      connect(intHP_heatlimiter.u, intHP) annotation (
        Line(points = {{-868, 302}, {-1000, 302}}, color = {0, 0, 127}));
      connect(int_MP, outMP_heatlimiter.u) annotation (
        Line(points = {{-996, 442}, {-870, 442}}, color = {0, 0, 127}));
      connect(intMP_heatlimiter.u, int_LP) annotation (
        Line(points = {{-872, 558}, {-998, 558}}, color = {0, 0, 127}));
      connect(powerSensor1.power, Power) annotation (
        Line(points = {{760, 66}, {760, -410}, {1010, -410}}, color = {0, 0, 127}));
      assert(HP_reheater.drumEquilibrium.Ts < T_HP_reheater_prim.T, "There is a temperature cross in the    HP reheater", AssertionLevel.warning);
      assert(LP_reheater.drumEquilibrium.Ts < T_LP_reheater_prim.T, "There is a temperature cross in the HP reheater", AssertionLevel.warning);
      connect(HP_feedwater_pump.inlet, flowJoin31.out) annotation (
        Line(points = {{84, -322}, {178, -322}}, color = {0, 0, 255}));
      connect(pressDropMSR1.outlet, flowJoin3.in2) annotation (
        Line(points = {{-62, -100}, {304, -100}, {304, -330}, {290, -330}}, color = {0, 0, 255}));
      connect(flowJoin31.in2, flowJoin3.out) annotation (
        Line(points = {{190, -326}, {278, -326}}, color = {0, 0, 255}));
      connect(flowJoin1.out, flowJoin31.in1) annotation (
        Line(points = {{220, -472}, {228, -472}, {228, -318}, {190, -318}}, color = {0, 0, 255}));
      connect(mSRflowsplit.out1, outMP_HX.infl) annotation (
        Line(points = {{-44, 142}, {-28, 142}, {-28, 330}, {-14, 330}}, color = {0, 0, 255}));
      connect(intHP_heatlimiter.y, sG_evaporator_actuator1.u) annotation (
        Line(points = {{-844, 302}, {-818, 302}, {-818, 300}, {-816, 300}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator1.y, intHP_heat.power) annotation (
        Line(points = {{-793, 300}, {-647, 300}, {-647, 302}, {-648, 302}}, color = {0, 0, 127}));
      connect(outMP_heatlimiter.y, sG_evaporator_actuator11.u) annotation (
        Line(points = {{-846, 442}, {-812, 442}, {-812, 440}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator11.y, outMP_heat.power) annotation (
        Line(points = {{-788, 440}, {-4, 440}, {-4, 372}}, color = {0, 0, 127}));
      connect(intMP_heatlimiter.y, sG_evaporator_actuator111.u) annotation (
        Line(points = {{-848, 558}, {-814, 558}, {-814, 554}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator111.y, intLP_heat.power) annotation (
        Line(points = {{-790, 554}, {272, 554}, {272, 274}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator1.y, heat_HP) annotation (
        Line(points = {{-793, 300}, {-720, 300}, {-720, 672}, {882, 672}, {882, 300}, {1008, 300}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator11.y, heat_MP) annotation (
        Line(points = {{-788, 440}, {-678, 440}, {-678, 636}, {832, 636}, {832, 442}, {1012, 442}}, color = {0, 0, 127}));
      connect(sG_evaporator_actuator111.y, heat_LP) annotation (
        Line(points = {{-790, 554}, {-634, 554}, {-634, 560}, {1008, 560}}, color = {0, 0, 127}));
      connect(steam_dryer.Liquid, pressDropMSR1.inlet) annotation (
        Line(points = {{-114, 96}, {-114, 9}, {-62, 9}, {-62, -80}}, color = {0, 0, 255}));
      connect(SG_superheating, heatSource1DFV.power) annotation (
        Line(points = {{-1020, -48}, {-644, -48}, {-644, -46}}, color = {0, 0, 127}));
      connect(SG_evaporator, prescribedHeatFlow.Q_flow) annotation (
        Line(points = {{-1020, -272}, {-578, -272}}, color = {0, 0, 127}));
      connect(MSR_pressDrop.outlet, mSRflowsplit.in1) annotation (
        Line(points = {{-44, 104}, {-32, 104}, {-32, 122}, {-68, 122}, {-68, 138}, {-56, 138}}, color = {0, 0, 255}));
      connect(mSRflowsplit.out2, state_HPT_LPT.inlet) annotation (
        Line(points = {{-44, 134}, {38, 134}, {38, 102}, {64, 102}}, color = {0, 0, 255}));
      connect(flowSplit1.out2, steam_dryer.Inlet) annotation (
        Line(points = {{-152, 100}, {-134, 100}, {-134, 106}, {-124, 106}}, color = {0, 0, 255}));
      connect(steam_dryer.Vapour, MSR_HX_sink.infl) annotation (
        Line(points = {{-114, 116}, {-114, 134}, {-108, 134}}, color = {0, 0, 255}));
      connect(MSR_HX_sink.outfl, MSR_pressDrop.inlet) annotation (
        Line(points = {{-88, 134}, {-76, 134}, {-76, 104}, {-64, 104}}, color = {0, 0, 255}));
      connect(LP_reheater.condensate, throughMassFlow1.inlet) annotation (
        Line(points = {{370, -324}, {370, -472}, {416, -472}}, color = {0, 0, 255}));
    protected
      ThermoPower.Water.PressDropLin pressDropMSR1(R = 2e4/24.51) annotation (
        Placement(transformation(origin = {-62, -90}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      annotation (
        Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-1020, 860}, {1020, -500}}, initialScale = 0.1)),
        Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}, initialScale = 0.1), graphics = {Rectangle(lineColor = {0, 0, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Text(textColor = {0, 0, 255}, extent = {{-88, 84}, {100, -96}}, textString = "P")}),
        Documentation(revisions = "First version of the BOP simulator for the TANDEM project. The Medium temperature heat input interface is still missing.", info = "<html><head></head><body>First version of the BOP simulator for the TANDEM project.<div><br></div></body></html>"));
    end BOP_modelInterfaces;
  end Models;

  package Water
    model Steam_dryer "Simple phase separator with no dynamic behaviour"
      extends ThermoPower.Icons.Water.Drum;
      replaceable package Medium = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model" annotation (
         choicesAllMatching = true);
      parameter Modelica.Units.SI.PerUnit eta = 1 "Separator efficiency";
      Medium.SaturationProperties sat "Saturation properties";
      Medium.AbsolutePressure p "Operating pressure";
      Modelica.Units.SI.MassFlowRate win "Inlet flowrate";
      Modelica.Units.SI.MassFlowRate wv "Outlet vapour flowrate";
      Modelica.Units.SI.MassFlowRate wl "Outlet liquid flowrate";
      Modelica.Units.SI.SpecificEnthalpy hin "Inlet specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hv "Outlet Vapour specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hl "Outlet Liquid specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hvs "Saturated Vapour specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hls "Saturated Liquid specific enthalpy";
      Modelica.Units.SI.PerUnit x "Vapor mass fraction in the vapour volume";
      ThermoPower.Water.FlangeA Inlet(redeclare package Medium = Medium, m_flow(min = 0)) annotation (
        Placement(visible = true, transformation(origin = {-40, 0}, extent = {{-80, -20}, {-40, 20}}, rotation = 0), iconTransformation(origin = {-40, 0}, extent = {{-80, -20}, {-40, 20}}, rotation = 0)));
      ThermoPower.Water.FlangeB Vapour(redeclare package Medium = Medium, m_flow(max = 0)) annotation (
        Placement(visible = true, transformation(origin = {40, 0}, extent = {{40, 20}, {80, 60}}, rotation = 0), iconTransformation(origin = {-60, 60}, extent = {{40, 20}, {80, 60}}, rotation = 0)));
      ThermoPower.Water.FlangeB Liquid(redeclare package Medium = Medium, m_flow(max = 0)) annotation (
        Placement(visible = true, transformation(origin = {40, 0}, extent = {{40, -60}, {80, -20}}, rotation = 0), iconTransformation(origin = {-60, -60}, extent = {{40, -60}, {80, -20}}, rotation = 0)));
    equation
      //Inlet boundary condition
      Inlet.p = p;
      Inlet.m_flow = win;
      hin = Inlet.h_outflow;
      hin = inStream(Inlet.h_outflow);
      //Outlet boundary conditions
      Vapour.p = p;
      Vapour.m_flow = -wv;
      Vapour.h_outflow = hv;
      // Liquid.p = p;
      Liquid.m_flow = -wl;
      Liquid.h_outflow = hl;
      //Perfect phase separation condition
      x = if (hin < hvs) then (hin - hls)/(hvs - hls) elseif (hin < hls) then 0 else 1/eta;
      wv = win*x*eta;
      hl = if (x > 0) then hls else hin;
      //Mass balance
      0 = win - wl - wv;
      // Energy balance
      0 = win*hin - wv*hv - wl*hl;
      // Saturation properties
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      hls = Medium.bubbleEnthalpy(sat);
      hvs = Medium.dewEnthalpy(sat);
      assert(x > 0, "Vapor phase is below the bubble point. The steam dryer is out of its working domain");
      //assert(Inlet.m_flow > 0, "A reverse flow is identified in the phase separator");
      assert(Vapour.m_flow < 0, "A reverse flow is identified in the phase separator");
      //assert(Liquid.m_flow < 0, "A reverse flow is identified in the phase separator");
      annotation (
        Icon(graphics),
        Documentation(info = "Equilibrium phase separator with no thermal or hydraulic capacitance."));
    end Steam_dryer;

    model Steam_dryer_elaborated "Simple phase separator with no dynamic behaviour"
      extends ThermoPower.Icons.Water.Drum;
      replaceable package Medium = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model" annotation (
         choicesAllMatching = true);
      parameter Modelica.Units.SI.Volume V "Steam dryem volume";
      parameter Modelica.Units.SI.Pressure pstart "Pressure start value" annotation (
        Dialog(tab = "Initialisation"));
      parameter Modelica.Units.SI.Volume Vlstart "Start value of drum water volume" annotation (
        Dialog(tab = "Initialisation"));
      parameter Modelica.Units.SI.SpecificEnthalpy hlstart = Medium.bubbleEnthalpy(Medium.setSat_p(pstart)) "Liquid volume start specific enthalpy";
      Medium.SaturationProperties sat "Saturation properties";
      Medium.AbsolutePressure p "Operating pressure";
      Modelica.Units.SI.MassFlowRate win "Inlet flowrate";
      Modelica.Units.SI.MassFlowRate wv "Outlet vapour flowrate";
      Modelica.Units.SI.MassFlowRate wl "Outlet liquid flowrate";
      Modelica.Units.SI.SpecificEnthalpy hin "Inlet specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hv "Vapour volume specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hvout "Vapour outlet specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hvs "Saturated Vapour specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hls "Saturated Liquid specific enthalpy";
      Modelica.Units.SI.Energy Ev "Vapour volume internal energy";
      Modelica.Units.SI.Energy El "Liquid volume internal energy";
      Modelica.Units.SI.Energy E "Total volume internal energy";
      Modelica.Units.SI.Mass Mv "Vapour mass in the control volume";
      Modelica.Units.SI.Mass Ml "Liquid mass in the control volume";
      Modelica.Units.SI.Mass M "Total mass in the control volume";
      Modelica.Units.SI.Density rhov "Vapour volume density";
      Modelica.Units.SI.Density rhol "Liquid volume density";
      Modelica.Units.SI.Volume Vv "Vapour volume";
      Modelica.Units.SI.Volume Vl "Saturated liquid volume";
      Modelica.Units.SI.PerUnit x "Vapor mass fraction in the vapour volume";
      Modelica.Units.SI.PerUnit y "Water fraction in the control volume";
      ThermoPower.Water.FlangeA Inlet(redeclare package Medium = Medium, m_flow(min = 0)) annotation (
        Placement(visible = true, transformation(origin = {-40, 0}, extent = {{-80, -20}, {-40, 20}}, rotation = 0), iconTransformation(origin = {-40, 0}, extent = {{-80, -20}, {-40, 20}}, rotation = 0)));
      ThermoPower.Water.FlangeB Vapour(redeclare package Medium = Medium, m_flow(max = 0)) annotation (
        Placement(visible = true, transformation(origin = {40, 0}, extent = {{40, 20}, {80, 60}}, rotation = 0), iconTransformation(origin = {-60, 60}, extent = {{40, 20}, {80, 60}}, rotation = 0)));
      ThermoPower.Water.FlangeB Liquid(redeclare package Medium = Medium, m_flow(max = 0)) annotation (
        Placement(visible = true, transformation(origin = {40, 0}, extent = {{40, -60}, {80, -20}}, rotation = 0), iconTransformation(origin = {-60, -60}, extent = {{40, -60}, {80, -20}}, rotation = 0)));
    equation
      //Inlet boundary condition
      Inlet.p = p;
      Inlet.m_flow = win;
      hv = Inlet.h_outflow;
      hin = Inlet.h_outflow;
      hin = inStream(Inlet.h_outflow);
      //Outlet boundary conditions
      Vapour.p = p;
      Vapour.m_flow = -wv;
      hvout = noEvent(if (hv < hvs) then hvs else hv);
      Vapour.h_outflow = hvout;
      //Liquid.p = p;
      Liquid.m_flow = -wl;
      Liquid.h_outflow = hls;
      //Perfect phase separation condition
      x = noEvent(if (hv < hvs) then (hv - hls)/(hvs - hls) else 1);
      wv = noEvent(if (x > 0) then win*x else 0);
      //Mass balance
      Mv = Vv*rhov;
      Ml = Vl*rhol;
      M = Ml + Mv;
      der(M) = win - wl - wv;
      //Energy balance
      Ev = Mv*hv - p*Vv "The vapour volume state is an unknow of the problem";
      El = Ml*hls - p*Vl "The liquid volume in the separator is saturated";
      E = Ev + El;
      der(E) = win*hin - wl*hls - wv*hvout;
      //Fundamental properties
      V = Vv + Vl;
      y = Vl/V;
      rhov = Medium.density_ph(p, hv);
      rhol = Medium.bubbleDensity(sat);
      // Saturation properties
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      hls = Medium.bubbleEnthalpy(sat);
      hvs = Medium.dewEnthalpy(sat);
      assert(x > 0, "Vapor phase is below the bubble point. The steam dryer is out of its working domain");
      //assert(Inlet.m_flow > 0, "A reverse flow is identified in the phase separator");
      assert(Vapour.m_flow < 0, "A reverse flow is identified in the phase separator");
      //assert(Liquid.m_flow < 0, "A reverse flow is identified in the phase separator");
    initial equation
      hv = inStream(Inlet.h_outflow);
      Vl = Vlstart;
      p = pstart;
      annotation (
        Icon(graphics),
        Documentation(info = "Equilibrium phase separator with no thermal or hydraulic capacitance."));
    end Steam_dryer_elaborated;

model ReheaterV1
  replaceable package Water = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialPureSubstance "Fluid model";
    
    parameter Modelica.Units.SI.Mass Mass_me "Dry heat exchanger mass";
    parameter Integer N_tube "Number of tube";
    parameter Integer N_pass "Number of pass";
    parameter Modelica.Units.SI.Length Lperpass "length tube per pass"; 
    parameter Modelica.Units.SI.Length Din_tube "internal diameter - tube"; 
    parameter Modelica.Units.SI.Length Dext_tube "external diameter - tube";
    parameter Modelica.Units.SI.Length Dshell "shell diameter ";
     parameter Modelica.Units.SI.Length Lshell "Shell length";
    
    parameter Modelica.Units.SI.Temperature T_start_prim = 300.0 "starting temperature on primary side";
    parameter Modelica.Units.SI.Volume V_prim =  N_tube*N_pass*Lperpass*3.14*(((Din_tube)^2)/4) "Volume primary = N_tube*N_pass*Lperpass*3.14*(((Din_tube)^2)/4)";
    parameter Modelica.Units.SI.MassFlowRate m_flow_nom_prim = 10.0 "Nominal primary flowrate";
    parameter Modelica.Units.SI.Pressure p_start_prim = 1e5 "starting pressure on primary side";
    
    parameter Modelica.Units.SI.Pressure dp_nom_prim "Primary circuit presure loss at nominal flow";
    
    parameter Modelica.Units.SI.Volume V_sec = abs(((Dshell^2)*3.14*Lshell/4)-(N_tube*N_pass*Lperpass*3.14*(((Dext_tube)^2)/4))) " Volume secondary = ((Dshell^2)*3.14*Lshell/4)-(N_tube*N_pass*Lperpass*3.14*(((Dext_tube)^2)/4))";
    parameter Modelica.Units.SI.Pressure p_start_sec = 1e5 "starting pressure on secondary side";
    
    
    
    Buildings.Fluid.MixingVolumes.MixingVolume primary_piping(redeclare package Medium = Water,T_start = T_start_prim, V = V_prim, m_flow_nominal = m_flow_nom_prim, nPorts = 2, p_start = p_start_prim) annotation(
      Placement(visible = true, transformation(origin = {10, -10}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
    Modelica.Thermal.HeatTransfer.Components.ThermalConductor thermalConductor(G = 1e7) annotation(
      Placement(visible = true, transformation(origin = {-30, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    BOP_TBL.Water.DrumReverse drumEquilibrium(Mm = Mass_me, Vd = V_sec, Vlstart = 0.5*V_sec, allowFlowReversal = false, cm = 450, initOpt = ThermoPower.Choices.Init.Options.fixedState, pstart(displayUnit = "Pa") = p_start_sec) annotation(
      Placement(visible = true, transformation(origin = {-30, 50}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
    ThermoPower.Water.FlangeA vapour_in annotation(
      Placement(visible = true, transformation(origin = {0, 98}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    ThermoPower.Water.FlangeB condensate annotation(
      Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    ThermoPower.Water.FlangeA primary_in annotation(
      Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    ThermoPower.Water.FlangeB primary_out annotation(
      Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    ThermoPower.Water.PressDropLin pressDropLin(R = dp_nom_prim/m_flow_nom_prim)  annotation(
      Placement(visible = true, transformation(origin = {-60, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealOutput level annotation(
      Placement(visible = true, transformation(origin = {110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Sources.RealExpression level_transmitter(y = drumEquilibrium.Vl / drumEquilibrium.Vd)  annotation(
      Placement(visible = true, transformation(origin = {70, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(thermalConductor.port_a, primary_piping.heatPort) annotation(
      Line(points = {{-30, 8}, {-30, -10}, {0, -10}}, color = {191, 0, 0}));
    connect(thermalConductor.port_b, drumEquilibrium.wall) annotation(
      Line(points = {{-30, 28}, {-30, 42}}, color = {191, 0, 0}));
    connect(primary_in, primary_piping.ports[1]) annotation(
      Line(points = {{100, 0}, {10, 0}}, color = {0, 0, 255}));
    connect(vapour_in, drumEquilibrium.Steam) annotation(
      Line(points = {{0, 98}, {0, 80}, {-36, 80}, {-36, 58}}, color = {0, 0, 255}));
    connect(drumEquilibrium.Condensate, condensate) annotation(
      Line(points = {{-20, 46}, {40, 46}, {40, -60}, {0, -60}, {0, -100}}, color = {0, 0, 255}));
    connect(primary_piping.ports[2], pressDropLin.inlet) annotation(
      Line(points = {{10, 0}, {-50, 0}}, color = {0, 0, 255}));
    connect(pressDropLin.outlet, primary_out) annotation(
      Line(points = {{-70, 0}, {-100, 0}}, color = {0, 0, 255}));
    connect(level_transmitter.y, level) annotation(
      Line(points = {{82, -50}, {110, -50}}, color = {0, 0, 127}));
  
  annotation(
      Icon(graphics = {Rectangle(fillColor = {170, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Line(origin = {-1, 0}, points = {{-99, 0}, {-39, 0}, {-39, 20}, {41, -20}, {41, 0}, {99, 0}}), Rectangle(origin = {0, -73}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 27}, {100, -27}})}, coordinateSystem(extent = {{-200, -200}, {200, 200}})),
      Diagram(coordinateSystem(extent = {{-200, -200}, {200, 200}})));


end ReheaterV1;

    model HE_WW
replaceable package ShellMedium = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialMedium "Liquid medium model" annotation(
         choicesAllMatching = true);
      replaceable package TubeMedium = ThermoPower.Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialMedium "Liquid medium model" annotation(
         choicesAllMatching = true);
      parameter Integer Nodes = 11;
      parameter Integer Nt;
      parameter Integer Np;
      parameter Modelica.Units.SI.Length L;
      parameter Modelica.Units.SI.Length di;
      parameter Modelica.Units.SI.Length do;
      parameter Modelica.Units.SI.Length pitch;
      parameter Real Bfrac "Baffle fraction";
      parameter Real CL;
      parameter Real CTP;
      parameter ThermoPower.Units.SpecificThermalResistance Rfl = 6.5e-5 "Fouling factor";
      parameter Modelica.Units.SI.Density rhom = 7763;
      parameter Modelica.Units.SI.SpecificHeatCapacity cpm = 510.4;
      parameter Modelica.Units.SI.ThermalConductivity km = 57.45;
      parameter Modelica.Units.SI.Power Qnom;
      parameter Modelica.Units.SI.MassFlowRate ms_nom;
      parameter Modelica.Units.SI.MassFlowRate mt_nom;
      parameter Modelica.Units.SI.Pressure ps_nom;
      parameter Modelica.Units.SI.Pressure pt_nom;
      parameter Modelica.Units.SI.SpecificEnthalpy hsi_nom;
      parameter Modelica.Units.SI.SpecificEnthalpy hso_nom;
      parameter Modelica.Units.SI.SpecificEnthalpy hti_nom;
      parameter Modelica.Units.SI.SpecificEnthalpy hto_nom;
      parameter Modelica.Units.SI.PressureDifference dpt_nom = 0;
      parameter Real Cf_t;
      parameter Modelica.Units.SI.CoefficientOfHeatTransfer hshell;
      replaceable model ShellHeatTransfer = ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV annotation(
         choicesAllMatching = true);
      replaceable model TubeHeatTransfer = ThermoPower.Thermal.HeatTransferFV.IdealHeatTransfer constrainedby ThermoPower.Thermal.BaseClasses.DistributedHeatTransferFV annotation(
         choicesAllMatching = true);
      final parameter Real PR = pitch/do;
      final parameter Modelica.Units.SI.Area Ao = Np*pi*do*Nt*L;
      final parameter Modelica.Units.SI.Length Ds = 0.637*sqrt(CL/CTP)*sqrt(Ao*PR^2*do/L);
      final parameter Modelica.Units.SI.Length C = pitch - do;
      final parameter Modelica.Units.SI.Length B = Bfrac*Ds;
      final parameter Modelica.Units.SI.Area As = Ds*C*B/pitch;
      final parameter Modelica.Units.SI.Area As_tot = pi*(Ds/2)^2 - Nt*Np*pi*(do/2)^2;
      ThermoPower.Water.FlangeB flangeB(redeclare package Medium = ShellMedium, m_flow(start = -ms_nom), p(start = ps_nom), h_outflow(start = hso_nom)) annotation(
        Placement(transformation(extent = {{-10, -90}, {10, -70}}), iconTransformation(origin = {0, -16}, extent = {{-10, -90}, {10, -70}})));
      ThermoPower.Water.FlangeA flangeA(redeclare package Medium = ShellMedium, p(start = ps_nom), m_flow(start = ms_nom), h_outflow(start = hsi_nom)) annotation(
        Placement(transformation(extent = {{-10, 68}, {10, 88}}), iconTransformation(origin = {0, 18}, extent = {{-10, 68}, {10, 88}})));
      ThermoPower.Water.FlangeA flangeA1(redeclare package Medium = TubeMedium, p(start = pt_nom), m_flow(start = mt_nom), h_outflow(start = hti_nom)) annotation(
        Placement(transformation(extent = {{70, -10}, {90, 10}}), iconTransformation(origin = {-182, -6}, extent = {{70, -10}, {90, 10}})));
      ThermoPower.Water.FlangeB flangeB1(redeclare package Medium = TubeMedium, p(start = pt_nom - dpt_nom), m_flow(start = -mt_nom), h_outflow(start = hto_nom)) annotation(
        Placement(transformation(extent = {{-88, -10}, {-68, 10}}), iconTransformation(origin = {180, -52}, extent = {{-88, -10}, {-68, 10}})));
      ThermoPower.Water.Flow1DFV2ph shell_2ph(redeclare package Medium = ShellMedium, N = Nodes, L = L, A = As, omega = Np*Nt*pi*do, Dhyd = 4*(pitch^2 - pi*do^2/4)/pi/do, wnom = ms_nom, FFtype = ThermoPower.Choices.Flow1D.FFtypes.NoFriction, FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.TwoPhases, pstart = ps_nom, hstartin = hsi_nom, hstartout = hso_nom, redeclare model HeatTransfer = ShellHeatTransfer, fixedMassFlowSimplified = true, Q(start = -Qnom)) annotation(
        Placement(transformation(origin = {0, 50}, extent = {{14, -14}, {-14, 14}}, rotation = 180)));
      ThermoPower.Water.Flow1DFV tube(redeclare package Medium = TubeMedium, N = Nodes, Nt = Nt, L = L*Np, A = pi*di^2/4, omega = pi*di, Dhyd = di, wnom = mt_nom, FFtype = ThermoPower.Choices.Flow1D.FFtypes.NoFriction, dpnom = dpt_nom, Kfnom = Cf_t, e = 1e-6, pstart = pt_nom, hstartin = hti_nom, hstartout = hto_nom, fixedMassFlowSimplified = true, redeclare model HeatTransfer = TubeHeatTransfer, Q(start = Qnom)) annotation(
        Placement(transformation(extent = {{14, -64}, {-14, -36}})));
      ThermoPower.Thermal.MetalTubeFV metalTubeFV(Nw = Nodes - 1, Nt = Np*Nt, L = L, rint = di/2, rext = do/2, rhomcm = rhom*cpm, lambda = km, Tstartbar = ShellMedium.temperature_ph(ps_nom, hso_nom)) annotation(
        Placement(transformation(extent = {{-14, -14}, {14, 14}}, rotation = 180, origin = {0, -10})));
      ThermoPower.Thermal.HeatExchangerTopologyFV heatExchangerTopologyFV(Nw = Nodes - 1, redeclare model HeatExchangerTopology = ThermoPower.Thermal.HeatExchangerTopologies.CounterCurrentFlow) annotation(
        Placement(transformation(extent = {{-12, -12}, {12, 12}}, rotation = 180, origin = {0, 16})));
      ThermoPower.Water.SensT1 sensT1_1(redeclare package Medium = TubeMedium, T(start = TubeMedium.temperature_ph(pt_nom, hto_nom))) annotation(
        Placement(transformation(extent = {{-54, -6}, {-34, 14}})));
      ThermoPower.Water.PressDrop pressDrop1(wnom = mt_nom, FFtype = ThermoPower.Choices.PressDrop.FFtypes.Kf, Kf = Cf_t, dp(start = dpt_nom), dpnom = dpt_nom, w(start = mt_nom)) annotation(
        Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 90, origin = {-26, -26})));
    protected
      constant Real pi = Modelica.Constants.pi;
    equation
      connect(flangeA1, tube.infl) annotation(
        Line(points = {{80, 0}, {60, 0}, {60, -50}, {14, -50}}, color = {0, 0, 255}));
      connect(shell_2ph.wall, heatExchangerTopologyFV.side2) annotation(
        Line(points = {{0, 43}, {0, 19.72}}, color = {255, 127, 0}));
      connect(heatExchangerTopologyFV.side1, metalTubeFV.ext) annotation(
        Line(points = {{0, 12.4}, {0, -5.66}}, color = {255, 127, 0}));
      connect(flangeA, shell_2ph.infl) annotation(
        Line(points = {{0, 78}, {-60, 78}, {-60, 50}, {-14, 50}}, color = {0, 0, 255}));
      connect(metalTubeFV.int, tube.wall) annotation(
        Line(points = {{0, -14.2}, {0, -43}}, color = {255, 127, 0}));
      connect(flangeB1, sensT1_1.flange) annotation(
        Line(points = {{-78, 0}, {-44, 0}}, color = {0, 0, 255}));
      connect(shell_2ph.outfl, flangeB) annotation(
        Line(points = {{14, 50}, {40, 50}, {40, -80}, {0, -80}}, color = {0, 0, 255}));
      connect(flangeB1, pressDrop1.outlet) annotation(
        Line(points = {{-78, 0}, {-26, 0}, {-26, -16}}, color = {0, 0, 255}));
      connect(pressDrop1.inlet, tube.outfl) annotation(
        Line(points = {{-26, -36}, {-26, -50}, {-14, -50}}, color = {0, 0, 255}));
      annotation(
        Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100, -100}, {100, 100}}), graphics),
        Icon(graphics = {Rectangle(lineColor = {0, 0, 255}, fillColor = {230, 230, 230}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Line(origin = {0.522389, -1.04478}, rotation = 90, points = {{(-0 - 16), -86}, {0, -40}, {40, -20}, {-40, 20}, {0, 40}, {0, 84}}, color = {0, 0, 255}, thickness = 0.5), Text(textColor = {85, 170, 255}, extent = {{-100, -115}, {100, -145}}, textString = "%name")}),
        Documentation(revisions = "<html>"));
    
    end HE_WW;

    model Drum_simplified
extends ThermoPower.Icons.Water.Drum;
      
      replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
        Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model"
        annotation(choicesAllMatching = true);
    
      Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heater
        "Metal wall thermal port" annotation (Placement(transformation(extent={{-28,
                -100},{28,-80}}, rotation=0)));
      Modelica.Units.SI.HeatFlowRate Q_heater "Heat flow to the risers";
    
    
      Medium.ThermodynamicState liquidState "Thermodynamic state of the liquid";
      Medium.ThermodynamicState vapourState "Thermodynamic state of the vapour";
      Medium.SaturationProperties sat;
      parameter Modelica.Units.SI.Length rint = 1.7841 "Internal radius";
      parameter Modelica.Units.SI.Length rext = 1.85 "External radius";
      parameter Modelica.Units.SI.Length L = 5 "Length";
      parameter Modelica.Units.SI.HeatCapacity Cm = 0 "Total Heat Capacity of the metal wall" annotation(
        Evaluate = true);
      parameter Modelica.Units.SI.Temperature Text = 293 "External atmospheric temperature";
      parameter Modelica.Units.SI.Time tauev = 15 "Time constant of bulk evaporation";
      parameter Modelica.Units.SI.Time tauc = 15 "Time constant of bulk condensation";
      parameter Real Kcs = 0.01 "Surface condensation coefficient [kg/(s.m2.K)]";
      parameter Real Ks = 100 "Surface heat transfer coefficient [W/(m2.K)]";
      parameter Modelica.Units.SI.CoefficientOfHeatTransfer gext = 0 "Heat transfer coefficient between metal wall and external atmosphere";
      parameter Modelica.Units.SI.CoefficientOfHeatTransfer gl = 200 "Heat transfer coefficient between metal wall and liquid phase" annotation(
        Evaluate = true);
      parameter Modelica.Units.SI.CoefficientOfHeatTransfer gv = 200 "Heat transfer coefficient between metal wall and vapour phase" annotation(
        Evaluate = true);
      parameter Modelica.Units.SI.ThermalConductivity lm = 20 "Metal wall thermal conductivity";
      parameter Real afd = 0.05 "Ratio of feedwater in downcomer flowrate";
      parameter Real avr = 1.2 "Phase separation efficiency coefficient";
      //  parameter Integer DrumOrientation = 1 "0: Horizontal; 1: Vertical";
      parameter Boolean allowFlowReversal = system.allowFlowReversal "= true to allow flow reversal, false restricts to design direction" annotation(
        Evaluate = true);
    
      outer ThermoPower.System system "System wide properties";
    
    
      parameter Medium.AbsolutePressure pstart = 1e5 "Pressure start value" annotation(
        Dialog(tab = "Initialisation"));
      parameter Medium.SpecificEnthalpy hlstart = Medium.bubbleEnthalpy(Medium.setSat_p(pstart)) "Liquid enthalpy start value" annotation(
        Dialog(tab = "Initialisation"));
      parameter Medium.SpecificEnthalpy hvstart = Medium.dewEnthalpy(Medium.setSat_p(pstart)) "Vapour enthalpy start value" annotation(
        Dialog(tab = "Initialisation"));
      parameter Medium.Temperature Tmstart = Medium.saturationTemperature(pstart) "Wall temperature start value" annotation(
        Dialog(tab = "Initialisation"));
      parameter Modelica.Units.SI.Length ystart = 0.0 "Start level value - zero = half level" annotation(
        Dialog(tab = "Initialisation"));
      parameter ThermoPower.Choices.Init.Options initOpt = system.initOpt "Initialisation option" annotation(
        Dialog(tab = "Initialisation"));
      parameter Boolean noInitialPressure = false "Remove initial equation on pressure" annotation(
        Dialog(tab = "Initialisation"),
        choices(checkBox = true));
    
      Modelica.Units.SI.Length level( start = L/2) "level as seen from bottom";
      
      constant Real eta=0.01 "fraction of volume at which simplfied equations are used to avoid numerical errors when the vessels becomes full/empty";
      
      
      constant Modelica.Units.SI.Acceleration g = Modelica.Constants.g_n;
      constant Real pi = Modelica.Constants.pi;
      Modelica.Units.SI.Volume Vv(start = pi * rint ^ 2 * L / 2) "Volume occupied by the vapour";
      Modelica.Units.SI.Volume Vl(start = pi * rint ^ 2 * L / 2) "Volume occupied by the liquid";
      Medium.AbsolutePressure p(start = pstart, stateSelect = StateSelect.prefer) "Surface pressure";
      Modelica.Units.SI.SpecificEnthalpy hl(start = hlstart, stateSelect = StateSelect.prefer) "Liquid specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hv(start = hvstart, stateSelect = StateSelect.prefer) "Vapour specific enthalpy";
      Modelica.Units.SI.SpecificEnthalpy hls "Specific enthalpy of saturated liquid";
      Modelica.Units.SI.SpecificEnthalpy hvs "Specific enthalpy of saturated vapour";
      Modelica.Units.SI.SpecificEnthalpy hf "Specific enthalpy of feedwater";
      Modelica.Units.SI.SpecificEnthalpy hvout "Specific enthalpy of steam at the outlet";
    
    
      Medium.MassFlowRate wf "Mass flowrate of feedwater";
      Medium.MassFlowRate wv "Mass flowrate of steam at the outlet";
      Medium.MassFlowRate wc "Mass flowrate of bulk condensation";
      Medium.MassFlowRate wcs "Mass flowrate of surface condensation";
      Medium.MassFlowRate wev "Mass flowrate of bulk evaporation";
      Medium.Temperature Tl "Liquid temperature";
      Medium.Temperature Tv "Vapour temperature";
      Modelica.Units.SI.Temperature Tm(start = Tmstart, stateSelect = if Cm > 0 then StateSelect.prefer else StateSelect.default) "Wall temperature";
      Medium.Temperature Ts "Saturated water temperature";
    
      Modelica.Units.SI.Power Qmv "Heat flow from the wall to the vapour";
      Modelica.Units.SI.Power Qvl "Heat flow from the vapour to the liquid";
      Modelica.Units.SI.Power Qml "Heat flow from the wall to the liquid";
      Modelica.Units.SI.Power Qme "Heat flow from the wall to the atmosphere";
    
      Modelica.Units.SI.Mass Ml "Liquid mass";
      Modelica.Units.SI.Mass Mv "Vapour mass";
      Modelica.Units.SI.Energy El "Liquid internal energy";
      Modelica.Units.SI.Energy Ev "Vapour internal energy";
      Modelica.Units.SI.Density rhol "Liquid density";
      Modelica.Units.SI.Density rhov "Vapour density";
      Modelica.Units.SI.PerUnit xl "Mass fraction of vapour in the liquid volume";
      Modelica.Units.SI.PerUnit xv "Steam quality in the vapour volume";
    
      Real gml "Total heat transfer coefficient (wall-liquid)";
      Real gmv "Total heat transfer coefficient (wall-vapour)";
      Real a;
      Modelica.Units.SI.Length y(start = ystart, stateSelect = StateSelect.prefer, min=-L/2, max = L/2) "Level (referred to the centreline)";
      Modelica.Units.SI.Area Aml "Surface of the wall-liquid interface";
      Modelica.Units.SI.Area Amv "Surface of the wall-vapour interface";
      Modelica.Units.SI.Area Asup "Surface of the liquid-vapour interface";
      Modelica.Units.SI.Area Aext "External drum surface";
    
    
      ThermoPower.Water.FlangeA feedwater(p(start = pstart), h_outflow(start = hlstart), redeclare package Medium = Medium, m_flow(min = if allowFlowReversal then -Modelica.Constants.inf else 0)) annotation(
        Placement(transformation(extent = {{-114, -32}, {-80, 2}}, rotation = 0)));
    
    
      ThermoPower.Water.FlangeB steam(p(start = pstart), h_outflow(start = hvstart), redeclare package Medium = Medium, m_flow(max = if allowFlowReversal then +Modelica.Constants.inf else 0)) annotation(
        Placement(transformation(extent = {{40, 52}, {76, 88}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealOutput level_centered annotation(
        Placement(visible = true, transformation(origin = {110, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
    
      if noEvent(level < (L-eta)) then
        der(Mv) = wev - wv - wc - wcs "Vapour volume mass balance";
        der(Ml) = wf + wc + wcs - wev "Liquid volume mass balance";
        der(Ev) = (wev - wcs) * hvs - wc * hls - wv * hvout + Qmv - Qvl - p * der(Vv) "Vapour volume energy balance";
        der(El) = wf * hf + wc * hls + (wcs - wev) * hvs + Qml + Qvl - p * der(Vl) + Q_heater "Liquid volume energy balance";
      else
        der(Mv) = wev "Vapour volume mass balance";
        der(Ml) = wf - wev "Liquid volume mass balance";
        der(Ev) = (wev) * hvs "Vapour volume energy balance";
        der(El) = wf * hf + wev * hvs + Qml - p * der(Vl) + Q_heater "Liquid volume energy balance";
      end if;
    //Metal wall energy balance with singular cases
      if Cm > 0 and (gl > 0 or gv > 0) then
        Cm * der(Tm) = (-Qml) - Qmv - Qme "Metal wall dynamic energy balance";
      elseif gl > 0 or gv > 0 then
        0 = (-Qml) - Qmv - Qme "Metal wall static energy balance";
      else
        Tm = 300 "Wall temperature doesn't matter";
      end if;
    
    
      Mv = Vv * rhov "Vapour volume mass";
      Ml = Vl * rhol "Liquid volume mass";
      Ev = Mv * Medium.specificInternalEnergy(vapourState) "Vapour volume energy";
      El = Ml * Medium.specificInternalEnergy(liquidState) "Liquid volume energy";
    
      wev = xl * rhol * Vl / tauev "Bulk evaporation flow rate in the liquid volume";
      wc = (1 - xv) * rhov * Vv / tauc "Bulk condensation flow rate in the vapour volume";
      wcs = Kcs * Asup * (Ts - Tl) "Surface condensation flow rate";
    
      Qme = gext * Aext * (Tm - Text) "Heat flow from metal wall to external environment";
      Qml = gml * Aml * (Tm - Tl) "Heat flow from metal wall to liquid volume";
      if noEvent(level < (L-eta)) then
        Qmv = gmv * Amv * (Tm - Tv) "Heat flow from metal wall to vapour volume";
        Qvl = Ks * Asup * (Tv - Ts) "Heat flow from vapour to liquid volume";
      else
        Qmv = 0.0;
        Qvl = 0.0;
      end if;
      
      Q_heater = heater.Q_flow;
      heater.T = Tl;
    
      xv = homotopy(if hv >= hvs then 1 else (hv - hls) / (hvs - hls), (hv - hls) / (hvs - hls)) "Steam quality in the vapour volume";
      xl = homotopy(if hl <= hls then 0 else (hl - hls) / (hvs - hls), 0) "Steam quality in the liquid volume";
      gml = if gl == 0 then 0 else 1 / (1 / gl + a * rint / lm) "Total Heat conductance metal-liquid";
      gmv = if gv == 0 then 0 else 1 / (1 / gv + a * rint / lm) "Total Heat conductance metal-vapour";
    
      a = rext ^ 2 / (rext ^ 2 - rint ^ 2) * log(rext / rint) - 0.5;
    // simplified to only vertical SG
      Vl = pi * rint ^ 2 * (y + L / 2) "Liquid volume";
      Aml = pi * rint ^ 2 + 2 * pi * rint * (y + L / 2) "Metal-liquid interface area";
      Asup = pi * rint ^ 2 "Liquid-vapour interface area";
    
    
      Vv = pi * rint ^ 2 * L - Vl "Vapour volume";
      Amv = 2 * pi * rint * L + 2 * pi * rint ^ 2 - Aml "Metal-vapour interface area";
      Aext = 2 * pi * rext ^ 2 + 2 * pi * rext * L "External metal surface area";
    
      level = y + L/2;
    // level from bottom
      level_centered = y;
    // Fluid properties
      liquidState = Medium.setState_phX(p, hl);
      Tl = Medium.temperature(liquidState);
      rhol = Medium.density(liquidState);
      vapourState = Medium.setState_phX(p, hv);
      Tv = Medium.temperature(vapourState);
      rhov = Medium.density(vapourState);
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      hls = Medium.bubbleEnthalpy(sat);
      hvs = Medium.dewEnthalpy(sat);
      Ts = sat.Tsat;
    // Boundary conditions
      feedwater.p = p;
      feedwater.m_flow = wf;
      feedwater.h_outflow = hl;
      hf = homotopy(if not allowFlowReversal then inStream(feedwater.h_outflow) else noEvent(actualStream(feedwater.h_outflow)), inStream(feedwater.h_outflow));
      
    
      steam.p = p;
      steam.m_flow = -wv;
      steam.h_outflow = hv;
      hvout = homotopy(if not allowFlowReversal then hv else noEvent(actualStream(steam.h_outflow)), hv);
      
      
    initial equation
      if initOpt == ThermoPower.Choices.Init.Options.noInit then
    // do nothing
      elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
        if not noInitialPressure then
          p = pstart;
        end if;
        hl = hlstart;
        hv = hvstart;
        y = ystart;
        if Cm > 0 and (gl > 0 or gv > 0) then
          Tm = Tmstart;
        end if;
    
      elseif initOpt == ThermoPower.Choices.Init.Options.steadyState then
        der(p) = 0;
        der(hl) = 0;
        der(hv) = 0;
        der(y) = 0;
        if Cm > 0 and (gl > 0 or gv > 0) then
          der(Tm) = 0;
        end if;
      elseif initOpt == ThermoPower.Choices.Init.Options.steadyStateNoP then
        der(hl) = 0;
        der(hv) = 0;
        der(y) = 0;
        if Cm > 0 and (gl > 0 or gv > 0) then
          der(Tm) = 0;
        end if;
      else
        assert(false, "Unsupported initialisation option");
      end if;
      annotation(
        Icon(graphics = {Text(extent = {{-150, 26}, {-78, 0}}, textString = "Feed"), Text(extent = {{-22, 100}, {50, 80}}, textString = "Steam"), Text(origin = {3, 12}, lineColor = {253, 0, 0}, extent = {{-49, 20}, {49, -20}}, textString = "non-equilibrium", textStyle = {TextStyle.Bold})}),
        Documentation(info = "<HTML>
    <p>This model describes the cylindrical drum of a drum boiler, without assuming thermodynamic equilibrium between the liquid and vapour holdups. Connectors are provided for feedwater inlet, steam outlet, downcomer outlet, riser inlet, and blowdown outlet.
    <p>The model is based on dynamic mass and energy balance equations of the liquid volume and vapour volume inside the drum. Mass and energy tranfer between the two phases is provided by bulk condensation and surface condensation of the vapour phase, and by bulk boiling of the liquid phase. Additional energy transfer can take place at the surface if the steam is superheated.
    <p>The riser flowrate is separated before entering the drum, at the vapour pressure. The (saturated) liquid fraction goes into the liquid volume; the (wet) vapour fraction goes into the vapour volume, vith a steam quality depending on the liquid/vapour density ratio and on the <tt>avr</tt> parameter.
    <p>The enthalpy of the liquid going to the downcomer is computed by assuming that a fraction of the total mass flowrate (<tt>afd</tt>) comes directly from the feedwater inlet. The pressure at the downcomer connector is equal to the vapour pressure plus the liquid head.
    <p>The metal wall dynamics is taken into account, assuming uniform temperature. Heat transfer takes place between the metal wall and the liquid phase, vapour phase, and external atmosphere, the corresponding heat transfer coefficients being <tt>gl</tt>, <tt>gv</tt>, and <tt>gext</tt>.
    <p>The drum level is referenced to the centreline.
    <p>The start values of drum pressure, liquid specific enthalpy, vapour specific enthalpy, and metal wall temperature can be specified by setting the parameters <tt>pstart</tt>, <tt>hlstart</tt>, <tt>hvstart</tt>, <tt>Tmstart</tt>
    <p><b>Modelling options</b></p>
    <p>The following options are available to specify the orientation of the cylindrical drum:
    <ul><li><tt>DrumOrientation = 0</tt>: horizontal axis.
    <li><tt>DrumOrientation = 1</tt>: vertical axis.
    </ul>
    </HTML>", revisions = "<html>
    <ul>
    <li><i>30 May 2005</i>
        by <a href=\"mailto:francesco.casella@polimi.it\">Francesco Casella</a>:<br>
           Initialisation support added.</li>
    <li><i>16 Dec 2004</i>
        by <a href=\"mailto:francesco.casella@polimi.it\">Francesco Casella</a>:<br>
           Standard medium definition added.</li>
    <li><i>5 Jul 2004</i>
        by <a href=\"mailto:francesco.casella@polimi.it\">Francesco Casella</a>:<br>
           Adapted to Modelica.Media.</li>
    <li><i>1 Feb 2004</i>
        by <a href=\"mailto:francesco.casella@polimi.it\">Francesco Casella</a>:<br>
           Improved equations for drum geometry.</li>
    <li><i>1 Oct 2003</i>
        by <a href=\"mailto:francesco.casella@polimi.it\">Francesco Casella</a>:<br>
           First release.</li>
    </ul>
        "),
        Diagram(graphics));
    
    end Drum_simplified;

    model PrescribedPressureCondenser
replaceable package Medium = Water.StandardWater constrainedby Modelica.Media.Interfaces.PartialMedium "Medium model";
      //Parameters
      parameter Modelica.Units.SI.Length D_ext;
      parameter Modelica.Units.SI.Length L;
      parameter Modelica.Units.SI.Volume V_heatex=210;
      parameter Integer N_tube;
      parameter Integer N_pass;
      parameter Modelica.Units.SI.Pressure p "Nominal inlet pressure";
      parameter Modelica.Units.SI.Volume Vtot = 0.85*abs(V_heatex-((D_ext^2)/4)*3.14*L*N_tube*N_pass) "Total volume of the fluid side";
      parameter Modelica.Units.SI.Volume Vlstart = 0.15*Vtot "Start value of the liquid water volume" annotation(
        Dialog(tab = "Initialisation"));
      parameter ThermoPower.Choices.Init.Options initOpt = system.initOpt "Initialisation option" annotation(
        Dialog(tab = "Initialisation"));
      outer System system "System object";
      //Variables
      Modelica.Units.SI.Density rhol "Density of saturated liquid";
      Modelica.Units.SI.Density rhov "Density of saturated steam";
      Medium.SaturationProperties sat "Saturation properties";
      Medium.SpecificEnthalpy hl "Specific enthalpy of saturated liquid";
      Medium.SpecificEnthalpy hv "Specific enthalpy of saturated vapour";
      Modelica.Units.SI.Mass M "Total mass, steam+liquid";
      Modelica.Units.SI.Mass Ml "Liquid mass";
      Modelica.Units.SI.Mass Mv "Steam mass";
      Modelica.Units.SI.Volume Vl(start = Vlstart) "Liquid volume";
      Modelica.Units.SI.Volume Vv "Steam volume";
      Modelica.Units.SI.Energy E "Internal energy";
      Modelica.Units.SI.Power Q "Thermal power";
      Modelica.Units.SI.Temperature Ts;
      //Connectors
      ThermoPower.Water.FlangeA steamIn(redeclare package Medium = Medium) annotation(
        Placement(transformation(extent = {{-20, 80}, {20, 120}}, rotation = 0)));
      ThermoPower.Water.FlangeB waterOut(redeclare package Medium = Medium) annotation(
        Placement(transformation(extent = {{-20, -120}, {20, -80}}, rotation = 0)));
    equation
      steamIn.p = p;
      steamIn.h_outflow = hl;
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      Ts = sat.Tsat;
      hl = Medium.bubbleEnthalpy(sat);
      hv = Medium.dewEnthalpy(sat);
      waterOut.p = p;
      waterOut.h_outflow = hl;
      rhol = Medium.bubbleDensity(sat);
      rhov = Medium.dewDensity(sat);
      Ml = Vl*rhol;
      Mv = Vv*rhov;
      Vtot = Vv + Vl;
      M = Ml + Mv;
      E = Ml*hl + Mv*inStream(steamIn.h_outflow) - p*Vtot;
    //Energy and Mass Balances
      der(M) = steamIn.m_flow + waterOut.m_flow;
      der(E) = steamIn.m_flow*hv + waterOut.m_flow*hl - Q;
    initial equation
      if initOpt == ThermoPower.Choices.Init.Options.noInit then
    // do nothing
      elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
        Vl = Vlstart;
      elseif initOpt == ThermoPower.Choices.Init.Options.steadyState then
        der(Vl) = 0;
      else
        assert(false, "Unsupported initialisation option");
      end if;
      annotation(
        Icon(graphics = {Ellipse(extent = {{-90, 100}, {90, -80}}, lineColor = {0, 0, 255}, lineThickness = 0.5, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid), Line(points = {{44, -40}, {-50, -40}, {8, 10}, {-50, 60}, {44, 60}}, color = {0, 0, 255}, thickness = 0.5), Rectangle(extent = {{-48, -66}, {48, -100}}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid), Text(extent = {{-100, -115}, {100, -145}}, lineColor = {85, 170, 255}, textString = "%name")}),
        Diagram(graphics),
        Documentation(revisions = "<html>
    <ul>
    <li><i>10 Dec 2008</i>
        by <a>Luca Savoldelli</a>:<br>
           First release.</li>
    </ul>
    </html>"));
    
    end PrescribedPressureCondenser;

    model System
// Assumptions
      parameter Boolean allowFlowReversal = true "= false to restrict to design flow direction (flangeA -> flangeB)" annotation(
        Evaluate = true,
        Dialog(group = "Simulation options"));
      parameter ThermoPower.Choices.Init.Options initOpt = ThermoPower.Choices.Init.Options.fixedState annotation(
        Dialog(group = "Simulation options"));
      // parameter ThermoPower.Choices.System.Dynamics Dynamics=ThermoPower.Choices.System.Dynamics.DynamicFreeInitial;
      parameter Modelica.Units.SI.Pressure p_amb = 101325 "Ambient pressure" annotation(
        Dialog(group = "Ambient conditions"));
      parameter Modelica.Units.SI.Temperature T_amb = 293.15 "Ambient Temperature (dry bulb)" annotation(
        Dialog(group = "Ambient conditions"));
      parameter Modelica.Units.SI.Temperature T_wb = 288.15 "Ambient temperature (wet bulb)" annotation(
        Dialog(group = "Ambient conditions"));
      parameter Modelica.Units.SI.Frequency fnom = 50 "Nominal grid frequency" annotation(
        Dialog(group = "Electrical system defaults"));
      parameter Modelica.Units.SI.Temperature T_ambient = T_amb "duplicate for compaibility with Modelica Fluid lib";
      parameter Modelica.Units.SI.AbsolutePressure p_ambient = p_amb "Default ambient pressure" annotation(
        Dialog(group = "Environment"));
      parameter Modelica.Units.SI.Acceleration g = Modelica.Constants.g_n "Constant gravity acceleration" annotation(
        Dialog(group = "Environment"));
      // Assumptions
      parameter Modelica.Fluid.Types.Dynamics energyDynamics = Modelica.Fluid.Types.Dynamics.DynamicFreeInitial "Default formulation of energy balances" annotation(
        Evaluate = true,
        Dialog(tab = "Assumptions", group = "Dynamics"));
      parameter Modelica.Fluid.Types.Dynamics massDynamics = energyDynamics "Default formulation of mass balances" annotation(
        Evaluate = true,
        Dialog(tab = "Assumptions", group = "Dynamics"));
      final parameter Modelica.Fluid.Types.Dynamics substanceDynamics = massDynamics "Default formulation of substance balances" annotation(
        Evaluate = true,
        Dialog(tab = "Assumptions", group = "Dynamics"));
      final parameter Modelica.Fluid.Types.Dynamics traceDynamics = massDynamics "Default formulation of trace substance balances" annotation(
        Evaluate = true,
        Dialog(tab = "Assumptions", group = "Dynamics"));
      parameter Modelica.Fluid.Types.Dynamics momentumDynamics = Modelica.Fluid.Types.Dynamics.SteadyState "Default formulation of momentum balances, if options available" annotation(
        Evaluate = true,
        Dialog(tab = "Assumptions", group = "Dynamics"));
      // Initialization
      parameter Modelica.Units.SI.MassFlowRate m_flow_start = 0 "Default start value for mass flow rates" annotation(
        Dialog(tab = "Initialization"));
      parameter Modelica.Units.SI.AbsolutePressure p_start = p_ambient "Default start value for pressures" annotation(
        Dialog(tab = "Initialization"));
      parameter Modelica.Units.SI.Temperature T_start = T_ambient "Default start value for temperatures" annotation(
        Dialog(tab = "Initialization"));
      // Advanced
      parameter Boolean use_eps_Re = false "= true to determine turbulent region automatically using Reynolds number" annotation(
        Evaluate = true,
        Dialog(tab = "Advanced"));
      parameter Modelica.Units.SI.MassFlowRate m_flow_nominal = if use_eps_Re then 1 else 1e2 * m_flow_small "Default nominal mass flow rate" annotation(
        Dialog(tab = "Advanced", enable = use_eps_Re));
      parameter Real eps_m_flow(min = 0) = 1e-4 "Regularization of zero flow for |m_flow| < eps_m_flow*m_flow_nominal" annotation(
        Dialog(tab = "Advanced", enable = use_eps_Re));
      parameter Modelica.Units.SI.AbsolutePressure dp_small(min = 0) = 1 "Default small pressure drop for regularization of laminar and zero flow" annotation(
        Dialog(tab = "Advanced", group = "Classic", enable = not use_eps_Re));
      parameter Modelica.Units.SI.MassFlowRate m_flow_small(min = 0) = 1e-2 "Default small mass flow rate for regularization of laminar and zero flow" annotation(
        Dialog(tab = "Advanced", group = "Classic", enable = not use_eps_Re));
      annotation(
        defaultComponentName = "system",
        defaultComponentPrefixes = "inner",
        missingInnerMessage = "The System object is missing, please drag it on the top layer of your model",
        Icon(graphics = {Polygon(points = {{-100, 60}, {-60, 100}, {60, 100}, {100, 60}, {100, -60}, {60, -100}, {-60, -100}, {-100, -60}, {-100, 60}}, lineColor = {0, 0, 255}, smooth = Smooth.None, fillColor = {170, 213, 255}, fillPattern = FillPattern.Solid), Text(extent = {{-80, 40}, {80, -20}}, lineColor = {0, 0, 255}, textString = "system")}),
        experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-06, Interval = 0.002));
    end System;

    model DrumReverse
    extends ThermoPower.Icons.Water.Drum;
      replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
        Modelica.Media.Interfaces.PartialTwoPhaseMedium "Medium model"
        annotation(choicesAllMatching = true);
      parameter Modelica.Units.SI.Volume Vd "Drum internal volume";
      parameter Modelica.Units.SI.Mass Mm "Drum metal mass";
      parameter Medium.SpecificHeatCapacity cm
        "Specific heat capacity of the metal";
      parameter Boolean allowFlowReversal=system.allowFlowReversal
        "= true to allow flow reversal, false restricts to design direction"
        annotation(Evaluate=true);
      outer ThermoPower.System system "System wide properties";
      parameter Modelica.Units.SI.Pressure pstart "Pressure start value"
        annotation (Dialog(tab="Initialisation"));
      parameter Modelica.Units.SI.Volume Vlstart "Start value of drum water volume"
        annotation (Dialog(tab="Initialisation"));
      parameter ThermoPower.Choices.Init.Options initOpt=system.initOpt
        "Initialisation option"
        annotation (Dialog(tab="Initialisation"));
      parameter Boolean noInitialPressure=false
        "Remove initial equation on pressure"
        annotation (Dialog(tab="Initialisation"),choices(checkBox=true));
    
      Medium.SaturationProperties sat "Saturation conditions";
      ThermoPower.Water.FlangeA Steam(redeclare package Medium = Medium, m_flow(min=if
              allowFlowReversal then -Modelica.Constants.inf else 0)) annotation (
         Placement(transformation(extent={{-110,-64},{-70,-24}}, rotation=0)));
      ThermoPower.Water.FlangeB Condensate(redeclare package Medium = Medium, m_flow(max=if
              allowFlowReversal then +Modelica.Constants.inf else 0)) annotation (
         Placement(transformation(extent={{48,52},{88,92}}, rotation=0)));
      Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a wall
        "Metal wall thermal port" annotation (Placement(transformation(extent={{-28,
                -100},{28,-80}}, rotation=0)));
    
      Modelica.Units.SI.Mass Ml "Liquid water mass";
      Modelica.Units.SI.Mass Mv "Steam mass";
      Modelica.Units.SI.Mass M "Total liquid+steam mass";
      Modelica.Units.SI.Energy E "Total energy";
      Modelica.Units.SI.Volume Vv(start = Vd - Vlstart) "Steam volume";
      Modelica.Units.SI.Volume Vl(
        start = Vlstart,
        stateSelect=StateSelect.prefer)
        "Liquid water total volume";
      Medium.AbsolutePressure p(
        start=pstart,
        stateSelect=StateSelect.prefer)
        "Drum pressure";
      Medium.MassFlowRate qc "Condensate mass flowrate";
      Medium.MassFlowRate qs "Steam mass flowrate";
      Modelica.Units.SI.HeatFlowRate Q "Heat flow to the risers";
      Medium.SpecificEnthalpy hs "Steam specific enthalpy";
      Medium.SpecificEnthalpy hl "Specific enthalpy of saturated liquid";
      Medium.SpecificEnthalpy hv "Specific enthalpy of saturated steam";
      Medium.Temperature Ts "Saturation temperature";
      Modelica.Units.SI.Density rhol "Density of saturated liquid";
      Modelica.Units.SI.Density rhov "Density of saturated steam";
    equation
      Ml = Vl*rhol "Mass of liquid";
      Mv = Vv*rhov "Mass of vapour";
      M = Ml + Mv "Total mass";
      E = Ml*hl + Mv*hv - p*Vd + Mm*cm*Ts "Total energy";
      Ts = sat.Tsat "Saturation temperature";
      der(M) = qs - qc "Mass balance";
      der(E) = Q + qs*hs - qc*hl "Energy balance";
      Vl + Vv = Vd "Total volume";
    
      // Boundary conditions
      p = Steam.p;
      p = Condensate.p;
      if not allowFlowReversal then
        hs = inStream(Steam.h_outflow);
      else
        // assume flow is entering during simplified initialization
        hs = homotopy(
          actualStream(Steam.h_outflow),
          inStream(Steam.h_outflow));
      end if;
      -Condensate.m_flow = qc;
      Steam.m_flow = qs;
      Condensate.h_outflow = hl;
      Steam.h_outflow = hv;
      Q = wall.Q_flow;
      wall.T = Ts;
    
      // Fluid properties
      sat.psat = p;
      sat.Tsat = Medium.saturationTemperature(p);
      rhol = Medium.bubbleDensity(sat);
      rhov = Medium.dewDensity(sat);
      hl = Medium.bubbleEnthalpy(sat);
      hv = Medium.dewEnthalpy(sat);
    initial equation
      if initOpt == ThermoPower.Choices.Init.Options.noInit then
        // do nothing
      elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
        if not noInitialPressure then
          p = pstart;
        end if;
        Vl = Vlstart;
      elseif initOpt == ThermoPower.Choices.Init.Options.steadyState then
        if not noInitialPressure then
          der(p) = 0;
        end if;
        der(Vl) = 0;
      else
        assert(false, "Unsupported initialisation option");
      end if;
      annotation (Icon(graphics = {Text(extent = {{-150, 26}, {-78, 0}}, textString = "Feed"), Text(extent = {{-22, 100}, {50, 80}}, textString = "Steam"), Text(origin = {3, 12}, lineColor = {253, 0, 0}, extent = {{-49, 20}, {49, -20}}, textString = "non-equilibrium", textStyle = {TextStyle.Bold})}),
        Documentation(info="<HTML>
    Reverse model of the simplified Drum Equilibrium from Thermopower. This model assumes
    
    Thermodynamic equilibrium between the liquid, vapour, and metal wall
    Perfect separation of the liquid and vapour phase
    he model has two state variables the pressure <code>p</code> and the liquid volume <code>Vl</code>. It is possible to extend it,
    adding a specific geometry and the computation of the level from the liquid volume. In that case, one may want to use the level as a state.
    "));
    end DrumReverse;
  end Water;


end BOP_TBL;
