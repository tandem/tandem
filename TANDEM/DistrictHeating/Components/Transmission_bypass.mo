within TANDEM.DistrictHeating.Components;
model Transmission_bypass
  parameter Integer Npipe = 10 "Number of axial volumes for the pipelines" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Length L = 8750 "Pipe length" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Length D = 0.4 "Pipe diameter" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.MassFlowRate mdot = 238.8344877 "Nominal flow rate" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Temperature T = 80 + 273.15 "Nominal average temperature" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Pressure p = 10e5 "Nominal pressure" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Pressure dp = 5.185875549e5 "Nominal pressure drop" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.ThermalConductance G = 3651.239656 "Global conductance for heat losses" annotation (
    Dialog(group = "Pipelines"));
  parameter Modelica.Units.SI.Temperature Tg = 10 + 273.15 "Ground temperature" annotation (
    Dialog(group = "Pipelines"));
  ThermoPower.Water.FlangeA flangeA annotation (
    Placement(transformation(extent = {{-130, -44}, {-110, -24}}), iconTransformation(extent = {{-130, -44}, {-110, -24}})));
  ThermoPower.Water.FlangeB flangeB annotation (
    Placement(transformation(extent = {{-130, 24}, {-110, 44}}), iconTransformation(extent = {{-130, 24}, {-110, 44}})));
  ThermoPower.Water.FlangeB flangeB1 annotation (
    Placement(transformation(extent = {{110, -44}, {130, -24}}), iconTransformation(extent = {{110, -44}, {130, -24}})));
  ThermoPower.Water.FlangeA flangeA1 annotation (
    Placement(transformation(extent = {{110, 24}, {130, 44}}), iconTransformation(extent = {{110, 24}, {130, 44}})));
  ThermoPower.Water.Flow1DFV hotLeg(N = Npipe + 1, L = L, A = pi*(D/2)^2, omega = pi*D, Dhyd = D, wnom = mdot, FFtype = ThermoPower.Choices.Flow1D.FFtypes.Colebrook, dpnom = 1642000, rhonom(displayUnit = "kg/m3") = ThermoPower.Water.StandardWater.density_pT(p, T, 1), Cfnom = 0.003188942, e = 0.0001, pstart = 750000, hstartin = 504136.916226, hstartout = 504136.916226, fixedMassFlowSimplified = true, redeclare
      model                                                                                                                                                                                             HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantThermalConductance (                                                                                                                                                                                                        UA = G)) annotation (
    Placement(transformation(extent = {{-4, -50}, {16, -30}})));
  ThermoPower.Water.Flow1DFV coldLeg(N = Npipe + 1, L = L, A = pi*(D/2)^2, omega = pi*D, Dhyd = D, wnom = mdot, FFtype = ThermoPower.Choices.Flow1D.FFtypes.Colebrook, dpnom = 1592000, rhonom(displayUnit = "kg/m3") = ThermoPower.Water.StandardWater.density_pT(p, T, 1), Cfnom = 0.003188942, e = 0.0001, pstart = 750000, hstartin = 335468.05673263, hstartout = 335468.05673263, fixedMassFlowSimplified = true, redeclare
      model                                                                                                                                                                                             HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantThermalConductance (                                                                                                                                                                                                        UA = G)) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin = {6, 40})));
  ThermoPower.Water.SensT1 sensT1_1 annotation (
    Placement(transformation(extent = {{72, 30}, {60, 42}})));
  ThermoPower.Water.SensT1 sensT1_2 annotation (
    Placement(transformation(extent = {{-84, -36}, {-74, -26}})));
  ThermoPower.Thermal.TempSource1DFV tempSource1DFV(Nw = Npipe) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 270, origin = {24, 0})));
  Modelica.Blocks.Sources.RealExpression groundT(y = Tg) annotation (
    Placement(transformation(extent = {{54, -8}, {38, 8}})));
  ThermoPower.Water.SensP sensP annotation (
    Placement(transformation(extent = {{-60, 36}, {-48, 48}})));
  ThermoPower.Water.Pump pump(redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (                                                             q_nom = {0, 0.8484447055, 1.5115449}, head_nom = {273.7133968, 187.47492934, 0}), dp0 = 1735000, hstart = 504136.916226, n0 = 1500, rho0(displayUnit = "kg/m3") = 943.3813214774999, use_in_n = false, w0 = 800.41) annotation (
    Placement(transformation(extent = {{-54, -46}, {-34, -26}})));
  ThermoPower.Water.Pump pump1(redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (                                                              q_nom = {0, 0.8233853446, 1.46690046}, head_nom = {260.27058334, 178.267522836, 0}), rho0(displayUnit = "kg/m3") = 972.0927057, n0 = 1500, hstart = 335468.05673263, w0 = 800.41, dp0 = 1700000, use_in_n = false) annotation (
    Placement(transformation(extent = {{50, 20}, {28, 42}})));
  ThermoPower.Water.FlowJoin flowJoin1 annotation (
    Placement(transformation(extent = {{-6, -6}, {6, 6}}, rotation = 180, origin = {80, 32})));
  ThermoPower.Water.ValveLin valveLin2(Kv = 0.006067850537465611) annotation (
    Placement(transformation(extent = {{6, 6}, {-6, -6}}, rotation = 180, origin = {98, -42})));
  ThermoPower.Water.FlowSplit flowSplit1 annotation (
    Placement(transformation(extent = {{-6, -6}, {6, 6}}, rotation = 0, origin = {80, -40})));
  ThermoPower.Water.ValveLin valveLin3(Kv = 0.006067850537465611) annotation (
    Placement(transformation(extent = {{-6, 6}, {6, -6}}, rotation = 90, origin = {84, 6})));
  ThermoPower.Thermal.MetalTubeFV metalTubeFV(Nw = Npipe, L = L, rint = 0.6/2, rext = 0.8/2, rhomcm = 1.82e6, lambda = 0.417284532, WallRes = false) annotation (
    Placement(transformation(extent = {{-4, 18}, {14, 36}})));
  ThermoPower.Thermal.MetalTubeFV metalTubeFV1(Nw = Npipe, L = L, rint = 0.6/2, rext = 0.8/2, rhomcm = 1.82e6, lambda = 0.417284532, WallRes = false) annotation (
    Placement(transformation(extent = {{-9, -9}, {9, 9}}, rotation = 180, origin = {5, -23})));
  Control.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,70},{-50,90}})));
  Control.SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,70},{70,90}})));
protected
  constant Real pi = Modelica.Constants.pi;
equation
  connect(flangeB, coldLeg.outfl) annotation (
    Line(points = {{-120, 34}, {-100, 34}, {-100, 40}, {-4, 40}}, color = {0, 0, 255}));
  connect(groundT.y, tempSource1DFV.temperature) annotation (
    Line(points = {{37.2, 0}, {32.6, 0}, {32.6, -6.66134e-16}, {28, -6.66134e-16}}, color = {0, 0, 127}));
  connect(flangeB, sensP.flange) annotation (
    Line(points = {{-120, 34}, {-100, 34}, {-100, 39.6}, {-54, 39.6}}, color = {0, 0, 255}));
  connect(pump.outfl, hotLeg.infl) annotation (
    Line(points = {{-38, -29}, {-38, -28}, {-22, -28}, {-22, -40}, {-4, -40}}, color = {0, 0, 255}));
  connect(pump1.outfl, coldLeg.infl) annotation (
    Line(points = {{32.4, 38.7}, {32.4, 40}, {16, 40}}, color = {0, 0, 255}));
  connect(flowSplit1.out2, valveLin2.inlet) annotation (
    Line(points = {{83.6, -42.4}, {92, -42.4}, {92, -42}}, color = {0, 0, 255}));
  connect(valveLin3.inlet, flowSplit1.out1) annotation (
    Line(points = {{84, 0}, {84, -37.6}, {83.6, -37.6}}, color = {0, 0, 255}));
  connect(hotLeg.outfl, flowSplit1.in1) annotation (
    Line(points = {{16, -40}, {76.4, -40}}, color = {0, 0, 255}));
  connect(valveLin2.outlet, flangeB1) annotation (
    Line(points = {{104, -42}, {108, -42}, {108, -34}, {120, -34}}, color = {0, 0, 255}));
  connect(tempSource1DFV.wall, metalTubeFV.ext) annotation (
    Line(points = {{21, 0}, {6, 0}, {6, 2}, {5, 2}, {5, 24.21}}, color = {255, 127, 0}));
  connect(metalTubeFV1.ext, metalTubeFV.ext) annotation (
    Line(points = {{5, -20.21}, {5, 0}, {6, 0}, {6, 2}, {5, 2}, {5, 24.21}}, color = {255, 127, 0}));
  connect(hotLeg.wall, metalTubeFV1.int) annotation (
    Line(points = {{6, -35}, {6, -30.35}, {5, -30.35}, {5, -25.7}}, color = {255, 127, 0}));
  connect(coldLeg.wall, metalTubeFV.int) annotation (
    Line(points = {{6, 35}, {6, 29.7}, {5, 29.7}}, color = {255, 127, 0}));
  connect(flangeB, flangeB) annotation (
    Line(points = {{-120, 34}, {-120, 34}}, color = {0, 0, 255}));
  connect(sensT1_2.flange, pump.infl) annotation (
    Line(points = {{-79, -33}, {-54, -33}, {-54, -34}, {-52, -34}}, color = {0, 0, 255}));
  connect(sensT1_2.flange, flangeA) annotation (
    Line(points = {{-79, -33}, {-120, -33}, {-120, -34}}, color = {0, 0, 255}));
  connect(flowJoin1.in1, valveLin3.outlet) annotation (
    Line(points = {{83.6, 29.6}, {83.6, 12}, {84, 12}}, color = {0, 0, 255}));
  connect(flowJoin1.in2, flangeA1) annotation (
    Line(points = {{83.6, 34.4}, {92, 34.4}, {92, 34}, {120, 34}}, color = {0, 0, 255}));
  connect(pump1.infl, sensT1_1.flange) annotation (
    Line(points = {{47.8, 33.2}, {66, 33.2}, {66, 33.6}}, color = {0, 0, 255}));
  connect(sensT1_1.flange, flowJoin1.out) annotation (
    Line(points = {{66, 33.6}, {66, 32}, {76.4, 32}}, color = {0, 0, 255}));
  connect(actuatorBus.Kv_DN, valveLin2.cmd) annotation (
    Line(points = {{-60, 80}, {-60, 58}, {98, 58}, {98, -37.2}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.Kv_bp, valveLin3.cmd) annotation (
    Line(points = {{-60, 80}, {-60, 58}, {98, 58}, {98, 6}, {88.8, 6}}, color = {80, 200, 120}, thickness = 0.5));
  connect(sensorBus.T_feed, sensT1_2.T) annotation (
    Line(points = {{60, 80}, {60, 68}, {-75, 68}, {-75, -28}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Tret, sensT1_1.T) annotation (
    Line(points = {{60, 80}, {60, 39.6}, {61.2, 39.6}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.pIHX, sensP.p) annotation (
    Line(points = {{60, 80}, {60, 68}, {-46, 68}, {-46, 45.6}, {-49.2, 45.6}}, color = {255, 219, 88}, thickness = 0.5));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent={{-120,-80},{120,
            80}}),                                                                         graphics={  Rectangle(extent = {{-120, 80}, {120, -80}}, lineColor = {244, 125, 35}, lineThickness = 1, fillColor = {244, 125, 35}, fillPattern = FillPattern.Solid), Rectangle(extent = {{-116, 76}, {116, -76}}, lineColor = {244, 125, 35}, lineThickness = 1, fillColor={215,215,
              215},                                                                                                                                                                                                        fillPattern=
              FillPattern.Solid),                                                                                                                                                                                                        Text(extent = {{-54, 36}, {64, -38}}, textColor = {244, 125, 35}, textString = "TN")}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent={{-120,-80},{
            120,80}})));
end Transmission_bypass;
