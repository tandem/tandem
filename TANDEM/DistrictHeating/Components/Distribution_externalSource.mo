within TANDEM.DistrictHeating.Components;
model Distribution_externalSource
  outer ThermoPower.System system "System wide properties";
  parameter Modelica.Units.SI.Volume V = 50186 "Network volume";
  parameter Integer Nhs=1 "Number of external heat sources to be connected";
  parameter ThermoPower.Choices.FluidPhase.FluidPhases FluidPhaseStart = ThermoPower.Choices.FluidPhase.FluidPhases.Liquid "Fluid phase (only for initialization!)" annotation (
    Dialog(tab = "Initialisation"));
  parameter Modelica.Units.SI.Pressure pstart "Pressure start value" annotation (
    Dialog(tab = "Initialisation"));
  parameter Modelica.Units.SI.SpecificEnthalpy hstart = if FluidPhaseStart == ThermoPower.Choices.FluidPhase.FluidPhases.Liquid then 1e5 else if FluidPhaseStart == ThermoPower.Choices.FluidPhase.FluidPhases.Steam then 3e6 else 1e6 "Specific enthalpy start value" annotation (
    Dialog(tab = "Initialisation"));
  parameter ThermoPower.Choices.Init.Options initOpt = system.initOpt "Initialisation option" annotation (
    Dialog(tab = "Initialisation"));
  parameter Boolean noInitialPressure = false "Remove initial equation on pressure" annotation (
    Dialog(tab = "Initialisation"),
    choices(checkBox = true));
  parameter Boolean noInitialEnthalpy = false "Remove initial equation on enthalpy" annotation (
    Dialog(tab = "Initialisation"),
    choices(checkBox = true));
  BaseClasses.Inertia inertia(
    Nhs=Nhs,                                         FluidPhaseStart = FluidPhaseStart, V = V, hstart = hstart,
    initOpt=initOpt,                                                                                            noInitialEnthalpy = noInitialEnthalpy, noInitialPressure = noInitialPressure, pstart = pstart) annotation (
    Placement(transformation(extent = {{-28, -28}, {28, 28}})));
  ThermoPower.Water.FlangeB flangeB annotation (
    Placement(transformation(extent = {{110, 40}, {130, 60}}), iconTransformation(extent = {{110, 40}, {130, 60}})));
  ThermoPower.Water.FlangeA flangeA annotation (
    Placement(transformation(extent = {{112, -60}, {132, -40}}), iconTransformation(extent = {{112, -60}, {132, -40}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Consumer annotation (
    Placement(transformation(extent = {{-78, -10}, {-58, 10}})));
  ThermoPower.Water.SensT1 T_out annotation (
    Placement(transformation(extent = {{110, 44}, {90, 64}})));
  ThermoPower.Water.SensT1 T_in annotation (
    Placement(transformation(extent = {{110, -56}, {90, -36}})));
  ThermoPower.Water.SinkPressure sinkPressure(R = 100, h = hstart, p0 = pstart) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin = {-68, -30})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Trade annotation (
    Placement(transformation(extent = {{-78, 50}, {-58, 70}})));
  Control.SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,90},{70,110}})));
  Control.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,90},{-50,110}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y = inertia.dE_dt) annotation (
    Placement(transformation(extent = {{-28, 80}, {-8, 100}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a HeatSources[Nhs]
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
equation
  connect(Consumer.port, inertia.HeatUser) annotation (
    Line(points = {{-58, 0}, {-41, 0}, {-41, 3.55271e-15}, {-26.32, 3.55271e-15}}, color = {191, 0, 0}));
  connect(flangeA, flangeA) annotation (
    Line(points = {{122, -50}, {122, -50}}, color = {0, 0, 255}));
  connect(T_out.flange, flangeB) annotation (
    Line(points = {{100, 50}, {120, 50}}, color = {0, 0, 255}));
  connect(inertia.inlet, T_in.flange) annotation (
    Line(points = {{22.4, -11.2}, {40, -11.2}, {40, -50}, {100, -50}}, color = {0, 0, 255}));
  connect(T_in.flange, flangeA) annotation (
    Line(points = {{100, -50}, {122, -50}}, color = {0, 0, 255}));
  connect(inertia.relief, sinkPressure.flange) annotation (
    Line(points = {{-17.36, -16.8}, {-18, -16.8}, {-18, -30}, {-58, -30}}, color = {0, 0, 255}));
  connect(Trade.port, inertia.Qexch) annotation (
    Line(points = {{-58, 60}, {0, 60}, {0, 42}, {3.55271e-15, 42}, {3.55271e-15, 24.64}}, color = {191, 0, 0}));
  connect(sensorBus.T_DN, inertia.T_DN) annotation (
    Line(points = {{60, 100}, {60, 90}, {10.64, 90}, {10.64, 21.84}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.T_DNout, T_out.T) annotation (
    Line(points = {{60, 100}, {60, 90}, {86, 90}, {86, 60}, {92, 60}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.T_DNin, T_in.T) annotation (
    Line(points = {{60, 100}, {60, 90}, {86, 90}, {86, -40}, {92, -40}}, color = {255, 219, 88}, thickness = 0.5));
  connect(inertia.outlet, T_out.flange) annotation (
    Line(points = {{22.4, 10.64}, {40, 10.64}, {40, 50}, {100, 50}}, color = {0, 0, 255}));
  connect(actuatorBus.Qtrade, Trade.Q_flow) annotation (
    Line(points = {{-60, 100}, {-60, 78}, {-98, 78}, {-98, 60}, {-78, 60}}, color = {80, 200, 120}, thickness = 0.5));
  connect(actuatorBus.Qdemand, Consumer.Q_flow) annotation (
    Line(points = {{-60, 100}, {-60, 78}, {-98, 78}, {-98, 0}, {-78, 0}}, color = {80, 200, 120}, thickness = 0.5));
  connect(sensorBus.Qtrans, inertia.Q_trans) annotation (
    Line(points = {{60, 100}, {60, 90}, {18, 90}, {18, 18.48}, {17.92, 18.48}}, color = {255, 219, 88}, thickness = 0.5));
  connect(sensorBus.Qacc, realExpression.y) annotation (
    Line(points = {{60, 100}, {60, 90}, {-7, 90}}, color = {255, 219, 88}, thickness = 0.5));
  connect(inertia.HeatSource, HeatSources)
    annotation (Line(points={{0,-26.32},{0,-100}}, color={191,0,0}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent={{-120,-100},{120,
            100}}),                                                                          graphics={  Rectangle(extent = {{-120, 100}, {120, -100}}, lineColor = {244, 125, 35}, lineThickness = 1, fillColor = {244, 125, 35}, fillPattern = FillPattern.Solid), Rectangle(extent = {{-116, 96}, {116, -96}}, lineColor = {244, 125, 35}, lineThickness = 1, fillColor={215,215,
              215},                                                                                                                                                                                                        fillPattern
            = FillPattern.Solid),                                                                                                                                                                                                        Text(extent = {{-56, 38}, {62, -36}}, textColor = {244, 125, 35}, textString = "DN")}),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent={{-120,-100},{
            120,100}})));
end Distribution_externalSource;
