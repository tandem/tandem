within TANDEM.DistrictHeating.Components;
package BaseClasses
  model Inertia
    replaceable package Medium = ThermoPower.Water.StandardWater constrainedby
      Modelica.Media.Interfaces.PartialMedium "Medium model"
      annotation(choicesAllMatching = true);
    Medium.ThermodynamicState fluidState "Thermodynamic state of the fluid";

    parameter Integer Nhs = 1;
    parameter Modelica.Units.SI.Volume V "Inner volume";
    parameter Boolean allowFlowReversal=system.allowFlowReversal
      "= true to allow flow reversal, false restricts to design direction"
      annotation(Evaluate=true);


    outer ThermoPower.System system "System wide properties";
    parameter ThermoPower.Choices.FluidPhase.FluidPhases FluidPhaseStart=ThermoPower.Choices.FluidPhase.FluidPhases.Liquid
      "Fluid phase (only for initialization!)"
      annotation (Dialog(tab="Initialisation"));
    parameter Medium.AbsolutePressure pstart "Pressure start value"
      annotation (Dialog(tab="Initialisation"));
    parameter Medium.SpecificEnthalpy hstart=if FluidPhaseStart == ThermoPower.Choices.FluidPhase.FluidPhases.Liquid
         then 1e5 else if FluidPhaseStart == ThermoPower.Choices.FluidPhase.FluidPhases.Steam
         then 3e6 else 1e6 "Specific enthalpy start value"
      annotation (Dialog(tab="Initialisation"));
    parameter ThermoPower.Choices.Init.Options initOpt=system.initOpt
      "Initialisation option"
      annotation (Dialog(tab="Initialisation"));
    parameter Boolean noInitialPressure=false
      "Remove initial equation on pressure"
      annotation (Dialog(tab="Initialisation"),choices(checkBox=true));
    parameter Boolean noInitialEnthalpy=false
      "Remove initial equation on enthalpy"
      annotation (Dialog(tab="Initialisation"),choices(checkBox=true));

      Medium.AbsolutePressure p(start=pstart, stateSelect=if Medium.singleState then StateSelect.avoid
           else StateSelect.prefer) "Fluid pressure at the outlet";
    Medium.SpecificEnthalpy h(start=hstart, stateSelect=StateSelect.prefer)
      "Fluid specific enthalpy";
    Medium.SpecificEnthalpy hi "Inlet specific enthalpy";
    Medium.SpecificEnthalpy ho "Outlet specific enthalpy";
    Modelica.Units.SI.Mass M "Fluid mass";
    Modelica.Units.SI.Energy E "Fluid energy";
    Modelica.Units.SI.Power Qhu "Heat user thermal power";
    Modelica.Units.SI.Power Qtrans "Heat user thermal power";
    Modelica.Units.SI.Power Qexchanged "Heat user thermal power";
    Modelica.Units.SI.Power Qhs[Nhs] "Heat user thermal power";
    Medium.Temperature T "Fluid temperature";
    Modelica.Units.SI.Time Tr "Residence time";
    Real dM_dt;
    Real dE_dt;

    ThermoPower.Water.FlangeA inlet
      annotation (Placement(transformation(extent={{70,-50},{90,-30}}),
          iconTransformation(extent={{70,-50},{90,-30}})));
    ThermoPower.Water.FlangeB outlet
      annotation (Placement(transformation(extent={{70,28},{90,48}}),
          iconTransformation(extent={{70,28},{90,48}})));
    ThermoPower.Thermal.HT HeatUser
      annotation (Placement(transformation(extent={{-104,-10},{-84,10}}),
          iconTransformation(extent={{-104,-10},{-84,10}})));
    ThermoPower.Thermal.HT HeatSource[Nhs]
      annotation (Placement(transformation(extent={{-10,-104},{10,-84}}),
          iconTransformation(extent={{-10,-104},{10,-84}})));
    Modelica.Blocks.Interfaces.RealOutput T_DN annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={40,104}), iconTransformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={38,78})));

    ThermoPower.Water.FlangeB relief annotation (Placement(transformation(extent={
              {-68,-66},{-48,-46}}), iconTransformation(extent={{-72,-70},{-52,-50}})));

    ThermoPower.Thermal.HT Qexch annotation (Placement(transformation(extent={{-10,
              70},{10,90}}), iconTransformation(extent={{-10,78},{10,98}})));
    Modelica.Blocks.Interfaces.RealOutput Q_trans annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={80,104}), iconTransformation(
          extent={{-10,-10},{10,10}},
          rotation=90,
          origin={64,66})));
  equation

    // Set fluid properties
    fluidState = Medium.setState_phX(p, h);
    T = Medium.temperature(fluidState);

    M = V*Medium.density(fluidState) "Fluid mass";
    E = M*h - p*V "Fluid energy";
    dM_dt = V*(Medium.density_derp_h(fluidState)*der(p) + Medium.density_derh_p(
      fluidState)*der(h));
    dE_dt = h*dM_dt + M*der(h) - V*der(p);

    dM_dt = inlet.m_flow + outlet.m_flow + relief.m_flow "Fluid mass balance";
    dE_dt = Qtrans + Qhu + sum(Qhs) + relief.m_flow*h + Qexchanged
      "Fluid energy balance";


    // Boundary conditions
    T_DN = T;

    hi = homotopy(if not allowFlowReversal then inStream(inlet.h_outflow) else
      actualStream(inlet.h_outflow), inStream(inlet.h_outflow));
    ho = homotopy(if not allowFlowReversal then h else actualStream(outlet.h_outflow),
      h);
    inlet.h_outflow = h;
    outlet.h_outflow = h;
    inlet.p = p;
    outlet.p = p;

    relief.p = p;
    relief.h_outflow = h;

    Qhu = HeatUser.Q_flow;
    HeatUser.T = T;

    Qtrans = inlet.m_flow*hi + outlet.m_flow*ho;
    Qtrans = Q_trans;

    Qexchanged = Qexch.Q_flow;
    Qexch.T = T;

    for i in 1:Nhs loop
      Qhs[i] = HeatSource[i].Q_flow;
      HeatSource[i].T = T;
    end for;

    Tr = noEvent(M/max(abs(inlet.m_flow), Modelica.Constants.eps))
      "Residence time";
  initial equation
    // Initial conditions
    if initOpt == ThermoPower.Choices.Init.Options.noInit then
      // do nothing
    elseif initOpt == ThermoPower.Choices.Init.Options.fixedState then
      if not noInitialPressure then
        p = pstart;
      end if;
      if not noInitialEnthalpy then
        h = hstart;
      end if;
    elseif initOpt == ThermoPower.Choices.Init.Options.steadyState then
      if not noInitialEnthalpy then
        der(h) = 0;
      end if;
      if (not Medium.singleState and not noInitialPressure) then
        der(p) = 0;
      end if;
    elseif initOpt == ThermoPower.Choices.Init.Options.steadyStateNoP then
      if not noInitialEnthalpy then
        der(h) = 0;
      end if;
    else
      assert(false, "Unsupported initialisation option");
    end if;

    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
            Ellipse(
            extent={{-88,88},{88,-88}},
            lineColor={0,0,0},
            lineThickness=0.5,
            fillColor={0,0,255},
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=180), Ellipse(
            extent={{-88,-88},{88,88}},
            lineColor={0,0,0},
            lineThickness=0.5,
            fillColor={192,220,255},
            fillPattern=FillPattern.Sphere,
            startAngle=0,
            endAngle=180)}),                                       Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end Inertia;
end BaseClasses;
