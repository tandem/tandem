within TANDEM.DistrictHeating.Control;
model DHNcontroller_PID
  Modelica.Blocks.Interfaces.RealInput HeatDemand annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={-80,108}), iconTransformation(
        extent={{-4,-4},{20,20}},
        rotation=0,
        origin={-108,-18})));
  Modelica.Blocks.Interfaces.RealInput HeatSupply annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={-40,108}),
                         iconTransformation(
        extent={{-4,-4},{20,20}},
        rotation=0,
        origin={-108,28})));
  Modelica.Blocks.Interfaces.RealInput HeatTrade annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,108}),  iconTransformation(
        extent={{-6.85739,41.1428},{17.1435,17.1428}},
        rotation=0,
        origin={-104.857,51.143})));
  SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  Modelica.Blocks.Math.Gain gain1(k=-1)
    annotation (Placement(transformation(extent={{-7,-7},{7,7}},
        rotation=270,
        origin={-79,65})));
  Modelica.Blocks.Math.Add add1
    annotation (Placement(transformation(extent={{-86,-62},{-78,-54}})));
  Modelica.Blocks.Math.Add add5(k2=-1)
    annotation (Placement(transformation(extent={{-72,-54},{-64,-46}})));
  Modelica.Blocks.Sources.Constant const(k=1)
    annotation (Placement(transformation(extent={{-100,-34},{-92,-26}})));
  Modelica.Blocks.Sources.Constant const3(k=-0.01)
    annotation (Placement(transformation(extent={{-12,6},{-20,-2}})));
  Modelica.Blocks.Math.Add add2
    annotation (Placement(transformation(extent={{-26,-8},{-34,-16}})));
  Modelica.Blocks.Continuous.LimPID PID2(
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=83.34,
    Ti=83.34/0.00975,
    yMax=0,
    yMin=-0.95,
    y_start=0)
    annotation (Placement(transformation(extent={{-2,-8},{-12,-18}})));
  Modelica.Blocks.Sources.Constant const4(k=0)
    annotation (Placement(transformation(extent={{20,-10},{14,-16}})));
  Modelica.Blocks.Math.Add add3(k2=-1)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=180,
        origin={32,4})));
  Modelica.Blocks.Math.Gain gain2(k=1/(40 + 273.15))
    annotation (Placement(transformation(extent={{20,10},{8,-2}})));
  Modelica.Blocks.Interfaces.RealInput Tset_ret annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=180,
        origin={110,40}), iconTransformation(
        extent={{-6.85739,41.1428},{17.1435,17.1428}},
        rotation=0,
        origin={-104.857,-90.857})));
equation
  connect(actuatorBus.Qsupply, HeatSupply) annotation (Line(
      points={{-60,-100},{-60,22},{-40,22},{-40,108}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(actuatorBus.Qtrade, HeatTrade) annotation (Line(
      points={{-60,-100},{-60,22},{0,22},{0,108}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(HeatDemand, gain1.u) annotation (Line(points={{-80,108},{-80,90},{
          -80,73.4},{-79,73.4}}, color={0,0,127}));
  connect(actuatorBus.Qdemand, gain1.y) annotation (Line(
      points={{-60,-100},{-60,22},{-79,22},{-79,57.3}},
      color={80,200,120},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,-6},{-3,-6}},
      horizontalAlignment=TextAlignment.Right));
  connect(add1.y,add5. u2) annotation (Line(points={{-77.6,-58},{-76,-58},{-76,
          -52.4},{-72.8,-52.4}},     color={0,0,127}));
  connect(const.y, add1.u1) annotation (Line(points={{-91.6,-30},{-90,-30},{-90,
          -55.6},{-86.8,-55.6}}, color={0,0,127}));
  connect(add5.u1, add1.u1) annotation (Line(points={{-72.8,-47.6},{-80,-47.6},
          {-80,-30},{-90,-30},{-90,-55.6},{-86.8,-55.6}}, color={0,0,127}));
  connect(actuatorBus.Kv_bp, add5.y) annotation (Line(
      points={{-60,-100},{-60,-50},{-63.6,-50}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_DN, add1.y) annotation (Line(
      points={{-60,-100},{-60,-58},{-77.6,-58}},
      color={80,200,120},
      thickness=0.5));
  connect(const3.y,add2. u2) annotation (Line(points={{-20.4,2},{-22,2},{-22,
          -10},{-25.2,-10},{-25.2,-9.6}},
                        color={0,0,127}));
  connect(const4.y,PID2. u_s) annotation (Line(points={{13.7,-13},{-1,-13}},
                     color={0,0,127}));
  connect(PID2.y,add2. u1) annotation (Line(points={{-12.5,-13},{-12.5,-14.4},{
          -25.2,-14.4}},        color={0,0,127}));
  connect(gain2.y,PID2. u_m) annotation (Line(points={{7.4,4},{-7,4},{-7,-7}},
                                       color={0,0,127}));
  connect(Tset_ret, add3.u2) annotation (Line(points={{110,40},{44,40},{44,7.6},
          {39.2,7.6}}, color={0,0,127}));
  connect(sensorBus.T_DNout, add3.u1) annotation (Line(
      points={{60,-100},{60,0.4},{39.2,0.4}},
      color={255,219,88},
      thickness=0.5));
  connect(add3.y, gain2.u)
    annotation (Line(points={{25.4,4},{21.2,4}}, color={0,0,127}));
  connect(add2.y, add1.u2) annotation (Line(points={{-34.4,-12},{-118,-12},{
          -118,-60.4},{-86.8,-60.4}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={102,44,145},
          fillColor={102,44,145},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={102,44,145},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-90,78},{88,-68}},
          textColor={102,44,145},
          textString="DH
control")}),                                                     Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end DHNcontroller_PID;
