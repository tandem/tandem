# District heating

The `DistrictHeating` package provides the main components required to simulate the dynamics of a district heating network. These include the transmission line, which encompasses a feed and a return pipeline for the transmission of the heating water from the power plant to a distribution network or between different distribution networks, the model of the distribution network itself. In this package, the classes are all based on components from the [ThermoPower](https://github.com/casella/ThermoPower) library.



## Package description
The package is further divided into the following subpackages:
- The `Test` package provides some test cases for the proposed components, namely for the transmission and distribution network (both in steady state conditions and with an illustrative control strategy).
- The aforementioned classes are collected in the `Components` package.
- The `Control` package provides examples of controllers to be coupled to the district heating network, as well as dedicated signal buses to facilitate the coupling.

## Components description
### Distribution network
The distribution network model `Distribution` is used to simulate the water inertia within the ramifications of pipelines in urban areas where heat consumers are located.
The `Inertia` component, available in the `BaseClasses` subpackage, is modelled as a closed water volume exchanging water with the transmission line through two [Flange](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flange.html) components from the ThermoPower library. Moreover, an additional flange allows for fluid exchange with the pressure sink, which ensures that the pressure within the distribution network remains at its nominal value. The volume exchanges thermal power through three different [HeatPort](https://doc.modelica.org/om/Modelica.Thermal.HeatTransfer.Interfaces.HeatPort.html) connectors, available in the [Modelica Standard Library](https://doc.modelica.org/om/Modelica.html). These are connected to ideal heat sources [PrescribedHeatFlow](https://doc.modelica.org/om/Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow.html) that convert the signal power profiles from a control system into exchanged power.
In particular, the connectors represent three separate thermal power flows: the heat demand, the heat supplied directly to the distribution network, and the potential heat trade with other distribution networks.
In a modified version of the model, `Distribution_externalSource`, one [HeatPort](https://doc.modelica.org/om/Modelica.Thermal.HeatTransfer.Interfaces.HeatPort.html) connector is extended to be able to couple the ideal heat source model directly to the distribution network.
The exchanged variables are used in the governing equations of the `Inertia` component, i.e., mass and energy balance equations.

Two signal buses, available in the `Control` package, are used to collect the actuator and sensor signals, such as the distribution network's feed and return temperature, which could play an important role in the control of the district heating network.



### Transmission network
The transmission line model `Transmission_bypass` is based on components from the ThermoPower library. In particular, the [Pump](https://build.openmodelica.org/Documentation/ThermoPower.Water.Pump.html) component is used to model the pumping stations in the transmission line. Two pumps. located at the inlet of the hot and cold legs, are included in the model. As for the pipelines, a 1D finite volume approach, implemented in ThermoPower's [Flow1DFV](https://build.openmodelica.org/Documentation/ThermoPower.Water.Flow1DFV.html), is used to model the fluid's flow in the tubes. The hot and cold legs are thermally coupled to the [MetalTubeFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.MetalTubeFV.html) components, used to simulate the thermal inertia and resistance of the metal walls. The external wall connector is linked to a temperature source [TempSource1DFV](https://build.openmodelica.org/Documentation/ThermoPower.Thermal.TempSource1DFV.html) to account for the heat losses to the environment.


Two [ValveLin](https://build.openmodelica.org/Documentation/ThermoPower.Water.ValveLin.html) classes are added to the model to facilitate the control of thermal power delivered to the distribution network. For example, if thermal power to be delivered to the distribution network increases, the lower valve is opened and, at the same time, the valve connecting the hot leg to the cold leg (or, in other words, a bypass valve) is closed to increase the mass flow rate delivered to the distribution network.



## Control strategy
The `Control` package provides two examples of controllers for the regulation of the valving system in the transmission line and the sensor and actuator signal buses required to connect the controllers to the models.

The `DHNcontroller_SST` imposes the nominal value for the valve opening, i.e., with the bypass valve fully closed and the distribution line admission valve fully open. Moreover, it transfers the distribution network's input power profiles to the model.

The latter aspect is also present in the `DHNcontroller_PID` component. In addition, it provides an illustrative control strategy to be adopted in district heating networks: the bypass and admission valve opening is coordinated through a PI controller (see Modelica Standard Library's [LimPID](https://doc.modelica.org/om/Modelica.Blocks.Continuous.LimPID.html) component) to ensure that the distribution network's return temperature meets a given input setpoint.

## Test cases
### Steady-state test case
In the steady-state demo `Test_simpleNetwork_SST`, the district heating network model is coupled to a controller that imposes the nominal value of the controlled variables, i.e., the valves in the transmission lines. The controller also delivers signals of heat demand and supply to the distribution network. Under steady state conditions, the difference between these signals reflects the thermal power supplied by the transmission line.


### Transient test case
In contrast to the previous demonstration, the `Test_simpleNetwork_ctrl` test case is used to showcase a possible control strategy using the `DHNcontroller_PID` controller component. In the test case, the heat demand is reduced, and the control system is activated to reduce the amount of thermal power supplied by the transmission network, thereby ensuring that the distribution network's return temperature meets the given setpoint.

A similar test case, `Test_simpleNetwork_ExternalSources_ctrl` is available to showcase the use of the distribution network model connected to ideal heat sources, in this case represented by the `SimpleCHP` and the `HeatPump` models.


The `TestCases` package encompasses a test case in which the district heating components are connected to the SMR and its BOP, showcasing a possible application of the model. 