within TANDEM.DistrictHeating.Test;
model Test_simpleNetwork_ExternalSources_ctrl
  Components.Transmission_bypass transmission_bypass(L = 20e3, D = 0.6, mdot = 799.15, p = 750000, dp = 1800000, G = 12711.68, coldLeg(dpnom = 1700000, hstartin = 168199.1611498, hstartout = 168199.1611498, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), hotLeg(dpnom = 1735000, hstartin = 335507.85, hstartout = 335507.85, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), pump1(hstart = 168199.1611498), pump(hstart = 335507.85)) annotation (
    Placement(transformation(extent={{-62,-26},{8,20}})));
  Components.Distribution_externalSource
                          Espoo(                   V = 61000,
    Nhs=2,                                                    hstart = 168199.1611498, noInitialEnthalpy = false, noInitialPressure = false,
    pstart=750000)                                                                                                                                            annotation (
    Placement(transformation(extent={{98,-26},{42,20}})));
  Control.DHNcontroller_PID dHNcontroller_PID annotation (
    Placement(transformation(extent={{-4,44},{28,72}})));
  Modelica.Blocks.Sources.Constant const5(k = 0) annotation (
    Placement(transformation(extent={{-76,92},{-64,104}})));
  ThermoPower.Water.SinkPressure sinkPressure(p0 = 750000) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin={-92,26})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(w0 = 799.15, p0 = 750000, h = 335507.85) annotation (
    Placement(transformation(extent={{-102,-44},{-82,-24}})));
  Modelica.Blocks.Sources.Ramp supply(height = 0, duration = 1, offset = 65e6) annotation (
    Placement(transformation(extent={{-56,76},{-44,88}})));
  Modelica.Blocks.Sources.Ramp demand(height = -100e6, duration = 1000, offset = 200e6, startTime = 100) annotation (
    Placement(transformation(extent={{-78,60},{-66,72}})));
  Modelica.Blocks.Sources.Constant Tset(k = 40 + 273.15) annotation (
    Placement(transformation(extent={{-56,48},{-44,60}})));
  inner ThermoPower.System system(initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
    Placement(transformation(extent={{130,70},{150,90}})));
  PowerSources.SimpleCHP
                       simpleCHP
    annotation (Placement(transformation(extent={{-32,-92},{0,-62}})));
  ThermoPower.Electrical.Grid grid(Pgrid=1e6)
    annotation (Placement(transformation(extent={{8,-80},{28,-60}})));
  Modelica.Blocks.Sources.Constant Pel(k=2e6)
    annotation (Placement(transformation(extent={{-28,-42},{-16,-30}})));
  Modelica.Blocks.Sources.Constant Pth_CHP(k=50e6)
    annotation (Placement(transformation(extent={{-44,-52},{-34,-42}})));
  PowerSources.HeatPump
                      heatPump
    annotation (Placement(transformation(extent={{106,-62},{134,-36}})));
  ThermoPower.Electrical.Grid grid1(Pgrid=1e6)
    annotation (Placement(transformation(extent={{128,-32},{148,-12}})));
  Modelica.Blocks.Sources.Constant Pth_HP(k=15e6)
    annotation (Placement(transformation(extent={{90,-78},{102,-66}})));
equation
  connect(transmission_bypass.flangeB1, Espoo.flangeA) annotation (
    Line(points={{8,-12.775},{20,-12.775},{20,-14.5},{41.5333,-14.5}},          color = {0, 0, 255}));
  connect(transmission_bypass.flangeA1, Espoo.flangeB) annotation (
    Line(points={{8,6.775},{12,6.775},{12,6},{18,6},{18,8.5},{42,8.5}},                          color = {0, 0, 255}));
  connect(dHNcontroller_PID.actuatorBus, transmission_bypass.actuatorBus) annotation (
    Line(points={{2.4,44},{2,44},{2,32},{-44.5,32},{-44.5,20}},            color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_PID.actuatorBus, Espoo.actuatorBus) annotation (
    Line(points={{2.4,44},{2,44},{2,32},{84,32},{84,20}},            color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_PID.sensorBus, transmission_bypass.sensorBus) annotation (
    Line(points={{21.6,44},{22,44},{22,26},{-9.5,26},{-9.5,20}},          color = {255, 219, 88}, thickness = 0.5));
  connect(dHNcontroller_PID.sensorBus, Espoo.sensorBus) annotation (
    Line(points={{21.6,44},{22,44},{22,26},{56,26},{56,20}},          color = {255, 219, 88}, thickness = 0.5));
  connect(const5.y, dHNcontroller_PID.HeatTrade) annotation (
    Line(points={{-63.4,98},{-8,98},{-8,69.24},{-3.95423,69.24}},          color = {0, 0, 127}));
  connect(sinkPressure.flange, transmission_bypass.flangeB) annotation (
    Line(points={{-82,26},{-70,26},{-70,6.775},{-62,6.775}},            color = {0, 0, 255}));
  connect(sourceMassFlow.flange, transmission_bypass.flangeA) annotation (
    Line(points={{-82,-34},{-72,-34},{-72,-12.775},{-62,-12.775}},          color = {0, 0, 255}));
  connect(Tset.y, dHNcontroller_PID.Tset_ret) annotation (
    Line(points={{-43.4,54},{-32,54},{-32,49.36},{-3.95423,49.36}},          color = {0, 0, 127}));
  connect(demand.y, dHNcontroller_PID.HeatDemand) annotation (
    Line(points={{-65.4,66},{-22,66},{-22,56.6},{-4,56.6}},          color = {0, 0, 127}));
  connect(supply.y, dHNcontroller_PID.HeatSupply) annotation (
    Line(points={{-43.4,82},{-18,82},{-18,63.04},{-4,63.04}},          color = {0, 0, 127}));
  connect(simpleCHP.powerConnection,grid. port) annotation (Line(
      points={{0,-69.5},{0,-70},{9.4,-70}},
      color={0,0,255},
      thickness=0.5));
  connect(Pel.y, simpleCHP.Pel_set) annotation (Line(points={{-15.4,-36},{-6.4,
          -36},{-6.4,-60.8}}, color={0,0,127}));
  connect(Pth_CHP.y, simpleCHP.Pth_set) annotation (Line(points={{-33.5,-47},{
          -33.5,-48},{-25.6,-48},{-25.6,-60.8}}, color={0,0,127}));
  connect(heatPump.powerConnection,grid1. port) annotation (Line(
      points={{120.28,-35.74},{120.28,-22},{129.4,-22}},
      color={0,0,255},
      thickness=0.5));
  connect(Pth_HP.y,heatPump. Pth) annotation (Line(points={{102.6,-72},{120.28,
          -72},{120.28,-62.78}},
                          color={0,0,127}));
  connect(Espoo.HeatSources[1], heatPump.ThermalPower) annotation (Line(points=
          {{70,-26.575},{70,-49.13},{105.86,-49.13}}, color={191,0,0}));
  connect(simpleCHP.ThermalPower, Espoo.HeatSources[2]) annotation (Line(points
        ={{0,-84.5},{70,-84.5},{70,-25.425}}, color={191,0,0}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, extent={{-100,-100},{160,
            100}})),
    Diagram(coordinateSystem(preserveAspectRatio = false, extent={{-100,-100},{
            160,100}})),
    experiment(StopTime=2000, __Dymola_Algorithm="Dassl"));
end Test_simpleNetwork_ExternalSources_ctrl;
