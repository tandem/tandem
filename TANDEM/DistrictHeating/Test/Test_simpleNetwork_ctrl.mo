within TANDEM.DistrictHeating.Test;
model Test_simpleNetwork_ctrl
  Components.Transmission_bypass transmission_bypass(L = 20e3, D = 0.6, mdot = 799.15, p = 750000, dp = 1800000, G = 12711.68, coldLeg(dpnom = 1700000, hstartin = 168199.1611498, hstartout = 168199.1611498, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), hotLeg(dpnom = 1735000, hstartin = 335507.85, hstartout = 335507.85, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), pump1(hstart = 168199.1611498), pump(hstart = 335507.85)) annotation (
    Placement(transformation(extent = {{-62, -48}, {8, -2}})));
  Components.Distribution Espoo(                   V = 61000, hstart = 168199.1611498, noInitialEnthalpy = false, noInitialPressure = false,
    pstart=750000)                                                                                                                                            annotation (
    Placement(transformation(extent = {{98, -48}, {42, -2}})));
  Control.DHNcontroller_PID dHNcontroller_PID annotation (
    Placement(transformation(extent = {{-4, 22}, {28, 50}})));
  Modelica.Blocks.Sources.Constant const5(k = 0) annotation (
    Placement(transformation(extent = {{-76, 70}, {-64, 82}})));
  ThermoPower.Water.SinkPressure sinkPressure(p0 = 750000) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin = {-92, 4})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(w0 = 799.15, p0 = 750000, h = 335507.85) annotation (
    Placement(transformation(extent = {{-102, -66}, {-82, -46}})));
  Modelica.Blocks.Sources.Ramp supply(height = 0, duration = 1, offset = 65e6) annotation (
    Placement(transformation(extent = {{-56, 54}, {-44, 66}})));
  Modelica.Blocks.Sources.Ramp demand(height = -100e6, duration = 1000, offset = 200e6, startTime = 100) annotation (
    Placement(transformation(extent = {{-78, 38}, {-66, 50}})));
  Modelica.Blocks.Sources.Constant Tset(k = 40 + 273.15) annotation (
    Placement(transformation(extent = {{-56, 26}, {-44, 38}})));
  inner ThermoPower.System system(initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
    Placement(transformation(extent = {{68, 52}, {88, 72}})));
equation
  connect(transmission_bypass.flangeB1, Espoo.flangeA) annotation (
    Line(points={{8,-34.775},{20,-34.775},{20,-36.5},{41.5333,-36.5}},          color = {0, 0, 255}));
  connect(transmission_bypass.flangeA1, Espoo.flangeB) annotation (
    Line(points = {{8, -15.225}, {12, -15.225}, {12, -16}, {18, -16}, {18, -13.5}, {42, -13.5}}, color = {0, 0, 255}));
  connect(dHNcontroller_PID.actuatorBus, transmission_bypass.actuatorBus) annotation (
    Line(points = {{2.4, 22}, {2, 22}, {2, 10}, {-44.5, 10}, {-44.5, -2}}, color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_PID.actuatorBus, Espoo.actuatorBus) annotation (
    Line(points = {{2.4, 22}, {2, 22}, {2, 10}, {84, 10}, {84, -2}}, color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_PID.sensorBus, transmission_bypass.sensorBus) annotation (
    Line(points = {{21.6, 22}, {22, 22}, {22, 4}, {-9.5, 4}, {-9.5, -2}}, color = {255, 219, 88}, thickness = 0.5));
  connect(dHNcontroller_PID.sensorBus, Espoo.sensorBus) annotation (
    Line(points = {{21.6, 22}, {22, 22}, {22, 4}, {56, 4}, {56, -2}}, color = {255, 219, 88}, thickness = 0.5));
  connect(const5.y, dHNcontroller_PID.HeatTrade) annotation (
    Line(points={{-63.4,76},{-8,76},{-8,47.24},{-3.95423,47.24}},          color = {0, 0, 127}));
  connect(sinkPressure.flange, transmission_bypass.flangeB) annotation (
    Line(points = {{-82, 4}, {-70, 4}, {-70, -15.225}, {-62, -15.225}}, color = {0, 0, 255}));
  connect(sourceMassFlow.flange, transmission_bypass.flangeA) annotation (
    Line(points = {{-82, -56}, {-72, -56}, {-72, -34.775}, {-62, -34.775}}, color = {0, 0, 255}));
  connect(Tset.y, dHNcontroller_PID.Tset_ret) annotation (
    Line(points={{-43.4,32},{-32,32},{-32,27.36},{-3.95423,27.36}},          color = {0, 0, 127}));
  connect(demand.y, dHNcontroller_PID.HeatDemand) annotation (
    Line(points = {{-65.4, 44}, {-22, 44}, {-22, 34.6}, {-4, 34.6}}, color = {0, 0, 127}));
  connect(supply.y, dHNcontroller_PID.HeatSupply) annotation (
    Line(points = {{-43.4, 60}, {-18, 60}, {-18, 41.04}, {-4, 41.04}}, color = {0, 0, 127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false)),
    Diagram(coordinateSystem(preserveAspectRatio = false)));
end Test_simpleNetwork_ctrl;
