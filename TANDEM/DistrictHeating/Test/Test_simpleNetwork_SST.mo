within TANDEM.DistrictHeating.Test;
model Test_simpleNetwork_SST
  Components.Transmission_bypass transmission_bypass(L = 20e3, D = 0.6, mdot = 799.15,
    p=750000,
    dp=1800000,                                                                                                  G = 12711.68, coldLeg(dpnom = 1700000, hstartin = 168199.1611498, hstartout = 168199.1611498, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), hotLeg(
      dpnom=1735000,                                                                                                                                                                                                        hstartin = 335507.85, hstartout = 335507.85, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialPressure = false), pump1(hstart = 168199.1611498), pump(hstart = 335507.85)) annotation (
    Placement(transformation(extent = {{-60, -32}, {10, 14}})));
  Components.Distribution Espoo(
    V=61000,                                                  hstart = 168199.1611498, initOpt = ThermoPower.Choices.Init.Options.steadyState, noInitialEnthalpy = false, noInitialPressure = false,
    pstart=750000)                                                                                                                                                                                                    annotation (
    Placement(transformation(extent = {{100, -32}, {44, 14}})));
  Control.DHNcontroller_SST dHNcontroller_SST annotation (
    Placement(transformation(extent = {{-2, 38}, {30, 66}})));
  Modelica.Blocks.Sources.Constant const5(k = 0) annotation (
    Placement(transformation(extent = {{-74, 86}, {-62, 98}})));
  ThermoPower.Water.SinkPressure sinkPressure(R = 100, p0 = 750000) annotation (
    Placement(transformation(extent = {{-10, -10}, {10, 10}}, rotation = 180, origin = {-90, 20})));
  ThermoPower.Water.SourceMassFlow sourceMassFlow(w0 = 799.15,
    p0=750000,                                                              h = 335507.85) annotation (
    Placement(transformation(extent = {{-100, -50}, {-80, -30}})));
  Modelica.Blocks.Sources.Ramp supply(height = 0, duration = 1, offset = 65e6) annotation (
    Placement(transformation(extent = {{-54, 70}, {-42, 82}})));
  Modelica.Blocks.Sources.Ramp demand(height = 0, duration = 1, offset = 200e6) annotation (
    Placement(transformation(extent = {{-76, 54}, {-64, 66}})));
  Modelica.Blocks.Sources.Constant Tset(k = 40 + 273.15) annotation (
    Placement(transformation(extent = {{-54, 42}, {-42, 54}})));
  inner ThermoPower.System system(initOpt = ThermoPower.Choices.Init.Options.steadyState) annotation (
    Placement(transformation(extent = {{70, 68}, {90, 88}})));
equation
  connect(transmission_bypass.flangeB1, Espoo.flangeA) annotation (
    Line(points={{10,-18.775},{22,-18.775},{22,-20.5},{43.5333,-20.5}},          color = {0, 0, 255}));
  connect(transmission_bypass.flangeA1, Espoo.flangeB) annotation (
    Line(points = {{10, 0.775}, {14, 0.775}, {14, 0}, {20, 0}, {20, 2.5}, {44, 2.5}}, color = {0, 0, 255}));
  connect(dHNcontroller_SST.actuatorBus, transmission_bypass.actuatorBus) annotation (
    Line(points = {{4.4, 38}, {4, 38}, {4, 26}, {-42.5, 26}, {-42.5, 14}}, color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_SST.actuatorBus, Espoo.actuatorBus) annotation (
    Line(points = {{4.4, 38}, {4, 38}, {4, 26}, {86, 26}, {86, 14}}, color = {80, 200, 120}, thickness = 0.5));
  connect(dHNcontroller_SST.sensorBus, transmission_bypass.sensorBus) annotation (
    Line(points = {{23.6, 38}, {24, 38}, {24, 20}, {-7.5, 20}, {-7.5, 14}}, color = {255, 219, 88}, thickness = 0.5));
  connect(dHNcontroller_SST.sensorBus, Espoo.sensorBus) annotation (
    Line(points = {{23.6, 38}, {24, 38}, {24, 20}, {58, 20}, {58, 14}}, color = {255, 219, 88}, thickness = 0.5));
  connect(const5.y, dHNcontroller_SST.HeatTrade) annotation (
    Line(points={{-61.4,92},{-6,92},{-6,63.24},{-1.95423,63.24}},          color = {0, 0, 127}));
  connect(sinkPressure.flange, transmission_bypass.flangeB) annotation (
    Line(points = {{-80, 20}, {-68, 20}, {-68, 0.775}, {-60, 0.775}}, color = {0, 0, 255}));
  connect(sourceMassFlow.flange, transmission_bypass.flangeA) annotation (
    Line(points = {{-80, -40}, {-70, -40}, {-70, -18.775}, {-60, -18.775}}, color = {0, 0, 255}));
  connect(Tset.y, dHNcontroller_SST.Tset_ret) annotation (
    Line(points={{-41.4,48},{-30,48},{-30,43.36},{-1.95423,43.36}},          color = {0, 0, 127}));
  connect(demand.y, dHNcontroller_SST.HeatDemand) annotation (
    Line(points = {{-63.4, 60}, {-20, 60}, {-20, 50.6}, {-2, 50.6}}, color = {0, 0, 127}));
  connect(supply.y, dHNcontroller_SST.HeatSupply) annotation (
    Line(points = {{-41.4, 76}, {-16, 76}, {-16, 57.04}, {-2, 57.04}}, color = {0, 0, 127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false)),
    Diagram(coordinateSystem(preserveAspectRatio = false)));
end Test_simpleNetwork_SST;
