within TANDEM.DistrictHeating.Utilities;
model control_icon
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={123,62,185},
          lineThickness=1,
          fillColor={123,62,185},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={123,62,185},
          lineThickness=1,
          fillColor={234,234,234},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-44,-28},{56,-84}},
          textColor={0,0,0},
          fontName="Calibri",
          textString="%name")}), Diagram(coordinateSystem(preserveAspectRatio=
           false)));
end control_icon;
