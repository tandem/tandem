within TANDEM.H2production.HTSE.Validation.Tests;
model Test_Adaptor_TspH2toModelica_SourceP
  import HTSE;

  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2 (reducedX=false)                  "Medium of the CEA model";

  TANDEM.H2production.HTSE.Tools.TspH2toModelica Fluid_Adaptor(redeclare package Medium = Medium) annotation (Placement(transformation(extent={{-34,-18},{38,8}})));
  ThermoSysPro_H2.Fluid.BoundaryConditions.SourceP sourceP(
    ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    T0=1023.15,
    option_temperature=true,
    Xco2=0,
    Xh2o=0.9,
    Xo2=0,
    Xh2=0.1,
    T(start=1023.15))
    annotation (Placement(transformation(extent={{-102,-16},{-82,4}})));

  Modelica.Fluid.Sources.Boundary_pT boundary(
    redeclare package Medium = Medium,
    p(displayUnit="Pa") = 101325,
    X={0.9,0.1,0},
    nPorts=1) annotation (Placement(transformation(extent={{148,-14},{128,6}})));

  Modelica.Fluid.Pipes.StaticPipe pipe(
     redeclare package Medium = Medium,
    length=1,
    diameter=0.1)
    annotation (Placement(transformation(extent={{72,-14},{92,6}})));
  Modelica.Fluid.Sensors.Temperature         TIn1(redeclare package Medium =
        Medium)                                   "Temperature sensor"
    annotation (Placement(transformation(extent={{44,8},{60,24}})));
  ThermoSysPro_H2.Fluid.PressureLosses.SingularPressureLoss
    singularPressureLoss(K=100)
    annotation (Placement(transformation(extent={{-72,-16},{-52,4}})));
equation
  connect(Fluid_Adaptor.Output_Modelica, pipe.port_a) annotation (Line(points={
          {39.2,-4.48},{62,-4.48},{62,-4},{72,-4}}, color={0,127,255}));
  connect(pipe.port_b, boundary.ports[1]) annotation (Line(points={{92,-4},{102,
          -4},{102,-4},{128,-4}}, color={0,127,255}));
  connect(Fluid_Adaptor.Output_Modelica, TIn1.port) annotation (Line(points={{
          39.2,-4.48},{52,-4.48},{52,8}}, color={0,127,255}));
  connect(sourceP.C, singularPressureLoss.C1)
    annotation (Line(points={{-82,-6},{-72,-6}}, color={0,0,0}));
  connect(singularPressureLoss.C2, Fluid_Adaptor.Input_TSP) annotation (Line(
        points={{-52,-6},{-48,-6},{-48,-8},{-33.4,-8},{-33.4,-5.26}}, color={0,
          0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Test_Adaptor_TspH2toModelica_SourceP;
