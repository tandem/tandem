within TANDEM.H2production.HTSE.Validation;
model Test_BOP_module_heat_noFlow
  import HTSE;
    // Fluid definition
    replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2 "Medium of the CEA model";

    parameter Integer N_boiler = 3 "Number of nodes in the boiler";

    // External Heat Source
    parameter Modelica.Units.SI.Pressure HeatSource_P = 7.547e5 "External Steam Pressure";
    parameter Modelica.Units.SI.SpecificEnthalpy HeatSource_H = 2639.73e3 "External Steam Specific Enthalpy";
    parameter Modelica.Units.SI.Temperature HeatSource_T = ThermoSysPro.Properties.WaterSteam.IF97_Utilities.Standard.T_ph(HeatSource_P,HeatSource_H) "Extrenal Steam Temperature";
    parameter Modelica.Units.SI.Power HeatSource_W=0 "Thermal power from the External Steam"; //(fixed=false,start=2.e6)

    // Stack interface
    parameter Modelica.Units.SI.Pressure inletStackP = 1.15e5 "Operating Pressure of the Stack (inlet)";

    parameter Modelica.Units.SI.MassFlowRate waterFlow = 0.05 "Water Inlet Mass Flow Rate";
    parameter Modelica.Units.SI.Temperature targetStackT = 750+273.15 "Target Stack Temperature at Inlet";

  TANDEM.H2production.HTSE.Subcomponents.BoP.BOP_module_1Dboiler bOP_module(
    redeclare package Medium = Medium,
    power_inversion=false,
    NoN_extHX=3,
    N_boiler=N_boiler,
    N_HR_LT=3,
    N_HR_HT=3,
    Q_inertia=true,
    decouplePressure=false) annotation (Placement(transformation(extent={{-26,4},{26,34}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource fuel(
    T0=fill(HeatSource_T, N_boiler),
    W0=fill(HeatSource_W/N_boiler, N_boiler),
    option_temperature=2)
    annotation (Placement(transformation(extent={{-10,50},{10,70}})));
  Modelica.Fluid.Sources.FixedBoundary stackInlet(
    redeclare package Medium = Medium,
    p=inletStackP,
    nPorts=1)
    annotation (Placement(transformation(extent={{96,-30},{76,-10}})));
  Modelica.Fluid.Sources.MassFlowSource_T stackOutlet(
    redeclare package Medium = Medium,
    m_flow=waterFlow*0.4,
    T=targetStackT,
    X={0.98,0.02,0},
    nPorts=1) annotation (Placement(transformation(extent={{78,12},{58,32}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe     rampe(
    Starttime=100,
    Duration=300,
    Initialvalue=waterFlow,
    Finalvalue=1)
    annotation (Placement(transformation(extent={{-78,0},{-58,20}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante
    TargetStack(k=targetStackT)
    annotation (Placement(transformation(extent={{-52,-38},{-32,-18}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe     rampe1(
    Starttime=100,
    Duration=300,
    Initialvalue=HeatSource_W/N_boiler,
    Finalvalue=3e6/N_boiler)
    annotation (Placement(transformation(extent={{-54,60},{-34,80}})));
  Modelica.Fluid.Pipes.StaticPipe pipe(
    redeclare package Medium = Medium,
    length=1,
    diameter=1)
    annotation (Placement(transformation(extent={{42,-12},{62,8}})));
equation
  connect(stackOutlet.ports[1], bOP_module.fromStack) annotation (Line(points={{58,22},{36,22},{36,25.9},{25.8471,25.9}},
                                                color={0,127,255}));
  connect(bOP_module.FuelQ, rampe.y)
    annotation (Line(points={{-25.6941,30.7},{-52,30.7},{-52,10},{-57,10}},
                                                   color={0,0,255}));
  connect(bOP_module.StackTargetT, TargetStack.y)
    annotation (Line(points={{-19.8824,4},{-19.8824,-28},{-31,-28}},
                                                       color={0,0,255}));
  connect(fuel.C, bOP_module.extHeat) annotation (Line(points={{0,50.2},{0,34}},
                              color={0,0,0}));
  connect(fuel.ISignal, rampe1.y) annotation (Line(points={{0,65},{0,74},{-28,74},
          {-28,70},{-33,70}}, color={0,0,255}));
  connect(bOP_module.toStack, pipe.port_a) annotation (Line(points={{25.8471,11.65},{36,11.65},{36,-2},{42,-2}},
                                          color={0,127,255}));
  connect(pipe.port_b, stackInlet.ports[1]) annotation (Line(points={{62,-2},{
          70,-2.5},{70,-20},{76,-20}}, color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=500, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentSetupOutput(equidistant=false));
end Test_BOP_module_heat_noFlow;
