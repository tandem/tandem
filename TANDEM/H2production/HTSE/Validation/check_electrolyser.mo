within TANDEM.H2production.HTSE.Validation;
model check_electrolyser
  import HTSE;
 extends HTSE.Electrolysers.test_uni;
  annotation (experiment(StopTime=3600, __Dymola_Algorithm="Dassl"), __Dymola_experimentFlags(
      Advanced(
        EvaluateAlsoTop=false,
        GenerateVariableDependencies=true,
        OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end check_electrolyser;
