within TANDEM.H2production.HTSE.Tests;
model Test_Adaptor_ModelicaToTspH2_SourceQ "Test of the fluid adaptor"
  import HTSE;
  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2 (     reducedX=false)             "Medium of the CEA model";

  TANDEM.H2production.HTSE.Tools.ModelicatoTspH2 Fluid_Adaptor(redeclare package Medium =
                       Medium)
    annotation (Placement(transformation(extent={{-48,-20},{-10,20}})));
  Modelica.Fluid.Sources.MassFlowSource_T
                                     boundary(
    redeclare package Medium = Medium,
    m_flow=1,
    T=1023.15,
    X={0.1,0.9,0},
    nPorts=1)
    annotation (Placement(transformation(extent={{-130,-10},{-110,10}})));
  ThermoSysPro_H2.Fluid.PressureLosses.LumpedStraightPipe
                                                       lumpedStraightPipe
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  ThermoSysPro_H2.Fluid.BoundaryConditions.SinkP
                                              sinkP
    annotation (Placement(transformation(extent={{36,-10},{56,10}})));
  Modelica.Fluid.Pipes.StaticPipe pipe(
    redeclare package Medium = Medium,
    length=1,
    diameter=0.1)
    annotation (Placement(transformation(extent={{-88,-10},{-68,10}})));
equation
  connect(Fluid_Adaptor.Output_TSP, lumpedStraightPipe.C1)
    annotation (Line(points={{-10.3167,-0.4},{0,-0.4},{0,0}}, color={0,0,0}));
  connect(boundary.ports[1], pipe.port_a) annotation (Line(points={{-110,0},{
          -102,0},{-102,2},{-88,2},{-88,0}}, color={0,127,255}));
  connect(pipe.port_b, Fluid_Adaptor.Input_Modelica)
    annotation (Line(points={{-68,0},{-48,-0.8}}, color={0,127,255}));
  connect(lumpedStraightPipe.C2, sinkP.C)
    annotation (Line(points={{20,0},{36,0}}, color={0,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=100, __Dymola_Algorithm="Dassl"));
end Test_Adaptor_ModelicaToTspH2_SourceQ;
