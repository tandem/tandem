within TANDEM.H2production.HTSE.Demo;
model Test_HTSE_steam

  parameter Modelica.Units.SI.Power HeatSource_W=2.e6 "Thermal power to the boiler";
  parameter Integer N_boiler=3 "Number of thermal nodes in the boiler";
  parameter Integer NoP_boiler = 100 "Number of pipes in the external steam HX";
  parameter Modelica.Units.SI.Length L_boiler = 10 "Length of external steam HX pipes";
  parameter Modelica.Units.SI.Diameter D_boiler = 0.04 "Diameter of external steam HX pipes";

  TANDEM.H2production.HTSE.HTSE_module_steam hTSE_module(N_boiler=N_boiler) annotation (Placement(transformation(extent={{-10,-50},{30,-10}})));
  Modelica.Blocks.Sources.Ramp H2_ramp(
    height=0.1,
    duration=500,
    startTime=500)
    annotation (Placement(transformation(extent={{-64,-16},{-44,4}})));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixVol(
    definiteReference=true,
    f=50,
    V=380000)
    annotation (Placement(transformation(extent={{-92,-68},{-64,-42}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe     rampe1(
    Starttime=500,
    Duration=500,
    Initialvalue=0.01,
    Finalvalue=20)
    annotation (Placement(transformation(extent={{-96,56},{-76,76}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SourcePQ    sourcePQ(
    P0=754700,
    Q0=0.1,
    h0=2639.73e3)
    annotation (Placement(transformation(extent={{-62,44},{-42,64}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.Sink    sink
    annotation (Placement(transformation(extent={{68,44},{88,64}})));
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicOnePhaseFlowPipe
    dynamicOnePhaseFlowPipe(
    L=L_boiler,
    ntubes=NoP_boiler,
    Ns=N_boiler,
    inertia=false,
    dynamic_mass_balance=false,
    h(start={2639730.0,2389303.2708829213,2101323.9613705045,
          1473388.4286087365,1473388.4286087365}),
    P(start={754700,754700,754700,754700,754700}))
    annotation (Placement(transformation(extent={{-6,64},{14,44}})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss wPloss1(K=1.e-2)
    annotation (Placement(transformation(extent={{-36,44},{-16,64}})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss wPloss2(K=1.e-2,
      pro(d(start=10.529422752712195, displayUnit="g/cm3")))
    annotation (Placement(transformation(extent={{32,46},{52,66}})));
equation
  connect(hTSE_module.H2_target, H2_ramp.y) annotation (Line(points={{-13.2,
          -16},{-38,-16},{-38,-6},{-43,-6}},             color={0,0,127}));
  connect(fixVol.terminal, hTSE_module.term_n) annotation (Line(points={{-64,-55},
          {-20,-55},{-20,-42},{-14,-42}},                     color={0,120,120}));
  connect(rampe1.y,sourcePQ. IMassFlow) annotation (Line(points={{-75,66},{-66,
          66},{-66,68},{-52,68},{-52,59}},
                                  color={0,0,255}));
  connect(sourcePQ.C,wPloss1. C1)
    annotation (Line(points={{-42,54},{-36,54}}, color={0,0,255}));
  connect(wPloss1.C2,dynamicOnePhaseFlowPipe. C1)
    annotation (Line(points={{-16,54},{-6,54}}, color={0,0,255}));
  connect(dynamicOnePhaseFlowPipe.C2,wPloss2. C1) annotation (Line(points={{14,54},
          {28,54},{28,56},{32,56}}, color={0,0,255}));
  connect(wPloss2.C2,sink. C) annotation (Line(points={{52,56},{64,56},{64,54},
          {68,54}},color={0,0,255}));
  connect(dynamicOnePhaseFlowPipe.CTh, hTSE_module.extHeat)
    annotation (Line(points={{4,51},{4,-6},{10,-6},{10,-10}}, color={0,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=1500, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentSetupOutput(equidistant=false));
end Test_HTSE_steam;
