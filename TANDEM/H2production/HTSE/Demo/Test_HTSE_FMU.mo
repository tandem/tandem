within TANDEM.H2production.HTSE.Demo;
model Test_HTSE_FMU

  parameter Modelica.Units.SI.Power HeatSource_W=2.e6 "Thermal power to the boiler";
  parameter Integer N_boiler=3 "Number of thermal nodes in the boiler";
  parameter Integer NoP_boiler = 100 "Number of pipes in the external steam HX";
  parameter Modelica.Units.SI.Length L_boiler = 10 "Length of external steam HX pipes";
  parameter Modelica.Units.SI.Diameter D_boiler = 0.04 "Diameter of external steam HX pipes";

  Modelica.Blocks.Sources.Ramp H2_ramp(
    height=1,
    duration=500,
    startTime=500)
    annotation (Placement(transformation(extent={{-72,-20},{-52,0}})));
  Modelica.Blocks.Sources.Ramp H2_ramp1(
    height=HeatSource_W/N_boiler,
    duration=500,
    startTime=500)
    annotation (Placement(transformation(extent={{-110,10},{-90,30}})));
  TANDEM_HTSE_FMU_fmu tANDEM_HTSE_FMU_fmu annotation (Placement(transformation(extent={{28,-26},{90,36}})));
equation
  connect(H2_ramp1.y, tANDEM_HTSE_FMU_fmu.heat_from_source) annotation (Line(points={{-89,20},{12,20},{12,15.54},{26.76,15.54}}, color={0,0,127}));
  connect(H2_ramp.y, tANDEM_HTSE_FMU_fmu.H2_target) annotation (Line(points={{-51,-10},{12,-10},{12,-5.23},{26.76,-5.23}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=1500, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentSetupOutput(equidistant=false));
end Test_HTSE_FMU;
