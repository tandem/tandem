within TANDEM.H2production.HTSE.Demo;
model Test_HTSE_heat

  parameter Modelica.Units.SI.Power HeatSource_W=2.e6 "Thermal power to the boiler";
  parameter Integer N_boiler=3 "Number of thermal nodes in the boiler";
  parameter Integer NoP_boiler = 100 "Number of pipes in the external steam HX";
  parameter Modelica.Units.SI.Length L_boiler = 10 "Length of external steam HX pipes";
  parameter Modelica.Units.SI.Diameter D_boiler = 0.04 "Diameter of external steam HX pipes";

  TANDEM.H2production.HTSE.HTSE_module_steam hTSE_module(
    N_boiler=N_boiler,
    HTSE_Module(physical(SOEC(I_dens(start={1.7349368133702872E-12,-2.907824063542754E-13,-1.6837860310990185E-13,-1.9070925936537576E-12,-2.6488761452193666E-13,2.8351831549383187E-13,-1.8121990508568652E-12,-9.804395922269622E-13,-6.429748903638031E-13,-7.574463253835338E-13,1.4658842390048005E-12,-1.4835753687399038E-13,1.397440159018875E-12,3.066221294869167E-13,1.1377665868813206E-13,-6.902027107898186E-13,1.018866919669321E-12,-2.8204591117844256E-13,3.2923115697334835E-13,1.294530843607775E-12}), cat_out_port(Xi_outflow(start={0.7460201743080869,0.2539798256919132,0.0}))))),
    bOP_module(
      Q_control(Limiteur1(u(signal(start=-0.6949815112735434)))),
      T_control1(Limiteur1(u(signal(start=0)))),
      Wall_HR_HT(Tp(start={592.7125759267673,734.7685584214644,874.9668675005843}, displayUnit="degC")),
      Wall_HR_LT(Tp(start={349.53252118734264,370.5019912369099,394.9486808241491}, displayUnit="degC")),
      boiler(mu2(start={0.0002639658269909485,0.0002639658269887603,0.00026396582698657277,0.00026396582698438354}), pro2(d(start={953.8427646724637,953.8427646706349,953.842764668806,953.8427646669775}, displayUnit="g/cm3"))),
      eHeater(Xco2(start=1.4349761869549854E-21), Xo2(start=5.492202735844653E-21)),
      heatRecoverHT_h(
        Tp(start={874.9688374773776,734.7704844573645,592.7144368998412}, displayUnit="degC"),
        mu2(start={3.340865766321351E-05,2.9564288724101522E-05,2.53857720045527E-05,2.0888413596565884E-05}),
        rho2(start={0.19296384360584978,0.22243365457070166,0.2634359770302942,0.3235158377943518}, displayUnit="g/cm3")),
      heatRecoverHT_w(
        Tp(start={592.7106781003563,734.7665942437711,874.9648585118246}, displayUnit="degC"),
        mu2(start={1.4619833608225352E-05,1.967117033420077E-05,2.4441267776878306E-05,2.8809737361566745E-05}),
        rho2(start={0.46187725237433885,0.34396030090900304,0.27441541430546795,0.22904962507533666}, displayUnit="g/cm3")),
      heatRecoverLT_h(
        Tp(start={395.0826752296174,370.6171455983306,349.6321339501858}, displayUnit="degC"),
        mu2(start={2.0888413596565884E-05,1.9435845192721382E-05,1.8154846788314828E-05,1.702428712645801E-05}),
        rho2(start={0.32351583632551945,0.3481798495766646,0.3728964186071958,0.39755101319067637}, displayUnit="g/cm3")),
      heatRecoverLT_w(mu2(start={0.0005689308455769882,0.0004364888736089875,0.0003378354242207914,0.00026396582702939794}), pro2(d(start={989.2534559563586,981.0104844938994,969.5041256174329,953.8427647045903}, displayUnit="g/cm3"))),
      mixing(
        Xco2(start=1.4349761869549854E-21),
        Xo2(start=5.492202735844653E-21),
        h(start=2785128.9513046555)),
      sPloss3(rho(start=0.4618772530752241, displayUnit="g/cm3")),
      sensorQ(C1(h_vol(start=632170.3273848991))),
      volumeBoiler(h(start=632170.3273848991)),
      wPloss1(C1(h_vol(start=446402.48690976296)), C2(h_vol(start=446402.48690976296))))) annotation (Placement(transformation(extent={{-10,-50},{30,-10}})));

  Modelica.Blocks.Sources.Ramp H2_ramp(
    height=0.1,
    duration=500,
    startTime=500)
    annotation (Placement(transformation(extent={{-64,-16},{-44,4}})));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixVol(
    definiteReference=true,
    f=50,
    V=380000)
    annotation (Placement(transformation(extent={{-92,-68},{-64,-42}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource fuel(
    T0=fill(600, N_boiler),
    W0=fill(HeatSource_W/N_boiler, N_boiler),
    option_temperature=2)
    annotation (Placement(transformation(extent={{0,30},{20,50}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe     rampe1(
    Starttime=500,
    Duration=500,
    Initialvalue=0,
    Finalvalue=HeatSource_W/N_boiler)
    annotation (Placement(transformation(extent={{-44,40},{-24,60}})));
equation
  connect(hTSE_module.H2_target, H2_ramp.y) annotation (Line(points={{-13.2,
          -16},{-38,-16},{-38,-6},{-43,-6}},             color={0,0,127}));
  connect(fixVol.terminal, hTSE_module.term_n) annotation (Line(points={{-64,-55},
          {-20,-55},{-20,-42},{-14,-42}},                     color={0,120,120}));
  connect(fuel.ISignal,rampe1. y) annotation (Line(points={{10,45},{10,54},{-18,
          54},{-18,50},{-23,50}},
                              color={0,0,255}));
  connect(fuel.C, hTSE_module.extHeat)
    annotation (Line(points={{10,30.2},{10,-10}}, color={0,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=1500, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentSetupOutput(equidistant=false));
end Test_HTSE_heat;
