within TANDEM.H2production.HTSE;
model HTSE_module_steam
 import HTSE;

   // Fluid definition
   replaceable package Medium =
     HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2 "Medium of the CEA model";

   parameter Integer N_boiler = 3 "Number of nodes in the boiler";
   parameter Integer NoP_boiler = 100 "Number of pipes in the external steam HX";
   parameter Modelica.Units.SI.Length L_boiler = 10 "Length of external steam HX pipes";
   parameter Modelica.Units.SI.Diameter D_boiler = 0.04 "Diameter of external steam HX pipes";

   // External Heat Source
   parameter Modelica.Units.SI.Pressure HeatSource_P = 7.547e5 "External Steam Pressure";
   parameter Modelica.Units.SI.SpecificEnthalpy HeatSource_H = 2639.73e3 "External Steam Specific Enthalpy";
   parameter Modelica.Units.SI.Temperature HeatSource_T = ThermoSysPro.Properties.WaterSteam.IF97_Utilities.Standard.T_ph(HeatSource_P,HeatSource_H) "Extrenal Steam Temperature";
   parameter Modelica.Units.SI.Power HeatSource_W=0 "Thermal power from the External Steam"; //(fixed=false,start=2.e6)
   parameter Modelica.Units.SI.MassFlowRate HeatSource_Q = 1 "External Steam Mass Flow Rate";

   // Stack interface
   parameter Modelica.Units.SI.Pressure inletStackP = 1.15e5 "Operating Pressure of the Stack (inlet)";

   parameter Modelica.Units.SI.MassFlowRate waterFlow = 1 "Water Inlet Mass Flow Rate";
   parameter Modelica.Units.SI.Temperature targetStackT = 750+273.15 "Target Stack Temperature at Inlet";

     import Modelica.Units.SI;
 import Modelica.Media.Interfaces.PartialMixtureMedium.massToMoleFractions;

 replaceable package Medium_MoistH2 =
     HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2;

  replaceable package Medium_Air =
          HTSE.Media.Predefined.PureSubstance.Gas.Air.IdealGasAir;

  parameter SI.MassFlowRate m_flow_start=0 "Simulation start value mass flow at fluid interfaces" annotation (Dialog(group="Initial values"));
  parameter SI.Pressure p_start_an=101325 "Cathode Simulation start value pressure " annotation (Dialog(group="Initial values"));
  parameter SI.Pressure p_start_cat=101325 "Cathode Simulation start value pressure " annotation (Dialog(group="Initial values"));

  parameter SI.Temperature T_start=750 + 273.15 "initla temperature" annotation (Dialog(group="Initial values"));

  parameter SI.MassFlowRate m_flow_small=1e-8 "small flow regularization" annotation (Dialog(group="Simulations"));
  parameter SI.Pressure dp_small=10 "small flow regularization" annotation (Dialog(group="Simulations"));

  //parameter
  constant SI.Temperature T_ref=273.15;
  constant SI.Pressure p_ref=101325;
  constant SI.MolarVolume Vm_ref=Modelica.Constants.R*T_ref/p_ref;
  //m3/mol

  parameter Real ElecEfficiency=0.8 "Electrical efficiency factor to take into account the conversion from AC to DC and the HTSE auxiliaries consumption";

  TANDEM.H2production.HTSE.Subcomponents.BoP.BOP_module_1Dboiler bOP_module(
    redeclare package Medium = Medium,
    power_inversion=false,
    NoN_extHX=3,
    N_boiler=N_boiler,
    NoP_boiler=NoP_boiler,
    L_boiler=L_boiler,
    D_boiler=D_boiler,
    N_HR_LT=3,
    N_HR_HT=3,
    Q_control(Limiteur1(u(signal(start=-0.6949597342377033)))),
    SteamAdmission(Pm(start=299999.7341999661, displayUnit="bar")),
    T_control1(Limiteur1(u(signal(start=-18.362033948378667)))),
    WallBoiler(Tp(start={425.3361266972541,423.745666991684,411.41227215169664})),
    Wall_HR_HT(Tp(start={593.8981362416604,735.5658586561649,875.373704613954})),
    Wall_HR_LT(Tp(start={349.66018727966565,370.7298747431956,395.29913975414206})),
    boiler(mu2(start={0.0002632398484100629,0.00020242726789310966,0.00018289892595975147,1.121658138864154E-05}), pro2(d(start={953.6357448143807,929.5382608149959,917.3295719697401,120.48350051521776}, displayUnit="g/cm3"))),
    eHeater(
      Xco2(start=1.673320158056545E-30),
      Xo2(start=-4.607185538838841E-31),
      h(start=4129711.732886607)),
    heatRecoverHT_h(
      Tp(start={875.3756693520619,735.5677797621613,593.899992831344}, displayUnit="degC"),
      mu2(start={3.340865766321351E-05,2.957505147919537E-05,2.5409057823915717E-05,2.0925420145648074E-05}),
      rho2(start={0.1929638436417293,0.22234148177854543,0.2631744444209682,0.32292799188362714}, displayUnit="g/cm3")),
    heatRecoverHT_w(
      Tp(start={593.8962428854448,735.5638995060033,875.3717009676228}, displayUnit="degC"),
      mu2(start={1.467831529165859E-05,1.9714173817269512E-05,2.446839477540639E-05,2.8823010690272156E-05}),
      rho2(start={0.46007284857846187,0.3431991235087598,0.2740896068013502,0.2289305601820239}, displayUnit="g/cm3")),
    heatRecoverLT_h(
      Tp(start={395.4337963880144,370.84557171642484,349.7602474151874}, displayUnit="degC"),
      mu2(start={2.0925420145648074E-05,1.9466662903779246E-05,1.8180272459571257E-05,1.7045041353967526E-05}),
      rho2(start={0.32292799040894477,0.34762197951793283,0.3723747343096706,0.3970709626735211}, displayUnit="g/cm3")),
    heatRecoverLT_w(mu2(start={0.0005689308455769882,0.00043601324162989373,0.0003371575606415894,0.0002632398484483108}), pro2(d(start={989.2534559563586,980.969665404022,969.3969624755402,953.6357448464889}, displayUnit="g/cm3"))),
    mixing(
      P(start=100000.00565249524, displayUnit="bar"),
      Xco2(start=1.673320158056545E-30),
      Xh2(start=0.012280464),
      Xh2o(start=0.98771954),
      Xo2(start=-4.607185538838841E-31),
      h(start=2788599.8)),
    sPloss3(rho(start=0.4600728492839385, displayUnit="g/cm3")),
    sensorQ(C1(h_vol(start=680838.1012786472))),
    steamInlet(Q(start=0.05)),
    volumeBoiler(h(start=680838.1012786472)),
    wPloss1(C1(h_vol(start=447569.7870003945)), C2(h_vol(start=447569.7870003945))),
    fromStack(m_flow(start=0.05062165746706871))) annotation (Placement(transformation(extent={{-44,-8},{8,22}})));

 HTSE.Electrolysers.SOEC.models.electrolyzer_module                       HTSE_Module(physical(
       SOEC(I_dens(start={5.202265921136511E-13,-2.0368330071999257E-13,9.188943101111755E-13,
             -2.318075910971484E-12,1.3745651984550564E-12,-1.5064801778455921E-12,
             -1.842977555470652E-12,1.3849666618007507E-12,-1.9041672279093323E-13,
             -1.1318257170444996E-12,-1.4268995360536267E-12,-1.5031608659486896E-12,
             1.618513000724456E-12,4.715176153922173E-13,-9.997250111333332E-13,
             1.8061344513284083E-12,8.649909631176857E-13,1.9230309621011046E-12,
             3.825341245419139E-13,-1.421290817076163E-13}), x_H2O_TPBc(start
           ={0.2473740757074185,0.24737407570741848,0.24737407570741848,0.24737407570741848,
             0.2473740757074185,0.2473740757074185,0.2473740757074185,0.2473740757074185,
             0.2473740757074185,0.2473740757074185,0.2473740757074185,0.2473740757074185,
             0.2473740757074185,0.2473740757074185,0.2473740757074185,0.2473740757074185,
             0.2473740757074185,0.2473740757074185,0.24737407570741848,0.24737407570741848}))))            annotation (Placement(transformation(extent={{32,-98},
           {82,-48}})));
 ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
   adaptorModelicaTSP annotation (Placement(transformation(
       extent={{-10,-10},{10,10}},
       rotation=90,
       origin={-38,-24})));
 ThermoSysPro.Thermal.Connectors.ThermalPort extHeat[N_boiler]
   annotation (Placement(transformation(extent={{-10,90},{10,110}})));
 Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n term_n annotation (
     Placement(transformation(extent={{-110,-80},{-70,-40}}),
       iconTransformation(extent={{-140,-80},{-100,-40}})));
 ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
   adaptorModelicaTSP1
                      annotation (Placement(transformation(
       extent={{-10,-10},{10,10}},
       rotation=0,
       origin={-76,70})));
 Modelica.Blocks.Interfaces.RealInput H2_target
   annotation (Placement(transformation(extent={{-136,50},{-96,90}})));
 Modelica.Blocks.Math.Add ElectricityPower(k2=1/ElecEfficiency)
   annotation (Placement(transformation(extent={{76,-22},{96,-2}})));
 ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
   adaptorTSPModelica
   annotation (Placement(transformation(extent={{42,-16},{62,4}})));
 Modelica.Blocks.Sources.RealExpression net_H2(y=-HTSE_Module.physical.SOEC.cat_out_port.Xi_outflow[
       2]*HTSE_Module.physical.SOEC.cat_out_port.m_flow - HTSE_Module.physical.SOEC.cat_in_port.m_flow
       *HTSE_Module.physical.SOEC.cat_in_port.Xi_outflow[2])
   annotation (Placement(transformation(extent={{30,46},{60,76}})));
 Modelica.Blocks.Interfaces.RealOutput H2_production annotation (Placement(
       transformation(extent={{92,50},{132,90}}), iconTransformation(extent={
           {92,50},{132,90}})));
 ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback DQ
   annotation (Placement(transformation(extent={{-50,60},{-30,80}})));
 ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PIsat Fuel_target(
   k=2,
   Ti=10,
   maxval=2,
   minval=0.05,
   ureset0=0,
   permanent=false)
   annotation (Placement(transformation(extent={{-94,0},{-74,20}})));
 ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
   adaptorModelicaTSP2
                      annotation (Placement(transformation(
       extent={{10,-10},{-10,10}},
       rotation=0,
       origin={40,40})));
equation

 term_n.v[1] * term_n.i[1] + term_n.v[2] * term_n.i[2] = ElectricityPower.y; // Active power
 term_n.v[2] * term_n.i[1] - term_n.v[1] * term_n.i[2] = 0; // Reactive power

 connect(bOP_module.toStack, HTSE_Module.Fuel_In) annotation (Line(points={{7.84706,
         -0.35},{12,-0.35},{12,-73},{32,-73}},
                 color={0,127,255}));
 connect(bOP_module.fromStack, HTSE_Module.Fuel_Out) annotation (Line(points={{7.84706,
         13.9},{102,13.9},{102,-73},{82,-73}},       color={0,127,255}));
 connect(HTSE_Module.StackTemperature, adaptorModelicaTSP.u) annotation (Line(
       points={{67,-45.5},{67,-36},{-12,-36},{-12,-50},{-38,-50},{-38,-36}},
       color={0,0,127}));
 connect(bOP_module.StackTargetT, adaptorModelicaTSP.outputReal) annotation (
     Line(points={{-37.8824,-8},{-37.8824,-13},{-38,-13}},
                                                     color={0,0,255}));
 connect(bOP_module.extHeat, extHeat) annotation (Line(points={{-18,22},{-18,
         84},{0,84},{0,100}},      color={0,0,0}));
 connect(H2_target, adaptorModelicaTSP1.u)
   annotation (Line(points={{-116,70},{-88,70}}, color={0,0,127}));
 connect(HTSE_Module.StackElectricalPower, ElectricityPower.u2) annotation (
     Line(points={{46.5,-46},{46.5,-28},{74,-28},{74,-18}},
       color={0,0,127}));
 connect(ElectricityPower.u1, adaptorTSPModelica.y)
   annotation (Line(points={{74,-6},{64,-6}},  color={0,0,127}));
 connect(bOP_module.ePower, adaptorTSPModelica.inputReal) annotation (Line(
       points={{-2.09412,-8.3},{-2.09412,-12},{36,-12},{36,-6},{41,-6}},
                                                             color={0,0,255}));
 connect(net_H2.y, H2_production) annotation (Line(points={{61.5,61},{86,61},
         {86,70},{112,70}}, color={0,0,127}));
 connect(adaptorModelicaTSP1.outputReal, DQ.u1)
   annotation (Line(points={{-65,70},{-51,70}}, color={0,0,255}));
 connect(net_H2.y, adaptorModelicaTSP2.u) annotation (Line(points={{61.5,61},
         {66,61},{66,40},{52,40}}, color={0,0,127}));
 connect(DQ.y, Fuel_target.u) annotation (Line(points={{-29,70},{-26,70},{-26,
         28},{-100,28},{-100,10},{-95,10}}, color={0,0,255}));
 connect(adaptorModelicaTSP2.outputReal, DQ.u2)
   annotation (Line(points={{29,40},{-40,40},{-40,59}}, color={0,0,255}));
 connect(Fuel_target.y, bOP_module.FuelQ) annotation (Line(points={{-73,10},{-54,10},{-54,18.7},{-43.6941,18.7}},
                                              color={0,0,255}));
 annotation (Icon(graphics={
       Line(
         points={{-76,78},{-56,24},{-96,-28},{-76,-82}},
         color={223,142,28},
         smooth=Smooth.Bezier,
         thickness=1),
       Text(
         extent={{-58,64},{100,-60}},
         textColor={28,108,200},
         textStyle={TextStyle.Bold},
         textString="H"),
       Text(
         extent={{-8,26},{140,-80}},
         textColor={28,108,200},
         textStyle={TextStyle.Bold},
         textString="2"),
       Line(
         points={{-56,78},{-36,24},{-76,-28},{-56,-82}},
         color={223,142,28},
         smooth=Smooth.Bezier,
         thickness=1),
       Line(
         points={{-36,78},{-16,24},{-56,-28},{-36,-82}},
         color={223,142,28},
         smooth=Smooth.Bezier,
         thickness=1),
       Rectangle(
         extent={{-100,100},{100,-100}},
         lineColor={0,128,255},
         lineThickness=1),
       Line(
         points={{-74,78},{-54,24},{-94,-28},{-74,-82}},
         color={135,135,135},
         smooth=Smooth.Bezier,
         thickness=1),
       Line(
         points={{-54,78},{-34,24},{-74,-28},{-54,-82}},
         color={135,135,135},
         smooth=Smooth.Bezier,
         thickness=1),
       Line(
         points={{-34,78},{-14,24},{-54,-28},{-34,-82}},
         color={135,135,135},
         smooth=Smooth.Bezier,
         thickness=1)}),
   experiment(StopTime=500, __Dymola_Algorithm="Dassl"),
   __Dymola_experimentSetupOutput(equidistant=false));
end HTSE_module_steam;
