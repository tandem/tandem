within TANDEM.H2production.HTSE.Subcomponents.BoP;
model BOP_module_1Dboiler "BOP of the HTSE plant"
  import HTSE;
  // Fluid definition
  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2 "Medium of the CEA model";

  parameter Boolean power_inversion = false "Compute input power so that...";

  parameter Real Xh2inlet = 0.1 "H2 molar fraction at the stack inlet";
  parameter Modelica.Units.SI.Temperature minSteamT = 150+273.15 "Minimal Temperature at the boiler outlet";

  parameter Real cvSteamAdm=13500 "CvMax of the Steam Admission Valve";

  parameter Integer NoN_extHX = 10 "Number of nodes in the external steam HX";
  parameter Integer NoP_extHX = 1000 "Number of pipes in the external steam HX";
  parameter Modelica.Units.SI.Length L_extHX = 10 "Length of external steam HX pipes";
  parameter Modelica.Units.SI.Diameter D_extHX = 0.04 "Diameter of external steam HX pipes";

  parameter Integer N_boiler = 10 "Number of nodes in the external steam HX";
  parameter Integer NoP_boiler = 100 "Number of pipes in the external steam HX";
  parameter Modelica.Units.SI.Length L_boiler = 10 "Length of external steam HX pipes";
  parameter Modelica.Units.SI.Diameter D_boiler = 0.04 "Diameter of external steam HX pipes";

  parameter Integer N_HR_LT = 4 "Number of Axial Nodes in the Low Temperature Heat Recover";
  parameter Integer N_HR_HT = 7 "Number of Axial Nodes in the High Temperature Heat Recover";
  parameter Integer NoP_HR_LT = 100 "Number of pipes in the Low Temperature Heat Recover";
  parameter Integer NoP_HR_HT = 1000 "Number of pipes in the High Temperature Heat Recover";
  parameter Modelica.Units.SI.Length L_HR_LT = 1 "Length of pipes in the Low Temperature Heat Recover";
  parameter Modelica.Units.SI.Length L_HR_HT = 3 "Length of pipes in the High Temperature Heat Recover";
  parameter Modelica.Units.SI.Diameter D_HR_LT_h2o = 0.01 "Diameter of pipes in the Low Temperature Heat Recover - Water side";
  parameter Modelica.Units.SI.Diameter D_HR_LT_h2 = 0.25 "Diameter of pipes in the Low Temperature Heat Recover - H2 side";
  parameter Modelica.Units.SI.Diameter D_HR_HT_h2o = 0.1 "Diameter of pipes in the Low Temperature Heat Recover - Water side";
  parameter Modelica.Units.SI.Diameter D_HR_HT_h2 = 0.1 "Diameter of pipes in the Low Temperature Heat Recover - H2 side";
  parameter Modelica.Units.SI.Area A_boiler = 100 "Boiler Thermal Exchange Surface -> Rth";

  parameter Modelica.Units.SI.Power eHeat=421301 "Electrical Heater Power";
  parameter Modelica.Units.SI.Temperature initStackT = 750+273.15 "Target Stack Temperature at Inlet";
  parameter Modelica.Units.SI.SpecificEnthalpy initStackH = 4.73645e6 "Initial Specific Enthalpy at the inlet of the Stack (eHeater)";
  parameter Boolean Q_inertia = false "Inertia of Mass Flow";
  parameter Boolean decouplePressure = true "Decouple pressure at the stack inlet";

protected
  parameter Boolean dyn_mass_balance = false "Dynamic mass balance";

  parameter Real molar_Xi[3] = {1-Xh2inlet, Xh2inlet, 0} "Desired molar fractions at the stack inlet";
  parameter Real mass_Xi[3] = Modelica.Media.Interfaces.PartialMixtureMedium.moleToMassFractions(molar_Xi, Medium.MMX) "Desired mass fractions at the stack inlet";

  parameter Modelica.Units.SI.SpecificEnthalpy h2h_init = ThermoSysPro_H2.Properties.H2mixGases.H2mixGases_h(PMF=1.5e5,TMF=initStackT,Xco2=0,Xh2o=mass_Xi[1],Xo2=0,Xh2=mass_Xi[2]) "Intial enthalpy @ target stack T";

public
  Modelica.Units.SI.TemperatureDifference overheating "Steam overheating at the boiler output";
  Modelica.Units.SI.Velocity v_heatRecoverHT_h = heatRecoverHT_h.Q[end]/heatRecoverHT_h.rho2[end]/heatRecoverHT_h.ntubes/(heatRecoverHT_h.D/2)^2/3.14 "Fluid speed at the outlet of the heatRecoverHT_h";
  Modelica.Units.SI.Velocity v_heatRecoverLT_h = heatRecoverLT_h.Q[end]/heatRecoverLT_h.rho2[end]/heatRecoverLT_h.ntubes/(heatRecoverLT_h.D/2)^2/3.14 "Fluid speed at the outlet of the heatRecoverLT_h";
  Modelica.Units.SI.Velocity v_heatRecoverHT_w = heatRecoverHT_w.Q[end]/heatRecoverHT_w.rho2[end]/heatRecoverHT_w.ntubes/(heatRecoverHT_w.D/2)^2/3.14 "Fluid speed at the outlet of the heatRecoverHT_w";
  Modelica.Units.SI.Velocity v_heatRecoverLT_w = heatRecoverLT_w.Q[end]/heatRecoverLT_w.rho2[end]/heatRecoverLT_w.ntubes/(heatRecoverLT_w.D/2)^2/3.14 "Fluid speed at the outlet of the heatRecoverLT_w";
  Modelica.Units.SI.Velocity v_boiler = boiler.Q[end]/boiler.rho2[end]/boiler.ntubes/(boiler.D/2)^2/3.14 "Fluid speed at the outlet of the boiler";
  Modelica.Units.SI.Power W_boiler = sum(boiler.CTh.W) "Thermal power to the boiler";

  ThermoSysPro_H2.Fluid.BoundaryConditions.SourceQ steamInlet(
    ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    Q0=1,
    T0=473.15,
    h0=1.e6,
    option_temperature=true,
    Xco2=0,
    Xh2o=1,
    Xo2=0,
    Xh2=0) annotation (Placement(transformation(extent={{26,-56},{46,-36}})));

  ThermoSysPro_H2.Fluid.PressureLosses.ControlValve SteamAdmission(Cvmax=
        cvSteamAdm, Pm(start=170000))
    annotation (Placement(transformation(extent={{74,-50},{90,-34}})));
  ThermoSysPro_H2.Fluid.Sensors.SensorP sensorP
    annotation (Placement(transformation(extent={{52,-48},{62,-38}})));
  ThermoSysPro_H2.Fluid.Volumes.VolumeA mixing(ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    dynamic_mass_balance=true,
    continuous_flow_reversal=true,
    dynamic_composition_balance=true,
    Xh2o0=mass_Xi[1],
    Xo20=0,
    Xh20=mass_Xi[2],
      h(start=3e6))
    annotation (Placement(transformation(extent={{100,-36},{120,-56}})));
  ThermoSysPro_H2.Fluid.BoundaryConditions.SourceQ h2rec(
    ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    Q0=0.1,
    T0=473.15,
    h0=1.e7,
    option_temperature=true,
    Xco2=0,
    Xh2o=0,
    Xo2=0,
    Xh2=1) annotation (Placement(transformation(extent={{40,-88},{60,-68}})));

  ThermoSysPro_H2.Fluid.Volumes.VolumeATh eHeater(ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    h0=initStackH,
    Xh2o0=mass_Xi[1],
    Xo20=0,
    Xh20=mass_Xi[2],
    T(start=initStackT),
    h(start=initStackH, fixed=false),
    Xh2o(start=mass_Xi[1]),
    Xh2(start=mass_Xi[2]))
    annotation (Placement(transformation(extent={{224,-36},{244,-56}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource elec(W0={eHeat},
      option_temperature=2)
    annotation (Placement(transformation(extent={{242,-68},{262,-88}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SourceP wInlet(
    P0=500000,
    h0=200000,
    option_temperature=2)
    annotation (Placement(transformation(extent={{-216,-52},{-196,-32}})));
  ThermoSysPro.WaterSteam.BoundaryConditions.SinkP wOutlet
    annotation (Placement(transformation(extent={{8,-56},{28,-36}})));
  ThermoSysPro.WaterSteam.Sensors.SensorQ sensorQ
    annotation (Placement(transformation(extent={{-4,-46},{4,-38}})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss wPloss1(K=1.e-2)
    annotation (Placement(transformation(extent={{-112,-54},{-92,-34}})));
  ThermoSysPro.Properties.WaterSteam.Common.PropThermoSat lsat annotation (
      Placement(transformation(extent={{-88,-78},{-72,-60}}, rotation=0)));
  ThermoSysPro.Properties.WaterSteam.Common.PropThermoSat vsat annotation (
      Placement(transformation(extent={{-68,-78},{-52,-62}},
                                                          rotation=0)));
  ThermoSysPro.WaterSteam.Sensors.SensorT sensorT
    annotation (Placement(transformation(extent={{-18,-44},{-8,-54}})));

  ThermoSysPro_H2.Fluid.BoundaryConditions.SinkP H2outlet
    annotation (Placement(transformation(extent={{-164,0},{-184,20}})));
  ThermoSysPro_H2.Fluid.PressureLosses.SingularPressureLoss sPloss3
    annotation (Placement(transformation(extent={{132,-56},{152,-36}})));

  ThermoSysPro_H2.Fluid.PressureLosses.SingularPressureLoss sPloss4
    annotation (Placement(transformation(extent={{54,0},{34,20}})));
  ThermoSysPro_H2.Fluid.HeatExchangers.DynamicOnePhaseFlowPipe heatRecoverHT_h(
    L=L_HR_HT,
    D=D_HR_HT_h2,
    ntubes=NoP_HR_HT,
    Ns=N_HR_HT,
    inertia=Q_inertia,
    dynamic_mass_balance=dyn_mass_balance,
    h(each start=5.e6))
    annotation (Placement(transformation(extent={{148,28},{124,-8}})));
  ThermoSysPro_H2.Fluid.HeatExchangers.DynamicOnePhaseFlowPipe heatRecoverHT_w(
    L=L_HR_HT,
    D=D_HR_HT_h2o,
    ntubes=NoP_HR_HT,
    Ns=N_HR_HT,
    inertia=Q_inertia,
    dynamic_mass_balance=dyn_mass_balance,
    continuous_flow_reversal=true,
    h(each start=3.e6))
    annotation (Placement(transformation(extent={{164,-64},{188,-28}})));
  ThermoSysPro_H2.Fluid.HeatExchangers.DynamicOnePhaseFlowPipe heatRecoverLT_h(
    L=L_HR_LT,
    D=D_HR_LT_h2,
    ntubes=NoP_HR_LT,                                                          Ns=
        N_HR_LT,
    inertia=Q_inertia,
    dynamic_mass_balance=dyn_mass_balance,
    h(each start=3e6))
    annotation (Placement(transformation(extent={{-88,28},{-112,-8}})));
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicOnePhaseFlowPipe
    heatRecoverLT_w(
    L=L_HR_LT,
    D=D_HR_LT_h2o,
    ntubes=NoP_HR_LT,
                    Ns=N_HR_LT,
    inertia=Q_inertia,
    dynamic_mass_balance=dyn_mass_balance)
    annotation (Placement(transformation(extent={{-162,-64},{-138,-26}})));
  ThermoSysPro.Thermal.Connectors.ThermalPort extHeat[N_boiler]
    annotation (Placement(transformation(extent={{-60,90},{-40,110}}),
        iconTransformation(extent={{-60,90},{-40,110}})));
  TANDEM.H2production.HTSE.Tools.TspH2toModelica toMSL(
    redeclare package Medium = Medium,
    DecouplingPressure=100000,
    DecouplePressure=decouplePressure) annotation (Placement(transformation(extent={{264,-50},{276,-40}})));
  Modelica.Fluid.Interfaces.FluidPort_b toStack(redeclare package Medium =
        Medium) annotation (Placement(transformation(extent={{324,-76},{392,-14}}),
                    iconTransformation(extent={{86,-80},{152,-18}})));
  TANDEM.H2production.HTSE.Tools.ModelicatoTspH2 fromMSL annotation (Placement(transformation(extent={{224,40},{212,50}})));
  inner Modelica.Fluid.System system(m_flow_small=1e-8)
    annotation (Placement(transformation(extent={{312,74},{332,94}})));
  Modelica.Fluid.Interfaces.FluidPort_a fromStack(redeclare package Medium =
        Medium) annotation (Placement(transformation(extent={{326,14},{390,74}}),
        iconTransformation(extent={{86,16},{152,76}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain Qh2rec(Gain=1/(1/
        mass_Xi[2] - 1))
    annotation (Placement(transformation(extent={{28,-70},{36,-62}})));
  ThermoSysPro.Thermal.HeatTransfer.HeatExchangerWallCounterFlow Wall_HR_LT(
    L=L_HR_LT,
    D=D_HR_LT_h2o,
    Ns=N_HR_LT,
    ntubes=NoP_HR_LT,
    Tp(each start=500))
    annotation (Placement(transformation(extent={{-134,-30},{-104,0}})));
  ThermoSysPro.Thermal.HeatTransfer.HeatExchangerWallCounterFlow Wall_HR_HT(
    L=L_HR_HT,
    D=D_HR_HT_h2o,
    Ns=N_HR_HT,
    ntubes=NoP_HR_HT,
    Tp(each start=800))
    annotation (Placement(transformation(extent={{126,-30},{156,0}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback DQ
    annotation (Placement(transformation(extent={{-114,70},{-94,90}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PIsat Q_control(
    maxval=0.3,
    minval=-0.7,
    permanent=true)
    annotation (Placement(transformation(extent={{-86,70},{-66,90}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante
    defaultOpening(k=0.7)
    annotation (Placement(transformation(extent={{-34,18},{-14,38}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add Opening
    annotation (Placement(transformation(extent={{2,44},{22,64}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal FuelQ
    annotation (Placement(transformation(extent={{-228,68},{-208,88}}),
        iconTransformation(extent={{-228,68},{-208,88}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal H2_Q
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={328,-100}), iconTransformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={81,-102})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback DT
    annotation (Placement(transformation(extent={{168,-104},{188,-84}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI T_control(k=1e3,
      permanent=true)
    annotation (Placement(transformation(extent={{208,-104},{228,-84}})));
  TANDEM.H2production.HTSE.Tools.WirelessSensor wirelessSensor(m=eHeater.T) annotation (Placement(transformation(extent={{136,-128},{156,-108}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal StackTargetT
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=270,
        origin={100,-100}), iconTransformation(
        extent={{10,-10},{-10,10}},
        rotation=270,
        origin={-180,-100})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal ePower
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={304,-100}), iconTransformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={54,-102})));
  ThermoSysPro_H2.Fluid.PressureLosses.SingularPressureLoss sPloss2
    annotation (Placement(transformation(extent={{196,-56},{216,-36}})));
  ThermoSysPro.WaterSteam.HeatExchangers.DynamicOnePhaseFlowPipe boiler(
    L=L_boiler,
    D=D_boiler,
    ntubes=NoP_boiler,
    Ns=N_boiler,
    inertia=Q_inertia,
    dynamic_mass_balance=dyn_mass_balance)
    annotation (Placement(transformation(extent={{-78,-62},{-54,-24}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.NonLineaire.Limiteur limiteur(maxval=20, minval=
        0.05)
    annotation (Placement(transformation(extent={{-180,70},{-160,90}})));
  ThermoSysPro.WaterSteam.Volumes.VolumeATh volumeBoiler
    annotation (Placement(transformation(extent={{-42,-54},{-26,-38}})));
  ThermoSysPro.Thermal.BoundaryConditions.HeatSource elec1(W0={eHeat},
      option_temperature=2)
    annotation (Placement(transformation(extent={{-44,-82},{-24,-102}})));
  TANDEM.H2production.HTSE.Tools.WirelessSensor wirelessSensor1(m=volumeBoiler.T) annotation (Placement(transformation(extent={{-136,-146},{-116,-126}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback DT1
    annotation (Placement(transformation(extent={{-114,-120},{-94,-100}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PIsat
                                                           T_control1(
    k=10,
    Ti=1,
    maxval=50e3,
      permanent=true)
    annotation (Placement(transformation(extent={{-72,-120},{-52,-100}})));
  TANDEM.H2production.HTSE.Tools.WirelessSensor wirelessSensor2(m=minSteamT) annotation (Placement(transformation(extent={{-148,-112},{-128,-92}})));
  ThermoSysPro.Thermal.HeatTransfer.HeatExchangerWallCounterFlow WallBoiler(
    L=L_boiler,
    D=D_boiler,
    ntubes=NoP_boiler,
    Ns=N_boiler,
    Tp(each start=450))
    annotation (Placement(transformation(extent={{-76,-12},{-56,-32}})));
  ThermoSysPro_H2.Fluid.Volumes.VolumeA h2volume(
    ftype=ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    V=50,
    steady_state=false,
    h0=h2h_init,
    dynamic_composition_balance=true,
    Xco20=0,
    Xh2o0=mass_Xi[1],
    Xo20=0,
    Xh20=mass_Xi[2])
    annotation (Placement(transformation(extent={{186,22},{166,42}})));

  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Add TotalHeater
    annotation (Placement(transformation(extent={{242,-154},{262,-134}})));
initial equation
  //boiler.h = vsat.h;    //Saturated steam at the boiler outlet
  if power_inversion then
    overheating = 30; //Overheated steam at the outlet
  end if;
  //boiler.Qevap = FuelQ.signal;
  //heatRecoverLT_w.pro2[N_HR_LT+1].x = 0.05;

equation
    overheating = boiler.T2[end] - vsat.T;
    (lsat,vsat) = ThermoSysPro.Properties.WaterSteam.IF97.Water_sat_P(boiler.P[end]);
    H2_Q.signal = H2outlet.C.Q * H2outlet.Xh2 - h2rec.C.Q;

  connect(steamInlet.C, sensorP.C1)
    annotation (Line(points={{46,-46},{46,-47},{52,-47}}, color={0,0,0}));
  connect(sensorP.C2, SteamAdmission.C1) annotation (Line(points={{62.1,-47},{62.1,
          -46.8},{74,-46.8}}, color={0,0,0}));
  connect(SteamAdmission.C2, mixing.Ce1) annotation (Line(points={{90,-46.8},{98,
          -46.8},{98,-46},{100,-46}}, color={0,0,0}));
  connect(h2rec.C, mixing.Ce2)
    annotation (Line(points={{60,-78},{110,-78},{110,-56}},
                                                          color={0,0,0}));
  connect(eHeater.Cth, elec.C[1]) annotation (Line(points={{234,-46},{234,-66},
          {252,-66},{252,-68.2}},color={0,0,0}));
  connect(sensorQ.C2, wOutlet.C) annotation (Line(points={{4.08,-45.2},{4.08,
          -46},{8,-46}},        color={0,0,255}));
  connect(sensorQ.Measure, steamInlet.IMassFlow) annotation (Line(points={{0,-37.92},
          {0,-32},{36,-32},{36,-41}},       color={0,0,255}));
  connect(sensorT.C2, sensorQ.C1) annotation (Line(points={{-7.9,-45},{-5.95,
          -45},{-5.95,-45.2},{-4,-45.2}},color={0,0,255}));
  connect(sensorT.Measure, steamInlet.ISpecificEnthalpyOrTemperature)
    annotation (Line(points={{-13,-54},{36,-54},{36,-51}},  color={0,0,255}));

  connect(mixing.Cs1, sPloss3.C1)
    annotation (Line(points={{120,-46},{132,-46}},
                                               color={0,0,0}));
  connect(sPloss4.C1, heatRecoverHT_h.C2)
    annotation (Line(points={{54,10},{124,10}}, color={0,0,0}));
  connect(sPloss3.C2, heatRecoverHT_w.C1)
    annotation (Line(points={{152,-46},{164,-46}},
                                                 color={0,0,0}));
  connect(H2outlet.C, heatRecoverLT_h.C2)
    annotation (Line(points={{-164,10},{-112,10}}, color={0,0,0}));
  connect(heatRecoverLT_h.C1, sPloss4.C2)
    annotation (Line(points={{-88,10},{34,10}}, color={0,0,0}));
  connect(wInlet.C, heatRecoverLT_w.C1) annotation (Line(points={{-196,-42},{-168,
          -42},{-168,-45},{-162,-45}},
                                   color={0,0,255}));
  connect(wPloss1.C1, heatRecoverLT_w.C2) annotation (Line(points={{-112,-44},{
          -114,-45},{-138,-45}},    color={0,0,255}));
  connect(eHeater.Cs1, toMSL.Input_TSP) annotation (Line(points={{244,-46},{244,
          -45.1},{264.1,-45.1}},     color={0,0,0}));
  connect(toMSL.Output_Modelica, toStack) annotation (Line(points={{276.2,-44.8},
          {318,-44.8},{318,-45},{358,-45}},                  color={0,127,255}));
  connect(fromMSL.Input_Modelica, fromStack) annotation (Line(points={{224,44.8},
          {226,44},{358,44}},                     color={0,127,255}));
  connect(h2rec.IMassFlow, Qh2rec.y)
    annotation (Line(points={{50,-73},{50,-66},{36.4,-66}}, color={0,0,255}));
  connect(sensorQ.Measure, Qh2rec.u) annotation (Line(points={{0,-37.92},{0,-32},
          {26,-32},{26,-66},{27.6,-66}}, color={0,0,255}));
  connect(Wall_HR_LT.WT2, heatRecoverLT_h.CTh) annotation (Line(points={{-119,-12},
          {-120,-12},{-120,4.6},{-100,4.6}}, color={0,0,0}));
  connect(Wall_HR_LT.WT1, heatRecoverLT_w.CTh) annotation (Line(points={{-119,-18},
          {-120,-18},{-120,-30},{-150,-30},{-150,-39.3}}, color={0,0,0}));
  connect(heatRecoverHT_w.CTh, Wall_HR_HT.WT1) annotation (Line(points={{176,-40.6},
          {176,-36},{141,-36},{141,-18}}, color={0,0,0}));
  connect(Wall_HR_HT.WT2, heatRecoverHT_h.CTh) annotation (Line(points={{141,-12},
          {140,-12},{140,4.6},{136,4.6}}, color={0,0,0}));
  connect(sensorP.Measure, wOutlet.IPressure) annotation (Line(points={{57,
          -37.9},{57,-34},{28,-34},{28,-46},{23,-46}}, color={0,0,255}));
  connect(sensorQ.Measure, DQ.u2) annotation (Line(points={{0,-37.92},{0,-4},{-72,
          -4},{-72,54},{-104,54},{-104,69}}, color={0,0,255}));
  connect(DQ.y, Q_control.u)
    annotation (Line(points={{-93,80},{-87,80}}, color={0,0,255}));
  connect(defaultOpening.y, Opening.u2) annotation (Line(points={{-13,28},{-8,28},
          {-8,30},{-6,30},{-6,48},{1,48}}, color={0,0,255}));
  connect(Q_control.y, Opening.u1) annotation (Line(points={{-65,80},{-62,80},{
          -62,78},{-42,78},{-42,60},{1,60}}, color={0,0,255}));
  connect(Opening.y, SteamAdmission.Ouv)
    annotation (Line(points={{23,54},{82,54},{82,-33.2}}, color={0,0,255}));
  connect(DT.y, T_control.u)
    annotation (Line(points={{189,-94},{207,-94}}, color={0,0,255}));
  connect(DT.u2, wirelessSensor.y) annotation (Line(points={{178,-105},{178,-126},
          {156.2,-126}}, color={0,0,255}));
  connect(T_control.y, elec.ISignal) annotation (Line(points={{229,-94},{252,-94},
          {252,-83}}, color={0,0,255}));
  connect(StackTargetT, DT.u1) annotation (Line(points={{100,-100},{162,-100},
          {162,-94},{167,-94}}, color={0,0,255}));
  connect(ePower, ePower)
    annotation (Line(points={{304,-100},{304,-100}}, color={0,0,255}));
  connect(heatRecoverHT_w.C2, sPloss2.C1)
    annotation (Line(points={{188,-46},{196,-46}}, color={0,0,0}));
  connect(sPloss2.C2, eHeater.Ce1)
    annotation (Line(points={{216,-46},{224,-46}}, color={0,0,0}));
  connect(wPloss1.C2, boiler.C1) annotation (Line(points={{-92,-44},{-92,-43},{
          -78,-43}},           color={0,0,255}));
  connect(DQ.u1, limiteur.y)
    annotation (Line(points={{-115,80},{-159,80}}, color={0,0,255}));
  connect(limiteur.u, FuelQ)
    annotation (Line(points={{-181,80},{-200,80},{-200,78},{-218,78}},
                                                   color={0,0,255}));
  connect(DT1.y,T_control1. u)
    annotation (Line(points={{-93,-110},{-73,-110}}, color={0,0,255}));
  connect(wirelessSensor1.y,DT1. u2) annotation (Line(points={{-115.8,-144},{-104,
          -144},{-104,-121}},                              color={0,0,255}));
  connect(T_control1.y,elec1. ISignal) annotation (Line(points={{-51,-110},{-34,
          -110},{-34,-97}},                        color={0,0,255}));
  connect(wirelessSensor2.y,DT1. u1) annotation (Line(points={{-127.8,-110},{-115,
          -110}},                              color={0,0,255}));
  connect(elec1.C[1], volumeBoiler.Cth)
    annotation (Line(points={{-34,-82.2},{-34,-46}}, color={0,0,0}));
  connect(volumeBoiler.Cs1, sensorT.C1) annotation (Line(points={{-26,-46},{-26,
          -45},{-18,-45}}, color={0,0,255}));
  connect(boiler.C2, volumeBoiler.Ce1) annotation (Line(points={{-54,-43},{-54,
          -42},{-50,-42},{-50,-46},{-42,-46}}, color={0,0,255}));
  connect(WallBoiler.WT2, boiler.CTh)
    annotation (Line(points={{-66,-24},{-66,-37.3}}, color={0,0,0}));
  connect(WallBoiler.WT1, extHeat) annotation (Line(points={{-66,-20},{-66,64},
          {-50,64},{-50,100}}, color={0,0,0}));
  connect(fromMSL.Output_TSP, h2volume.Ce1) annotation (Line(points={{212.1,44.9},
          {190,44.9},{190,32},{186,32}}, color={0,0,0}));
  connect(h2volume.Cs1, heatRecoverHT_h.C1) annotation (Line(points={{166,32},{
          156,32},{156,10},{148,10}}, color={0,0,0}));
  connect(TotalHeater.y, ePower) annotation (Line(points={{263,-144},{304,-144},
          {304,-100}}, color={0,0,255}));
  connect(T_control1.y, TotalHeater.u2) annotation (Line(points={{-51,-110},{
          84,-110},{84,-150},{241,-150}}, color={0,0,255}));
  connect(T_control.y, TotalHeater.u1) annotation (Line(points={{229,-94},{236,
          -94},{236,-138},{241,-138}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(                           extent={{-220,-100},
            {120,100}}), graphics={
        Rectangle(
          extent={{-220,100},{120,-100}},
          lineColor={28,108,200},
          lineThickness=0.5),
        Rectangle(
          extent={{-220,52},{92,34}},
          lineColor={255,170,85},
          lineThickness=1,
          fillColor={230,230,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-218,-40},{-38,-60}},
          lineColor={28,108,200},
          lineThickness=1,
          fillColor={28,108,200},
          fillPattern=FillPattern.Forward),
        Line(
          points={{-130,66},{-164,66},{-156,74},{-164,66},{-156,58},{-164,66},{
              -130,66}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{60,66},{26,66},{34,74},{26,66},{34,58},{26,66},{60,66}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-36,66},{-70,66},{-62,74},{-70,66},{-62,58},{-70,66},{-36,66}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-164,-76},{-130,-76},{-138,-68},{-130,-76},{-138,-84},{-130,
              -76},{-164,-76}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-68,-76},{-34,-76},{-42,-68},{-34,-76},{-42,-84},{-34,-76},{
              -68,-76}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{26,-76},{60,-76},{52,-68},{60,-76},{52,-84},{60,-76},{26,-76}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-60,24},{-40,10},{-76,-10},{-52,-26}},
          color={223,142,28},
          smooth=Smooth.Bezier,
          thickness=1),
        Line(
          points={{-42,24},{-22,10},{-58,-10},{-34,-26}},
          color={223,142,28},
          smooth=Smooth.Bezier,
          thickness=1),
        Line(
          points={{-24,24},{-4,10},{-40,-10},{-16,-26}},
          color={223,142,28},
          smooth=Smooth.Bezier,
          thickness=1),
        Text(
          extent={{-226,34},{-66,-34}},
          textColor={0,0,0},
          textString="HTSE",
          textStyle={TextStyle.Italic}),
        Text(
          extent={{-38,34},{122,-34}},
          textColor={0,0,0},
          textStyle={TextStyle.Italic},
          textString="BoP"),
        Rectangle(
          extent={{-38,-40},{90,-60}},
          lineColor={28,108,200},
          lineThickness=1,
          fillColor={0,255,255},
          fillPattern=FillPattern.Forward)}),                    Diagram(
        coordinateSystem(                           extent={{-220,-100},{360,100}}),
                    graphics={
        Text(
          extent={{-30,118},{54,88}},
          textColor={28,108,200},
          fontSize=28,
          textString="External 
heat source"),
        Text(
          extent={{-234,-56},{-150,-86}},
          textColor={28,108,200},
          fontSize=28,
          textString="Water
Inlet"),Text(
          extent={{-236,56},{-152,26}},
          textColor={28,108,200},
          fontSize=28,
          textString="H2
outlet")}));
end BOP_module_1Dboiler;
