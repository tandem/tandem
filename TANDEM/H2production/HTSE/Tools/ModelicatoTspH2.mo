within TANDEM.H2production.HTSE.Tools;
model ModelicatoTspH2
  import HTSE;
  // Fluid definition
  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2       "Medium of the CEA model";
//  parameter Boolean allowFlowReversal = false;
  parameter Real m_flow_nominal = 1;

  Modelica.Fluid.Interfaces.FluidPort_a Input_Modelica(
    p(start=Medium.p_default),
    redeclare package Medium = Medium,
    h_outflow(start=Medium.h_default, nominal=Medium.h_default))
    "Fluid connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-70,-22},{-50,-2}})));

  ThermoSysPro_H2.Fluid.Interfaces.Connectors.FluidOutlet Output_TSP
    annotation (Placement(transformation(extent={{54,-16},{64,-6}}, rotation=0),
        iconTransformation(extent={{54,-16},{64,-6}})));
  Modelica.Fluid.Sources.Boundary_ph  sin(
    redeclare package Medium = Medium,
    use_p_in=true,
    p(displayUnit="Pa") = 101325,
    use_h_in=false,
    nPorts=1) "Inlet for fluid flow" annotation (Placement(transformation(extent={{14,-34},{-2,-18}})));
  SourceQ_Xvar                                       sourceQ_Xvar(
                                                             ftype=
        ThermoSysPro_H2.Fluid.Interfaces.PropertyInterfaces.FluidType.H2mixGases,
    option_temperature=true,
    Xco2=0,
    Xh2o=TIn.port_b.Xi_outflow[1],
    Xo2=0,
    Xh2=TIn.port_b.Xi_outflow[2],
    T(fixed=false))                                          annotation (Placement(transformation(extent={{16,2},{36,22}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
                                          adaptorModelicaTSP annotation (Placement(transformation(extent={{-4,-6},{12,8}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
                                          adaptorTSPModelica1 annotation (Placement(transformation(extent={{42,-32},{28,-20}})));
  ThermoSysPro_H2.Fluid.Sensors.SensorP   sensorP    annotation (Placement(transformation(extent={{6,-5},{-6,5}},
        rotation=90,
        origin={51,10})));
  Modelica.Fluid.Sensors.TemperatureTwoPort  TIn(redeclare package Medium = Medium,
    allowFlowReversal=false,                                                        m_flow_nominal=m_flow_nominal,
    m_flow_small=1e-10)                           "Temperature sensor"
    annotation (Placement(transformation(extent={{-46,-34},{-30,-20}})));
  Modelica.Fluid.Sensors.MassFlowRate        TIn1(redeclare package Medium = Medium, allowFlowReversal=false,
    m_flow_nominal=m_flow_nominal,
    m_flow_small=1e-10)                           "Temperature sensor"
    annotation (Placement(transformation(extent={{-24,-34},{-8,-18}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
                                          adaptorModelicaTSP1 annotation (Placement(transformation(extent={{-2,18},{14,28}})));

equation

//    Xco2=0.0;
//    Xh2o=Input_Modelica.Xi_outflow[1];
//    Xo2=0.0;
//    Xh2=0.0;

//    Xco2=0.0;
//    Xh2o=0.2;
//    Xo2=0.0;
//    Xh2=0.8;

  connect(sensorP.Measure, adaptorTSPModelica1.inputReal)
    annotation (Line(points={{45.9,10},{44,10},{44,-26},{42.7,-26}}, color={28,108,200}));
  connect(TIn.port_b, TIn1.port_a) annotation (Line(points={{-30,-27},{-30,-26},{-24,-26}}, color={0,127,255}));
  connect(TIn1.port_b, sin.ports[1]) annotation (Line(points={{-8,-26},{-2,-26}}, color={0,127,255}));
  connect(TIn1.m_flow, adaptorModelicaTSP1.u) annotation (Line(points={{-16,-17.2},{-16,22},{-3.6,22},{-3.6,23}}, color={0,0,127}));
  connect(adaptorModelicaTSP1.outputReal, sourceQ_Xvar.IMassFlow)
    annotation (Line(points={{14.8,23},{26,23},{26,17}}, color={28,108,200}));
  connect(sourceQ_Xvar.C, sensorP.C1) annotation (Line(points={{36,12},{36,26},
          {55,26},{55,16}}, color={0,0,255}));
  connect(Input_Modelica, TIn.port_a) annotation (Line(points={{-60,-12},{-46,-12},{-46,-27}}, color={0,127,255}));
  connect(sensorP.C2, Output_TSP)
    annotation (Line(points={{55,3.88},{55,-11},{59,-11}}, color={0,0,255}));
  connect(sin.p_in, adaptorTSPModelica1.y) annotation (Line(points={{15.6,-19.6},{26.6,-19.6},{26.6,-26}}, color={0,0,127}));
  connect(TIn.T, adaptorModelicaTSP.u)
    annotation (Line(points={{-38,-19.3},{-38,1},{-5.6,1}}, color={0,0,127}));
  connect(adaptorModelicaTSP.outputReal, sourceQ_Xvar.ISpecificEnthalpyOrTemperature)
    annotation (Line(points={{12.8,1},{26,1},{26,7}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-60,-60},{60,40}})), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-60,-60},{60,40}})));
end ModelicatoTspH2;
