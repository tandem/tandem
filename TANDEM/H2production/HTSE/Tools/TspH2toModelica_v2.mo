within TANDEM.H2production.HTSE.Tools;
model TspH2toModelica_v2
  import HTSE;
    // Fluid definition
  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2                          "Medium of the CEA model";

//    parameter ThermoSysPro_H2.Units.SI.MassFraction Xh2o(start=0.9)
//      "H2O mass fraction of the fluid crossing the boundary of the control volume";
//    parameter ThermoSysPro_H2.Units.SI.MassFraction Xh2(start=0.1)
//      "H2 mass fraction of the fluid crossing the boundary of the control volume";
 //  Medium.MassFraction X[Medium.nX]( start = Medium.X_default)
 //   "Fixed value of composition";

  ThermoSysPro_H2.Fluid.BoundaryConditions.SinkP sinkP
    annotation (Placement(transformation(extent={{-36,-22},{-16,-2}})));
  ThermoSysPro_H2.Fluid.Interfaces.Connectors.FluidInlet Input_TSP annotation (
      Placement(transformation(extent={{-64,-16},{-54,-6}}, rotation=0),
        iconTransformation(extent={{-64,-16},{-54,-6}})));
  Modelica.Fluid.Interfaces.FluidPort_b Output_Modelica(
     redeclare package Medium = Medium) annotation (Placement(transformation(extent={{52,-18},{72,2}})));

  Modelica.Fluid.Sources.MassFlowSource_T source(
    redeclare package Medium = Medium,
    use_m_flow_in=true,
    use_T_in=true,
    use_X_in=true,
    nPorts=1) "Inlet for water flow" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={26,2})));
    //X={Xh2o,Xh2},
  Modelica.Blocks.Sources.RealExpression Xi[3](y={Input_TSP.Xh2o,Input_TSP.Xh2,
        0}) annotation (Placement(transformation(extent={{-22,20},{-2,40}})));
  Modelica.Blocks.Sources.RealExpression Q(y=Input_TSP.Q)
    annotation (Placement(transformation(extent={{-14,-28},{6,-8}})));
  Modelica.Blocks.Sources.RealExpression P(y=Output_Modelica.p)
    annotation (Placement(transformation(extent={{52,-50},{32,-30}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
    adaptorModelicaTSP
    annotation (Placement(transformation(extent={{8,-50},{-12,-30}})));
  ThermoSysPro_H2.Fluid.Sensors.SensorT sensorT
    annotation (Placement(transformation(extent={{-52,-12},{-42,0}})));
  Modelica.Blocks.Sources.RealExpression T(y=sensorT.Measure.signal)
    annotation (Placement(transformation(extent={{-28,-4},{-8,16}})));
equation
//   Xh2o = Input_TSP.Xh2o;
//   Xh2 = Input_TSP.Xh2;
  //X={Input_TSP.Xh2o,Input_TSP.Xh2};

  connect(source.ports[1], Output_Modelica) annotation (Line(points={{36,2},{
          48,2},{48,-8},{62,-8}},
                               color={0,127,255}));
  connect(Xi.y, source.X_in) annotation (Line(points={{-1,30},{10,30},{10,6},{
          14,6}}, color={0,0,127}));
  connect(source.m_flow_in, Q.y)
    annotation (Line(points={{16,-6},{16,-18},{7,-18}}, color={0,0,127}));
  connect(P.y, adaptorModelicaTSP.u)
    annotation (Line(points={{31,-40},{10,-40}}, color={0,0,127}));
  connect(adaptorModelicaTSP.outputReal, sinkP.IPressure) annotation (Line(
        points={{-13,-40},{-18,-40},{-18,-38},{-21,-38},{-21,-12}}, color={0,0,
          255}));
  connect(sinkP.C, sensorT.C2) annotation (Line(points={{-36,-12},{-36,-10.8},
          {-41.9,-10.8}}, color={0,0,0}));
  connect(sensorT.C1, Input_TSP) annotation (Line(points={{-52,-10.8},{-55.5,
          -10.8},{-55.5,-11},{-59,-11}}, color={0,0,0}));
  connect(source.T_in, T.y) annotation (Line(points={{14,-2},{-4,-2},{-4,6},{
          -7,6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-60,-60},{60,40}})), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-60,-60},{60,40}})));
end TspH2toModelica_v2;
