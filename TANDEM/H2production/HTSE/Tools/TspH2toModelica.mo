within TANDEM.H2production.HTSE.Tools;
model TspH2toModelica
  import HTSE;
    // Fluid definition
  replaceable package Medium =
      HTSE.Media.Predefined.Mixture.Gas.IdealGasMixture_H2O_H2_N2                          "Medium of the CEA model";

  parameter Modelica.Units.SI.AbsolutePressure DecouplingPressure=1 "Decoupling Pressure";
  parameter Boolean DecouplePressure=false "Decouple upstream from downstream pressure";

//    parameter ThermoSysPro_H2.Units.SI.MassFraction Xh2o(start=0.9)
//      "H2O mass fraction of the fluid crossing the boundary of the control volume";
//    parameter ThermoSysPro_H2.Units.SI.MassFraction Xh2(start=0.1)
//      "H2 mass fraction of the fluid crossing the boundary of the control volume";
 //  Medium.MassFraction X[Medium.nX]( start = Medium.X_default)
 //   "Fixed value of composition";

  ThermoSysPro_H2.Fluid.BoundaryConditions.SinkP sinkP(P0=DecouplingPressure)
    annotation (Placement(transformation(extent={{-10,-32},{10,-12}})));
  ThermoSysPro_H2.Fluid.Sensors.SensorQ   sensorQ
    annotation (Placement(transformation(extent={{6,-5},{-6,5}},
        rotation=90,
        origin={-31,-12})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
                                          adaptorTSPModelica2 annotation (Placement(transformation(extent={{-14,-50},{4,-32}})));
  ThermoSysPro_H2.Fluid.Sensors.SensorT   sensorT
    annotation (Placement(transformation(extent={{6,5},{-6,-5}},
        rotation=90,
        origin={-35,6})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
                                          adaptorTSPModelica1 annotation (Placement(transformation(extent={{-18,2},{0,20}})));
  ThermoSysPro_H2.Fluid.Interfaces.Connectors.FluidInlet Input_TSP annotation (
      Placement(transformation(extent={{-64,-16},{-54,-6}}, rotation=0),
        iconTransformation(extent={{-64,-16},{-54,-6}})));
  Modelica.Fluid.Interfaces.FluidPort_b Output_Modelica(
     redeclare package Medium = Medium) annotation (Placement(transformation(extent={{52,-18},{72,2}})));

  Modelica.Fluid.Sources.MassFlowSource_T source(
    redeclare package Medium = Medium,
    use_m_flow_in=true,
    use_T_in=true,
    use_X_in=true,
    nPorts=2) "Inlet for water flow" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={26,2})));
    //X={Xh2o,Xh2},
  Modelica.Blocks.Sources.RealExpression realExpression[3](y={Input_TSP.Xh2o,
        Input_TSP.Xh2, 0})
    annotation (Placement(transformation(extent={{-22,20},{-2,40}})));
  Modelica.Fluid.Sensors.Pressure            TIn1(redeclare package Medium =
        Medium)                                   "Temperature sensor"
    annotation (Placement(transformation(extent={{40,10},{56,26}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorModelicaTSP
                                          adaptorModelicaTSP  annotation (Placement(transformation(extent={{78,12},
            {96,30}})));
equation

  if not DecouplePressure then
  connect(adaptorModelicaTSP.outputReal, sinkP.IPressure) annotation (Line(
        points={{96.9,21},{108,21},{108,-22},{5,-22}}, color={0,0,255}));
  end if;

//   Xh2o = Input_TSP.Xh2o;
//   Xh2 = Input_TSP.Xh2;
  //X={Input_TSP.Xh2o,Input_TSP.Xh2};

  connect(sensorQ.Measure, adaptorTSPModelica2.inputReal)
    annotation (Line(points={{-36.1,-12},{-40,-12},{-40,-41},{-14.9,-41}}, color={28,108,200}));
  connect(sensorT.C2, sensorQ.C1) annotation (Line(points={{-39,-0.12},{-27,-0.12},{-27,-6}}, color={0,0,255}));
  connect(sensorT.Measure,adaptorTSPModelica1. inputReal)
    annotation (Line(points={{-29.9,6},{-24,6},{-24,11},{-18.9,11}},
                                                                 color={28,108,200}));
  connect(sinkP.C, sensorQ.C2) annotation (Line(points={{-10,-22},{-27,-22},{-27,
          -18.12}}, color={28,108,200}));
  connect(source.ports[1], Output_Modelica) annotation (Line(points={{36,2.25},
          {48,2.25},{48,-8},{62,-8}},
                               color={0,127,255}));
  connect(adaptorTSPModelica2.y, source.m_flow_in) annotation (Line(points={{5.8,
          -41},{12,-41},{12,-6},{16,-6}}, color={0,0,127}));
  connect(adaptorTSPModelica1.y, source.T_in) annotation (Line(points={{1.8,11},
          {8,11},{8,-2},{14,-2}}, color={0,0,127}));
  connect(Input_TSP, sensorT.C1) annotation (Line(points={{-59,-11},{-48,-11},{-48,
          22},{-39,22},{-39,12}}, color={0,0,0}));
  connect(realExpression.y, source.X_in)
    annotation (Line(points={{-1,30},{10,30},{10,6},{14,6}}, color={0,0,127}));
  connect(source.ports[2], TIn1.port) annotation (Line(points={{36,1.75},{38,1.75},
          {38,6},{46,6},{46,10},{48,10}}, color={0,127,255}));
  connect(TIn1.p, adaptorModelicaTSP.u) annotation (Line(points={{56.8,18},{64,
          18},{64,21},{76.2,21}}, color={0,0,127}));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-60,-60},{60,40}})), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-60,-60},{60,40}})));
end TspH2toModelica;
