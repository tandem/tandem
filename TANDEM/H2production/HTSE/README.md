# High Temperature Steam Electrolyser 

## Component Description
The HTSE component produces H2 in a *Solid Oxyde Electrolyser Cell*, operating at high temperature (about 750 °C). To do so the _fuel_, steam with a small amount of hydrogen, is produced and pre-heated using thermal power coming from an external source, i.e. a SMR plant.

The HTSE component may be divided in two sub-components:
- The **Stack**, where the electrolysis of the steam takes place.
- The **HTSE-BoP**, which provide the *fuel* for the reaction. The BoP has also the role of recuperating the heat from the stack outlet gases to preheat the fuel and cool the produced hydrogen.

### Circuit Description
In the _feed_ line the fuel, i.e. the water, goes through several heat-exchangers to reach the target temperature:
1. A water pre-heater, fed, in terms of thermal power, by the _exhaust_ gases.
1. A boiler, fed by the external heat source.
1. A steam pre-heater, fed by the exhaust gases.
1. An electrical heater.

Between 2 and 3, the steam is mixed with recycled hydrogen (10% of molar fraction). To guarantee no humidity in the steam, an auxiliary electrical boiler is also present and used in case of absence of external heat source.   

The steam enters then the stack where part of it is converted to hydrogen. The produced gases flow back to the pre-heater 3 and the pre-heater 1 to cool down. The hydrogen is then separated from the residual steam/humidity, compressed and sent to some storage.


## Model description
The HTSE model is composed by two separate sub-components: the stack and the BoP.  


### The Stack
The stack model assumptions can be divided in two parts :
- **The conversion assumptions** : the model for the conversion of hydrogen is described in the scientific publication of [Laurencin](#references).
The overall assumptions are : 
    - Cells are stricly identicals. No effect of the position of the cell are taken into account.
    - The description is one dimensional, in particular, we consider that all the channels providing gas to the cells are identical and no difference is made between the edge and the center of the cell (where in reality some difference would appear especially thermally).
    - The temperature is therefore homogenous along all the cell, and is piloted using a PID to respect the [thermoneutral voltage](https://en.wikipedia.org/wiki/Thermoneutral_voltage) ensuring the most efficient behavior for hydrogen conversion. We could summarize by saying the cells and the stack is perfectly controlled in temperature. 
    - The flow of gas is co-linear, the gases flows from the cathode and anode are parallel and in the same direction. Only this model is provided but other type of flow can be implemented in the structure.
    - The cells data comes from the paper cited above. They can be easily replaced by more efficient and state of the art cells using the same format.
    - The pressure loss are linear and  averaged (the pressure is not described in a one dimensional array).
    - The media used is a perfect mixture of hydrogen, steam and nitrogen (unused in the model here).
- **The encapsulation assumptions** : 
    - A mass flow of pure hydrogen is provided in case no fuel is provided from the BOP. No current is used while the mass flow rate remains less then 10% of the nominal one. The main idea is to maintain gas inside the electrolyser even when it cannot be used due to nominal woking range failure.
    - No current is used while the mass flow rate remains less then 10% of the nominal one.
    - The anode gas is standard air, the mass flow rate is two times the Fuel mass flow rate, as it is often seen in the literature.
    - Electrical power is computed as the product of the current and voltage provided to the overall stack. A conversion efficiency of 0.9 is taken into account from the grid current.
    - The current is computed at all times to convert 70% of the steam provided in the Fuel_In connector. This steam conversion is quite standard for a normal working SOEC. A delay of 60s is introduced for the current to synchronise with the mass flow rate. (A valid hypothesis compared to reality also making the numerical resolution easier temporally decoupling the current and the steam flow).


Here is a summary of the different model descriptions found in the different model of the stack; further detail can be found in the Modelica documentation of each subcomponent.  

**the stack model**
HTSE stack model
in/out : 
cat_in_port, cat_out_port, an_in_port, an_out_port, p, n

parameters :
n_cells : number of cells per stack
n_stack : total number of stacks


description :
This model allows for simulating the electrochemical, thermal, and fluidic behavior of a Solid Oxide Electrolysis Cell (SOEC), composed of a stack of elementary cells connected in series, subjected to a current source and supplied by two circuits: the air side at the anode, and a mixture of H2O/H2 called the fuel side at the cathode.
The model is interfaced with 4 fluid ports (in anode, out anode, in cathode, out cathode) and 2 electrical pins (p and n) to provide electrical power to the electrolyzer. Both current or voltage source can be used but a current source has to be preferred.
The model is discretized along the gas channel to better represent the current density repartition inside the cell. The model aggregates various sub-models and components that communicate with each other through outer variables :

- The fluidic model : transport equations and link to the fluid ports. the sub model is mostly composed of equations that describes either a co-flow/counter-flow/cross-flow type of fluid movement and the interactions between molar fractions of gas components and current density. The electrolyte crossing of gas components is handled here.
- The electro chemical model : equation between current density, molar fractions and voltages. The main outputs are the cell voltage and the current density repartition along the 1D/2D cell.
- The thermic model : computes the temperature along the electrolyzer cell. 
- The pressure model : computes the pressure drop along the cell for various fluid movements. 
- A data record that provides material information and other data. Separate the cell type from the rest of the Model.

The sub model are described in their own description sections. The choices made for the TANDEM library are : 
- to keep the electro chemical model physical with real equations from the literature. 
- to have a co-flow fluid movement. it means that gas in the anode and cathode channel are parallel and flows in the same direction. It is the most simple way to transport gas in the cell. Also ignore second order complications.
- the temperature is computed to maintain the cell voltage to follow thermoneutral voltage using a basic PID. 
- Pressure losses are linear and averaged in the cell.
### The HTSE-BoP
The *Balance of Plant* modeling is essentially a thermo-hydraulic modeling, based on the ThermoSysPro library. The detailed documentation of the library is available in the book of [El Hefni and Bouskela](#ElHefni) and on the library [website](https://thermosyspro.com/).

The BoP is modeled from the liquid water coming from the main pump (not included) to the separator of the exhaust gases (not included).

The heat-exchangers, the pre-heaters and the boiler, are modeled as meshed 1D pipes with staggered mesh scheme: the ThermoSysPro component used is `DynamicOnePhaseFlowPipe`. Volumes are also used to model:
- the mixing of steam and hydrogen before the last pre-heater;
- the final and te auxiliary electrical heaters.

The main assumptions of the BoP model can be synthesized as follows:
- Thermal inertia is taken into account. This is not the case for the mass flow inertia because of some incompatibilities with the stack modelling; mass flow inertia is supposed to have little impact on the results for the target studies. 
- Correlations are used to evaluate the heat transfer coefficients and the pressure drops so that they depend on the mass flow rate (e.g. Dittus-Boelter for heat transfer; see [El Hefni and Bouskela](#ElHefni) for further details).
- There is no mass accumulation in the model (e.g. thermal dilatation is neglected).
- A minimal value of mass flow rate of 0.05 kg/s is hardcoded for stability reason.
- For compatibility reasons, the pressure in *decoupled* at the inlet of the stack, as if there were a perfect valve controlling the pressure at the stack. This is supposed to have no significant impact on results, taking into account the low operating pressure of the stack. 

### Parameters
The model is designed for a nominal fuel mass flow rate of 1 kg/s. It is supposed to start with no hydrogen production (null stack current, 0.05 kg/s of mass flow rate in the circuit) and no external heat provided.

The BoP design is not *optimized*, since such optimization would depend on the application (e.g. the desired H2 mass flow rate, the available external steam characteristics...). The model being essentially composed by heat exchangers, the main *design* parameters are *Diameters*, *Length* and *Number of Pipes*.

### Fluids
Several fluids are used in the circuit:
- `ThermoSysPro.WaterSteam` (IAPWS-IF97) is used from the inlet to the boiler (included).
- `ThermoSysPro.H2mixGases` is used from the mixing between steam and recycled H2 to the electrical heater.
- Adaptors are then used to connect the BoP to the Stack inlet and outlet, which use media defined as standard media of the Modelica Standard Library.
- `ThermoSysPro.H2mixGases` is then used from the stack outlet to the BoP outlet.


## Control Strategy
The component is operated to achieve a requested production of H2: this target is the main **input** for the I&C of the component.

The target H2 production is compared to the actual H2 production (measured @ the outlet of the BoP) and a PI controller is used to set the target fuel mass flow rate.

This information is processed in the BoP by a PI to properly open the steam admission valve.
In the BoP, a second PI adjust the power sent to the electrical heater to meet the target temperature at the stack inlet; this target is provided by the stack module to maximize the efficiency of the electrolysis. 

At the stack level, the current is set to be proportional to the fuel mass flow rate, with a delay of 60 seconds. 

This global control logic is illustrated by the following diagram.

```mermaid
%%{init: {'theme': 'base', 'themeVariables': { 'fontSize': '48px'}}}%%
flowchart TD
input[input: H2 production] --PI--> steam[Target Fuel Mass Flow Rate]
subgraph BoP
steam --PI--> valve[Steam Admission Valve]
valve -.-> realSteam[Fuel Mass Flow Rate]
Wel
Tsteam[Min Steam Temperature] --PI--> Aux[Auxiliary Boiler Power]:::power
end
subgraph Stack
subgraph test[" "]
Voltage
Current
end
Voltage --PID--> T[Target Temperature]
Current -.-> elec[Power Consumed]:::power
Current -.-> Voltage
Voltage -.-> elec
elec ~~~ Voltage
elec ~~~ T
end
realSteam --60s delay--> Current
T --PI--> Wel[Electrical Heater Power]:::power
BoP:::left

style input fill:#C0E410

classDef left padding-right:20em;
classDef power fill:#FF1493,color:#fff
```



## Package Structure
The HTSE package is structured as follows:
- The *HTSE_module*, at the root, is the coupled Stack+BoP component.
- The *Subcomponents* package contains the submodules *BoP_module_1Dboiler* and *Stack* (??? to be confirmed ???).
- The *Tools* package contains some useful components, such as the adaptors to connect ThermoSysPro, with H2 gases, to the MSL blocks. The *Tests* package hosts some examples and checks of these tools.
- The *Demo* package presents some demonstrative uses of the *HTSE_module*:
    - In **Test_HTSE_steam**, the external heat source is represented by hot steam flowing through the external side of the HTSE-BoP boiler. The model is initialized, as requested, with null hydrogen production (resulting in the minimal mass flow rate in the HTSE-BoP, 0.05 kg/s) and negligible but non-null external steam mass flow rate (0.01 kg/s). Then a ramp of both H2 production request and external steam mass flow rate is realized.
    - In **Test_HTSE_heat**, the external heat source is modeled by its injected power. Starting from zero, it is ramped up as in the previous example.  


## References
- <a id="Laurencin"></a> J. Laurencin et al. _Modelling of solid oxide steam electrolyser: Impact of the operating conditions on hydrogen production_, Journal of Power Sources, Volume 196, Issue 4, 2011.
- <a id="ElHefni"></a> B. El Hefni, D. Bouskela. _Modeling and Simulation of Thermal Power Plants with ThermoSysPro: A Theoretical Introduction and a Practical Guide_. Springer, 2019. 

