# Low Temperature Electrolyser
## Component Description

The present component models a Low Temperature Electrolysis (LTE) Plant based on the Proton Exchange Membrane (PEM) technology.

This component is intended to be connected to the TANDEM’s Electrical Grid component through the "term_n" electrical port. To be able to run simulations, the LTE component also needs a real input signal giving the H2 production target. This target is given as an input to a PI controller block which adapt the current intensity applied to the stack module until reaching the H2 production target. As an output, the LTE component provides the real instantaneous H2 production of the plant.  

The stack represents the heart of the LTE plant where the inlet water is converted to H2 and O2. The proposed stack Modelica module is based on four types of equations: 
- The **electrical equations** that link the electrical consumptions of the stack (Pelec) to the stack’s current and voltage. The stack cells are assumed to be connected in series. We note that Pelec doesn’t account for the electrical consumptions of the plant’s auxiliaries such as AC/DC converters, pumps, inlet water conditioning, de-ionized water production unit, gas purification and compression units, and so on. Those previous auxiliaries electrical consumption are taken into account at the LTE component level through the “ElecEfficiency” factor parameter. Finally, the energy efficiency of the electrochemical conversion is given by the “ne” variable.
- The **mass balance equations** which are based on the Faraday’s law. As in [[M. Espinosa-Lopez et al.](https://www.sciencedirect.com/science/article/pii/S0960148117311825)], we assume that the faraday efficiency is equal to 1.
- The **thermal modeling equations** which can be used either in their static form (when the parameter UseDynamicModeling=false) or in their dynamic one (when UseDynamicModeling=true). When static modeling is chosen, the stack temperature is set to the operating temperature (Top). When dynamic modeling is chosen, the user needs to specify the response time characteristic “Tau”, the stack starting temperature “Tamb” (which is assumed to be equal to the ambient temperature) and the stack operating temperature “Top”. The stack’s temperature evolves then following the first order differential equation “Tau*der(T) + T = Top”. In both static and dynamic modeling approaches, the electrolysis heat loss (Qelectrolysis) and the heat loss to ambient (Qloss) are computed. To perform this last calculation, an equivalent thermal resistance (Rth) must be known.
- The **electrochemical equations** which express (using some semi-empirical equations) the cell voltage as the sum of the cell reversible voltage, the ohmic overvoltage and the activation overvoltage. In order to compute the reversible voltage, we use the Nernst equation and the partial pressures Dalton’s law, while assuming that the absolute pressure is the same at anode and cathode outlets, and that the water vapor is saturated at both cathode and anode outlets.
 
### Circuit Description 
The LTE component was developed as a simplified component. The Balance of Plant (BoP) is not represented. We assume that the input water feeds the stack at the convenient mass flow rate (following the desired H2 production target), and that a water conditioning loop exists to regulate the feeding water's temperature around the operating value. 

## Main Modeling Assumptions
- The gas-liquid separators, the demisters as well as the gas purification unit are not modeled. We assume that pure H2 is produced at the cathode outlet and pure O2 is produced at the anode outlet.
- The electrolysis cells are assumed to be series-connected.
- We assume that the absolute pressure is the same at anode and cathode outlets, and that the water vapor is saturated at both cathode and anode outlets.
- Faraday's efficiency is assumed to be equal to 1 [[M. Espinosa-Lopez et al.](https://www.sciencedirect.com/science/article/pii/S0960148117311825)].
- The number of cells is computed based on the MYRTE platform characteristics using a simple rule of three once knowing the LTE plant's nominal H2 production rate.
 
## Main parameters
The LTE component parameters are : 

| Name | Description |
| -- | -- |
| ElecEfficiency | Electrical efficiency factor to take into account the conversion from AC to DC and the LTE auxiliaries consumption |
| PID_Ti | Time constant of the PID integrator block |
| PID_k | PID controller gain |
| QvolNormalNominal_H2_out | Nominal volume flow rate of produced H2 in standard conditions (p=1.01325 bar, T = 273.15K). This quantity is used to compute the number of electrolyser cells (series-connected) |
| p | Operating Pressure |
| Top | Targeted Operating Temperature |
| UseDynamicModeling | Choosing Dynamic or static modeling |
| Tamb | Ambient Temperature = stack starting temperature |
| Tau | Response time characteristic for a first order differential equation : T(t=Tau)=0.63(Top-Tstart) |

## Control Strategy
The LTE component is controlled through a PI block which allows to adapt the current intensity applied to the stack module until reaching the H2 production target.

## Prerequisites
The model uses the [Buildings 10.0](https://github.com/lbl-srg/modelica-buildings) and the [ThermoSysPro 4.0](https://thermosyspro.com/) Modelica libraries.

## Provided Examples

The "Stack_Validation" subpackage contains two examples that allowed to validate the "Stack" module:
- MYRTE_Platform : modeling the stack of the MYRTE platform at nominal conditions [[M. Espinosa-Lopez et al.](https://www.sciencedirect.com/science/article/pii/S0960148117311825)]. 
- MYRTE_Platform_PolarizationCurve : modeling the stack of the MYRTE platform at (P = 35 bar, T = 50°C) and variable current intensity in order to plot the numerical polarization curve and compare it to the experimental one.

The "LTE_Validation" subpackage contains an example that allowed to validate the "LTE" component:
- GridConnection : testing the connection of the LTE component to the electrical grid while targeting a dynamic H2 production rate objective. 