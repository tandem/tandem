within TANDEM.H2production.LTE.LTE_Validation;
model GridConnection

  Components.LTE                                  LTE(
    QvolNormalNominal_H2_out(displayUnit="m3/h") = 0.0083333333333333,
    p(displayUnit="MPa") = 1400000,
    Top=363.15,
    Tamb=296.15,
    UseDynamicModeling=true)
    annotation (Placement(transformation(extent={{12,-12},{54,20}})));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixVol(
    definiteReference=true,
    f=50,
    V=380000) annotation (Placement(transformation(extent={{-62,22},{-30,52}})));
  Modelica.Blocks.Sources.Step H2ProductionTarget(
    height=20,
    offset=10,
    startTime=500)
    annotation (Placement(transformation(extent={{-54,-52},{-24,-22}})));
equation
  connect(fixVol.terminal, LTE.term_n) annotation (Line(points={{-30,37},{0,37},
          {0,5.45455},{7.8,5.45455}}, color={0,120,120}));
  connect(H2ProductionTarget.y, LTE.H2_ProdTarget) annotation (Line(points={{-22.5,
          -37},{0,-37},{0,-4},{7.8,-4},{7.8,-3.27273}}, color={0,0,127}));
  annotation (experiment(StopTime=1000, __Dymola_Algorithm="Dassl"));
end GridConnection;
