within TANDEM.H2production.LTE.Stack_Validation;
model MYRTE_Platform
  Components.stack                                  stack(
    p(displayUnit="bar") = 3500000,
    Top=333.15,
    Tamb=296.15,
    UseDynamicModeling=true)
    annotation (Placement(transformation(extent={{-40,-2},{-12,22}})));
  Modelica.Electrical.Analog.Sources.ConstantCurrent Source(I=400)
                                                                 annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-26,-38})));

  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{28,-36},{48,-16}})));
equation
  connect(Source.n, stack.Anode) annotation (Line(points={{-36,-38},{-90,-38},{
          -90,11.95},{-42.1,11.95}}, color={0,0,255}));
  connect(Source.p, stack.Cathode) annotation (Line(points={{-16,-38},{6,-38},{
          6,12.1},{-10.6,12.1}}, color={0,0,255}));
  connect(ground.p, stack.Cathode) annotation (Line(points={{38,-16},{38,12.1},
          {-10.6,12.1}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,
            -60},{80,80}})),                                     Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-140,-60},{80,80}})),
    experiment(
      StopTime=1000,
      Interval=1,
      __Dymola_Algorithm="Dassl"));
end MYRTE_Platform;
