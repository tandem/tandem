within TANDEM.H2production.LTE.Components;
model LTE

  // LTE parameters

  // Electrical efficiency
  parameter Real ElecEfficiency = 0.8 "Electrical efficiency factor to take into account the conversion from AC to DC and the LTE auxiliaries consumption" annotation (
    Dialog(group = "Global parameters"));

  // PID controller parameters
  parameter Modelica.Units.SI.Time PID_Ti = 0.5 "Time constant of the PID integrator block" annotation (
    Dialog(group = "PID controller parameters"));
  parameter Real PID_k = 1.0 "PID controller gain" annotation (
    Dialog(group = "PID controller parameters"));

  // Operating conditions
  parameter ThermoSysPro.Units.SI.VolumeFlowRate QvolNormalNominal_H2_out = 10/3600 "Nominal volume flow rate of produced H2 in standard conditions (p=1.01325 bar, T = 273.15K). This quantity is used to compute the number of electrolyser cells (series-connected).";
  parameter ThermoSysPro.Units.SI.Pressure p = 14e5 "Operating Pressure" annotation (
    Dialog(group = "Operating conditions"));
  parameter ThermoSysPro.Units.SI.Temperature Top = 273.15 + 90 "Targeted Operating Temperature" annotation (
    Dialog(group = "Operating conditions"));

  // Dynamic simulation parameters
  parameter Boolean UseDynamicModeling = true "Choosing Dynamic or static modeling" annotation (
    Dialog(group = "Dynamic simulation parameters"));
  parameter ThermoSysPro.Units.SI.Temperature Tamb = 273.15 + 23 "Ambient Temperature = stack starting temperature" annotation (
    Dialog(group = "Dynamic simulation parameters"));
  parameter ThermoSysPro.Units.SI.Time Tau = 60 "Response time characteristic for a first order differential equation : T(t=Tau)=0.63(Top-Tstart)" annotation (
    Dialog(group = "Dynamic simulation parameters"));

  TANDEM.H2production.LTE.Components.stack           stack(
    Ncell( fixed = false),
    p(displayUnit="MPa") = p,
    Top=Top,
    Tamb=Tamb,
    UseDynamicModeling=UseDynamicModeling,
    Tau=Tau) annotation (Placement(transformation(extent={{-40,-2},{-12,22}})));
  Modelica.Electrical.Analog.Sources.SignalCurrent   Source      annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-26,-38})));

  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{28,-36},{48,-16}})));
  Modelica.Blocks.Continuous.LimPID PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=PID_k,
    Ti=PID_Ti,
    yMax=2000,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y_start=1)
    annotation (Placement(transformation(extent={{-72,-74},{-52,-54}})));
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n term_n
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}}),
        iconTransformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealInput H2_ProdTarget
    "H2 Volume Flow rate target at standard conditions (in Nm3/h unit)"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}}),
        iconTransformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealOutput H2_Prod
    "Producted H2 Volume Flow rate at standard conditions (in Nm3/h unit)"
    annotation (Placement(transformation(extent={{100,-80},{140,-40}}),
        iconTransformation(extent={{100,-80},{140,-40}})));

protected
  Integer Ncell_MYRTE = 60 "Nominal number of cells for the MYRTE Platform";
  ThermoSysPro.Units.SI.VolumeFlowRate QvolNormal_H2_out_MYRTE = 10/3600 "Nominal volume flow rate of produced H2 in standard conditions (p=1.01325 bar, T = 273.15K).";

initial equation
   stack.Ncell = integer(floor(Ncell_MYRTE * QvolNormalNominal_H2_out / QvolNormal_H2_out_MYRTE));

equation
  term_n.v[1] * term_n.i[1] + term_n.v[2] * term_n.i[2] = stack.Pelec / ElecEfficiency; // Active power = stack.Pelec / ElecEfficiency
  term_n.v[2] * term_n.i[1] - term_n.v[1] * term_n.i[2] = 0; // Reactive power = 0

  connect(Source.n, stack.Anode) annotation (Line(points={{-36,-38},{-90,-38},{
          -90,11.95},{-42.1,11.95}}, color={0,0,255}));
  connect(Source.p, stack.Cathode) annotation (Line(points={{-16,-38},{6,-38},{
          6,12.1},{-10.6,12.1}}, color={0,0,255}));
  connect(ground.p, stack.Cathode) annotation (Line(points={{38,-16},{38,12.1},
          {-10.6,12.1}}, color={0,0,255}));
  connect(PID.y, Source.i)
    annotation (Line(points={{-51,-64},{-26,-64},{-26,-50}}, color={0,0,127}));
  connect(stack.H2_VolFlowRateProd, H2_Prod) annotation (Line(points={{-26,24.7},
          {-26,46},{90,46},{90,-60},{120,-60}}, color={0,0,127}));
  connect(H2_ProdTarget, PID.u_s) annotation (Line(points={{-120,-60},{-98,-60},
          {-98,-64},{-74,-64}},  color={0,0,127}));
  connect(stack.H2_VolFlowRateProd, PID.u_m) annotation (Line(points={{-26,24.7},
          {-26,32},{58,32},{58,-96},{-62,-96},{-62,-76}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -120},{100,100}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,
            100}})),
    experiment(
      StopTime=1000,
      Interval=1,
      __Dymola_Algorithm="Dassl"));
end LTE;
