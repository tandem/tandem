﻿within TANDEM.H2production.LTE.Components;
model stack

  Modelica.Electrical.Analog.Interfaces.PositivePin Anode annotation (Placement(
        transformation(extent={{-122,-26},{-80,16}}), iconTransformation(extent=
           {{-126,-18},{-104,4}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin Cathode annotation (
      Placement(transformation(extent={{80,-30},{120,10}}), iconTransformation(
          extent={{100,-16},{120,4}})));

  // Parameters of the model
  parameter ThermoSysPro.Units.SI.Area A_cell=0.29 "Electrolyis cell area";
  parameter Integer Ncell=60 "Number of cells";
  parameter ThermoSysPro.Units.SI.Thickness delta_mem=178*10^(-6) "Membrane thickness";
  parameter ThermoSysPro.Units.SI.Pressure p = 14e5 "Operating Pressure";
  parameter ThermoSysPro.Units.SI.Temperature Top = 273.15 + 90 "Targeted Operating Temperature";
  parameter ThermoSysPro.Units.SI.Temperature Tamb = 273.15 + 23 "Ambient Temperature = stack starting temperature";
  parameter ThermoSysPro.Units.SI.ThermalResistance Rth = 0.0668 "Equivalent thermal resistance between the control volume and the environment";
  parameter Boolean UseDynamicModeling = true "Choosing Dynamic or static modeling";
  parameter ThermoSysPro.Units.SI.Time Tau = 60 "Response time characteristic for a first order differential equation : T(t=Tau)=0.63(Top-Tstart)";

  // Variables of the model
  ThermoSysPro.Units.SI.Temperature T "stack Temperature";
  ThermoSysPro.Units.SI.Temperature Tstart = Tamb "stack starting Temperature";

  ThermoSysPro.Units.SI.Power Pelec "Stack electrical power consumption";
  ThermoSysPro.Units.SI.Voltage Vstack "Stack voltage";
  ThermoSysPro.Units.SI.HeatFlowRate Qelectrolysis "Electrolysis heat loss";
  ThermoSysPro.Units.SI.HeatFlowRate Qloss "Heat loss to ambient";

  Real ne "Energy efficiency (LHV)";
  Real nf "Faraday Efficiency";

  ThermoSysPro.Units.SI.MassFlowRate Q_H2O_in "Input H2O mass flow rate";
  ThermoSysPro.Units.SI.MassFlowRate Q_H2_out "Output H2 mass flow rate";
  ThermoSysPro.Units.SI.MassFlowRate Q_O2_out "Output O2 mass flow rate";

  ThermoSysPro.Units.SI.Density rho_ideal_H2_out "Output H2 density (ideal gas assumption)";
  ThermoSysPro.Units.SI.Density rho_ideal_Norm_H2_out "Output H2 density in standard conditions (p=1.01325 bar, T = 273.15K) (ideal gas assumption)";

  ThermoSysPro.Units.SI.VolumeFlowRate Qvol_H2_out "Output H2 volume flow rate";
  ThermoSysPro.Units.SI.VolumeFlowRate QvolNormal_H2_out "Output H2 normal volume flow rate in standard conditions (p=1.01325 bar, T = 273.15K)";

  ThermoSysPro.Units.SI.Voltage Urev "Reversible potential"; // [V]
  ThermoSysPro.Units.SI.Voltage Urev0_T "Standard reversible potential"; // [V]
  ThermoSysPro.Units.SI.Voltage Ures "Resistive overpotential"; // [V]
  ThermoSysPro.Units.SI.Voltage Uact "Activation overpotential"; // [V]
  ThermoSysPro.Units.SI.Voltage Ucell "Cell voltage"; // [V]

  ThermoSysPro.Units.SI.CurrentDensity i0_an "Exchange current density at anode";
  ThermoSysPro.Units.SI.CurrentDensity J "Cell current density";

  Real R_mem "Membrane resistance (Ω⋅m2)";
  ThermoSysPro.Units.SI.Conductivity sigma_mem "Membrane conductivity"; // [S/m]

  ThermoSysPro.Units.SI.Pressure pvH2O "Saturation water vapor pressure";

  Modelica.Blocks.Interfaces.RealOutput H2_VolFlowRateProd "Output H2 normal volume flow rate in standard conditions (p=1.01325 bar, T = 273.15K) in Nm3/h unit" annotation (Placement(transformation(
           extent={{-6,76},{14,96}}), iconTransformation(
         extent={{-10,-10},{10,10}},
         rotation=90,
         origin={0,78})));

protected
  parameter ThermoSysPro.Units.SI.Temperature T0 = 273.15 + 20 "Reference temperature"; // [K]
  constant Real R=Modelica.Constants.R "Universal gas constant";
  constant Real F=Modelica.Constants.F "Faraday constant";
  constant Real LHV = 120000000 "H2 Lower Heating Value [J/kg]";
  constant ThermoSysPro.Units.SI.MolarMass MO2=31.999e-3 "O2 Molar mass";
  constant ThermoSysPro.Units.SI.MolarMass MH2=2.01588e-3 "H2 Molar mass";
  constant ThermoSysPro.Units.SI.MolarMass MH2O=18.01528e-3 "H2O Molar mass";
  constant ThermoSysPro.Units.SI.Voltage Vtn=1.48 "Thermoneutral voltage of water electrolysis";

  // Empirical constants to compute the ohmic overvoltage (Ures)
  ThermoSysPro.Units.SI.Conductivity sigma_mem_std=10.31 "standard membrane conductivity"; // [S.m-1]
  ThermoSysPro.Units.SI.MolarEnergy Epro = 10536 "the activation energy required for the proton transport in membrane"; // [J/mol]

  // Empirical constants to compute the activation overvoltage (Uact)
  constant Real alpha_an = 0.7353 "Anode charge transfer coefficient"; // (Espinosa-Lopez et al., 2018)
  constant ThermoSysPro.Units.SI.CurrentDensity i0_an_std = 1.08*10^(-4) "Anode reference exchange current density"; // [A.m^-2]
  constant ThermoSysPro.Units.SI.Energy Eexc = 52994 "the activation energy required for the electron transport in the anode electrode"; // [J/mol] (Espinosa-Lopez et al., 2018)

initial equation
  if UseDynamicModeling then
    T = Tstart;
  else
    T = Top;
  end if;

equation

  Urev0_T = 1.229 - 8.5*10^(-4)*(T-298);

  // Saturation water vapor pressure approximation (valid between 25-250°C)
  pvH2O = Modelica.Math.exp(37.04-(6276/T)-3.416*Modelica.Math.log(T))*100000;

  // Nernst equation development after making some assumptions
  Urev = Urev0_T + ((R*T)/(2*F))*Modelica.Math.log((p - pvH2O)^(3/2)/pvH2O);

  // Ohmic overvoltage
  sigma_mem = sigma_mem_std*Modelica.Math.exp((-Epro/R)*(1/T-1/T0));
  R_mem = delta_mem/sigma_mem;
  Ures = R_mem *(Anode.i/A_cell);

  // Activation overvoltage
  i0_an = i0_an_std*Modelica.Math.exp((-Eexc/R)*(1/T-1/T0));
  Uact = R*T*Modelica.Math.asinh((Anode.i/ A_cell)/(2*i0_an))/(2*F*alpha_an);

  // Cell voltage and current density
  Ucell = Urev + Ures + Uact;
  J = Anode.i/A_cell;

  // Electrical equations
  Anode.i + Cathode.i=0;
  Pelec = Ncell*Anode.i*(Anode.v - Cathode.v);
  Vstack = Ncell*Ucell;
  Pelec/Anode.i = Vstack;

  // Faraday efficiency
  nf = 1;

  // Mass balance equations
  Q_H2O_in = nf*Ncell*MH2O*Anode.i/2/F;
  Q_H2_out = nf*Ncell*MH2*Anode.i/2/F;
  Q_O2_out = nf*Ncell*MO2*Anode.i/4/F;

  rho_ideal_H2_out = (MH2*p)/(R*T);
  rho_ideal_Norm_H2_out = (MH2*(1.01325*10^5))/(R*273.15);

  Qvol_H2_out = Q_H2_out/rho_ideal_H2_out;//Ideal gas assumption
  QvolNormal_H2_out = Q_H2_out/rho_ideal_Norm_H2_out;

  // Energy efficiency
  ne = (Q_H2_out*LHV)/(Pelec);

  // Thermal model
  Qelectrolysis = (Ucell-Vtn)*Anode.i*Ncell;
  Qloss = (T-Tamb)/Rth;
  if UseDynamicModeling then
    Tau*der(T) + T = Top;
  else
    T = Top;
  end if;

  H2_VolFlowRateProd = QvolNormal_H2_out * 3600;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,60}}), graphics={
        Rectangle(
          extent={{-102,68},{100,-100}},
          lineColor={28,108,200},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-106,68},{-90,-64}},
          lineColor={135,135,135},
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{88,68},{104,-66}},
          lineColor={135,135,135},
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{116,30},{132,10}},
          lineColor={28,108,200},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="-"),
        Text(
          extent={{-134,28},{-118,8}},
          lineColor={28,108,200},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="+"),
        Line(
          points={{0,68},{0,-100},{0,-100}},
          color={28,108,200},
          thickness=0.5,
          pattern=LinePattern.Dash)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,60}})),
    experiment(Interval=1));
end stack;
