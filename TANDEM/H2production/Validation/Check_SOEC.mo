﻿within TANDEM.H2production.Validation;
model Check_SOEC
  import Modelica.Units.SI;

replaceable package Medium_MoistH2 =
SOEC_library.Media.Predefined.Mixture.Diphasic.MoistGas.MoistHydrogen.PerfectGasMoistH2;

replaceable package Medium_Air =
SOEC_library.Media.Predefined.PureSubstance.Gas.Air.IdealGasAir;

constant SI.Temperature T_ref=273.15;
constant SI.Pressure p_ref=101325;
constant SI.MolarVolume Vm_ref=Modelica.Constants.R*T_ref/p_ref;//m3/mol

parameter SI.MassFlowRate m_flow_start=0 "Simulation start value mass flow at fluid interfaces" annotation (Dialog(group="Initial values"));
parameter SI.Pressure p_start_an=101325 "Cathode Simulation start value pressure " annotation (Dialog(group="Initial values"));
parameter SI.Pressure p_start_cat=101325 "Cathode Simulation start value pressure " annotation (Dialog(group="Initial values"));

parameter SI.Temperature T_start=750+273.15 "initla temperature" annotation (Dialog(group="Initial values"));

parameter SI.MassFlowRate m_flow_small=1e-8 "small flow regularization" annotation (Dialog(group="Simulations"));
parameter SI.Pressure dp_small=10 "small flow regularization" annotation (Dialog(group="Simulations"));
parameter SI.Pressure p_start = 101325;

parameter Real SteamConversion = 0.7;

SI.MassFlowRate m_flow_fuel;
SI.MolarMass MM_fuel;
SI.MassFraction[Medium_MoistH2.nX] X_Fuel;
SI.MoleFraction[Medium_MoistH2.nX] x_Fuel_in;

parameter SI.MolarConcentration x_H2_in = 0.1;
parameter SI.MolarConcentration x_H2O_in = 0.9;

constant Integer Pos_H2= 2;
constant Integer Pos_H2O=1;
  SOEC_library.Electrolysers.SOEC.Electrolyzer SOEC(
    redeclare model Electrolyzer_Voltage =
        SOEC_library.Electrolysers.SOEC.BaseClasses.Physics.Voltage.V_cell_1,
    redeclare model Electrolyzer_Molar_Flow =
        SOEC_library.Electrolysers.SOEC.BaseClasses.Physics.Mass_Flow.Mass_Flow_1,
    redeclare model Electrolyzer_Temperature =
        SOEC_library.Electrolysers.SOEC.BaseClasses.Physics.Temperature.T_op_1,
    redeclare model Electrolyzer_Converter =
        SOEC_library.Electrolysers.SOEC.BaseClasses.Physics.Converter.Converter_1,
    redeclare package Medium_MoistH2 =
        SOEC_library.Media.Predefined.Mixture.Diphasic.MoistGas.MoistHydrogen.PerfectGasMoistH2,
    T_op_start=1025.15,
    X_start_cat={0.9,0.1})
    annotation (Placement(transformation(extent={{-20,-4},{26,40}})));

  Modelica.Electrical.Analog.Sources.SignalCurrent signalCurrent
    annotation (Placement(transformation(extent={{10,-24},{-10,-44}})));
  Modelica.Blocks.Sources.Ramp     ramp(
    height=1.1*100*100*SOEC.SOEC_area,
    duration=500,
    offset=0)
    annotation (Placement(transformation(extent={{-28,-74},{-8,-54}})));
  Modelica.Fluid.Sources.MassFlowSource_T boundary(
    redeclare package Medium =
        SOEC_library.Media.Predefined.PureSubstance.Gas.Air.IdealGasAir,
    use_m_flow_in=true,
    T=T_start,
    nPorts=1)
    annotation (Placement(transformation(extent={{-70,-28},{-50,-8}})));

  Modelica.Fluid.Sources.MassFlowSource_T boundary1(
    redeclare package Medium =
        SOEC_library.Media.Predefined.Mixture.Diphasic.MoistGas.MoistHydrogen.PerfectGasMoistH2,
    use_m_flow_in=true,
    use_X_in=false,
    T=T_start,
    X={0.9,0.1},
    nPorts=1)
    annotation (Placement(transformation(extent={{86,-26},{66,-6}})));

  Modelica.Fluid.Sources.Boundary_pT boundary2(
    redeclare package Medium =
        SOEC_library.Media.Predefined.PureSubstance.Gas.Air.IdealGasAir,
    p=p_start,
    nPorts=1)
    annotation (Placement(transformation(extent={{-72,44},{-52,64}})));

  Modelica.Fluid.Sources.Boundary_pT boundary3(
    redeclare package Medium =
        SOEC_library.Media.Predefined.Mixture.Diphasic.MoistGas.MoistHydrogen.PerfectGasMoistH2,
    p=p_start,
    X={0.9,0.1},
    nPorts=1)
    annotation (Placement(transformation(extent={{72,42},{52,62}})));

  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature(T=1025.15)
    annotation (Placement(transformation(extent={{-68,8},{-48,28}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{34,-64},{54,-44}})));
  Modelica.Blocks.Sources.RealExpression massFlow(y=m_flow_fuel)
    annotation (Placement(transformation(extent={{-122,-90},{-102,-70}})));
equation

x_Fuel_in = {0.9,0.1};

X_Fuel=Medium_MoistH2.moleToMassFractions(x_Fuel_in,Medium_MoistH2.MMX);//9

MM_fuel=Medium_MoistH2.molarMass(Medium_MoistH2.setState_pTX(p_start_cat,T_start,X_Fuel));

m_flow_fuel= MM_fuel*ramp.y/(2*96500*0.9*SteamConversion); // (12/1000/1000/60*SOEC.n_cells*SOEC.SOEC_area*1e4)/Vm_ref*MM_fuel;//
  connect(signalCurrent.p, SOEC.n) annotation (Line(points={{10,-34},{16,
          -34},{16,-26},{5.09091,-26},{5.09091,-4}},
                                            color={0,0,255}));
  connect(signalCurrent.n, SOEC.p) annotation (Line(points={{-10,-34},{-14,
          -34},{-14,-26},{-3.27273,-26},{-3.27273,-4}},
                                                   color={0,0,255}));
  connect(ramp.y, signalCurrent.i)
    annotation (Line(points={{-7,-64},{0,-64},{0,-46}}, color={0,0,127}));
  connect(SOEC.Out_an, boundary2.ports[1]) annotation (Line(points={{
          -11.6364,40},{-11.6364,54},{-52,54}},
                                   color={0,127,255}));
  connect(SOEC.Out_cat, boundary3.ports[1]) annotation (Line(points={{13.4545,
          40},{13.4545,52},{52,52}},
                                 color={0,127,255}));
  connect(boundary1.ports[1], SOEC.In_cat) annotation (Line(points={{66,-16},
          {32,-16},{32,-4},{13.4545,-4}},
                                      color={0,127,255}));
  connect(fixedTemperature.port, SOEC.Ambiant)
    annotation (Line(points={{-48,18},{-19.5818,18}}, color={191,0,0}));
  connect(signalCurrent.p, ground.p)
    annotation (Line(points={{10,-34},{44,-34},{44,-44}}, color={0,0,255}));
  connect(massFlow.y, boundary.m_flow_in) annotation (Line(points={{-101,
          -80},{-78,-80},{-78,-10},{-70,-10}},            color={0,0,127}));
  connect(massFlow.y, boundary1.m_flow_in) annotation (Line(points={{-101,
          -80},{94,-80},{94,-8},{86,-8}},                  color={0,0,127}));
  connect(boundary.ports[1], SOEC.In_an) annotation (Line(points={{-50,-18},
          {-11.6364,-18},{-11.6364,-4}}, color={0,127,255}));
  annotation (experiment(StopTime=1000, __Dymola_Algorithm="Dassl"),
      __Dymola_experimentFlags(
      Advanced(
        EvaluateAlsoTop=false,
        GenerateVariableDependencies=true,
        OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false),
    __Dymola_Commands(
      executeCall(ensureSimulated=true) = {createPlot(
            id=1,
            position={35,35,584,361},
            y={"SOEC.i_dens_a","SOEC.i_dens_a_max","SOEC.i_dens_a_lim"},
            range={0.0,1000.0,-5000.0,50000.0},
            grid=true,
            colors={{28,108,200},{238,46,47},{0,140,72}},
            timeUnit="s",
            displayUnits={"A/m2","A/m2","A/m2"})}
        "current density comparison",
      executeCall(ensureSimulated=true) = {createPlot(
            id=1,
            position={0,0,1289,640},
            y={"boundary2.ports[1].m_flow"},
            range={0.0,1000.0,-0.0019000000000000002,9.999999999999983E-05},
            grid=true,
            colors={{28,108,200}},
            timeUnit="s",
            displayUnits={"kg/s"})} "débit total en sortie de cathode",
      executeCall(ensureSimulated=true) = {createPlot(
            id=1,
            position={0,0,1289,640},
            y={"boundary2.ports[1].Xi_outflow[1]",
          "boundary2.ports[1].Xi_outflow[2]"},
            range={0.0,1000.0,0.2,0.8},
            grid=true,
            colors={{28,108,200},{238,46,47}},
            timeUnit="s",
            displayUnits={"kg/kg","kg/kg"})}
        "fraction massique H2/H2O en sortie de cathode"));
end Check_SOEC;
