within TANDEM.Desalination;
model ReverseOsmosis

  parameter Modelica.Units.SI.EnergyDensity SEC = 2.5*3.6e6 "Specific energy consumption";



  Modelica.Blocks.Interfaces.RealInput powerSignal annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,110})));
  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));

  Modelica.Blocks.Continuous.Integrator integrator(k=1)
    annotation (Placement(transformation(extent={{-32,-10},{-12,10}})));
  Modelica.Blocks.Math.Gain gain(k=1/SEC)
    annotation (Placement(transformation(extent={{10,-8},{26,8}})));
  Modelica.Blocks.Interfaces.RealOutput WaterOutput(unit="m3")
    annotation (Placement(transformation(extent={{96,30},{116,50}})));
  Modelica.Blocks.Continuous.Der der1
    annotation (Placement(transformation(extent={{60,-50},{80,-30}})));
  Modelica.Blocks.Interfaces.RealOutput WaterFlow(unit="m3/s")
    annotation (Placement(transformation(extent={{96,-50},{116,-30}})));
equation
  powerConnection.P = -powerSignal;
  connect(powerSignal, integrator.u)
    annotation (Line(points={{0,110},{0,60},{-44,60},{-44,0},{-34,0}},
                                                    color={0,0,127}));
  connect(integrator.y, gain.u)
    annotation (Line(points={{-11,0},{8.4,0}}, color={0,0,127}));
  connect(gain.y, WaterOutput)
    annotation (Line(points={{26.8,0},{40,0},{40,40},{106,40}},
                                                color={0,0,127}));
  connect(der1.u, gain.y) annotation (Line(points={{58,-40},{40,-40},{40,0},{26.8,
          0}}, color={0,0,127}));
  connect(der1.y, WaterFlow)
    annotation (Line(points={{81,-40},{106,-40}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={85,170,255},
          fillColor={85,170,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={85,170,255},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-70,74},{80,-68}},
          textColor={85,170,255},
          textString="RO plant")}),                              Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>The <i>ReverseOsmosis</i> model is a black box model to convert a given input power into fresh water production. The main governing parameter is the Specific Energy Consumption (SEC) of the considered desalination technology. </p>
</html>"));
end ReverseOsmosis;
