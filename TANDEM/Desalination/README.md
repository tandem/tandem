# Desalination

Desalination is seen as a potential end-user segment to be coupled to a nuclear hybrid energy system. Within the scope of the TANDEM project, **reverse osmosis** stands out as the reference technology for this process. As a result, this industrial process solely relies on electrical power and is not thermally coupled to the integrated energy system. 

The package contains the `ReverseOsmosis` model as well as a test case with a ramp in input power to showcase a possible utilisation of the model.

## Component description
Having only an electrical interconnection, a simplified version of the model has been proposed within the framework of the TANDEM project. In particular, the desalination plant is treated as a black box, converting a given input power signal to the electrical power exchanged by ThermoPower's  [PowerConnection](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.PowerConnection.html) connector, facilitating its potential link to the electrical grid model.

The power signal is integrated to compute the electrical energy delivered to the desalination plant, which is then converted into the cumulative amount of produced fresh water through a proportionality constant given by the **specific energy consumption (SEC)** of the considered reverse osmosis desalination plant. The model also provides the volumetric water flow produced in each instant by computing the derivative of the cumulative water output.

## Required parameters
| Name | Nominal value | Description |
|-- | -- | -- |
| SEC | 2.5 kWh/m3 | Specific energy consumption of the desalination plant |