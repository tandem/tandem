within TANDEM.Desalination.Test;
model Test_RO
  ReverseOsmosis RO
    annotation (Placement(transformation(extent={{-16,-16},{16,16}})));
  ThermoPower.Electrical.Grid grid(Pgrid=100) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-52,0})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=-2.5e3,
    duration=100,
    offset=2.5e3,
    startTime=3600)
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{60,60},{80,80}})));
equation
  connect(grid.port, RO.powerConnection) annotation (Line(
      points={{-43.4,0},{-16,0}},
      color={0,0,255},
      thickness=0.5));
  connect(ramp.y, RO.powerSignal)
    annotation (Line(points={{-39,60},{0,60},{0,17.6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Test_RO;
