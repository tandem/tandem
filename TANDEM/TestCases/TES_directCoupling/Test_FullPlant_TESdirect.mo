within TANDEM.TestCases.TES_directCoupling;
model Test_FullPlant_TESdirect
  "Test case of the NSSS coupled to the BOP and a simplified TES model connected directly to the BOP"

  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{-170,-2},{-150,18}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex2
                       NSSSctrl
    annotation (Placement(transformation(extent={{-248,8},{-210,40}})));
  TANDEM.SMR.NSSS.NSSS_ThermoPower.NSSSsimplified_fluid nsss(
    core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom(displayUnit="Pa") = 22280,
    Cfnom=0.0037,
    Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp1=200000,
    Cfnom1=0.004,
    htc2=14500,
    Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp2=40000,
    Cfnom2=0.0086448975,
    rho_sg(displayUnit="kg/m3"),
    eta=0.9,
    q_nom={0,0.85035195,1.51494276},
    head_nom={52.9277496,36.251883,0},
    hstart_pump=1.33804e6,
    dp0=257900,
    SG(Secondary(noInitialPressure=false, h(start={691295.13120434,
              1065733.8385727042,1341574.263984386,1584718.4519740841,
              1768563.5328741434,1960744.8979423766,2194096.372327492,
              2417901.2813089825,2618672.8710228545,2818443.265851936,
              2943649.7424823088})), Primary(p(start=14766833.046578314,
            displayUnit="bar"), wall(T(start={589.5984469973522,
                587.7017498216869,585.4084276982339,583.16821641854,
                583.7302684392605,580.0427052798126,577.7590297482865,
                575.7327125774938,573.7717613972867,565.6184754008839},
              displayUnit="degC")))),
    LowerPlenum(p(start=15058335.330254601, displayUnit="bar")),
    core(fuel(
        Tc(start={599.044948813225,601.5987749386036,604.1149622184299,
              606.5915411553357,609.0264543324972,611.4175538943198,
              613.7625941450433,616.0592155379976,618.3049153676808,
              620.4970001011553}, displayUnit="degC"),
        Tci(start={606.7427428721659,609.2865814461284,611.792948744553,
              614.2598819683861,616.6853306985386,619.067154385736,
              621.4031149773621,623.690860968195,625.9278982048954,
              628.1115423996662}, displayUnit="degC"),
        Tco(start={591.8578932730682,594.4210442889529,596.946400007321,
              599.4319846769142,601.8757343474647,604.2754943418222,
              606.6290118281951,608.933919752005,611.1877074230115,
              613.3876726765399}, displayUnit="degC"),
        Tvol(start=[1106.2158309259346,959.6770137270776,901.081523808358,
              846.5119581147102,794.9628315101688; 1109.1476835301366,
              962.2713736354813,903.543705010586,848.8529595427409,
              797.1911395711874; 1112.0394178484976,964.8300310918536,
              905.9719158132306,851.1615835777632,799.3885572969391;
              1114.8886134199338,967.3508538785122,908.3641365538011,
              853.4359134665903,801.55326336812; 1117.6927428434935,
              969.8316156512884,910.7182584702026,855.6739479547313,
              803.6833562262644; 1120.4491688019236,972.2699933096244,
              913.0320812082816,857.8735989202202,805.7768518249557;
              1123.1551354760352,974.6635594030728,915.3033056186252,
              860.0326845285076,807.8316771176848; 1125.8077500092586,
              977.009765737336,917.5295182029794,862.1489144495056,
              809.8456559913963; 1128.4039485435335,979.3059133381712,
              919.7081626181017,864.2198627731655,811.8164834944805;
              1130.9404408820722,981.5491035218076,921.8364932571012,
              866.2429238922465,813.741683858821], displayUnit="degC")),
        neutronicKinetics(D(start={2.6297433004241203E+17,6.425892720202542E+17,
              1.7257554821707536E+17,1.6728770166583648E+17,13303534357977988.0,
              1894272960287383.5}))),
    flangeA(h_outflow(start=1065733.8385727042)),
    flangeB(h_outflow(start=2943649.7827971983)),
    pressurizer(pressurizer(h(start=1862126.0574145121))),
    pump(h(start=1337760.7266616537), q_single(start=0.8497969601150965)))
    annotation (Placement(transformation(extent={{-258,-58},{-200,-22}})));
  ThermoPower.Water.PressDropLin pressDropLin(R=3.6e5/239.68) annotation (
      Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=180,
        origin={-179,-51})));
  Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
    use_m_flow_in=true,
    use_h_in=false,
    m_flow=100,
    h=135000,
    nPorts=1) annotation (Placement(transformation(
        extent={{-4.5,-4.5},{4.5,4.5}},
        rotation=270,
        origin={-45.5,21.5})));
  Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
    use_m_flow_in=true,
    use_h_in=false,
    m_flow=100,
    h=135000,
    nPorts=1,
    ports(p(start={7056.841690486489})))
              annotation (Placement(transformation(
        extent={{-4.5,-4},{4.5,4}},
        rotation=180,
        origin={79.5,-56})));
  Modelica.Blocks.Sources.Constant
                               const1(k=0)
                   annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={98,-60})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro1(
      steam_outlet(h(start=808791.3714520645)))                                    annotation (
      Placement(transformation(
        extent={{7,-7},{-7,7}},
        rotation=90,
        origin={-263,47})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro2
                                                         annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=90,
        origin={-51,-3})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro3
                                                         annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={15,7})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro6 annotation (
      Placement(transformation(
        extent={{7,-7},{-7,7}},
        rotation=0,
        origin={55,-57})));
  TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
    adaptorRealModelicaTSP3
                           annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=180,
        origin={74,28})));
  Modelica.Blocks.Sources.Ramp Set_P_TapSteam_IP(
    height=0,
    duration=900,
    offset=7.56e5,
    startTime=100) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={102,26})));
  TANDEM.SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
    adaptorRealModelicaTSP4
                           annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=180,
        origin={78,-10})));
  Modelica.Blocks.Sources.Ramp Set_P_TapSteam_LP(
    height=0,
    duration=900,
    offset=0.815e5,
    startTime=100) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={108,-8})));
  TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_IP annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={46,26})));
  TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Tap_LP annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={50,-12})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_MainDrum_P(
    Starttime=100,
    Duration=900,
    Initialvalue=7.147E5,
    Finalvalue=7.147E5) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=90,
        origin={-30,-106})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_P_In_TurbHP(k=44.5E5)
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=270,
        origin={-74,8})));
  TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI CTR_P_Turb_HP
    annotation (Placement(transformation(extent={{-80,-12},{-66,0}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Set_Ouv_Vv_SG_In(
    Starttime=100,
    Duration=900,
    Initialvalue=1,
    Finalvalue=1) annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=90,
        origin={-87,-103})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro
                                    fluid2TSPro7(steam_outlet(h(start=2944000.0)),
      port_a(h_outflow(start=2943649.7827971987)))
    annotation (Placement(transformation(extent={{-148,-36},{-132,-20}})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid
                                    fluid2TSPro8 annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=180,
        origin={-144,-54})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro
                                    fluid2TSPro4(steam_outlet(h(start=2944000.0)),
      port_a(h_outflow(start=2962802.891927479)))
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=0,
        origin={-124,-118})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid
                                    fluid2TSPro5(steam_inlet(h(start=
            2943649.74248231)))                  annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=90,
        origin={-100,22})));
  Modelica.Fluid.Sources.MassFlowSource_h Heat_IP_Network_Out1(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
    use_m_flow_in=true,
    use_h_in=false,
    m_flow=100,
    h=135000,
    nPorts=1) annotation (Placement(transformation(
        extent={{-4.5,-4.5},{4.5,4.5}},
        rotation=0,
        origin={-153.5,-118.5})));
  Modelica.Blocks.Sources.Constant
                               HeatNwk_IP_In1(k=0)
                   annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={-174,-114})));
  Modelica.Blocks.Sources.Constant
                               HeatNwk_IP_In2(k=0)
                   annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=270,
        origin={-40,38})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.TSPro2Fluid          fluid2TSPro9(
      steam_inlet(h(start=486684.2674966917)))           annotation (
      Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={31,-119})));
  TANDEM.SMR.BOP.BOP_TSPro.AdaptatorForMSL.Fluid.Fluid2TSPro          fluid2TSPro10(
      steam_outlet(h(start=2964247.556)))                                          annotation (
      Placement(transformation(
        extent={{7,-7},{-7,7}},
        rotation=90,
        origin={165,-3})));
  Modelica.Fluid.Sources.MassFlowSource_h HeatNwk_LP_In1(
    redeclare package Medium = Modelica.Media.Water.WaterIF97_ph,
    use_m_flow_in=true,
    use_h_in=false,
    m_flow=100,
    h=135000,
    nPorts=1) annotation (Placement(transformation(
        extent={{-4.5,-4},{4.5,4}},
        rotation=180,
        origin={33.5,6})));
  Modelica.Blocks.Sources.Constant
                               const(k=0)
                   annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={54,6})));
  Components.BOP_TSPro_TESdirect StaticBOP2_PlugSGTSP_PlugCogSML(
    Pp_LP(
      C2(h(start=164184.75305878202), h_vol(start=164191.49349927544)),
      Qv(start=0.1842701620036864),
      h(start=163778.5043512623),
      Pm(start=363782.6099259994, displayUnit="bar"),
      R(start=1.1088978261489797)),
    Sing7(Pm(start=81483.99252825856, displayUnit="bar"), C2(h_vol(start=
              317491.6243445445))),
    P11(C1(h_vol(start=371544.21154913754))),
    Sing14(Pm(start=694722.1099619151, displayUnit="bar")),
    Vol14(h(start=489326.49295901513)),
    Sing4b(Pm(start=755865.5982030496, displayUnit="bar"), C2(h_vol(start=
              700826.7650865436))),
    Pp4(C2(h_vol(start=2639587.8110064324), P(start=755865.5982739213,
            displayUnit="bar"))),
    Vv12(Ouv(signal(start=0.7, fixed=false))),
    Vol10(h(start=164191.49349927544)),
    Vv10(Pm(start=717639.5996307728, displayUnit="bar")),
    Vol9(h(start=163365.51520324874)),
    Vol8(h(start=2164372.4464344834), Cs(h(start=2164372.446434485))),
    Vv7(Pm(start=80938.2970467849, displayUnit="bar"), C1(h_vol(start=
              2643272.624379609))),
    Pip7(C2(P(start=81483.9927983659, displayUnit="bar"), h_vol(start=
              2643272.624379609)), Q(start=16.308538020753613)),
    Vol7(
      Ce(h(start=2619840.1257646517)),
      Cs1(Q(start=-2.842170943040401E-14), h(start=2619840.125764651)),
      Cs2(h(start=2620466.1773813386)),
      h(start=2643272.624379609)),
    Vol6(h(start=2975760.573362564)),
    Vol5(h(start=2767838.2375168446)),
    Vv4(Pm(start=740860.47826703, displayUnit="bar"), C1(h_vol(start=
              2639595.2860046183))),
    P4(C1(h_vol(start=2639595.2860046183))),
    Vol4(Cs2(Q(start=24.39923933957964)), h(start=2639587.8110064324)),
    Sing4(C2(h_vol(start=2492812.8893823405), P(start=741168.7528933026,
            displayUnit="bar"))),
    P2b(C1(h_vol(start=2751055.4478353322))),
    Pip2(C1(P(start=4468614.331148661, displayUnit="bar"), h_vol(start=
              2943649.782797198))),
    Sing2(C1(h_vol(start=2943649.7827971987)), Pm(start=4484429.777640034,
          displayUnit="bar")),
    Bach(
      h(start=487169.0303074137),
      Ce4(h(start=370518.6159570959)),
      Cs1(Q(start=244.2983525092077)),
      Ce3(Q(start=21.204195564406678))),
    Dry(Cev(h(start=2639676.0950998147)), Csv(h(start=2765971.6797407805))),
    SupH(
      DPfc(start=38393.65182948505, displayUnit="bar"),
      Ec(h(start=2943649.7827978437)),
      Ef(h(start=2767838.2375168446)),
      Sf(h(start=2975760.573362564), h_vol(start=2975760.573362564)),
      DPf(
        start=40000,
        displayUnit="bar",
        fixed=true),
      Sc(h_vol(start=1118785.4784898118))),
    Vol2(
      Cs1(h(start=2854461.5968234967)),
      h(start=2943649.74248231),
      Ce(h(start=2943649.78279752))),
    Vol1(h(start=691295.1312043399)),
    ReH_BP(
      HDesF(start=371544.21154913754),
      HeiF(start=170988.0258267126),
      promeF(d(start=981.4362402154908, displayUnit="g/cm3")),
      Ee(h_vol(start=164205.69145221374), h(start=164191.49349927544)),
      Hep(start=393725.22688442253)),
    ReH_HP(
      HDesF(start=691295.1312043415),
      HeiF(start=495535.49466649216),
      Se(
        h_vol(start=679621.7421491045),
        h(start=691295.1312043415),
        P(start=4972964.078124865, displayUnit="bar")),
      promeF(d(start=928.4157529118911, displayUnit="g/cm3")),
      Hep(start=710778.5352241131)),
    Pp_HP(
      C2(h(start=494525.4647646706), h_vol(start=494525.4647646706)),
      Pm(start=2830296.2925047562, displayUnit="bar"),
      Qv(start=0.25386095853998975),
      h(start=489785.7885485311),
      R(start=1.0286492116442472)),
    Vv1(Ouv(signal(start=0.7, fixed=false)), Pm(start=4943134.841542619,
          displayUnit="bar")),
    Vv2(
      Q(start=217.5901213379239),
      C1(h_vol(start=2943649.7827978437), P(start=4451800.957266087,
            displayUnit="bar")),
      C2(h_vol(start=2943649.7827978437), P(start=4400001.189086886,
            displayUnit="bar"))),
    Q1(C1(h_vol(start=691295.13120434))),
    T1(C1(h_vol(start=691295.1312043397))),
    P1(C1(h_vol(start=691295.1312043399))),
    T2(C1(h_vol(start=2943649.7827971973)), C2(h_vol(start=2943649.7827971987))),
    Turb_BP1(
      Cs(h(start=2622123.0702394107), h_vol(start=2619840.125764651)),
      xm(start=0.9950023521185347),
      Ps(start=81048.134197905, displayUnit="bar"),
      Pe(
        start=560260.05824026,
        displayUnit="bar",
        fixed=true),
      Ce(h_vol(start=2975760.573362564))),
    CsBP1a(start=17287.28454801896, fixed=false),
    CsBP2(start=646.0753467159474, fixed=false),
    CsHP(start=694031.4195235869, fixed=false),
    HeatSink(proe(d(start=990.7121800265442, displayUnit="g/cm3")), Cv(Q(start=
              182.47772238669975))),
    SPurgeBP(start=188.23374753410693),
    SPurgeHP(start=35.86826283480325),
    ScondesBP(start=1117.4768845481353),
    ScondesHP(start=1340.6503847191232),
    Turb_BP2(
      Ps(start=7056.5611290758, displayUnit="bar"),
      pros(d(start=0.05459371059084007, displayUnit="g/cm3")),
      xm(start=0.947948852019423),
      Ce(h_vol(start=2622123.070239542)),
      Pe(
        start=81036.189414669,
        displayUnit="bar",
        fixed=true)),
    SG_Secondary_In(h_vol(start=691076.9808245787)),
    SG_Secondary_Out(h_vol(start=2943659.8831241573), Q(start=
            239.72480633161524)),
    Turb_HP(
      Pe(fixed=true),
      Cs(h(start=2639595.286004529)),
      Ps(start=756001.14925809, displayUnit="bar"),
      Hrs(start=2639587.8110068804)),
    HeatNetwk_HP_Out(P(start=4451161.532520275, displayUnit="bar"), h(start=
            2943659.8353077616)),
    HeatNetwk_IP_Out(h_vol(start=2639595.2860046183)),
    HeatNetwk_LP_In(P(start=7055.9126210368, displayUnit="bar")),
    HeatNetwk_LP_Out(h(start=2622123.0648966096)),
    HeatNetwk_HP_In(P(start=4624073.085866618, displayUnit="bar")),
    kfric_Surch(start=7, fixed=false))
    annotation (Placement(transformation(extent={{-108,-88},{32,-18}})));

  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Constante Set_Pout_SG(k=300 +
        273) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={-218,-96})));
  TANDEM.SMR.BOP.BOP_TSPro.BOP_2Plug.CTRL_PI
                    CTR_PT_PpHP annotation (Placement(transformation(
        extent={{6,-5},{-6,5}},
        rotation=90,
        origin={-193,-96})));
  EnergyStorage.ThermalStorage.SensibleLoop_HX0D SL(
    cPump(
      rho0=892.248149,
      w0=521.853,
      h(start=372454.1175014998),
      outfl(p(start=579436.4312789366, displayUnit="bar")),
      redeclare function flowCharacteristic =
          ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,
              0.584873811895376,1.0419807316167864}, head_nom={32.5261607186619,
              22.278192273056096,0})),
    dPump(
      w0=521.853,
      h(start=525775.4825940173),
      outfl(p(start=568430.8392555639, displayUnit="bar")),
      redeclare function flowCharacteristic =
          ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom={0,
              0.6201374843981228,1.1048046545324521}, head_nom={
              34.48725361772262,23.62140658748125,0})),
    flangeB1(h_outflow(start=2974976.0604749024)),
    tank_simp(
      V=53579.8786519978,
      Rin=40.86052553649712/2,
      hl_in(start=528984.3108803069),
      wl_in(start=26.895704666106976),
      wl_out(start=-26.274376357238104)),
    tank_simp1(
      V=53579.8786519978,
      Rin=40.86052553649712/2,
      hl_in(start=372679.9333559406)),
    flangeB(h_outflow(start=808791.3714520645)),
    hX_0D(mh_nom=39.55, mc_nom=521.853),
    hX_0D1(
      mh_nom=521.853,
      mc_nom=32.301,
      pc_nom=754700),
    valveLin(Kv=0.001304631441617743),
    valveLin1(Kv=0.001304631441617743))
    annotation (Placement(transformation(extent={{4,76},{52,122}})));
  ThermoPower.Water.ValveLin valveLin(Kv=39.55/(45e5 - 7.147e5), outlet(
        h_outflow(start=826385.0551291436)))                     annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-30,68})));
  ThermoPower.Water.SensW sensW1
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=270,
        origin={142,-2})));
  EnergyStorage.ThermalStorage.Control.TEScontroller_Tin_ctrl TESctrl(
    Tcold_set=463.15,
    Kp_char=1e-3,
    Ki_char=19,
    PID(
      initType=Modelica.Blocks.Types.Init.InitialState,
      xi_start=-956.88434,
      y_start=-0.96,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.LowerLimit),
    PID1(
      initType=Modelica.Blocks.Types.Init.InitialState,
      xi_start=-956.88434,
      y_start=-0.97,
      homotopyType=Modelica.Blocks.Types.LimiterHomotopy.LowerLimit))
    annotation (Placement(transformation(extent={{16,134},{36,154}})));
  ThermoPower.Water.ThroughMassFlow throughMassFlow(use_in_w0=true) annotation (
     Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={82,54})));
  Components.PowerController powerController(PID1(
      k=-1*217,
      Ti=217/37.97,
      yMax=1.02*32.3,
      yMin=0.01*32.3,
      initType=Modelica.Blocks.Types.Init.InitialOutput,
      y_start=0.05*32.3), PID(
      k=4.193,
      Ti=4.193/0.7339,
      yMax=0.99,
      yMin=0.01))
    annotation (Placement(transformation(extent={{-140,86},{-120,106}})));
  Modelica.Blocks.Sources.TimeTable april14(table=[0,170e6; 10,170e6; 13,140e6;
        15,140e6; 20,190e6; 24,190e6], timeScale(displayUnit="h") = 3600)
    annotation (Placement(transformation(extent={{-190,138},{-170,158}})));
  Modelica.Blocks.Sources.TimeTable april12(table=[0,170e6; 5,170e6; 7,190e6; 8,
        190e6; 12,140e6; 17,140e6; 19,190e6; 21,190e6; 23,170e6; 24,170e6],
      timeScale(displayUnit="h") = 3600)
    annotation (Placement(transformation(extent={{-190,106},{-170,126}})));
  ThermoSysPro.InstrumentationAndControl.AdaptorForFMU.AdaptorTSPModelica
    adaptorTSPModelica
    annotation (Placement(transformation(extent={{-184,86},{-174,96}})));
equation
  connect(NSSSctrl.actuatorBus,nsss. actuatorBus) annotation (Line(
      points={{-240.4,8},{-240,8},{-240,-6},{-243.5,-6},{-243.5,-22.36}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
      points={{-217.6,8},{-218,8},{-218,-2},{-214.5,-2},{-214.5,-22.36}},
      color={255,219,88},
      thickness=0.5));
  connect(nsss.flangeA, pressDropLin.outlet) annotation (Line(points={{-200,
          -51.16},{-196,-51.16},{-196,-51},{-184,-51}},color={0,0,255}));
  connect(const1.y,HeatNwk_LP_In. m_flow_in) annotation (Line(points={{93.6,-60},
          {93.6,-59.2},{84,-59.2}},          color={0,0,127}));
  connect(HeatNwk_LP_In.ports[1],fluid2TSPro6. port_a) annotation (
      Line(points={{75,-56},{76,-57},{61.86,-57}},    color={0,127,
          255}));
  connect(adaptorRealModelicaTSP3.u,Set_P_TapSteam_IP. y)
    annotation (Line(points={{78.8,28},{94,28},{94,26},{97.6,26}},
        color={0,0,127}));
  connect(adaptorRealModelicaTSP4.u,Set_P_TapSteam_LP. y)
    annotation (Line(points={{82.8,-10},{100,-10},{100,-8},{103.6,-8}},
        color={0,0,127}));
  connect(CTR_P_Tap_IP.Set,adaptorRealModelicaTSP3. outputReal)
    annotation (Line(points={{55.6,27},{55.6,28},{69.6,28}},           color={0,0,255}));
  connect(CTR_P_Tap_LP.Set,adaptorRealModelicaTSP4. outputReal)
    annotation (Line(points={{59.6,-11},{68,-11},{68,-10},{73.6,-10}}, color={0,0,255}));
  connect(Set_P_In_TurbHP.y,CTR_P_Turb_HP. Set)
    annotation (Line(points={{-74,3.6},{-73.7,4},{-73.7,-0.24}},                     color={0,0,255}));
  connect(nsss.flangeB, fluid2TSPro7.port_a) annotation (Line(points={{-200,
          -29.2},{-154,-29.2},{-154,-28},{-147.84,-28}}, color={0,0,255}));
  connect(pressDropLin.inlet, fluid2TSPro8.port_b) annotation (Line(points={{-174,
          -51},{-158,-51},{-158,-54},{-152,-54}},      color={0,0,255}));
  connect(HeatNwk_IP_In1.y, Heat_IP_Network_Out1.m_flow_in) annotation (Line(
        points={{-169.6,-114},{-170,-114.9},{-158,-114.9}},
                                                     color={0,0,127}));
  connect(Heat_IP_Network_Out1.ports[1], fluid2TSPro4.port_a) annotation (Line(
        points={{-149,-118.5},{-148,-118},{-131.84,-118}},
                                                color={0,127,255}));
  connect(HeatNwk_IP_In2.y, Heat_IP_Network_Out.m_flow_in) annotation (Line(
        points={{-40,33.6},{-40,30},{-41.9,30},{-41.9,26}}, color={0,0,127}));
  connect(Heat_IP_Network_Out.ports[1], fluid2TSPro2.port_b) annotation (Line(
        points={{-45.5,17},{-45.5,14},{-51,14},{-51,4}}, color={0,127,255}));
  connect(const.y, HeatNwk_LP_In1.m_flow_in) annotation (Line(points={{49.6,6},
          {49.6,2.8},{38,2.8}},   color={0,0,127}));
  connect(fluid2TSPro3.port_b, HeatNwk_LP_In1.ports[1])
    annotation (Line(points={{22,7},{22,6},{29,6}},       color={0,127,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_SG_In_Opening,
    Set_Ouv_Vv_SG_In.y) annotation (Line(points={{-89.1111,-88},{-89.1111,-97.5},
          {-87,-97.5}},        color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.Set_P_PpBP_Out, Set_MainDrum_P.y)
    annotation (Line(points={{-26.2222,-88.5185},{-26.2222,-101.6},{-30,-101.6}},
        color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pp_LP_Out, fluid2TSPro9.steam_inlet)
    annotation (Line(points={{-40.2222,-88},{-42,-88},{-42,-120},{-10,-120},{
          -10,-119},{24,-119}},
        color={255,0,0}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_In, fluid2TSPro6.steam_outlet)
    annotation (Line(points={{32,-57.9259},{32,-57.0018},{48.035,-57.0018}},
                                        color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_LP_Out, fluid2TSPro3.steam_inlet)
    annotation (Line(points={{-2.66667,-18},{-2.66667,7},{8,7}},
                          color={255,0,0}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.Turb_IP_In, fluid2TSPro10.steam_outlet)
    annotation (Line(points={{-36,-17.7407},{165.002,-17.7407},{165.002,-9.965}},
        color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbLP_In, CTR_P_Tap_LP.Sensor)
    annotation (Line(points={{4.44444,-17.7407},{4.44444,-7.25},{40.4,-7.25}},
        color={0,0,255}));
  connect(CTR_P_Tap_LP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbLP_In_Opening)
    annotation (Line(points={{39.8,-18.25},{34,-18.25},{34,-12},{8.66667,-12},{
          8.66667,-18.2593}}, color={0,0,255}));
  connect(CTR_P_Tap_IP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbIP_In_Opening)
    annotation (Line(points={{35.8,19.75},{-15.5556,19.75},{-15.5556,-18.5185}},
                      color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.P_TurbIP_In, CTR_P_Tap_IP.Sensor)
    annotation (Line(points={{-46.5556,-17.6111},{-46.5556,12},{-18,12},{-18,
          30.75},{36.4,30.75}},
        color={0,0,255}));
  connect(fluid2TSPro1.steam_outlet, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_In)
    annotation (Line(points={{-262.998,40.035},{-262.998,-16},{-264,-16},{-264,
          -132},{-58,-132},{-58,-120},{-58.6667,-120},{-58.6667,-88}},
                               color={0,0,255}));
  connect(fluid2TSPro2.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_IP_Out)
    annotation (Line(points={{-51,-10},{-52,-10},{-52,-16},{-51.1111,-16},{
          -51.1111,-17.7407}}, color={0,0,255}));
  connect(CTR_P_Turb_HP.Sensor, StaticBOP2_PlugSGTSP_PlugCogSML.P_SG_Outb)
    annotation (Line(points={{-76.325,-11.76},{-74.4444,-11.76},{-74.4444,
          -17.7407}},
        color={0,0,255}));
  connect(CTR_P_Turb_HP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_Vv_TurbHP_In_Opening)
    annotation (Line(points={{-68.625,-12.12},{-69.7778,-12.12},{-69.7778,-18}},
        color={0,0,255}));
  connect(fluid2TSPro5.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_Out)
    annotation (Line(points={{-100,14},{-100,-18},{-97.5556,-18}}, color={0,0,
          255}));
  connect(fluid2TSPro4.steam_outlet, StaticBOP2_PlugSGTSP_PlugCogSML.HeatNetwk_HP_In)
    annotation (Line(points={{-116.04,-118.002},{-76,-118.002},{-76,-94},{-84,
          -94},{-84,-88}},color={0,0,255}));
  connect(fluid2TSPro8.steam_inlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_In)
    annotation (Line(points={{-136,-54},{-116,-54},{-116,-59.7407},{-108.222,
          -59.7407}}, color={0,0,255}));
  connect(fluid2TSPro7.steam_outlet, StaticBOP2_PlugSGTSP_PlugCogSML.SG_Secondary_Out)
    annotation (Line(points={{-132.04,-28.002},{-116,-28.002},{-116,-52.2222},{
          -108.222,-52.2222}}, color={0,0,255}));
  connect(CTR_PT_PpHP.Command, StaticBOP2_PlugSGTSP_PlugCogSML.Set_PpHP_RPM)
    annotation (Line(points={{-187.9,-99.75},{-186,-99.75},{-186,-124},{
          -64.8889,-124},{-64.8889,-87.7407}},                                      color={0,0,255}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.T_SG_Out,CTR_PT_PpHP. Sensor)
    annotation (Line(points={{-108.222,-29.6667},{-126,-29.6667},{-126,-93.15},
          {-188.2,-93.15}},                                                                   color={0,0,255}));
  connect(Set_Pout_SG.y,CTR_PT_PpHP. Set) annotation (Line(points={{-213.6,-96},
          {-214,-95.4},{-197.8,-95.4}},                                                                              color={0,0,255}));
  connect(SL.flangeB,valveLin. inlet) annotation (Line(points={{4,85.2},{-10,
          85.2},{-10,68},{-20,68}},                   color={0,0,255}));
  connect(valveLin.outlet, fluid2TSPro1.port_a)
    annotation (Line(points={{-40,68},{-262,68},{-262,58},{-263,58},{-263,53.86}},
                                                             color={0,0,255}));
  connect(SL.flangeA, fluid2TSPro5.port_b) annotation (Line(points={{4,112.8},{
          -100,112.8},{-100,30}},    color={0,0,255}));
  connect(sensW1.inlet, fluid2TSPro9.port_b)
    annotation (Line(points={{138,-8},{138,-119},{38,-119}}, color={0,0,255}));
  connect(SL.flangeB1, fluid2TSPro10.port_a) annotation (Line(points={{52,112.8},
          {165,112.8},{165,3.86}},            color={0,0,255}));
  connect(TESctrl.actuatorBus, SL.actuatorBus) annotation (Line(
      points={{20,134},{20,122},{13.6,122}},
      color={80,200,120},
      thickness=0.5));
  connect(TESctrl.sensorBus, SL.sensorBus) annotation (Line(
      points={{32,134},{32,128},{42.4,128},{42.4,122}},
      color={255,219,88},
      thickness=0.5));
  connect(SL.flangeA1, throughMassFlow.outlet) annotation (Line(points={{52,85.2},
          {62,85.2},{62,54},{72,54}},             color={0,0,255}));
  connect(throughMassFlow.inlet, sensW1.outlet)
    annotation (Line(points={{92,54},{138,54},{138,4}}, color={0,0,255}));
  connect(powerController.ChargingValve, valveLin.cmd) annotation (Line(points={{-119.2,
          101},{-98,101},{-98,58},{-46,58},{-46,52},{-30,52},{-30,60}},
                                                                 color={0,0,127}));
  connect(powerController.DischargingPump, throughMassFlow.in_w0) annotation (
      Line(points={{-119.2,91},{2,91},{2,42},{86,42},{86,48}}, color={0,0,127}));
  connect(adaptorTSPModelica.y, powerController.PowerOutput)
    annotation (Line(points={{-173,91},{-140.8,91}}, color={0,0,127}));
  connect(StaticBOP2_PlugSGTSP_PlugCogSML.Pwe, adaptorTSPModelica.inputReal)
    annotation (Line(points={{32.4444,-33.5556},{148,-33.5556},{148,-140},{-268,
          -140},{-268,91},{-184.5,91}}, color={0,0,255}));
  connect(april12.y, powerController.PowerSetpoint) annotation (Line(points={{
          -169,116},{-158,116},{-158,101},{-140.8,101}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-280,
            -140},{180,160}})),
      Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-280,-140},{
            180,160}})),
    experiment(StopTime=86400, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentFlags(
      Advanced(
        EvaluateAlsoTop=false,
        GenerateAnalyticJacobian=false,
        GenerateVariableDependencies=false,
        OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=false,
      OutputFlatModelica=false));
end Test_FullPlant_TESdirect;
