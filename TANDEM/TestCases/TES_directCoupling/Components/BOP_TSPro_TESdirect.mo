﻿within TANDEM.TestCases.TES_directCoupling.Components;
model BOP_TSPro_TESdirect "BoP Static model, with Steam Generator temperature control by inlet water flow - 
   Fluid flow plugs for HP, IP and LP cogeneration"

  parameter Real Tmax_synop = 1000;

  Real Eff_Cycle_net;
  Real Eff_Cycle_brut;
  Real Pw_eGrid;
 // Real Pw_COG_IP;
//  Real Pw_COG_LP;
//  Real Pw_COG_HP;
//  Real T_COG_IP;
//  Real T_COG_HP;
//  Real T_COG_LP;
 // Real Heat_To_ePower_ratio;
  Real Tin_SG;
  Real Tout_SG;
  Real Pin_SG;
  Real Pout_SG;
  Real Nuclear_Pw_load;
  Real Pwth_SG;

  //Variables de calibration
  parameter Real CsHP( fixed=true) =688136;
                                                    // stodola
  parameter Real CsBP1a( fixed=true) =24244.3;
                                                     // stodola
 // parameter Real CsBP1b( fixed=false, start=1692.6621744084814);
                                                    // stodola
  parameter Real CsBP2( fixed=true) =658.99;
                                                  // stodola

  //parameter Real ScondesHP( fixed=false, start=1583.5760574510198);
                                                     //surface réchauffeur
  //parameter Real SPurgeHP( fixed=false, start=31.44935690972406);
                                                   //surface réchauffeur
  //parameter Real ScondesBP( fixed=false, start=1194.892006718808);
                                                  //surface réchauffeur
  //parameter Real SPurgeBP( fixed=false, start=182.49482878824173);
                                                    //surface réchauffeur
  //parameter Real SpHP_Q(fixed=true)= 24.51;
                                                    //débit réchauffeur
  //parameter Real SpHP_H(fixed=true) = 700.8e3;
                                                    //enthalpie réchauffeur
  //parameter Real SpBP_Q(fixed=true) = 16.41;
                                                    //débit réchauffeur
  //parameter Real SpBP_H(fixed=true) = 317.89e3;
                                                    //enthalpie réchauffeur

  parameter Real ScondesHP( fixed=true) = 1587.34;// =1583.5760574510198;
                                                     //surface réchauffeur
  parameter Real SPurgeHP( fixed=true) =32.5;//=31.4493569097240;
                                                   //surface réchauffeur
  parameter Real ScondesBP( fixed=true) = 1172.5;//=9.892006718808;
                                                  //surface réchauffeur
  parameter Real SPurgeBP( fixed=true) = 186.6; //=182.4948287882417;
                                                    //surface réchauffeur
  parameter Real SpHP_Q(fixed=false, start= 24.51);
                                                    //débit réchauffeur
  parameter Real SpHP_H(fixed=false,start= 700.8e3);
                                                    //enthalpie réchauffeur
  parameter Real SpBP_Q(fixed=false, start= 16.41);
                                                    //débit réchauffeur
  parameter Real SpBP_H(fixed=false, start= 317.89e3);
                                                    //enthalpie réchauffeur

//  parameter Real CvmaxSurch(fixed=false, start=207);
                                                     //Cvmax Surchauffe

  parameter Real CvmaxSurch=200;
                                                     //Cvmax Surchauffe

  parameter Real kfric_Surch(fixed=true)= 7.97;  // coef de friction

  parameter Real kfric_SortieGV(fixed=true)= 10.62;  // coef de friction

ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv2(
    continuous_flow_reversal=false,
    Cvmax=90000,
    Pm(fixed=false, start=4045476.4188144),
    C1(h_vol(start=2944110.0)),
    Q(fixed=false, start=218.55650270820513),
    T(fixed=false, start=558.15),
    fluid=1,
    h(fixed=false, start=2985800.4338921653),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=15.63,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={-36,186},
        extent={{-10,-12},{10,12}},
        rotation=0)));
ThermoSysPro.WaterSteam.Machines.StaticCentrifugalPump Pp_HP(
    VRot=1466.0,
    VRotn=1400,
    rm=0.88,
    fixed_rot_or_power=1,
    adiabatic_compression=false,
    continuous_flow_reversal=false,
    Pm(fixed=false, start=2787395.7528958),
    Q(start=239, fixed=false),
    Qv(fixed=false, start=0.2529285181736584),
    a1=-2551,
    a3=594,
    b1=-16.5,
    b2=7.65,
    h(fixed=false, start=489938.01847988553),
    hn(fixed=false, start=454.3),
    C1(P(start=554266.93940273)),
    C2(
      h_vol(start=492487.0680898815),
      P(start=4902408.737046973),
      h(start=492487.0680898815))) annotation (Placement(visible=
          true, transformation(
        origin={8,-41},
        extent={{-14,13},{14,-13}},
        rotation=180)));
ThermoSysPro.WaterSteam.BoundaryConditions.SourceQ WaterIn_HeatSink(Q0=4000,
      h0=137770) annotation (Placement(visible=true, transformation(
        origin={699,-135},
        extent={{-15,-13},{15,13}},
        rotation=0)));
ThermoSysPro.WaterSteam.BoundaryConditions.SinkP WaterOut_HeatSink
    annotation (Placement(visible=true, transformation(
        origin={815,-131},
        extent={{-13,-13},{13,13}},
        rotation=0)));
ThermoSysPro.WaterSteam.HeatExchangers.SimpleDynamicCondenser HeatSink(
    A=100,
    Ccond=1,
    D=0.018,
    P0=7000,
    V=1000,
    Vf0=0.5,
    mode=0,
    ntubes=28700,
    steady_state=false,
    proe(d(start=990.7815182343393)),
    Cl(h(start=93158.8975545407), Q(start=176.88363316813704)),
    Pfond(start=7000),
    Cv(Q(start=175.84668052425536),h(start=2319048.8977629677)))
                       annotation (Placement(visible=true, transformation(
        origin={766,-130},
        extent={{-24,-26},{24,26}},
        rotation=0)));
  ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                            Turb_HP(
    Cst=CsHP,
    W_fric=1,
    eta_is_nom=0.90,
    mode_e=0,
    eta_is(start=0.90),
    Pe(start=4400000, fixed=false),
    Ps(start=754701.12073853),
    rhos(start=86, displayUnit="kg/m3"),
    pros(d(start=12.181371168968184)),
    Cs(h(start=2639678.79892835),  h_vol(start=2634009.304269357)))
    annotation (Placement(transformation(
        extent={{-19,24},{19,-24}},
        rotation=0,
        origin={49,180})));
  ThermoSysPro.WaterSteam.Machines.StodolaTurbine Turb_BP1(
    Cst=CsBP1a,
    W_fric=1,
    eta_is_nom=0.89,
    mode_e=0,
    eta_is(start=0.89),
    Pe(start=674700, fixed=false),
    Ps(start=5000),
    rhos(start=86, displayUnit="kg/m3"),
    pros(d(start=0.3188866622217379)),
    xm(start=0.9475591259592065),
    Ce(h_vol(start=2977121.8265122357)),
    Cs(h(start=2724624.7568167527), h_vol(start=2724624.756816755)))
    annotation (Placement(transformation(
        extent={{-20.5,36.5},{20.5,-36.5}},
        rotation=0,
        origin={499.5,178.5})));
  ThermoSysPro.WaterSteam.Sensors.SensorT T2(C2(h_vol(start=
            2944110.0)), C1(h_vol(start=2944110.0))) annotation (
      Placement(transformation(extent={{-328,178},{-312,196}})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P2b(C1(h_vol(start=
            2944110.0))) annotation (Placement(transformation(extent={{-85,178},
            {-70,196}})));
ThermoSysPro.WaterSteam.Machines.StaticCentrifugalPump Pp_LP(
    VRot=1565,
    VRotn=1400,
    rm=0.88,
    fixed_rot_or_power=1,
    adiabatic_compression=false,
    continuous_flow_reversal=false,
    Pm(fixed=false, start=360849.99833194),
    Q(start=181.649685154607, fixed=false),
    Qv(fixed=false, start=0.18343476011269896),
    a1=-477.7,
    a3=72.8,
    b1=-25.2,
    b2=9.47,
    h(fixed=false, start=163792.70351459057),
    hn(fixed=false, start=59.1),
    C1(P(start=6999.996697349103)),
    C2(h_vol(start=164202.32858770146), h(start=164202.3285877014)))
    annotation (Placement(visible=true, transformation(
        origin={559,-173},
        extent={{-13,13},{13,-13}},
        rotation=180)));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing4(C2(h_vol(
          start=2639678.7989283497), P(start=755921.1331388752)))
    annotation (Placement(transformation(
        extent={{-9.5,-12.5},{9.5,12.5}},
        rotation=0,
        origin={96.5,181.5})));
  ThermoSysPro.WaterSteam.Volumes.VolumeDTh Vol4 annotation (
      Placement(transformation(
        extent={{-12.5,-11.5},{12.5,11.5}},
        rotation=0,
        origin={131.5,180.5})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Pp4(
    L=1,
    D=0.4,
    lambda_fixed=false,
    C2(h_vol(start=2639678.7989283507), P(start=754564.1414536542)))
    annotation (Placement(transformation(
        extent={{-15,-14},{15,14}},
        rotation=180,
        origin={67,104})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing4b(
    K=1E-6,
    Q(start=22.3, fixed=false),
    Pm(start=754564.14138201),
    C2(h_vol(start=700800.0000000022))) annotation (Placement(
        transformation(
        extent={{-12,-11},{12,11}},
        rotation=270,
        origin={-106,57})));
  ThermoSysPro.WaterSteam.Machines.Generator Gen(eta=98) annotation (
     Placement(transformation(extent={{824,52},{880,108}})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P11(C1(h_vol(start=
            370950.6125952169))) annotation (Placement(
        transformation(
        extent={{-11,-11},{11,11}},
        rotation=180,
        origin={321,-51})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback FeedBack_P11
    annotation (Placement(transformation(
        extent={{-12,-12},{12,12}},
        rotation=0,
        origin={356,-196})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI PropInt_P11(
    k=1.2,
    Ti=1,
    permanent=true) annotation (Placement(transformation(
        extent={{-8.5,-8.5},{8.5,8.5}},
        rotation=0,
        origin={445.5,-196.5})));
  ThermoSysPro.WaterSteam.Junctions.SteamDryer Dry(
    Csv(h(start=2765900.246437356)),
    Cev(h(start=2639678.7989283497)),
    h(start=2765900.246437356)) annotation (Placement(transformation(
        extent={{-9.5,-11},{9.5,11}},
        rotation=0,
        origin={208.5,175})));
  ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss InvSing13
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={152,50})));
  ThermoSysPro.WaterSteam.Machines.StodolaTurbine
                            Turb_BP2(
    Cst= CsBP2,
    W_fric=1,
    eta_is_nom=0.89,
    mode_e=0,
    eta_is(start=0.93),
    Pe(start=80100, fixed=false),
    Ps(start=7055.7188610531),
    rhos(displayUnit="kg/m3"),
    pros(d(start=0.05475343284416332)),
    xm(start=0.9365574142290112),
    Ce(h_vol(start=2612098.433622263)))  annotation (Placement(
        transformation(
        extent={{-26,40},{26,-40}},
        rotation=0,
        origin={730,178})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Pip7(
    L=1,
    D=0.8,
    lambda_fixed=false,
    C2(h_vol(start=2612098.433622263), P(start=80083.75057360485)),
    Q(start=13.785457418931628)) annotation (Placement(
        transformation(
        extent={{-17,-13},{17,13}},
        rotation=270,
        origin={569,91})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing7(
    K=1E-6,
    Q(fixed=false, start=13.3),
    Pm(start=80083.750299409),
    C2(h_vol(start=317889.9999999945))) annotation (Placement(
        transformation(
        extent={{-10,-11},{10,11}},
        rotation=180,
        origin={484,39})));
  ThermoSysPro.WaterSteam.Volumes.VolumeCTh Vol9(
    P0=500000,
    h0=138000,
    dynamic_mass_balance=false,
    P(fixed=false),
    h(start=163383.07583870483)) annotation (Placement(
        transformation(
        extent={{-13,-12},{13,12}},
        rotation=180,
        origin={613,-174})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing9
    annotation (Placement(transformation(
        extent={{-11,-12},{11,12}},
        rotation=180,
        origin={663,-172})));
  ThermoSysPro.WaterSteam.Volumes.VolumeITh Bach(h(start=
          487361.6852763552), Ce3(Q(start=21.020048599606543)))
    annotation (Placement(transformation(extent={{36,-35},{-36,35}},
        rotation=90,
        origin={152,-41})));
  ThermoSysPro.WaterSteam.HeatExchangers.SimpleStaticCondenser SupH(
    Kc=3547,
    Kf=kfric_Surch,
    z1c=0,
    z2c=0,
    z1f=0,
    z2f=0,
    DPf(fixed=false, start=10000),
    Qc(fixed=false),
    Ec(h(start=2944110.0)),
    Sc(h_vol(start=1118785.4784898118)),
    Sf(h_vol(start=2977121.8265122357)),
    DPfc(start=39271.594830806),
    DPgc(start=0)) annotation (Placement(transformation(
        extent={{-25,-20},{25,20}},
        rotation=0,
        origin={333,180})));
 //   DPff(start=79999.999999991),
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing14(
    K=658,
    Q(start=38),
    Pm(start=694745.75289576),
    C2(P(start=674700, fixed=false))) annotation (Placement(
        transformation(
        extent={{10,-11},{-10,11}},
        rotation=0,
        origin={96,-41})));

  ThermoSysPro.WaterSteam.Volumes.VolumeDTh Vol2 annotation (
      Placement(transformation(extent={{-120,170},{-98,190}})));
  ThermoSysPro.WaterSteam.Volumes.VolumeATh Vol14(
    P0=500000,
    h0=138000,
    dynamic_mass_balance=false,
    P(start=600000, fixed=false),
    h(start=487388.9688698895),
    Ce1(Q(start=0.09907550047151403))) annotation (Placement(
        transformation(
        extent={{-14.5,15.5},{14.5,-15.5}},
        rotation=90,
        origin={64.5,-41.5})));
  ThermoSysPro.WaterSteam.Volumes.VolumeDTh Vol7(P(start=600000))
    annotation (Placement(transformation(
        extent={{-11,10},{11,-10}},
        rotation=0,
        origin={569,178})));
  ThermoSysPro.WaterSteam.HeatExchangers.NTUWaterHeating ReH_BP(
    SCondDes=ScondesBP,
    SPurge=SPurgeBP,
    Sp(Q(start=SpBP_Q, fixed=true), h(start=SpBP_H, fixed=true)),
    KCond=1500,
    KPurge=150,
    Ev(Q(start=13.970501574743839)),
    HDesF(start=370950.6125952169),
    SDes(start=1E-09),
    promeF(d(start=981.4875389741408)),
    HeiF(start=170859.06740391976),
    Hep(start=391757.3376993904)) annotation (Placement(
        transformation(extent={{450,-90},{374,6}})));
  //  SCondDes(fixed=true, start=1438.0),
  //  SPurge(fixed=true, start=1238.0),
  //  Sp(Q(fixed=false, start=13.3), h(fixed=false, start=147.55E3)),
  ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss InvSing8
    annotation (Placement(transformation(
        extent={{-9.5,-13},{9.5,13}},
        rotation=0,
        origin={590.5,-79})));
  ThermoSysPro.WaterSteam.HeatExchangers.NTUWaterHeating ReH_HP(
    SCondDes=ScondesHP,
    SPurge=SPurgeHP,
    Sp(Q(start=SpHP_Q, fixed=true), h(start=SpHP_H, fixed=true)),
    KCond=1500,
    KPurge=150,
    Ev(Q(start=13.970501574743839)),
    HDesF(start=690818.6628840484),
    SDes(start=1E-09),
    promeF(d(start=928.6768477967344)),
    HeiF(start=493476.20727467356),
    Hep(start=710469.7699779422),
    Se(h_vol(start=690818.6628840484))) annotation (Placement(
        transformation(extent={{-48,-88},{-122,6}})));
  ThermoSysPro.WaterSteam.PressureLosses.InvSingularPressureLoss
    invSingularPressureLoss2 annotation (Placement(transformation(
        extent={{-4,-5},{4,5}},
        rotation=270,
        origin={-64,-87})));

  ThermoSysPro.WaterSteam.Volumes.VolumeC Vol8(
    P0=5000,
    h0=138000,
    dynamic_mass_balance=false,
    steady_state=true,
    P(fixed=false),
    h(start=2142104.6394994687),
    rho(start=30)) annotation (Placement(transformation(
        extent={{-15,-15},{15,15}},
        rotation=270,
        origin={765,11})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing8
    annotation (Placement(transformation(
        extent={{-9.25,-12.25},{9.25,12.25}},
        rotation=270,
        origin={765.75,-70.75})));
ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv12(
    continuous_flow_reversal=false,
    Cvmax=CvmaxSurch,
    Pm(fixed=false, start=1500000),
    T(fixed=false, start=518.15),
    fluid=1,
    h(fixed=false, start=1084E3),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=1000,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={353.5,92.5},
        extent={{-12.5,-8.5},{12.5,8.5}},
        rotation=270)));
  //  Cv(start=119, fixed=true),
  //  Cv(start=15, fixed=false),
 //   Q(fixed=false, start=20),
  //  Q(fixed=true, start=20.57),
  ThermoSysPro.WaterSteam.Sensors.SensorT T6(Q(start=174))
    annotation (Placement(transformation(
        extent={{9,-9},{-9,9}},
        rotation=180,
        origin={387,173})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Feedback FeedBack_T6
    annotation (Placement(transformation(
        extent={{-13,13},{13,-13}},
        rotation=270,
        origin={431,127})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Continu.PI pI_Surch(
    k=1.2,
    Ti=1,
    permanent=true) annotation (Placement(transformation(
        extent={{7.5,-8},{-7.5,8}},
        rotation=0,
        origin={406,91.5})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe PropInt_T6(
    Starttime=20,
    Duration=1000,
    Initialvalue=260.44 + 273.15,
    Finalvalue=260.44 + 273.15) annotation (Placement(transformation(
        extent={{8,-9},{-8,9}},
        rotation=0,
        origin={468,127})));
  ThermoSysPro.WaterSteam.PressureLosses.LumpedStraightPipe Pip2(
    L=20,
    D=0.5,
    lambda_fixed=false,
    C1(P(start=4468505.728150848), h_vol(start=2944110.0)))
    annotation (Placement(transformation(
        extent={{-19,-14},{19,14}},
        rotation=0,
        origin={-195,180})));
  ThermoSysPro.WaterSteam.PressureLosses.SingularPressureLoss Sing2(
    K=kfric_SortieGV,
    Pm(start=4450000.1220833),
    C1(P(start=4500000, fixed=false), h_vol(start=2944110.0)))
    annotation (Placement(transformation(
        extent={{-8.5,-12.5},{8.5,12.5}},
        rotation=0,
        origin={-246.5,179.5})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P2 annotation (Placement(
        transformation(extent={{-294,178},{-278,196}})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P4(C1(h_vol(start=
            2639678.7989283497))) annotation (Placement(
        transformation(
        extent={{-9,-10},{9,10}},
        rotation=90,
        origin={124,267})));
  ThermoSysPro.WaterSteam.Sensors.SensorT T4 annotation (Placement(
        transformation(
        extent={{-10,-9},{10,9}},
        rotation=90,
        origin={125,230})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P1(C1(h_vol(start=
            690818.6628840484))) annotation (Placement(
        transformation(
        extent={{-8.5,-8},{8.5,8}},
        rotation=180,
        origin={-341.5,-46})));
  ThermoSysPro.WaterSteam.Sensors.SensorT T1(C1(h_vol(start=
            690818.6628840484))) annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=180,
        origin={-304,-46})));
  ThermoSysPro.WaterSteam.Sensors.SensorQ Q1 annotation (Placement(
        transformation(
        extent={{-8,-9},{8,9}},
        rotation=180,
        origin={-272,-47})));
  ThermoSysPro.WaterSteam.Sensors.SensorQ Q2 annotation (Placement(
        transformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={-349,187})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI SG_Secondary_Out
    annotation (Placement(transformation(extent={{-390,48},{-374,64}}),
        iconTransformation(extent={{-390,48},{-374,64}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI SG_Secondary_In
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=0,
        origin={-382,-2}),
        iconTransformation(extent={{-8,-8},{8,8}},
        rotation=0,
        origin={-382,-2})));
ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv1(
    continuous_flow_reversal=false,
    Cvmax=1200,
    Ouv(signal(start=0.7, fixed=false)),
    Q(start=239.68),
    Pm(fixed=false, start=4900000),
    T(fixed=false, start=436.15),
    fluid=1,
    h(fixed=false, start=691E3),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=1000,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={-210,-49},
        extent={{12,13},{-12,-13}},
        rotation=0)));
ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv10(
    continuous_flow_reversal=false,
    Cvmax=3167,
    Pm(fixed=false, start=1500000),
    T(fixed=false, start=518.15),
    fluid=1,
    h(fixed=false, start=1084E3),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=1000,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={504,-166.5},
        extent={{12,-10.5},{-12,10.5}},
        rotation=0)));
  ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv4(
    continuous_flow_reversal=true,
    Cvmax=0.7e6,
    Pm(fixed=false, start=735900.74476952),
    C1(h_vol(start=2626632.7346792007)),
    Q(fixed=false, start=163),
    T(fixed=false, start=558.15),
    fluid=1,
    h(fixed=false, start=2628E3),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=3.8,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={452,186},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  ThermoSysPro.WaterSteam.PressureLosses.ControlValve Vv7(
    continuous_flow_reversal=true,
    Cvmax=2.8e7,
    Pm(fixed=false, start=70000),
    C1(h_vol(start=2626632.7346792007)),
    Q(fixed=false, start=163),
    T(fixed=false, start=558.15),
    fluid=1,
    h(fixed=false, start=2628E3),
    mode=0,
    option_rho_water=1,
    p_rho(displayUnit="kg/m3") = 0,
    rho(
      fixed=false,
      start=3.8,
      displayUnit="kg/m3")) annotation (Placement(visible=true,
        transformation(
        origin={670,185},
        extent={{-12,-11},{12,11}},
        rotation=0)));
  ThermoSysPro.WaterSteam.Volumes.VolumeATh Vol1(
    P0=500000,
    h0=138000,
    dynamic_mass_balance=false,
    P(start=600000, fixed=false),
    h(start=487388.9688698895),
    Ce1(Q(start=0.09907550047151403))) annotation (Placement(
        transformation(
        extent={{14,-15},{-14,15}},
        rotation=270,
        origin={-165,-42})));
  ThermoSysPro.WaterSteam.Volumes.VolumeATh Vol10(
    dynamic_mass_balance=false,
    steady_state=true,
    mode=1) annotation (Placement(transformation(
        extent={{-9.5,-9.5},{9.5,9.5}},
        rotation=90,
        origin={468.5,-104.5})));
  ThermoSysPro.WaterSteam.Volumes.VolumeATh Vol11(
    dynamic_mass_balance=false,
    steady_state=true,
    mode=1) annotation (Placement(transformation(
        extent={{-13,-11},{13,11}},
        rotation=180,
        origin={263,-57})));
  ThermoSysPro.WaterSteam.Volumes.VolumeB   Vol5 annotation (
      Placement(transformation(
        extent={{-10,-9},{10,9}},
        rotation=0,
        origin={246,179})));
  ThermoSysPro.WaterSteam.Volumes.VolumeCTh Vol6(
    P0=500000,
    h0=138000,
    dynamic_mass_balance=false,
    P(fixed=false),
    h(start=163383.07583870483)) annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=0,
        origin={418,180})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI HeatNetwk_HP_In
    annotation (Placement(transformation(extent={{-172,-228},{-156,-212}}),
                                                                          iconTransformation(extent={{-172,
            -228},{-156,-212}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI HeatNetwk_HP_Out
    annotation (Placement(transformation(extent={{-294,312},{-278,328}}),
                                                                        iconTransformation(extent={{-294,312},{-278,328}})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI HeatNetwk_IP_In
    annotation (Placement(transformation(extent={{56,-228},{72,-212}}), iconTransformation(extent={{56,-228},
            {72,-212}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI HeatNetwk_IP_Out
    annotation (Placement(transformation(extent={{124,314},{140,330}}), iconTransformation(extent={{124,314},
            {140,330}})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI HeatNetwk_LP_In
    annotation (Placement(transformation(extent={{872,4},{888,20}}),    iconTransformation(extent={{872,4},
            {888,20}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI HeatNetwk_LP_Out
    annotation (Placement(transformation(extent={{560,312},{576,328}}), iconTransformation(extent={{560,312},
            {576,328}})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P4b(C1(h_vol(start=2639678.7989283497)))
    annotation (Placement(transformation(
        extent={{-8,-9},{8,9}},
        rotation=0,
        origin={174,187})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P7(C1(h_vol(start=
            2639678.7989283497))) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={632,186})));
  ThermoSysPro.WaterSteam.Sensors.SensorP P3(C1(h_vol(start=
            2944110.0))) annotation (Placement(transformation(
        extent={{8,9},{-8,-9}},
        rotation=180,
        origin={-1.77636e-15,185})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Sources.Rampe Ouv_Vv10(
    Starttime=100,
    Duration=900,
    Initialvalue=1,
    Finalvalue=1) annotation (Placement(transformation(
        extent={{-8.5,-9.5},{8.5,9.5}},
        rotation=270,
        origin={503.5,-132.5})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_Vv_TurbHP_In_Opening
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-36,320})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_PpHP_RPM
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={8,-218})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal P_TurbIP_In
    annotation (Placement(transformation(
        extent={{-9,-9},{9,9}},
        rotation=90,
        origin={173,323}), iconTransformation(
        extent={{-9,-9},{9,9}},
        rotation=90,
        origin={173,323})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_Vv_TurbIP_In_Opening
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={452,316})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal P_TurbLP_In
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={632,322})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_Vv_TurbLP_In_Opening
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={670,318})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_P_PpBP_Out
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={356,-224})));
  ThermoSysPro.WaterSteam.Volumes.VolumeDTh Vol3
    annotation (Placement(transformation(extent={{-156,170},{-134,190}})));
  ThermoSysPro.WaterSteam.Volumes.VolumeC Vol_b(
    P0=5000,
    h0=138000,
    dynamic_mass_balance=false,
    steady_state=true,
    P(fixed=false),
    h(start=2142104.6394994687),
    rho(start=30)) annotation (Placement(transformation(
        extent={{-15,-15},{15,15}},
        rotation=270,
        origin={765,-33})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal P_SG_Outb
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-78,322})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal T_SG_Out
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-382,230})));
  ThermoSysPro.InstrumentationAndControl.Connectors.InputReal Set_Vv_SG_In_Opening
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-210,-220})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal P_TurbHP_1stRow
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={2,322})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal P_SG_Outa
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-386,260})));
  ThermoSysPro.WaterSteam.Connectors.FluidInletI Turb_IP_In
    annotation (Placement(transformation(extent={{260,314},{276,330}}), iconTransformation(extent={{260,314},
            {276,330}})));
  ThermoSysPro.WaterSteam.Connectors.FluidOutletI Pp_LP_Out
    annotation (Placement(transformation(extent={{222,-228},{238,-212}}),
                                                                        iconTransformation(extent={{222,
            -228},{238,-212}})));
  ThermoSysPro.InstrumentationAndControl.Connectors.OutputReal Pwe
    annotation (Placement(transformation(extent={{874,190},{894,210}})));
equation

  Pwe.signal = Pw_eGrid;
//  PwHyHP.signal = Pw_COG_HP;
//  PwHyIP.signal = Pw_COG_IP;
//  PwHyLP.signal = Pw_COG_LP;

 // Pwth_SG = e_Boiler.Signal_Elec.signal;
  Pwth_SG =Q2.C1.Q*Q2.C1.h - Q1.C2.Q*Q1.C2.h;
  Pw_eGrid =Gen.Welec -Pp_LP.Wh /0.99 - Pp_HP.Wh/0.99;
  Eff_Cycle_net = Pw_eGrid/Pwth_SG;
  Eff_Cycle_brut =Gen.Welec/(Gen.eta/100)/Pwth_SG;
//  T_COG_IP =TCond_COG_IP.T - 273.15;
//  T_COG_HP =TCond_COG_HP.T - 273.15;
//  T_COG_LP =TCond_COG_LP.T - 273.15;
//  Pw_COG_IP =HE_Hybrid_IP.W;
 // Pw_COG_LP =HE_Hybrid_LP.W;
//  Pw_COG_HP =HE_Hybrid_HP.W;
//  Heat_To_ePower_ratio = (Pw_COG_IP + Pw_COG_LP + Pw_COG_HP)/ Pw_eGrid;
 // Tin_SG = e_Boiler.Te - 273.1;
  Tin_SG =T1.T - 273.15;
  Tout_SG =T2.T - 273.15;
  Pin_SG =P1.C1.P/1e5;
  Pout_SG =P2.C1.P/1E5;

  Nuclear_Pw_load = Pwth_SG/540E6;
  connect(HeatSink.Cse, WaterOut_HeatSink.C) annotation (Line(points={{790,
          -135.2},{790,-131},{802,-131}},      color={0,0,255}));
  connect(WaterIn_HeatSink.C, HeatSink.Cee) annotation (Line(points={{714,-135},
          {714,-135.72},{742,-135.72}},             color={0,0,255}));
  connect(P2b.C2, Vv2.C1) annotation (Line(points={{-69.85,179.8},{-69.85,178.8},
          {-46,178.8}},                                   color={0,0,
          255},
      thickness=0.5));
  connect(Turb_HP.MechPower, Gen.Wmec1) annotation (Line(points={{69.9,201.6},{
          100,201.6},{100,292},{798,292},{798,102.4},{824,102.4}},
                       color={0,0,255}));
  connect(Dry.Csl, InvSing13.C1) annotation (Line(points={{208.595,164},{
          208.595,76},{152,76},{152,60}},
        color={0,0,255},
      thickness=0.5));
  connect(Vol9.Ce1, Sing9.C2) annotation (Line(points={{626,-174},{628,-172},{
          652,-172}},                       color={0,0,255},
      thickness=0.5));
  connect(P11.Measure, FeedBack_P11.u1) annotation (Line(points={{321,-62.22},{
          321,-196},{342.8,-196}},
        color={0,0,255}));
  connect(FeedBack_P11.y, PropInt_P11.u) annotation (Line(points={{369.2,-196},
          {372,-196.5},{436.15,-196.5}},               color={0,0,
          255}));
  connect(Sing7.C1, Pip7.C2) annotation (Line(points={{494,39},{569,39},{569,74}},
                         color={0,0,255},
      thickness=0.5));
  connect(Sing9.C1, HeatSink.Cl) annotation (Line(points={{674,-172},{766.48,
          -172},{766.48,-156}},         color={0,0,255},
      thickness=0.5));
  connect(Vol9.Cs,Pp_LP. C1) annotation (Line(points={{600,-174},{600,-173},{
          572,-173}},                       color={0,0,255},
      thickness=0.5));
  connect(Sing4b.C1, Pp4.C2) annotation (Line(points={{-106,69},{-108,69},{-108,
          104},{52,104}},         color={0,0,255},
      thickness=0.5));
  connect(Turb_BP1.MechPower, Gen.Wmec3) annotation (Line(points={{522.05,
          211.35},{520,211.35},{520,210},{552,210},{552,264},{786,264},{786,80},
          {824,80}},color={0,0,255}));
  connect(Turb_BP2.MechPower, Gen.Wmec5) annotation (Line(points={{758.6,214},{
          774,214},{774,56},{824,56},{824,57.6}},          color={0,
          0,255}));
  connect(InvSing13.C2, Bach.Ce2) annotation (Line(points={{152,40},{152,-5}},
                                 color={0,0,255},
      thickness=0.5));
  connect(Turb_HP.Cs, Sing4.C1) annotation (Line(points={{68.19,180},{68.19,
          181.5},{87,181.5}},        color={0,0,255},
      thickness=0.5));
  connect(Vol2.Cs2, SupH.Ec) annotation (Line(points={{-109,170.2},{-109,132},{
          318,132},{318,160}},         color={0,0,255},
      thickness=0.5));
  connect(Sing14.C2, Vol14.Ce2) annotation (Line(points={{86,-41},{88,-41.5},{
          80,-41.5}},                               color={0,0,255},
      thickness=0.5));
  connect(Vol7.Ce, Turb_BP1.Cs) annotation (Line(points={{558,178},{556,178.5},
          {520.205,178.5}},                      color={0,0,255},
      thickness=0.5));
  connect(Sing7.C2, ReH_BP.Ev) annotation (Line(points={{474,39},{390,39},{390,
          -26.64},{389.2,-26.64}},              color={0,0,255},
      thickness=0.5));
  connect(ReH_BP.Sp, InvSing8.C1) annotation (Line(points={{434.8,-57.84},{436,
          -57.84},{436,-79},{581,-79}},              color={0,0,255},
      thickness=0.5));
  connect(ReH_BP.Se, P11.C1) annotation (Line(points={{374,-42},{376,-42.2},{
          332,-42.2}},                  color={0,0,255},
      thickness=0.5));
  connect(Sing4b.C2, ReH_HP.Ev) annotation (Line(points={{-106,45},{-106,-25.96},
          {-107.2,-25.96}},                       color={0,0,255},
      thickness=0.5));
  connect(ReH_HP.Sp, invSingularPressureLoss2.C1) annotation (Line(
        points={{-62.8,-56.51},{-64,-56.51},{-64,-83}},  color={0,0,
          255},
      thickness=0.5));
  connect(invSingularPressureLoss2.C2, Bach.Ce1) annotation (Line(
        points={{-64,-91},{-64,-104},{-16,-104},{-16,20},{124,20},{124,-5}},
                                                   color={0,0,255},
      thickness=0.5));
  connect(Pp_HP.C2, ReH_HP.Ee) annotation (Line(points={{-6,-41},{-47.26,-41}},
        color={0,0,255},
      thickness=0.5));

  connect(Sing8.C2, HeatSink.Cv) annotation (Line(points={{765.75,-80},{766,-80},
          {766,-104}},                                color={0,0,255},
      thickness=0.5));
  connect(InvSing8.C2, Vol8.Ce3) annotation (Line(points={{600,-79},{740,-79},{
          740,11},{750,11}},                  color={0,0,255},
      thickness=0.5));
  connect(SupH.Sc, Vv12.C1) annotation (Line(points={{348,160},{348.4,156},{
          348.4,105}},      color={0,0,255},
      thickness=0.5));
  connect(SupH.Sf, T6.C1) annotation (Line(points={{358,179.8},{378,179.8},{378,
          180.2}},           color={0,0,255},
      thickness=0.5));
  connect(T6.Measure, FeedBack_T6.u1) annotation (Line(points={{387,164},{387,
          152},{431,152},{431,141.3}},
                           color={0,0,255}));
  connect(Vv12.Ouv, pI_Surch.y) annotation (Line(points={{362.85,92.5},{364,
          91.5},{397.75,91.5}},               color={0,0,255}));
  connect(FeedBack_T6.u2, PropInt_T6.y) annotation (Line(points={{445.3,127},{
          459.2,127}},           color={0,0,255}));
  connect(Vol2.Cs3, P2b.C1) annotation (Line(points={{-98,180},{-98,179.8},{-85,
          179.8}},                   color={0,0,255},
      thickness=0.5));
  connect(T2.C2, P2.C1) annotation (Line(points={{-311.84,179.8},{-311.84,180},
          {-294,180},{-294,179.8}},
                        color={0,0,255},
      thickness=0.5));
  connect(P2.C2, Sing2.C1) annotation (Line(points={{-277.84,179.8},{-272,179.5},
          {-255,179.5}},              color={0,0,255},
      thickness=0.5));
  connect(Sing2.C2, Pip2.C1) annotation (Line(points={{-238,179.5},{-238,180},{
          -214,180}},                         color={0,0,255},
      thickness=0.5));
  connect(T4.C2, P4.C1) annotation (Line(points={{132.2,240.2},{132.2,250},{132,
          250},{132,258}},                          color={0,0,255},
      thickness=0.5));
  connect(P1.C1, T1.C2) annotation (Line(points={{-333,-39.6},{-312.16,-39.6}},
                                          color={0,0,255},
      thickness=0.5));
  connect(T1.C1, Q1.C2) annotation (Line(points={{-296,-39.6},{-296,-39.8},{
          -280.16,-39.8}},
                        color={0,0,255},
      thickness=0.5));
  connect(T2.C1, Q2.C2) annotation (Line(points={{-328,179.8},{-328,180},{
          -339.82,180},{-339.82,179.8}},
                           color={0,0,255},
      thickness=0.5));
  connect(SG_Secondary_Out, Q2.C1) annotation (Line(points={{-382,56},{-358,56},
          {-358,179.8}},           color={0,0,255},
      thickness=0.5));
  connect(Q1.C1, Vv1.C2) annotation (Line(points={{-264,-39.8},{-264,-41.2},{
          -222,-41.2}},         color={0,0,255},
      thickness=0.5));
  connect(Pp_LP.C2, Vv10.C1) annotation (Line(points={{546,-173},{548,-172.8},{
          516,-172.8}},                         color={0,0,255},
      thickness=0.5));
  connect(Vv7.C2, Turb_BP2.Ce) annotation (Line(points={{682,178.4},{684,178},{
          703.74,178}},
        color={0,0,255},
      thickness=0.5));
  connect(Vol1.Ce2, ReH_HP.Se) annotation (Line(points={{-150,-42},{-148,-41},{
          -122,-41}},                       color={0,0,255},
      thickness=0.5));
  connect(Vv1.C1, Vol1.Cs2) annotation (Line(points={{-198,-41.2},{-196,-42},{
          -179.7,-42}},                         color={0,0,255},
      thickness=0.5));
  connect(Pp_HP.C1, Vol14.Cs2) annotation (Line(points={{22,-41},{22,-41.5},{
          49.31,-41.5}},
        color={0,0,255},
      thickness=0.5));
  connect(Vv10.C2, Vol10.Ce1) annotation (Line(points={{492,-172.8},{468.5,
          -172.8},{468.5,-114}},    color={0,0,255},
      thickness=0.5));
  connect(Vol10.Cs1, ReH_BP.Ee) annotation (Line(points={{468.5,-95},{468.5,-42},
          {450.76,-42}},           color={0,0,255},
      thickness=0.5));
  connect(P11.C2, Vol11.Ce1) annotation (Line(points={{309.78,-42.2},{284,-42.2},
          {284,-57},{276,-57}},               color={0,0,255},
      thickness=0.5));
  connect(Vol11.Cs2, Bach.Ce4) annotation (Line(points={{263,-46.22},{263,-41},
          {187,-41}},                       color={0,0,255},
      thickness=0.5));
  connect(Vol4.Cs2, Pp4.C1) annotation (Line(points={{131.5,169.23},{131.5,104},
          {82,104}},                         color={0,0,255},
      thickness=0.5));
  connect(Vol4.Cs1, T4.C1) annotation (Line(points={{131.5,192},{131.5,212},{
          132.2,212},{132.2,220}},       color={0,0,255},
      thickness=0.5));
  connect(Sing4.C2, Vol4.Ce) annotation (Line(points={{106,181.5},{106,180.5},{
          119,180.5}},         color={0,0,255},
      thickness=0.5));
  connect(Vol7.Cs1, Pip7.C1) annotation (Line(points={{569,168},{569,108}},
                                  color={0,0,255},
      thickness=0.5));
  connect(Vol5.Cs2, SupH.Ef) annotation (Line(points={{246,170.18},{246,166},{
          278,166},{278,180},{308,180}},
                             color={0,0,255},
      thickness=0.5));
  connect(T6.C2, Vol6.Ce1) annotation (Line(points={{396.18,180.2},{396.18,180},
          {410,180}},                   color={0,0,255},
      thickness=0.5));
  connect(Vol4.Cs3, P4b.C1) annotation (Line(points={{144,180.5},{160,180.5},{
          160,179.8},{166,179.8}},                                                                                     color={0,0,255},
      thickness=0.5));
  connect(P7.C2, Vv7.C1) annotation (Line(points={{642.2,178},{642.2,178.4},{
          658,178.4}},             color={0,0,255},
      thickness=0.5));
  connect(Vol7.Cs3, P7.C1) annotation (Line(points={{580,178},{622,178}},
                                         color={0,0,255},
      thickness=0.5));
  connect(Turb_HP.Ce, P3.C2) annotation (Line(points={{29.81,180},{28,177.8},{
          8.16,177.8}},           color={0,0,255},
      thickness=0.5));
  connect(P3.C1, Vv2.C2) annotation (Line(points={{-8,177.8},{-8,178},{-26,178},
          {-26,178.8}},                       color={0,0,255},
      thickness=0.5));
  connect(Ouv_Vv10.y, Vv10.Ouv) annotation (Line(points={{503.5,-141.85},{504,
          -141.85},{504,-154.95}},                 color={0,0,255}));
  connect(P4b.Measure, P_TurbIP_In) annotation (Line(points={{174,196.18},{174,
          260},{173,260},{173,323}},                                                                      color={0,0,255}));
  connect(Vv4.Ouv, Set_Vv_TurbIP_In_Opening) annotation (Line(points={{452,197},
          {452,316}},                                                                                           color={0,0,255}));
  connect(P7.Measure, P_TurbLP_In) annotation (Line(points={{632,196.2},{632,
          322}},                                                                                         color={0,0,255}));
  connect(Vv7.Ouv, Set_Vv_TurbLP_In_Opening) annotation (Line(points={{670,
          197.1},{670,318}},                                                                                      color={0,0,255}));
  connect(FeedBack_P11.u2, Set_P_PpBP_Out) annotation (Line(points={{356,-209.2},
          {356,-224}},                                                                                   color={0,0,255}));
  connect(Pip2.C2, Vol3.Ce) annotation (Line(points={{-176,180},{-156,180}},                                  color={0,0,255},
      thickness=0.5));
  connect(Vol_b.Cs, Sing8.C1) annotation (Line(points={{765,-48},{765.75,-48},{
          765.75,-61.5}},                                                                                color={0,0,255},
      thickness=0.5));
  connect(Vol8.Cs, Vol_b.Ce1) annotation (Line(points={{765,-4},{765,-18}},            color={0,0,255},
      thickness=0.5));
  connect(P_SG_Outb, P2b.Measure) annotation (Line(points={{-78,322},{-78,
          196.18},{-77.5,196.18}},                                                                        color={0,0,255}));
  connect(Set_Vv_TurbHP_In_Opening, Vv2.Ouv) annotation (Line(points={{-36,320},
          {-36,199.2}},                                                                                               color={0,0,255}));
  connect(HeatNetwk_HP_In, Vol1.Ce1) annotation (Line(points={{-164,-220},{-165,
          -220},{-165,-56}},                                                                                           color={0,0,255},
      thickness=0.5));
  connect(T2.Measure, T_SG_Out) annotation (Line(points={{-320,196},{-320,230},
          {-382,230}},                                                                                                          color={0,0,255}));
  connect(Set_PpHP_RPM, Pp_HP.rpm_or_mpower) annotation (Line(points={{8,-218},
          {8,-55.3}},                                                                                         color={0,0,255}));
  connect(P_TurbHP_1stRow, P3.Measure) annotation (Line(points={{2,322},{0,322},
          {0,194.18}},                                                                          color={0,0,255}));
  connect(P2.Measure, P_SG_Outa) annotation (Line(points={{-286,196.18},{-292,
          196.18},{-292,260},{-386,260}},                                                         color={0,0,255}));
  connect(Vol11.Cs1, Pp_LP_Out) annotation (Line(points={{250,-57},{230,-57},{
          230,-220}},                                                                    color={0,0,255},
      thickness=0.5));
  connect(Dry.Csv, Vol5.Ce1) annotation (Line(points={{217.905,179.4},{216,179},
          {236,179}},                                                                         color={0,0,255},
      thickness=0.5));
  connect(Vol5.Ce2, Turb_IP_In) annotation (Line(points={{256,179},{268,179},{
          268,322}},                                                                                                   color={0,0,255},
      thickness=0.5));
  connect(Turb_BP1.Ce, Vv4.C2) annotation (Line(points={{478.795,178.5},{
          478.795,180},{462,180}},   color={0,0,255},
      thickness=0.5));
  connect(Vv4.C1, Vol6.Cs)
    annotation (Line(points={{442,180},{426,180}},
                                                 color={0,0,255},
      thickness=0.5));
  connect(P4b.C2, Dry.Cev) annotation (Line(points={{182.16,179.8},{182.16,
          179.4},{199.095,179.4}}, color={0,0,255},
      thickness=0.5));
  connect(Vol3.Cs3, Vol2.Ce) annotation (Line(
      points={{-134,180},{-120,180}},
      color={0,0,255},
      thickness=0.5));
  connect(Vol7.Cs2, HeatNetwk_LP_Out) annotation (Line(
      points={{569,187.8},{569,320},{568,320}},
      color={0,0,255},
      thickness=0.5));
  connect(Vol8.Ce1, Turb_BP2.Cs) annotation (Line(
      points={{765,26},{766,26},{766,178},{756.26,178}},
      color={0,0,255},
      thickness=0.5));
  connect(Vol8.Ce2, HeatNetwk_LP_In) annotation (Line(
      points={{778.5,11},{880,11},{880,12}},
      color={0,0,255},
      thickness=0.5));
  connect(FeedBack_T6.y, pI_Surch.u) annotation (Line(points={{431,112.7},{431,
          91.5},{414.25,91.5}}, color={0,0,255}));
  connect(Vol14.Ce1, HeatNetwk_IP_In) annotation (Line(
      points={{64.5,-56},{64,-56},{64,-220},{64,-220}},
      color={0,0,255},
      thickness=0.5));
  connect(PropInt_P11.y, Pp_LP.rpm_or_mpower) annotation (Line(points={{454.85,
          -196.5},{560,-196.5},{560,-187.3},{559,-187.3}}, color={0,0,255}));
  connect(P1.C2, SG_Secondary_In) annotation (Line(
      points={{-350.17,-39.6},{-358,-39.6},{-358,-2},{-382,-2}},
      color={0,0,255},
      thickness=0.5));
  connect(Vol2.Cs1, HeatNetwk_HP_Out) annotation (Line(
      points={{-109,190},{-108,190},{-108,284},{-286,284},{-286,320}},
      color={0,0,255},
      thickness=0.5));
  connect(P4.C2, HeatNetwk_IP_Out) annotation (Line(
      points={{132,276.18},{132,322}},
      color={0,0,255},
      thickness=0.5));
  connect(Set_Vv_SG_In_Opening, Vv1.Ouv)
    annotation (Line(points={{-210,-220},{-210,-63.3}}, color={0,0,255}));
  connect(Sing14.C1, Bach.Cs4) annotation (Line(
      points={{106,-41},{117,-41}},
      color={0,0,255},
      thickness=0.5));
  connect(Bach.Ce3, Vv12.C2) annotation (Line(
      points={{180,-5},{180,20},{348.4,20},{348.4,80}},
      color={0,0,255},
      thickness=0.5));
  annotation (Diagram(coordinateSystem(extent={{-380,-220},{880,320}})), Icon(
        coordinateSystem(extent={{-380,-220},{880,320}}), graphics={
        Rectangle(extent={{-380,320},{-222,-162}}, pattern=
              LinePattern.None),
        Rectangle(extent={{-380,322},{-320,192}}, pattern=
              LinePattern.None),
        Rectangle(
          extent={{-454,286},{-372,162}},
          pattern=LinePattern.None,
          lineThickness=1,
          lineColor={0,0,0}),
        Rectangle(
          extent={{-332,294},{-208,192}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          lineThickness=1),
        Rectangle(
          extent={{-294,210},{-220,136}},
          lineColor={0,0,0},
          pattern=LinePattern.None,
          lineThickness=1),
        Ellipse(
          extent={{-294,258},{-174,112}},
          lineColor={238,46,47},
          pattern=LinePattern.None,
          lineThickness=0.5),
        Rectangle(
          extent={{-256,104},{-116,210}},
          lineColor={238,46,47},
          pattern=LinePattern.None,
          lineThickness=0.5),                                                                            Rectangle(lineColor={0,140,72},      fillColor={0,140,72},      fillPattern=
              FillPattern.Solid,                                                                                                                                                                          lineThickness = 1, extent={{-380,
              320},{880,-220}}),                                                                                                                                                                                                        Rectangle(lineColor={0,140,72},      fillColor = {234, 234, 234}, fillPattern = FillPattern.Solid, lineThickness = 1, extent={{-370,
              312},{868,-210}}),
        Text(
          extent={{112,154},{444,-58}},
          textColor={0,140,72},
          fontName="Bahnschrift",
          textString="BOP")}),
    experiment(Tolerance=1e-06, __Dymola_Algorithm="Dassl"),
    Documentation(info="<html>
<p>BOP, static version : matches CYCLOP thermodynamics optimisation (see WP2 meeting, June 2023)</p>
<p>&gt; OD heat source </p>
<p>&gt; Cogeneration is ready to be plugged : MSL connector </p>
</html>"));
end BOP_TSPro_TESdirect;
