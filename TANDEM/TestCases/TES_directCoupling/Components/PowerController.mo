within TANDEM.TestCases.TES_directCoupling.Components;
model PowerController
  Modelica.Blocks.Interfaces.RealInput PowerSetpoint
    annotation (Placement(transformation(extent={{-128,30},{-88,70}})));
  Modelica.Blocks.Interfaces.RealInput PowerOutput
    annotation (Placement(transformation(extent={{-128,-70},{-88,-30}})));
  Modelica.Blocks.Interfaces.RealOutput ChargingValve
    annotation (Placement(transformation(extent={{98,40},{118,60}})));
  Modelica.Blocks.Interfaces.RealOutput DischargingPump
    annotation (Placement(transformation(extent={{98,-60},{118,-40}})));
  Modelica.Blocks.Continuous.LimPID PID(
    yMax=0.95,
    yMin=0.01,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y_start=0.05)
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  Modelica.Blocks.Continuous.LimPID PID1(
    k=1500,
    Ti=1500,
    yMax=1500,
    yMin=1240,
    initType=Modelica.Blocks.Types.Init.SteadyState)
    annotation (Placement(transformation(extent={{40,-60},{60,-40}})));
  Modelica.Blocks.Sources.Constant const(k=0)
    annotation (Placement(transformation(extent={{4,42},{20,58}})));
  Modelica.Blocks.Sources.Constant const1(k=0)
    annotation (Placement(transformation(extent={{6,-58},{22,-42}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-38,58},{-22,42}})));
  Modelica.Blocks.Sources.Constant const2(k=170e6)
    annotation (Placement(transformation(extent={{-100,82},{-84,98}})));
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-72,-6},{-60,6}})));
  Modelica.Blocks.Logical.Less less
    annotation (Placement(transformation(extent={{-68,56},{-56,44}})));
  Modelica.Blocks.Sources.Constant const3(k=0)
    annotation (Placement(transformation(extent={{-68,68},{-56,80}})));
  Modelica.Blocks.Logical.Switch switch2
    annotation (Placement(transformation(extent={{-38,-58},{-22,-42}})));
  Modelica.Blocks.Logical.Greater greater
    annotation (Placement(transformation(extent={{-64,-44},{-52,-56}})));
  Modelica.Blocks.Sources.Constant const4(k=0)
    annotation (Placement(transformation(extent={{-64,-78},{-52,-66}})));
  Modelica.Blocks.Math.Gain gain(k=1/170e6)
    annotation (Placement(transformation(extent={{-54,-4},{-46,4}})));
equation
  connect(const.y, PID.u_s)
    annotation (Line(points={{20.8,50},{38,50}}, color={0,0,127}));
  connect(const1.y, PID1.u_s)
    annotation (Line(points={{22.8,-50},{38,-50}}, color={0,0,127}));
  connect(PowerOutput, add.u2) annotation (Line(points={{-108,-50},{-84,-50},{
          -84,-3.6},{-73.2,-3.6}}, color={0,0,127}));
  connect(PowerSetpoint, add.u1) annotation (Line(points={{-108,50},{-84,50},{
          -84,3.6},{-73.2,3.6}}, color={0,0,127}));
  connect(less.y, switch1.u2)
    annotation (Line(points={{-55.4,50},{-39.6,50}}, color={255,0,255}));
  connect(const2.y, less.u2) annotation (Line(points={{-83.2,90},{-78,90},{-78,
          54.8},{-69.2,54.8}}, color={0,0,127}));
  connect(less.u1, PowerSetpoint)
    annotation (Line(points={{-69.2,50},{-108,50}}, color={0,0,127}));
  connect(switch1.y, PID.u_m) annotation (Line(points={{-21.2,50},{-6,50},{-6,
          32},{50,32},{50,38}}, color={0,0,127}));
  connect(const3.y, switch1.u3) annotation (Line(points={{-55.4,74},{-48,74},{
          -48,56.4},{-39.6,56.4}}, color={0,0,127}));
  connect(PID.y, ChargingValve)
    annotation (Line(points={{61,50},{108,50}}, color={0,0,127}));
  connect(const4.y, switch2.u3) annotation (Line(points={{-51.4,-72},{-46,-72},
          {-46,-56.4},{-39.6,-56.4}}, color={0,0,127}));
  connect(const2.y, greater.u2) annotation (Line(points={{-83.2,90},{-78,90},{
          -78,-45.2},{-65.2,-45.2}}, color={0,0,127}));
  connect(greater.y, switch2.u2)
    annotation (Line(points={{-51.4,-50},{-39.6,-50}}, color={255,0,255}));
  connect(switch2.y, PID1.u_m) annotation (Line(points={{-21.2,-50},{2,-50},{2,
          -70},{50,-70},{50,-62}}, color={0,0,127}));
  connect(PID1.y, DischargingPump)
    annotation (Line(points={{61,-50},{108,-50},{108,-50}}, color={0,0,127}));
  connect(add.y, gain.u)
    annotation (Line(points={{-59.4,0},{-54.8,0}}, color={0,0,127}));
  connect(switch1.u1, gain.y) annotation (Line(points={{-39.6,43.6},{-44,43.6},
          {-44,0},{-45.6,0}}, color={0,0,127}));
  connect(switch2.u1, gain.y) annotation (Line(points={{-39.6,-43.6},{-44,-43.6},
          {-44,0},{-45.6,0}}, color={0,0,127}));
  connect(greater.u1, PowerSetpoint) annotation (Line(points={{-65.2,-50},{-80,
          -50},{-80,50},{-108,50}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={                               Rectangle(lineColor={102,44,
              145},                                                                                                                           fillColor={102,44,
              145},                                                                                                                                                      fillPattern=
              FillPattern.Solid,                                                                                                                                                                          lineThickness = 1, extent={{-100,
              100},{100,-100}}),                                                                                                                                                                                                        Rectangle(lineColor={102,44,
              145},                                                                                                                                                                                                        fillColor = {234, 234, 234}, fillPattern = FillPattern.Solid, lineThickness = 1, extent={{-96,96},
              {96,-96}}),
        Text(
          extent={{-84,76},{86,-68}},
          textColor={102,44,145},
          fontName="Bahnschrift",
          textString="Power 
controller")}),                                                  Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PowerController;
