# Test cases
Two preliminary test cases, integrating several components of the TANDEM library, are available in the `TestCases` package.

## SMR for electricity production and district heating in the Espoo urban area
In particular, the `NSSS_ThermoPower`, coupled to the `StaticBOP_PlugSGMSL_PlugCogSML`, the BOP version developed in the ThermoSysPro library. In this layout, described in [1], intermediate temperature steam is extracted at the high-pressure turbine stage outlet to power a district heating network composed of the transmission line, the distribution network, and a simplified thermal energy storage system, representing the Espoo urban area in Finland.

## Direct coupling of the thermal energy storage system to a SMR
A second test case, integrating once again the `NSSS_ThermoPower` model, the `StaticBoP3` model in ThermoSysPro, and the `SensibleLoop_HX0D` model, is also available. In this case study, presented in [2], a variable electrical power demand is met by exploiting the TES to store excess power and perform electrical peaking.


## References
[1] Masotti, G. C., Haubensack, D., Ikonen, J.-P., Lindroos, T. J., Lorenzi, S., Pavel, G. L., Ricotti, M. E., “Dynamic Modelling and Optimisation of a Small Modular Reactor for Electricity Production and District Heating in the Helsinki Region”, *International Congress on Advances in Nuclear Power Plants (ICAPP)*, Las Vegas, NV (2024).

[2] Masotti, G. C., Lorenzi, S., Ricotti, M. E., Alpy, N., Nguyen, H. D., Haubensack, D., Simonini, G., “Simulation of flexible Small Modular Reactor operation with a thermal energy storage system”, *International Conference on Small Modular Reactors and their Applications*, IAEA, Vienna (2024).
