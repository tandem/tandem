within TANDEM.TestCases.DHN_directCoupling_Espoo.Components;
model TESControl
  "Controller considering charging/discharging power limits and the TES state of charge"

  parameter Modelica.Units.SI.Energy maxCapacity(displayUnit="kW.h") "Maximal capacity";
    parameter Modelica.Units.SI.Energy minCapacity(displayUnit="kW.h") "Minimal capacity";

  parameter Modelica.Units.SI.Power maxCharPower;
  parameter Modelica.Units.SI.Power maxDischPower;

  Modelica.Blocks.Interfaces.RealInput Tfeed_set annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-166,0}), iconTransformation(
        extent={{0,0},{20,20}},
        rotation=180,
        origin={152,-30})));
  Modelica.Blocks.Interfaces.RealInput SOC annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={62,-108}), iconTransformation(
        extent={{5,5},{25,25}},
        rotation=90,
        origin={75,-117})));
  Modelica.Blocks.Interfaces.RealOutput powerSignal annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-60,-104})));
  Modelica.Blocks.Logical.LessThreshold lessThreshold annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={0,20})));
  Modelica.Blocks.Logical.Switch switch annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-50})));
  Modelica.Blocks.Nonlinear.VariableLimiter variableLimiter annotation (
      Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=270,
        origin={-51,1})));
  Modelica.Blocks.Logical.Switch switch1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-70,50})));
  Modelica.Blocks.Sources.Constant const(k=0)
    annotation (Placement(transformation(extent={{-20,14},{-32,26}})));
  Modelica.Blocks.Sources.Constant const1(k=maxCharPower)
    annotation (Placement(transformation(extent={{-98,64},{-86,76}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold(threshold
      =maxCapacity)
          annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={-56,86})));
  Modelica.Blocks.Sources.Constant const2(k=0)
    annotation (Placement(transformation(extent={{-42,64},{-54,76}})));
  Modelica.Blocks.Nonlinear.VariableLimiter variableLimiter1 annotation (
      Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=270,
        origin={43,1})));
  Modelica.Blocks.Sources.Constant const3(k=0)
    annotation (Placement(transformation(extent={{18,14},{30,26}})));
  Modelica.Blocks.Logical.Switch switch2 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={70,50})));
  Modelica.Blocks.Logical.LessEqualThreshold lessEqualThreshold(threshold=
        minCapacity)                                            annotation (
      Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=270,
        origin={70,76})));
  Modelica.Blocks.Sources.Constant const4(k=0)
    annotation (Placement(transformation(extent={{94,64},{84,74}})));
  Modelica.Blocks.Sources.Constant const5(k=-maxDischPower)
    annotation (Placement(transformation(extent={{44,64},{54,74}})));
  Modelica.Blocks.Interfaces.RealInput Tfeed_mes annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-166,60}), iconTransformation(
        extent={{0,0},{20,20}},
        rotation=180,
        origin={150,50})));
  Modelica.Blocks.Math.Gain gain1(k=120e6) annotation (Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=180,
        origin={-88,112})));
  Modelica.Blocks.Continuous.LimPID PID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=-1*1,
    Ti=1,
    yMax=1,
    yMin=-1) annotation (Placement(transformation(extent={{-124,102},{-104,122}})));
equation
  connect(lessThreshold.y, switch.u2)
    annotation (Line(points={{0,13.4},{0,-38}}, color={255,0,255}));
  connect(const.y, variableLimiter.limit2) annotation (Line(points={{-32.6,20},{
          -43.8,20},{-43.8,11.8}}, color={0,0,127}));
  connect(const1.y, switch1.u3)
    annotation (Line(points={{-85.4,70},{-78,70},{-78,62}}, color={0,0,127}));
  connect(const2.y, switch1.u1)
    annotation (Line(points={{-54.6,70},{-62,70},{-62,62}}, color={0,0,127}));
  connect(SOC, greaterEqualThreshold.u) annotation (Line(points={{62,-108},{62,-80},
          {96,-80},{96,86},{-51.2,86}}, color={0,0,127}));
  connect(greaterEqualThreshold.y, switch1.u2) annotation (Line(points={{-60.4,86},
          {-70,86},{-70,62}}, color={255,0,255}));
  connect(switch1.y, variableLimiter.limit1) annotation (Line(points={{-70,39},{
          -70,20},{-58.2,20},{-58.2,11.8}}, color={0,0,127}));
  connect(variableLimiter.y, switch.u3) annotation (Line(points={{-51,-8.9},{-51,
          -22},{-8,-22},{-8,-38}}, color={0,0,127}));
  connect(switch.y, powerSignal) annotation (Line(points={{0,-61},{0,-72},{-60,-72},
          {-60,-104}}, color={0,0,127}));
  connect(variableLimiter1.y, switch.u1) annotation (Line(points={{43,-8.9},{43,
          -22},{8,-22},{8,-38}}, color={0,0,127}));
  connect(lessEqualThreshold.y, switch2.u2)
    annotation (Line(points={{70,71.6},{70,62}}, color={255,0,255}));
  connect(lessEqualThreshold.u, SOC) annotation (Line(points={{70,80.8},{70,86},
          {96,86},{96,-80},{62,-80},{62,-108}}, color={0,0,127}));
  connect(const3.y, variableLimiter1.limit1) annotation (Line(points={{30.6,20},
          {35.8,20},{35.8,11.8}}, color={0,0,127}));
  connect(switch2.y, variableLimiter1.limit2) annotation (Line(points={{70,39},{
          70,20},{50.2,20},{50.2,11.8}}, color={0,0,127}));
  connect(switch2.u1, const4.y)
    annotation (Line(points={{78,62},{78,69},{83.5,69}}, color={0,0,127}));
  connect(const5.y, switch2.u3)
    annotation (Line(points={{54.5,69},{62,69},{62,62}}, color={0,0,127}));
  connect(PID.y,gain1. u)
    annotation (Line(points={{-103,112},{-92.8,112}},
                                                 color={0,0,127}));
  connect(Tfeed_set, PID.u_s) annotation (Line(points={{-166,0},{-132,0},{-132,
          112},{-126,112}}, color={0,0,127}));
  connect(Tfeed_mes, PID.u_m) annotation (Line(points={{-166,60},{-114,60},{
          -114,100}}, color={0,0,127}));
  connect(gain1.y, lessThreshold.u)
    annotation (Line(points={{-83.6,112},{0,112},{0,27.2}}, color={0,0,127}));
  connect(gain1.y, variableLimiter1.u) annotation (Line(points={{-83.6,112},{0,
          112},{0,40},{43,40},{43,11.8}}, color={0,0,127}));
  connect(gain1.y, variableLimiter.u) annotation (Line(points={{-83.6,112},{0,
          112},{0,40},{-51,40},{-51,11.8}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -100},{140,100}}), graphics={
        Rectangle(
          extent={{-160,100},{140,-100}},
          lineColor={123,62,185},
          lineThickness=1,
          fillColor={123,62,185},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-156,96},{136,-96}},
          lineColor={123,62,185},
          lineThickness=1,
          fillColor={234,234,234},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-44,-28},{56,-84}},
          textColor={0,0,0},
          fontName="Calibri",
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-100},{140,
            100}}),                                  graphics={Text(
          extent={{48,-10},{84,-28}},
          textColor={0,0,0},
          textString="Discharging"), Text(
          extent={{-84,-12},{-56,-26}},
          textColor={0,0,0},
          textString="Charging")}),
    Documentation(info="<html>
<p>This control system translates a power setpoint into the power that is exchanged by the battery, accounting for the maximal charging and discharging power as well as the maximal and minimal state-of-charge (SOC) of the battery.</p>
</html>"));
end TESControl;
