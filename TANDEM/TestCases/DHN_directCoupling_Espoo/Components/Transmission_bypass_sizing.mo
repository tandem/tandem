within TANDEM.TestCases.DHN_directCoupling_Espoo.Components;
model Transmission_bypass_sizing

  parameter Integer Npipe=10 "Number of axial volumes for the pipelines" annotation(Dialog(group="Pipelines"));
  parameter Modelica.Units.SI.Temperature Tg = 10+273.15  "Ground temperature" annotation(Dialog(group="Pipelines"));

  parameter Modelica.Units.SI.Power Q = 135e6;
  parameter Modelica.Units.SI.Pressure pIHX = 7.5e5;
  parameter Modelica.Units.SI.Pressure pDN= 7.5e5;
  parameter Modelica.Units.SI.Temperature Tfeed = 80+273.15;
  parameter Modelica.Units.SI.Temperature Tret= 40+273.15;

  parameter Modelica.Units.SI.Length L=20e3;

  parameter Modelica.Units.SI.PressureDifference dpLIM = L*1e2;

  parameter Real e=0.0001;

  final parameter Integer Nsched = 19;
  final parameter Modelica.Units.SI.Length Do_sched[Nsched] = {0.250, 1.200, 1.400, 0.280, 0.288, 0.372, 0.125, 0.416, 0.500, 0.140, 0.630, 0.160, 0.710, 0.800, 0.180, 0.900, 0.200, 1.000, 1.100};
  final parameter Modelica.Units.SI.Length Di_sched[Nsched] = {0.100, 1.000, 1.200, 0.125, 0.150, 0.200, 0.025, 0.250, 0.300, 0.040, 0.400, 0.050, 0.500, 0.600, 0.065, 0.700, 0.080, 0.800, 0.900};
  final parameter Modelica.Units.SI.ThermalConductance Rloss_vect[Nsched] = {0.243756825, 0.965951243, 1.121210655, 0.272108143, 0.34418559, 0.349336805, 0.149242506, 0.293968659, 0.41938727, 0.181700633, 0.417284532, 0.197516639, 0.52951473, 0.635584116, 0.222451779, 0.717258852, 0.235709303, 0.801598925, 0.881727529}.*L;
  final parameter Modelica.Units.SI.Area A[Nsched] = pi.*(Di_sched/2).^2;

  final parameter Medium.ThermodynamicState hotLeg=Medium.setState_pTX(pIHX, Tfeed);
  final parameter Medium.ThermodynamicState coldLeg=Medium.setState_pTX(pIHX, Tret);
  final parameter Medium.SpecificEnthalpy hHL = Medium.specificEnthalpy(hotLeg);
  final parameter Medium.SpecificEnthalpy hCL = Medium.specificEnthalpy(coldLeg);
  final parameter Medium.Density rhoHL=Medium.density(hotLeg);
  final parameter Medium.Density rhoCL=Medium.density(coldLeg);
  final parameter Medium.DynamicViscosity muHL=Medium.dynamicViscosity(hotLeg);
  final parameter Medium.DynamicViscosity muCL=Medium.dynamicViscosity(coldLeg);

  final parameter Modelica.Units.SI.MassFlowRate mTN = Q/(hHL - hCL);

  final parameter Real fHL_vect[Nsched]= {ThermoPower.Water.f_colebrook(mTN,Di_sched[i]/A[i],e,muHL) for i in 1:Nsched};
  final parameter Modelica.Units.SI.PressureDifference dpHL_vect[Nsched]= {fHL_vect[i]*2*mTN^2*L/Di_sched[i]/A[i]^2/rhoHL for i in 1:Nsched};
  final parameter Modelica.Units.SI.PressureDifference dpHL_check[Nsched]= {if dpHL_vect[i] < dpLIM then dpHL_vect[i] else -Modelica.Constants.inf for i in 1:Nsched};

  final parameter Integer index = Modelica.Math.Vectors.find(max(dpHL_check), dpHL_check);

  final parameter Real fCL = ThermoPower.Water.f_colebrook(mTN,Di/A[index], e,muCL);

  final parameter Modelica.Units.SI.Length Di = Di_sched[index];
  final parameter Modelica.Units.SI.Length Do = Do_sched[index];

  final parameter Modelica.Units.SI.PressureDifference dpHL = dpHL_check[index];
  final parameter Modelica.Units.SI.PressureDifference dpCL = fCL*2*mTN^2*L/Di/A[index]^2/rhoCL;
  final parameter Modelica.Units.SI.ThermalConductance Rloss = Rloss_vect[index];
  final parameter Modelica.Units.SI.Length headHL[3](fixed=false);
  final parameter Modelica.Units.SI.VolumeFlowRate qHL[3](fixed=false);
  final parameter Modelica.Units.SI.Length headCL[3](fixed=false);
  final parameter Modelica.Units.SI.VolumeFlowRate qCL[3](fixed=false);

  ThermoPower.Water.FlangeA flangeA
    annotation (Placement(transformation(extent={{-130,-44},{-110,-24}}),
        iconTransformation(extent={{-130,-44},{-110,-24}})));
  ThermoPower.Water.FlangeB flangeB
    annotation (Placement(transformation(extent={{-130,24},{-110,44}}),
        iconTransformation(extent={{-130,24},{-110,44}})));
  ThermoPower.Water.FlangeB flangeB1
    annotation (Placement(transformation(extent={{110,-44},{130,-24}}),
        iconTransformation(extent={{110,-44},{130,-24}})));
  ThermoPower.Water.FlangeA flangeA1
    annotation (Placement(transformation(extent={{110,24},{130,44}}),
        iconTransformation(extent={{110,24},{130,44}})));
  ThermoPower.Water.Flow1DFV  HotLeg(
    N=Npipe + 1,
    L=L,
    A=pi*(Di/2)^2,
    omega=pi*Di,
    Dhyd=Di,
    wnom=mTN,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Colebrook,
    dpnom=dpHL,
    rhonom(displayUnit="kg/m3"),
    e=e,
    pstart=pIHX,
    hstartin=hHL,
    hstartout=hCL,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantThermalConductance (UA=
            Rloss))
    annotation (Placement(transformation(extent={{-4,-50},{16,-30}})));
  ThermoPower.Water.Flow1DFV  ColdLeg(
    N=Npipe + 1,
    L=L,
    A=pi*(Di/2)^2,
    omega=pi*Di,
    Dhyd=Di,
    wnom=mTN,
    FFtype=ThermoPower.Choices.Flow1D.FFtypes.Colebrook,
    dpnom=dpCL,
    rhonom(displayUnit="kg/m3"),
    e=e,
    pstart=pIHX,
    hstartin=hCL,
    hstartout=hCL,
    fixedMassFlowSimplified=true,
    redeclare model HeatTransfer =
        ThermoPower.Thermal.HeatTransferFV.ConstantThermalConductance (UA=
            Rloss))
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={6,40})));

  ThermoPower.Water.SensT1 sensT1_1
    annotation (Placement(transformation(extent={{72,30},{60,42}})));
  ThermoPower.Water.SensT1 sensT1_2
    annotation (Placement(transformation(extent={{-84,-36},{-74,-26}})));
  ThermoPower.Thermal.TempSource1DFV  tempSource1DFV(Nw=Npipe)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=270,
        origin={24,0})));
  Modelica.Blocks.Sources.RealExpression groundT(y=Tg)
    annotation (Placement(transformation(extent={{54,-8},{38,8}})));
  ThermoPower.Water.SensP sensP
    annotation (Placement(transformation(extent={{-60,36},{-48,48}})));
  ThermoPower.Water.Pump pump(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom=qHL,
          head_nom=headHL),
    rho0(displayUnit="kg/m3") = rhoHL,
    n0=1500,
    hstart=hHL,
    w0=mTN,
    dp0=dpHL,
    use_in_n=false)
    annotation (Placement(transformation(extent={{-54,-46},{-34,-26}})));
  ThermoPower.Water.Pump pump1(
    redeclare function flowCharacteristic =
        ThermoPower.Functions.PumpCharacteristics.quadraticFlow (q_nom=qCL,
          head_nom=headCL),
    rho0(displayUnit="kg/m3") = rhoCL,
    n0=1500,
    hstart=hCL,
    w0=mTN,
    dp0=dpCL,
    use_in_n=false)
    annotation (Placement(transformation(extent={{50,20},{28,42}})));

  ThermoPower.Water.FlowJoin flowJoin1 annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={80,32})));
  ThermoPower.Water.ValveLin valveLin2(Kv=mTN*1e-5)  annotation (Placement(
        transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={98,-42})));
  ThermoPower.Water.FlowSplit flowSplit1 annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={80,-40})));
  ThermoPower.Water.ValveLin valveLin3(Kv=mTN*1e-5)  annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={84,6})));
  ThermoPower.Thermal.MetalTubeFV metalTubeFV(
    Nw=Npipe,
    L=L,
    rint=Di/2,
    rext=Do/2,
    rhomcm=1.82e6,
    lambda=0.417284532,
    WallRes=false)
    annotation (Placement(transformation(extent={{-4,18},{14,36}})));
  ThermoPower.Thermal.MetalTubeFV metalTubeFV1(
    Nw=Npipe,
    L=L,
    rint=Di/2,
    rext=Do/2,
    rhomcm=1.82e6,
    lambda=0.417284532,
    WallRes=false)
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=180,
        origin={5,-23})));
  DistrictHeating.Control.ActuatorBus actuatorBus
    annotation (Placement(transformation(extent={{-70,70},{-50,90}})));
  DistrictHeating.Control.SensorBus sensorBus
    annotation (Placement(transformation(extent={{50,70},{70,90}})));

protected
  constant Real pi=Modelica.Constants.pi;
  constant Modelica.Units.SI.Acceleration g=Modelica.Constants.g_n;
  package Medium = ThermoPower.Water.StandardWater;

initial equation
  headHL={1.46*headHL[2], dpHL/rhoHL/g,0};
  qHL={0,mTN/rhoHL,sqrt(qHL[2]^2*headHL[1]/(headHL[1] - headHL[2]))};
  headCL = {1.46*headCL[2], dpCL/rhoCL/g,0};
  qCL = {0,mTN/rhoCL,sqrt(qCL[2]^2*headCL[1]/(headCL[1] - headCL[2]))};

equation
  connect(flangeB, ColdLeg.outfl)
    annotation (Line(points={{-120,34},{-100,34},{-100,40},{-4,40}},
                                                  color={0,0,255}));
  connect(groundT.y, tempSource1DFV.temperature)
    annotation (Line(points={{37.2,0},{32.6,0},{32.6,-6.66134e-16},{28,
          -6.66134e-16}},                      color={0,0,127}));
  connect(flangeB, sensP.flange)
    annotation (Line(points={{-120,34},{-100,34},{-100,39.6},{-54,39.6}},
                                                  color={0,0,255}));
  connect(pump.outfl, HotLeg.infl) annotation (Line(points={{-38,-29},{-38,-28},
          {-22,-28},{-22,-40},{-4,-40}},
                                     color={0,0,255}));
  connect(pump1.outfl, ColdLeg.infl) annotation (Line(points={{32.4,38.7},{32.4,
          40},{16,40}},           color={0,0,255}));
  connect(flowSplit1.out2,valveLin2. inlet) annotation (Line(points={{83.6,
          -42.4},{92,-42.4},{92,-42}}, color={0,0,255}));
  connect(valveLin3.inlet,flowSplit1. out1) annotation (Line(points={{84,0},{84,
          -37.6},{83.6,-37.6}},    color={0,0,255}));
  connect(HotLeg.outfl, flowSplit1.in1) annotation (Line(points={{16,-40},{76.4,
          -40}},                                       color={0,0,255}));
  connect(valveLin2.outlet, flangeB1) annotation (Line(points={{104,-42},{108,
          -42},{108,-34},{120,-34}}, color={0,0,255}));
  connect(tempSource1DFV.wall, metalTubeFV.ext) annotation (Line(points={{21,0},{
          6,0},{6,2},{5,2},{5,24.21}},      color={255,127,0}));
  connect(metalTubeFV1.ext, metalTubeFV.ext) annotation (Line(points={{5,-20.21},
          {5,0},{6,0},{6,2},{5,2},{5,24.21}},              color={255,127,0}));
  connect(HotLeg.wall, metalTubeFV1.int) annotation (Line(points={{6,-35},{6,
          -30.35},{5,-30.35},{5,-25.7}},   color={255,127,0}));
  connect(ColdLeg.wall, metalTubeFV.int)
    annotation (Line(points={{6,35},{6,29.7},{5,29.7}},    color={255,127,0}));
  connect(flangeB, flangeB)
    annotation (Line(points={{-120,34},{-120,34}}, color={0,0,255}));
  connect(sensT1_2.flange, pump.infl) annotation (Line(points={{-79,-33},{-54,
          -33},{-54,-34},{-52,-34}}, color={0,0,255}));
  connect(sensT1_2.flange, flangeA) annotation (Line(points={{-79,-33},{-120,
          -33},{-120,-34}}, color={0,0,255}));
  connect(flowJoin1.in1, valveLin3.outlet)
    annotation (Line(points={{83.6,29.6},{83.6,12},{84,12}}, color={0,0,255}));
  connect(flowJoin1.in2, flangeA1) annotation (Line(points={{83.6,34.4},{92,
          34.4},{92,34},{120,34}}, color={0,0,255}));
  connect(pump1.infl, sensT1_1.flange) annotation (Line(points={{47.8,33.2},{66,
          33.2},{66,33.6}}, color={0,0,255}));
  connect(sensT1_1.flange, flowJoin1.out)
    annotation (Line(points={{66,33.6},{66,32},{76.4,32}}, color={0,0,255}));
  connect(actuatorBus.Kv_DN, valveLin2.cmd) annotation (Line(
      points={{-60,80},{-60,58},{98,58},{98,-37.2}},
      color={80,200,120},
      thickness=0.5));
  connect(actuatorBus.Kv_bp, valveLin3.cmd) annotation (Line(
      points={{-60,80},{-60,58},{98,58},{98,6},{88.8,6}},
      color={80,200,120},
      thickness=0.5));
  connect(sensorBus.T_feed, sensT1_2.T) annotation (Line(
      points={{60,80},{60,68},{-75,68},{-75,-28}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.Tret, sensT1_1.T) annotation (Line(
      points={{60,80},{60,39.6},{61.2,39.6}},
      color={255,219,88},
      thickness=0.5));
  connect(sensorBus.pIHX, sensP.p) annotation (Line(
      points={{60,80},{60,68},{-46,68},{-46,45.6},{-49.2,45.6}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,-80},
            {120,80}}), graphics={
        Rectangle(
          extent={{-120,80},{120,-80}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-116,76},{116,-76}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-44,30},{48,-28}},
          textColor={244,125,35},
          textString="TN")}),                                    Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-120,-80},{120,80}})));
end Transmission_bypass_sizing;
