within TANDEM.TestCases.DHN_directCoupling_Espoo;
model EspooDHN_FMU
  parameter Modelica.Units.SI.Power Pcog_nom = 135e6;

  inner ThermoPower.System system(allowFlowReversal=true,
                                  initOpt=ThermoPower.Choices.Init.Options.steadyState)   annotation (
    Placement(transformation(extent={{-60,200},{-40,220}})));
  Components.BOP_CEA_modified bop(
    Bache_a(Ce3(Q(start=21.076476996289127)), h(start=487332.0254840841)),
    Bache_b(h(start=487332.95685587695)),
    CsBP1a(start=24083.992164119794),
    CsBP1b(start=1692.3189644945173),
    CsBP2(start=647.274162652389),
    CsHP(start=687735.9296062593),
    Extraction_TurbBP1a_Outlet(Cex(Q(start=8.526512829121202E-14))),
    HeatSink(proe(d(start=990.7810255233652))),
    Pe_SG(C1(h_vol(start=690757.7130269211))),
    Pump_BP(C2(h(start=164184.75305852963), h_vol(start=164184.75305852963)),
        Qv(start=0.18344895901144723)),
    Pump_HP(
      C2(h(start=492431.01656085387), h_vol(start=492431.01656085387)),
      Pm(start=2787393.218157292),
      Qv(start=0.25293197833742753)),
    ReHeat_HP(
      HDesF(start=690757.7130269211),
      HeiF(start=493420.13131620403),
      Se(h_vol(start=690757.7130269211)),
      promeF(d(start=928.6767869690904))),
    Reheat_BP(HDesF(start=370916.7066875473), HeiF(start=170840.96608047135)),
    SG_Secondary_In(h_outflow(start=690757.7130269211)),
    SG_Secondary_Out(h_outflow(start=2944110.0)),
    SPurgeBP(start=182.47701457606448),
    SPurgeHP(start=31.44105545499017),
    ScondesBP(start=1194.2122299506973),
    ScondesHP(start=1581.8735921365774),
    SuperHeat(DPfc(start=39275.264571455)),
    Te_SG(C1(h_vol(start=690757.7130269211))),
    Turb_BP2(
      Ps(start=7055.73076582384),
      pros(d(start=0.05475351719790078)),
      xm(start=0.9365574436684199)),
    Valve_HP(Q(start=218.5655230037108)),
    sensorP1(C1(h_vol(start=370916.7066875473))),
    volumeC1(h(start=163365.51520324877)),
    volumeC2(h(start=2142123.3107644753)),
    sensorT_Turb_LP1a_In(Q(start=182.2)),
    TCOG_ITfroid(Q(start=623.6655)),
    HE_Hybrid_IP(
      Tec(start=441.15),
      Tsc(start=441.15),
      Tef(start=330.15),
      Tsf(start=330.15),
      rhof(displayUnit="kg/m3", start=985.989),
      Qf(start=623.6655),
      Ef(h_vol(start=242778.58922059755))))
    annotation (Placement(transformation(extent={{-94,4},{58,108}})));
  ThermoSysPro.InstrumentationAndControl.Blocks.Math.Gain gain(Gain=1/(2639676
         - 710502.06)) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={72,40})));
  Components.SimpleTES tes(SOCinit=3600000*(0.5*11600000))
    annotation (Placement(transformation(extent={{210,80},{256,40}})));
  Components.Transmission_bypass_sizing tn(Tfeed(displayUnit="K") = 379.9555556,
      Tret(displayUnit="K") = 328.3833333)
    annotation (Placement(transformation(extent={{138,94},{208,140}})));
  DistrictHeating.Components.Distribution dn(
    V=61000,
    pstart=750000,
    hstart=231841.03,
    noInitialPressure=true,
    noInitialEnthalpy=true)
    annotation (Placement(transformation(extent={{366,92},{310,138}})));
  DistrictHeating.Control.DHNcontroller_PID
                            dHNcontroller_PID(PID2(yMin=-0.98, initType=
          Modelica.Blocks.Types.Init.InitialState))
    annotation (Placement(transformation(extent={{196,164},{228,192}})));
  Modelica.Blocks.Sources.Constant const5(k=0)
    annotation (Placement(transformation(extent={{164,196},{176,208}})));
  ThermoPower.Water.Header header1(
    V=1,
    pstart=750000,
    hstart=448303.22)    annotation (Placement(transformation(
        extent={{-7,7},{7,-7}},
        rotation=0,
        origin={233,107})));
  ThermoPower.Water.SensT1 sensT1_1 annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={282,100})));
  Components.TESControl TESctrl(
    maxCapacity=41760000000000,
    minCapacity=0,
    maxCharPower=120e6,
    maxDischPower=120e6)
    annotation (Placement(transformation(extent={{218,28},{246,8}})));
  SMR.NSSS.NSSS_ThermoPower.NSSSsimplified_fluid
                                     nsss(
    core_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dpnom(displayUnit="Pa") = 22280,
    Cfnom=0.0037,
    Primary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp1=200000,
    Cfnom1=0.004,
    Secondary_dp=ThermoPower.Choices.Flow1D.FFtypes.Cfnom,
    dp2=400000,
    Cfnom2=0.11,
    eta=0.9,
    q_nom={0,0.85035195,1.51494276},
    head_nom={52.9277496,36.251883,0},
    hstart_pump=1.33804e6,
    dp0=257900,
    SG(redeclare model Secondary_HT =
          ThermoPower.Thermal.HeatTransferFV.ConstantHeatTransferCoefficient (
            gamma=14500), Secondary(noInitialPressure=false)))
    annotation (Placement(transformation(extent={{-166,48},{-108,84}})));
  Modelica.Blocks.Interfaces.RealInput Pth_COG
    annotation (Placement(transformation(extent={{-246,0},{-206,40}})));
  Modelica.Blocks.Interfaces.RealInput Pth_DEM
    annotation (Placement(transformation(extent={{-248,150},{-208,190}})));
  Modelica.Blocks.Interfaces.RealInput Pth_SUPP
    annotation (Placement(transformation(extent={{-248,200},{-208,240}})));
  SMR.BOP.BOP_TSPro.FMU_Coupling.Adaptor4FMU.AdaptorRealModelicaTSP
    adaptorRealModelicaTSP annotation (Placement(transformation(
        extent={{-7,-6},{7,6}},
        rotation=180,
        origin={91,40})));
  Modelica.Blocks.Interfaces.RealInput T_FEED annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-228,70})));
  Modelica.Blocks.Interfaces.RealInput T_RETURN annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-228,120})));
  Modelica.Blocks.Interfaces.RealOutput Pel_BOP
    annotation (Placement(transformation(extent={{416,168},{436,188}})));
  Modelica.Blocks.Interfaces.RealOutput SOC_TES
    annotation (Placement(transformation(extent={{416,108},{436,128}})));
  Modelica.Blocks.Interfaces.RealOutput Pth_NSSS
    annotation (Placement(transformation(extent={{416,48},{436,68}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=10,
    offset=0.007407407407,
    startTime=5)
    annotation (Placement(transformation(extent={{-116,-52},{-96,-32}})));
  Modelica.Blocks.Math.Product product1
    annotation (Placement(transformation(extent={{-58,-38},{-38,-18}})));
  SMR.NSSS.NSSS_ThermoPower.Control.NSSSctrl_ex1 NSSSctrl(Qprop_max=0, Tavg(PID(
          k=6683, Ti=6683/1600)))
    annotation (Placement(transformation(extent={{-148,104},{-122,124}})));
equation
  Pel_BOP = bop.Pw_eGrid;
  SOC_TES = tes.SOC;
  Pth_NSSS = nsss.core.Power;

  connect(gain.y, bop.inputReal) annotation (Line(points={{67.6,40},{67.6,36.5},
          {57.4154,36.5}},   color={0,0,255}));
  connect(tn.flangeA1, dn.flangeB) annotation (Line(points={{208,126.775},{210,126.5},
          {310,126.5}}, color={0,0,255}));
  connect(dHNcontroller_PID.actuatorBus, tn.actuatorBus) annotation (Line(
      points={{202.4,164},{202,164},{202,152},{155.5,152},{155.5,140}},
      color={80,200,120},
      thickness=0.5));
  connect(dHNcontroller_PID.actuatorBus, dn.actuatorBus) annotation (Line(
      points={{202.4,164},{202.4,152},{352,152},{352,138}},
      color={80,200,120},
      thickness=0.5));
  connect(dHNcontroller_PID.sensorBus, tn.sensorBus) annotation (Line(
      points={{221.6,164},{222,164},{222,148},{190.5,148},{190.5,140}},
      color={255,219,88},
      thickness=0.5));
  connect(dHNcontroller_PID.sensorBus, dn.sensorBus) annotation (Line(
      points={{221.6,164},{221.6,148},{324,148},{324,138}},
      color={255,219,88},
      thickness=0.5));
  connect(const5.y,dHNcontroller_PID. HeatTrade) annotation (Line(points={{176.6,
          202},{182,202},{182,189.24},{196.046,189.24}},  color={0,0,127}));
  connect(TESctrl.powerSignal,tes. signalPower)
    annotation (Line(points={{227.333,28.4},{227.333,32},{219.2,32},{219.2,39.6}},
                                                         color={0,0,127}));
  connect(TESctrl.SOC,tes. SOC)
    annotation (Line(points={{238.533,28.2},{238.533,32},{246.8,32},{246.8,39.2}},
                                                         color={0,0,127}));
  connect(tn.flangeB, bop.Heat_NetWork_Out) annotation (Line(points={{138,
          126.775},{27.6,126.775},{27.6,107.35}},
                                              color={0,0,255}));
  connect(tn.flangeA, bop.Heat_NetWork_In) annotation (Line(points={{138,
          107.225},{66,107.225},{66,118},{38.1231,118},{38.1231,107.35}},
                                                                color={0,0,255}));
  connect(nsss.flangeA, bop.SG_Secondary_In) annotation (Line(points={{-108,
          54.84},{-102,54.84},{-102,58.6},{-88.1538,58.6}},
                                                      color={0,0,255}));
  connect(nsss.flangeB, bop.SG_Secondary_Out) annotation (Line(points={{-108,
          76.8},{-102,76.8},{-102,69.65},{-88.1538,69.65}},
                                                     color={0,0,255}));
  connect(sensT1_1.T, TESctrl.Tfeed_mes) annotation (Line(points={{274,94},{264,
          94},{264,14},{246,14}}, color={0,0,127}));
  connect(tn.flangeB1, header1.inlet) annotation (Line(points={{208,107.225},{212,
          107.225},{212,107},{225.93,107}}, color={0,0,255}));
  connect(header1.thermalPort,tes. ThermalPower)
    annotation (Line(points={{233,103.01},{233,79.2}}, color={191,0,0}));
  connect(adaptorRealModelicaTSP.outputReal, gain.u)
    annotation (Line(points={{83.3,40},{76.4,40}}, color={0,0,255}));
  connect(Pth_SUPP, dHNcontroller_PID.HeatSupply) annotation (Line(points={{-228,
          220},{-184,220},{-184,183.04},{196,183.04}},      color={0,0,127}));
  connect(Pth_DEM, dHNcontroller_PID.HeatDemand) annotation (Line(points={{-228,
          170},{-190,170},{-190,176.6},{196,176.6}}, color={0,0,127}));
  connect(T_RETURN, dHNcontroller_PID.Tset_ret) annotation (Line(points={{-228,
          120},{-156,120},{-156,169.36},{196.046,169.36}},
                                              color={0,0,127}));
  connect(T_FEED, TESctrl.Tfeed_set) annotation (Line(points={{-228,70},{-172,
          70},{-172,4},{278,4},{278,22},{246.187,22}},
                                                 color={0,0,127}));
  connect(header1.outlet, dn.flangeA) annotation (Line(points={{240,107},{244,
          107},{244,108},{250,108},{250,103.5},{309.533,103.5}},
                                                            color={0,0,255}));
  connect(header1.outlet, sensT1_1.flange) annotation (Line(points={{240,107},{244,
          107},{244,108},{250,108},{250,103.5},{282,104}}, color={0,0,255}));
  connect(Pth_COG, product1.u1) annotation (Line(points={{-226,20},{-96,20},{
          -96,-22},{-60,-22}}, color={0,0,127}));
  connect(ramp.y, product1.u2) annotation (Line(points={{-95,-42},{-80,-42},{
          -80,-34},{-60,-34}}, color={0,0,127}));
  connect(product1.y, adaptorRealModelicaTSP.u) annotation (Line(points={{-37,
          -28},{118,-28},{118,40},{99.4,40}}, color={0,0,127}));
  connect(NSSSctrl.actuatorBus, nsss.actuatorBus) annotation (Line(
      points={{-142.8,104},{-144,104},{-144,96},{-151.5,96},{-151.5,83.64}},
      color={80,200,120},
      thickness=0.5));
  connect(NSSSctrl.sensorBus, nsss.sensorBus) annotation (Line(
      points={{-127.2,104},{-128,104},{-128,98},{-122.5,98},{-122.5,83.64}},
      color={255,219,88},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-220,0},
            {420,240}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-220,0},{420,240}})));
end EspooDHN_FMU;
