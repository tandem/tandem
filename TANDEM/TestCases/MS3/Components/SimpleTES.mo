within TANDEM.TestCases.MS3.Components;
model SimpleTES
  parameter Modelica.Units.SI.Energy SOCinit(displayUnit="kW.h") "Initial state of charge";

  Modelica.Blocks.Interfaces.RealInput signalPower annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={-60,106}), iconTransformation(
        extent={{-3.33345,-3.33336},{16.6671,16.6666}},
        rotation=270,
        origin={-66.6666,108.667})));
  Modelica.Blocks.Interfaces.RealOutput SOC annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={60,104})));
  Modelica.Blocks.Continuous.Integrator integrator(y_start=SOCinit)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a ThermalPower annotation (
      Placement(transformation(extent={{-22,-122},{24,-76}}),iconTransformation(
          extent={{-10,-106},{10,-86}})));
equation
    ThermalPower.Q_flow = signalPower;

  connect(signalPower, integrator.u)
    annotation (Line(points={{-60,106},{-60,0},{-12,0}}, color={0,0,127}));
  connect(integrator.y, SOC)
    annotation (Line(points={{11,0},{60,0},{60,104}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={85,170,255},
          lineThickness=1,
          fillColor={85,170,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={85,170,255},
          lineThickness=1,
          fillColor={234,234,234},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-46,28},{46,-30}},
          textColor={85,170,255},
          textString="TES")}),                                   Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(revisions="<html>
</html>", info="<html>
<p>The battery model converts an input power signal to the <i>ThermoPower.Electrical.PowerConnection</i> connector, which can be used to couple the battery to other components. The state-of-charge (<i>SOC</i>) is obtained by integrating the power signal over time. </p>
<p>The operating limits of the battery, in terms of maximal and minimal charging and discharging power, as well as capacity limits, are accounted for in a dedicated control system. </p>
</html>"));
end SimpleTES;
