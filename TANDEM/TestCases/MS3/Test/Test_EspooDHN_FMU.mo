within TANDEM.TestCases.MS3.Test;
model Test_EspooDHN_FMU
  EspooDHN_FMU espooDHN_FMU(TESctrl(
      maxCharPower=135e6,
      maxDischPower=135e6,
      gain1(k=135e6)))
    annotation (Placement(transformation(extent={{6,2},{70,26}})));
  Modelica.Blocks.Sources.TimeTable demand(
    table=[0,629900000; 1,629100000; 2,622000000; 3,628400000; 4,646300000; 5,
        689200000; 6,739300000; 7,752600000; 8,787500000; 9,790350156.4; 10,
        761000000; 11,750800000; 12,709980156.4; 13,709980156.4; 14,652870012.6;
        15,695300000; 16,709980812.2; 17,709980812.2; 18,697800000; 19,
        680300000; 20,504600000; 21,523222811; 22,497779978.1; 23,495593817; 24,
        495593161.1],
    timeScale(displayUnit="h") = 3600,
    offset=0)
    annotation (Placement(transformation(extent={{-44,36},{-34,46}})));
  Modelica.Blocks.Sources.TimeTable supply(
    table=[0,494900000; 1,494100000; 2,487000000; 3,493400000; 4,511300000; 5,
        554200000; 6,604300000; 7,617600000; 8,652500000; 9,655350156.4; 10,
        626000000; 11,615800000; 12,574980156.4; 13,574980156.4; 14,517870012.6;
        15,560300000; 16,574980812.2; 17,574980812.2; 18,562800000; 19,
        545300000; 20,369600000; 21,388222811; 22,362779978.1; 23,360593817; 24,
        360593161.1],
    timeScale(displayUnit="h") = 3600,
    offset=2e6)
    annotation (Placement(transformation(extent={{-44,58},{-34,68}})));
  Modelica.Blocks.Sources.TimeTable Tfeed(
    table=[0,379.9555556; 1,380.2055556; 2,380.3642857; 3,380.6261905; 4,
        380.8246032; 5,380.9436508; 6,380.5428571; 7,379.8365079; 8,379.0190476;
        9,378.0111111; 10,376.2849206; 11,374.0746032; 12,372.7452381; 13,
        372.3484127; 14,372.5150794; 15,372.3722222; 16,372.65; 17,372.6222222;
        18,370.8166667; 19,367.4833333; 20,364.5269841; 21,363.4277778; 22,
        363.3484127; 23,362.99; 24,362.48],
    timeScale(displayUnit="h") = 3600,
    offset=0)
    annotation (Placement(transformation(extent={{-44,-12},{-34,-2}})));
  Modelica.Blocks.Sources.TimeTable Tret(
    table=[0,328.3833333; 1,328.3833333; 2,328.4785714; 3,328.6357143; 4,
        328.7547619; 5,328.8261905; 6,328.5857143; 7,328.1619048; 8,327.6714286;
        9,327.0666667; 10,326.0309524; 11,324.7047619; 12,323.9071429; 13,
        323.6690476; 14,323.7690476; 15,323.6833333; 16,323.85; 17,323.8333333;
        18,322.75; 19,320.75; 20,318.9761905; 21,318.3166667; 22,318.2690476;
        23,318.07; 24,317.815],
    timeScale(displayUnit="h") = 3600,
    offset=0)
    annotation (Placement(transformation(extent={{-44,16},{-34,26}})));
  Modelica.Blocks.Sources.Constant const(k=135e6)
    annotation (Placement(transformation(extent={{-88,-68},{-68,-48}})));
equation
  connect(supply.y, espooDHN_FMU.Pth_SUPP) annotation (Line(points={{-33.5,63},
          {-10,63},{-10,24},{5.2,24}}, color={0,0,127}));
  connect(demand.y, espooDHN_FMU.Pth_DEM) annotation (Line(points={{-33.5,41},{
          -12,41},{-12,19},{5.2,19}}, color={0,0,127}));
  connect(Tret.y, espooDHN_FMU.T_RETURN) annotation (Line(points={{-33.5,21},{
          -14,21},{-14,14},{5.2,14}}, color={0,0,127}));
  connect(Tfeed.y, espooDHN_FMU.T_FEED) annotation (Line(points={{-33.5,-7},{
          -33.5,-8},{-4,-8},{-4,9},{5.2,9}}, color={0,0,127}));
  connect(const.y, espooDHN_FMU.Pth_COG) annotation (Line(points={{-67,-58},{
          -44,-58},{-44,-62},{-2,-62},{-2,4},{5.4,4}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=86400, __Dymola_Algorithm="Dassl"),
    __Dymola_experimentFlags(
      Advanced(
        EvaluateAlsoTop=false,
        GenerateAnalyticJacobian=false,
        GenerateVariableDependencies=false,
        OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=false,
      OutputFlatModelica=false));
end Test_EspooDHN_FMU;
