within TANDEM.PowerSources;
model HeatPump
  parameter Real COP = 4 "Coefficient of performance";

  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{-8,92},{12,112}}),
        iconTransformation(extent={{-8,92},{12,112}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b ThermalPower annotation (Placement(transformation(
          extent={{-116,-16},{-86,14}}),
                                       iconTransformation(extent={{-116,-16},{
            -86,14}})));

  Modelica.Blocks.Interfaces.RealInput Pth annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={2,-106}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={2,-106})));
equation
  Pth = -ThermalPower.Q_flow;
  powerConnection.P = Pth/COP;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={162,29,33},
          fillColor={162,29,33},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={162,29,33},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-70,66},{78,-60}},
          textColor={162,29,33},
          textString="Heat pump")}),                             Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HeatPump;
