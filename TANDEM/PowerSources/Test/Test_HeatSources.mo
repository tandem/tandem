within TANDEM.PowerSources.Test;
model Test_HeatSources
  PowerSources.SimpleCHP simpleCHP
    annotation (Placement(transformation(extent={{-16,0},{16,30}})));
  ThermoPower.Electrical.Grid grid(Pgrid=1e6)
    annotation (Placement(transformation(extent={{60,44},{80,64}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature(T=
        353.15) annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=180,
        origin={65,7})));
  Modelica.Blocks.Sources.Constant const(k=2e6)
    annotation (Placement(transformation(extent={{-32,62},{-12,82}})));
  Modelica.Blocks.Sources.Constant const1(k=1e6)
    annotation (Placement(transformation(extent={{-62,40},{-42,60}})));
  inner ThermoPower.System system(initOpt=ThermoPower.Choices.Init.Options.steadyState)
    annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
  PowerSources.HeatPump heatPump
    annotation (Placement(transformation(extent={{-14,-62},{14,-36}})));
  ThermoPower.Electrical.Grid grid1(Pgrid=1e6)
    annotation (Placement(transformation(extent={{40,-34},{60,-14}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature1(T=
        353.15) annotation (Placement(transformation(
        extent={{-7,-7},{7,7}},
        rotation=0,
        origin={-59,-49})));
  Modelica.Blocks.Sources.Constant const2(k=1e6)
    annotation (Placement(transformation(extent={{-46,-90},{-26,-70}})));
equation
  connect(simpleCHP.powerConnection, grid.port) annotation (Line(
      points={{16,22.5},{40,22.5},{40,54},{61.4,54}},
      color={0,0,255},
      thickness=0.5));
  connect(simpleCHP.ThermalPower, fixedTemperature.port) annotation (Line(
        points={{16,7.5},{22,7.5},{22,7},{58,7}}, color={191,0,0}));
  connect(const.y, simpleCHP.Pel_set)
    annotation (Line(points={{-11,72},{9.6,72},{9.6,31.2}}, color={0,0,127}));
  connect(const1.y, simpleCHP.Pth_set) annotation (Line(points={{-41,50},{-9.6,
          50},{-9.6,31.2}}, color={0,0,127}));
  connect(heatPump.powerConnection, grid1.port) annotation (Line(
      points={{0.28,-35.74},{0.28,-24},{41.4,-24}},
      color={0,0,255},
      thickness=0.5));
  connect(fixedTemperature1.port, heatPump.ThermalPower) annotation (Line(
        points={{-52,-49},{-48,-49},{-48,-48},{-28,-48},{-28,-49.13},{-14.14,
          -49.13}}, color={191,0,0}));
  connect(const2.y, heatPump.Pth) annotation (Line(points={{-25,-80},{0.28,-80},
          {0.28,-62.78}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Test_HeatSources;
