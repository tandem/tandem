within TANDEM.PowerSources;
model SimpleCHP
  Modelica.Blocks.Interfaces.RealInput Pth_set annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={-60,108})));
  Modelica.Blocks.Interfaces.RealInput Pel_set annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=270,
        origin={60,108})));
  ThermoPower.Electrical.PowerConnection powerConnection
    annotation (Placement(transformation(extent={{90,40},{110,60}}),
        iconTransformation(extent={{90,40},{110,60}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b ThermalPower annotation (
      Placement(transformation(extent={{90,-60},{110,-40}}), iconTransformation(
          extent={{90,-60},{110,-40}})));

equation
  powerConnection.P = -Pel_set;
  ThermalPower.Q_flow = -Pth_set;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={162,29,33},
          fillColor={162,29,33},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-96,96},{96,-96}},
          lineColor={162,29,33},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-74,68},{74,-58}},
          textColor={162,29,33},
          textString="CHP")}),                                   Diagram(
        coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{76,82},{126,54}},
          textColor={238,46,47},
          textString="ElectricalPower")}));
end SimpleCHP;
