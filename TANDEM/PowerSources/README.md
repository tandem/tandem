# Additional power sources
The package provides the simplified models for the additional power sources, such as the conventional combined heat and power plants (CHP) and heat pumps. The models are based on components from the [ThermoPower](https://github.com/casella/ThermoPower) library.



## Components description
### Combined heat and power plant
The combined heat and power plant (CHP) `SimpleCHP` model is treated as a black box component, assigning the values of two input signals for the thermal and electrical power output to the [HeatPort](https://doc.modelica.org/om/Modelica.Thermal.HeatTransfer.Interfaces.HeatPort.html) connector of the Modelica Standard Library and to the [PowerConnection](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.PowerConnection.html) of ThermoPower.


### Heat pump
Similarly, the `HeatPump` model requires a signal for the thermal power output, which is assigned to the [HeatPort](https://doc.modelica.org/om/Modelica.Thermal.HeatTransfer.Interfaces.HeatPort.html). The required electrical power, exchanged through [PowerConnection](https://build.openmodelica.org/Documentation/ThermoPower.Electrical.PowerConnection.html), to achieve the required thermal power output is computed through the definition of the coefficient of performance that characterises the heat pump:
$$
COP = \frac{P_{th}}{P_{el}}
$$



## Test cases
In the `Test_HeatSources` model, the simplified heat sources available in the `PowerSources` package, namely the `SimpleCHP` and the `HeatPump` classes, have been tested. The goal is to showcase the required connections, such as those to a heat sink and to the electrical grid. In this test case, constant values are assigned to the power signals.
