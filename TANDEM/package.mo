within ;
package TANDEM "SMR powered hybrid system"
  annotation (Icon(graphics={  Bitmap(extent = {{-100, -100}, {100, 100}},
            fileName="modelica://TANDEM/../resources/TANDEM_LOGO_FINAL_V1.png")}),
    uses(Modelica(version = "4.0.0"), Buildings(version = "11.0.0"), ThermoPower_TBL(version = "1.0"),
      ThermoSysPro_H2(version="4.0"),
      ThermoPower(version="3.2")));
end TANDEM;
