within TANDEM.Renewables.Test;
model Wind_RealConnection
  extends WindPowerPlants.Examples.GenericPlantRealData;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Wind_RealConnection;
