within TANDEM.Renewables.Test;
model Test_GridConnection
  Modelica.Blocks.Continuous.Integrator energyIntegrator(k=1, y(unit="J"))   annotation(Placement(transformation(origin={40,62},    extent = {{-10, -10}, {10, 10}})));
  WindPowerPlants.Plants.GenericVariableSpeed plant(limitMot=0.01, k=120*112.8)       annotation(Placement(transformation(extent={{-10,32},
            {10,52}})));
  WindPowerPlants.WindSources.RealData
                       windSource(fileName=
        Modelica.Utilities.Files.loadResource(
        "modelica://WindPowerPlants/Resources/Data/beresford2006.txt"))
                                                       annotation(Placement(transformation(extent={{-70,32},
            {-50,52}})));
  WindPowerPlants.Blocks.SpeedAdaptor speedadaptor1(
    hin=50,
    hout=105,
    roughness=0.1)                                                                         annotation(Placement(transformation(origin={-30,42},   extent = {{-10, -10}, {10, 10}})));
  WindPowerPlants.Blocks.LogTerminal logTerminal(
    preString="Total energy = ",
    postString=" kWh",
    gain=1/3.6E6)                                                                                                      annotation(Placement(transformation(extent={{60,52},
            {80,72}})));
  ElectricalGrid.Models.grid_node node_2(
    line_length(displayUnit="km") = 20000,
    load_voltage_out(displayUnit="kV") = 50000,
    plant_voltage_in(displayUnit="kV") = 2000)                                                                                                                               annotation (
    Placement(visible = true, transformation(origin={26,-18},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R=450)   annotation (
    Placement(visible = true, transformation(origin={58,-18},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixed_grid_voltage(V=380000,
      f=50)                                                                                    annotation (
    Placement(visible = true, transformation(origin={-70,-18},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node node_0(
    line_length(displayUnit="km") = 20000,
    load_voltage_out(displayUnit="kV") = 50000,
    plant_voltage_in=2000)                                                                                                                               annotation (
    Placement(visible = true, transformation(origin={-20,-18},  extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance domestic_load(L=0, R=3)     annotation (
    Placement(visible = true, transformation(origin={-20,-46},    extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  Tools.Electrical_Adaptor.Real2BLD real2BLD annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={26,16})));
  Desalination.ReverseOsmosis DESALIN
    annotation (Placement(transformation(extent={{66,-74},{86,-54}})));
  Modelica.Blocks.Sources.Constant const(k=10e6)
    annotation (Placement(transformation(extent={{58,-46},{70,-34}})));
  Tools.Electrical_Adaptor.TP2BLD tP2BLD annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={26,-44})));
  Modelica.Blocks.Math.Gain gain(k=-1) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=270,
        origin={26,38})));
equation
  connect(plant.power,energyIntegrator. u) annotation(Line(points={{6,53},{6,62},
          {28,62}},                                                                                 color = {0, 0, 127}));
  connect(energyIntegrator.y,logTerminal. u) annotation(Line(points={{51,62},{
          58,62}},                                                                                    color = {0, 0, 127}));
  connect(windSource.v,speedadaptor1. vin) annotation(Line(points={{-49,42},{
          -42,42}},                                                                                   color = {0, 0, 127}));
  connect(speedadaptor1.vout,plant. v) annotation(Line(points={{-19,42},{-12,42}},                                    color = {0, 0, 127}));
  connect(node_2.node_out,imp. terminal) annotation (
    Line(points={{33,-18},{48,-18}}));
  connect(fixed_grid_voltage.terminal,node_0. node_in) annotation (
    Line(points={{-60,-18},{-27,-18}}));
  connect(domestic_load.terminal,node_0. load_conn) annotation (
    Line(points={{-20,-36},{-20,-24}}));
  connect(node_2.node_in,node_0. node_out) annotation (
    Line(points={{19,-18},{-13,-18}}));
  connect(real2BLD.term_n, node_2.power_plant_conn)
    annotation (Line(points={{26,5.6},{26,-12}}, color={0,120,120}));
  connect(const.y, DESALIN.powerSignal)
    annotation (Line(points={{70.6,-40},{76,-40},{76,-53}}, color={0,0,127}));
  connect(node_2.load_conn, tP2BLD.term_n)
    annotation (Line(points={{26,-24},{26,-33.6}}, color={0,120,120}));
  connect(tP2BLD.powerConnection, DESALIN.powerConnection) annotation (Line(
      points={{25.9,-53.9},{25.9,-64},{66,-64}},
      color={0,0,255},
      thickness=0.5));
  connect(real2BLD.P, gain.y)
    annotation (Line(points={{26,26.8},{26,31.4}}, color={0,0,127}));
  connect(gain.u, plant.power) annotation (Line(points={{26,45.2},{26,52},{6,52},
          {6,53}}, color={0,0,127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=2678400, __Dymola_Algorithm="Dassl"));
end Test_GridConnection;
