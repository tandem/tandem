within TANDEM.Renewables.Test;
model Wind_ElectricalConnection
  extends WindPowerPlants.Examples.GenericPlantElectricRealData;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Wind_ElectricalConnection;
