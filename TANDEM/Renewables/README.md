# Renewable energy sources

The dynamic model of the wind turbines will be used to compute the electric power output provided by this renewable energy source considering fluctuating environmental conditions, e.g., in terms of wind speed. Only the dynamic model of wind turbines will be provided in the TANDEM library, whereas the contribution of other renewable sources, such as solar PV, will be accounted for directly in the electrical load profile.

The open source library [WindPowerPlants](https://github.com/christiankral/WindPowerPlants) has been exploited to account for the dynamic behaviour of this power source.