﻿within TANDEM.ElectricalGrid;
package Models

  model grid_node
    final parameter Modelica.Units.SI.Resistance line_R = (line_length/1000)*0.0233;
    parameter Modelica.Units.SI.Length line_length;
    final parameter Modelica.Units.SI.Inductance line_L = (line_length/1000)*8.79e-4;
    final parameter Modelica.Units.SI.Capacitance line_C =0;
    parameter Modelica.Units.SI.Voltage plant_voltage_in;
    parameter Modelica.Units.SI.Voltage load_voltage_out;
    //first item is voltage [kV] and second is X/R ratio [/]
    parameter Real XoR(fixed = false);
    parameter Modelica.Units.SI.ApparentPower apower_transfo(fixed=false);
    parameter Real Zp(fixed=false);
    //compensation device parameters
    parameter Modelica.Units.SI.Capacitance C_compensation=0.000001;
    parameter Modelica.Units.SI.Inductance L_compensation=0;
    Buildings.Electrical.AC.OnePhase.Conversion.ACACTransformer transformer_load(VABase = apower_transfo, VHigh( displayUnit = "kV")= 380000, VLow( displayUnit = "kV")= load_voltage_out, XoverR = XoR, Zperc = Zp)  annotation (
      Placement(visible = true, transformation(origin = {34, -32}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p load_conn annotation (
      Placement(visible = true, transformation(origin = {66, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Buildings.Electrical.AC.OnePhase.Conversion.ACACTransformer transformer_power_plant(VABase = apower_transfo, VHigh( displayUnit = "kV")= 380000, VLow( displayUnit = "kV")= plant_voltage_in, XoverR = XoR, Zperc = Zp) annotation (
      Placement(visible = true, transformation(origin = {-14, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n power_plant_conn annotation (
      Placement(visible = true, transformation(origin = {-14, 84}, extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin = {0, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    //separate device for capacitive and inductive mandatory
    Buildings.Electrical.AC.OnePhase.Loads.Impedance compensation_device(C( displayUnit = "F")= C_compensation, L=0, R = 0, inductive = false) annotation (
      Placement(visible = true, transformation(origin = {-14, -44}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p node_out annotation (
      Placement(visible = true, transformation(origin = {66, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n node_in annotation (
      Placement(visible = true, transformation(origin = {-90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Lines.TwoPortRLC lineRLC(L=line_L, R=line_R, C=line_C) annotation (
      Placement(visible = true, transformation(origin = {-50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  //separate objects for inductive compensation which exists only if L > 0 (otherwise would create short circuit to the ground)
  Buildings.Electrical.AC.OnePhase.Loads.Impedance compensation_device_inductive( L=L_compensation, R = 0, inductive = true) if L_compensation > 0;

  initial equation
      (XoR, apower_transfo, Zp) = getTransfoParams(voltage_in=380e3);

  equation
    if L_compensation > 0 then
      connect(compensation_device_inductive.terminal, lineRLC.terminal_p);
    end if;
    connect(node_in, lineRLC.terminal_n) annotation (
      Line(points = {{-90, 0}, {-56, 0}}, color = {0, 120, 120}));
    connect(transformer_load.terminal_n, lineRLC.terminal_p) annotation (
      Line(points = {{24, -32}, {4, -32}, {4, 0}, {-36, 0}}));
    connect(transformer_load.terminal_p, load_conn) annotation (
      Line(points = {{44, -32}, {66, -32}}));
    connect(transformer_power_plant.terminal_p, power_plant_conn) annotation (
      Line(points = {{-14, 48}, {-14, 84}}));
    connect(transformer_power_plant.terminal_n, lineRLC.terminal_p) annotation (
      Line(points = {{-14, 28}, {-14, 0}, {-36, 0}}));
    connect(compensation_device.terminal, lineRLC.terminal_p) annotation (
      Line(points = {{-14, -34}, {-14, 0}, {-36, 0}}));
    connect(node_out, lineRLC.terminal_p) annotation (
      Line(points = {{66, 0}, {-36, 0}}));
    annotation (
      Diagram(graphics = {Rectangle(extent = {{72, -2}, {72, -2}})}),
      uses(Buildings(version = "7.0.0"), ThermoPower(version = "3.1"), Modelica(version = "3.2.3")),
     Icon(graphics = {Rectangle(extent = {{-70, -60}, {70, 60}})}));
  end grid_node;

  model main_grid

    parameter Integer number_of_nodes=5 annotation(Evaluate = true);
    parameter Modelica.Units.SI.Length array_lines_length;
    parameter Modelica.Units.SI.Voltage array_plant_voltage_in;
    parameter Modelica.Units.SI.Voltage array_load_voltage_out;
    parameter Modelica.Units.SI.Inductance array_load_inductance;
    parameter Modelica.Units.SI.Resistance array_load_resistance;
    grid_node node_matrix[number_of_nodes](each final line_length= array_lines_length,
                                           each final plant_voltage_in=array_plant_voltage_in,
                                           each final load_voltage_out=array_load_voltage_out);
    Buildings.Electrical.AC.OnePhase.Loads.Impedance domestic_loads[number_of_nodes](each final L = array_load_inductance,
                                                                                     each final R = array_load_resistance);
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n array_in annotation (
      Placement(visible = true, transformation(origin = {-90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p array_out annotation (
      Placement(visible = true, transformation(origin = {88, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    for i in 1:number_of_nodes loop
      if i == 1 then
        connect(array_in, node_matrix[i].node_in);
      else
        connect(node_matrix[i-1].node_out, node_matrix[i].node_in);
        if i == number_of_nodes then
          connect(node_matrix[i].node_out, array_out);
        end if;
      end if;
      connect(node_matrix[i].load_conn, domestic_loads[i].terminal);
    end for;
    //Buildings.Electrical.AC.OnePhase.Loads.Impedance
  end main_grid;

  function getTransfoParams
  input Modelica.Units.SI.Voltage voltage_in;
  output  Real XoverR;
  output Modelica.Units.SI.ApparentPower apower;
  output Real Zper;
  // 1st is voltage [V], 2nd is XoverR ratio, 3rd is apparent power [VA]
  protected
  Real transfoPower_XR[7,3]={{69e3,20,42e6}, {115e3,25,53e6}, {138e3,30,83e6}, {161e3,32,100e6}, {230e3,44,203e6}, {345e3,60,444e6}, {500e3,70,812e6}};

  algorithm
    for i in 1:6 loop
      if (voltage_in >= transfoPower_XR[i,1] and voltage_in < transfoPower_XR[i+1, 1]) then
        //rule of 3 on XoverR because interpolation's R² is too large
        //XoverR := 0.0001*voltage_in+13.029;
          XoverR := transfoPower_XR[i,2] +  (voltage_in - transfoPower_XR[i,1]) / (transfoPower_XR[i+1,1] - transfoPower_XR[i,1]) * (transfoPower_XR[i+1,2] - transfoPower_XR[i,2]);
        //equation comes from interpolation of points in table hereabove
          apower := 0.9974*voltage_in^1.628;
        //apower :=  transfoPower_XR[i,3] +  (voltage_in - transfoPower_XR[i,1]) / (transfoPower_XR[i+1,1] - transfoPower_XR[i,1]) * (transfoPower_XR[i+1,3] - transfoPower_XR[i,3]);
      end if;
    end for;
    if voltage_in < transfoPower_XR[1,1] then
      XoverR := transfoPower_XR[1,2];
      apower := transfoPower_XR[1,3];
    elseif voltage_in >=  transfoPower_XR[7,1] then
     XoverR := transfoPower_XR[7,2];
     apower := transfoPower_XR[7,3];
    end if;
  Zper := 0.0112* (apower/1000)^0.2059;
  end getTransfoParams;
end Models;
