within TANDEM.ElectricalGrid.Tests;
model UT_node_behavior
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage E(V = 380000, definiteReference = true, f = 50)  annotation (
    Placement(visible = true, transformation(origin = {-60, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R = 300)  annotation (
    Placement(visible = true, transformation(origin = {32, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node basic_node(C_compensation = 0.000001, L_compensation = 0.001,line_length(displayUnit = "km") = 2000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000)  annotation (
    Placement(visible = true, transformation(origin = {-14, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(E.terminal, basic_node.node_in) annotation (
    Line(points = {{-50, -6}, {-20, -6}}));
  connect(imp.terminal, basic_node.node_out) annotation (
    Line(points = {{22, -6}, {-6, -6}}));
  annotation (
    uses(Buildings(version = "7.0.0")));
end UT_node_behavior;
