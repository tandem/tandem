within TANDEM.ElectricalGrid.Tests;
model UT_sizable_grid
  ElectricalGrid.Models.main_grid node_array_1(array_lines_length(displayUnit = "km") = 10000, array_load_inductance(displayUnit = "mH") = 0.01, array_load_resistance = 100, array_load_voltage_out(displayUnit = "kV") = 2000, array_plant_voltage_in(displayUnit = "kV") = 380000, number_of_nodes = 8) annotation (
    Placement(visible = true, transformation(origin = {-14, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixed_grid_voltage(V = 380000, f = 50) annotation (
    Placement(visible = true, transformation(origin = {-60, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R = 150) annotation (
    Placement(visible = true, transformation(origin = {26, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.main_grid node_array_2(array_lines_length(displayUnit = "km") = 7000, array_load_inductance(displayUnit = "mH") = 0.01, array_load_resistance = 100, array_load_voltage_out(displayUnit = "kV") = 2000, array_plant_voltage_in(displayUnit = "kV") = 380000, number_of_nodes = 2) annotation (
    Placement(visible = true, transformation(origin = {-14, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance impedance(L = 0.1, R = 900) annotation (
    Placement(visible = true, transformation(origin = {24, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node grid_node5(C_compensation = 0.000001,line_length(displayUnit = "km") = 5000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000) annotation (
    Placement(visible = true, transformation(origin = {18, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage power_plant1(V = 2000, f = 50, potentialReference = false) annotation (
    Placement(visible = true, transformation(origin = {44, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance domestic_load(L = 2, R = 850) annotation (
    Placement(visible = true, transformation(origin = {18, -62}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
  ElectricalGrid.Models.grid_node grid_node3(C_compensation = 0.000001, L_compensation = 0.001,line_length(displayUnit = "km") = 25000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000) annotation (
    Placement(visible = true, transformation(origin = {48, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance impedance1(R = 300) annotation (
    Placement(visible = true, transformation(origin = {74, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node grid_node26(C_compensation = 0.000001,line_length(displayUnit = "km") = 14000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000) annotation (
    Placement(visible = true, transformation(origin = {48, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance impedance2(R = 240) annotation (
    Placement(visible = true, transformation(origin = {108, -84}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node grid_node7(C_compensation = 0.000001,line_length(displayUnit = "km") = 86000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000) annotation (
    Placement(visible = true, transformation(origin = {74, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(fixed_grid_voltage.terminal, node_array_1.array_in) annotation (
    Line(points = {{-50, 14}, {-24, 14}}));
  connect(node_array_1.array_out, imp.terminal) annotation (
    Line(points = {{-4, 14}, {16, 14}}));
  connect(fixed_grid_voltage.terminal, node_array_2.array_in) annotation (
    Line(points = {{-24, 50}, {-40, 50}, {-40, 14}, {-50, 14}}));
  connect(impedance.terminal, node_array_2.array_out) annotation (
    Line(points = {{-4, 50}, {14, 50}}));
  connect(node_array_1.array_out, grid_node5.node_in) annotation (
    Line(points = {{12, -30}, {2, -30}, {2, 14}, {-4, 14}}));
  connect(grid_node5.power_plant_conn, power_plant1.terminal) annotation (
    Line(points = {{18, -24}, {18, -6}, {34, -6}}));
  connect(grid_node5.load_conn, domestic_load.terminal) annotation (
    Line(points = {{18, -36}, {18, -52}}));
  connect(grid_node5.node_out, grid_node3.node_in) annotation (
    Line(points = {{26, -30}, {42, -30}}));
  connect(impedance1.terminal, grid_node3.node_out) annotation (
    Line(points = {{56, -30}, {64, -30}}));
  connect(grid_node5.node_out, grid_node26.node_in) annotation (
    Line(points = {{42, -64}, {30, -64}, {30, -30}, {26, -30}}));
  connect(grid_node26.node_out, grid_node7.node_in) annotation (
    Line(points = {{68, -64}, {56, -64}}));
  connect(grid_node7.node_out, impedance2.terminal) annotation (
    Line(points = {{82, -64}, {98, -64}, {98, -84}}));
  annotation (
    uses(Buildings(version = "7.0.0")));
end UT_sizable_grid;
