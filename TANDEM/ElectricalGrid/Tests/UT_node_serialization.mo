within TANDEM.ElectricalGrid.Tests;
model UT_node_serialization
  ElectricalGrid.Models.grid_node node_2(line_length(displayUnit = "km") = 20000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in(displayUnit = "kV") = 2000) annotation (
    Placement(visible = true, transformation(origin = {26, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R = 450) annotation (
    Placement(visible = true, transformation(origin = {58, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixed_grid_voltage(V = 380000, f = 50) annotation (
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ElectricalGrid.Models.grid_node node_0(line_length(displayUnit = "km") = 20000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in = 2000) annotation (
    Placement(visible = true, transformation(origin = {-20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage power_plant1(V = 2000, f = 50) annotation (
    Placement(visible = true, transformation(origin = {26, 34}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance domestic_load(L = 0, R = 3) annotation (
    Placement(visible = true, transformation(origin = {-20, -28}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
equation

  connect(node_2.node_out, imp.terminal) annotation (
    Line(points = {{34, 0}, {48, 0}}));
  connect(fixed_grid_voltage.terminal, node_0.node_in) annotation (
    Line(points = {{-60, 0}, {-26, 0}}));
  connect(domestic_load.terminal, node_0.load_conn) annotation (
    Line(points = {{-20, -18}, {-20, -6}}));
  connect(power_plant1.terminal, node_2.power_plant_conn) annotation (
    Line(points = {{26, 24}, {26, 6}}));
  connect(node_2.node_in, node_0.node_out) annotation (
    Line(points = {{20, 0}, {-12, 0}}));
  annotation (
    uses(Buildings(version = "7.0.0")));
end UT_node_serialization;
