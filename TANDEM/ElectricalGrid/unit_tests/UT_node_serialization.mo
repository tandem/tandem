model UT_node_serialization
  grid_node node_2(line_length (displayUnit = "km")= 20000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in (displayUnit = "kV")= 2000)  annotation(
    Placement(visible = true, transformation(origin = {26, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R = 3) annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage fixed_grid_voltage(V = 380000, f = 50) annotation(
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  grid_node node_0(line_length (displayUnit = "km")= 20000, load_voltage_out(displayUnit = "kV") = 50000, plant_voltage_in = 2000)  annotation(
    Placement(visible = true, transformation(origin = {-20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage power_plant1(V = 2000, f = 50) annotation(
    Placement(visible = true, transformation(origin = {26, 34}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance domestic_load(L = 0, R = 0.5) annotation(
    Placement(visible = true, transformation(origin = {-20, -28}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
equation
  connect(node_2.power_plant_conn, node_2.power_plant_conn) annotation(
    Line(points = {{26, 6}, {26, 6}}, color = {255, 255, 255}));
  connect(node_2.node_out, imp.terminal) annotation(
    Line(points = {{34, 0}, {50, 0}}, color = {0, 0, 0}));
  connect(fixed_grid_voltage.terminal, node_0.node_in) annotation(
    Line(points = {{-60, 0}, {-26, 0}}));
  connect(node_0.node_out, node_2.node_in) annotation(
    Line(points = {{-12, 0}, {20, 0}}));
  connect(power_plant1.terminal, node_2.power_plant_conn) annotation(
    Line(points = {{26, 24}, {26, 6}}));
  connect(domestic_load.terminal, node_0.load_conn) annotation(
    Line(points = {{-20, -18}, {-20, -6}}));
  annotation(
    uses(Buildings(version = "7.0.0")));
end UT_node_serialization;
