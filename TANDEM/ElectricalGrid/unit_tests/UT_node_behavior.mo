model UT_node_behavior
  Buildings.Electrical.AC.OnePhase.Sources.FixedVoltage E(V = 380000, definiteReference = true, f = 50)  annotation(
    Placement(visible = true, transformation(origin = {-60, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Buildings.Electrical.AC.OnePhase.Loads.Impedance imp(R = 3)  annotation(
    Placement(visible = true, transformation(origin = {32, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  grid_node basic_node annotation(
    Placement(visible = true, transformation(origin = {-14, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(imp.terminal, basic_node.node_out) annotation(
    Line(points = {{22, -6}, {-7, -6}}));
  connect(E.terminal, basic_node.node_in) annotation(
    Line(points = {{-50, -6}, {-20, -6}}));
  annotation(
    uses(Buildings(version = "7.0.0")));
end UT_node_behavior;
