# Electrical grid
## Component Description
The electrical grid component serves as a connection between electrical ports of various electrical producers and consumers such as the BOP, HTSE, etc.

The grid component is divided in 2 sub-components : 
- The grid node, a base element including electrical line impedance, compensation devices and transformers. Several nodes can be put in series or parallel in order to produce various grid architectures.
- The "node array" which consists of a serialization of identical nodes with the same loads connected along the line. This allows for a faster configuration of a large grid with homogeneous load repartition. 

There is no fixed grid architecture : it is left to the user to create his own architecture from the 2 sub-components available depending on the simulation needs. 
### Circuit Description 
The node input and output are directly connected to the line impedance. The compensation device, supply transformer and load transformer are all connected in parallel to the output, after this impedance. The transformers can be left floating, i-e it is not mandatory to connect a power supply or a load. 
## Main Modeling Assumptions
- The electrical line is modeled as a RLC impedance, characterized by a linear impedance value. The linear capacity of the line is neglected by default which is realistic for lines shorter than 150 km. 
- The linear impedance value is typical value for 380 kV overhead lines. 
- Transformers characteristics (X/R, Zcc and power) are interpolated from typical values found in the litterature based on the voltage operating point [[1](https://webstore.iec.ch/publication/603), [2](https://www.mdpi.com/1996-1073/10/8/1233), [3](https://pure.tue.nl/ws/files/3431198/446823988742549.pdf)]. 
## Main parameters
The nodes parameters are : 
- Line length (km) : used to compute line impedance.
- Plant voltage (kV) in : source nominal voltage used to calculate transformer characteristics.
- Load voltage out (kV) : load nominal voltage used to calculate transformer characteristics.
- C compensation (F) : capacity value of the compensation device. Default is set to 1 µF.
- L Compensation (H) : inducance value of the compensation device. Default is set to 0 H.

The following parameters are present in component settings but should be left blank : 
- XoR 
- apower_transfo
- Zp
These parameters are computed by the "getTransfoParams" function based on the plant/load voltage value and therefore can be left blank. 

## Prerequisites
The model uses the [Buildings 10.0 Modelica library](https://github.com/lbl-srg/modelica-buildings), which must be opened in order to compile the code. 

## Provided Examples

In the Tests subpackage some usage examples can be found : 

1. UT_node_behavior : test single node behavior with fixed and ideal voltage source, and load. 
2. UT_node_serialization : test 2 nodes in series with fixed grid voltage, an extra ideal voltage source and loads.
3. UT_sizable_grid : test a "random" grid architecture with several nodes, nodes arrays, ideal voltage sources and loads. 