# The TANDEM Modelica library
This project hosts the TANDEM Modelica library. This Open-Source library is devoted to the modeling of an SMR powered hybrid system.


## The TANDEM Project
The [TANDEM](https://tandemproject.eu) (Small Modular ReacTor for a European sAfe aNd Decarbonized Energy Mix) is an Euratom-funded project investigating the potential integration of Small Modular Reactors with other energy sources, storage devices, and non-electric applications, such as district heating and hydrogen production, in low-carbon hybrid energy systems.

The aim of the project is to expore the hybridization potential of SMRs from multiple standpoints, ranging from safety to techno-ecnomic aspects, across different scenarios. To support these assessments, an open-source Modelica library collecting a wide range of dynamic models of hybrid system components has been developed to build hybrid system simulators and investigate the interactions and dynamics of such systems.

![image](resources/TANDEM_LOGO_FINAL_V1.png "TANDEM project icon")


## Description
The components of the libraries (SMR NSSS or BOP, hydrolyzer, heat storage...), as well as usage examples, can be found in the `TANDEM` folder. The `SupportingLibrary` folder contains the external libraries (as files or as links) used within the components :
- [ThermoSysPro](https://thermosyspro.com/) by EDF
- An extraction of the EnergyProcess Library of CEA
- [ThermoPower](https://casella.github.io/ThermoPower/) by Polimi
- [Buildings](https://simulationresearch.lbl.gov/modelica/) by the Lawrence Berkeley National Laboratory.

The models available in the library can be used to build a comprehensive hybrid energy system simulator, enabling the analysis of the system dynamics across different scenarios and the exploration of different control strategies and operational philosophies.
In addition to the main models for each hybrid system subcomponent, the library includes several test cases, collected into dedicated packages, to showcase the usage of such models. Moreover, multiple versions are available for some components (e.g., nuclear steam supply system and balance of plant) both for benchmarking purposes and to allow users to select the model that best meets their requirements.

## Installation & Usage
The recommended way to download the library is by running `git clone` of the repository. It is then necessary to `cd` into the repository folder and run:
```
git submodule init
git submodule update
```
to _clone_ some of the supporting libraries that are hosted on their own repositories. To download updates, both `git pull` and `git submomdule update` have to be run.

It is also possible to directly download the `zip` file, but in that case the supporting library that are hosted in other repository have to be downloaded individually.

---

Once your local files are ready, to load the required libraries and the TANDEM one in Dymola, you can use the `loadTANDEM.mos` file (tested with Dymola).

The TANDEM library has been tested in Dymola only.

## Documentation
Detailed documentation for the library can be found in:
- The deliverable 2.3 of the TANDEM project, [_Modelica models description for 
the ‘TANDEM’ library_](https://tandemproject.eu/wp-content/uploads/2024/07/D2_3_Modelica_models_description_for_the_tandem_library_V1-1.pdf),  hosted on the [TANDEM website](tandemproject.eu).
- [G. Simonini et al.. Integrating Small Modular Reactors into hybrid energy systems: the tandem Modelica library. _International Conference on Small Modular Reactors and their Applications_, IAEA, Oct 2024, Vienne, Austria.](https://hal.science/hal-04645663).

Please use the latter to cite this library. 

## Roadmap
The first version of this library was developed as an official deliverable (the 2.4) of the TANDEM project in the 2024 summer. Corrections, modifications and upgrades of the library may be provided by the partners during the upcoming phases of the project (development of the _hybrid energy system_ simulator, study cases...). 

## Authors and acknowledgment
This library is developed by the partners of the TANDEM project (2022-2025), funded by the Euratom Research and Training Programme of the European Union.

## License
The Modelica TANDEM Library is available under a 3-clause BSD-license *slightly modified* (additional paragraph at the end) to makes it easy to accept improvements. See the full licence [here](https://gitlab.pam-retd.fr/tandem/tandem/-/blob/main/LICENSE.md?ref_type=heads). 
